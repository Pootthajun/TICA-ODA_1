﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Public Class ODAENG

    Public Function GetTHDate(pDate As String) As String
        Dim strDate As String = ""
        Dim strMonth As String = ""
        Dim NumMonth As String = pDate.Substring(4, 2)
        Dim intDay As Integer = 1
        Try
            intDay = Convert.ToInt16(pDate.Substring(6, 2))
        Catch ex As Exception

        End Try

        If NumMonth = "01" Then
            strMonth = "มกราคม"
        ElseIf NumMonth = "02" Then
            strMonth = "กุมภาพันธ์"
        ElseIf NumMonth = "03" Then
            strMonth = "มีนาคม"
        ElseIf NumMonth = "04" Then
            strMonth = "เมษายน"
        ElseIf NumMonth = "05" Then
            strMonth = "พฤษภาคม"
        ElseIf NumMonth = "06" Then
            strMonth = "มิถุนายน"
        ElseIf NumMonth = "07" Then
            strMonth = "กรกฎาคม"
        ElseIf NumMonth = "08" Then
            strMonth = "สิงหาคม"
        ElseIf NumMonth = "09" Then
            strMonth = "กันยายน"
        ElseIf NumMonth = "10" Then
            strMonth = "ตุลาคม"
        ElseIf NumMonth = "11" Then
            strMonth = "พฤศจิกายน"
        ElseIf NumMonth = "12" Then
            strMonth = "ธันวาคม"
        End If

        Dim Year As Long = 0
        Try
            Year = Convert.ToInt64(pDate.Substring(0, 4)) + 543
        Catch ex As Exception

        End Try

        strDate = intDay.ToString() + " " + strMonth + " " + Year.ToString()

        Return strDate

    End Function

    Public Function GetTHDateAbbr(pDate As String) As String
        Dim strDate As String = ""
        Dim strMonth As String = ""
        Dim NumMonth As String = pDate.Substring(4, 2)
        Dim intDay As Integer = 1
        Try
            intDay = Convert.ToInt16(pDate.Substring(6, 2))
        Catch ex As Exception

        End Try

        If NumMonth = "01" Then
            strMonth = "ม.ค."
        ElseIf NumMonth = "02" Then
            strMonth = "ก.พ."
        ElseIf NumMonth = "03" Then
            strMonth = "มี.ค."
        ElseIf NumMonth = "04" Then
            strMonth = "เม.ย."
        ElseIf NumMonth = "05" Then
            strMonth = "พ.ค."
        ElseIf NumMonth = "06" Then
            strMonth = "มิ.ย."
        ElseIf NumMonth = "07" Then
            strMonth = "ก.ค."
        ElseIf NumMonth = "08" Then
            strMonth = "ส.ค."
        ElseIf NumMonth = "09" Then
            strMonth = "ก.ย."
        ElseIf NumMonth = "10" Then
            strMonth = "ต.ค."
        ElseIf NumMonth = "11" Then
            strMonth = "พ.ย."
        ElseIf NumMonth = "12" Then
            strMonth = "ธ.ค."
        End If

        Dim Year As Long = 0
        Try
            Year = Convert.ToInt64(pDate.Substring(0, 4)) + 543
        Catch ex As Exception

        End Try

        strDate = intDay.ToString() + " " + strMonth + " " + Year.ToString()

        Return strDate

    End Function

    Public Function GetList_BudgetDetail(id As Long, strSearch As String, Optional budgetYear As String = "") As DataTable
        Dim sql As String = "Select ROW_NUMBER() OVER(ORDER BY b.id ASC) As Seq,b.id,b.budget_year,record," & Environment.NewLine
        sql &= " (select [dbo].[GetFullThaiDate](b.received_date)) str_received_date," & Environment.NewLine
        sql &= " received_date, sum(d.amount) amount, b.received_by, b.[description]" & Environment.NewLine
        sql &= " From tb_budget b inner join tb_budget_detail d on b.id=d.budget_id where 1=1  " & Environment.NewLine

        If id <> 0 Then
            sql &= " And b.id = @_ID"
        End If
        If strSearch.Trim <> "" Then
            sql &= " And (budget_year Like '%' + @_SEARCH + '%' or (select [dbo].[GetFullThaiDate](b.received_date)) like '%' + @_SEARCH + '%')"
        End If
        If budgetYear <> "" Then
            sql &= " and budget_year = @_BUDGET_YEAR"
        End If
        sql &= " Group by b.id, budget_year, record,b.received_date, b.received_by, b.[description]"
        sql &= " order by budget_year, record"

        Dim p(3) As SqlParameter
        p(0) = SqlDB.SetInt("@_ID", id)
        p(1) = SqlDB.SetText("@_SEARCH", strSearch.Replace("'", "''"))
        p(2) = SqlDB.SetText("@_BUDGET_YEAR", budgetYear)

        Dim dt As New DataTable
        dt = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function


    Public Function GetList_Report2(strSearch As String) As DataTable
        Dim sql As String = "Select id,names,detail From TB_Multilateral WHERE 1=1 "
        If strSearch.Trim <> "" Then
            sql &= "And (names Like '%' + @_SEARCH + '%')"
        End If

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_SEARCH", strSearch.Replace("'", "''"))

        Dim dt As New DataTable
        dt = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Public Function GetPlan(project_id As String) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = "select PL.plan_name,(select isnull(sum(ED.amount),0)from TB_Expense_Detail As ED " + Environment.NewLine
            sql += "inner join TB_Expense E On E.id = ED.expense_id where e.project_id=P.id ) as expense" + Environment.NewLine
            sql += ",(select isnull(sum(ab.amount),0) from TB_Activity_Budget As ab" + Environment.NewLine
            sql += "join tb_activity As a On a.id = ab.activity_id where a.project_id = P.id) as allocate_budget" + Environment.NewLine
            sql += ",P.project_name,[dbo].GetFullThaiDate(PL.start_date) As StartPlan_date," + Environment.NewLine
            sql += "[dbo].GetFullThaiDate(PL.end_date) As EndPlan_date,((select isnull(sum(ab.amount),0)" + Environment.NewLine
            sql += "from TB_Activity_Budget As ab join tb_activity As a On a.id = ab.activity_id " + Environment.NewLine
            sql += "where a.project_id = P.id)) - (select isnull(sum(ED.amount),0) from TB_Expense_Detail As ED " + Environment.NewLine
            sql += "inner join TB_Expense E On E.id = ED.expense_id where e.project_id=P.id ) As Total " + Environment.NewLine
            sql += ",[dbo].GetFullThaiDate(P.start_date)+' ถึงวันที่ '+[dbo].GetFullThaiDate(P.end_date) As date" + Environment.NewLine
            sql += "from TB_Plan As PL left join TB_ProjectPlan PP On PL.id = PP.plan_id " + Environment.NewLine
            sql += "left join TB_Project As P On PP.project_id = P.id where PL.id =@_PROJECT_ID"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function

    Public Function sumamount(project_id As String) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql += "select Sum(Ed.amount) sumamount from TB_Expense_Detail As Ed join TB_Expense As e on Ed.expense_id = e.id  where e.project_id = @_PROJECT_ID"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function
    Public Function GetReportExpense(project_id As String) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = "select P.project_name,Op.name_th,Es.sub_name,(select [dbo].[GetFullThaiDate](E.payment_date))" + Environment.NewLine
            sql += " Pay_MentDate,Ed.activity_id,A.activity_name,Ed.amount,Ed.amount As amount2 " + Environment.NewLine
            sql += " from TB_Expense_Sub As Es join TB_Expense_Detail As Ed on Es.id = Ed.expense_sub_id join" + Environment.NewLine
            sql += " TB_Activity As A on A.id = Ed.activity_id Join TB_Project P On A.project_id= P.id join TB_Expense As E On P.id = E.project_id" + Environment.NewLine
            sql += " join TB_OU_Person As Op On Ed.recipince_id = Op.node_id Where P.id = @_PROJECT_ID"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function
    Public Function GetProject(project_id As String) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " Select *,(budget-expense) balance from ( " + Environment.NewLine
            sql += " Select id,project_id,Project_name,start_date,end_date," + Environment.NewLine
            sql += " (select sum(amount) from TB_Activity_Budget As ab join tb_activity As a On a.id = ab.activity_id join TB_Project As P On P.id = a.project_id where a.project_id=@_PROJECT_ID) budget," + Environment.NewLine
            sql += " (Select  isnull(sum(amount), 0) from TB_Expense_Detail where activity_id in (select activity_id from tb_activity where project_id=@_PROJECT_ID)) expense," + Environment.NewLine
            sql += " (Select [dbo].GetFullThaiDate(start_date)) strStartP_date,(Select [dbo].GetFullThaiDate(end_date)) strEndP_date" + Environment.NewLine
            sql += " From tb_project Where id =@_PROJECT_ID" + Environment.NewLine
            sql += " ) T"
            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function

    Public Function GetAllActivityByProject(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " With activity (parent_id, id, activity_name, Level, plan_start, plan_end, duration) As" + Environment.NewLine
            sql += " (" + Environment.NewLine
            sql += " Select parent_id,id,isnull(activity_name,[description]) activity_name, 0 As Level, plan_start, plan_end,datediff(day, plan_start, plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity a Where PROJECT_ID =  @_PROJECT_ID And parent_id=0" + Environment.NewLine
            sql += " UNION ALL" + Environment.NewLine
            sql += " Select a.parent_id,a.id,isnull(a.activity_name,[description]) activity_name, Level+1 As Level " + Environment.NewLine
            sql += " , a.plan_start, a.plan_end,datediff(day, a.plan_start, a.plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity As a" + Environment.NewLine
            sql += " INNER JOIN activity As at" + Environment.NewLine
            sql += " On a.Parent_ID=at.ID " + Environment.NewLine
            sql += " )" + Environment.NewLine

            sql += " Select *,(budget - expense) balance from (" + Environment.NewLine
            sql += " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name," + Environment.NewLine
            sql += " Len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," + Environment.NewLine
            sql += " isnull((Select sum(amount) from tb_activity_budget where activity_id= activity.id),0) commitment_budget" + Environment.NewLine
            sql += " ,(Select isnull(sum(amount),0)  from TB_Activity_Budget " + Environment.NewLine
            sql += " where activity_id = activity.id) budget" + Environment.NewLine
            sql += " ,(Select isnull(sum(amount),0) from TB_Expense_Detail" + Environment.NewLine
            sql += " where activity_id = activity.id) expense," + Environment.NewLine
            sql += " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" + Environment.NewLine
            sql += " From activity) T" + Environment.NewLine
            sql += " Order By ID" + Environment.NewLine


            'sql += " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name,len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," + Environment.NewLine
            'sql += " isnull((Select sum(amount) from tb_activity_budget where activity_id= activity.id),0) commitment_budget," + Environment.NewLine
            'sql += " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" + Environment.NewLine
            'sql += " From activity" + Environment.NewLine
            'sql += " ORDER BY ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function
End Class
