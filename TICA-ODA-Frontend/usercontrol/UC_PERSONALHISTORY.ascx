﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_PERSONALHISTORY.ascx.vb" Inherits="usercontrol_UC_PERSONALHISTORY" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div >
    <!-- BEGIN FORM-->

    <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control select2" Width="100%"></asp:DropDownList>

    <h5 class="text-info"><b>A. PERSONAL HISTORY</b></h5>
    <table class="table">
        <tr>
            <th style="width: 250px"><b class="pull-right">Prefix <span style ="color :red ;"  > </span>:</b></th>
            <td>

                <div class="col-sm-12">
                    <asp:DropDownList ID="ddlPrefix" runat="server" CssClass="form-control select2" Width="180px"></asp:DropDownList>
                </div>

            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Family name </br> (as shown in passport) <span style ="color :red ;"  > </span>:</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtFamilyName" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Given names <span style ="color :red ;"  > </span>:</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtGivenName" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Sex  <!-(เพศ)--> <span style ="color :red ;"  > </span>:</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:DropDownList ID="ddlSex" runat="server" CssClass="form-control select2" Width="180px"></asp:DropDownList>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Country Of Birth <span style ="color :red ;"  > </span>:</b></th>
            <td>

                <div class="col">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Width="180px"></asp:DropDownList>
                </div>
                <div class="coltext">
                    <b class="pull-right">City :</b>
                </div>
                <div class="col">
                    <asp:TextBox ID="txtCity" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
                </div>

            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Nationality :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtNationality" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Date of Birth :</b></th>
            <td>

                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox CssClass="form-control m-b" ID="txtBirthDate" runat="server" placeholder=""></asp:TextBox>
                        <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server"
                            format="dd/MM/yyyy" targetcontrolid="txtBirthDate" popupposition="BottomLeft"></ajaxtoolkit:calendarextender>
                    </div>
                    <!-- /.input group -->
                </div>

            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Marital Status :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="form-control select2" Width="180px"></asp:DropDownList>
                </div>

            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Religion :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtReligion" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Work Address :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtWorkAddress" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtWorkTelphone" runat="server" Width="90%" MaxLength="50"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Fax :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtFax" runat="server" Width="90%" MaxLength="50"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Email :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Width="90%" MaxLength="250"></asp:TextBox><br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Style="color: red"
                        ControlToValidate="txtEmail" Display="Dynamic"
                        ErrorMessage="Invalid email format"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                    </asp:RegularExpressionValidator>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Home Address :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtHomeAddress" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtHomeTelephone" runat="server" Width="90%" MaxLength="50"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">International Airport :</b><br />
                <b class="pull-right">City for departure :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtCityDepartture" runat="server" TextMode="MultiLine" Width="90%" MaxLength="500" Height="60px"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Name and address of person to<br />
                be notified in case of emergency :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtEmergencyPerson" runat="server" Width="90%" MaxLength="250"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtEmergencyTelephone" runat="server" Width="90%" MaxLength="50"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <th style="width: 250px"><b class="pull-right">Relationship with applicant:</b></th>
            <td>
                <div class="col-sm-12">
                    <asp:TextBox ID="txtEmergencyRelationship" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                </div>
            </td>
        </tr>
    </table>



    <hr>
    <!-- <div class="control-group">
		<label class="control-label"></label>
			<div class="controls">
				<a  href="#Edu" data-toggle="tab" type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
				<button type="button" class="btn red">Cancel</button>
			</div>
	</div>-->

</div>
