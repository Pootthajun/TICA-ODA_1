﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Partial Class Print_rptProjectDetail
    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim _ProjectID As String = Request.QueryString("ProjectID")
        Dim _ProjectType As String = Request.QueryString("ProjectTypeName")
        Dim dt As New DataTable
        dt = BL.GetProjectInfoByID(_ProjectID)
        Dim cc As New ReportDocument()



        Dim _assistant As String = ""
        Dim Project_Code As String = ""
        If dt.Rows.Count > 0 Then
            _assistant = dt.Rows(0)("assistant").ToString
            Project_Code = dt.Rows(0)("project_id").ToString

        End If

        Dim _assistantname As String = ""
        Dim _dtas As New DataTable
        If _assistant <> "" Then
            Dim _strAssistant() As String = _assistant.Split(", ")
            _dtas = BL.GetAssistantByID(_assistant)
            For i As Integer = 0 To _dtas.Rows.Count - 1
                _assistantname &= _dtas.Rows(i)("name_th")
                If i < (_dtas.Rows.Count - 1) Then _assistantname &= ","
            Next
        End If

        Dim _dtfile As New DataTable
        _dtfile = BL.GetProjectFile(_ProjectID)

        Dim _dtact As New DataTable
        _dtact = BL.GetAllActivityByProject(_ProjectID)

        Dim _dtImp As New DataTable
        _dtImp = BL.GetProjectImplementingAgency(_ProjectID)

        Dim _dtcof As New DataTable
        _dtcof = BL.GetProjectCoFunding(_ProjectID)

        Dim _dtrec As New DataTable
        _dtrec = BL.GetProjectRecipincePerson(_ProjectID)
        Dim PrintDate As String = BL.GetTHDateAbbr(DateTime.Now.ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))) + " " + DateTime.Now.ToString("HH:mm", New System.Globalization.CultureInfo("en-US"))

        cc.Load(Server.MapPath("../reports/rptProjectDetail.rpt"))
        cc.DataDefinition.FormulaFields("PrintDate").Text = "'" + PrintDate + "'"
        cc.DataDefinition.FormulaFields("ProjectType").Text = "'" + _ProjectType + "'"
        cc.DataDefinition.FormulaFields("Assistant").Text = "'" + _assistantname + "'"
        cc.Subreports("SUB_ImplementingAgency").SetDataSource(_dtImp)
        cc.Subreports("SUB_CoFunding").SetDataSource(_dtcof)
        cc.Subreports("SUB_File").SetDataSource(_dtfile)
        cc.Subreports("SUB_Activity").SetDataSource(_dtact)
        cc.Subreports("SUB_Recipience").SetDataSource(_dtrec)
        cc.SetDataSource(dt)

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=รายละเอียดของ_" + _ProjectType + "_" + Project_Code + "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=รายละเอียดของ_" + _ProjectType + "_" + Project_Code + "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub


End Class
