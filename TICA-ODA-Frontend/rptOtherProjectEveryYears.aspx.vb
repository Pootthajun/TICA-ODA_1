﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidSummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptOtherProjectEveryYears_Page")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptOtherProjectEveryYears_Page") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        'li.Attributes.Add("class", "active")
        'Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_foreign")
        'li_mnuReports_foreign.Attributes.Add("class", "active")
        'Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptOtherProjectEveryYears")
        'a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            ProjectType = 0  ' เนื่องจากเฉพาะประเภทของ Project Type=0
            BindList()
            BL.Bind_DDL_Year(ddlProject_Year, True)


        End If

    End Sub

    Public Function GetProjectList(project_type As Integer) As DataTable

        Dim dt As New DataTable
        Dim Title As String = ""
        Dim sql As String = ""
        Try
            sql = " select ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq ,_vw_Project_Year_Pay_Actual.* " + Environment.NewLine
            sql += " FROM _vw_Project_Year_Pay_Actual " + Environment.NewLine
            sql += " WHERE 1=1 AND Pay_Amount_Actual > 0 AND Pay_Amount_Actual IS NOT NULL  " + Environment.NewLine


            If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
                sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
            End If

            If Not String.IsNullOrEmpty(txtSearch_Project.Text) Then
                sql += " and project_name like '%" + txtSearch_Project.Text + "%' " + Environment.NewLine
            End If
            sql += "order by budget_year Desc ,project_name  "
            dt = SqlDB.ExecuteTable(sql)
            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " ประเทศ"
            End If

            Session("Search_Other_Project_EveryYears_Title") = lblTotalRecord.Text

        Catch ex As Exception
        End Try
        Return dt

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetProjectList(ProjectType)

        If (DT.Rows.Count > 0) Then
            lblProject_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Country_Amount)", "")).ToString("#,##0")
            lblDisbursed_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("rptOtherProjectEveryYears_Page") = DT



        AllData = DT
        PageNavigation.SesssionSourceName = "rptOtherProjectEveryYears_Page"
        PageNavigation.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles PageNavigation.PageChanging
        PageNavigation.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Dim Lastproject_id As String = ""
    Dim LastSector As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblproject_id As Label = DirectCast(e.Item.FindControl("lblproject_id"), Label)
        Dim lblproject_name As Label = DirectCast(e.Item.FindControl("lblproject_name"), Label)
        Dim lblCountry_Amount As Label = DirectCast(e.Item.FindControl("lblCountry_Amount"), Label)
        Dim lblDisbursed_Amount As Label = DirectCast(e.Item.FindControl("lblDisbursed_Amount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        Dim tdproject_id As HtmlTableCell = e.Item.FindControl("tdproject_id")
        Dim btnSelect As Button = e.Item.FindControl("btnSelect")

        For i As Integer = 1 To 5
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If

        '--------LastCountry------------
        If Lastproject_id <> e.Item.DataItem("project_id").ToString Then
            Lastproject_id = e.Item.DataItem("Project_Code").ToString
            lblproject_id.Text = Lastproject_id
            'LastSector = ""
        Else
            'LastSector = ""
            tdproject_id.Attributes("style") &= DuplicatedStyleTop
        End If


        lblNo.Text = e.Item.DataItem("seq").ToString
        'lblCountry.Text = e.Item.DataItem("country_name_th").ToString
        'lblSector.Text = e.Item.DataItem("perposecat_name").ToString
        lblproject_name.Text = e.Item.DataItem("project_name").ToString
        lblCountry_Amount.Text = e.Item.DataItem("Country_Amount").ToString
        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblDisbursed_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        lblproject_name.Attributes("project_id") = e.Item.DataItem("project_id")
        lblproject_name.Attributes("project_type") = e.Item.DataItem("project_type")


    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblproject_name As Label = DirectCast(e.Item.FindControl("lblproject_name"), Label)
        Dim btnSelect As Button = e.Item.FindControl("Button")

        Dim _project_id As String = lblproject_name.Attributes("project_id")
        Dim _project_type As String = lblproject_name.Attributes("project_type")

        Dim para As String = GetParameter("PDF", _project_id)

        Select Case e.CommandName
            Case "select"

                If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then

                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProjectDetail.aspx?Mode=PDF" + para + "');", True)

                End If
                If ProjectType = Constants.Project_Type.Loan Then

                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptLoanDetail.aspx?Mode=PDF" + para + "');", True)

                End If
                If ProjectType = Constants.Project_Type.Contribuition Then

                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptContributionDetail.aspx?Mode=PDF" + para + "');", True)

                End If

        End Select
    End Sub

    Function GetParameter(Reportformat As String, ProjectID As Integer) As String
        Dim para As String = "&ReportName=rptProjectDetail"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectID=" & ProjectID

        If ProjectType = Constants.Project_Type.Project Then
            para += "&ProjectTypeName=Project"
        End If
        If ProjectType = Constants.Project_Type.NonProject Then
            para += "&ProjectTypeName=Non Project"
        End If

        Return para
    End Function

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptOtherProjectEveryYears.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptOtherProjectEveryYears.aspx?Mode=EXCEL');", True)
    End Sub

#End Region

End Class
