﻿Imports System.Data
Public Class Image_Upload
    Inherits System.Web.UI.Page

    Dim C As New ConverterENG


    Protected Property ProductImage As Byte()
        Get
            Try
                Return Session("Image")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Byte())
            Session("Image") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub

        Dim fileContent As HttpPostedFile = Request.Files(0)
        Dim b As Byte() = C.StreamToByte(fileContent.InputStream)
        ProductImage = b

    End Sub



End Class