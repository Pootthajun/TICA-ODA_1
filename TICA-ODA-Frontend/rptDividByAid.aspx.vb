﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidSummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptDividByAid_Page")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptDividByAid_Page") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        'li.Attributes.Add("class", "active")
        'Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_foreign")
        'li_mnuReports_foreign.Attributes.Add("class", "active")
        'Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptDividByAid")
        'a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            ProjectType = 0  ' เนื่องจากเฉพาะประเภทของ Project Type=0
            BindList()
            BL.Bind_DDL_Year(ddlProject_Year, True)
            BL.Bind_DDL_Sector_th_By_rptAidByCountrySummary(ddlProject_Sector, True)

        End If

    End Sub

    Public Function GetProjectList(project_type As Integer) As DataTable

        Dim dt As New DataTable
        Dim Title As String = ""
        Dim sql As String = ""


        Try
            sql = "Select ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq, " + Environment.NewLine
            sql += "budget_year,sector_id,perposecat_name,count(sector_id) as Project_Amount,SUM(convert(decimal(18,2),Disbursement_For_Country)) as Disbursed_Amount FROM vw_Summary_Aid SD where 1=1 " + Environment.NewLine

            If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
                sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
            End If

            If Not String.IsNullOrEmpty(ddlProject_Sector.SelectedValue.ToString) Then
                sql += " and sector_id = '" + ddlProject_Sector.SelectedValue.ToString + "' " + Environment.NewLine
            End If


            sql += "group by budget_year,sector_id,perposecat_name" + Environment.NewLine
            sql += "order by budget_year DESC"

            dt = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " ประเทศ"
            End If

            Session("Search_DividByAid_Title") = lblTotalRecord.Text

        Catch ex As Exception
        End Try
        Return dt

    End Function



    'Private Sub BindList()


    '    Dim DT As DataTable = GetProjectList(ProjectType)

    '    If (DT.Rows.Count > 0) Then
    '        lblDisbursed_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Disbursed_Amount)", "")).ToString("#,##0.00")
    '        pnlFooter.Visible = True
    '    Else
    '        pnlFooter.Visible = False
    '    End If

    '    Session("Search_DividByAid") = DT


    '    AllData = DT
    '    Pager.SesssionSourceName = "rptDividByAid_Page"
    '    Pager.RenderLayout()
    'End Sub

    Private Sub BindList()
        Dim Title As String = ""
        Dim sql As String = ""

        sql = " Select ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq ,_vw_Sector_Year_Pay_Actual.* " + Environment.NewLine
        sql += " FROM _vw_Sector_Year_Pay_Actual where 1=1 AND Pay_Amount_Actual >0 AND Pay_Amount_Actual IS NOT NULL   " + Environment.NewLine

        If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
            sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
            Title += " ปีงบประมาณ " & ddlProject_Year.SelectedValue.ToString
        End If

        If Not String.IsNullOrEmpty(ddlProject_Sector.SelectedValue.ToString) Then
            sql += " and sector_id = '" + ddlProject_Sector.SelectedValue.ToString + "' " + Environment.NewLine
        End If

        sql += "order by budget_year DESC,perposecat_name"

        Dim DT As New DataTable
        DT = SqlDB.ExecuteTable(sql)

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_DividByAid_Title") = lblTotalRecord.Text

        If (DT.Rows.Count > 0) Then
            lblProject_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Amount)", "")).ToString("#,##0")
            lblDisbursed_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("rptDividByAid_Page") = DT


        AllData = DT
        Pager.SesssionSourceName = "rptDividByAid_Page"
        Pager.RenderLayout()
    End Sub


    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Dim Lastproject_id As String = ""
    Dim LastSector As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        'Dim lblproject_id As Label = DirectCast(e.Item.FindControl("lblproject_id"), Label)
        Dim lblSector As Label = DirectCast(e.Item.FindControl("lblSector"), Label)
        Dim lblProject_Amount As Label = DirectCast(e.Item.FindControl("lblProject_Amount"), Label)
        Dim lblDisbursed_Amount As Label = DirectCast(e.Item.FindControl("lblDisbursed_Amount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        'Dim tdproject_id As HtmlTableCell = e.Item.FindControl("tdproject_id")
        'Dim tdSector As HtmlTableCell = e.Item.FindControl("tdSector")

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If

        '--------LastCountry------------
        'If Lastproject_id <> e.Item.DataItem("project_id").ToString Then
        '    Lastproject_id = e.Item.DataItem("project_id").ToString
        '    lblproject_id.Text = Lastproject_id
        '    'LastSector = ""
        'Else
        '    'LastSector = ""
        '    tdproject_id.Attributes("style") &= DuplicatedStyleTop
        'End If

        lblNo.Text = e.Item.DataItem("seq").ToString
        lblSector.Text = e.Item.DataItem("perposecat_name").ToString
        lblProject_Amount.Text = e.Item.DataItem("Project_Amount").ToString
        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblDisbursed_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If


    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptDividByAid.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptDividByAid.aspx?Mode=EXCEL');", True)
    End Sub

#End Region

End Class
