﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports ProcessReturnInfo
Imports Constants
Imports System.Globalization
Imports System.Web.UI.UserControl

Public Class ODAENG
    Dim GL As New GenericLib

#Region "Set validate Textbox Property"
    Public Sub SetTextIntKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusInt(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Sub SetTextDblKeypress(txt As TextBox)
        txt.Attributes.Add("OnKeyPress", "ChkMinusDbl(this,event);")
        txt.Attributes.Add("onKeyDown", "CheckKeyNumber(event);")
    End Sub
    Public Sub SetTextAreaMaxLength(txt As TextBox, MaxLength As Int16)
        txt.Attributes.Add("onKeyDown", "checkTextAreaMaxLength(this,event,'" & MaxLength & "');")
    End Sub
#End Region

#Region "_DDL"

    Public Sub Bind_DDL_Activity(ByRef ddl As DropDownList)
        Dim sql As String = " select id,activity_name from tb_activity where notify ='Y' and isnull(activity_name,'') <> '' order by activity_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Activity", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("activity_name").ToString, dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Prefix_En(ByRef ddl As DropDownList)
        Dim sql As String = " select id,prefix_name_en from tb_prefix where active_status='Y' order by prefix_name_en"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Prefix", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("prefix_name_en").ToString, dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub
    Public Sub Bind_DDL_Sex(ByRef ddl As DropDownList)
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("Sex")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("ID") = "M"
        dr("Sex") = "Male"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = "F"
        dr("Sex") = "Female"
        dt.Rows.Add(dr)

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("Sex"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Country_En(ByRef ddl As DropDownList)
        Dim sql As String = "select node_id,name_en, '0' parent_id from TB_OU_Country where isnull(active_status,'Y') = 'Y' order by name_en"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_MaritalStatus(ByRef ddl As DropDownList)
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("MaritalStatus")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("ID") = "1"
        dr("MaritalStatus") = "Single"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = "2"
        dr("MaritalStatus") = "married"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = "3"
        dr("MaritalStatus") = "separated"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = "4"
        dr("MaritalStatus") = "divorced"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = "5"
        dr("MaritalStatus") = "widowed"
        dt.Rows.Add(dr)

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("MaritalStatus"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub
#End Region

#Region "_Recipince"
    Public Function GetOUNodeID() As String
        Return Convert.ToDateTime(DateTime.Now).ToString("yyyyMMddHHmmssff", New System.Globalization.CultureInfo("th-TH"))
    End Function

    Public Function SaveRecipience(lnqouperson As TbRecipiencePersonLinqDB, lnqrec As TbRecipienceLinqDB, DTEducation As DataTable, DTLanguage As DataTable, DTEmpRecpord As DataTable, username As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB

        Try

            Dim exc As New ExecuteDataInfo
            Dim _lnqperson As New TbOuPersonLinqDB
            With _lnqperson
                .GetDataByPK(lnqouperson.ID, trans.Trans)
                .PREFIX_TH_ID = lnqouperson.PREFIX_TH_ID
                .NAME_TH = lnqouperson.NAME_TH
                .PREFIX_EN_ID = lnqouperson.PREFIX_EN_ID
                .NAME_EN = lnqouperson.NAME_EN
                .PARENT_ID = lnqouperson.PARENT_ID
                .BIRTHDATE = lnqouperson.BIRTHDATE
                .PERSON_TYPE = "1"
                .ACTIVE_STATUS = "Y"
                If .ID > 0 Then
                    exc = .UpdateData(username, trans.Trans)
                Else
                    .NODE_ID = GetOUNodeID()
                    exc = .InsertData(username, trans.Trans)
                End If
            End With
            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If


            Dim _lnq As New TbRecipienceLinqDB
            With _lnq
                .ChkDataByPERSON_ID(_lnqperson.NODE_ID, trans.Trans)
                .ACTIVITY_ID = lnqrec.ACTIVITY_ID
                .PERSON_ID = _lnqperson.NODE_ID
                .PSH_SEX = lnqrec.PSH_SEX
                .PSH_CITY = lnqrec.PSH_CITY
                .PSH_NATIONALITY = lnqrec.PSH_NATIONALITY
                .PSH_MARITAL_STATUS = lnqrec.PSH_MARITAL_STATUS
                .PSH_RELIGION = lnqrec.PSH_RELIGION
                .PSH_WORK_ADDRESS = lnqrec.PSH_WORK_ADDRESS
                .PSH_WORK_TELEPHONE = lnqrec.PSH_WORK_TELEPHONE
                .PSH_WORK_FAX = lnqrec.PSH_WORK_FAX
                .PSH_WORK_EMAIL = lnqrec.PSH_WORK_EMAIL
                .PSH_HOME_ADDRESS = lnqrec.PSH_HOME_ADDRESS
                .PSH_HOME_TELEPHONE = lnqrec.PSH_HOME_TELEPHONE
                .PSH_CITY_DEPARTTURE = lnqrec.PSH_CITY_DEPARTTURE
                .PSH_EMERGENCY_CONTACT = lnqrec.PSH_EMERGENCY_CONTACT
                .PSH_EMERGENCY_TELEPHONE = lnqrec.PSH_EMERGENCY_TELEPHONE
                .PSH_EMERGENCY_RELATIONSHIP = lnqrec.PSH_EMERGENCY_RELATIONSHIP
                .EDU_TOEFLSCORE = lnqrec.EDU_TOEFLSCORE
                .EDU_IELTSSCORE = lnqrec.EDU_IELTSSCORE
                .EDU_OTHERSCORE = lnqrec.EDU_OTHERSCORE
                .EDU_TRAINED_DETAIL = lnqrec.EDU_TRAINED_DETAIL
                .EDU_PUBLICATIONS = lnqrec.EDU_PUBLICATIONS
                .EXP_EXPECTATION_DESCRIPTION = lnqrec.EXP_EXPECTATION_DESCRIPTION
                .EXP_SIGNATURE = lnqrec.EXP_SIGNATURE
                .EXP_PRINT_NAME = lnqrec.EXP_PRINT_NAME
                .EXP_PRINT_DATE = lnqrec.EXP_PRINT_DATE
                .GA_TITLE_POST = lnqrec.GA_TITLE_POST
                .GA_DUTIES = lnqrec.GA_DUTIES
                .GA_STAMP = lnqrec.GA_STAMP
                .GA_TITLE = lnqrec.GA_TITLE
                .GA_ORG = lnqrec.GA_ORG
                .GA_ADDRESS = lnqrec.GA_ADDRESS
                .GA_DATE = lnqrec.GA_DATE
                .PROFILE_IMAGE = lnqrec.PROFILE_IMAGE
                .ACTIVE_STATUS = lnqrec.ACTIVE_STATUS

                If .ID > 0 Then
                    exc = .UpdateData(username, trans.Trans)
                Else
                    exc = .InsertData(username, trans.Trans)
                End If
            End With

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            '== Education
            Dim p_edu(1) As SqlParameter
            p_edu(0) = SqlDB.SetText("@_RECIPIENCE_ID", _lnq.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Recipience_Education WHERE recipience_id=@_RECIPIENCE_ID", trans.Trans, p_edu)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            For cnt As Integer = 0 To DTEducation.Rows.Count - 1
                Dim _lnqedu As New TbRecipienceEducationLinqDB
                With _lnqedu
                    .RECIPIENCE_ID = _lnq.ID
                    .INSTITUTION = DTEducation.Rows(cnt)("INSTITUTION").ToString
                    .CITY = DTEducation.Rows(cnt)("CITY").ToString
                    .ATTENDED_FROM = DTEducation.Rows(cnt)("ATTENDED_FROM").ToString
                    .ATTENDED_TO = DTEducation.Rows(cnt)("ATTENDED_TO").ToString
                    .CERTIFICATES = DTEducation.Rows(cnt)("CERTIFICATES").ToString
                    .SPECIAL_FIELDS = DTEducation.Rows(cnt)("SPECIAL_FIELDS").ToString
                    .RECORD = cnt + 1

                    exc = .InsertData(username, trans.Trans)
                End With

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            '== Language
            Dim p_lng(1) As SqlParameter
            p_lng(0) = SqlDB.SetText("@_RECIPIENCE_ID", _lnq.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Recipience_Language WHERE recipience_id=@_RECIPIENCE_ID", trans.Trans, p_lng)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            For cnt As Integer = 0 To DTLanguage.Rows.Count - 1
                Dim _lnqlng As New TbRecipienceLanguageLinqDB
                With _lnqlng
                    .RECIPIENCE_ID = _lnq.ID
                    .LANGUAGES = DTLanguage.Rows(cnt)("LANGUAGES").ToString
                    .SKILL_READ = DTLanguage.Rows(cnt)("SKILL_READ").ToString
                    .SKILL_WRITE = DTLanguage.Rows(cnt)("SKILL_WRITE").ToString
                    .SKILL_SPEAK = DTLanguage.Rows(cnt)("SKILL_SPEAK").ToString
                    .RECORD = cnt + 1

                    exc = .InsertData(username, trans.Trans)
                End With

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            '== TB_Recipience_EmpRecord
            Dim p_emp(1) As SqlParameter
            p_emp(0) = SqlDB.SetText("@_RECIPIENCE_ID", _lnq.ID)
            exc = SqlDB.ExecuteNonQuery("DELETE FROM TB_Recipience_EmpRecord WHERE recipience_id=@_RECIPIENCE_ID", trans.Trans, p_emp)

            If exc.IsSuccess = False Then
                trans.RollbackTransaction()
                ret.IsSuccess = False
                ret.ErrorMessage = exc.ErrorMessage()
                Return ret
            End If

            For cnt As Integer = 0 To DTEmpRecpord.Rows.Count - 1
                Dim _lnqlng As New TbRecipienceEmprecordLinqDB
                With _lnqlng
                    .RECIPIENCE_ID = _lnq.ID
                    .POST_FROM = DTEmpRecpord.Rows(cnt)("POST_FROM").ToString
                    .POST_TO = DTEmpRecpord.Rows(cnt)("POST_TO").ToString
                    .TITLE_POST = DTEmpRecpord.Rows(cnt)("TITLE_POST").ToString
                    .ORG_NAME = DTEmpRecpord.Rows(cnt)("ORG_NAME").ToString
                    .ORG_TYPE = DTEmpRecpord.Rows(cnt)("ORG_TYPE").ToString
                    .ADDRESS = DTEmpRecpord.Rows(cnt)("ADDRESS").ToString
                    .DESCRIPTION = DTEmpRecpord.Rows(cnt)("DESCRIPTION").ToString
                    .RECORD = cnt + 1

                    exc = .InsertData(username, trans.Trans)
                End With

                If exc.IsSuccess = False Then
                    trans.RollbackTransaction()
                    ret.IsSuccess = False
                    ret.ErrorMessage = exc.ErrorMessage()
                    Return ret
                End If
            Next

            trans.CommitTransaction()
            ret.IsSuccess = True
            ret.ErrorMessage = ""
        Catch ex As Exception
            trans.RollbackTransaction()
            ret.IsSuccess = False
            ret.ErrorMessage = ex.ToString()
        End Try
        Return ret
    End Function
#End Region




    '===========GetAllActivityByProject  สำหรับ Loan
    Public Function GetAllActivityByLoan(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " With activity (parent_id,id,activity_name, Level,plan_start, plan_end,duration) As" + Environment.NewLine
            sql += " (" + Environment.NewLine
            sql += " Select parent_id,id,isnull(activity_name,[description]) activity_name, 0 As Level, plan_start, plan_end,datediff(day, plan_start, plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity a Where PROJECT_ID =  @_PROJECT_ID And parent_id=0" + Environment.NewLine
            sql += " UNION ALL" + Environment.NewLine
            sql += " Select a.parent_id,a.id,isnull(a.activity_name,[description]) activity_name, Level+1 As Level " + Environment.NewLine
            sql += " , a.plan_start, a.plan_end,datediff(day, a.plan_start, a.plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity As a" + Environment.NewLine
            sql += " INNER JOIN activity As at" + Environment.NewLine
            sql += " On a.Parent_ID=at.ID " + Environment.NewLine
            sql += " )" + Environment.NewLine
            sql += " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name,len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," + Environment.NewLine
            sql += " isnull((Select sum(commitment_budget) from TB_Activity where id= activity.id),0) commitment_budget," + Environment.NewLine
            sql += " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" + Environment.NewLine
            sql += " From activity" + Environment.NewLine
            sql += " ORDER BY ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function

#Region "DDL"

    Public Sub BindData_dll(ByRef ddl As DropDownList, ByVal UserName As String, ByRef DataTableName As DataTable, ByVal SelectTitle As String, ByVal itemName As String)
        Dim dt As DataTable = DataTableName
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem(SelectTitle, ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item(itemName), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)

        Next
        ddl.SelectedIndex = 0
    End Sub

    'Public Sub Bind_DDL_GroupBudget(ByRef ddl As DropDownList)
    '    Dim sql As String = " select id,group_name,active_status from tb_budget_group where isnull(active_status,'Y')='Y' order by group_name"
    '    Dim dt As DataTable = SqlDB.ExecuteTable(sql)

    '    ddl.Items.Clear()
    '    ddl.Items.Add(New ListItem("Select Group Budget", ""))
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
    '        ddl.Items.Add(Item)
    '    Next
    '    ddl.SelectedIndex = 0
    'End Sub

    Public Sub Bind_DDL_Recipience(ByRef ddl As DropDownList)
        Dim sql As String = " select * from vw_PSN_IN_Country where person_type = 1"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Recipience", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CountryByRecipience(ByRef ddl As DropDownList, node_ID As String)
        Dim sql As String = " select * from vw_PSN_IN_Country where person_type = 1"
        sql += " and node_id = '" & node_ID & "'"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th_OU"), dt.Rows(i).Item("node_id_OU"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_DisbursementType(ByRef ddl As DropDownList)
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = "1"
        dr("name") = "Loan"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("id") = "2"
        dr("name") = "Grant"
        dt.Rows.Add(dr)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Disbursement Type", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0

    End Sub

    Public Sub Bind_DDL_CountryGroup(ByRef ddl As DropDownList)
        Dim sql As String = " select id,countrygroup_name from tb_countrygroup where isnull(active_status,'Y') = 'Y' order by countrygroup_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("countrygroup_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CountryType(ByRef ddl As DropDownList)
        Dim sql As String = "Select id,countrytypename from web_countrytype order by countrytypename"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("-- เลือก --", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("countrytypename"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_RegionZone(ByRef ddl As DropDownList)
        Dim sql As String = "select id,regionoda_name from tb_regionzoneoda order by regionoda_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Zone", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("regionoda_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_RegionZone_By_Region(ByRef ddl As DropDownList, regionoda_id As Integer)
        Dim sql As String = "select * from tb_regionzoneoda where regionoda_id=" & GL.ConvertCINT(regionoda_id) & "  order by regionoda_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country Zone", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("regionoda_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub


    Public Sub Bind_DDL_Region(ByRef ddl As DropDownList)
        Dim sql As String = "select id,region_name from tb_region  where isnull(active_status,'Y') = 'Y' order by region_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Region", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("region_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Region_forSearch(ByRef ddl As DropDownList)
        Dim sql As String = "select id,region_name from tb_region order by region_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Region", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("region_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_CoperationFrameWork(ByRef ddl As DropDownList)
        Dim sql As String = "select id,detail + '(' + fwork_name + ') ' as fwork_name from tb_coperationframework  where isnull(active_status,'Y') = 'Y' order by fwork_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Coperation Framework", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("fwork_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Coperationtype(ByRef ddl As DropDownList)
        Dim sql As String = "select id,detail + '(' + cptype_name + ')' as cptype_name from tb_coperationtype where isnull(active_status,'Y') = 'Y' order by cptype_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Coperation Type", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("cptype_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_OECD(ByRef ddl As DropDownList)
        Dim sql As String = "select id,oecd_name from tb_oecd where isnull(active_status,'Y') = 'Y' order by oecd_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select OECD", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("oecd_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DLL_Plan(ByRef ddl As DropDownList)
        Dim sql As String = "select id,plan_name from TB_Plan where isnull(active_status,'Y') = 'Y'"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Plan", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("plan_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Organize(ByRef ddl As DropDownList)
        Dim sql As String = "select node_id,name_en from TB_OU_Organize where isnull(active_status,'Y') = 'Y' order by name_th"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ChildsOrganize(ByRef ddl As DropDownList, parent_id As String)
        Dim sql As String = "select node_id,name_th from TB_OU_Organize where isnull(active_status,'Y') = 'Y' "
        If parent_id <> "" Then
            sql &= " and parent_id= @_Parent_id"
        End If
        sql &= " order by name_th"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_Parent_id", parent_id)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_OrganizeByCountry(ByRef ddl As DropDownList, country_id As String)
        'Dim sql As String = "select node_id,name_th from TB_OU_Organize where isnull(active_status,'Y') = 'Y' "
        Dim dt As DataTable = GetOrganizeUnitByNodeID(country_id, OU.Organize) 'SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Function Bind_TableFormOrganize(dt As DataTable, dtou As DataTable) As DataTable
        Dim sql As String = ""
        Dim DTOrganize As New DataTable
        Dim CheckId As String = "False"
        With DTOrganize
            .Columns.Add("node_id")
            .Columns.Add("name_th")
            .Columns.Add("name_en")
            .Columns.Add("parent_id")
        End With
        Dim dr As DataRow
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                dr = DTOrganize.NewRow
                dr("node_id") = dt.Rows(i).Item("node_id")
                dr("name_th") = dt.Rows(i).Item("name_th")
                dr("name_en") = dt.Rows(i).Item("name_en")
                dr("parent_id") = dt.Rows(i).Item("parent_id")
                DTOrganize.Rows.Add(dr)
                For j As Integer = 0 To dtou.Rows.Count - 1
                    If dt.Rows(i).Item("node_id").ToString = dtou.Rows(j).Item("parent_id").ToString Then
                        For k As Integer = 0 To dt.Rows.Count - 1
                            If dtou.Rows(j).Item("node_id").ToString = dt.Rows(k).Item("node_id").ToString Then
                                CheckId = "True"
                            End If
                        Next
                        If CheckId = "True" Then
                            CheckId = "False"
                        Else
                            dr = DTOrganize.NewRow
                            dr("node_id") = dtou.Rows(j).Item("node_id")
                            dr("name_th") = dtou.Rows(j).Item("name_th")
                            dr("name_en") = dtou.Rows(j).Item("name_en")
                            dr("parent_id") = dtou.Rows(j).Item("parent_id")
                            DTOrganize.Rows.Add(dr)
                            CheckId = "False"
                        End If

                    End If
                Next

            Next
        End If
        Return DTOrganize
    End Function

    Public Sub Bind_DDL_OrganizeFromCountry(ByRef ddl As DropDownList, country_id As String)
        Dim sql As String = ""
        Dim dtou As DataTable
        Dim dt As New DataTable
        Dim dttemp As New DataTable
        Dim DTOrganize As New DataTable
        Dim dt1 As New DataTable

        sql = "select node_id,name_th,name_en,parent_id from TB_OU_Organize where parent_id = '" & country_id.ToString & "'"
        dttemp = SqlDB.ExecuteTable(sql)
        sql = "select node_id,name_th,name_en,parent_id from TB_OU_Organize"
        dtou = SqlDB.ExecuteTable(sql)

        dt = Bind_TableFormOrganize(dttemp, dtou)
        DTOrganize = Bind_TableFormOrganize(dt, dtou)
        Dim a As Integer = DTOrganize.Rows.Count
        dt1 = DTOrganize

        'For w As Integer = 0 To dt.Rows.Count - 1
        '    For x As Integer = 0 To DTOrganize.Rows.Count - 1
        '        If dt.Rows(w).Item("node_id").ToString = DTOrganize.Rows(x).Item("node_id").ToString Then
        '            dt1.Rows.Remove(row:=dt1.Rows(x))
        '        End If
        '    Next
        'Next

        Dim dr As DataRow
        For z As Integer = 0 To dt1.Rows.Count - 1
            dr = dt.NewRow
            dr("node_id") = dt1.Rows(z).Item("node_id")
            dr("name_th") = dt1.Rows(z).Item("name_th")
            dr("name_en") = dt1.Rows(z).Item("name_en")
            dr("parent_id") = dt1.Rows(z).Item("parent_id")
            dt.Rows.Add(dr)
        Next

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Organize", ""))
        For i As Integer = 0 To DTOrganize.Rows.Count - 1
            Dim Item As New ListItem(DTOrganize.Rows(i).Item("name_th"), DTOrganize.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Person(ByRef ddl As DropDownList, person_type As String)
        Dim sql As String = "select node_id,name_th from TB_OU_Person where isnull(active_status,'Y') = 'Y' and person_type = " & person_type & " order by name_th"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Person", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_th"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Multilateral(ByRef ddl As DropDownList)
        Dim sql As String = "select id,detail + '(' + names + ')' as names from tb_multilateral where isnull(active_status,'Y') = 'Y' order by names"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Multilateral", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("names"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Component(ByRef ddl As DropDownList)
        Dim sql As String = "select id,component_name from tb_component where isnull(active_status,'Y') = 'Y' order by component_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Component", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("component_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)
        Dim sql As String = "select node_id,name_en from TB_OU_Country where isnull(active_status,'Y') = 'Y' order by name_en"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub


    Public Sub Bind_DDL_CoFunding(ByRef ddl As DropDownList)
        Dim sql As String = "select node_id,name_en,'c' datatype from TB_OU_Country where isnull(active_status,'Y') = 'Y' "
        sql &= " union "
        sql &= " Select  node_id,isnull(abbr_name_en + '-'  ,'')+ name_en as name_en,'o' datatype  "
        sql &= " From TB_OU_Organize where isnull(active_status,'Y') = 'Y' "
        sql &= " order by datatype ,name_en"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Co-Funding", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name_en"), dt.Rows(i).Item("node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Project(ByRef ddl As DropDownList)
        Dim sql As String = "select id,project_name from TB_Project order by project_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Project", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("project_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Perposecat(ByRef ddl As DropDownList)
        Dim sql As String = "select id,perposecat_name from tb_purposecat where isnull(active_status,'Y') = 'Y' order by perposecat_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sector", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("perposecat_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Inkind(ByRef ddl As DropDownList)
        Dim sql As String = "select id,inkind_name from tb_inkind where isnull(active_status,'Y') = 'Y' order by inkind_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Inkind", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("inkind_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Prefix_th(ByRef ddl As DropDownList)
        Dim sql As String = "select id,prefix_name_th from tb_prefix where isnull(active_status,'Y')='Y' order by prefix_name_th"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Prefix", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("prefix_name_th"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub


    Public Sub Bind_DDL_Perpose(ByRef ddl As DropDownList, sectorid As String)
        Dim sql As String = "select id,name from tb_purpose where isnull(active_status,'Y') = 'Y'"
        If sectorid <> "" Then
            sql &= " and CategoryID=@_SECTOR_ID"
        End If
        sql &= " order by name"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_SECTOR_ID", sectorid)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sub Sector", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_SubBudget(ByRef ddl As DropDownList, budget_group_id As String)
        Dim sql As String = " select id,sub_name from TB_Budget_Sub where isnull(active_status,'Y')='Y' and budget_group_id= @_BUDGET_GROUP_ID order by sub_name"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_BUDGET_GROUP_ID", budget_group_id)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, p)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sub Budget", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("sub_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_BudgetGroup(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_BudgetGroupForSearch(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group order by group_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_Year(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim _year As Integer = Now.Date.Year
        If _year < 2500 Then _year += 543

        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("All", ""))
        End If
        For i As Integer = 0 To 19
            Dim Item As New ListItem(_year - i, _year - i)
            ddl.Items.Add(Item)
        Next


    End Sub

    Public Sub Bind_DDL_ExpenseGroup(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Expense_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Expense Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ExpenseSub(ByRef ddl As DropDownList)
        Dim sql As String = " select id,sub_name from TB_Expense_Sub where isnull(active_status,'Y')='Y' order by sub_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Expense", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("sub_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ExpenseTemplate(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = " select id,template_name from TB_Template"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Template", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("template_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_ProjectType(ByRef ddl As DropDownList)
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("ProjectType")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Project)
        dr("ProjectType") = "Project"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.NonProject)
        dr("ProjectType") = "Non Project"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Loan)
        dr("ProjectType") = "Loan"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("ID") = CInt(Project_Type.Contribuition)
        dr("ProjectType") = "Contribuition"
        dt.Rows.Add(dr)

        'ddl.Items.Clear()
        'ddl.Items.Add(New ListItem("Select Project Type", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("ProjectType"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = " select DISTINCT country_node_id,country_name_th  from vw_Project_Component "
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_Expense_Activity_GroupBy_Country(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""
        sql &= " Select DISTINCT node_id_OU country_node_id, name_th_OU country_name_th " + Environment.NewLine
        sql &= " From vw_Expense_Activity_GroupBy_Country " + Environment.NewLine

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country_By_rptSummary_ByCountry(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""

        sql += " 	SELECT DISTINCT country_node_id,country_name_th FROM (																								  " + Environment.NewLine
        sql += " 	 SELECT TB_Detail_By_Project.budget_year,country_node_id,country_name_th									  " + Environment.NewLine
        sql += " 		   ,COUNT(project_id) QTY_Project						--จำนวนโปรเจคทั้ง ภายใต้และนอกโครงการ				     " + Environment.NewLine
        sql += " 		   ,vw_Recipient_In_Year.amout_person QTY_Recipient		--จำนวนทุน									     " + Environment.NewLine
        sql += " 		   ,SUM(Total_Amount) SUM_Amount						--มูลค่า											  " + Environment.NewLine
        sql += " 		FROM (																									  " + Environment.NewLine
        sql += " 			SELECT budget_year																					  " + Environment.NewLine
        sql += " 				  ,vw_Summary_Aid_By_Country.project_id															  " + Environment.NewLine
        sql += " 				  ,vw_Summary_Aid_By_Country.project_name														  " + Environment.NewLine
        sql += " 				  ,tb_project.project_type																		  " + Environment.NewLine
        sql += " 				  ,country_node_id																				  " + Environment.NewLine
        sql += " 				  ,country_name_th																				  " + Environment.NewLine
        sql += " 				  ,commitment_budget																			  " + Environment.NewLine
        sql += " 				  ,convert(decimal ,Bachelor)+convert(decimal,Training)+convert(decimal,Expert)+convert(decimal,Volunteer)+convert(decimal,Equipment)+convert(decimal,Other) Total_Amount	  " + Environment.NewLine
        sql += "																														  " + Environment.NewLine
        sql += " 			  FROM  vw_Summary_Aid_By_Country																			  " + Environment.NewLine
        sql += " 			  left JOIN tb_project ON tb_project.id=vw_Summary_Aid_By_Country.project_id								  " + Environment.NewLine
        sql += " 	 ) TB_Detail_By_Project																								  " + Environment.NewLine
        sql += " 	 LEFT JOIN vw_Recipient_In_Year ON  vw_Recipient_In_Year.budget_year = TB_Detail_By_Project.budget_year				  " + Environment.NewLine
        sql += " 										AND vw_Recipient_In_Year.node_id_OU = TB_Detail_By_Project.country_node_id		  " + Environment.NewLine
        sql += " 																														  " + Environment.NewLine
        sql += "  																														  " + Environment.NewLine
        sql += " 	 GROUP BY  TB_Detail_By_Project.budget_year,country_node_id,country_name_th ,vw_Recipient_In_Year.amout_person		    " + Environment.NewLine
        sql += " 	 ) AS TB																											    " + Environment.NewLine
        sql += " 	 WHERE SUM_Amount>0																									    " + Environment.NewLine
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub


    '--Componance
    Public Sub Bind_DDL_Componance(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        Dim sql As String = ""
        sql &= " SELECT * FROM vw_Component Order by Desc_name  " + Environment.NewLine

        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("Desc_name"), dt.Rows(i).Item("component_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub


    Public Sub Bind_DDL_Year_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะปีที่อยู่ใน List
        Dim sql As String = " select distinct(budget_year) FROM vw_Summary_Aid "
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Year", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("budget_year"), dt.Rows(i).Item("budget_year"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub


    Public Sub Bind_DDL_Country_name_th_By_rptCountryReceiveAidEveryYear(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะประเทศที่อยู่ใน List
        Dim sql As String = " Select distinct country_name_th ,AP.country_node_id from vw_Activity_Disbursement AD inner join vw_Activity_PSN AP On AD.activity_id = AP.activity_id  order by AP.Country_name_th DESC"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Country_name_th_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะประเทศที่อยู่ใน List
        Dim sql As String = "select distinct(country_node_id),country_name_th FROM vw_Summary_Aid "
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Country", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("country_name_th"), dt.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub

    Public Sub Bind_DDL_Sector_th_By_rptAidByCountrySummary(ByRef ddl As DropDownList, Optional IsAddFirstIndex As Boolean = False)
        '--Distinct เฉพาะสาขาที่อยู่ใน List
        Dim sql As String = "select distinct(sector_id),perposecat_name FROM vw_Summary_Aid  where sector_id IS NOT NULL	"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        If IsAddFirstIndex = True Then
            ddl.Items.Add(New ListItem("Select Sector", ""))
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("perposecat_name"), dt.Rows(i).Item("sector_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0


    End Sub


    Public Sub Bind_DDL_Year_Reprot_AidSummary(ByRef ddl As DropDownList)
        Dim sql As String = " select id,group_name from TB_Budget_Group where isnull(active_status,'Y')='Y' order by group_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Budget Group", ""))
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim Item As New ListItem(dt.Rows(i).Item("group_name"), dt.Rows(i).Item("id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub



#End Region

    '-------------------------------


    '===========GetAllActivityByProject  สำหรับ Project  /  non Project

    Public Function GetAllActivityByProject(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " With activity (parent_id,id,activity_name, Level,plan_start, plan_end,duration) As" + Environment.NewLine
            sql += " (" + Environment.NewLine
            sql += " Select parent_id,id,isnull(activity_name,[description]) activity_name, 0 As Level, plan_start, plan_end,datediff(day, plan_start, plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity a Where PROJECT_ID =  @_PROJECT_ID And parent_id=0" + Environment.NewLine
            sql += " UNION ALL" + Environment.NewLine
            sql += " Select a.parent_id,a.id,isnull(a.activity_name,[description]) activity_name, Level+1 As Level " + Environment.NewLine
            sql += " , a.plan_start, a.plan_end,datediff(day, a.plan_start, a.plan_end) duration" + Environment.NewLine
            sql += " From TB_Activity As a" + Environment.NewLine
            sql += " INNER JOIN activity As at" + Environment.NewLine
            sql += " On a.Parent_ID=at.ID " + Environment.NewLine
            sql += " )" + Environment.NewLine
            sql += " Select  DISTINCT ROW_NUMBER() OVER(ORDER BY id ASC) As Seq, parent_id,id,right(space(60) + activity_name,len(activity_name) + (level*3)) activity_name, Level,plan_start, plan_end,duration," + Environment.NewLine
            sql += " isnull((Select sum(amount) from tb_activity_budget where activity_id= activity.id),0) commitment_budget," + Environment.NewLine
            sql += " (Select [dbo].GetFullThaiDate(plan_start)) strStart_date,(Select [dbo].GetFullThaiDate(plan_end)) strEnd_date" + Environment.NewLine
            sql += " From activity" + Environment.NewLine
            sql += " ORDER BY ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)
        Catch ex As Exception
        End Try
        Return dt
    End Function


    Public Function GetProjectCoFunding(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            'sql = " Select p.id,project_id,country_id,amount,c.name_th "
            'sql &= " From TB_Project_CoFunding p inner join tb_ou_country c on p.country_id=c.node_id"
            'sql &= " where project_id = @_PROJECT_ID"

            sql = " Select  p.id,project_id,country_id,amount,c.name_en,c.name_th  "
            sql &= " From TB_Project_CoFunding p "
            sql &= " inner Join "
            sql &= " (select node_id,name_en,'c' datatype,name_th from TB_OU_Country where isnull(active_status,'Y') = 'Y' "
            sql &= " union"
            sql &= " Select  node_id,isnull(abbr_name_en + '-'  ,'')+ name_en as name_en,'o' datatype,name_th  "
            sql &= " From TB_OU_Organize Where isnull(active_status,'Y') = 'Y' ) c on p.country_id=c.node_id "
            sql &= "  where project_id = @_PROJECT_ID"


            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)

        Catch ex As Exception
        End Try

        Return dt
    End Function

    Public Function GetProjectFile(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " Select id, id file_id,project_id,original_file_name,content_type,description,file_path from TB_Project_File where project_id = @_PROJECT_ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)

        Catch ex As Exception
        End Try

        Return dt
    End Function

    Public Function GetProjectImplementingAgency(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = "  Select pia.id,project_id,pia.country_id,pia.organize_id , isnull(cou.name_th, cou.name_en) country_name, oou.name_en organize_name "
            sql += " from TB_Project_Implementing_Agency pia "
            sql += " inner join vw_ou cou On cou.node_id=pia.country_id"
            sql += " inner join vw_ou oou On oou.node_id=pia.organize_id"
            sql += " where pia.project_id = @_PROJECT_ID"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)

        Catch ex As Exception
        End Try

        Return dt
    End Function

    Public Function GetProjectInfoByID(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            'sql = " Select ROW_NUMBER() OVER(ORDER BY p.id ASC) As Seq,p.id,p.project_id,p.project_name,p.project_type,p.objective,p.description,p.start_date" + Environment.NewLine
            'sql += " ,p.end_date,p.cooperation_framework_id,p.cooperation_type_id,p.region_oecd_id" + Environment.NewLine
            'sql += " ,p.oecd_id,p.location,p.funding_agency_id,p.executing_agency_id,p.implementing_agency_id" + Environment.NewLine
            'sql += " ,p.assistant ,p.remark,p.transfer_project_to,p.multilateral_id,p.allocate_budget,c.address,c.contact_name" + Environment.NewLine
            'sql += " ,c.telephone,c.fax,c.website,c.description,c.email,c.position" + Environment.NewLine
            'sql += " ,DATEPART(m,p.start_date) month_start,DATEPART(m,p.end_date) month_end,YEAR(p.start_date) year_start, YEAR(p.end_date) year_end,DATEPART(m,getdate()) month_now,YEAR(getdate()) year_now, getdate() now_date " + Environment.NewLine
            'sql += " ,oecd_aid_type_id,location,grance_period,maturity_period,interest_rate,implementing_agency_loan,implementing_agency_contribution" + Environment.NewLine
            'sql += " From tb_project p " + Environment.NewLine
            'sql += " Left join tb_ou_contact  c On p.id = c.parent_id And parent_type = " & Contact_Parent_Type.Project & "" + Environment.NewLine
            'sql += " Where p.id = @_PROJECT_ID"



            sql = " Select ROW_NUMBER() OVER(ORDER BY p.id ASC) As Seq,p.id,p.project_id,p.project_name,p.project_type,p.objective,p.description,p.start_date" + Environment.NewLine
            sql += " ,p.end_date,(Select [dbo].[GetFullThaiDate](p.start_date)) start_date_th,(Select [dbo].[GetFullThaiDate](p.end_date)) end_date_th,p.cooperation_framework_id,p.cooperation_type_id,p.region_oecd_id" + Environment.NewLine
            sql += " ,p.oecd_id,p.location,p.funding_agency_id,p.executing_agency_id,p.implementing_agency_id" + Environment.NewLine
            sql += " ,p.assistant ,p.remark,p.transfer_project_to,p.multilateral_id,p.allocate_budget,c.address,c.contact_name" + Environment.NewLine
            sql += " ,c.telephone,c.fax,c.website,c.description,c.email,c.position" + Environment.NewLine
            sql += " ,DATEPART(m,p.start_date) month_start,DATEPART(m,p.end_date) month_end," + Environment.NewLine
            sql += " Year(p.start_date) year_start, YEAR(p.end_date) year_end," + Environment.NewLine
            sql += " DatePart(m, getdate()) month_now,YEAR(getdate()) year_now, getdate() now_date" + Environment.NewLine
            sql += " ,oecd_aid_type_id,location,grance_period,maturity_period,interest_rate,implementing_agency_loan,implementing_agency_contribution" + Environment.NewLine
            sql += " ,f.fwork_name cooperation_framework_name,t.cptype_name cooperation_type_name,r.regionoda_name region_oecd_name,o.oecd_name oecd_name,fo.name_th funding_agency_name" + Environment.NewLine
            sql += " ,eo.name_th executing_agency_name,ipo.name_th implementing_agency_name,po.name_th" + Environment.NewLine
            sql += " ,m.names multilateral_name,oa.oecd_name oecd_aid_type_name,aco.name_th implementing_agency_contribution_name" + Environment.NewLine
            sql += " ,(select [dbo].GetFullThaiDate(p.start_date)) strStart_date,(select [dbo].GetFullThaiDate(p.end_date)) strEnd_date" + Environment.NewLine
            sql += " From tb_project p " + Environment.NewLine
            sql += " Left Join tb_ou_contact  c On p.id = c.parent_id And parent_type = " & Contact_Parent_Type.Project & "" + Environment.NewLine
            sql += "  Left Join TB_CoperationFramework f on p.cooperation_framework_id = f.id" + Environment.NewLine
            sql += " Left Join TB_CoperationType t On p.cooperation_type_id=t.id" + Environment.NewLine
            sql += " Left Join TB_Regionzoneoda r on p.region_oecd_id = r.id" + Environment.NewLine
            sql += " Left Join TB_oecd o On p.oecd_id = o.id" + Environment.NewLine
            sql += " Left Join TB_OU_Organize fo on p.funding_agency_id = fo.node_id" + Environment.NewLine
            sql += " Left Join TB_OU_Organize eo On p.executing_agency_id = eo.node_id" + Environment.NewLine
            sql += " Left Join TB_OU_Organize ipo on p.implementing_agency_id = ipo.node_id" + Environment.NewLine
            sql += " Left Join TB_OU_Person po On p.transfer_project_to=po.node_id" + Environment.NewLine
            sql += " Left Join TB_multilateral m on p.multilateral_id = m.id" + Environment.NewLine
            sql += " Left Join TB_oecd oa On p.oecd_id = oa.id" + Environment.NewLine
            sql += " Left Join TB_OU_Organize aco on p.implementing_agency_id = aco.node_id" + Environment.NewLine
            sql += " Where p.id =  @_PROJECT_ID"


            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)

        Catch ex As Exception
        End Try

        Return dt
    End Function

    Public Function GetProjectRecipincePerson(project_id As String) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            sql = " Select ROW_NUMBER() OVER(ORDER BY r.id ASC) As Seq,r.id,r.activity_id,r.recipient_id,isnull(ft.prefix_name_th,'') + ' ' + p.name_th  name_th," + Environment.NewLine
            sql += " isnull(fe.prefix_name_en,'') + ' ' + p.name_en name_en,'' student_id,(select [dbo].[GetParentByChildNode](r.recipient_id,'TB_OU_Country')) country_name," + Environment.NewLine
            sql += " a.activity_name course, a.plan_start, a.plan_end, a.actual_start, a.actual_end, (select [dbo].[GetParentByChildNode](r.recipient_id,'TB_OU_Organize')) agency," + Environment.NewLine
            sql += " '' emp_current_positions,case -1 when  0 then 'กำลังศึกษา' when 1 then 'จบการศึกษา' when 2 then 'ไม่จบการศึกษา' end [status]" + Environment.NewLine
            'sql += " ,convert(varchar(10),a.plan_start,103)+'-'+ convert(varchar(10),a.plan_end,103) period_plan_date" + Environment.NewLine
            sql += " ,(Select [dbo].[GetFullThaiDate](a.plan_start)) +'-'+ (Select [dbo].[GetFullThaiDate](a.plan_end)) period_plan_date " + Environment.NewLine

            sql += " From TB_Activity_Recipience r" + Environment.NewLine
            sql += " inner join tb_activity a on r.activity_id=a.id" + Environment.NewLine
            sql += " Left Join tb_ou_person p on r.recipient_id = p.node_id " + Environment.NewLine
            sql += " Left join tb_recipience n on p.id = n.person_id" + Environment.NewLine
            sql += " Left Join tb_prefix ft on p.prefix_th_id = ft.id" + Environment.NewLine
            sql += " Left join tb_prefix fe on p.prefix_en_id = fe.id where project_id = @_PROJECT_ID And r.recipient_id Is Not null"

            Dim p(1) As SqlParameter
            p(0) = SqlDB.SetText("@_PROJECT_ID", project_id)
            dt = SqlDB.ExecuteTable(sql, p)

        Catch ex As Exception
        End Try

        Return dt
    End Function



#Region "_OtherFunction"
    Public Function GetTHDateAbbr(pDate As String) As String
        Dim strDate As String ' yyyyMMdd
        Dim strMonth As String = ""
        Dim NumMonth As String = pDate.Substring(4, 2)
        Dim intDay As Integer = 1

        Try
            intDay = Convert.ToInt16(pDate.Substring(6, 2))
        Catch ex As Exception
        End Try

        If NumMonth = "01" Then
            strMonth = "ม.ค."
        ElseIf NumMonth = "02" Then
            strMonth = "ก.พ."
        ElseIf NumMonth = "03" Then
            strMonth = "มี.ค."
        ElseIf NumMonth = "04" Then
            strMonth = "เม.ย."
        ElseIf NumMonth = "05" Then
            strMonth = "พ.ค."
        ElseIf NumMonth = "06" Then
            strMonth = "มิ.ย."
        ElseIf NumMonth = "07" Then
            strMonth = "ก.ค."
        ElseIf NumMonth = "08" Then
            strMonth = "ส.ค."
        ElseIf NumMonth = "09" Then
            strMonth = "ก.ย."
        ElseIf NumMonth = "10" Then
            strMonth = "ต.ค."
        ElseIf NumMonth = "11" Then
            strMonth = "พ.ย."
        ElseIf NumMonth = "12" Then
            strMonth = "ธ.ค."
        End If

        Dim Year As Long = 0
        Try
            Year = Convert.ToInt64(pDate.Substring(0, 4)) + 543
        Catch ex As Exception
        End Try

        strDate = intDay.ToString() + " " + strMonth + " " + Year.ToString()

        Return strDate
    End Function

    Function GetAssistantByID(ass_id As String) As DataTable
        Dim sql As String = "select P.id,node_id,name_th from TB_OU_Person P"
        sql &= " inner join (select * from dbo.Split(@_ASSID,',')) T on P.ID = T.Data"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetText("@_ASSID", ass_id)
        Dim dt As New DataTable
        dt = SqlDB.ExecuteTable(sql, p)
        Return dt
    End Function

    Function CalPlanDuration(startdate As String, enddate As String) As String
        If startdate = "" Or enddate = "" Then
            Return ""
        End If

        Dim d As Date = Converter.StringToDate(startdate, "dd/MM/yyyy")
        Dim ts As TimeSpan = Converter.StringToDate(enddate, "dd/MM/yyyy") - d
        Dim y As Double
        Dim m As Double
        Dim ds As Double = ts.TotalDays

        y = Math.Floor(ds / 365)
        ds -= y * 365
        m = Math.Floor(ds / 31)
        ds -= m * 31

        Dim yy As String = CStr(y)
        Dim mm As String = CStr(m)
        Dim dd As String = CStr(ds)

        Dim ret As String = ""
        If yy <> "0" Then ret &= yy & " ปี"
        If mm <> "0" Then ret &= mm & " เดือน"
        If dd <> "0" Then ret &= dd & " วัน"

        Return ret

        'Example: Dim strduration As String = BL.CalPlanDuration("06/01/2017", "08/02/2019")
    End Function

    Function GetPeriodMonth(y As String, m As String) As Integer

        Dim ym As Integer = 0
        If y <> "" Then
            ym = CInt(y) * 12
        End If

        Dim allm As Integer = ym
        If m <> "" Then
            allm += CInt(m)
        End If

        Return allm
    End Function

    Function GetConvertPeriodMonth(m As Integer) As ArrayList
        Dim result As String() = (m / 12).ToString.Split(".")
        Dim y As Integer = result(0)
        Dim totalm As Integer = m Mod 12

        Dim arr As New ArrayList
        arr.Add(y.ToString)
        arr.Add(totalm.ToString)

        Return arr
    End Function

#End Region


#Region "_OU"

    Public Function GetOrganizeUnitByNodeID(node_id As String, node_type As Integer) As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try

            Select Case (node_type)
                Case OU.Country
                    sql = " Select c.node_id,c.name_th,c.name_en,c.description,c.active_status ," & OU.Country & " node_type," + Environment.NewLine
                    sql += " (Select  count(o.node_id)  from tb_ou_organize o where o.parent_id=c.node_id) " + Environment.NewLine
                    sql += " + (Select  count(p.node_id)  from tb_ou_person p where p.parent_id=c.node_id) childs, '0' parent_id" + Environment.NewLine
                    sql += " From tb_ou_country c" + Environment.NewLine
                    sql += " where isnull(c.active_status,'Y') ='Y'" + Environment.NewLine
                    sql += " order by c.name_th,c.name_en" + Environment.NewLine
                    dt = SqlDB.ExecuteTable(sql)
                Case OU.Organize
                    sql = " select o.node_id,o.name_th,o.name_en ," & OU.Country & " node_type," + Environment.NewLine
                    sql += " (select  count(p.node_id)  from tb_ou_person p where p.parent_id=o.node_id) + " + Environment.NewLine
                    sql += " (select count(n.node_id) from tb_ou_organize n where n.parent_id = o.node_id) childs, o.parent_id" + Environment.NewLine
                    sql += " From tb_ou_organize o" + Environment.NewLine
                    'sql += " where isnull(o.active_status,'Y') ='Y'" + Environment.NewLine
                    sql += " where o.parent_id = @_NODE_ID" + Environment.NewLine
                    sql += " order by o.name_th,o.name_en" + Environment.NewLine
                    Dim p(1) As SqlParameter
                    p(0) = SqlDB.SetText("@_NODE_ID", node_id)
                    dt = SqlDB.ExecuteTable(sql, p)
                Case OU.Person
                    sql = "select p.node_id,p.name_th,p.name_en," & OU.Country & " node_type,0 childs, p.parent_id from tb_ou_person p where parent_id=@_NODE_ID"
                    'sql += " where isnull(p.active_status,'Y') ='Y'" + Environment.NewLine
                    sql += " order by p.name_th,p.name_en" + Environment.NewLine
                    Dim p(1) As SqlParameter
                    p(0) = SqlDB.SetText("@_NODE_ID", node_id)
                    dt = SqlDB.ExecuteTable(sql, p)
            End Select
        Catch ex As Exception
        End Try


        Return dt
    End Function

#End Region

End Class
