﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmFrontRecipient.aspx.vb" Inherits="frmFrontRecipient" %>


<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>--%>
<%@ Register Src="~/usercontrol/UC_PERSONALHISTORY.ascx" TagPrefix="uc1" TagName="UC_PERSONALHISTORY" %>
<%@ Register Src="~/usercontrol/UC_EDUCATIONRECORD.ascx" TagPrefix="uc1" TagName="UC_EDUCATIONRECORD" %>
<%@ Register Src="~/usercontrol/UC_EMPLOYMENTRECORD.ascx" TagPrefix="uc1" TagName="UC_EMPLOYMENTRECORD" %>
<%@ Register Src="~/usercontrol/UC_EXPECTATIONS.ascx" TagPrefix="uc1" TagName="UC_EXPECTATIONS" %>
<%@ Register Src="~/usercontrol/UC_GOVERNMENTAUTHORISATION.ascx" TagPrefix="uc1" TagName="UC_GOVERNMENTAUTHORISATION" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Metronic Frotnend | Features - Forms</title>
    <style>
        div.bottom {
            /*height: 200px;
            width: 80%;
            margin-left: auto;
            margin-right: auto;
            margin-top: none;
            margin-bottom: none;
            border-top: 4px solid #00ccff;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
            background-color: #575757;*/
        }

        div.coltext {
            float: left;
            width: 15%;
        }

        div.col {
            float: left;
            width: 40%;
        }

        .NumberActive {
            background-color: #279434;
            color: white;
            display: inline-block;
            font-size: 16px;
            font-weight: 300;
            padding: 10px 15px 10px 15px !important;
            margin-right: 10px;
            -webkit-border-radius: 50% !important;
            -moz-border-radius: 50% !important;
            border-radius: 50% !important;
        }

        .NumberNoneActive {
            background-color: #9aa09b;
            color: white;
            display: inline-block;
            font-size: 16px;
            font-weight: 300;
            padding: 10px 15px 10px 15px !important;
            margin-right: 10px;
            -webkit-border-radius: 50% !important;
            -moz-border-radius: 50% !important;
            border-radius: 50% !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server">
        <ContentTemplate>

            <!-- BEGIN BREADCRUMBS -->
            <div class="row-fluid breadcrumbs margin-bottom-40">
                <div class="container">
                </div>
            </div>
            <!-- END BREADCRUMBS -->

            <!-- BEGIN CONTAINER -->
            <div class="container min-hight">

                <!-- ROW 1 -->

                <div class="span12" style="margin-top: -20px;">
                    <asp:Panel ID="pnlTimeline" runat="server">
                        <div class="">
                            <ul class="row-fluid nav nav-pills">
                                <li class="span2">
                                    <span id="Step_1" runat="server" class="NumberActive"><b>A</b></span>
                                    <span class="desc">Personal History</span>

                                </li>
                                <li class="span2">
                                    <span id="Step_2" runat="server" class="NumberNoneActive"><b>B</b></span>
                                    <span class="desc">Education</span>

                                </li>
                                <li class="span2">
                                    <span id="Step_3" runat="server" class="NumberNoneActive"><b>C</b></span>
                                    <span class="desc">Employment</span>

                                </li>
                                <li class="span2">
                                    <span id="Step_4" runat="server" class="NumberNoneActive"><b>D</b></span>
                                    <span class="desc">Expectations</span>

                                </li>
                                <li class="span4">
                                    <span id="Step_5" runat="server" class="NumberNoneActive"><b>E</b></span>
                                    <span class="desc">Goverment Authorisation</span>

                                </li>


                            </ul>
                        </div>
                    </asp:Panel>


                </div>
                <div class="row-fluid margin-bottom-40">





                    <!-- COLUMN 1 -->
                    <div class="span3" style="margin-top: -20px;">
                        <div class="row-fluid recent-work margin-bottom-40">
                            <center>
                           <asp:Label id="lblActivity_id" runat="Server" visible="false"></asp:Label>
                        <div class="row" style="padding-left:20px">
                            <img id="ProfileImg" runat="server" src="" style="height: 250px; width: 250px; cursor: pointer;" />
                        </div>
                        <div class="row" id="divProfileImage" runat="server" >
                            <a href="#" onclick="UploadInsertProfileImage();return false;"  id="btnInsertProfileImage" runat="server">
                                <i class="icon-plus"></i>
                                <span >Add Profile Image</span>
                            </a>
                            <asp:FileUpload ID="fulInsertProfileImage" ClientIDMode="Static" runat="server" Style="display: none;" onchange="asyncProfileImageInsert();" />
                            <asp:Button ID="btnUpdateProfileImage" ClientIDMode="Static" runat="server" Style="display: none;" />
                        </div>
                       </center>


                        </div>
                        <!-- BLOCK BUTTONS -->
                        <div class="row-fluid" id="div_Step_btn" runat="server" visible="true">
                            <p>
                                <%-- <a id="aPer" runat="server" href="#Per" data-toggle="tab" class="btn blue btn-block">A. PERSONAL HISTORY</a>
						    <a id="aEdu" runat="server" href="#Edu" data-toggle="tab" class="btn blue btn-block">B. EDUCATION RECORD</a>
						    <a id="aEmp" runat="server" href="#Emp" data-toggle="tab" class="btn blue btn-block">C. EMPLOYMENT RECORD</a>
						    <a id="aExp" runat="server" href="#Exp" data-toggle="tab" class="btn blue btn-block">D. EXPECTATIONS</a>
						    <a id="aGov" runat="server" href="#Gov" data-toggle="tab" class="btn blue btn-block">E. GOVERNMENT AUTHORISATION</a>--%>

                                <a id="aPer" runat="server" class="btn blue btn-block">A. PERSONAL HISTORY</a>
                                <a id="aEdu" runat="server" class="btn blue btn-block">B. EDUCATION RECORD</a>
                                <a id="aEmp" runat="server" class="btn blue btn-block">C. EMPLOYMENT RECORD</a>
                                <a id="aExp" runat="server" class="btn blue btn-block">D. EXPECTATIONS</a>
                                <a id="aGov" runat="server" class="btn blue btn-block">E. GOVERNMENT AUTHORISATION</a>
                            </p>
                        </div>

                        <div class="row-fluid" id="div_Intro" runat="server" visible="true">
                            <span>This application form is composed for five parts (part A to part E) and sholud be complete in triplicate. Part A to part D sholud be completed by the candidate and part E by the government authority in typewritten form. Each queastion must be answered clearly and completely. Detailed answer are required in order to make the most appropriate arragements. Official authority of the nominating Gorvernment will then forward three copies of the certified application forms to the Ministry of Foreign Affairs, The Government Complex, Building B, Chaengwattana Road, Bangkok 10210, Thailand, through the Royal Thai Embassy in the nominating country. The nominee is required to attach medical report or health status certification.

                            </span>

                        </div>



                        <!-- END BLOCK BUTTONS -->
                    </div>
                    <div class="span9">
                        <%-- <div class="form-wizard">
                           										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid">
													<li class="span3">
														<a href="#tab1" data-toggle="tab" class="step ">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Account Setup</span>   
														</a>
													</li>
													<li class="span3">
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Profile Setup</span>   
														</a>
													</li>
													<li class="span3">
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Billing Setup</span>   
														</a>
													</li>
													<li class="span3">
														<a href="#tab4" data-toggle="tab" class="step">
														<span class="number">4</span>
														<span class="desc"><i class="icon-ok"></i> Confirm</span>   
														</a> 
													</li>
												</ul>
											</div>
										</div>
										<div id="bar" class="progress progress-success progress-striped">
											<div class="bar"></div>
										</div>
                               </div>--%>
                        <div class="tab-content">




                            <asp:Panel ID="Per" runat="server">
                                <div class="tab-pane row-fluid fade in active">
                                    <!-- BEGIN FORM-->
                                    <uc1:UC_PERSONALHISTORY runat="server" ID="UC_PERSONALHISTORY" />
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="Edu" runat="server">
                                <uc1:UC_EDUCATIONRECORD runat="server" ID="UC_EDUCATIONRECORD" />
                            </asp:Panel>

                            <asp:Panel ID="Emp" runat="server">
                                <uc1:UC_EMPLOYMENTRECORD runat="server" ID="UC_EMPLOYMENTRECORD" />
                            </asp:Panel>

                            <asp:Panel ID="Exp" runat="server">
                                <uc1:UC_EXPECTATIONS runat="server" ID="UC_EXPECTATIONS" />
                            </asp:Panel>

                            <asp:Panel ID="Gov" runat="server">
                                <uc1:UC_GOVERNMENTAUTHORISATION runat="server" ID="UC_GOVERNMENTAUTHORISATION" />
                            </asp:Panel>

<%--                            <asp:Panel ID="pnlCaptcha" runat="server">

                                <div>
                                    <cc1:CaptchaControl ID="cptCaptcha" runat="server"
                                        CaptchaBackgroundNoise="Low" CaptchaLength="5"
                                        CaptchaHeight="60" CaptchaWidth="200"
                                        CaptchaLineNoise="None" CaptchaMinTimeout="5"
                                        CaptchaMaxTimeout="240" FontColor="#529E00" />

                                </div>
                                <asp:TextBox ID="txtCaptcha" runat="server"></asp:TextBox>
                                <dx:ASPxCaptcha ID="ASPxCaptcha1" runat="server">
                                </dx:ASPxCaptcha>
                                <br />
                                <asp:Button ID="btnVerify" runat="server" Text="Verify Image"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required" ControlToValidate="txtCaptcha"></asp:RequiredFieldValidator>
                                <br />
                                <br />
                                <asp:Label ID="lblErrorMessage" runat="server" Font-Names="Arial" Text=""></asp:Label>


                            </asp:Panel>--%>

                        </div>
                    </div>
                </div>
                <!-- END ROW 1 -->

            </div>
            <!-- END CONTAINER -->
            <div class="modal-footer">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success" OnClientClick="if ( ! SaveConfirmation()) return false;" OnClick="btnSave_Click">
               <i class="fa fa-save"></i> Register
                </asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google" OnClick="btnCancel_Click">
                <i class="fa fa-reply"></i> Cancel
                </asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function SaveConfirmation() {
            return confirm("Are you sure you want to save data?");
        }

        function UploadInsertProfileImage() {
            $('#fulInsertProfileImage').click();
        }

        function asyncProfileImageInsert() {
            var ful = $("#fulInsertProfileImage");
            var filename = ful.val();

            if (filename.trim() == "")
                return;

            var filecontent = ful.prop("files")[0];
            if (filecontent.size > (1 * 1024 * 1024)) {
                //alert("ไฟล์ที่อัพโหลดต้องไม่เกิน 1 Mb");
                alert("File size must not larger than 12MB");
                return false;
            }

            if (!inFileNameValid(filename)) return;
            //################ Validate Image Dimension Width ajax #################
            var img = new Image();
            img.src = window.URL.createObjectURL(filecontent);

            img.onload = function () {
                window.URL.revokeObjectURL(img.src);
                if ((parseInt(img.width) > 1000) || (parseInt(img.height) > 1000)) {
                    alert("File dimention must not larger than 1000px x 1000px");
                    return false;
                } else {
                    var formData = new FormData();
                    formData.append("filecontent", filecontent);
                    sendPostDataToURLUpdateProfileImage("Image_Upload.aspx", formData);
                }
            };
        }

        function sendPostDataToURLUpdateProfileImage(url, formData) {
            $.ajax({
                url: url,
                type: 'POST',
                success: function () { $("#btnUpdateProfileImage").click(); },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function inFileNameValid(filename) {
            var lastDot = filename.lastIndexOf(".");
            var lastSlash = filename.lastIndexOf("\\");

            //Firefox=Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0
            //ถ้า navigator.userAgent เป็น Firefox ให้กำหนดค่า lastSlash = 1 เลย
            var browserDetail = navigator.userAgent;
            if (browserDetail.indexOf("Firefox") != -1) {
                lastSlash = 1;
            }

            if (lastDot == -1 || lastSlash == -1) {
                //alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg หรือ gif เท่านั้น');
                alert("Support only image jpeg gif png 1");
                return false;
            }

            var ext = filename.substring(lastDot + 1, filename.length).toLowerCase();
            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'gif': return true;
                default:
                    //alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg หรือ gif เท่านั้น');
                    alert("Support only image jpeg gif png 2");
                    return false;
            }
        }

        function UploadInsertSignatureImage() {
            $('#fulInsertSignatureImage').click();
        }

        function asyncSignatureImageInsert() {
            var ful = $("#fulInsertSignatureImage");
            var filename = ful.val();

            if (filename.trim() == "")
                return;

            var filecontent = ful.prop("files")[0];
            if (filecontent.size > (1 * 1024 * 1024)) {
                //alert("ไฟล์ที่อัพโหลดต้องไม่เกิน 1 Mb");
                alert("File size must not larger than 12MB");
                return false;
            }

            if (!inFileNameValid(filename)) return;
            //################ Validate Image Dimension Width ajax #################
            var img = new Image();
            img.src = window.URL.createObjectURL(filecontent);

            img.onload = function () {
                window.URL.revokeObjectURL(img.src);
                if ((parseInt(img.width) > 1000) || (parseInt(img.height) > 1000)) {
                    alert("File dimention must not larger than 1000px x 1000px");
                    return false;
                } else {
                    var formData = new FormData();
                    formData.append("filecontent", filecontent);
                    sendPostDataToURLUpdateSignatureImage("Image_Upload.aspx", formData);
                }
            };
        }

        function sendPostDataToURLUpdateSignatureImage(url, formData) {
            $.ajax({
                url: url,
                type: 'POST',
                success: function () { $("#btnUpdateSignatureImage").click(); },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
        }
        function ChkMinusInt(ctl, e) {
            //var evt = e ? e : window.event; 
            var zz = e.keyCode || e.charCode;
            if (zz == 8 || zz == 9 || zz == 37 || zz == 39)
                return;

            if (zz < 48 || zz > 57) {
                if (window.event) {//IE 
                    var ieVersion = parseFloat(navigator.appVersion);
                    if (ieVersion == 5)  //IE 9, 10 
                        e.preventDefault();
                    else if (ieVersion == 4)  //IE 7,8 
                        event.returnValue = false;
                } else if (e) {//Firefox 
                    if (e.keyCode == 46) //e.keyCode = 46 = Del
                        return;

                    e.preventDefault();
                }
            }
        }
    </script>
</asp:Content>

