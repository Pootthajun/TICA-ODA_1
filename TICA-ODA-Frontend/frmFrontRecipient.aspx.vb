﻿Imports System.Data
Imports LinqDB.TABLE

Partial Class frmFrontRecipient
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return "Administrator" 'Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property


    Public Property Activity_ID As Integer
        Get
            Return lblActivity_id.Attributes("Activity_ID")
        End Get
        Set(value As Integer)
            lblActivity_id.Attributes("Activity_ID") = value
        End Set
    End Property

    Private Sub frmFrontRecipient_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuRecipient")
        li.Attributes.Add("class", "active")

        If IsPostBack = False Then
            Activity_ID = Request.QueryString("id")
            UC_PERSONALHISTORY.ddlActivity_value = Activity_ID

            UC_PERSONALHISTORY.Recipient_ID = 0  '---Add ผู้สมัคร

            aPer_ServerClick(sender, e)
        End If

    End Sub

    Sub btnSave_Click(sender As Object, e As EventArgs)

        Try
            '## Validate
            If Validate() = False Then
                Exit Sub
            End If

            '##Save
            Dim RecipiencePerson As New TbRecipiencePersonLinqDB
            With RecipiencePerson
                .PREFIX_TH_ID = UC_PERSONALHISTORY.ddlPrefix_value
                .NAME_TH = UC_PERSONALHISTORY.GivenName + " " + UC_PERSONALHISTORY.FamilyName
                .PREFIX_EN_ID = UC_PERSONALHISTORY.ddlPrefix_value
                .NAME_EN = UC_PERSONALHISTORY.GivenName + " " + UC_PERSONALHISTORY.FamilyName
                .PARENT_ID = UC_PERSONALHISTORY.ddlCountry_value
                If UC_PERSONALHISTORY.BirthDate <> "" Then
                    .BIRTHDATE = ConverterENG.StringToDate(UC_PERSONALHISTORY.BirthDate, "dd/MM/yyyy")
                End If
            End With

            Dim lnqrec As New TbRecipienceLinqDB
            With lnqrec
                .ACTIVITY_ID = UC_PERSONALHISTORY.ddlActivity_value
                .PSH_SEX = UC_PERSONALHISTORY.ddlSex_value
                .PSH_CITY = UC_PERSONALHISTORY.City
                .PSH_NATIONALITY = UC_PERSONALHISTORY.Nationality
                .PSH_MARITAL_STATUS = UC_PERSONALHISTORY.ddlMaritalStatus_value
                .PSH_RELIGION = UC_PERSONALHISTORY.Religion
                .PSH_WORK_ADDRESS = UC_PERSONALHISTORY.WorkAddress
                .PSH_WORK_TELEPHONE = UC_PERSONALHISTORY.WorkTelphone
                .PSH_WORK_FAX = UC_PERSONALHISTORY.Fax
                .PSH_WORK_EMAIL = UC_PERSONALHISTORY.Email
                .PSH_HOME_ADDRESS = UC_PERSONALHISTORY.HomeAddress
                .PSH_HOME_TELEPHONE = UC_PERSONALHISTORY.HomeTelephone
                .PSH_CITY_DEPARTTURE = UC_PERSONALHISTORY.CityDepartture
                .PSH_EMERGENCY_CONTACT = UC_PERSONALHISTORY.EmergencyPerson
                .PSH_EMERGENCY_TELEPHONE = UC_PERSONALHISTORY.EmergencyTelephone
                .PSH_EMERGENCY_RELATIONSHIP = UC_PERSONALHISTORY.EmergencyRelationship
                .EDU_TOEFLSCORE = UC_EDUCATIONRECORD.TOEFLScore
                .EDU_IELTSSCORE = UC_EDUCATIONRECORD.IELTsScore
                .EDU_OTHERSCORE = UC_EDUCATIONRECORD.OtherScore
                .EDU_TRAINED_DETAIL = UC_EDUCATIONRECORD.Trained
                .EDU_PUBLICATIONS = UC_EDUCATIONRECORD.Researches
                .EXP_EXPECTATION_DESCRIPTION = UC_EXPECTATIONS.ExpectationDescription
                .EXP_SIGNATURE = UC_EXPECTATIONS.Signature
                .EXP_PRINT_NAME = UC_EXPECTATIONS.PrintName
                If UC_EXPECTATIONS.PrintDate <> "" Then
                    .EXP_PRINT_DATE = ConverterENG.StringToDate(UC_EXPECTATIONS.PrintDate, "dd/MM/yyyy")
                End If
                .GA_TITLE_POST = UC_GOVERNMENTAUTHORISATION.TitleOfPost
                .GA_DUTIES = UC_GOVERNMENTAUTHORISATION.Duties
                .GA_STAMP = UC_GOVERNMENTAUTHORISATION.OfficialStamp
                .GA_TITLE = UC_GOVERNMENTAUTHORISATION.Title
                .GA_ORG = UC_GOVERNMENTAUTHORISATION.Organisation
                .GA_ADDRESS = UC_GOVERNMENTAUTHORISATION.OrgOfficialAddress
                If UC_GOVERNMENTAUTHORISATION.txtDate_value <> "" Then
                    .GA_DATE = ConverterENG.StringToDate(UC_GOVERNMENTAUTHORISATION.txtDate_value, "dd/MM/yyyy")
                End If

                Dim ByteImage As Byte() = CType(Session("ProfileImage"), Byte())
                If Not ByteImage Is Nothing Then
                    .PROFILE_IMAGE = ByteImage
                End If

                .ACTIVE_STATUS = "Y"
            End With

            Dim DTEducation As New DataTable
            DTEducation = UC_EDUCATIONRECORD.GetEducationDT

            Dim DTLanguage As New DataTable
            DTLanguage = UC_EDUCATIONRECORD.GetLanguageDT

            Dim DTEmpRecpord As New DataTable
            DTEmpRecpord = UC_EMPLOYMENTRECORD.GetEmpRecpordDT


            Dim ret As New ProcessReturnInfo
            ret = BL.SaveRecipience(RecipiencePerson, lnqrec, DTEducation, DTLanguage, DTEmpRecpord, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('Your information has been submitted successfully.'); window.location ='frmFrontIndex.aspx';", True)
            Else
                alertmsg("Something went wrong. Your data cannot be saved!" & ret.ErrorMessage)
            End If

        Catch ex As Exception
            alertmsg("Something went wrong. Your data cannot be saved!" & ex.ToString())
        End Try
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If UC_PERSONALHISTORY.ddlActivity_value = "" Then
            alertmsg("Please specify Activity")
            ret = False
        End If

        If UC_PERSONALHISTORY.FamilyName = "" Then
            alertmsg("Please specify Family name")
            ret = False
        End If

        If UC_PERSONALHISTORY.GivenName Is Nothing Then
            alertmsg("Please specify Given Name")
            ret = False
        End If

        Dim ByteImage As Byte() = CType(Session("ProfileImage"), Byte())
        If ByteImage Is Nothing Then
            alertmsg("Please attach a current photo of yourself.")
            ret = False
        End If

        If UC_EXPECTATIONS.Signature Is Nothing Then
            alertmsg("Please attach a signature image of yourself.")
            ret = False
        End If



        Return ret
    End Function

    Sub btnCancel_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmFrontIndex.aspx")
    End Sub

    Private Sub btnUpdateProfileImage_Click(sender As Object, e As EventArgs) Handles btnUpdateProfileImage.Click
        Dim ByteImage As Byte() = CType(Session("Image"), Byte())
        Dim base64String As String = Convert.ToBase64String(ByteImage, 0, ByteImage.Length)
        ProfileImg.Attributes.Remove("src")
        ProfileImg.Attributes.Add("src", "data:image/jpeg;base64," + base64String)
        Session("ProfileImage") = Session("Image")
        Session.Remove("Image")
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub TimeLine_Step()
        Select Case True
            Case Per.Visible
                Step_1.Attributes("class") = "NumberActive"
                Step_2.Attributes("class") = "NumberNoneActive"
                Step_3.Attributes("class") = "NumberNoneActive"
                Step_4.Attributes("class") = "NumberNoneActive"
                Step_5.Attributes("class") = "NumberNoneActive"

            Case Edu.Visible

                Step_1.Attributes("class") = "NumberNoneActive"
                Step_2.Attributes("class") = "NumberActive"
                Step_3.Attributes("class") = "NumberNoneActive"
                Step_4.Attributes("class") = "NumberNoneActive"
                Step_5.Attributes("class") = "NumberNoneActive"
            Case Emp.Visible

                Step_1.Attributes("class") = "NumberNoneActive"
                Step_2.Attributes("class") = "NumberNoneActive"
                Step_3.Attributes("class") = "NumberActive"
                Step_4.Attributes("class") = "NumberNoneActive"
                Step_5.Attributes("class") = "NumberNoneActive"
            Case Exp.Visible
                Step_1.Attributes("class") = "NumberNoneActive"
                Step_2.Attributes("class") = "NumberNoneActive"
                Step_3.Attributes("class") = "NumberNoneActive"
                Step_4.Attributes("class") = "NumberActive"
                Step_5.Attributes("class") = "NumberNoneActive"

            Case Gov.Visible

                Step_1.Attributes("class") = "NumberNoneActive"
                Step_2.Attributes("class") = "NumberNoneActive"
                Step_3.Attributes("class") = "NumberNoneActive"
                Step_4.Attributes("class") = "NumberNoneActive"
                Step_5.Attributes("class") = "NumberActive"

        End Select
    End Sub

    Private Sub aPer_ServerClick(sender As Object, e As EventArgs) Handles aPer.ServerClick
        Per.Visible = True
        Edu.Visible = False
        Emp.Visible = False
        Exp.Visible = False
        Gov.Visible = False

        Step_1.Attributes("class") = "NumberActive"
        Step_2.Attributes("class") = "NumberNoneActive"
        Step_3.Attributes("class") = "NumberNoneActive"
        Step_4.Attributes("class") = "NumberNoneActive"
        Step_5.Attributes("class") = "NumberNoneActive"

    End Sub

    Private Sub aEdu_ServerClick(sender As Object, e As EventArgs) Handles aEdu.ServerClick
        Per.Visible = False
        Edu.Visible = True
        Emp.Visible = False
        Exp.Visible = False
        Gov.Visible = False

        Step_1.Attributes("class") = "NumberNoneActive"
        Step_2.Attributes("class") = "NumberActive"
        Step_3.Attributes("class") = "NumberNoneActive"
        Step_4.Attributes("class") = "NumberNoneActive"
        Step_5.Attributes("class") = "NumberNoneActive"

    End Sub

    Private Sub aEmp_ServerClick(sender As Object, e As EventArgs) Handles aEmp.ServerClick
        Per.Visible = False
        Edu.Visible = False
        Emp.Visible = True
        Exp.Visible = False
        Gov.Visible = False

        Step_1.Attributes("class") = "NumberNoneActive"
        Step_2.Attributes("class") = "NumberNoneActive"
        Step_3.Attributes("class") = "NumberActive"
        Step_4.Attributes("class") = "NumberNoneActive"
        Step_5.Attributes("class") = "NumberNoneActive"

    End Sub

    Private Sub aExp_ServerClick(sender As Object, e As EventArgs) Handles aExp.ServerClick
        Per.Visible = False
        Edu.Visible = False
        Emp.Visible = False
        Exp.Visible = True
        Gov.Visible = False


        Step_1.Attributes("class") = "NumberNoneActive"
        Step_2.Attributes("class") = "NumberNoneActive"
        Step_3.Attributes("class") = "NumberNoneActive"
        Step_4.Attributes("class") = "NumberActive"
        Step_5.Attributes("class") = "NumberNoneActive"

    End Sub

    Private Sub aGov_ServerClick(sender As Object, e As EventArgs) Handles aGov.ServerClick
        Per.Visible = False
        Edu.Visible = False
        Emp.Visible = False
        Exp.Visible = False
        Gov.Visible = True

        Step_1.Attributes("class") = "NumberNoneActive"
        Step_2.Attributes("class") = "NumberNoneActive"
        Step_3.Attributes("class") = "NumberNoneActive"
        Step_4.Attributes("class") = "NumberNoneActive"
        Step_5.Attributes("class") = "NumberActive"

    End Sub



    'Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
    '    cptCaptcha.ValidateCaptcha(txtCaptcha.Text.Trim())
    '    If cptCaptcha.UserValidated Then
    '        lblErrorMessage.ForeColor = System.Drawing.Color.Green
    '        lblErrorMessage.Text = "Valid text"
    '    Else
    '        lblErrorMessage.ForeColor = System.Drawing.Color.Red
    '        lblErrorMessage.Text = "InValid Text"
    '    End If
    'End Sub

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================


End Class
