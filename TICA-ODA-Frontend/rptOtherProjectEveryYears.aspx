﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="rptOtherProjectEveryYears.aspx.vb" Inherits="rptAidSummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 <div class="container">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
      
         <section class="content-header">
            <%--<h4>รายงานโครงการต่างๆในแต่ละปี</h4>--%>
            <ol class="breadcrumb">
                <li class="active"><h4>รายงานโครงการต่างๆในแต่ละปี</h4></li>
            </ol>
        </section>

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white"  DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   
                                    
                                    <div class="box-header">
                                       
                                         
                                        <div class="row">
                                            <div class="span6">
                                                 <label for="inputname" class="span3 control-label line-height">Year :</label>
                                                 <div class="span7">
                                                     <asp:DropDownList ID="ddlProject_Year" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                       
                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="span6">
                                                 <label for="inputname" class="span3 control-label line-height">Project :</label>
                                                 <div class="span7">
                                                     <asp:TextBox ID="txtSearch_Project" runat="server"  Style="width: 50%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                         
                                      

                                        <div class="row" >
                                            <div class="span6">
                                                 <label for="inputname" class="span3 control-label line-height"></label>
                                                 <div class="span9">

                                                 </div>
                                                </div>
                                            <div class="span6">
                                                 <label for="inputname" class="span3 control-label line-height"></label>
                                                 <div class="span6">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <div class="btn-group pull-right" style="margin-left :10px;"  data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <span >Export</span>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                                         
                                                         <asp:LinkButton ID="btnSearch" runat="server"  CssClass="btn bg-blue margin-r-5 btn-social  pull-right"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                        
                                                    </p> 
                                                    


                                                 </div>
                                                </div>

                                        </div>


                                         
                                        <div class="span12">
                                        <div class="row pull-left">
                                             
                                            <h4 class="text-primary" style="margin-left :30px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>

                                             
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                       </div>
                                         <div class="span12">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray" style ="text-align :center ;">
                                                    <th class="no"  id="th1" runat="server"  style ="width: 50px;text-align :center ;" >No.<br/>(ลำดับ)</th>
                                                    <th style ="text-align :center ;">Project Code<br/>(รหัส)</th>
                                                    <th style ="text-align :center ;">Project Title<br/>(ชื่อโปรเจค)</th>
                                                    <th style ="text-align :center ;">Country Amount<br/>(จำนวนประเทศ)</th>
                                                    <th style ="text-align :center ;">Disbursed Amount<br/>(จ่ายจริง)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Budget year" colspan="5" style="text-align: center">
                                                             <b>  ปี  <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-title="No" class="center" id="td1" runat="server" style ="cursor: pointer; " tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                            <td id="td2" runat="server"  tooltip="ดูรายละเอียดของโครงการ" data-title="project_id" style ="cursor: pointer; "><asp:Label ID="lblproject_id" runat="server" ForeColor="black"></asp:Label></td>
                                                            <%--<td id="tdproject_name" runat ="server"  data-title="project_name"><asp:Label ID="lblproject_name" runat="server"></asp:Label></td>--%>
                                                            <td id="td3" runat="server"  data-title="project_name" style="cursor: pointer; text-align :left ;width: 600px;"><asp:Label ID="lblproject_name" runat="server"></asp:Label></td>
                                                            <td id="td4" runat="server"  data-title="Country_Amount" style="cursor: pointer; text-align :center ; "><asp:Label ID="lblCountry_Amount" runat="server"></asp:Label></td>
                                                            <td id="td5" runat="server"  data-title="Disbursed_Amount" style="cursor: pointer; text-align :right;width: 180px;"><b><asp:Label ID="lblDisbursed_Amount" runat="server" ForeColor="black"></asp:Label></b>
                                                                 
                                                                    <asp:Button  ID="btnSelect" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" CommandName="select"></asp:Button>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >

                                            
                                            <td data-title="Sector"  style="text-align :center  ;" colspan ="3" ><b>Budget Summary</b></td>
                                            <td data-title="Project"  style="text-align :center ;" ><b><asp:Label ID="lblProject_Sum" runat="server" ForeColor="black" Text =""></asp:Label></b></td>
                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblDisbursed_Amount" runat="server" ForeColor="black"></asp:Label></b></td>
                                                    

										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <%--<uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />--%>
                                    <uc1:PageNavigation runat="server" id="PageNavigation" style="margin-right: 30px;" PageSize="10"  />

                                             </div>
                                        </div>   

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
     </div>
</asp:Content>

