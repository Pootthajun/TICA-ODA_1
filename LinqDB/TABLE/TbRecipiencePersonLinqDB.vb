Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Linq.Mapping
Imports System.Linq
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Recipience_Person table LinqDB.
    '[Create by  on January, 5 2017]
    Public Class TbRecipiencePersonLinqDB
        Public Sub TbRecipiencePersonLinqDB()

        End Sub
        ' TB_Recipience_Person
        Const _tableName As String = "TB_Recipience_Person"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _NODE_ID As String = ""
        Dim _PREFIX_TH_ID As System.Nullable(Of Long)
        Dim _NAME_TH As String = ""
        Dim _PREFIX_EN_ID As System.Nullable(Of Long)
        Dim _NAME_EN As String = ""
        Dim _PARENT_ID As String = ""
        Dim _URL As String = ""
        Dim _ID_CARD As String = ""
        Dim _PASSPORT_NO As String = ""
        Dim _EXPIRED_DATE_PASSPORT As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)
        Dim _BIRTHDATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)
        Dim _USERNAME As String = ""
        Dim _PASSWORD As String = ""
        Dim _CREATED_BY As String = ""
        Dim _CREATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)
        Dim _UPDATED_BY As String = ""
        Dim _UPDATED_DATE As System.Nullable(Of DateTime) = New DateTime(1, 1, 1)
        Dim _PERSON_TYPE As String = ""
        Dim _ACTIVE_STATUS As System.Nullable(Of Char) = ""

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ", CanBeNull:=False)>
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
                _ID = value
            End Set
        End Property
        <Column(Storage:="_NODE_ID", DbType:="VarChar(50) NOT NULL ", CanBeNull:=False)>
        Public Property NODE_ID() As String
            Get
                Return _NODE_ID
            End Get
            Set(ByVal value As String)
                _NODE_ID = value
            End Set
        End Property
        <Column(Storage:="_PREFIX_TH_ID", DbType:="Int")>
        Public Property PREFIX_TH_ID() As System.Nullable(Of Long)
            Get
                Return _PREFIX_TH_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PREFIX_TH_ID = value
            End Set
        End Property
        <Column(Storage:="_NAME_TH", DbType:="VarChar(100)")>
        Public Property NAME_TH() As String
            Get
                Return _NAME_TH
            End Get
            Set(ByVal value As String)
                _NAME_TH = value
            End Set
        End Property
        <Column(Storage:="_PREFIX_EN_ID", DbType:="Int")>
        Public Property PREFIX_EN_ID() As System.Nullable(Of Long)
            Get
                Return _PREFIX_EN_ID
            End Get
            Set(ByVal value As System.Nullable(Of Long))
                _PREFIX_EN_ID = value
            End Set
        End Property
        <Column(Storage:="_NAME_EN", DbType:="VarChar(100)")>
        Public Property NAME_EN() As String
            Get
                Return _NAME_EN
            End Get
            Set(ByVal value As String)
                _NAME_EN = value
            End Set
        End Property
        <Column(Storage:="_PARENT_ID", DbType:="VarChar(50)")>
        Public Property PARENT_ID() As String
            Get
                Return _PARENT_ID
            End Get
            Set(ByVal value As String)
                _PARENT_ID = value
            End Set
        End Property
        <Column(Storage:="_URL", DbType:="VarChar(500)")>
        Public Property URL() As String
            Get
                Return _URL
            End Get
            Set(ByVal value As String)
                _URL = value
            End Set
        End Property
        <Column(Storage:="_ID_CARD", DbType:="VarChar(20)")>
        Public Property ID_CARD() As String
            Get
                Return _ID_CARD
            End Get
            Set(ByVal value As String)
                _ID_CARD = value
            End Set
        End Property
        <Column(Storage:="_PASSPORT_NO", DbType:="VarChar(20)")>
        Public Property PASSPORT_NO() As String
            Get
                Return _PASSPORT_NO
            End Get
            Set(ByVal value As String)
                _PASSPORT_NO = value
            End Set
        End Property
        <Column(Storage:="_EXPIRED_DATE_PASSPORT", DbType:="DateTime")>
        Public Property EXPIRED_DATE_PASSPORT() As System.Nullable(Of DateTime)
            Get
                Return _EXPIRED_DATE_PASSPORT
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _EXPIRED_DATE_PASSPORT = value
            End Set
        End Property
        <Column(Storage:="_BIRTHDATE", DbType:="DateTime")>
        Public Property BIRTHDATE() As System.Nullable(Of DateTime)
            Get
                Return _BIRTHDATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _BIRTHDATE = value
            End Set
        End Property
        <Column(Storage:="_USERNAME", DbType:="VarChar(50)")>
        Public Property USERNAME() As String
            Get
                Return _USERNAME
            End Get
            Set(ByVal value As String)
                _USERNAME = value
            End Set
        End Property
        <Column(Storage:="_PASSWORD", DbType:="VarChar(50)")>
        Public Property PASSWORD() As String
            Get
                Return _PASSWORD
            End Get
            Set(ByVal value As String)
                _PASSWORD = value
            End Set
        End Property
        <Column(Storage:="_CREATED_BY", DbType:="VarChar(100)")>
        Public Property CREATED_BY() As String
            Get
                Return _CREATED_BY
            End Get
            Set(ByVal value As String)
                _CREATED_BY = value
            End Set
        End Property
        <Column(Storage:="_CREATED_DATE", DbType:="DateTime")>
        Public Property CREATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _CREATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _CREATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_BY", DbType:="VarChar(100)")>
        Public Property UPDATED_BY() As String
            Get
                Return _UPDATED_BY
            End Get
            Set(ByVal value As String)
                _UPDATED_BY = value
            End Set
        End Property
        <Column(Storage:="_UPDATED_DATE", DbType:="DateTime")>
        Public Property UPDATED_DATE() As System.Nullable(Of DateTime)
            Get
                Return _UPDATED_DATE
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                _UPDATED_DATE = value
            End Set
        End Property
        <Column(Storage:="_PERSON_TYPE", DbType:="VarChar(20)")>
        Public Property PERSON_TYPE() As String
            Get
                Return _PERSON_TYPE
            End Get
            Set(ByVal value As String)
                _PERSON_TYPE = value
            End Set
        End Property
        <Column(Storage:="_ACTIVE_STATUS", DbType:="Char(1)")>
        Public Property ACTIVE_STATUS() As System.Nullable(Of Char)
            Get
                Return _ACTIVE_STATUS
            End Get
            Set(ByVal value As System.Nullable(Of Char))
                _ACTIVE_STATUS = value
            End Set
        End Property


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _NODE_ID = ""
            _PREFIX_TH_ID = Nothing
            _NAME_TH = ""
            _PREFIX_EN_ID = Nothing
            _NAME_EN = ""
            _PARENT_ID = ""
            _URL = ""
            _ID_CARD = ""
            _PASSPORT_NO = ""
            _EXPIRED_DATE_PASSPORT = New DateTime(1, 1, 1)
            _BIRTHDATE = New DateTime(1, 1, 1)
            _USERNAME = ""
            _PASSWORD = ""
            _CREATED_BY = ""
            _CREATED_DATE = New DateTime(1, 1, 1)
            _UPDATED_BY = ""
            _UPDATED_DATE = New DateTime(1, 1, 1)
            _PERSON_TYPE = ""
            _ACTIVE_STATUS = ""
        End Sub

        'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIf(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SqlTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Recipience_Person table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                _CREATED_BY = CreatedBy
                _CREATED_DATE = DateTime.Now
                Return doInsert(trans)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Person table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                If _ID > 0 Then
                    _UPDATED_BY = UpdatedBy
                    _UPDATED_DATE = DateTime.Now

                    Return doUpdate("ID = @_ID", trans)
                Else
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Person table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SqlTransaction, cmbParm() As SqlParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience_Person table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SqlTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then
                Dim p(1) As SqlParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SqlTransaction) As Boolean
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Recipience_Person by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SqlTransaction) As TbRecipiencePersonLinqDB
            Dim p(1) As SqlParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified PARENT_ID key is retrieved successfully.
        '/// <param name=cPARENT_ID>The PARENT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPARENT_ID(cPARENT_ID As String, trans As SqlTransaction) As Boolean
            Dim cmdPara(2) As SqlParameter
            cmdPara(0) = DB.SetText("@_PARENT_ID", cPARENT_ID)
            Return doChkData("PARENT_ID = @_PARENT_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_Recipience_Person by specified PARENT_ID key is retrieved successfully.
        '/// <param name=cPARENT_ID>The PARENT_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByPARENT_ID(cPARENT_ID As String, cID As Long, trans As SqlTransaction) As Boolean
            Dim cmdPara(2) As SqlParameter
            cmdPara(0) = DB.SetText("@_PARENT_ID", cPARENT_ID)
            cmdPara(1) = DB.SetBigInt("@_ID", cID)
            Return doChkData("PARENT_ID = @_PARENT_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified NODE_ID key is retrieved successfully.
        '/// <param name=cNODE_ID>The NODE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByNODE_ID(cNODE_ID As String, trans As SqlTransaction) As Boolean
            Dim cmdPara(2) As SqlParameter
            cmdPara(0) = DB.SetText("@_NODE_ID", cNODE_ID)
            Return doChkData("NODE_ID = @_NODE_ID", trans, cmdPara)
        End Function

        '/// Returns an duplicate data record of TB_Recipience_Person by specified NODE_ID key is retrieved successfully.
        '/// <param name=cNODE_ID>The NODE_ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDuplicateByNODE_ID(cNODE_ID As String, cID As Long, trans As SqlTransaction) As Boolean
            Dim cmdPara(2) As SqlParameter
            cmdPara(0) = DB.SetText("@_NODE_ID", cNODE_ID)
            cmdPara(1) = DB.SetBigInt("@_ID", cID)
            Return doChkData("NODE_ID = @_NODE_ID And ID <> @_ID", trans, cmdPara)
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Recipience_Person table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt As DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = False
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Person table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SqlTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> "" Then

                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString()
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 & ex.ToString()
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience_Person table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            Dim sql As String = SqlDelete & tmpWhere
            If whText.Trim() <> "" Then

                Try
                    ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                    If ret.IsSuccess = False Then
                        _error = MessageResources.MSGED001
                    Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                    End If
                Catch ex As ApplicationException
                    _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                Catch ex As Exception
                    _error = " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                    ret.IsSuccess = False
                    ret.ErrorMessage = _error
                    ret.SqlStatement = sql
                End Try
            Else
                _error = MessageResources.MSGED003 & "### SQL: " & sql
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                ret.SqlStatement = sql
            End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(19) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_NODE_ID", SqlDbType.VarChar)
            cmbParam(1).Value = _NODE_ID.Trim

            cmbParam(2) = New SqlParameter("@_PREFIX_TH_ID", SqlDbType.Int)
            If _PREFIX_TH_ID IsNot Nothing Then
                cmbParam(2).Value = _PREFIX_TH_ID.Value
            Else
                cmbParam(2).Value = DBNull.Value
            End If

            cmbParam(3) = New SqlParameter("@_NAME_TH", SqlDbType.VarChar)
            If _NAME_TH.Trim <> "" Then
                cmbParam(3).Value = _NAME_TH.Trim
            Else
                cmbParam(3).Value = DBNull.Value
            End If

            cmbParam(4) = New SqlParameter("@_PREFIX_EN_ID", SqlDbType.Int)
            If _PREFIX_EN_ID IsNot Nothing Then
                cmbParam(4).Value = _PREFIX_EN_ID.Value
            Else
                cmbParam(4).Value = DBNull.Value
            End If

            cmbParam(5) = New SqlParameter("@_NAME_EN", SqlDbType.VarChar)
            If _NAME_EN.Trim <> "" Then
                cmbParam(5).Value = _NAME_EN.Trim
            Else
                cmbParam(5).Value = DBNull.Value
            End If

            cmbParam(6) = New SqlParameter("@_PARENT_ID", SqlDbType.VarChar)
            If _PARENT_ID.Trim <> "" Then
                cmbParam(6).Value = _PARENT_ID.Trim
            Else
                cmbParam(6).Value = DBNull.Value
            End If

            cmbParam(7) = New SqlParameter("@_URL", SqlDbType.VarChar)
            If _URL.Trim <> "" Then
                cmbParam(7).Value = _URL.Trim
            Else
                cmbParam(7).Value = DBNull.Value
            End If

            cmbParam(8) = New SqlParameter("@_ID_CARD", SqlDbType.VarChar)
            If _ID_CARD.Trim <> "" Then
                cmbParam(8).Value = _ID_CARD.Trim
            Else
                cmbParam(8).Value = DBNull.Value
            End If

            cmbParam(9) = New SqlParameter("@_PASSPORT_NO", SqlDbType.VarChar)
            If _PASSPORT_NO.Trim <> "" Then
                cmbParam(9).Value = _PASSPORT_NO.Trim
            Else
                cmbParam(9).Value = DBNull.Value
            End If

            cmbParam(10) = New SqlParameter("@_EXPIRED_DATE_PASSPORT", SqlDbType.DateTime)
            If _EXPIRED_DATE_PASSPORT.Value.Year > 1 Then
                cmbParam(10).Value = _EXPIRED_DATE_PASSPORT.Value
            Else
                cmbParam(10).Value = DBNull.Value
            End If

            cmbParam(11) = New SqlParameter("@_BIRTHDATE", SqlDbType.DateTime)
            If _BIRTHDATE.Value.Year > 1 Then
                cmbParam(11).Value = _BIRTHDATE.Value
            Else
                cmbParam(11).Value = DBNull.Value
            End If

            cmbParam(12) = New SqlParameter("@_USERNAME", SqlDbType.VarChar)
            If _USERNAME.Trim <> "" Then
                cmbParam(12).Value = _USERNAME.Trim
            Else
                cmbParam(12).Value = DBNull.Value
            End If

            cmbParam(13) = New SqlParameter("@_PASSWORD", SqlDbType.VarChar)
            If _PASSWORD.Trim <> "" Then
                cmbParam(13).Value = _PASSWORD.Trim
            Else
                cmbParam(13).Value = DBNull.Value
            End If

            cmbParam(14) = New SqlParameter("@_CREATED_BY", SqlDbType.VarChar)
            If _CREATED_BY.Trim <> "" Then
                cmbParam(14).Value = _CREATED_BY.Trim
            Else
                cmbParam(14).Value = DBNull.Value
            End If

            cmbParam(15) = New SqlParameter("@_CREATED_DATE", SqlDbType.DateTime)
            If _CREATED_DATE.Value.Year > 1 Then
                cmbParam(15).Value = _CREATED_DATE.Value
            Else
                cmbParam(15).Value = DBNull.Value
            End If

            cmbParam(16) = New SqlParameter("@_UPDATED_BY", SqlDbType.VarChar)
            If _UPDATED_BY.Trim <> "" Then
                cmbParam(16).Value = _UPDATED_BY.Trim
            Else
                cmbParam(16).Value = DBNull.Value
            End If

            cmbParam(17) = New SqlParameter("@_UPDATED_DATE", SqlDbType.DateTime)
            If _UPDATED_DATE.Value.Year > 1 Then
                cmbParam(17).Value = _UPDATED_DATE.Value
            Else
                cmbParam(17).Value = DBNull.Value
            End If

            cmbParam(18) = New SqlParameter("@_PERSON_TYPE", SqlDbType.VarChar)
            If _PERSON_TYPE.Trim <> "" Then
                cmbParam(18).Value = _PERSON_TYPE.Trim
            Else
                cmbParam(18).Value = DBNull.Value
            End If

            cmbParam(19) = New SqlParameter("@_ACTIVE_STATUS", SqlDbType.Char)
            If _ACTIVE_STATUS.Value <> "" Then
                cmbParam(19).Value = _ACTIVE_STATUS.Value
            Else
                cmbParam(19).Value = DBNull.Value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _ID = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("node_id")) = False Then _NODE_ID = Rdr("node_id").ToString()
                        If Convert.IsDBNull(Rdr("prefix_th_id")) = False Then _PREFIX_TH_ID = Convert.ToInt32(Rdr("prefix_th_id"))
                        If Convert.IsDBNull(Rdr("name_th")) = False Then _NAME_TH = Rdr("name_th").ToString()
                        If Convert.IsDBNull(Rdr("prefix_en_id")) = False Then _PREFIX_EN_ID = Convert.ToInt32(Rdr("prefix_en_id"))
                        If Convert.IsDBNull(Rdr("name_en")) = False Then _NAME_EN = Rdr("name_en").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _PARENT_ID = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("url")) = False Then _URL = Rdr("url").ToString()
                        If Convert.IsDBNull(Rdr("id_card")) = False Then _ID_CARD = Rdr("id_card").ToString()
                        If Convert.IsDBNull(Rdr("passport_no")) = False Then _PASSPORT_NO = Rdr("passport_no").ToString()
                        If Convert.IsDBNull(Rdr("expired_date_passport")) = False Then _EXPIRED_DATE_PASSPORT = Convert.ToDateTime(Rdr("expired_date_passport"))
                        If Convert.IsDBNull(Rdr("birthdate")) = False Then _BIRTHDATE = Convert.ToDateTime(Rdr("birthdate"))
                        If Convert.IsDBNull(Rdr("username")) = False Then _USERNAME = Rdr("username").ToString()
                        If Convert.IsDBNull(Rdr("password")) = False Then _PASSWORD = Rdr("password").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _CREATED_BY = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _UPDATED_BY = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("person_type")) = False Then _PERSON_TYPE = Rdr("person_type").ToString()
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _ACTIVE_STATUS = Rdr("active_status").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Person by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SqlTransaction, cmdPara() As SqlParameter) As TbRecipiencePersonLinqDB
            ClearData()
            _haveData = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SqlDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _ID = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("node_id")) = False Then _NODE_ID = Rdr("node_id").ToString()
                        If Convert.IsDBNull(Rdr("prefix_th_id")) = False Then _PREFIX_TH_ID = Convert.ToInt32(Rdr("prefix_th_id"))
                        If Convert.IsDBNull(Rdr("name_th")) = False Then _NAME_TH = Rdr("name_th").ToString()
                        If Convert.IsDBNull(Rdr("prefix_en_id")) = False Then _PREFIX_EN_ID = Convert.ToInt32(Rdr("prefix_en_id"))
                        If Convert.IsDBNull(Rdr("name_en")) = False Then _NAME_EN = Rdr("name_en").ToString()
                        If Convert.IsDBNull(Rdr("parent_id")) = False Then _PARENT_ID = Rdr("parent_id").ToString()
                        If Convert.IsDBNull(Rdr("url")) = False Then _URL = Rdr("url").ToString()
                        If Convert.IsDBNull(Rdr("id_card")) = False Then _ID_CARD = Rdr("id_card").ToString()
                        If Convert.IsDBNull(Rdr("passport_no")) = False Then _PASSPORT_NO = Rdr("passport_no").ToString()
                        If Convert.IsDBNull(Rdr("expired_date_passport")) = False Then _EXPIRED_DATE_PASSPORT = Convert.ToDateTime(Rdr("expired_date_passport"))
                        If Convert.IsDBNull(Rdr("birthdate")) = False Then _BIRTHDATE = Convert.ToDateTime(Rdr("birthdate"))
                        If Convert.IsDBNull(Rdr("username")) = False Then _USERNAME = Rdr("username").ToString()
                        If Convert.IsDBNull(Rdr("password")) = False Then _PASSWORD = Rdr("password").ToString()
                        If Convert.IsDBNull(Rdr("created_by")) = False Then _CREATED_BY = Rdr("created_by").ToString()
                        If Convert.IsDBNull(Rdr("created_date")) = False Then _CREATED_DATE = Convert.ToDateTime(Rdr("created_date"))
                        If Convert.IsDBNull(Rdr("updated_by")) = False Then _UPDATED_BY = Rdr("updated_by").ToString()
                        If Convert.IsDBNull(Rdr("updated_date")) = False Then _UPDATED_DATE = Convert.ToDateTime(Rdr("updated_date"))
                        If Convert.IsDBNull(Rdr("person_type")) = False Then _PERSON_TYPE = Rdr("person_type").ToString()
                        If Convert.IsDBNull(Rdr("active_status")) = False Then _ACTIVE_STATUS = Rdr("active_status").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Recipience_Person
        Private ReadOnly Property SqlInsert() As String
            Get
                Dim Sql As String = ""
                Sql += "INSERT INTO " & TableName & " (NODE_ID, PREFIX_TH_ID, NAME_TH, PREFIX_EN_ID, NAME_EN, PARENT_ID, URL, ID_CARD, PASSPORT_NO, EXPIRED_DATE_PASSPORT, BIRTHDATE, USERNAME, PASSWORD, CREATED_BY, CREATED_DATE, PERSON_TYPE, ACTIVE_STATUS)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.NODE_ID, INSERTED.PREFIX_TH_ID, INSERTED.NAME_TH, INSERTED.PREFIX_EN_ID, INSERTED.NAME_EN, INSERTED.PARENT_ID, INSERTED.URL, INSERTED.ID_CARD, INSERTED.PASSPORT_NO, INSERTED.EXPIRED_DATE_PASSPORT, INSERTED.BIRTHDATE, INSERTED.USERNAME, INSERTED.PASSWORD, INSERTED.CREATED_BY, INSERTED.CREATED_DATE, INSERTED.UPDATED_BY, INSERTED.UPDATED_DATE, INSERTED.PERSON_TYPE, INSERTED.ACTIVE_STATUS"
                Sql += " VALUES("
                Sql += "@_NODE_ID" & ", "
                Sql += "@_PREFIX_TH_ID" & ", "
                Sql += "@_NAME_TH" & ", "
                Sql += "@_PREFIX_EN_ID" & ", "
                Sql += "@_NAME_EN" & ", "
                Sql += "@_PARENT_ID" & ", "
                Sql += "@_URL" & ", "
                Sql += "@_ID_CARD" & ", "
                Sql += "@_PASSPORT_NO" & ", "
                Sql += "@_EXPIRED_DATE_PASSPORT" & ", "
                Sql += "@_BIRTHDATE" & ", "
                Sql += "@_USERNAME" & ", "
                Sql += "@_PASSWORD" & ", "
                Sql += "@_CREATED_BY" & ", "
                Sql += "@_CREATED_DATE" & ", "
                Sql += "@_PERSON_TYPE" & ", "
                Sql += "@_ACTIVE_STATUS"
                Sql += ")"
                Return Sql
            End Get
        End Property


        'Get update statement form table TB_Recipience_Person
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & TableName & " SET "
                Sql += "NODE_ID = " & "@_NODE_ID" & ", "
                Sql += "PREFIX_TH_ID = " & "@_PREFIX_TH_ID" & ", "
                Sql += "NAME_TH = " & "@_NAME_TH" & ", "
                Sql += "PREFIX_EN_ID = " & "@_PREFIX_EN_ID" & ", "
                Sql += "NAME_EN = " & "@_NAME_EN" & ", "
                Sql += "PARENT_ID = " & "@_PARENT_ID" & ", "
                Sql += "URL = " & "@_URL" & ", "
                Sql += "ID_CARD = " & "@_ID_CARD" & ", "
                Sql += "PASSPORT_NO = " & "@_PASSPORT_NO" & ", "
                Sql += "EXPIRED_DATE_PASSPORT = " & "@_EXPIRED_DATE_PASSPORT" & ", "
                Sql += "BIRTHDATE = " & "@_BIRTHDATE" & ", "
                Sql += "USERNAME = " & "@_USERNAME" & ", "
                Sql += "PASSWORD = " & "@_PASSWORD" & ", "
                Sql += "UPDATED_BY = " & "@_UPDATED_BY" & ", "
                Sql += "UPDATED_DATE = " & "@_UPDATED_DATE" & ", "
                Sql += "PERSON_TYPE = " & "@_PERSON_TYPE" & ", "
                Sql += "ACTIVE_STATUS = " & "@_ACTIVE_STATUS" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Recipience_Person
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & TableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Recipience_Person
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, NODE_ID, PREFIX_TH_ID, NAME_TH, PREFIX_EN_ID, NAME_EN, PARENT_ID, URL, ID_CARD, PASSPORT_NO, EXPIRED_DATE_PASSPORT, BIRTHDATE, USERNAME, PASSWORD, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE, PERSON_TYPE, ACTIVE_STATUS FROM " & TableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
