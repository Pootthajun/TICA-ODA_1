Imports System
Imports System.Data 
Imports System.Data.SQLClient
Imports System.Data.Linq.Mapping 
Imports System.Linq 
Imports DB = LinqDB.ConnectDB.SqlDB
Imports LinqDB.ConnectDB

Namespace TABLE
    'Represents a transaction for TB_Recipience_Education table LinqDB.
    '[Create by  on December, 28 2016]
    Public Class TbRecipienceEducationLinqDB
        Public sub TbRecipienceEducationLinqDB()

        End Sub 
        ' TB_Recipience_Education
        Const _tableName As String = "TB_Recipience_Education"

        'Set Common Property
        Dim _error As String = ""
        Dim _information As String = ""
        Dim _haveData As Boolean = False

        Public ReadOnly Property TableName As String
            Get
                Return _tableName
            End Get
        End Property
        Public ReadOnly Property ErrorMessage As String
            Get
                Return _error
            End Get
        End Property
        Public ReadOnly Property InfoMessage As String
            Get
                Return _information
            End Get
        End Property


        'Generate Field List
        Dim _ID As Long = 0
        Dim _RECIPIENCE_ID As  System.Nullable(Of Long) 
        Dim _INSTITUTION As  String  = ""
        Dim _CITY As  String  = ""
        Dim _ATTENDED_FROM As  String  = ""
        Dim _ATTENDED_TO As  String  = ""
        Dim _CERTIFICATES As  String  = ""
        Dim _SPECIAL_FIELDS As  String  = ""
        Dim _RECORD As  String  = ""

        'Generate Field Property 
        <Column(Storage:="_ID", DbType:="BigInt NOT NULL ",CanBeNull:=false)>  _
        Public Property ID() As Long
            Get
                Return _ID
            End Get
            Set(ByVal value As Long)
               _ID = value
            End Set
        End Property 
        <Column(Storage:="_RECIPIENCE_ID", DbType:="BigInt")>  _
        Public Property RECIPIENCE_ID() As  System.Nullable(Of Long) 
            Get
                Return _RECIPIENCE_ID
            End Get
            Set(ByVal value As  System.Nullable(Of Long) )
               _RECIPIENCE_ID = value
            End Set
        End Property 
        <Column(Storage:="_INSTITUTION", DbType:="VarChar(250)")>  _
        Public Property INSTITUTION() As  String 
            Get
                Return _INSTITUTION
            End Get
            Set(ByVal value As  String )
               _INSTITUTION = value
            End Set
        End Property 
        <Column(Storage:="_CITY", DbType:="VarChar(250)")>  _
        Public Property CITY() As  String 
            Get
                Return _CITY
            End Get
            Set(ByVal value As  String )
               _CITY = value
            End Set
        End Property 
        <Column(Storage:="_ATTENDED_FROM", DbType:="VarChar(20)")>  _
        Public Property ATTENDED_FROM() As  String 
            Get
                Return _ATTENDED_FROM
            End Get
            Set(ByVal value As  String )
               _ATTENDED_FROM = value
            End Set
        End Property 
        <Column(Storage:="_ATTENDED_TO", DbType:="VarChar(20)")>  _
        Public Property ATTENDED_TO() As  String 
            Get
                Return _ATTENDED_TO
            End Get
            Set(ByVal value As  String )
               _ATTENDED_TO = value
            End Set
        End Property 
        <Column(Storage:="_CERTIFICATES", DbType:="VarChar(500)")>  _
        Public Property CERTIFICATES() As  String 
            Get
                Return _CERTIFICATES
            End Get
            Set(ByVal value As  String )
               _CERTIFICATES = value
            End Set
        End Property 
        <Column(Storage:="_SPECIAL_FIELDS", DbType:="VarChar(500)")>  _
        Public Property SPECIAL_FIELDS() As  String 
            Get
                Return _SPECIAL_FIELDS
            End Get
            Set(ByVal value As  String )
               _SPECIAL_FIELDS = value
            End Set
        End Property 
        <Column(Storage:="_RECORD", DbType:="VarChar(1)")>  _
        Public Property RECORD() As  String 
            Get
                Return _RECORD
            End Get
            Set(ByVal value As  String )
               _RECORD = value
            End Set
        End Property 


        'Clear All Data
        Private Sub ClearData()
            _ID = 0
            _RECIPIENCE_ID = Nothing
            _INSTITUTION = ""
            _CITY = ""
            _ATTENDED_FROM = ""
            _ATTENDED_TO = ""
            _CERTIFICATES = ""
            _SPECIAL_FIELDS = ""
            _RECORD = ""
        End Sub

       'Define Public Method 
        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=orderBy>The fields for sort data.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>
        Public Function GetDataList(whClause As String, orderBy As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(SqlSelect & IIf(whClause = "", "", " WHERE " & whClause) & IIF(orderBy = "", "", " ORDER BY  " & orderBy), trans, cmdParm)
        End Function


        'Execute the select statement with the specified condition and return a System.Data.DataTable.
        '/// <param name=whereClause>The condition for execute select statement.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>The System.Data.DataTable object for specified condition.</returns>

        Public Function GetListBySql(Sql As String, trans As SQLTransaction, cmdParm() As SqlParameter) As DataTable
            Return DB.ExecuteTable(Sql, trans, cmdParm)
        End Function


        '/// Returns an indication whether the current data is inserted into TB_Recipience_Education table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Public Function InsertData(CreatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return doInsert(trans)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Education table successfully.
        '/// <param name=userID>The current user.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateData(UpdatedBy As String,trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                If _id > 0 Then 

                    Return doUpdate("ID = @_ID", trans)
                Else 
                    _error = "No ID Data"
                    Dim ret As New ExecuteDataInfo
                    ret.IsSuccess = False
                    ret.SqlStatement = ""
                    ret.ErrorMessage = _error
                    Return ret
                End If 
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.SqlStatement = ""
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Education table successfully.
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Public Function UpdateBySql(Sql As String, trans As SQLTransaction, cmbParm() As SQLParameter) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Return DB.ExecuteNonQuery(Sql, trans, cmbParm)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience_Education table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Public Function DeleteByPK(cID As Long, trans As SQLTransaction) As ExecuteDataInfo
            If trans IsNot Nothing Then 
                Dim p(1) As SQLParameter
                p(0) = DB.SetBigInt("@_ID", cID)
                Return doDelete("ID = @_ID", trans, p)
            Else 
                _error = "Transaction Is not null"
                Dim ret As New ExecuteDataInfo
                ret.IsSuccess = False
                ret.ErrorMessage = _error
                Return ret
            End If 
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Education by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByPK(cID As Long, trans As SQLTransaction) As Boolean
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doChkData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record and Mapping field to Data Class of TB_Recipience_Education by specified ID key is retrieved successfully.
        '/// <param name=cID>The ID key.</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function GetDataByPK(cID As Long, trans As SQLTransaction) As TbRecipienceEducationLinqDB
            Dim p(1) As SQLParameter
            p(0) = DB.SetBigInt("@_ID", cID)
            Return doGetData("ID = @_ID", trans, p)
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Education by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Public Function ChkDataByWhere(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Return doChkData(whText, trans, cmdPara)
        End Function



        '/// Returns an indication whether the current data is inserted into TB_Recipience_Education table successfully.
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if insert data successfully; otherwise, false.</returns>
        Private Function doInsert(trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            If _haveData = False Then
                Try
                    Dim dt as DataTable = DB.ExecuteTable(SqlInsert, trans, SetParameterData())
                    If dt.Rows.Count = 0 Then
                        ret.IsSuccess = False
                        ret.ErrorMessage = DB.ErrorMessage
                    Else
                        _ID = dt.Rows(0)("ID")
                        _haveData = True
                        ret.IsSuccess = True
                        _information = MessageResources.MSGIN001
                        ret.InfoMessage = _information
                    End If
                Catch ex As ApplicationException
                    ret.IsSuccess = false
                    ret.ErrorMessage = ex.Message & "ApplicationException :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                Catch ex As Exception
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEC101 & " Exception :" & ex.ToString()  
                    ret.SqlStatement = SqlInsert
                End Try
            Else
                ret.IsSuccess = False
                ret.ErrorMessage = MessageResources.MSGEN002  
                ret.SqlStatement = SqlInsert
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is updated to TB_Recipience_Education table successfully.
        '/// <param name=whText>The condition specify the updating record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if update data successfully; otherwise, false.</returns>
        Private Function doUpdate(whText As String, trans As SQLTransaction) As ExecuteDataInfo
            Dim ret As New ExecuteDataInfo
            Dim tmpWhere As String = " Where " & whText
            If _haveData = True Then
                Dim sql As String = SqlUpdate & tmpWhere
                If whText.Trim() <> ""
                    Try
                        ret = DB.ExecuteNonQuery(sql, trans, SetParameterData())
                        If ret.IsSuccess = False Then
                            _error = DB.ErrorMessage
                        Else
                            _information = MessageResources.MSGIU001
                            ret.InfoMessage = MessageResources.MSGIU001
                        End If
                    Catch ex As ApplicationException
                        ret.IsSuccess = False
                        ret.ErrorMessage = "ApplicationException:" & ex.Message & ex.ToString() 
                        ret.SqlStatement = sql
                    Catch ex As Exception
                        ret.IsSuccess = False
                        ret.ErrorMessage = "Exception:" & MessageResources.MSGEC102 &  ex.ToString() 
                        ret.SqlStatement = sql
                    End Try
                Else
                    ret.IsSuccess = False
                    ret.ErrorMessage = MessageResources.MSGEU003 
                    ret.SqlStatement = sql
                End If
            Else
                ret.IsSuccess = True
            End If

            Return ret
        End Function


        '/// Returns an indication whether the current data is deleted from TB_Recipience_Education table successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if delete data successfully; otherwise, false.</returns>
        Private Function doDelete(whText As String, trans As SQLTransaction, cmdPara() As SqlParameter) As ExecuteDataInfo
             Dim ret As New ExecuteDataInfo
             Dim tmpWhere As String = " Where " & whText
             Dim sql As String = SqlDelete & tmpWhere
             If whText.Trim() <> ""
                 Try
                     ret = DB.ExecuteNonQuery(sql, trans, cmdPara)
                     If ret.IsSuccess = False Then
                         _error = MessageResources.MSGED001
                     Else
                        _information = MessageResources.MSGID001
                        ret.InfoMessage = MessageResources.MSGID001
                     End If
                 Catch ex As ApplicationException
                     _error = "ApplicationException :" & ex.Message & ex.ToString() & "### SQL:" & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 Catch ex As Exception
                     _error =  " Exception :" & MessageResources.MSGEC103 & ex.ToString() & "### SQL: " & sql
                     ret.IsSuccess = False
                     ret.ErrorMessage = _error
                     ret.SqlStatement = sql
                 End Try
             Else
                 _error = MessageResources.MSGED003 & "### SQL: " & sql
                 ret.IsSuccess = False
                 ret.ErrorMessage = _error
                 ret.SqlStatement = sql
             End If

            Return ret
        End Function

        Private Function SetParameterData() As SqlParameter()
            Dim cmbParam(8) As SqlParameter
            cmbParam(0) = New SqlParameter("@_ID", SqlDbType.BigInt)
            cmbParam(0).Value = _ID

            cmbParam(1) = New SqlParameter("@_RECIPIENCE_ID", SqlDbType.BigInt)
            If _RECIPIENCE_ID IsNot Nothing Then 
                cmbParam(1).Value = _RECIPIENCE_ID.Value
            Else
                cmbParam(1).Value = DBNull.value
            End IF

            cmbParam(2) = New SqlParameter("@_INSTITUTION", SqlDbType.VarChar)
            If _INSTITUTION.Trim <> "" Then 
                cmbParam(2).Value = _INSTITUTION.Trim
            Else
                cmbParam(2).Value = DBNull.value
            End If

            cmbParam(3) = New SqlParameter("@_CITY", SqlDbType.VarChar)
            If _CITY.Trim <> "" Then 
                cmbParam(3).Value = _CITY.Trim
            Else
                cmbParam(3).Value = DBNull.value
            End If

            cmbParam(4) = New SqlParameter("@_ATTENDED_FROM", SqlDbType.VarChar)
            If _ATTENDED_FROM.Trim <> "" Then 
                cmbParam(4).Value = _ATTENDED_FROM.Trim
            Else
                cmbParam(4).Value = DBNull.value
            End If

            cmbParam(5) = New SqlParameter("@_ATTENDED_TO", SqlDbType.VarChar)
            If _ATTENDED_TO.Trim <> "" Then 
                cmbParam(5).Value = _ATTENDED_TO.Trim
            Else
                cmbParam(5).Value = DBNull.value
            End If

            cmbParam(6) = New SqlParameter("@_CERTIFICATES", SqlDbType.VarChar)
            If _CERTIFICATES.Trim <> "" Then 
                cmbParam(6).Value = _CERTIFICATES.Trim
            Else
                cmbParam(6).Value = DBNull.value
            End If

            cmbParam(7) = New SqlParameter("@_SPECIAL_FIELDS", SqlDbType.VarChar)
            If _SPECIAL_FIELDS.Trim <> "" Then 
                cmbParam(7).Value = _SPECIAL_FIELDS.Trim
            Else
                cmbParam(7).Value = DBNull.value
            End If

            cmbParam(8) = New SqlParameter("@_RECORD", SqlDbType.VarChar)
            If _RECORD.Trim <> "" Then 
                cmbParam(8).Value = _RECORD.Trim
            Else
                cmbParam(8).Value = DBNull.value
            End If

            Return cmbParam
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Education by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doChkData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As Boolean
            Dim ret As Boolean = True
            Dim tmpWhere As String = " WHERE " & whText
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("recipience_id")) = False Then _recipience_id = Convert.ToInt64(Rdr("recipience_id"))
                        If Convert.IsDBNull(Rdr("institution")) = False Then _institution = Rdr("institution").ToString()
                        If Convert.IsDBNull(Rdr("city")) = False Then _city = Rdr("city").ToString()
                        If Convert.IsDBNull(Rdr("attended_from")) = False Then _attended_from = Rdr("attended_from").ToString()
                        If Convert.IsDBNull(Rdr("attended_to")) = False Then _attended_to = Rdr("attended_to").ToString()
                        If Convert.IsDBNull(Rdr("certificates")) = False Then _certificates = Rdr("certificates").ToString()
                        If Convert.IsDBNull(Rdr("special_fields")) = False Then _special_fields = Rdr("special_fields").ToString()
                        If Convert.IsDBNull(Rdr("record")) = False Then _record = Rdr("record").ToString()
                    Else
                        ret = False
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    ret = False
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                ret = False
                _error = MessageResources.MSGEV001
            End If

            Return ret
        End Function


        '/// Returns an indication whether the record of TB_Recipience_Education by specified condition is retrieved successfully.
        '/// <param name=whText>The condition specify the deleting record(s).</param>
        '/// <param name=trans>The System.Data.SQLClient.SQLTransaction used by this System.Data.SQLClient.SQLCommand.</param>
        '/// <returns>true if data is retrieved successfully; otherwise, false.</returns>
        Private Function doGetData(whText As String, trans As SQLTransaction, cmdPara() As SQLParameter) As TbRecipienceEducationLinqDB
            ClearData()
            _haveData  = False
            If whText.Trim() <> "" Then
                Dim tmpWhere As String = " WHERE " & whText
                Dim Rdr As SQLDataReader
                Try
                    Rdr = DB.ExecuteReader(SqlSelect() & tmpWhere, trans, cmdPara)
                    If Rdr.Read() Then
                        _haveData = True
                        If Convert.IsDBNull(Rdr("id")) = False Then _id = Convert.ToInt64(Rdr("id"))
                        If Convert.IsDBNull(Rdr("recipience_id")) = False Then _recipience_id = Convert.ToInt64(Rdr("recipience_id"))
                        If Convert.IsDBNull(Rdr("institution")) = False Then _institution = Rdr("institution").ToString()
                        If Convert.IsDBNull(Rdr("city")) = False Then _city = Rdr("city").ToString()
                        If Convert.IsDBNull(Rdr("attended_from")) = False Then _attended_from = Rdr("attended_from").ToString()
                        If Convert.IsDBNull(Rdr("attended_to")) = False Then _attended_to = Rdr("attended_to").ToString()
                        If Convert.IsDBNull(Rdr("certificates")) = False Then _certificates = Rdr("certificates").ToString()
                        If Convert.IsDBNull(Rdr("special_fields")) = False Then _special_fields = Rdr("special_fields").ToString()
                        If Convert.IsDBNull(Rdr("record")) = False Then _record = Rdr("record").ToString()
                    Else
                        _error = MessageResources.MSGEV002
                    End If

                    Rdr.Close()
                Catch ex As Exception
                    ex.ToString()
                    _error = MessageResources.MSGEC104 & " #### " & ex.ToString()
                End Try
            Else
                _error = MessageResources.MSGEV001
            End If
            Return Me
        End Function



        ' SQL Statements


        'Get Insert Statement for table TB_Recipience_Education
        Private ReadOnly Property SqlInsert() As String 
            Get
                Dim Sql As String=""
                Sql += "INSERT INTO " & tableName  & " (RECIPIENCE_ID, INSTITUTION, CITY, ATTENDED_FROM, ATTENDED_TO, CERTIFICATES, SPECIAL_FIELDS, RECORD)"
                Sql += " OUTPUT INSERTED.ID, INSERTED.RECIPIENCE_ID, INSERTED.INSTITUTION, INSERTED.CITY, INSERTED.ATTENDED_FROM, INSERTED.ATTENDED_TO, INSERTED.CERTIFICATES, INSERTED.SPECIAL_FIELDS, INSERTED.RECORD"
                Sql += " VALUES("
                sql += "@_RECIPIENCE_ID" & ", "
                sql += "@_INSTITUTION" & ", "
                sql += "@_CITY" & ", "
                sql += "@_ATTENDED_FROM" & ", "
                sql += "@_ATTENDED_TO" & ", "
                sql += "@_CERTIFICATES" & ", "
                sql += "@_SPECIAL_FIELDS" & ", "
                sql += "@_RECORD"
                sql += ")"
                Return sql
            End Get
        End Property


        'Get update statement form table TB_Recipience_Education
        Private ReadOnly Property SqlUpdate() As String
            Get
                Dim Sql As String = ""
                Sql += "UPDATE " & tableName & " SET "
                Sql += "RECIPIENCE_ID = " & "@_RECIPIENCE_ID" & ", "
                Sql += "INSTITUTION = " & "@_INSTITUTION" & ", "
                Sql += "CITY = " & "@_CITY" & ", "
                Sql += "ATTENDED_FROM = " & "@_ATTENDED_FROM" & ", "
                Sql += "ATTENDED_TO = " & "@_ATTENDED_TO" & ", "
                Sql += "CERTIFICATES = " & "@_CERTIFICATES" & ", "
                Sql += "SPECIAL_FIELDS = " & "@_SPECIAL_FIELDS" & ", "
                Sql += "RECORD = " & "@_RECORD" + ""
                Return Sql
            End Get
        End Property


        'Get Delete Record in table TB_Recipience_Education
        Private ReadOnly Property SqlDelete() As String
            Get
                Dim Sql As String = "DELETE FROM " & tableName
                Return Sql
            End Get
        End Property


        'Get Select Statement for table TB_Recipience_Education
        Private ReadOnly Property SqlSelect() As String
            Get
                Dim Sql As String = "SELECT ID, RECIPIENCE_ID, INSTITUTION, CITY, ATTENDED_FROM, ATTENDED_TO, CERTIFICATES, SPECIAL_FIELDS, RECORD FROM " & tableName
                Return Sql
            End Get
        End Property

    End Class
End Namespace
