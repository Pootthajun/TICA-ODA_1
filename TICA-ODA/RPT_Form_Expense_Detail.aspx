﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RPT_Form_Expense_Detail.aspx.vb" Inherits="RPT_Form_Expense_Detail" %>

<%@ Register Src="~/usercontrol/UCActivityExpensePlan.ascx" TagName="UCActivityExpensePlan" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>


</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtNameSheet" runat="server"></asp:TextBox>
            <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" EnableScriptLocalization="true" EnableScriptGlobalization="true"></asp:ScriptManager>
            <input type="button" onclick="tableToExcel('testTable', txtNameSheet.value)" value="Export to Excel">

            <%--   <asp:Panel ID="PanelBudget" runat="server" Visible="True">
                <uc1:UCActivityExpensePlan runat="server" ID="UCActivityExpensePlan" />


            </asp:Panel>--%>

            <table id="testTable" runat="server">
                <tr>
                    <td colspan="2">
                        <b><span style="text-align: center; font-size: 28px;">บันทึกการเบิกค่าใช้จ่ายผู้รับทุน</span></b>
                    </td>
                </tr>
                <tr>
                    <td>ชื่อ : xxx  xcccc
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>หลักสูตร :  yyyy
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="table table-bordered " style="border-collapse: initial; table-layout: fixed; padding-bottom: 75px;">

                            <thead>
                                <%--<tr class="bg-gray">--%>
                                <tr style="background-color: #1766a9 !important; color: #fdfdfd;">

                                    <th style="width: 160px;">วันที่</th>
                                    <th style="width: 250px;">รายการ</th>
                                    <th style="width: 90px;">เลขเอกสาร</th>
                                    <th style="width: 170px;">รวมค่าใช้จ่าย</th>
                                    <asp:Label ID="lblActivityID1" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblHeaderID" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblTemplate" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblRecipience" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblTemplateId" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblUsername" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblType" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblPID" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Repeater ID="RptHead_ForPlan" runat="server">
                                        <ItemTemplate>
                                            <th style="min-width: 170px; width: 170px;">
                                                <asp:Label ID="lbHeader_ForPlan" runat="server"></asp:Label></th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                            <asp:Repeater ID="rptList_ForPlan" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                                            <asp:Label ID="lblNo" runat="server">

                                            </asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:Label ID="lblPaymentDate" runat="server"></asp:Label>


                                            </div>
                                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td style="padding: 0px; width: 200px;">
                                            <asp:Label ID="lblDetail" runat="server"></asp:Label>
                                        </td>
                                        <td id="td" runat="server" style="padding: 0px; width: 80px">
                                            <asp:Label ID="lblDetailNo" runat="server"></asp:Label>
                                        </td>
                                        <td style="padding: 0px; background-color: powderblue;">
                                            <asp:Label ID="lblSumCol" runat="server"></asp:Label>
                                        </td>
                                        <asp:Repeater ID="rptColItem_ForPlan" runat="server" OnItemDataBound="rptColItem_ItemDataBound_ForPlan">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="udpList" runat="server">
                                                    <ContentTemplate>
                                                        <td style="padding: 0px" id="colExpense_ForPlan" runat="server">
                                                            <asp:Label ID="lblitem_subexpenseid_ForPlan" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="Pay_Amount_Plan" runat="server" Visible="false"></asp:Label>
                                                            <%-- <asp:TextBox ID="txtCol2" MaxLength="10" runat="server" Text="" Style="text-align: right; border: none; width: 100%; height: 35px"></asp:TextBox>
                                                    <asp:TextBox ID="txtTemp" MaxLength="10" runat="server" Text="" Style="text-align: right; border: none; width: 100%; height: 35px; display: none;"></asp:TextBox>--%>

                                                            <asp:Label ID="lblCol2_ForPlan" runat="server" visis="false"></asp:Label>
                                                            <asp:Label ID="lblTemp_ForPlan" runat="server"></asp:Label>
                                                        </td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr style="background-color: lightgoldenrodyellow;">
                                <td colspan="3"><b>รวมงบประมาณ</b></td>
                                <td style="text-align: right;">
                                    <asp:Label ID="lblSum_ForPlan" runat="server"></asp:Label>
                                </td>
                                <asp:Repeater ID="rptSum_ForPlan" runat="server">
                                    <ItemTemplate>
                                        <td style="padding: 0px">
                                            <%--<asp:TextBox ID="txtSum" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" Enabled="false"></asp:TextBox>--%>
                                            <asp:Label ID="lblSum" runat="server"></asp:Label>

                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr style="border-bottom: 1px solid black;">
                </tr>


                <tr>
                    <td colspan="2">
                        <table class="table table-bordered " style="border-collapse: initial; table-layout: fixed; padding-bottom: 75px;">

                            <thead>
                                <%--<tr class="bg-gray">--%>
                                <tr style="background-color: #1766a9 !important; color: #fdfdfd;">

                                    <th style="width: 160px;">วันที่</th>
                                    <th style="width: 250px;">รายการ</th>
                                    <th style="width: 90px;">เลขเอกสาร</th>
                                    <th style="width: 170px;">รวมค่าใช้จ่าย</th>

                                    <asp:Repeater ID="RptHead_ForActual" runat="server">
                                        <ItemTemplate>
                                            <th style="min-width: 170px; width: 170px;">
                                                <asp:Label ID="lbHeader_ForActual" runat="server"></asp:Label></th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptList_ForActual" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                                            <asp:Label ID="lblNo" runat="server">

                                            </asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:Label ID="lblPaymentDate" runat="server"></asp:Label>


                                            </div>
                                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td style="padding: 0px; width: 200px;">
                                            <asp:Label ID="lblDetail" runat="server"></asp:Label>
                                        </td>
                                        <td id="td" runat="server" style="padding: 0px; width: 80px">
                                            <asp:Label ID="lblDetailNo" runat="server"></asp:Label>
                                        </td>
                                        <td style="padding: 0px; background-color: powderblue;">
                                            <asp:Label ID="lblSumCol" runat="server"></asp:Label>
                                        </td>
                                        <asp:Repeater ID="rptColItem_ForActual" runat="server" OnItemDataBound="rptColItem_ItemDataBound_ForActual">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="udpList_ForActual" runat="server">
                                                    <ContentTemplate>
                                                        <td style="padding: 0px" id="colExpense_ForActual" runat="server">
                                                            <asp:Label ID="lblitem_subexpenseid_ForActual" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="Pay_Amount_Actual" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblCol2_ForActual" runat="server" visis="false"></asp:Label>
                                                            <asp:Label ID="lblTemp_ForActual" runat="server"></asp:Label>
                                                        </td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr style="background-color: lightgoldenrodyellow;">
                                <td colspan="3"><b>รวมค่าใช้จ่าย</b></td>
                                <td style="text-align: right;">
                                    <asp:Label ID="lblSum_ForActual" runat="server"></asp:Label>
                                </td>
                                <asp:Repeater ID="rptSum_ForActual" runat="server">
                                    <ItemTemplate>
                                        <td style="padding: 0px">
                                            <asp:Label ID="lblSum" runat="server"></asp:Label>

                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr></tr>
                <tr></tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; color :red;"><b>รวมงบประมาณ</b></td>
                    <td ></td>
                    <td style="text-align: right; color :red;">
                        <asp:Label ID="lblSum_Plan" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; color :red;"><b>รวมค่าใช้จ่าย</b></td>
                    <td ></td>
                    <td style="text-align: right; color :red;">
                        <asp:Label ID="lblSum_Actual" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; color :red;"><b>คงเหลือ</b></td>
                    <td ></td>
                    <td style="text-align: right; color :red;">
                        <asp:Label ID="lblSum_Balance" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>


            <div style="overflow: auto;" runat="server">
            </div>









        </div>
    </form>
</body>
</html>

<script type="text/javascript">
    function myFunction(txt, txtOldVal, textSUM, textSUMCOL) {
        if (txt.value == "") {
            txt.value = 0
        }
        var valtxt = txt.value;

        while (valtxt.search(",") >= 0) {
            valtxt = (valtxt + "").replace(',', '');
        }
        var valtxtOldVal = parseFloat(document.getElementById(txtOldVal).value);
        var valDiff = valtxt - valtxtOldVal;
        var vSum = document.getElementById(textSUM).value;
        var vSumCol = document.getElementById(textSUMCOL).value;
        if (document.getElementById(textSUM).value == "") {
            vSum = 0;
        }
        while (vSum.search(",") >= 0) {
            vSum = (vSum + "").replace(',', '');
        }
        if (document.getElementById(textSUMCOL).value == "") {
            vSumCol = 0;
        }
        while (vSumCol.search(",") >= 0) {
            vSumCol = (vSumCol + "").replace(',', '');
        }

        var vSumConvert = parseFloat(vSum) + parseFloat(valDiff);
        document.getElementById(textSUM).value = addCommas(vSumConvert.toFixed(2));
        var vSumColConvert = parseFloat(vSumCol) + parseFloat(valDiff);
        document.getElementById(textSUMCOL).value = addCommas(vSumColConvert.toFixed(2));
        document.getElementById(txtOldVal).value = valtxt;
    }
</script>

