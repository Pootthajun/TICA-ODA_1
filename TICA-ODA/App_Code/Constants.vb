﻿Imports Microsoft.VisualBasic

Public Class Constants
    Public Enum OU
        Country = 1
        Organize = 2
        Person = 3
    End Enum

    Public Enum Contact_Parent_Type
        Oraganize = 0
        Person = 1
        Project = 2
    End Enum

    Public Enum Person_Type
        Officer = 0
        Recipient = 1
    End Enum

    Public Enum AgencyType
        FunddingAgency = 0
        ExcutingAgency = 1
        ImplementingAgency = 2
    End Enum

    Public Enum Project_Type
        Project = 0
        NonProject = 1
        Loan = 2
        Contribuition = 3
    End Enum

    Public Enum Menu
        'Overall = 1
        'Project = 2
        'NonProject = 3
        'Loan = 4
        'Contribution = 5
        'IncomingBudget = 6
        'BudgetGroup = 7
        'SubBudget = 8
        'BudgetReport = 9
        'Expense = 10
        'ExpenseGroup = 11
        'SubExpense = 12
        'ExpenseReport = 13
        'Report = 14
        'OrganizeStructure = 15
        'CountryGroup = 16
        'OECD = 17
        'OECDRegion = 18
        'CooperationFramework = 19
        'CooperationType = 20
        'Sector = 21
        'SubSector = 22
        'Component = 23
        'Multilateral = 24
        'InKind = 25
        'Person = 30

        'rptProject = 26
        'rptCLMU = 27
        'OECDRegionZone = 28

        'Candidate = 32
        'Recipient = 33

        Overall = 1
        Project = 2
        Non_Project = 3
        Loan = 4
        Contribution = 5
        Incoming_Budget = 6
        Budget_Group = 7
        Sub_Budget = 8
        Expense = 10
        Expense_Group = 11
        Sub_Expense = 12
        Organize_Structure = 15
        Country_Group = 16
        OECD = 17
        OECD_Region = 18
        Cooperation_Framework = 19
        Cooperation_Type = 20
        Sector = 21
        Sub_Sector = 22
        Component = 23
        Multilateral = 24
        In_Kind = 25
        Report_Project = 26
        CLMU = 27
        OECD_RegionZone = 28
        Person = 30
        Plan = 31
        Candidate = 32
        Recipient = 33
        AidSummary = 34
        AidByProjectSummary = 35
        AidByCountrySummary = 36
        Reports_foreign = 37
        Reports_Admin = 38
        Template = 39


    End Enum

    'Structure Prefix_Language
    '    Const Thai = "T"
    '    Const English = "E"
    'End Structure



End Class
