﻿Imports System.Data
Imports System.Data.SqlClient

Imports System.IO
Imports System.Reflection
Imports ClosedXML.Excel
Imports Excel = Microsoft.Office.Interop.Excel
Partial Class RPT_Form_Expense_Detail
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Dim status As String
    Dim dtDtial As DataTable
    Dim dtTemplate As DataTable

    Public Event EditBudget()

    'Dim _ComponentDT_ForPlan As DataTable
    'Public Property ComponentDT_ForPlan As DataTable
    '    Get
    '        Return GetDataFromRpt_ForPlan(rptList_ForPlan)
    '    End Get
    '    Set(value As DataTable)
    '        _ComponentDT_ForPlan = value
    '    End Set
    'End Property

    'Dim _ComponentDT_ForActual As DataTable
    'Public Property ComponentDT_ForActual As DataTable
    '    Get
    '        Return GetDataFromRpt_ForActual(rptList_ForPlan)
    '    End Get
    '    Set(value As DataTable)
    '        _ComponentDT_ForActual = value
    '    End Set
    'End Property


    Dim _ExpenstListDT_ForPlan As DataTable
    Public Property ExpenstListDT_ForPlan As DataTable
        Get
            Return GetDataFromRptcol_ForPlan(rptList_ForPlan)
        End Get
        Set(value As DataTable)
            _ExpenstListDT_ForPlan = value
        End Set
    End Property

    Dim _ExpenstListDT_ForActual As DataTable
    Public Property ExpenstListDT_ForActual As DataTable
        Get
            Return GetDataFromRptcol_ForActual(rptList_ForActual)
        End Get
        Set(value As DataTable)
            _ExpenstListDT_ForActual = value
        End Set
    End Property

    Public Property Recipience_id As Integer
        Get
            Try
                Return ViewState("Recipience_id")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Recipience_id") = value
        End Set
    End Property

    Dim _UserName As String
    Public WriteOnly Property UserName As String
        Set(value As String)
            _UserName = value
        End Set
    End Property

    Public Property Activity_id As Long
        Get
            Try
                Return ViewState("Activity_id")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("Activity_id") = value
        End Set
    End Property

    Dim _Type As String
    Public WriteOnly Property Type As String
        Set(value As String)
            _Type = value
        End Set
    End Property

    Public Property Header_id As String
        Get
            Try
                Return ViewState("Header_id")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            ViewState("Header_id") = value
        End Set
    End Property

    Public Property Template_id As Integer
        Get
            Try
                Return ViewState("Template_id")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Template_id") = value
        End Set
    End Property



    Dim _PID As String
    Public WriteOnly Property PID As String
        Set(value As String)
            _PID = value
        End Set
    End Property


    Private Sub RPT_Form_Expense_Detail_Load(sender As Object, e As EventArgs) Handles Me.Load


        Activity_id = 10809
        Header_id = 10392
        PID = 10313
        Type = 0
        Recipience_id = 107


        '_ForPlan
        SetHead_ForPlan()
        SetDataRpt_ForPlan()
        SetDataRptSum_ForPlan()
        GetComponentDT("Plan")
        GetDataFromRpt_ForPlan()

        '_ForActual
        SetHead_ForActual()
        SetDataRpt_ForActual()
        SetDataRptSum_ForActual()
        GetComponentDT("Actual")
        GetDataFromRpt_ForActual()

    End Sub

    Public Sub GetComponentDT(_For As String)
        Dim dt As New DataTable
        Dim Recip As DataTable = BL.GetRecipinceType(Activity_id)
        If Recip.Rows.Count > 0 Then
            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            If rt = "G" Then
                dt = BL.GetActivityRecipinceGroup(Activity_id)
            ElseIf rt = "C" Then
                dt = BL.GetRecipinceCountry(Activity_id)
            ElseIf rt = "R" Then
                dt = BL.GetActivityRecipinceIndividual(Activity_id)
            ElseIf rt = "O" Then
                dt = BL.GetActivityRecipinceOrganize(Activity_id)
            ElseIf rt = "M" Then
                dt = BL.GetRecipinceMultilateral(Activity_id)
            End If
        End If
        'If _For = "Plan" Then
        '    ComponentDT_ForPlan = dt
        'Else
        '    ComponentDT_ForActual = dt
        'End If

    End Sub

    Function SetHead_ForPlan() As DataTable
        Dim dt_col As New DataTable
        Template_id = BL.GetList_TemplateByActivity(GL.ConvertCINT(Activity_id))
        lblTemplateId.Text = Template_id.ToString

        dt_col = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(Template_id))
        RptHead_ForPlan.DataSource = dt_col
        RptHead_ForPlan.DataBind()

        Return dt_col
    End Function

    Function SetDataRpt_ForPlan() As DataTable

        lblActivityID1.Text = Activity_id
        lblHeaderID.Text = Header_id
        lblTemplate.Text = Template_id
        lblRecipience.Text = Recipience_id
        lblUsername.Text = _UserName
        lblType.Text = _Type
        lblPID.Text = _PID
        Dim dt As New DataTable
        dt = BL.GetDataExpensePlanList_FromQurey(Header_id, Recipience_id)
        If dt.Rows.Count > 0 Then

            dt.Columns.Add("no", GetType(Long))

            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList_ForPlan.DataSource = dt
            rptList_ForPlan.DataBind()

        End If
        ' lblActivityID.Text = Header_id
        Return dt
    End Function

    Function GetDataFromRpt_ForPlan() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Plan", GetType(DateTime))
        dt.Columns.Add("Pay_Plan_Detail")
        dt.Columns.Add("Payment_Plan_Detail")
        dt.Columns.Add("SumCol")
        Dim Sum_ForPlan As Decimal = 0.00
        Dim dr As DataRow
        For i As Integer = 0 To rptList_ForPlan.Items.Count - 1
            Dim lblPaymentDate As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblPaymentDate"), Label)
            Dim lblExpand As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblExpand"), Label)
            Dim lblDetail As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblDetail"), Label)
            Dim lblDetailNo As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblDetailNo"), Label)
            Dim lblID As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblID"), Label)
            Dim lblSumCol As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblSumCol"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            'dr("Payment_Date_Plan") = lblPaymentDate.Text

            If lblPaymentDate.Text <> "" Then
                dr("Payment_Date_Plan") = Converter.StringToDate(lblPaymentDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            dr("Pay_Plan_Detail") = lblDetail.Text
            dr("Payment_Plan_Detail") = lblDetailNo.Text
            dr("SumCol") = lblSumCol.Text
            Sum_ForPlan = Convert.ToDecimal(Sum_ForPlan) + Convert.ToDecimal(lblSumCol.Text.Replace(",", ""))
            dt.Rows.Add(dr)

        Next
        If dt.Rows.Count > 0 Then
            lblSum_ForPlan.Text = Convert.ToDecimal(Sum_ForPlan).ToString("#,##0.00")
        End If
        Return dt
    End Function
    Function SetDataForGetRpt_ForPlan(Aepi As String) As DataTable

        Dim dt As New DataTable

        dt.Columns.Add("amount")
        dt.Columns.Add("expense_sub_id")
        dt.Columns.Add("aepi")

        Dim dr As DataRow
        For j As Integer = 0 To dtDtial.Rows.Count - 1

            dr = dt.NewRow
            Dim ID As String = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
            If ID = Aepi Then
                dr("amount") = dtDtial.Rows(j).Item("Pay_Amount_Plan").ToString
                dr("expense_sub_id") = dtDtial.Rows(j).Item("Expense_Sub_Activity_id").ToString
                dr("aepi") = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
                dt.Rows.Add(dr)
            End If

        Next

        Return dt
    End Function

    Function SetDataRptSum_ForPlan() As DataTable
        Dim dtSubExid As DataTable
        dtSubExid = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(lblTemplateId.Text))


        Dim dt As New DataTable
        With dt
            .Columns.Add("Expense_Sub_Activity_id")
            .Columns.Add("Pay_Amount_Plan")
        End With
        Dim dr As DataRow
        For i As Integer = 0 To dtSubExid.Rows.Count - 1
            dr = dt.NewRow
            dr("Expense_Sub_Activity_id") = dtSubExid.Rows(i)("id").ToString
            dr("Pay_Amount_Plan") = 0
            dt.Rows.Add(dr)
        Next


        Dim dtDetail1 As DataTable = ExpenstListDT_ForPlan

        For i As Integer = 0 To dt.Rows.Count - 1
            For j As Integer = 0 To dtDetail1.Rows.Count - 1
                If dt.Rows(i)("Expense_Sub_Activity_id").ToString = dtDetail1.Rows(j)("Expense_Sub_Activity_id").ToString Then
                    Dim a As Double = Convert.ToDouble(dtDetail1.Rows(j)("Pay_Amount_Plan"))
                    Dim b As Double = Convert.ToDouble(dt.Rows(i)("Pay_Amount_Plan"))
                    a += b
                    dt.Rows(i)("Pay_Amount_Plan") = a
                End If
            Next
        Next
        rptSum_ForPlan.DataSource = dt
        rptSum_ForPlan.DataBind()

    End Function

    Function GetDataFromRptcol_ForPlan(rpt As Repeater) As DataTable
        Dim dte As New DataTable
        dte.Columns.Add("Header_id")
        dte.Columns.Add("Recipience_id")
        dte.Columns.Add("Expense_Sub_Activity_id")
        dte.Columns.Add("Pay_Amount_Plan")
        dte.Columns.Add("Activity_Expense_Plan_id")

        Dim dre As DataRow
        For i As Integer = 0 To rptList_ForPlan.Items.Count - 1
            Dim lblPaymentDate As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblPaymentDate"), Label)
            Dim lblExpand As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblExpand"), Label)
            Dim lblDetail As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblDetail"), Label)
            Dim lblDetailNo As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblDetailNo"), Label)
            Dim lblID As Label = DirectCast(rptList_ForPlan.Items(i).FindControl("lblID"), Label)
            Dim rptColItem_ForPlan As Repeater = DirectCast(rptList_ForPlan.Items(i).FindControl("rptColItem_ForPlan"), Repeater)

            For j As Integer = 0 To rptColItem_ForPlan.Items.Count - 1
                Dim lblitem_subexpenseid_ForPlan As Label = DirectCast(rptColItem_ForPlan.Items(j).FindControl("lblitem_subexpenseid_ForPlan"), Label)
                Dim lblCol2_ForPlan As Label = DirectCast(rptColItem_ForPlan.Items(j).FindControl("lblCol2_ForPlan"), Label)
                dre = dte.NewRow
                dre("Header_id") = lblitem_subexpenseid_ForPlan.Text
                dre("Recipience_id") = lblitem_subexpenseid_ForPlan.Text
                dre("Expense_Sub_Activity_id") = lblitem_subexpenseid_ForPlan.Text
                If lblCol2_ForPlan.Text <> "" Then
                    dre("Pay_Amount_Plan") = lblCol2_ForPlan.Text
                Else
                    dre("Pay_Amount_Plan") = 0
                End If
                dre("Activity_Expense_Plan_id") = lblID.Text


                dte.Rows.Add(dre)
            Next
        Next

        Return dte
    End Function

    Private Sub RptHead_ForPlan_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptHead_ForPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lbHeader_ForPlan As Label = DirectCast(e.Item.FindControl("lbHeader_ForPlan"), Label)

        lbHeader_ForPlan.Text = e.Item.DataItem("sub_name").ToString
    End Sub

    Protected Sub rptList_ForPlan_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList_ForPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label)
        Dim lblDetailNo As Label = DirectCast(e.Item.FindControl("lblDetailNo"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim rptColItem_ForPlan As Repeater = DirectCast(e.Item.FindControl("rptColItem_ForPlan"), Repeater)
        Dim lblPaymentDate As Label = DirectCast(e.Item.FindControl("lblPaymentDate"), Label)
        Dim lblSumCol As Label = DirectCast(e.Item.FindControl("lblSumCol"), Label)

        lblID.Text = e.Item.DataItem("id").ToString
        'lblPaymentDate.Text = e.Item.DataItem("Payment_Date_Plan").ToString
        'If lblPaymentDate.Text <> "" Then
        '    lblPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        'End If

        If Not IsDBNull(e.Item.DataItem("Payment_Date_Plan")) Then
            lblPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End If

        lblDetail.Text = e.Item.DataItem("Pay_Plan_Detail").ToString
        lblDetailNo.Text = e.Item.DataItem("Payment_Plan_Detail").ToString
        If Not IsDBNull(e.Item.DataItem("SumCol")) Then
            lblSumCol.Text = Convert.ToDecimal(e.Item.DataItem("SumCol")).ToString("#,##0.00")
        End If
        'e.Item.DataItem("SumCol").ToString()

        Session("b") = lblSumCol.ClientID
        AddHandler rptColItem_ForPlan.ItemDataBound, AddressOf rptColItem_ItemDataBound_ForPlan
        Dim dt As DataTable
        If status = "1" Then
            dt = SetDataForGetRpt_ForPlan(lblID.Text)
        Else
            If lblID.Text = "0" Then
                dt = BL.GetList_SubExpenseDetailPlanListG(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblID.Text)
            Else
                dt = BL.GetList_SubExpenseDetailPlanList(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblRecipience.Text, lblID.Text)
            End If
        End If

        SetDataRptSum_ForPlan()
        rptColItem_ForPlan.DataSource = dt
        rptColItem_ForPlan.DataBind()

        SetDataRptSum_ForPlan()
    End Sub

    Protected Sub rptColItem_ItemDataBound_ForPlan(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblCol2_ForPlan As Label = DirectCast(e.Item.FindControl("lblCol2_ForPlan"), Label)
        Dim lblitem_subexpenseid_ForPlan As Label = DirectCast(e.Item.FindControl("lblitem_subexpenseid_ForPlan"), Label)
        Dim Pay_Amount_Plan As Label = DirectCast(e.Item.FindControl("Pay_Amount_Plan"), Label)
        Dim lblTemp_ForPlan As Label = DirectCast(e.Item.FindControl("lblTemp_ForPlan"), Label)

        If Not IsDBNull(e.Item.DataItem("amount")) Then
            If e.Item.DataItem("amount") > 0 Then
                lblCol2_ForPlan.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
            End If
            'lblTemp.Text = e.Item.DataItem("amount").ToString
        End If

        'ImplementJavaMoneyText(lblCol2_ForPlan)

        Dim lblSum As Label = rptSum_ForPlan.Items(e.Item.ItemIndex).FindControl("lblSum")
        Dim b As String = Session("b")
        lblCol2_ForPlan.Attributes.Add("onblur", "myFunction(this,'" + lblTemp_ForPlan.ClientID + "','" + lblSum.ClientID + "','" + b + "')")

        lblitem_subexpenseid_ForPlan.Text = e.Item.DataItem("expense_sub_id").ToString
    End Sub

    Private Sub rptSum_ForPlan_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptSum_ForPlan.ItemDataBound
        Dim lblSum As Label = DirectCast(e.Item.FindControl("lblSum"), Label)

        lblSum.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
    End Sub




#Region "Actual"
    Function SetHead_ForActual() As DataTable
        Dim dt_col As New DataTable
        Template_id = BL.GetList_TemplateByActivity(GL.ConvertCINT(Activity_id))
        lblTemplateId.Text = template_id.ToString
        dt_col = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(template_id))
        RptHead_ForActual.DataSource = dt_col
        RptHead_ForActual.DataBind()
        Return dt_col
    End Function

    Function SetDataRpt_ForActual() As DataTable
        lblActivityID1.Text = Activity_id
        lblHeaderID.Text = Header_id
        lblTemplate.Text = Template_id
        lblRecipience.Text = Recipience_id
        lblUsername.Text = _UserName
        lblType.Text = _Type
        lblPID.Text = _PID
        Dim dt As New DataTable
        dt = BL.GetDataExpenseActualList_FromQurey(Header_id, Recipience_id)
        If dt.Rows.Count > 0 Then

            dt.Columns.Add("no", GetType(Long))

            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList_ForActual.DataSource = dt
            rptList_ForActual.DataBind()

        End If
        ' lblActivityID.Text = Header_id
        Return dt
    End Function

    Function GetDataFromRpt_ForActual() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Actual", GetType(DateTime))
        dt.Columns.Add("Pay_Actual_Detail")
        dt.Columns.Add("Payment_Actual_Detail")
        dt.Columns.Add("SumCol")
        Dim Sum_ForActual As Decimal = 0.00
        Dim dr As DataRow
        For i As Integer = 0 To rptList_ForActual.Items.Count - 1
            Dim lblPaymentDate As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblPaymentDate"), Label)
            Dim lblExpand As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblExpand"), Label)
            Dim lblDetail As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblDetail"), Label)
            Dim lblDetailNo As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblDetailNo"), Label)
            Dim lblID As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblID"), Label)
            Dim lblSumCol As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblSumCol"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            'dr("Payment_Date_Actual") = txtPaymentDate.Text
            If lblPaymentDate.Text <> "" Then
                dr("Payment_Date_Actual") = Converter.StringToDate(lblPaymentDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            dr("Pay_Actual_Detail") = lblDetail.Text
            dr("Payment_Actual_Detail") = lblDetailNo.Text
            dr("SumCol") = lblSumCol.Text
            Sum_ForActual = Convert.ToDecimal(Sum_ForActual) + Convert.ToDecimal(lblSumCol.Text.Replace(",", ""))
            dt.Rows.Add(dr)

        Next
        If dt.Rows.Count > 0 Then
            lblSum_ForActual.Text = Convert.ToDecimal(Sum_ForActual).ToString("#,##0.00")
        End If

        Return dt
    End Function

    Function SetDataForGetRpt_ForActual(Aepi As String) As DataTable

        Dim dt As New DataTable

        dt.Columns.Add("amount")
        dt.Columns.Add("expense_sub_id")
        dt.Columns.Add("aepi")

        Dim dr As DataRow
        For j As Integer = 0 To dtDtial.Rows.Count - 1

            dr = dt.NewRow
            Dim ID As String = dtDtial.Rows(j).Item("Activity_Expense_Actual_id").ToString
            If ID = Aepi Then
                dr("amount") = dtDtial.Rows(j).Item("Pay_Amount_Actual").ToString
                dr("expense_sub_id") = dtDtial.Rows(j).Item("Expense_Sub_Actual_id").ToString
                dr("aepi") = dtDtial.Rows(j).Item("Activity_Expense_Actual_id").ToString
                dt.Rows.Add(dr)
            End If

        Next

        Return dt
    End Function

    Function SetDataRptSum_ForActual() As DataTable
        Dim dtSubExid As DataTable
        dtSubExid = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(lblTemplateId.Text))

        Dim dt As New DataTable
        With dt
            .Columns.Add("Expense_Sub_Actual_id")
            .Columns.Add("Pay_Amount_Actual")
        End With
        Dim dr As DataRow
        For i As Integer = 0 To dtSubExid.Rows.Count - 1
            dr = dt.NewRow
            dr("Expense_Sub_Actual_id") = dtSubExid.Rows(i)("id").ToString
            dr("Pay_Amount_Actual") = 0
            dt.Rows.Add(dr)
        Next


        Dim dtDetail1 As DataTable = ExpenstListDT_ForActual

        For i As Integer = 0 To dt.Rows.Count - 1
            For j As Integer = 0 To dtDetail1.Rows.Count - 1
                If dt.Rows(i)("Expense_Sub_Actual_id").ToString = dtDetail1.Rows(j)("Expense_Sub_Actual_id").ToString Then
                    Dim a As Double = Convert.ToDouble(dtDetail1.Rows(j)("Pay_Amount_Actual"))
                    Dim b As Double = Convert.ToDouble(dt.Rows(i)("Pay_Amount_Actual"))
                    a += b
                    dt.Rows(i)("Pay_Amount_Actual") = a
                End If
            Next
        Next
        rptSum_ForActual.DataSource = dt
        rptSum_ForActual.DataBind()

    End Function

    Function GetDataFromRptcol_ForActual(rpt As Repeater) As DataTable
        Dim dte As New DataTable
        dte.Columns.Add("Header_id")
        dte.Columns.Add("Recipience_id")
        dte.Columns.Add("Expense_Sub_Actual_id")
        dte.Columns.Add("Pay_Amount_Actual")
        dte.Columns.Add("Activity_Expense_Actual_id")

        Dim dre As DataRow
        For i As Integer = 0 To rptList_ForActual.Items.Count - 1
            Dim lblPaymentDate As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblPaymentDate"), Label)
            Dim lblExpand As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblExpand"), Label)
            Dim lblDetail As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblDetail"), Label)
            Dim lblDetailNo As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblDetailNo"), Label)
            Dim lblID As Label = DirectCast(rptList_ForActual.Items(i).FindControl("lblID"), Label)
            Dim rptColItem_ForActual As Repeater = DirectCast(rptList_ForActual.Items(i).FindControl("rptColItem_ForActual"), Repeater)

            For j As Integer = 0 To rptColItem_ForActual.Items.Count - 1
                Dim lblitem_subexpenseid_ForActual As Label = DirectCast(rptColItem_ForActual.Items(j).FindControl("lblitem_subexpenseid_ForActual"), Label)
                Dim lblCol2_ForActual As Label = DirectCast(rptColItem_ForActual.Items(j).FindControl("lblCol2_ForActual"), Label)
                dre = dte.NewRow
                dre("Header_id") = lblitem_subexpenseid_ForActual.Text
                dre("Recipience_id") = lblitem_subexpenseid_ForActual.Text
                dre("Expense_Sub_Actual_id") = lblitem_subexpenseid_ForActual.Text
                If lblCol2_ForActual.Text <> "" Then
                    dre("Pay_Amount_Actual") = lblCol2_ForActual.Text
                Else
                    dre("Pay_Amount_Actual") = 0
                End If
                dre("Activity_Expense_Actual_id") = lblID.Text


                dte.Rows.Add(dre)
            Next
        Next

        Return dte
    End Function

    Private Sub RptHead_ForActual_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptHead_ForActual.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lbHeader_ForActual As Label = DirectCast(e.Item.FindControl("lbHeader_ForActual"), Label)

        lbHeader_ForActual.Text = e.Item.DataItem("sub_name").ToString
    End Sub

    Protected Sub rptList_ForActual_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList_ForActual.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label)
        Dim lblDetailNo As Label = DirectCast(e.Item.FindControl("lblDetailNo"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim rptColItem_ForActual As Repeater = DirectCast(e.Item.FindControl("rptColItem_ForActual"), Repeater)
        Dim lblPaymentDate As Label = DirectCast(e.Item.FindControl("lblPaymentDate"), Label)
        Dim lblSumCol As Label = DirectCast(e.Item.FindControl("lblSumCol"), Label)

        lblID.Text = e.Item.DataItem("id").ToString
        'txtPaymentDate.Text = e.Item.DataItem("Payment_Date_Actual").ToString
        'If txtPaymentDate.Text <> "" Then
        '    txtPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Actual")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        'End If
        If Not IsDBNull(e.Item.DataItem("Payment_Date_Actual")) Then
            lblPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Actual")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End If
        lblDetail.Text = e.Item.DataItem("Pay_Actual_Detail").ToString
        lblDetailNo.Text = e.Item.DataItem("Payment_Actual_Detail").ToString
        'txtSumCol.Text = e.Item.DataItem("SumCol").ToString
        If Not IsDBNull(e.Item.DataItem("SumCol")) Then
            lblSumCol.Text = Convert.ToDecimal(e.Item.DataItem("SumCol")).ToString("#,##0.00")
        End If

        Session("a") = lblSumCol.ClientID
        AddHandler rptColItem_ForActual.ItemDataBound, AddressOf rptColItem_ItemDataBound_ForActual
        Dim dt As DataTable
        If status = "1" Then
            dt = SetDataForGetRpt_ForActual(lblID.Text)
        Else
            dt = BL.GetList_SubExpenseDetailActualList(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblRecipience.Text, lblID.Text)
        End If

        SetDataRptSum_ForActual()

        rptColItem_ForActual.DataSource = dt
        rptColItem_ForActual.DataBind()
        SetDataRptSum_ForActual()

    End Sub

    Protected Sub rptColItem_ItemDataBound_ForActual(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblCol2_ForActual As Label = DirectCast(e.Item.FindControl("lblCol2_ForActual"), Label)
        Dim lblitem_subexpenseid_ForActual As Label = DirectCast(e.Item.FindControl("lblitem_subexpenseid_ForActual"), Label)
        Dim rptSum_ForPlan As Label = DirectCast(e.Item.FindControl("rptSum_ForPlan"), Label)
        Dim lblTemp_ForActual As Label = DirectCast(e.Item.FindControl("lblTemp_ForActual"), Label)


        If Not IsDBNull(e.Item.DataItem("amount")) Then
            If e.Item.DataItem("amount") > 0 Then
                lblCol2_ForActual.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
            End If
        End If
        'ImplementJavaMoneyText(txtCol2)

        Dim lblSum As Label = rptSum_ForActual.Items(e.Item.ItemIndex).FindControl("lblSum")
        Dim a As String = Session("a")
        lblCol2_ForActual.Attributes.Add("onblur", "myFunction(this,'" + lblTemp_ForActual.ClientID + "','" + lblSum.ClientID + "','" + a + "')")

        lblitem_subexpenseid_ForActual.Text = e.Item.DataItem("expense_sub_id").ToString

    End Sub

    Private Sub rptSum_ForActual_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptSum_ForActual.ItemDataBound
        Dim lblSum As Label = DirectCast(e.Item.FindControl("lblSum"), Label)
        lblSum.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")

    End Sub







#End Region


















#Region "Excel"

    'Private Sub btnDownloadForm_Click(sender As Object, e As EventArgs) Handles btnDownloadForm.Click
    '    Try

    '        Dim appXL As Excel.Application
    '        Dim wbXl As Excel.Workbook
    '        Dim shXL As Excel.Worksheet
    '        Dim raXL As Excel.Range

    '        ' Start Excel and get Application object.
    '        appXL = CreateObject("Excel.Application")
    '        appXL.Visible = True

    '        ' Add a new workbook.
    '        wbXl = appXL.Workbooks.Add

    '        shXL = wbXl.ActiveSheet

    '        shXL.Name = "Authorize"
    '        Dim SQL As String = "SELECT USER_ID,EMPLOYEE_ID,LOGIN_NAME,PASSWORD,FIRST_NAME,LAST_NAME  FROM MS_USER ORDER BY USER_ID "
    '        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        ' Add table headers going cell by cell.
    '        shXL.Cells(1, 1).Value = "USER_ID"
    '        shXL.Cells(1, 2).Value = "EMPLOYEE_ID"
    '        shXL.Cells(1, 3).Value = "LOGIN_NAME"
    '        shXL.Cells(1, 4).Value = "PASSWORD"
    '        shXL.Cells(1, 5).Value = "FIRST_NAME"
    '        shXL.Cells(1, 6).Value = "LAST_NAME"


    '        ' Format as bold, vertical alignment = center.
    '        With shXL.Range("A1", "F1")
    '            .Font.Bold = True
    '            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
    '        End With

    '        Dim cell(DT.Rows.Count - 1, 6) As String
    '        For i As Integer = 0 To DT.Rows.Count - 1
    '            cell(i, 0) = DT.Rows(i).Item("USER_ID")
    '            cell(i, 1) = DT.Rows(i).Item("EMPLOYEE_ID").ToString()
    '            cell(i, 2) = DT.Rows(i).Item("LOGIN_NAME").ToString()
    '            cell(i, 3) = DT.Rows(i).Item("PASSWORD").ToString()
    '            cell(i, 4) = DT.Rows(i).Item("FIRST_NAME").ToString()
    '            cell(i, 5) = DT.Rows(i).Item("LAST_NAME").ToString()

    '        Next

    '        '' Fill A2:B6 with an array of values (First and Last Names).
    '        shXL.Range("A2", "F" & DT.Rows.Count + 1).Value = cell

    '        ' AutoFit columns A:D.
    '        raXL = shXL.Range("A1", "F1")
    '        raXL.EntireColumn.AutoFit()

    '        ' Make sure Excel is visible and give the user control
    '        ' of Excel's lifetime.
    '        appXL.Visible = True
    '        appXL.UserControl = True

    '        ' Release object references.
    '        raXL = Nothing
    '        shXL = Nothing
    '        wbXl = Nothing
    '        appXL.Quit()
    '        appXL = Nothing
    '        Exit Sub

    '    Catch ex As Exception
    '        Alert(Me.Page, ex.Message)
    '        Exit Sub
    '    End Try

    'End Sub


#End Region

End Class
