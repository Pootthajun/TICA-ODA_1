﻿<%@ Control Language="vb" AutoEventWireup="false" CodeFile="frmChart.ascx.vb" Inherits="frmChart" %>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="dist/css/floating-menu.css">

<script src="http://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="dist/js/floating-menu.js"></script>

<script>
    $.floatingMenu({
        selector: '.designer-actions a[data-action="show-actions-menu-add"]',
        items: [
          
            {
                icon: 'fa fa-building',
                title: 'Add Agency',
                action: function (event) {
                    alert('insert');
                }
            },
            {
                icon: 'fa fa-user',
                title: 'Add Person',
                action: function (event) {
                    alert('edit');
                }
            },
        ]
    });
</script>

<script>
    $.floatingMenu({
        selector: '.designer-actions a[data-action="show-actions-menu-edit"]',
        items: [
          
            {
                icon: 'fa fa-building',
                title: 'Edit Agency',
                action: function (event) {
                    alert('insert');
                }
            },
            {
                icon: 'fa fa-user',
                title: 'Edit Person',
                action: function (event) {
                    alert('edit');
                }
            },
        ]
    });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
