﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="~/frmEditCountry.aspx.vb" Inherits="frmEditCountry" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Add Country | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
   <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <section class="content-header">
           <h1>Country (ประเทศ)</h1>
           <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-area-chart"></i>Dashboard</a></li>
               <li><a href="#">Country</a></li>
               <li class="active">
                   <asp:Label ID="lblTitle1" runat="server" Text=""></asp:Label>
                   Country</li>
           </ol>
       </section>

       <asp:UpdatePanel ID="udpList" runat="server">
           <ContentTemplate>
               <!-- Main content -->
               <section class="content">
                   <br />
                   <!-- START CUSTOM TABS -->
                   <div class="row">
                       <div class="col-md-12">
                           <!-- Horizontal Form -->
                           <div class="box">
                               <div class="box-header with-border bg-blue-gradient">
                                   <h4 class="box-title">
                                       <asp:Label ID="lblTitle2" runat="server" Text=""></asp:Label>
                                       Country
                                   </h4>
                               </div>

                               <!-- form start -->
                               <form class="form-horizontal">
                                   <div class="box-body">

                                       <div class="form-group">
                                           <label for="district" class="col-sm-2 control-label">ชื่อประเทศ : <span style="color: red">*</span></label>
                                           <div class="col-sm-4">
                                               <asp:TextBox ID="txtCountryNameTH" runat="server" Width="325px"></asp:TextBox>
                                           </div>

                                           <label for="district" class="col-sm-2 control-label">Country Name : <span style="color: red">*</span></label>
                                           <div class="col-sm-4">
                                               <asp:TextBox ID="txtCountryNameEN" runat="server" Width="325px"></asp:TextBox>
                                           </div>
                                           <div class="clearfix"></div>
                                           <br />

                                           <label for="inputLevel" class="col-sm-2 control-label">Region (ภูมิภาค) : <span style="color: red">*</span></label>
                                           <div class="col-sm-4">
                                               <asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control select2" Style="width: 100%;">
                                               </asp:DropDownList>
                                           </div>

                                           <label for="inputLevel" class="col-sm-2 control-label">Country Group<br />
                                               (กลุ่มประเทศ) : <span style="color: red">*</span></label>
                                           <div class="col-sm-4">
                                               <asp:DropDownList ID="ddlCountryGruop" runat="server" CssClass="form-control select2" Style="width: 100%;">
                                               </asp:DropDownList>
                                           </div>

                                           <div class="clearfix"></div>

                                           <label for="inputLevel" class="col-sm-2 control-label">Country Zone<br />
                                               (โซนประเทศ) : <span style="color: red">*</span></label>
                                           <div class="col-sm-4">
                                               <asp:DropDownList ID="ddlCountryZone" runat="server" CssClass="form-control select2" Style="width: 100%;">
                                               </asp:DropDownList>
                                           </div>
                                           <div class="form-group">
                                           </div>                                           
                                       </div>
                                       <br />
                                       <div class="clearfix"></div>
                                       <div class="form-group">
                                           <label for="inputname" class="col-sm-2 control-label">Description<br />
                                               (รายละเอียด) :</label>
                                           <div class="col-sm-10">
                                               <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                           </div>

                                       </div>

                                       <div class="clearfix"></div>


                                       <div class="form-group"></div>

                                       <div class="form-group">
                                           <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                                           <div class="col-sm-9">
                                               <label>
                                                   <asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" Checked="true" /></label>
                                           </div>
                                       </div>
                                   </div>
                                   <!-- /.box-body -->
                                   <div class="box-footer">
                                       <div class="col-sm-9"></div>
                                       <div class="col-lg-8"></div>
                                       <div class="col-lg-2">


                                           <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                                                <i class="fa fa-save"></i> Save
                                           </asp:LinkButton>
                                       </div>
                                       <div class="col-lg-2">

                                           <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                                                <i class="fa fa-reply"></i> Cancel
                                           </asp:LinkButton>
                                       </div>
                                   </div>
                                   <!-- /.box-footer -->
                               </form>
                           </div>
                       </div>
                       <!-- /.box -->
                   </div>
                   <!-- /.col -->

               </section>
               <!-- /.content -->
           </ContentTemplate>
       </asp:UpdatePanel>
    </div>
       

<!----------------Page Advance---------------------->  
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
