﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmDashbord_Summary_Cooperation.aspx.vb" Inherits="frmDashbord_Summary_Cooperation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Project | ODA</title>
    <style>
        .icon {
            display: inline;
            padding: 5px;
        }

            .icon li {
                margin-right: 10px;
            }

            .icon i {
                margin-right: 5px;
            }

        .poiter {
            cursor: pointer;
        }
    </style>

    <style> 
 #th_radius_1 {
    border-radius: 30px 30px 0px 0px;
    background: #d3d4d3;
    padding: 20px; 
    
    height: 30px; 
}   #th_radius_2 {
    border-radius: 30px 30px 0px 0px;
    background: #73AD21;
    padding: 20px; 
    
    height: 50px; 
}#th_radius_3 {
    border-radius: 30px 30px 0px 0px;
    background: #d3d4d3;
    padding: 20px; 
    
    height: 30px; 
}
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>Overall (สถานะภาพรวม)
            </h4>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-area-chart"></i>Overall</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div id="div_Project" runat="server">
                        <div class="inner">
                            <font size="5">Project</font>
                            <h4>
                                <%--180 โครงการ--%>
                                <asp:Label ID="lblTotalProject" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer" id="Project_link">More info... <i class="fa fa-arrow-circle-right"></i></a>
                    </div>

                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div id="div_NonProject" runat="server">
                        <div class="inner">
                            <font size="5">Non Project</font>
                            <h4>
                                <%--738 โครงการ--%>
                                <asp:Label ID="lblTotalNonProject" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer" id="NonProject_link">More info... <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div id="div_Loan" runat="server">
                        <div class="inner">
                            <font size="5">Loan</font>
                            <h4>
                                <asp:Label ID="lblTotalLoan" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer" id="Loan_link">More info... <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div id="div_Contribution" runat="server">
                        <div class="inner">
                            <font size="5">Contribution</font>
                            <h4>
                                <asp:Label ID="lblTotalContribution" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer" id="Contribution_link">More info...<i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">จำนวนโครงการ</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="chart-responsive">
                                        <div class="chart" id="bar-chart" style="height: 300px;"></div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <ul class="chart-legend clearfix">
                                            <li class="icon"><i class="fa fa-circle text-aqua"></i>Project</li>
                                            <li class="icon"><i class="fa fa-circle text-green"></i>Non-Project</li>
                                            <li class="icon"><i class="fa fa-circle text-yellow"></i>Loan</li>
                                            <li class="icon"><i class="fa fa-circle text-red"></i>Contribuition</li>
                                        </ul>
                                    </div>
                                </div>

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>


                <div class="col-md-6">
                    <!-- BAR CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:Label ID="lbltextcomponent" runat="server" Text=""></asp:Label>
                            </h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <%-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--%>
                            </div>
                        </div>
                        <br />

                        <div class="box-body chart-responsive">
                            <div class="col-sm-12">
                                <asp:Label ID="lblComponent" runat="server"></asp:Label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->













            </div>
            <!-- /.row -->








            <div class="row">
                <div class="col-md-6">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <%--<h3 class="box-title">Total Value of Thailand International Cooperation Program By Programme</h3>--%>
                            <h3 class="box-title">By Programme</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Chart ID="ChartByProgramme" runat="server"  CssClass="ChartHighligh" Height="500px" Width="500px" BackSecondaryColor="White">
                                    <titles>
                                        <%--<asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="GSP#X" />--%>
                                        <%--<asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" />--%>
                                      
                                    </titles>
                                    <series>
                                        <asp:Series ChartType="Pie" Name="Series1"   MarkerStyle="Square"
                                            XValueType="String" Legend="Legend1"     >
                                            <points>
                                               
                                            </points>
                                            <emptypointstyle LabelAngle="20" />
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                        <legends>
                                            <asp:Legend Docking="Bottom" Name="Legend1" Font="Microsoft Sans Serif, 9.75pt" IsTextAutoFit="False">
                                            </asp:Legend>
                                        </legends>
                                </asp:Chart>
                                </div>
                                

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>


                <div class="col-md-6">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">By Sub-region</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Chart ID="Chart_Sub_region" runat="server"  CssClass="ChartHighligh" Height="500px" Width="500px" BackSecondaryColor="White" Palette="Pastel">
                                    <titles>
                                        <%--<asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="GSP#X" />--%>
                                        <%--<asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" />--%>
                                      
                                    </titles>
                                    <series>
                                        <asp:Series ChartType="Pie" Name="Series_Sub_region" 
                                            XValueType="String" Legend="Legend_Sub_region"     >
                                            <points>
                                               
                                            </points>
                                            <emptypointstyle LabelAngle="20" />
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea_Sub_region">
                                        </asp:ChartArea>
                                    </chartareas>
                                        <legends>
                                            <asp:Legend Docking="Bottom" Name="Legend_Sub_region" Font="Microsoft Sans Serif, 9.75pt" IsTextAutoFit="False">
                                            </asp:Legend>
                                        </legends>
                                </asp:Chart>
                                </div>
                                

                                <!-- /.col -->
                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.col -->













            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <b><h2 class="box-title">Total Value of Thailand International Cooperation Program</h2></b>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                <table id="example2" class="table table-bordered table-hover" style="border-collapse: initial">

                                    <thead>
                                        <tr class="bg-gray text-center">
                                            <th id="th_radius_1" >Sub-region</th>

                                            <%-- Add Header Tablestyle ="border-top-left-radius: 40px;border-top-right-radius : 40px;"--%>
                                            <asp:Repeater ID="rptCol" runat="server">
                                                <ItemTemplate>
                                                    <th id="th_radius_2" runat ="server" >
                                                        <asp:Label ID="lbl_cooperation_type_id" runat="server" Visible="false" ></asp:Label>

                                                        <asp:Label ID="lbl_cooperation_type_Name" runat="server" ></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <%-- End Add Header Table--%>
                                            <th   id="th_radius_3"  >Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                       
                                        <asp:Repeater ID="rptList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td data-title="Regionoda Name" >
                                                        <asp:Label ID="lbl_regionoda_name" runat ="server" ></asp:Label>
                                                    </td>
                                                    <asp:Repeater ID="rptPayment" runat="server">
                                                        <ItemTemplate>
                                                            <td id="tb_lbl_Pay_Amount_Actual"  style=" text-align :right ">
                                                                <asp:Label ID="lbl_Pay_Amount_Actual" runat="server" ></asp:Label>
                                                            </td>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    
                                                    <td data-title="Total" id="td1" runat="server" style="text-align: right;">
                                                        <asp:Label ID="lblTotal_Row" runat="server" Text="0.00"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                    <tfoot >
                                        <%--Total--%>
                                        <tr class="bg-gray text-center">
                                            <td style ="text-align :left ;"     ><b>Total</b></td>
                                            <asp:Repeater ID="rptTotal" runat="server">
                                                <ItemTemplate>
                                                    <td  style ="text-align : right  ;"  >
                                                        <asp:Label ID="lbl_Footer_SUM" runat="server" ></asp:Label>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <%-- End Add Header Table--%>
                                            <td  style ="text-align :right ;"   ><asp:Label ID="lbl_Footer_Tatal" runat="server" ></asp:Label></td>
                                        </tr>


                                        <%--%Total--%>
                                        <tr class="bg-gray text-center">
                                            <td  style ="text-align :left ;"     ><b>%Total</b></td>
                                            <asp:Repeater ID="rptPerTotal" runat="server">
                                                <ItemTemplate>
                                                    <td  style ="text-align :right  ;"  >
                                                        <asp:Label ID="lbl_Per_Footer_SUM" runat="server" ></asp:Label>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <%-- End Add Header Table--%>
                                            <td  style ="text-align :right ;"   ><asp:Label ID="lbl_Per_Footer_Tatal" runat="server" ></asp:Label></td>
                                        </tr>
                                    </tfoot>
                                        
                                
                                </table>
                                </div>




                            </div>
                            <!-- /.box-body -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                </div>















            </div>
            <!-- /.row -->











        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Morris charts -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- Morris.js charts -->
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <asp:Label ID="lblChart" runat="server"></asp:Label>

    <script>
        function link(_type) {
            window.location = 'frmProject.aspx?type=' + _type;

        }
    </script>

</asp:Content>

