﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmExpenseGroup.aspx.vb" Inherits="frmExpenseGroup" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <title>Expense Group | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Expense Group (กลุ่มค่าใช้จ่าย)
            </h1>
            <ol class="breadcrumb">
                <li><a href="frmProjectExpense.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
                <li class="active">Expense Group</li>
            </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto">


                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                             <i class="fa fa-plus"></i>Add Expense Group
                                            </asp:LinkButton>
                                        <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <br />
                                            <div class="col-sm-1">
                                            </div>

                                            <table>
                                                <tr>
                                                    <td>Expense Group :</td>
                                                    <td colspan="3">
                                                         <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อรายจ่าย" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Status :</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                            <asp:ListItem Value="Y">Activate</asp:ListItem>
                                                            <asp:ListItem Value="N">Disabled</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td>
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ</h4>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>Expense Group (รายจ่าย)</th>
                                                    <th style="width: 11%;">Status (สถานะ)</th>
                                                    <th class="tools" id="HeadEdit" runat="server">Edit (แก้ไข)</th>
                                                    <th id="HeadColDelete" runat="server" class="tools">Delete (ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Expense Group">
                                                                <asp:Label ID="lblNames" runat="server"></asp:Label>
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social">
                             <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                             <i class="fa fa-times"></i>Disabled
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td data-title="Edit" id="ColEdit" runat="server">
                                                                <center>
                                                                    <asp:LinkButton ID="btnEdit" runat="server"   CommandName="Edit" CommandArgument='<%# Eval("id") %>'>
                                                                        <i class="fa fa-edit text-success"></i> Edit

                                                                    </asp:LinkButton>
                                                                </center>
                                                            </td>
                                                            <td data-title="Delete" id="ColDelete" runat="server">
                                                                <center>
                                                                  <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' 
                                                                    CommandArgument='<%# Eval("id") %>' CommandName="Delete"><i class="fa fa-trash text-danger"></i> Delete</asp:LinkButton>
                                                                </center>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    
                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

