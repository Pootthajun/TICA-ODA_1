﻿Imports System.Data
Imports System.Globalization
Imports Constants
Partial Class frmOverduePlan
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("OpPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("OpPage") = value
        End Set
    End Property
    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Private Sub frmOverduePlan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aProjectExpense")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then

            BindList("", "", "", "", "")
            Authorize()
            pnlAdSearch.Visible = False
        End If
    End Sub

    Sub Authorize()


        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

        Next
    End Sub
    Private Sub BindList(SDate As String, EDate As String, ProjectName As String, ActivityName As String, Status As String)

        Dim DT As DataTable = BL.GetList_ExpenseList(0, SDate, EDate, ProjectName, ActivityName, Status)
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "OpPage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblDateEnd As Label = DirectCast(e.Item.FindControl("lblDateEnd"), Label)
        Dim lblDateStart As Label = DirectCast(e.Item.FindControl("lblDateStart"), Label)
        Dim lblNameProject As Label = DirectCast(e.Item.FindControl("lblNameProject"), Label)
        Dim lblNameAct As Label = DirectCast(e.Item.FindControl("lblNameAct"), Label)
        Dim lblAmount As Label = DirectCast(e.Item.FindControl("lblAmount"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        lblID.Text = e.Item.DataItem("id").ToString
        lblDateStart.Text = e.Item.DataItem("datestart").ToString
        lblDateEnd.Text = e.Item.DataItem("dateend").ToString
        lblNameProject.Text = e.Item.DataItem("project_name").ToString
        lblNameAct.Text = e.Item.DataItem("activity_name").ToString
    End Sub


    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim _act_id As String = txtClickActivityID.Text
        Response.Redirect("frmActivityExpense.aspx?ActivityID=" & _act_id & "&type=4&mode=" & "Edit" & "&HeaderId=" & _id)
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim _act_id As String = txtClickActivityID.Text
        Dim _type As String = txtClickType.Text
        Dim _pid As String = txtClickPID.Text
        Response.Redirect("frmProject_Detail_Activity.aspx?id=" & _pid & "&type=" & _type & "&mode=" & "palnex" & "&mo=" & _act_id)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Validate() Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                BindList(StartDate, EndDate, txtSearchProname.Text.Trim().Replace("'", ""), TxtserachActivity.Text.Trim().Replace("'", ""), ddlStatus.SelectedValue)
            End If
        Else
            BindList("", "", txtSearchProname.Text.Trim().Replace("'", ""), TxtserachActivity.Text.Trim().Replace("'", ""), ddlStatus.SelectedValue)
        End If
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        btnSearch_Click(Nothing, Nothing)
    End Sub

    Function clearFormSearch()
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        txtSearchProname.Text = ""
        TxtserachActivity.Text = ""
        ddlStatus.SelectedValue = ""
    End Function

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub
    Private Function Validate() As Boolean
        Dim ret As Boolean = True

        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            Try
                Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบรูปแบบวันวันที่")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End Try
        End If

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("กรุณาระบุวันที่เริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
            ret = False
            pnlAdSearch.Visible = True
            Exit Function
        End If

        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub
End Class
