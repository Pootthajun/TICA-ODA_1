﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmActivityExpense.aspx.vb" Inherits="frmActivityExpense" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>--%>
<%@ Register Src="~/usercontrol/UCActivityExpensePlan.ascx" TagName="UCActivityExpensePlan" TagPrefix="uc1" %>
<%@ Register Src="~/usercontrol/UCActivityExpenseActual.ascx" TagName="UCActivityExpenseActual" TagPrefix="uc2" %>

<%@ Register Src="~/usercontrol/UCActivityExpenseAVGRecord.ascx" TagName="UCActivityExpenseAVGRecord" TagPrefix="uc3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
    <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tr-top {
            height: 45px;
            background-color: lightblue;
            border-color: black;
        }

        .tb-fix {
            width: 100%;
            overflow-x: scroll;
            margin-left: 1px;
            overflow-y: visible;
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }

        .tbExpense tr th {
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="Scripts/txtClientControl.js"></script>
</head>
<body>
    <br />
    <form id="form1" runat="server">
        <div class="content-wrapper" style="margin-left: 0px;">

            <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" EnableScriptLocalization="true" EnableScriptGlobalization="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="udpList" runat="server">
                <ContentTemplate>
                    <!-- Main content -->
                    <%--  <section class="content">--%>
                    <div class="row">
                        <div class="col-xs-12">
                            <%-- <asp:Panel ID="Panel1" runat="server" CssClass="card bg-white" Style="position: fixed;width: 100%;">--%>
                            <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Style="">
                                <asp:Label ID="lblexpenseid" runat="server" Text="0" Visible="false"></asp:Label>
                                <table id="example2" class="table" style="margin-bottom: unset; background-color: white; border-bottom: 1px solid #cccccc; z-index: 100;">
                                    <tbody>
                                        <tr class="bg-light-blue-gradient">
                                            <th>
                                                <table>

                                                    <tr>
                                                        <td rowspan="2" style="margin-right: 50px; padding-right: 30px;">
                                                            <b>
                                                                <h3 style="color: blanchedalmond;">Expense (ค่าใช้จ่าย)   </h3>
                                                            </b>
                                                        </td>
                                                        <td>
                                                            <h4 style="color: white"><b>
                                                                <asp:Label ID="txtActivitytName" runat="server"></asp:Label>
                                                                <asp:Label ID="txtProjectName" runat="server"></asp:Label>
                                                            </b></h4>

                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="margin-left: 20px;">Plan start (กำหนดแผนค่าใช้จ่าย) :<span style="color: red">*</span>

                                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentStartDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            To (ถึง) :<span style="color: red">*</span>


                                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentEndDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>

                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            <asp:Button ID="btnLoad" runat="server" ClientIDMode="Static" Text="Test" Style="display: none;" />
                                                        </td>

                                                    </tr>
                                                </table>





                                            </th>
                                            <th></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                            <%--<div id="topmargin"></div>--%>
                            <asp:Panel runat="server" ID="PanelListPlan">
                                <table id="lp" class="table table-bordered " style="border-collapse: initial; table-layout: fixed;">
                                    <tbody>
                                        <asp:Repeater ID="rptList" runat="server">
                                            <ItemTemplate>
                                                <%--<tr class="tr-top" >--%>
                                                <tr style="background-color: silver;">

                                                    <td style="width: 40%; border-left: none; border-right: none">
                                                        <div style="font-size: medium; color: #003d99; margin-left: 20px;">
                                                            <b>
                                                                <img id="imgicon" runat="server" />
                                                                <asp:Label ID="lblName" runat="server"></asp:Label></b>
                                                        </div>
                                                    </td>
                                                    <td id="tdBudget" runat="server" style="width: 20%; border-left: none; border-right: none;">
                                                        <div style="cursor: pointer;">
                                                            <asp:LinkButton ID="btnEditBudget" runat="server" CommandArgument='<%# Eval("recipient_id") %>' CommandName="budget">
                                                                <asp:Label ID="lblbudget" runat="server" Style="font-size: 14px; color: black; font-weight: bold;"></asp:Label>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                    <td id="tdExpense" runat="server" style="width: 20%; border-left: none; border-right: none">
                                                        <div style="cursor: pointer;">
                                                            <asp:LinkButton ID="btnEditExpense" runat="server" CommandArgument='<%# Eval("recipient_id") %>' CommandName="expense">
                                                                <asp:Label ID="lblExpense" runat="server" Style="font-size: 14px; color: black; font-weight: bold;"></asp:Label>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                    <td style="width: 20%; border-left: none; border-right: none">
                                                        <asp:Label ID="lblTotal" runat="server" Style="font-size: 14px; color: black; font-weight: bold;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="PanelBudget" runat="server" Visible="false">
                                                    <tr id="trid1" runat="server" visible="true">
                                                        <td colspan="4">
                                                            <uc1:UCActivityExpensePlan runat="server" ID="UCActivityExpensePlan" />
                                                        </td>
                                                    </tr>

                                                </asp:Panel>
                                                <asp:Panel ID="PanelExpense" runat="server" Visible="false">
                                                    <tr id="tr1" runat="server" visible="true">
                                                        <td colspan="4">
                                                            <uc2:UCActivityExpenseActual runat="server" ID="UCActivityExpenseActual" />
                                                        </td>
                                                    </tr>

                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>




                            </asp:Panel>

                        </div>
                    </div>


                    <asp:Panel ID="PanelListTempRecord" runat="server" Visible="false">

                        <h3 style="padding: 5px; margin-left: 20px;">
                            <asp:Label ID="lblTitle_TempRecord" runat="server"></asp:Label>
                        </h3>
                         <h4  style="  margin-left: 30px;">หมายเหตุ : การเพิ่มรายการนี้เป็นการเพิ่มทีละหลายรายการให้กับประเทศ หรือ ผู้รับทุน โดยใช้ข้อมูลชุดเดียวกันเป็นข้อมูลตั้งต้น</h4> 

                        <%--<div class="alert alert-warning alert-dismissible" role="alert">
                           
                            
                        </div>--%>

                        <uc3:UCActivityExpenseAVGRecord runat="server" ID="UCActivityExpenseAVGRecord" />


                    </asp:Panel>

                    <div class="modal-footer" style="bottom: 0px; margin-top: 30px; text-align: left; background-color: white; width: 100%;">
                        <asp:LinkButton ID="lnkTempRecord_Plan" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-plus"></i> สร้างรายการข้อมูลเฉลี่ย ของงบประมาณ
                        </asp:LinkButton>

                        <asp:LinkButton ID="lnkTempRecord_Actual" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-plus"></i> สร้างรายการข้อมูลเฉลี่ย ของค่าใช้จ่าย
                        </asp:LinkButton>

                        <asp:LinkButton ID="lnkTempRecord_Back" runat="server" CssClass="btn  btn-social btn-google">
                <i class="fa fa-reply"></i> Cancel
                        </asp:LinkButton>


                    </div>


                    <%-- </section>--%>

                    <%--            <div class="modal-footer" style="position: fixed; bottom: 0px; text-align: right; background-color: white; width: 100%;">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-save"></i> Save
                </asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google">
                <i class="fa fa-reply"></i> Cancel
                </asp:LinkButton>
            </div>--%>

        <asp:Panel ID="pnlFooter_btnSave" runat="server"  >


        <div id="divFooter_btnSave" runat="server" class="modal-footer" style="bottom: 0px; text-align: right; background-color: white; width: 100%;">
            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success"  >
               <i class="fa fa-save"></i> Save
            </asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google"    >
                <i class="fa fa-reply"></i> Cancel
            </asp:LinkButton>
        </div>
</asp:Panel>


                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- <div style="height: 70px;"></div>--%>
        </div>
        <%--         <div class="modal-footer" style="position: fixed; bottom: 0px; text-align: right; background-color: white; width: 100%;">--%>

        



    </form>



   <%-- <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>
   <%-- <script type="text/javascript">
        $(document).ready(function () {
            Tang();
            $('#btnLoad').click();
        });

        function Tang() {
            var a = $('#example2').height();
            $('#topmargin').height(a);
        }
    </script>--%>
</body>
</html>
