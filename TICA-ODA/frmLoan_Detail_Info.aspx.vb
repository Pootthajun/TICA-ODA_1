﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmLoan_Detail_Info
    Inherits System.Web.UI.Page


    Dim BL As New ODAENG

#Region "Property"
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property



    Public Property EditProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property

    Public Property DisplayActivity_Type As String
        'month/year
        Get
            Try
                Return ViewState("DisplayActivity_Type")
            Catch ex As Exception
                Return "month"
            End Try
        End Get
        Set(value As String)
            ViewState("DisplayActivity_Type") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property GrancePeriod As Int32
        Get
            Try
                Return ViewState("GrancePeriod")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Int32)
            ViewState("GrancePeriod") = value
        End Set
    End Property
    Public Property GrancePeriodMonth As Int32
        Get
            Try
                Return ViewState("GrancePeriodMonth")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Int32)
            ViewState("GrancePeriodMonth") = value
        End Set
    End Property
    Public Property MaturityPeriod As Int32
        Get
            Try
                Return ViewState("MaturityPeriod")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Int32)
            ViewState("MaturityPeriod") = value
        End Set
    End Property
    Public Property MaturityPeriodMonth As Int32
        Get
            Try
                Return ViewState("MaturityPeriodMonth")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Int32)
            ViewState("MaturityPeriodMonth") = value
        End Set
    End Property

    Public Property ImplementingAgency As String
        Get
            Try
                Return ViewState("ImplementingAgency")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("ImplementingAgency") = value
        End Set
    End Property


#End Region

    Private Sub frmLoanDetail_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack = False Then
            Try

                Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuproject")
                li.Attributes.Add("class", "active")
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aLoan")
                a.Attributes.Add("style", "color:#FF8000")


                ClearForm()
                EditProjectID = CInt(Request.QueryString("ID"))
                mode = Request.QueryString("mode").ToString()
            Catch ex As Exception
                EditProjectID = 0
            End Try

            ProjectType = CInt(Request.QueryString("type"))
            If ProjectType = Constants.Project_Type.Loan Then
                lblProjectType.Text = "Loan"
                lblNavProjectType.Text = "Loan"
                lblHeadProjectType.Text = "Loan"
            End If

            'pnlDialog.Visible = False
            BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
            BL.Bind_DDL_Coperationtype(ddlCooperationType)
            BL.Bind_DDL_OECD(ddlOECDAidType)
            BL.Bind_DDL_Organize(ddlFundingAgency)
            ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
            BL.Bind_DDL_Organize(ddlImplementingAgency)
            BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)
            BL.Bind_DLL_Plan(DropDownListPlan)

            SetAssistant()
            SetProjectFile()
            CurrentTab = Tab.ProjectDetail

            BL.SetTextIntKeypress(txtGrancePeriod)
            BL.SetTextIntKeypress(txtMaturityPeriod)
            BL.SetTextDblKeypress(txtInterestRate)
            'BL.SetTextDblKeypress(txtAllocatedBudget)

            If EditProjectID <> 0 Then
                SetProjectInfoByID(EditProjectID)
                If mode = "view" Then
                    SetControlToViewMode(True)
                Else
                    SetControlToViewMode(False)
                End If
            End If
        Else
            RestoreJQueryUI()
        End If

        If mode = "palnex" Then
            mode = "edit"
        End If

        btnTabActivity.Enabled = False
        If txtProjectID.Text.Trim <> "" Then
            btnTabActivity.Enabled = True
            lblTitle_Project.Visible = False
        Else
            lblTitle_Project.Visible = True

        End If


    End Sub


#Region "Event"

    Private Sub nav_click(sender As Object, e As EventArgs) Handles nav.ServerClick
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Try

            '## Validate
            If Validate() = False Then
                Exit Sub
            End If

            '##Save
            '##tabProjectDetail##
            '-- lnqProject
            Dim lnqProject As New TbProjectLinqDB
            With lnqProject
                .ID = EditProjectID
                .PROJECT_ID = txtProjectID.Text
                .PROJECT_TYPE = ProjectType 'Project_Type.Loan
                .PROJECT_NAME = txtProjectName.Text
                .OBJECTIVE = txtObjective.Text
                .START_DATE = Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .END_DATE = Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text

                'If txtAllocatedBudget.Text = "" Then
                '    .ALLOCATE_BUDGET = 0
                'Else
                '    .ALLOCATE_BUDGET = txtAllocatedBudget.Text
                'End If

                If ddlCooperationFramework.SelectedValue <> "" Then
                    .COOPERATION_FRAMEWORK_ID = ddlCooperationFramework.SelectedValue
                End If

                If ddlCooperationType.SelectedValue <> "" Then
                    .COOPERATION_TYPE_ID = ddlCooperationType.SelectedValue
                End If

                If ddlOECDAidType.SelectedValue <> "" Then
                    .OECD_AID_TYPE_ID = ddlOECDAidType.SelectedValue
                End If

                If ddlFundingAgency.SelectedValue <> "" Then
                    .FUNDING_AGENCY_ID = ddlFundingAgency.SelectedValue
                End If

                If ddlExecutingAgency.SelectedValue <> "" Then
                    .EXECUTING_AGENCY_ID = ddlExecutingAgency.SelectedValue
                End If
                If ddlImplementingAgency.SelectedValue <> "" Then
                    .IMPLEMENTING_AGENCY_LOAN = ddlImplementingAgency.SelectedValue
                End If
                If txtGrancePeriod.Text = "" And txtGrancePeriodMonth.Text = "" Then
                    .GRANCE_PERIOD = 0
                Else
                    Dim grance_period As Integer = BL.GetPeriodMonth(txtGrancePeriod.Text, txtGrancePeriodMonth.Text)
                    .GRANCE_PERIOD = grance_period
                End If

                If txtMaturityPeriod.Text = "" And txtMaturityPeriodMonth.Text = "" Then
                    .MATURITY_PERIOD = 0
                Else
                    Dim maturity_period As Integer = BL.GetPeriodMonth(txtMaturityPeriod.Text, txtMaturityPeriodMonth.Text)
                    .MATURITY_PERIOD = maturity_period
                End If

                If txtInterestRate.Text = "" Then
                    .INTEREST_RATE = 0
                Else
                    .INTEREST_RATE = txtInterestRate.Text
                End If
                .ASSISTANT = GetAssistant()
                .REMARK = txtNote.Text
                If ddlTransferto.SelectedValue <> "" Then
                    .TRANSFER_PROJECT_TO = ddlTransferto.SelectedValue
                End If

            End With


            '--lnqContact
            Dim DTContact As New DataTable
            DTContact = UCContact.ContactDT
            Dim lnqContact As New TbOuContactLinqDB
            If DTContact.Rows.Count > 0 Then
                Dim _dr As DataRow = DTContact.Rows(0)
                With lnqContact
                    .PARENT_TYPE = Contact_Parent_Type.Project
                    .PARENT_ID = lnqProject.ID
                    .CONTACT_NAME = _dr("contact_name").ToString()
                    .TELEPHONE = _dr("telephone").ToString()
                    .FAX = _dr("fax").ToString()
                    .EMAIL = _dr("email").ToString()
                    .POSITION = _dr("position").ToString()
                End With
            End If


            '##DTProjectFile
            Dim DTProjectFile As DataTable = UCFileUploadList1.GetFileList()

            '##DTProjectSection

            '##DTProjectSection
            Dim lnqPjPlan As New TbProjectplanLinqDB
            With lnqPjPlan
                If DropDownListPlan.SelectedValue <> "" Then
                    .PLAN_ID = DropDownListPlan.SelectedValue
                End If
            End With

            Dim redirect As String
            redirect = "frmLoan_Detail_Activity.aspx?"

            Dim ret As New ProcessReturnInfo
            ret = BL.SaveProject(lnqProject, Nothing, Nothing, Nothing, lnqContact, DTProjectFile, Nothing, lnqPjPlan, UserName)
            If ret.IsSuccess Then
                txtProjectID.Text = ret.PROJECT_ID
                EditProjectID = ret.ID

                btnTabActivity.Enabled = True
                ProjectType = CInt(Request.QueryString("type"))
                Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว'); window.location ='" + redirect + Param + "';", True)
            Else
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End If

        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้")
        End Try

    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub
#End Region

#Region "Sub&Function"

    Private Sub SetProjectInfoByID(id As String)

        Try
            Dim dtProject As New DataTable
            dtProject = BL.GetProjectInfoByID(id)

            '##tabProjectDetail##
            If dtProject.Rows.Count > 0 Then
                Dim _project_id As String = ""
                Dim _project_name As String = ""
                Dim _objective As String = ""
                Dim _start_date As String = ""
                Dim _end_date As String = ""
                Dim _allocate_budget As String = ""
                Dim _cooperation_framework_id As String = ""
                Dim _cooperation_type_id As String = ""
                Dim _oecd_aid_type_id As String = ""
                Dim _funding_agency_id As String = ""
                Dim _executing_agency_id As String = ""
                Dim _implementing_agency_loan As String = ""
                Dim _grance_period As String = ""
                Dim _maturity_period As String = ""
                Dim _interest_rate As String = ""
                Dim _assistant As String = ""
                Dim _remark As String = ""
                Dim _transfer_project_to As String = ""

                With dtProject
                    _project_id = .Rows(0)("project_id").ToString()
                    _project_name = .Rows(0)("project_name").ToString()
                    _objective = .Rows(0)("objective").ToString()
                    _start_date = Convert.ToDateTime(.Rows(0)("start_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _end_date = Convert.ToDateTime(.Rows(0)("end_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _allocate_budget = .Rows(0)("allocate_budget").ToString()
                    _cooperation_framework_id = .Rows(0)("cooperation_framework_id").ToString()
                    _cooperation_type_id = .Rows(0)("cooperation_type_id").ToString()
                    _oecd_aid_type_id = .Rows(0)("oecd_aid_type_id").ToString()
                    _funding_agency_id = .Rows(0)("funding_agency_id").ToString()
                    _executing_agency_id = .Rows(0)("executing_agency_id").ToString
                    _remark = .Rows(0)("remark").ToString
                    _transfer_project_to = .Rows(0)("transfer_project_to").ToString
                    _assistant = .Rows(0)("assistant").ToString
                    _implementing_agency_loan = .Rows(0)("implementing_agency_loan").ToString
                    _grance_period = .Rows(0)("grance_period").ToString
                    _maturity_period = .Rows(0)("maturity_period").ToString
                    _interest_rate = .Rows(0)("interest_rate").ToString
                End With


                'ตรงนี้GetProjectPlan
                Dim DT_ProjectPlan As New DataTable
                DT_ProjectPlan = BL.GetProjectPlan(id)
                If DT_ProjectPlan.Rows.Count > 0 Then
                    DropDownListPlan.SelectedValue = DT_ProjectPlan.Rows(0)("plan_id").ToString()
                End If

                txtProjectID.Text = _project_id
                txtProjectName.Text = _project_name
                txtObjective.Text = _objective
                txtStartDate.Text = _start_date
                txtEndDate.Text = _end_date
                'txtAllocatedBudget.Text = _allocate_budget
                If _cooperation_framework_id <> "" Then ddlCooperationFramework.SelectedValue = _cooperation_framework_id
                If _cooperation_type_id <> "" Then ddlCooperationType.SelectedValue = _cooperation_type_id
                If _oecd_aid_type_id <> "" Then ddlOECDAidType.SelectedValue = _oecd_aid_type_id
                Try
                    If _funding_agency_id <> "" Then
                        ddlFundingAgency.SelectedValue = _funding_agency_id
                        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
                        If _executing_agency_id <> "" Then ddlExecutingAgency.SelectedValue = _executing_agency_id
                    End If
                Catch ex As Exception
                End Try

                If _implementing_agency_loan <> "" Then ddlImplementingAgency.SelectedValue = _implementing_agency_loan
                If ddlImplementingAgency.SelectedValue = "" Then
                    ImplementingAgency = ""
                Else
                    ImplementingAgency = ddlImplementingAgency.SelectedItem.Text
                End If


                Dim arr_grance As ArrayList = BL.GetConvertPeriodMonth(_grance_period)
                txtGrancePeriod.Text = arr_grance(0)
                txtGrancePeriodMonth.Text = arr_grance(1)
                ViewState("GrancePeriod") = arr_grance(0)
                ViewState("GrancePeriodMonth") = arr_grance(1)

                Dim arr_maturity As ArrayList = BL.GetConvertPeriodMonth(_maturity_period)
                txtMaturityPeriod.Text = arr_maturity(0)
                txtMaturityPeriodMonth.Text = arr_maturity(1)
                ViewState("MaturityPeriod") = arr_maturity(0)
                ViewState("MaturityPeriodMonth") = arr_maturity(1)

                txtInterestRate.Text = _interest_rate

                UCContact.ContactDT = dtProject
                txtNote.Text = _remark
                ddlTransferto.SelectedValue = _transfer_project_to
                Dim _assistantname As String = ""
                If _assistant <> "" Then
                    Dim _strAssistant() As String = _assistant.Split(", ")
                    For Each Item As ListItem In ctlSelectAssistant.Items
                        For i As Integer = 0 To _strAssistant.Length - 1
                            If Item.Value = _strAssistant(i) Then
                                Item.Selected = True
                                _assistantname &= Item.Text & ","
                            End If
                        Next
                    Next
                End If

                txtAssistant.Text = ""
                If _assistantname.Length > 0 Then
                    txtAssistant.Text = _assistantname.Substring(0, _assistantname.Length - 1)
                End If
            End If

            '##tabActivity


            'ตรงนี้GetProjectPlan
            Dim ProjectPlan As New DataTable
            ProjectPlan = BL.GetProjectPlan(id)
            If ProjectPlan.Rows.Count > 0 Then
                DropDownListPlan.SelectedValue = ProjectPlan.Rows(0)("plan_id").ToString()
            End If

        Catch ex As Exception
            alertmsg(ex.Message.ToString)
        End Try
    End Sub



    Private Sub SetControlToViewMode(IsView As Boolean)
        DropDownListPlan.Enabled = Not IsView
        txtProjectID.Enabled = Not IsView
        txtProjectName.Enabled = Not IsView
        txtObjective.Enabled = Not IsView
        txtStartDate.Enabled = Not IsView
        txtEndDate.Enabled = Not IsView
        'txtAllocatedBudget.Enabled = Not IsView
        ddlCooperationFramework.Enabled = Not IsView
        ddlCooperationType.Enabled = Not IsView
        ddlOECDAidType.Enabled = Not IsView
        ddlImplementingAgency.Enabled = Not IsView
        txtGrancePeriod.Enabled = Not IsView
        txtMaturityPeriod.Enabled = Not IsView
        txtInterestRate.Enabled = Not IsView
        ddlFundingAgency.Enabled = Not IsView
        ddlExecutingAgency.Enabled = Not IsView
        ctlSelectAssistant.Visible = Not IsView
        txtAssistant.Visible = IsView
        txtAssistant.Enabled = Not IsView
        UCContact.SetToViewMode(IsView)
        txtNote.Enabled = Not IsView
        ddlTransferto.Enabled = Not IsView

        txtGrancePeriodMonth.Enabled = Not IsView
        txtMaturityPeriodMonth.Enabled = Not IsView

        btnSave.Visible = Not IsView

        UCFileUploadList1.SetToViewMode(IsView)
    End Sub

    Private Sub ClearForm()
        ''##tabProjectDetail##
        EditProjectID = 0
        txtProjectID.Text = ""
        lblTitle_Project.Text = "<span style='color: red;'>*</span>" + " Loan Name<br>(ชื่อโครงการเงินกู้)"
        'lblTitle_Project.Attributes("PlaceHolder") = "Project Name"
        txtProjectName.Text = ""
        txtObjective.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        'txtAllocatedBudget.Text = ""
        BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
        BL.Bind_DDL_Organize(ddlFundingAgency)
        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
        BL.Bind_DDL_Organize(ddlImplementingAgency)
        txtGrancePeriod.Text = ""
        txtMaturityPeriod.Text = ""
        txtInterestRate.Text = ""
        BL.Bind_DDL_Coperationtype(ddlCooperationType)
        BL.Bind_DDL_OECD(ddlOECDAidType)

        SetAssistant()
        ctlSelectAssistant.Visible = True
        txtAssistant.Visible = False
        UCContact.ClearForm()
        txtNote.Text = ""
        BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)

    End Sub

    Private Sub SetProjectFile()
        UCFileUploadList1.ClearTempFolder(UserName)

        Dim dt As DataTable = BL.GetProjectFile(EditProjectID)
        If dt.Rows.Count > 0 Then
            UCFileUploadList1.SetFileList(dt)
        End If
    End Sub

    Private Sub RestoreJQueryUI()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RestoreJQueryUI", "restoreJQueryUI();", True)
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub


    Private Sub SetAssistant()
        Dim dt As New DataTable
        dt = BL.GetPersonList(Person_Type.Officer)

        ctlSelectAssistant.DataValueField = "id"
        ctlSelectAssistant.DataTextField = "name_th"

        ctlSelectAssistant.DataSource = dt
        ctlSelectAssistant.DataBind()
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtProjectName.Text.Trim() = "" Then
            alertmsg("กรุณาระบุชื่อโครงการ")
            ret = False
        End If
        If txtStartDate.Text = "" Then
            alertmsg("กรุณากรอกวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtEndDate.Text = "" Then
            alertmsg("กรุณากรอกวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlCooperationFramework.SelectedValue = "" Then
            alertmsg("กรุณาระบุกรอบความร่วมมือ")
            ret = False
        End If

        Try
            Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("แผนของวันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        Return ret
    End Function

    Private Function GetAssistant() As String
        Dim strAssistant As String = ""

        For Each Item As ListItem In ctlSelectAssistant.Items
            If Item.Selected Then
                strAssistant += Item.Value & ","
            End If
        Next
        If strAssistant.Length > 0 Then
            Return strAssistant.Substring(0, strAssistant.Length - 1)
        Else
            Return ""
        End If

    End Function

    Private Sub ddlFundingAgency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFundingAgency.SelectedIndexChanged
        BL.Bind_DDL_ChildsOrganize(ddlExecutingAgency, ddlFundingAgency.SelectedValue)
    End Sub
#End Region

#Region "Tab"
    Protected Enum Tab
        Unknown = 0
        ProjectDetail = 1
        Activity = 2
        Recipient = 3
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case tabProjectDetail.Visible
                    Return Tab.ProjectDetail
                Case Else
                    Return Tab.Unknown
            End Select
        End Get
        Set(value As Tab)
            tabProjectDetail.Visible = False

            liTabProjectDetail.Attributes("class") = ""
            liTabActivity.Attributes("class") = ""

            Select Case value
                Case Tab.ProjectDetail
                    tabProjectDetail.Visible = True
                    liTabProjectDetail.Attributes("class") = "active"
                Case Else
            End Select
        End Set
    End Property

    Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabProjectDetail.Click, btnTabActivity.Click
        Select Case True
            Case Equals(sender, btnTabProjectDetail)
                CurrentTab = Tab.ProjectDetail
            Case Equals(sender, btnTabActivity)
                CurrentTab = Tab.Activity
            Case Else
        End Select
    End Sub


#End Region


#Region " Print Button"
    Function GetParameter(Reportformat As String) As String
        Dim para As String = "&ReportName=rptLoanDetail"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectID=" & EditProjectID
        para += "&GrancePeriod=" & GrancePeriod
        para += "&GrancePeriodMonth=" & GrancePeriodMonth
        para += "&MaturityPeriod=" & MaturityPeriod
        para += "&MaturityPeriodMonth=" & MaturityPeriodMonth

        para += "&ProjectTypeName=Loan"
        Return para
    End Function



#End Region

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptLoanDetail.aspx?Mode=PDF" + para + "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptLoanDetail.aspx?Mode=EXCEL" + para + "');", True)
    End Sub

#End Region


#Region "Navigator"


    Private Sub btnTabProjectDetail_Click(sender As Object, e As EventArgs) Handles btnTabProjectDetail.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmLoan_Detail_Info.aspx?" & Param & "';", True)
    End Sub

    Private Sub btnTabActivity_Click(sender As Object, e As EventArgs) Handles btnTabActivity.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmLoan_Detail_Activity.aspx?" & Param & "';", True)
    End Sub

#End Region


End Class
