﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmProjectExpenseDetail.aspx.vb" Inherits="frmProjectExpenseDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/usercontrol/UCExpenseDetail.ascx" TagPrefix="uc1" TagName="UCExpenseDetail" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <title>Expense | ODA</title>

    <style type="text/css">
        table { border-collapse:separate;}        
        .tb-fix { 
            width: 1060px; 
            overflow-x:scroll;  
            /*margin-left:245px;*/ 
            overflow-y:visible;
            position: relative;
        }        
        .tb-fix-col-1 {
            position:fixed; 
            width:75px; 
            left:0;
            top:auto;
        }        
        .tb-fix-col-2 {
            position:fixed; 
            width:170px; 
            left:75px;
            top:auto;
        }
	</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>Expense Detail (รายละเอียดรายจ่าย)
            </h4>
            <ol class="breadcrumb">
                <li><a href="frmProjectExpense.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
                <li><a href="frmProjectExpense.aspx">Expense</a></li>
                <li class="active">Expense Detail</li>
            </ol>
        </section>
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-info">
                                            <th style="width: 150px" colspan="2">
                                                <h5><b class="text-blue">Informations (ข้อมูลทั่วไป) </b></h5>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Project Title (ชื่อโครงการ) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtProjectName" runat="server" ReadOnly="true" TextMode="MultiLine" BackColor="#F2F2F2" Height="60px" Width="100%" MaxLength="500"></asp:TextBox>
                                                    <asp:TextBox ID="txtProjectID" runat="server" Text="" Visible="false"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Record (ครั้งที่) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtRecord" runat="server" Width="180px" ReadOnly="true" Text="" BackColor="#F2F2F2"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-4">
                                                    <p class="pull-right">Payment Date (วันที่จ่าย) :<span style="color: red">*</span></p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtPaymentDate" Width="150px" runat="server" placeholder=""></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtPaymentDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Received By (ผู้รับ) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtReceivedBy" runat="server" Width="100%" MaxLength="250"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Description (รายละเอียด) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Template (รูปแบบตาราง) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--Table Part--%>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" >
                                    <asp:Label ID="lblexpenseid" runat="server" Text="0" Visible="false"></asp:Label>

                                    <table id="example2" class="table table-bordered table-hover" style="border-collapse: initial; width:100%; table-layout:fixed">
                                        <tbody>
                                            <asp:Repeater ID="rptList" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblactivityid" runat="server" Text="0" Visible="false"></asp:Label>
                                                            <uc1:UCExpenseDetail runat="server" ID="UCExpenseDetail" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>

                                    <!-- PageNavigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="100" />

                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /.content -->
        <div class="modal-footer">
            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success" OnClick="btnSave_Click">
               <i class="fa fa-save"></i> Save
            </asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google" OnClick="btnCancel_Click">
                <i class="fa fa-reply"></i> Cancel
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>

