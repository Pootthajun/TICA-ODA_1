﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditCooperationFramework.aspx.vb" Inherits="frmEditCooperationFramework" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Cooperation Framework | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cooperation Framework (กรอบความร่วมมือ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
            <li><a href="frmCooperationFramework.aspx"> Cooperation Framework</a></li>
            <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> Cooperation Framework</li>
          </ol>
        </section>

         <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title"><asp:Label ID="lblEditMode2" runat="server" Text=""></asp:Label> Cooperation Framework</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Cooperation Framework 
                          <br /> (กรอบความร่วมมือ) : <span style="color:red">*</span></label>
                      <div class="col-sm-4">

                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>

                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Description English <br /> (รายละเอียดภาษาอังกฤษ):</label>
                      <div class="col-sm-8">
                        
                          <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control" ></asp:TextBox>
                      </div>
                    </div>
              <!-- /.box -->
                        <div class="clearfix"></div>
                       <br />

                      <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Description Thai <br /> (รายละเอียดไทย):</label>
                      <div class="col-sm-8">
                        
                          <asp:TextBox ID="TxtDetail_th" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>
              <!-- /.box -->
                        <div class="clearfix"></div>
                       <br />
                      
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Active Status :</label>
                      <div class="col-sm-8">
                     <label><asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" checked="true"/></label>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                   <div class="col-sm-9"></div>
                    <div class="col-lg-8"></div><div class="col-lg-2">
                      
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                                 <i class="fa fa-save"></i> Save
                            </asp:LinkButton>
                        </a>
                        </div>
                    <div class="col-lg-2">
                      
                         <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                                <i class="fa fa-reply"></i> Cancel
                            </asp:LinkButton>
                    </div>
                  </div><!-- /.box-footer -->

<%--                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class="bg-gray">
                        <th style="width: 100px">ลำดับ</th>
                        <th>Cooperation Framework 5 Last Order (กรอบความร่วมมือ 5 ลำดับล่าสุด )</th>
                         <th>Description (รายละเอียด)</th>
                      </tr>
                    </thead>
                    <tbody>
                         <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound">
						<ItemTemplate> 
                      <tr>
                         <td><asp:Label ID="lblNumber" runat="server"></asp:Label></td>
                        <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                          <td><asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                      </tr>
                     </ItemTemplate>
                          </asp:Repeater>
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->--%>
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
         </ContentTemplate>
        </asp:UpdatePanel>

        </div>
     

<!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>

