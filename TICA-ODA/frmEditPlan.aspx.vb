﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmEditPlan
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditPlanID As Long
        Get
            Try
                Return ViewState("PlanID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("PlanID") = value
        End Set
    End Property

    Private Sub frmEditPlan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aPlan")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            txtNames.Focus() 'Focus txtNames

            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditPlanID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditPlanID = 0
                End Try

            End If

            GetPlanInfoForEdit()
        End If
    End Sub

    Sub GetPlanInfoForEdit()
        ClearEditForm()

        If EditPlanID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_Plan(EditPlanID, "", "", "", "")
            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("plan_name").ToString()
                txtNames_TH.Text = dt.Rows(0)("plan_name_th").ToString()
                Dim _start_date As String = ""
                Dim _end_date As String = ""

                _start_date = Convert.ToDateTime(dt.Rows(0)("start_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                _end_date = Convert.ToDateTime(dt.Rows(0)("end_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

                txtStartDate.Text = _start_date
                txtEndDate.Text = _end_date

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "New "
            lblEditMode2.Text = "New "
        End If
    End Sub

    Private Sub ClearEditForm()
        txtNames.Text = ""
        txtNames_TH.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub

#Region "Button"
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) 'Handles btnSave.Click
        If Validate() = True Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbPlanLinqDB
                'Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByPLAN_NAME(txtNames.Text.Trim, EditPlanID, Nothing)
                'If chkDupName = True Then
                '    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                '    Exit Sub
                'End If

                lnqLoc.ChkDataByPK(EditPlanID, trans.Trans)
                With lnqLoc
                    .PLAN_NAME = txtNames.Text.Replace("'", "''")
                    .PLAN_NAME_TH = txtNames_TH.Text.Replace("'", "''")
                    .START_DATE = Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                    .END_DATE = Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If

                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmPlan.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.Message.Replace("'", """") & "');", True)
            End Try
        End If
    End Sub

    Protected Sub btnCancle_Click(sender As Object, e As System.EventArgs) 'Handles btnCancle.Click
        Response.Redirect("frmPlan.aspx")
    End Sub
#End Region

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtNames.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ ชื่อนโยบายและแผนงาน');", True)
            ret = False

            Exit Function
        End If

        Try
            Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False

            Exit Function
        End Try

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("แผนของวันเริ่มต้นต้องน้อยกว่าวันสิ้นสุด")
            ret = False

            Exit Function
        End If

        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

End Class
