﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmActivityExpensePlan.aspx.vb" Inherits="frmActivityExpensePlan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/usercontrol/UCExpenseDetailPlan.ascx" TagPrefix="uc2" TagName="UCExpenseDetailPlan" %>
<%@ Register Src="~/usercontrol/UCExpenseDetail.ascx" TagPrefix="uc3" TagName="UCExpenseDetail" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
        <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tb-fix {
            width: 100%;
            /*overflow-x: scroll;*/
            /*margin-left:245px;*/
            /*overflow-y: visible;*/
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }
    </style>
</head>
<body>
    <br/>
    <form id="form1" runat="server">
                <div class="content-wrapper" style="margin-left: 0px;">
                    <section class="content-header">
                        <h1>Expense (ค่าใช้จ่าย)
                        </h1>
                    </section>
                    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
                    <asp:UpdatePanel ID="udpList" runat="server">
                        <ContentTemplate>
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box">
                                            <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white">
                                                <asp:Label ID="lblexpenseid" runat="server" Text="0" Visible="false"></asp:Label>

                                                <table id="example2" class="table table-bordered">
                                                    <tbody>
                                                        <tr class="bg-info">
                                                    <th style="width: 150px" colspan="4">
                                                        <h4 class="text-blue"><b>
                                                            ค่าใช้จ่ายของ <asp:Label ID="txtActivitytName" runat="server" Text="Label"></asp:Label>
                                                            จาก <asp:Label ID="txtProjectName" runat="server" Text="Label"></asp:Label><br/>
                                                        </b></h4>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td colspan ="1">
                                                        <p class="pull-right">duration start(เริ่มต้น) :<span style="color: red">*</span></p>

                                                    </td>
                                                    <td colspan ="1">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox CssClass="form-control m-b" ID="txtPaymentStartDate" Width="50%" runat="server" placeholder=""></asp:TextBox>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtPaymentStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            </div>
                                                    </td>
                                                    <td colspan ="1">
                                                        <p class="pull-right">duration End (วันที่สิ้นสุด) :<span style="color: red">*</span></p>

                                                    </td>
                                                    <td colspan ="1">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <asp:TextBox CssClass="form-control m-b" ID="txtPaymentEndDate" Width="50%" runat="server" placeholder=""></asp:TextBox>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtPaymentEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            </div>
                                                    </td>
                                                    </tr>
                                                        <asp:Repeater ID="rptList" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lblactivityid" runat="server" Text="0" Visible="false"></asp:Label>
                                                                        <uc2:UCExpenseDetailPlan runat="server" ID="UCExpenseDetailPlan" visible="false"/>
                                                                        <uc3:UCExpenseDetail runat="server" ID="UCExpenseDetail" visible="false"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    <tr>
                                                    <td colspan ="1">
                                                        <p class="pull-right">Detail (รายละเอียด) :</p>

                                                    </td>
                                                    <td colspan ="3">
                                                            <asp:TextBox ID="txtDetail" runat="server" TextMode="MultiLine" Width="80%" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                    </tbody>
                                                </table>
                                                </asp:Panel>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="height: 50px;"></div>
                    <div class="modal-footer" style="position: fixed; bottom: 0px; text-align: right; width: 100%;">
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-save"></i> Save
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google">
                <i class="fa fa-reply"></i> Cancel
                        </asp:LinkButton>
                    </div>
                </div>
            
    </form>
</body>
</html>
