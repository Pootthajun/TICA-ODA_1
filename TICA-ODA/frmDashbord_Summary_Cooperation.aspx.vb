﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class frmDashbord_Summary_Cooperation
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Private Sub frmProjectStatus_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuProjectStatus")
        li.Attributes.Add("class", "active")
        If IsPostBack = False Then
            Bind_Cooperation_Master_Col()
            BindData_Summary_Cooperation()

        End If


        'BindTop5Project()
        'BindTop5Component()
    End Sub

    Function Get_Cooperation_Master() As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "SELECT id,cptype_id,cptype_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationtype WHERE ACTIVE_STATUS='Y' Order by sort  "
        DT = SqlDB.ExecuteTable(SQL)

        Return DT
    End Function

    Private Sub Bind_Cooperation_Master_Col()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        rptCol.DataSource = DT_Cooperation
        rptCol.DataBind()


    End Sub

    Private Sub BindData_Summary_Cooperation()
        Dim DT As New DataTable
        Dim SQL As String = ""

        '------ข้อมูล Cooperation จาก Master เพื่อสร้างเป็น Collumn------
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()


        DT = SqlDB.ExecuteTable(SQL)
        Dim str_Field_SUM As String = ""

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  Select *, (" & str_Field_SUM & ")  SUM_Total FROM (  " & vbNewLine

        '----------------------------------------------------------
        SQL &= "  Select  ID, regionoda_name  " & vbNewLine
        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString() & ") SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString() & "          " & vbNewLine

            Next
        End If
        SQL &= "    FROM (   " & vbNewLine
        '---------------------------------------------------------

        SQL &= "  Select																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  --//---Loop เพื่อหา cooperation_type_id สร้าง คอลัมน์																				   " & vbNewLine

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,Case When cooperation_type_id =" & DT_Cooperation.Rows(i).Item("id") & " 																							 " & vbNewLine
                SQL &= "        Then SUM(SUM_Payment_Actual)																							 " & vbNewLine
                SQL &= "  	  Else 0																													 " & vbNewLine
                SQL &= "  	  End SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " 																									 " & vbNewLine

                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  																																 " & vbNewLine
        SQL &= "  FROM TB_Regionzoneoda																											 " & vbNewLine
        SQL &= "  LEFT JOIN 																													 " & vbNewLine
        SQL &= "  			 (																													 " & vbNewLine
        SQL &= "  			  Select vw_Activity.project_id																						 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.start_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.end_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.cooperation_type_id																				 " & vbNewLine
        SQL &= "  			  ,Data_Project_Recipience_RegionZone.*  FROM (																		 " & vbNewLine
        SQL &= "  			  --====== กิจกรรมและจำนวนเงิน ของ Region Zone ============															   " & vbNewLine
        SQL &= "  			  Select TB_Activity_Expense_Header.Activity_id, Data_Activity_Expense_Actual.* 									 " & vbNewLine
        SQL &= "  				  ,(Select dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')) node_id_OU 								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode(vw_ou.node_id ,'TB_OU_Country')) name_th_OU  								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode_eng(vw_ou.node_id ,'TB_OU_Country')) name_en_OU  							 " & vbNewLine
        SQL &= "  				  ,TB_OU_CountryZone.country_zone_id , TB_Regionzoneoda.regionoda_name											 " & vbNewLine
        SQL &= "  			  FROM (																											 " & vbNewLine
        SQL &= "  			  select Header_id,SUM(Pay_Amount_Actual) SUM_Payment_Actual   ,Recipience_id										 " & vbNewLine
        SQL &= "  			  from TB_Activity_Expense_Actual_Detail																			 " & vbNewLine
        SQL &= "  			  GROUP BY Header_id,Recipience_id																					 " & vbNewLine
        SQL &= "  			  ) AS Data_Activity_Expense_Actual																					 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Activity_Expense_Header ON TB_Activity_Expense_Header.id=Data_Activity_Expense_Actual.Header_id		 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  -- หา ผู้รับทุนอยู่ region zone อะไร																					  " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_ou ON Data_Activity_Expense_Actual.Recipience_id = vw_ou.node_id										 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_OU_CountryZone ON TB_OU_CountryZone.country_id =dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')			 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Regionzoneoda ON TB_Regionzoneoda.id = TB_OU_CountryZone.country_zone_id								 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  WHERE TB_Activity_Expense_Header.Activity_id IS NOT NULL 															 " & vbNewLine
        SQL &= "  			  ) AS Data_Project_Recipience_RegionZone 																			 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_Activity ON vw_Activity.activity_id = Data_Project_Recipience_RegionZone.Activity_id					 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_AllProject ON vw_AllProject.id =vw_Activity.project_id 												 " & vbNewLine
        SQL &= "  			  																													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  ) AS Data_Project_Recipience_RegionZone  ON Data_Project_Recipience_RegionZone.country_zone_id = TB_Regionzoneoda.id			 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  GROUP BY 																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name,cooperation_type_id														 " & vbNewLine

        SQL &= "  )AS TB_SUM  Group by id,regionoda_name  " & vbNewLine
        SQL &= "  )AS TB   " & vbNewLine
        SQL &= "  Order by  regionoda_name  " & vbNewLine


        If (DT_Cooperation.Rows.Count > 0) Then
            SQL &= "  "
        End If


        DT = SqlDB.ExecuteTable(SQL)
        Session("DT_Data_Summary_Cooperation") = DT

        BindTotal()
        BindPerTotal()

        rptList.DataSource = DT
        rptList.DataBind()





    End Sub

    Private Sub BindTotal()

        Dim DT As DataTable = Session("DT_Data_Summary_Cooperation")
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()

        Dim DT_Data As New DataTable
        Dim DR As DataRow

        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                DR = DT_Data.NewRow
                DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name") & ")", "")).ToString("#,##0.00")
                DT_Data.Rows.Add(DR)

            Next

        End If

        '---% row ของ sub-region        
        Dim myAL As New ArrayList()
        If (DT.Rows.Count > 0) Then
            For i As Integer = 0 To DT.Rows.Count - 1
                myAL.Add(Convert.ToDecimal(Convert.ToDecimal((DT.Rows(i).Item("SUM_Total") / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")))

            Next
        End If

        lbl_Footer_Tatal.Text = Convert.ToDecimal(DT.Compute("SUM(SUM_Total)", "")).ToString("#,##0.00")

        rptTotal.DataSource = DT_Data
        rptTotal.DataBind()

        '----bind Chart Sub-region----
        If (DT.Rows.Count > 0) Then
            Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())
            Chart_Sub_region.Series("Series_Sub_region").Points.DataBindY(YValue)
            For i As Integer = 0 To DT.Rows.Count - 1
                Dim Value_SUM_Pay_Amount_Actual As Double = DT.Rows(i).Item("SUM_Total")
                Chart_Sub_region.Series("Series_Sub_region").Points(i).LegendText = DT.Rows(i).Item("regionoda_name").ToString()
                Chart_Sub_region.Series("Series_Sub_region").Points(i).ToolTip = DT.Rows(i).Item("regionoda_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " ฿"
                If (Value_SUM_Pay_Amount_Actual > 0) Then
                    Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = YValue(i) & " % "
                Else
                    Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = ""
                End If

                Chart_Sub_region.Series("Series_Sub_region").YValueType = DataVisualization.Charting.ChartValueType.Auto

            Next

        End If



    End Sub

    Private Sub BindPerTotal()

        Dim DT As DataTable = Session("DT_Data_Summary_Cooperation")
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        Dim DT_Data As New DataTable
        Dim DR As DataRow
        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")
        DT_Data.Columns.Add("Amount_Actual")

        Dim myAL As New ArrayList()
        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                DR = DT_Data.NewRow
                DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name") & ")", "")) / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")
                DR("Amount_Actual") = Convert.ToDecimal((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name") & ")", ""))).ToString("#,##0.00")

                DT_Data.Rows.Add(DR)
                myAL.Add(Convert.ToDecimal(DR("SUM_Pay_Amount_Actual")))

            Next
        End If

        lbl_Per_Footer_Tatal.Text = Convert.ToDecimal(100).ToString("#,##0.00")
        rptPerTotal.DataSource = DT_Data
        rptPerTotal.DataBind()

        Dim Get_Color_Pie As String = ""
        '----bind Chart Programme----
        If (DT_Data.Rows.Count > 0) Then
            Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())
            'Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}
            ChartByProgramme.Series("Series1").Points.DataBindY(YValue)
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                Dim Value_SUM_Pay_Amount_Actual As Double = DT_Data.Rows(i).Item("SUM_Pay_Amount_Actual")
                ChartByProgramme.Series("Series1").Points(i).LegendText = DT_Cooperation.Rows(i).Item("cptype_name").ToString()
                ChartByProgramme.Series("Series1").Points(i).ToolTip = DT_Cooperation.Rows(i).Item("detail").ToString() & " : " & Convert.ToDecimal(DT_Data.Rows(i).Item("Amount_Actual")).ToString("#,##0.00") & " ฿"
                If (Value_SUM_Pay_Amount_Actual > 0) Then
                    ChartByProgramme.Series("Series1").Points(i).Label = DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " % "
                Else
                    ChartByProgramme.Series("Series1").Points(i).Label = ""
                End If

                ChartByProgramme.Series("Series1").YValueType = DataVisualization.Charting.ChartValueType.Auto




            Next

        End If


    End Sub







    Private Sub BindTop5Project()
        Dim dt As New DataTable
        dt = BL.GetCountProjectLastestFiveYear
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Script As String = ""
        Script &= " <script> $(function () {"
        Script &= """use strict"";"
        Script &= " var bar = new Morris.Bar({"
        Script &= "     element: 'bar-chart',"
        Script &= "     resize: true,"
        Script &= "     data: ["

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim y As String = dt.Rows(i)("stryear").ToString
            Dim a As String = dt.Rows(i)("a").ToString
            Dim b As String = dt.Rows(i)("b").ToString
            Dim c As String = dt.Rows(i)("c").ToString
            Dim d As String = dt.Rows(i)("d").ToString

            Script &= "         {y: '" & y & "', a: " & a & ", b: " & b & ",c: " & c & ",d:" & d & "}"
            If i < (dt.Rows.Count - 1) Then
                Script &= ","
            End If
        Next

        Script &= "     ],"
        Script &= "     barColors: ['#00c0ef', '#00a65a', '#f39c12', '#dd4b39'],"
        Script &= "     xkey: 'y',"
        Script &= "     ykeys: ['a', 'b','c','d'],"
        Script &= "     labels: ['Project', 'Non-Project','Loan','Contribuition'],"
        Script &= "     hideHover: 'auto'"
        Script &= "     });"
        Script &= " }); </script>"
        lblChart.Text = Script
    End Sub

    Private Sub BindTop5Component()
        Dim dt As New DataTable
        dt = BL.GetCountComponentProject()
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim TextHtml As String = "" ' "<div class='col-sm-12'>"

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim component_name As String = dt.Rows(i)("component_name").ToString.Replace(vbNullChar, "")
            Dim count_pj As String = dt.Rows(i)("count_pj").ToString
            Dim percent_pj As String = dt.Rows(i)("percent_pj").ToString

            TextHtml &= "  <div class='clearfix'>"
            TextHtml &= "     <span Class='pull-left'>" & i + 1 & "." & component_name & " </span>"
            TextHtml &= "     <p class='pull-right'>" & count_pj & " โครงการ</p>"
            TextHtml &= "  </div>"
            TextHtml &= "  <div class=""progress sm"">"
            TextHtml &= "        <div Class='progress-bar progress-bar-light-blue' style='width: " & percent_pj & "%;'></div>"
            TextHtml &= "  </div>"
        Next


        'TextHtml &= " </div>"

        lblComponent.Text = TextHtml
    End Sub



    Private Sub rptCol_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCol.ItemDataBound
        Dim lbl_cooperation_type_id As Label = e.Item.FindControl("lbl_cooperation_type_id")
        Dim lbl_cooperation_type_Name As Label = e.Item.FindControl("lbl_cooperation_type_Name")
        Dim th_radius_2 As HtmlTableCell = e.Item.FindControl("th_radius_2")

        Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}

        th_radius_2.Style("background-color") = ArrBrightPastel(e.Item.ItemIndex).ToString()
        th_radius_2.Style("border-radius") = "30px 30px 0px 0px"
        th_radius_2.Style("height") = "30px"

        lbl_cooperation_type_id.Text = e.Item.DataItem("id")
        lbl_cooperation_type_Name.Text = e.Item.DataItem("cptype_name").ToString()

    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim lbl_regionoda_name As Label = e.Item.FindControl("lbl_regionoda_name")
        Dim lblTotal_Row As Label = e.Item.FindControl("lblTotal_Row")

        lbl_regionoda_name.Text = e.Item.DataItem("regionoda_name").ToString()



        If Convert.IsDBNull(e.Item.DataItem("SUM_Total")) = False Then
            lblTotal_Row.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Total")).ToString("#,##0.00")
        End If

        Dim rptPayment As Repeater = e.Item.FindControl("rptPayment")
        AddHandler rptPayment.ItemDataBound, AddressOf rptPayment_ItemDataBound
        '-----แปลงข้อมูลแนวนอนเป็นแนวตั้ง -----
        Dim DT_Data_Summary As DataTable = Session("DT_Data_Summary_Cooperation")
        DT_Data_Summary.DefaultView.RowFilter = "id=" & e.Item.DataItem("id")
        DT_Data_Summary = DT_Data_Summary.DefaultView.ToTable()


        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        Dim DT_Data As New DataTable
        Dim DR As DataRow
        DT_Data.Columns.Add("Pay_Amount_Actual")

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                DR = DT_Data.NewRow
                DR("Pay_Amount_Actual") = DT_Data_Summary.Rows(0).Item("SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name"))
                DT_Data.Rows.Add(DR)
            Next

        End If

        rptPayment.DataSource = DT_Data
        rptPayment.DataBind()

    End Sub

    Protected Sub rptPayment_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim lbl_Pay_Amount_Actual As Label = e.Item.FindControl("lbl_Pay_Amount_Actual")

        '----สร้าง columl ตามจำนวนใน Master----
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        If (DT_Cooperation.Rows.Count > 0) Then

        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lbl_Pay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If
    End Sub



    '// Total
    Private Sub rptTotal_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTotal.ItemDataBound
        Dim lbl_Footer_SUM As Label = e.Item.FindControl("lbl_Footer_SUM")

        lbl_Footer_SUM.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Pay_Amount_Actual")).ToString("#,##0.00")

    End Sub


    '// %Total
    Private Sub rptPerTotal_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPerTotal.ItemDataBound
        Dim lbl_Per_Footer_SUM As Label = e.Item.FindControl("lbl_Per_Footer_SUM")

        lbl_Per_Footer_SUM.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Pay_Amount_Actual")).ToString("#,##0.00")

    End Sub


End Class
