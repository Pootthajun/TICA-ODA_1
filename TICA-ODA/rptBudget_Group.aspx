﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptBudget_Group.aspx.vb" Inherits="rptBudget_Group" %>


<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานการจัดสรรงบประมาณ </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายในองค์กร</a></li>
                <li class="active">รายงานการจัดสรรงบประมาณ</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;" id="gprint" runat ="server"  >
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top: 30px; */"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Report Type :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control select2" Style="width: 50%" AutoPostBack="true">
                                                        <asp:ListItem Value="01" Text="Project & Non Project" ></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="Loan & Contribuition"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6"  id="divSearchProject_23" runat="server">
                                                <label for="inputname" class="col-sm-3 control-label line-height">ค้นหา :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากชื่อโปรเจค" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-sm-6" id="divSearchDate_23" runat="server">
                                                <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">วันยืม :</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="เริ่มต้น" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label for="inputname" class="col-sm-2 control-label line-height">To</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุด" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row" id="divSearch_01" runat="server">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Year :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">งบประมาณ :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlBudgetGroup" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>

                                        </div>



                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                        <asp:Panel ID="pnlType_01" runat="server" Visible="True">

                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>รายการ</th>
                                                    <th>งบประมาณ<br />
                                                        (A)</th>
                                                    <th>จัดสรรแล้ว<br />
                                                        (B)</th>
                                                    <th>เบิกจ่าย<br />
                                                        (C)</th>
                                                    <th>คงเหลือ<br />
                                                        A-B</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trgroup_name" runat="server" style="background-color: ivory;">
                                                            <td data-title="Group name" colspan="5" style="text-align: left">
                                                                <b>
                                                                    <asp:Label ID="lblgroup_name" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>


                                                            <td data-title="รายการ" id="td1" runat="server" style="padding-left: 50px;">
                                                                <asp:Label ID="lblsub_name" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="งบประมาณ" style="text-align: right;" id="td2" runat="server">
                                                                <asp:Label ID="lblAmount_Budget" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="จัดสรรแล้ว" style="text-align: right;" id="td3" runat="server">
                                                                <asp:Label ID="lblAllocate_Budget" runat="server"></asp:Label></td>
                                                            <td data-title="เบิกจ่าย" style="text-align: right;" id="td4" runat="server">
                                                                <asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                            <td data-title="คงเหลือ" style="text-align: right;" id="td5" runat="server"><b>
                                                                <asp:Label ID="lblBalance" runat="server" ForeColor="black" ToolTip="งบประมาณ-จัดสรรแล้ว"></asp:Label></b>
                                                                <asp:Button ID="btnDrillDown" runat="server" ToolTip="ดูรายชื่อโครงการ" Style="display: none;" CommandName="select"></asp:Button>
                                                            </td>


                                                        </tr>
                                                        <asp:Panel ID="pnlProject" runat="server">
                                                            <asp:Repeater ID="rptProject" runat="server">
                                                                <ItemTemplate>
                                                                    <tr style="color: blue;">
                                                                        <td data-title="รายการ" id="tdLinkProject1" runat="server" style="padding-left: 100px;" colspan="2">
                                                                            <asp:Label ID="lblProject_Name" runat="server"></asp:Label></td>


                                                                        <td data-title="จัดสรรแล้ว" style="text-align: right;" id="tdLinkProject2" runat="server">
                                                                            <asp:Label ID="lblAllocate_Budget" runat="server"></asp:Label></td>
                                                                        <td data-title="เบิกจ่าย" style="text-align: right;" id="tdLinkProject3" runat="server">
                                                                            <asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                                        <td data-title="คงเหลือ" style="text-align: right;" id="tdLinkProject4" runat="server">
                                                                            <asp:Label ID="lblBalance" runat="server" ToolTip="จัดสรรแล้ว-เบิกจ่าย"></asp:Label>
                                                                            <a id="lnkSelect" runat="server" tooltip="ดูรายละเอียดของโครงการ" style="display: none;" target="_blank"></a>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="Total" style="text-align: center;"><b>Total </b></td>
                                                        <td data-title="Count_Project" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAmount_Budget_SUM" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Count_Component_Bachelor" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAllocate_Budget_SUM" runat="server"></asp:Label></b></td>
                                                        <td data-title="Count_Component_Training" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblPay_Amount_Actual_SUM" runat="server"></asp:Label></b></td>
                                                        <td data-title="Count_Component_Other" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBalance_Budget_SUM" runat="server" ForeColor="black"></asp:Label></b></td>

                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>
                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />
                                       
                                        <div class="row"></div>

                                    </div>
                                    <!-- /.box-body -->
                                    </asp:Panel>


                                    <div class="box-body">
                                        <asp:Panel ID="pnlType_23" runat="server" Visible="True">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr class="bg-gray">

                                                        <th>โครงการ</th>
                                                        <th>งบประมาณ<br />(A)</th>
                                                        <th>จัดสรรแล้ว<br />(B)</th>
                                                        <th>เบิกจ่าย<br />(C)</th>
                                                        <th>คงเหลือ<br />(A-B)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="RptList_Type_23" runat="server">
                                                        <ItemTemplate> 
                                                                        <tr>
                                                                            <td data-title="รายการ" id="tdLinkProject1" runat="server">
                                                                                <asp:Label ID="lblProject_Name" runat="server"></asp:Label></td>
                                                                            <td data-title="งบประมาณ" style="text-align: right;" id="tdLinkProject2" runat="server">
                                                                                <asp:Label ID="lblcommitment_budget" runat="server"></asp:Label></td>
                                                                            <td data-title="จัดสรรแล้ว" style="text-align: right;" id="tdLinkProject3" runat="server">
                                                                                <asp:Label ID="lblAllocate_Budget" runat="server"></asp:Label></td>
                                                                            <td data-title="เบิกจ่าย" style="text-align: right;" id="tdLinkProject4" runat="server">
                                                                                <asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                                            <td data-title="คงเหลือ" style="text-align: right;" id="tdLinkProject5" runat="server">
                                                                                <b><asp:Label ID="lblBalance" runat="server" ToolTip="งบประมาณ-จัดสรรแล้ว"></asp:Label></b>
                                                                                <a id="lnkSelect" runat="server" tooltip="ดูรายละเอียดของโครงการ" style="display: none;" target="_blank"></a>
                                                                            </td>

                                                                        </tr>
                                                            
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </tbody>
                                                <asp:Panel ID="pnlFooter_Type_23" runat="server" Visible="True">
                                                    <tfoot style="background-color: LemonChiffon;">
                                                        <tr id="tr1" runat="server">

                                                            <td data-title="Total" style="text-align: center;"><b>Total </b></td>
                                                            <td data-title="SUM_commitment_budget" style="text-align: right; text-decoration: underline;"><b>
                                                                <asp:Label ID="lblSUM_commitment_budget" runat="server"></asp:Label></b></td>
                                                            <td data-title="SUM_Pay_Amount_Plan" style="text-align: right; text-decoration: underline;"><b>
                                                                <asp:Label ID="lblSUM_Pay_Amount_Plan_Type_23" runat="server"></asp:Label></b></td>
                                                            <td data-title="SUM_Pay_Amount_Actual" style="text-align: right; text-decoration: underline;"><b>
                                                                <asp:Label ID="lblSUM_Pay_Amount_Actual_Type_23" runat="server"></asp:Label></b></td>
                                                            <td data-title="SUM_Balance" style="text-align: right; text-decoration: underline;"><b>
                                                                <asp:Label ID="lblSUM_Balance_Type_23" runat="server" ForeColor="black"></asp:Label></b></td>

                                                        </tr>


                                                    </tfoot>
                                                </asp:Panel>
                                            </table>

                                            <uc1:PageNavigation runat="server" ID="Pager_Type23" PageSize="20" />

                                        </asp:Panel>
                                        <div class="row"></div>

                                    </div>

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


</asp:Content>






