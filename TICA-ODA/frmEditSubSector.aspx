﻿<%@ Page Title="" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditSubSector.aspx.vb" Inherits="frmEditSubSector" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Sub Sector | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Sub Sector (สาขาย่อย)</h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
                <li><a href="frmSubSector.aspx">Sub Sector</a></li>
                <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label>
                    Sub Sector</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <br />
            <!-- START CUSTOM TABS -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border bg-blue-gradient">
                            <h4 class="box-title">
                                <asp:Label ID="lblEditMode2" runat="server" Text=""></asp:Label>
                                Sub Sector</h4>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="district" class="col-sm-3 control-label">Sector/Purpose<br />
                                        (สาขาย่อย/วัตถุประสงค์) : <span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="255"></asp:TextBox>
                                    </div>
                                </div>

                                <br /><br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="inputLevel" class="col-sm-3 control-label">Sector<br />                                        
                                        (สาขา) : <span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlPurposeCategory" runat="server" CssClass="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <br /><br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="DAC5code" class="col-sm-3 control-label line-height">DAC5code :</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtDAC5code" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                    </div>
                                </div>

                                <br /><br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="CRScode" class="col-sm-3 control-label line-height">CRScode :</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtCRScode" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                    </div>
                                </div>

                                <br /><br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="inputname" class="col-sm-3 control-label">Description (รายละเอียด) :</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px" MaxLength="255"></asp:TextBox>
                                    </div>
                                </div>

                                <br /><br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="inputname" class="col-sm-3 control-label">Active Status :</label>
                                    <div class="col-sm-9">
                                        <label>
                                            <asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" Checked="true" /></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <div class="col-sm-8"></div>
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                                 <i class="fa fa-save"></i> Save
                                    </asp:LinkButton>
                                </div>
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                                <i class="fa fa-reply"></i> Cancel
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

        </section>
        <!-- /.content -->
    </div>


    <!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
