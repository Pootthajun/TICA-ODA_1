﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptCLMV.aspx.vb" Inherits="rptCLMV" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Search CLMV </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">Search CLMV</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white"  DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                   
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top:30px;"></div>



                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Budget Year :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                    </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Detail :</label>
                                                 <div class="col-sm-9 ">
                                                     <div class="col-sm-4" style="margin-left:-20px;">
                                                     <label class="input-group"><asp:CheckBox ID="ckShowActivity" runat="server" CssClass="minimal" AutoPostBack ="true"   />
                                                         <span style=" margin-left:10px;"></span></label>
                                                     </div>
                                                      
                                                            <div class="col-sm-4">
                                                         
                                                    </div>
                                                 </div>
                                            </div>

                                        </div>
                                        <asp:Panel ID="pnlSearchActivity" runat ="server" >

                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Project Type :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlProject_Type" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                         <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                                                         <asp:ListItem Value="0">Project</asp:ListItem>
                                                         <asp:ListItem Value="1">Non Project</asp:ListItem>
                                                         <asp:ListItem Value="2">Loan</asp:ListItem>
                                                         <asp:ListItem Value="3">Contribuition</asp:ListItem>

                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">โครงการ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากชื่อโครงการ" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        </asp:Panel>
                                        
                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>

                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no"  id="th1" runat="server" visible="false">No.<br/>(ลำดับ)</th>

                                                    <th>Budget Year<br/>(ปีงบประมาณ)</th>
                                                    <th id="tdHeader" runat="server" >Project Name<br/>(โครงการ)</th>
                                                    <th>Cambodia<br/>(กัมพูชา)</th>
                                                    <th>Laos<br/>(สปป. ลาว)</th>
                                                    <th>Myanmar<br/>(เมียนมา)</th>
                                                    <th>Vietnam<br/>(เวียดนาม)</th>
                                                    <th>Total Amount<br/>(รวม)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="No" class="center" id="tr1" runat="server" visible="false">
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>

                                                            <td data-title="Budget Year" style="text-align :center ;"  id="tdLinkProject1" runat="server" tooltip="ดูรายละเอียดของโครงการ"><asp:Label ID="lblBudget_Year" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Activity" style="text-align :left  ; "  id="tdLinkProject2" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" ><asp:Label ID="lblProject_Name" runat="server" ForeColor="black"></asp:Label></td>
                                                            
                                                            <td data-title="Cambodia"  style="text-align :right ; "  id="tdLinkProject3" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" ><asp:Label ID="lblCambodia_Disbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Laos"  style="text-align :right ; "  id="tdLinkProject4" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" ><asp:Label ID="lblLaos_Disbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Myanmar" style="text-align :right ; "  id="tdLinkProject5" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" ><asp:Label ID="lblMyanmar_Disbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Vietnam" style="text-align :right ; "  id="tdLinkProject6" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" ><asp:Label ID="lblVietnam_Disbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Total Amount" style="text-align :right ;background-color:LemonChiffon;text-decoration: underline; "  id="tdLinkProject7" runat ="server"  tooltip="ดูรายละเอียดของโครงการ" >
                                                                <b><asp:Label ID="lblTotal_Amount" runat="server" ForeColor="black" ></asp:Label></b>
                                                                <a  ID="lnkSelect" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>
                                                        
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                    <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >

                                            <td data-title="No" class="center" id="tr1" runat="server" visible="false">
                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                            <td id="tdfooter1" runat="server"  data-title="Budget Year" style="text-align :center ;" colspan ="1" ><b>Total</b></td>
                                            <td id="tdfooter2" runat="server"  data-title="Budget Year" style="text-align :center ;" colspan ="2" ><b>Total</b></td>                
                                            <td data-title="Cambodia"  style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblCambodia_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Laos"  style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblLaos_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Myanmar" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblMyanmar_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Vietnam" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblVietnam_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblTotal_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                      

										</tr>
                                    

                                        </tfoot>
                                    </asp:Panel> 

                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>




