﻿<%@ Page Title="" enableEventValidation="false" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditComponent.aspx.vb" Inherits="frmEditComponent" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Add Component | ODA</title>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Component (องค์ประกอบ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
            <li><a href="frmComponent.aspx"> Component</a></li>
            <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> Component</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title"><asp:Label ID="lblEditMode2" runat="server" Text=""></asp:Label> Component</h4>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                   
                   <div class="form-group">
                      <label for="district" class="col-sm-2 control-label">Component 
                          <br /> (องค์ประกอบ) : <span style="color:red">*</span> </label>
                      <div class="col-sm-4">                   
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                    </div>  
                  </div>
 
                   <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Description <br /> (รายละเอียด) :</label>
                      <div class="col-sm-9">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px"></asp:TextBox>
                      </div>
                    </div>

                  <div class="clearfix"></div>

                  <div class="form-group">
                      <label class="col-sm-2 control-label">การจัดสรร งปม. :</label>
                      
                      <div class="col-sm-4">
                        <!-- radio -->
                      <div class="form-group">
                        
                        <label>
                           <asp:RadioButton ID="rdiDisGroup" runat="server" CssClass="minimal" GroupName="chkDisburse"  Checked ="true"/>
                            Group
                        </label>

                        <label>       
                            <asp:RadioButton ID="rdiDisPeople" runat="server" CssClass="minimal" GroupName="chkDisburse"/>
                            People
                        </label>
                        
                      </div>

                      </div>

                      <div class="clearfix"></div>
                      <label class="col-sm-2 control-label">Payment (การจ่ายเงิน) :</label>
                      
                      <div class="col-sm-4">
                        <!-- radio -->
                      <div class="form-group">
                        <label>
                            <asp:RadioButton ID="rdiPayGroup" runat="server" CssClass="minimal" GroupName="chkPayment" Checked ="true" />
                            Group
                        </label>

                        <label>
                            <asp:RadioButton ID="rdiPayPeople" runat="server" CssClass="minimal" GroupName="chkPayment"/>
                            People
                        </label>                        
                      </div>

                    </div>   
                   
                 </div>

                 
                   <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                      <div class="col-sm-9">
                     <label><asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" checked="true"/></label>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                      <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                                 <i class="fa fa-save"></i> Save
                            </asp:LinkButton>
                    </div>
                    <div class="col-sm-2">
                       <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                                <i class="fa fa-reply"></i> Cancel
                            </asp:LinkButton>
                    </div>
                        
                    </div>
                </form>
                 </div>
              </div><!-- /.box -->
            </div><!-- /.col -->

        </section>
        <!-- /.content -->
    </div>
    
<!----------------Page Advance----------------------> 

    <uc1:frmScriptAdvance runat="server" id="frmScriptAdvance" />

</asp:Content>

