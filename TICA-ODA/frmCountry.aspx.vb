﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmCountry
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        BindData()



    End Sub
    Protected Sub BindData()
        'Dim sql As String = "SELECT web_country.id,countrygroup,web_countrygroup.countrygroup_name,country"
        'sql += ",region_id, web_region.region_name,web_countrytype.CountryTypeName,regionzoneoda,regionoda_name "
        'sql += "FROM web_country "
        'sql += " INNER Join web_countrygroup On web_countrygroup.id=web_country.countrygroup"
        'sql += " Left JOIN web_countrytype On web_countrytype.id=web_country.countrytype"
        'sql += " Left JOIN web_region On web_region.id=web_country.region_id"
        'sql += " Left JOIN web_regionzoneoda On web_regionzoneoda.id=web_country.regionzoneoda WHERE 1=1"

        'Dim trans As New TransactionDB
        'Dim lnq As New WebCountryLinqDB
        'Dim p(1) As SqlParameter

        'p(0) = SqlDB.SetBigInt("@_ID", 1)
        'Dim dt As DataTable = lnq.GetListBySql(sql, trans.Trans, p)

        'Dim Filter As String = ""
        'If txtSearch.Text <> "" Then
        '    Filter += "country Like '%" + txtSearch.Text.Trim() + "%' OR CountryTypeName Like '%" + txtSearch.Text.Trim() + "%' OR countrygroup_name Like '%" + txtSearch.Text.Trim() + "%'"
        '    Filter += " OR regionoda_name Like '%" + txtSearch.Text.Trim() + "%'"
        'End If
        'If dt.Rows.Count > 0 Then
        '    trans.CommitTransaction()
        '    dt.DefaultView.RowFilter = Filter
        '    rptList.DataSource = dt.DefaultView
        '    rptList.DataBind()
        '    lblCount.Text = dt.DefaultView.Count()
        'Else
        '    trans.RollbackTransaction()
        '    Dim err = lnq.ErrorMessage()
        'End If
        'lnq = Nothing
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)

        Response.Redirect("frmEditCountry.aspx")

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        BindData()
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblCountryName As Label = DirectCast(e.Item.FindControl("lblCountryName"), Label)
        Dim lblCountryGroup As Label = DirectCast(e.Item.FindControl("lblCountryGroup"), Label)
        Dim lblCountryType As Label = DirectCast(e.Item.FindControl("lblCountryType"), Label)
        Dim lblRegionZoneODA As Label = DirectCast(e.Item.FindControl("lblRegionZoneODA"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

        lblCountryName.Text = drv("country").ToString()
        lblCountryGroup.Text = drv("countrygroup_name").ToString()
        lblCountryType.Text = drv("CountryTypeName").ToString()
        lblRegionZoneODA.Text = drv("regionoda_name").ToString()
    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        'If e.CommandName = "cmdEdit" Then
        '    Session("SESSION_ID") = e.CommandArgument
        '    Response.Redirect("frmEditCountry.aspx")
        'End If

        'If e.CommandName = "cmdDelete" Then
        '    Dim tran As New TransactionDB
        '    Dim lnq As New WebCountryLinqDB

        '    Dim ret As ExecuteDataInfo = lnq.DeleteByPK(e.CommandArgument, tran.Trans)
        '    If ret.IsSuccess = True Then
        '        tran.CommitTransaction()
        '        Alert("ลบข้อมูลเรียบร้อยแล้ว")
        '    Else
        '        tran.RollbackTransaction()
        '        Alert("ไม่สามารถลบข้อมูลได้")
        '        Dim _err As String = tran.ErrorMessage()
        '    End If
        '    lnq = Nothing
        'End If
        'BindData()
    End Sub
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class