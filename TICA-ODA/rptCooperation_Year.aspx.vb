﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptCooperation_Year
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptCooperation_Year_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_1")
            a.Attributes.Add("style", "color:#FF8000")


            BL.Bind_DDL_Coperationtype(ddlCoperation_Type)
            ddlCoperation_Type.SelectedValue = 32  '--fix ไว้สำหรับรายงาน  Trilateral  ไตรภาคี

            BindList()
            btnSearch_Click(Nothing, Nothing)
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable
        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""
            sql &= "   SELECT * FROM VW_Activity_Cooperation "
            sql &= "     where activity_id IS NOT NULL																	 " + Environment.NewLine

            If (ddlCoperation_Type.SelectedIndex > 0) Then
                filter &= " cooperation_type_id =" & ddlCoperation_Type.SelectedValue & "  AND " + Environment.NewLine
                Title += " ประเภทความร่วมมือ : " & ddlCoperation_Type.SelectedItem.ToString
            End If

            If (txtSearch_OU.Text <> "") Then
                filter &= "  ORG_List Like '%" & txtSearch_OU.Text & "%'  AND " + Environment.NewLine
                Title += " ประเทศ / แหล่งคู่ร่วมมือ : " & txtSearch_OU.Text
            End If

            If (txtSearch_Activity.Text <> "") Then
                filter &= "  activity_name Like '%" & txtSearch_Activity.Text & "%'  AND " + Environment.NewLine
                Title += " ในกิจกรรม : " & txtSearch_Activity.Text
            End If

            If (txtSearch_Aid_Type.Text <> "") Then
                filter &= "  Aid_Type_Name Like '%" & txtSearch_Aid_Type.Text & "%'  AND " + Environment.NewLine
                Title += " ประเภท : " & txtSearch_Aid_Type.Text
            End If

            If (txtSearch_Country.Text <> "") Then
                filter &= "  Recipient_List Like '%" & txtSearch_Country.Text & "%'  AND " + Environment.NewLine
                Title += " ประเทศผู้รับทุน : " & txtSearch_Country.Text
            End If


            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) & vbLf
            End If

            DT = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Cooperation_Year_Title") = lblTotalRecord.Text
            Lastfunding_agency = ""

        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()
        Dim DT As DataTable = GetList()
        If (DT.Rows.Count > 0) Then
            lblAmount_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            lblQTY_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Recipient_Count)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If
        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Cooperation_Year") = DT

        Pager.SesssionSourceName = "Search_Cooperation_Year"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Dim Lastfunding_agency As String = ""


    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If


        Dim lblfunding_agency_TH As Label = DirectCast(e.Item.FindControl("lblfunding_agency_TH"), Label)
        Dim lblSeq As Label = DirectCast(e.Item.FindControl("lblSeq"), Label)
        Dim lblActivity_Name As Label = DirectCast(e.Item.FindControl("lblActivity_Name"), Label)
        Dim lbl_Aid_Type_Name As Label = DirectCast(e.Item.FindControl("lbl_Aid_Type_Name"), Label)
        Dim lblRecipience_List As Label = DirectCast(e.Item.FindControl("lblRecipience_List"), Label)

        Dim lblQTY_Recipience As Label = DirectCast(e.Item.FindControl("lblQTY_Recipience"), Label)
        Dim lblAmount As Label = DirectCast(e.Item.FindControl("lblAmount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")

        '--------------------

        lblfunding_agency_TH.Text = e.Item.DataItem("ORG_List").ToString
        lblSeq.Text = e.Item.ItemIndex + 1
        lblActivity_Name.Text = e.Item.DataItem("activity_name").ToString
        lbl_Aid_Type_Name.Text = e.Item.DataItem("Aid_Type_Name").ToString

        lblRecipience_List.Text = e.Item.DataItem("Recipient_List").ToString

        If Not IsDBNull(e.Item.DataItem("Recipient_Count")) Then
            lblQTY_Recipience.Text = GL.ConvertCINT(e.Item.DataItem("Recipient_Count"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblAmount.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        Dim lnkSelectProject As HtmlAnchor = e.Item.FindControl("lnkSelectProject")
        For i As Integer = 1 To 7
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & lnkSelectProject.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Or (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelectProject.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelectProject.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelectProject.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        End If
    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCooperation_Year.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCooperation_Year.aspx?Mode=EXCEL');", True)
    End Sub

#End Region



End Class

