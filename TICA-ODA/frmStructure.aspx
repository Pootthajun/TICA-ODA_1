﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmStructure.aspx.vb" Inherits="frmStructure" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/frmModalStructure.ascx" TagPrefix="uc1" TagName="frmChart" %>
<%@ Register Src="~/usercontrol/UCFormOUPerson.ascx" TagPrefix="uc1" TagName="UCFormOUPerson" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Organization Structure | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Organization Structure (โครงสร้างองค์กร)
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-users"></i> Organization Structure</li>    
            </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <!-- Main content -->

                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>
                                        <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                            <i class="fa fa-plus"></i>Add Country
                                        </asp:LinkButton>
                                    </p>
                                </div>
                                <asp:TextBox ID="txtFilterToolIDs" runat ="server" AutoPostBack ="true"  Visible ="false"  ></asp:TextBox>
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-gray">
                                            <th>โครงสร้างองค์กร</th>
                                        </tr>
                                        <asp:TreeView ID="TRV_OU" runat="server" CssClass="btn-defult text-blue">
                                        </asp:TreeView>
                                    </table>
                                    <asp:LinkButton ID="btndelete" OnClick="btnDelete_Click" runat="server" Text="ลบ" Style="display: none" />
                                    <asp:LinkButton ID="btndeleteOrganize" runat="server" Text="ลบ" Style="display: none" />
                                    <asp:LinkButton ID="btndeletePerson" runat="server" Text="ลบ" Style="display: none" />
                                    <asp:TextBox ID="txtNodeID" runat="server" Style="display: none"></asp:TextBox>

                                    <asp:TextBox ID="txtstrEditAgenct" runat="server" Style="display: none"></asp:TextBox>
                                    <asp:TextBox ID="txtstrEditPerson" runat="server" Style="display: none"></asp:TextBox>
                                    <asp:TextBox ID="txtparentAgencyID" runat="server" Style="display: none"></asp:TextBox>
                                    <asp:TextBox ID="txtparentAgency" runat="server" Style="display: none"></asp:TextBox>

                                    <asp:Button ID="btnSetDataCountry" runat="server" Text="Button" Style="display: none" />
                                    
                                    <asp:Button ID="btnSetDataCountry1" runat="server" Text="Button" Style="display: none" />

                                    <asp:Button ID="btnGetCountryID" runat="server" Text="Button" Style="display: none" />
                                    <asp:Button ID="btnGetOrganizeID" runat="server" Text="Button" Style="display: none" />

                                    <asp:Button ID="btnGetAgencyToEdit" runat="server" Text="Button" Style="display: none" />                                    
                                    <asp:Button ID="btnGetAgencyToView" runat="server" Text="Button" Style="display: none" />

                                    <asp:Button ID="btnGetPersonToEdit" runat="server" Text="Button" Style="display: none" />
                                    <asp:Button ID="btnGetPersonToView" runat="server" Text="Button" Style="display: none" />

                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.col -->
                        </div>

                    </div>
                    <!-- /.row -->
                </section>

                <asp:Panel CssClass="modal" ID="PNLmyModalCountry" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Country</h4>
                            </div>
                            <div class="modal-body">
                                <div class="box-body">
                                    <table class="table table-bordered">

                                        <tr class="bg-info">
                                            <td colspan="2">
                                                <h4 class="box-title"><a href="#">
                                                    <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label></a></h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">ชื่อประเทศ :</b></th>
                                            <td>
                                                <div class="col-sm-12">

                                                    <asp:TextBox ID="txtCountryNameTH" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาไทย" placeholder=""></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Country Name :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                <asp:TextBox ID="txtCountryNameEN" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาอังกฤษ" placeholder=""></asp:TextBox>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Description<br />
                                                (รายละเอียด) :</b></th>
                                            <td>
                                                <div class="col-sm-12">

                                                    <asp:TextBox ID="txtDescriptionCountry" runat="server" TextMode="MultiLine" Width="350px"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right"><span style="color: red;">*</span>Region OECD<br />
                                                (ภูมิภาค) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DDL_CountryRegion" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right"><span style="color: red;">*</span>Country Group<br />
                                                (กลุ่มประเทศ) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DDL_CountryGroup" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right"><span style="color: red;">*</span>Country Zone<br />
                                                (โซนประเทศ) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DDL_CountryZone" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <label>

                                                        <asp:CheckBox ID="chkACTIVE_STATUS_Country" CssClass="minimal-red" runat="server" checked="true" />
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="modal-footer">

                                    <asp:LinkButton ID="btnSave_Country" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel_Country" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel CssClass="modal" ID="PNLmyModalViewCountry" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">  
                            <div class="modal-header">
                                <h4 class="modal-title">Country</h4>
                            </div>
                            <div class="modal-body">
                                <div class="box-body">
                                    <table class="table table-bordered">

                                        <tr class="bg-info">
                                            <td colspan="2">
                                                <h4 class="box-title"><a href="#">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label></a></h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">ชื่อประเทศ :</b></th>
                                            <td>
                                                <div class="col-sm-12">

                                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาไทย" placeholder=""></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Country Name :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาอังกฤษ" placeholder=""></asp:TextBox>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Description<br />
                                                (รายละเอียด) :</b></th>
                                            <td>
                                                <div class="col-sm-12">

                                                    <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Width="350px"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Region OECD<br />
                                                (ภูมิภาค) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Country Group<br />
                                                (กลุ่มประเทศ) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Country Zone<br />
                                                (โซนประเทศ) :</b></th>
                                            <td class="text-primary">
                                                <div class="col-sm-12">

                                                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <!-- /.form-group -->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <label>

                                                        <asp:CheckBox ID="CheckBox1" CssClass="minimal-red" runat="server" checked="true" />
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="modal-footer">

                                    <asp:LinkButton ID="BtnCancelCountry" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel CssClass="modal" ID="PNLmyModalAgency" runat="server" ScrollBars="Auto" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">                               
                                <h4 class="modal-title">Agency</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">

                                    <tr class="bg-info">
                                        <td colspan="2">
                                            <h4 class="box-title"><a href="#">
                                                <asp:Label ID="lblAgencyCountryName" runat="server" Text=""></asp:Label>
                                            </a>
                                                <asp:Label ID="lblAgencyOrganizeName" runat="server" Text=""></asp:Label></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">ชื่อหน่วยงาน : <span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>

                                                    <asp:TextBox ID="txtAgencyNameTH" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาไทย" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">ชื่อย่อ :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>

                                                    <asp:TextBox ID="txtAgencyABBRNameTH" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่ย่ออภาษาไทย" placeholder="" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Agency Name :<span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtAgencyNameEN" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาอังกฤษ" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Inc. :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtAgencyABBRNameEN" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อย่อภาษาอังกฤษ" placeholder="" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Location (ที่ตั้ง) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtAgencyLocation" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtAgencyTelephone" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Fax. (โทรสาร) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-fax"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtAgencyFax" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Website :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtAgencyWebsite" runat="server" Width="350px"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Email :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtAgencyEmail" runat="server" Width="350px" MaxLength="100"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Style="color: red"
                                                    ControlToValidate="txtAgencyEmail" Display="Dynamic"
                                                    ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง!"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                                                </asp:RegularExpressionValidator>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtAgencyNote" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Agency Type (ประเภทหน่วยงาน) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <label>
                                                    <asp:CheckBox ID="chkAgency_FundingAgency" CssClass="minimal" runat="server" />
                                                    Funding Agency
                                                </label>
                                                <br />
                                                <label>
                                                    <asp:CheckBox ID="chkAgency_ExecutingAgency" CssClass="minimal" runat="server" />
                                                    Executing Agency
                                                </label>
                                                <br />
                                                <label>
                                                    <asp:CheckBox ID="chkAgency_ImplementingAgency" CssClass="minimal" runat="server" />
                                                    Implementing Agency 
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <label>
                                                    <asp:CheckBox ID="chkACTIVE_STATUS_Agency" CssClass="minimal-red" runat="server" checked="true"/>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">



                                <asp:LinkButton ID="btnAgencySave" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnAgencyCancel" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>

                            </div>
                        </div>

                    </div>
                </asp:Panel>

                <asp:Panel CssClass="modal" ID="PNLmyModalAgencyView" runat="server" ScrollBars="Auto" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">                               
                                <h4 class="modal-title">Agency</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">

                                    <tr class="bg-info">
                                        <td colspan="2">
                                            <h4 class="box-title"><a href="#">
                                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                            </a>
                                                <asp:Label ID="Label3" runat="server" Text=""></asp:Label></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">ชื่อหน่วยงาน : <span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>

                                                    <asp:TextBox ID="txtViewAgencyNameTh" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาไทย" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">ชื่อย่อ :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>

                                                    <asp:TextBox ID="txtViewAgencyAbbrNameTh" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่ย่ออภาษาไทย" placeholder="" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Agency Name :<span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtViewAgencyNameEn" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อภาษาอังกฤษ" placeholder=""></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Inc. :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtViewAgencyAbbrNameEn" runat="server" CssClass="form-control" data-toggle="tooltip" title="ชื่อย่อภาษาอังกฤษ" placeholder="" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Location (ที่ตั้ง) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txtViewAgencyAddress" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtViewAgencyTelNo" CssClass="form-control" runat="server" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Fax. (โทรสาร) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-fax"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtViewAgencyFaxNo" CssClass="form-control" runat="server" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Website :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtViewAgencyWebSite" runat="server" Width="350px"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Email :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtViewAgencyEmail" runat="server" Width="350px" MaxLength="100"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Style="color: red"
                                                    ControlToValidate="txtViewAgencyEmail" Display="Dynamic"
                                                    ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง!"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                                                </asp:RegularExpressionValidator>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="txtViewAgencyNote" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Agency Type (ประเภทหน่วยงาน) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <label>
                                                    <asp:CheckBox ID="chkViewAgencyTypeFunding" CssClass="minimal" runat="server" />
                                                    Funding Agency
                                                </label>
                                                <br />
                                                <label>
                                                    <asp:CheckBox ID="chkViewAgencyTypeExecuting" CssClass="minimal" runat="server" />
                                                    Executing Agency
                                                </label>
                                                <br />
                                                <label>
                                                    <asp:CheckBox ID="chkViewAgencyTypeImplementing" CssClass="minimal" runat="server" />
                                                    Implementing Agency 
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <label>
                                                    <asp:CheckBox ID="chkViewAgencyActiveStatus" CssClass="minimal-red" runat="server" checked="true"/>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="btnAgencyCancel1" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>

                            </div>
                        </div>

                    </div>
                </asp:Panel>

                <asp:Panel CssClass="modal" ID="PNLmyModalPerson" runat="server" ScrollBars="Auto" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Person</h4>
                            </div>
                            <div class="modal-body">
                                <uc1:UCFormOUPerson runat="server" ID="UCFormOUPerson" />
                            </div>
                            <div class="modal-footer">

                                <asp:LinkButton ID="btnPersonSave" runat="server" CssClass="btn  btn-social btn-success">
                                    <i class="fa fa-save"></i> Save
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnPersonCancel" runat="server" CssClass="btn  btn-social btn-google">
                                    <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel CssClass="modal" ID="PNLmyModalPersonView" runat="server" ScrollBars="Auto" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">                                
                                <h4 class="modal-title">Person</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <tr class="bg-info">
                                        <td colspan="2">
                                            <h5 class="box-title"><a href="#">
                                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                                            </a><a href="#">
                                                <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                            </a>
                                                <asp:Label ID="Label6" runat="server" Text=""></asp:Label></h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย : <span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </div>

                                                    <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Firstname - Lastname : <span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <asp:TextBox ID="TextBox15" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Id Card : <span style="color:red">*</span></b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-credit-card"></i>
                                                    </div>
                                                    <asp:TextBox ID="TextBox16" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                        <tr>
                                        <th style="width: 180px"><b class="pull-right">Passport Id :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-credit-card"></i>
                                                    </div>
                                                    <asp:TextBox ID="TextBox17" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <%--<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                    <asp:TextBox ID="txtPersonBirthDay" CssClass="form-control" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'" MaxLength="8"></asp:TextBox>--%>

                                                    <asp:TextBox CssClass="form-control m-b" ID="TextBox18" runat="server" placeholder="01/01/2016"></asp:TextBox>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <asp:TextBox ID="TextBox19" CssClass="form-control" runat="server" MaxLength="9"></asp:TextBox>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </div>
                                                    <asp:TextBox ID="TextBox20" CssClass="form-control" runat="server"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                                        <td>
                                            <div class="col-sm-12">

                                                <asp:TextBox ID="TextBox21" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                                        <td>
                                            <div class="col-sm-12">
                                                <label>

                                                    <asp:CheckBox ID="CheckBox6" runat="server" checked="true"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">

                                <asp:LinkButton ID="btnPersonViewCancel" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />


    <script type="text/javascript">


        function fndelete(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btndelete.ClientID %>').click();
            var str = strNODE_ID;

        }

        function fndeleteorganize(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btndeleteOrganize.ClientID %>').click();
            var str = strNODE_ID;

        }

        function fndeleteperson(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btndeletePerson.ClientID %>').click();
            var str = strNODE_ID;

        }


        function fnBindCountry(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btnSetDataCountry.ClientID %>').click();


        }

        function fnBindCountry1(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btnSetDataCountry1.ClientID %>').click();


        }


        function fnBindAgency(strNODE_ID, strParentNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btnGetCountryID.ClientID %>').click();


        }
        function fnBindAgencyToEdit(strNODE_ID, strParentNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= txtstrEditAgenct.ClientID %>').value = "EDIT";
            document.getElementById('<%= txtparentAgencyID.ClientID %>').value = strParentNODE_ID;

            document.getElementById('<%= btnGetAgencyToEdit.ClientID %>').click();


        }

        function fnBindAgencyToView(strNODE_ID, strParentNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= txtstrEditAgenct.ClientID %>').value = "EDIT";
            document.getElementById('<%= txtparentAgencyID.ClientID %>').value = strParentNODE_ID;

            document.getElementById('<%= btnGetAgencyToView.ClientID %>').click();


        }

        function fnBindPersonToEdit(strNODE_ID, strParentNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= txtstrEditPerson.ClientID %>').value = "EDIT";
            document.getElementById('<%= txtparentAgency.ClientID %>').value = strParentNODE_ID;
            document.getElementById('<%= btnGetPersonToEdit.ClientID %>').click();


        }

        function fnBindPersonToView(strNODE_ID, strParentNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= txtstrEditPerson.ClientID %>').value = "EDIT";
            document.getElementById('<%= txtparentAgency.ClientID %>').value = strParentNODE_ID;
            document.getElementById('<%= btnGetPersonToView.ClientID %>').click();


        }

        function fnBindOrganize(strNODE_ID) {

            document.getElementById('<%= txtNodeID.ClientID %>').value = strNODE_ID;
            document.getElementById('<%= btnGetOrganizeID.ClientID %>').click();


        }


    </script>
</asp:Content>

