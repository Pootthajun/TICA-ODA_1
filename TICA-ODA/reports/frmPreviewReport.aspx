﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPreviewReport.aspx.vb" Inherits="reports_frmPreviewReport" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" DisplayToolbar="True" SeparatePages="True" AutoDataBind="true" 
        EnableDatabaseLogonPrompt="False" EnableDrillDown="False" EnableParameterPrompt="False" HasPrintButton="True" 
        HasDrillUpButton="False" HasCrystalLogo="False" HasRefreshButton="False" PrintMode="Pdf" HasExportButton="True"
        HasSearchButton="False" HasToggleGroupTreeButton="True" />
       
       
       
       
        <center>
            <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
        </center>
        
        
    </div>
    </form>
</body>
</html>
