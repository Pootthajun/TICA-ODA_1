﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmTemplate.aspx.vb" Inherits="frmTemplate" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Template Form | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Template Form (รูปแบบ)</h1>
            <ol class="breadcrumb">
                <li><a href="frmProjectExpense.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
                <li class="active">Template Form</li>
            </ol>
        </section>

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" min-Height="610px;">

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12 form-horizontal">

                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                                <i class="fa fa-plus"></i>Add Template Form
                                            </asp:LinkButton>
                                        </p>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ</h4>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-xs-6 col-sm-10 ">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อ Template (รูปแบบ)" CssClass="form-control"></asp:TextBox>
                                            </div>

                                            <div class="col-xs-6 col-sm-2">
                                                <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>Template (รูปแบบ)</th>
                                                    <th class="tools">Edit (แก้ไข)</th>
                                                    <th id="HeadColDelete" runat="server" class="tools">Delete (ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Template">
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblTemplate" runat="server"></asp:Label></td>
                                                            <td data-title="Edit" id="ColEdit" runat="server">
                                                                <center>
                                                                    <asp:LinkButton ID="btnEdit" runat="server"   CommandName="Edit" CommandArgument='<%# Eval("id") %>'>
                                                                        <i class="fa fa-edit text-success"></i> Edit </asp:LinkButton>
                                                                </center>
                                                            </td>
                                                            <td data-title="Delete" id="ColDelete" runat="server">
                                                                <center>
                                                                  <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' 
                                                                    CommandArgument='<%# Eval("id") %>' CommandName="Delete"><i class="fa fa-trash text-danger"></i> Delete</asp:LinkButton>
                                                                </center>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
