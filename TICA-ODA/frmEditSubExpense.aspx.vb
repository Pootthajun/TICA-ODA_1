﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Partial Class frmEditSubExpense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditSubExpenseID As Long
        Get
            Try
                Return ViewState("SubExpenseID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("SubExpenseID") = value
        End Set
    End Property

    Private Sub frmEditSubExpense_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim li2 As HtmlGenericControl = Me.Page.Master.FindControl("mnuMasterExpense")
        li2.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSubExpense")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            If Not Request.QueryString("id") Is Nothing Then
                Try
                    EditSubExpenseID = CInt(Request.QueryString("id"))
                Catch ex As Exception
                    EditSubExpenseID = 0
                End Try

            End If
            BL.Bind_DDL_ExpenseGroup(ddlExpenseGroup)
            GetGroupExpenseInfoForEdit()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtNames.Enabled = False
            ddlExpenseGroup.Enabled = False
            chkActive.Enabled = False
            btnSave.Visible = False
        End If
    End Sub

    Sub GetGroupExpenseInfoForEdit()
        ClearEditForm()

        If EditSubExpenseID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_SubExpense(EditSubExpenseID, "", "", "")
            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("sub_name").ToString()
                ddlExpenseGroup.SelectedValue = dt.Rows(0)("expense_group_id").ToString()

            End If

            lblEditMode.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
        End If
    End Sub

    Private Sub ClearEditForm()
        txtNames.Text = ""
        BL.Bind_DDL_ExpenseGroup(ddlExpenseGroup)
        chkActive.Checked = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbExpenseSubLinqDB
                lnqLoc.ChkDataByPK(EditSubExpenseID, trans.Trans)
                With lnqLoc
                    .SUB_NAME = txtNames.Text.Replace("'", "''")
                    .EXPENSE_GROUP_ID = ddlExpenseGroup.SelectedValue
                    .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N").ToString()
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmSubExpense.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.Message.Replace("'", """") & "');", True)
            End Try
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtNames.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุรายจ่ายย่อย');", True)
            ret = False
        End If

        If ddlExpenseGroup.SelectedValue = "" Or ddlExpenseGroup.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุกลุ่มรายจ่าย');", True)
            ret = False
        End If

        Return ret
    End Function

    Private Sub btnCancle_Click(sender As Object, e As System.EventArgs) Handles btnCancle.Click
        Response.Redirect("frmSubExpense.aspx")
    End Sub
End Class
