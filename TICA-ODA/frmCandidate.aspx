﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmCandidate.aspx.vb" Inherits="frmCandidate" %>


<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Candidate | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Candidate (รายชื่อผู้สมัครขอรับทุน)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Manage Recipient</a></li>
                <li class="active">Candidate </li>
            </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>

                                        <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                    </p>

                                    <asp:Panel ID="pnlAdSearch" runat="server">
                                        
                                                                                <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Project Type :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlProject_Type" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                         <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                                                         <asp:ListItem Value="0">Project</asp:ListItem>
                                                         <asp:ListItem Value="1">Non Project</asp:ListItem>
                                                         <asp:ListItem Value="2">Loan</asp:ListItem>
                                                         <asp:ListItem Value="3">Contribuition</asp:ListItem>

                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">โครงการ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากโครงการที่เปิดรับสมัคร" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">กิจกรรม :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Activity" runat="server" placeholder="ค้นหาจากกิจกรรมที่เปิดรับสมัคร" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">ประเทศ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Country" runat="server" placeholder="ค้นหาจากประเทศผู้มัคร" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:10px;">
                                              <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">Start/End Date:</label>
                                                 <div class="col-sm-3">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="วันเริ่มต้น" Width="120px"></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                </div>
                                                 <div class="col-sm-1"><label for="inputname" class="col-sm-2 control-label line-height">To</label></div>
                                                     <div class="col-sm-4">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุดโครงการ" Width="120px"></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                 </div>
                                             </div>
                                            
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">สถานะ :</label>
                                                 <div class="col-sm-9 ">
                                                     <asp:DropDownList ID="ddlStatus_Recipient" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                        <asp:ListItem Value="-2" Text="...." ></asp:ListItem>
                                                        <asp:ListItem Value="-1" Text="ยังไม่ได้พิจารณา" Selected></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="ไม่ผ่านการคัดเลือก"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ผ่านการคัดเลือก"></asp:ListItem> 

                                                    </asp:DropDownList>
                                             </div>
                                        </div>




                                    </div>

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>







                                     </asp:Panel>
                                    <div class="col-md-8">
                                        <h4 class="text-primary">ผู้สมัครขอรับทุน :
                                            <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                            </h4>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-gray">
                                                <%--                                          Name - Surname
                                                Country
                                                Course
                                                Start - End Date
                                                Institute
                                                Agency
                                                Agency Address
                                                Position
                                                Home Address
                                                Student ID
                                                Passport ID
                                                Expiry Date of Insurance
                                                Expiry Date of VISA
                                                Expiry Date of Passport
                                                Date Of Birth
                                                E - Mail
                                                Telephone
                                                Status
                                                Remarks/Detail--%>
                                                <th>Select</th>
                                                <th style="width:200px">Course</th>
                                                <th style="width:150px">Name - Surname</th>
                                                <th>Country</th>
                                                <th>Project</th>
                                                <%--<th  style="width:80px" >Start - End Date</th>--%>
                                                <th>Institute</th>
                                                <th>Agency</th>
                                                <th>Agency Address </th>
                                                <th>สถานะผู้สมัคร</th>
                                                <th  >Tool</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <asp:Repeater ID="rptList" runat="server" >
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Select">
                                                                <asp:CheckBox ID="chkCheck" runat="server" visible ="false"/>
                                                                 <asp:ImageButton ID="check" runat="server"  ImageUrl="images/none.png"   CommandName ="Select" AutoPostBack="true" visible ="true"/>
                                                            </td>
                                                            <td><asp:Label ID="lblCourse" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblPeriod" runat="server" Font-Size ="13px"  ForeColor="Blue"></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCandidate_Name" runat="server"></asp:Label>
                                                                <asp:Label ID="lblCandidate_ID" runat="server" Visible="false"></asp:Label>
                                                            </td>
                                                            <td><asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblProject" runat="server"></asp:Label></td>
                                                            <%--<td></td>--%>
                                                            <td><asp:Label ID="lblInstitute" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblAgency" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblAgency_Address" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblStatus_Candidate" runat="server"></asp:Label></td>
                                                            <td> </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.box-body -->
                                <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                            </div>
                            <!-- /.box -->
                            <%--Footer--%>
                                  <div class="box-footer">
                                      <div class="col-lg-2">
                                          <asp:LinkButton ID="btnAccept" runat="server" CssClass="btn btn-block btn-social btn-success" >
                                              <i class="fa fa-save"></i>เพิ่มเข้าเป็นผู้รับทุน</asp:LinkButton>
                                      </div> 
                                      <div class="col-lg-2">
                                          <asp:LinkButton ID="btnUnAccept" runat="server" CssClass="btn btn-block btn-social btn-google" >
                                              <i class="fa fa-reply"></i> ไม่ผ่านการคัดเลือก</asp:LinkButton>
                                      </div>
                                  </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="plugins/fastclick/fastclick.min.js"></script>
 
</asp:Content>

