﻿Imports System.Data
Imports Constants
Partial Class frmProjectExpense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("ExpensePage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ExpensePage") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aProjectExpense")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            'BL.Bind_DDL_Year(ddlPlanYear, True)
            BL.Bind_DDL_ProjectType(ddlProjectType)
            BL.Bind_DDL_ProjectType(ddlProjectTypeDialog)
            BindList("", ddlProjectType.SelectedValue)
            Authorize()
        End If
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Expense & "' and isnull(is_save,'N') ='Y'")
        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = False
            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                liview.Visible = True
                liedit.Visible = False
                lidelete.Visible = False
            End If
        Next

    End Sub

    Private Sub BindList(txtSearch As String, project_type As String)
        If project_type = "" Then
            project_type = Constants.Project_Type.Project
        End If

        Dim DT As DataTable = BL.GetList_Expense(0, txtSearch, project_type)
        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "ExpensePage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblProjectID As Label = DirectCast(e.Item.FindControl("lblProjectID"), Label)
        Dim lblProjectName As Label = DirectCast(e.Item.FindControl("lblProjectName"), Label)
        Dim lblrecord As Label = DirectCast(e.Item.FindControl("lblrecord"), Label)
        Dim lblPaymentDate As Label = DirectCast(e.Item.FindControl("lblPaymentDate"), Label)
        'Dim lblRecievedBy As Label = DirectCast(e.Item.FindControl("lblRecievedBy"), Label)
        Dim lblExpense As Label = DirectCast(e.Item.FindControl("lblExpense"), Label)

        lblProjectID.Text = e.Item.DataItem("project_id").ToString
        lblProjectName.Text = e.Item.DataItem("project_name").ToString
        lblrecord.Text = e.Item.DataItem("record").ToString
        lblPaymentDate.Text = e.Item.DataItem("str_payment_date").ToString
        'lblRecievedBy.Text = e.Item.DataItem("received_by").ToString
        lblExpense.Text = Convert.ToDecimal(e.Item.DataItem("expense")).ToString("#,##0.00")
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        BindList(txtSearch.Text, ddlProjectType.SelectedValue)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) 'Handles btnAdd.Click

        'Dim _id As String = txtClickID.Text
        'Dim _project_id As String = txtClickProjectID.Text
        'Response.Redirect("frmProjectExpenseDetail.aspx?id=" & _id & " &mode=" & "add" & "&pjid=" & _project_id)
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

    End Sub
    Protected Sub btnView_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim _project_id As String = txtClickProjectID.text
        Response.Redirect("frmProjectExpenseDetail.aspx?id=" & _id & " &mode=" & "view" & "&pjid=" & _project_id)
    End Sub
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim _project_id As String = txtClickProjectID.text
        Response.Redirect("frmProjectExpenseDetail.aspx?id=" & _id & " &mode=" & "edit" & " &pjid=" & _project_id)
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim ret As ProcessReturnInfo
        ret = BL.DeleteExpenseDetail(_id)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        Else
            BindList(txtSearch.Text, ddlProjectType.SelectedValue)
        End If
    End Sub

    Private Sub ddlProjectType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProjectType.SelectedIndexChanged
        BindList(txtSearch.Text, ddlProjectType.SelectedValue)
    End Sub

    Sub BindProjectForSelect(project_type As String, strSearch As String)
        If project_type = "" Then
            project_type = Constants.Project_Type.Project
        End If

        Dim DT As DataTable = BL.GetProjectList(project_type, strSearch)
        rptProject.DataSource = DT
        rptProject.DataBind()
    End Sub

    Private Sub ddlProjectTypeDialog_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProjectTypeDialog.SelectedIndexChanged
        BindProjectForSelect(ddlProjectTypeDialog.SelectedValue, txtSearchProject.Text.Trim().Replace("'", ""))
    End Sub

    Private Sub btnServerModal_Click(sender As Object, e As EventArgs) Handles btnServerModal.Click
        BindProjectForSelect(ddlProjectTypeDialog.SelectedValue, txtSearchProject.Text.Trim().Replace("'", ""))
        pnlDialog.Visible = True
    End Sub

    Sub btnCancelAct_Click(sender As Object, e As EventArgs)
        pnlDialog.Visible = False
    End Sub

    Protected Sub rptProject_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptProject.ItemCommand
        If e.CommandName = "Select" Then
            Dim _id As String = "0"
            Dim _project_id As String = e.CommandArgument
            Response.Redirect("frmProjectExpenseDetail.aspx?id=" & _id & " &mode=" & "add" & "&pjid=" & _project_id)

            pnlDialog.Visible = False

        End If
    End Sub

    Protected Sub btnSearchProject_Click(sender As Object, e As EventArgs) Handles btnSearchProject.Click
        BindProjectForSelect(ddlProjectTypeDialog.SelectedValue, txtSearchProject.Text.Trim().Replace("'", ""))
    End Sub
    Function GetParameter(Reportformat As String) As String
        Dim para As String = "?ReportName=rptProjectExpense"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectType=" & ddlProjectType.SelectedValue
        para += "&ProjectTypeName=" & ddlProjectType.SelectedItem.Text
        para += "&TextSearch=" & txtSearch.Text.Trim.Replace("'", "''")
        Return para
    End Function
#Region " Print Button"

    Private Sub aPDF_ServerClick(sender As Object, e As EventArgs) Handles aPDF.ServerClick
        myframe.Attributes.Add("src", "about: blank")

        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
    End Sub

    Private Sub aEXCEL_ServerClick(sender As Object, e As EventArgs) Handles aEXCEL.ServerClick
        Dim para As String = GetParameter("EXCEL")
        'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
        myframe.Attributes.Add("src", "reports/frmPreviewReport.aspx" + para)
    End Sub

#End Region
End Class
