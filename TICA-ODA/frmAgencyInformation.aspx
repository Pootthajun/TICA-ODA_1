﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmAgencyInformation.aspx.vb" Inherits="frmUserInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Agency Information | ODA</title>
</asp:Content>

 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Agency Information 
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                  <p>
                    
                      <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn bg-orange btn-flat margin btn-social" OnClick="btnEdit_Click">
                          <i class="fa fa-reply"></i> Edit Agency Information
                      </asp:LinkButton>
                  </p>
                
                      <table class="table table-bordered">
                        <tr>
                          <th style="width: 250px"><b class="pull-right">ชื่อหน่วยงาน :</b></th>
                          <td class="text-primary"><asp:Label ID="lblGOVNameTH" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Agency Name :</b></th>
                          <td class="text-primary"><asp:Label ID="lblGOVNameEN" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Location (ที่ตั้ง) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblTelephone" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Fax. (โทรสาร) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblFax" runat="server"></asp:Label> </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Website :</b></th>
                          <td class="text-primary"><asp:Label ID="lblWebSite" runat="server"></asp:Label></td>
                        </tr>
                           <tr>
                          <th style="width: 250px"><b class="pull-right">Email :</b></th>
                          <td class="text-primary"><asp:Label ID="lblEmail" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Division / Part (ส่วน/กอง) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblDivision" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Ministry (กระทรวง) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblMinistry" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td class="text-primary"><asp:Label ID="lblNote" runat="server" Text=""></asp:Label></td>
                        </tr>
                      </table>
                    
                 
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
                 </ContentTemplate>
        </asp:UpdatePanel>
        <style>
      .color-palette {
        height: 35px;
        line-height: 35px;
        text-align: center;
      }
      .color-palette-set {
        margin-bottom: 15px;
      }
      .color-palette span {
        display: none;
        font-size: 12px;
      }
      .color-palette:hover span {
        display: block;
      }
      .color-palette-box h4 {
        position: absolute;
        top: 100%;
        left: 25px;
        margin-top: -40px;
        color: rgba(255, 255, 255, 0.8);
        font-size: 12px;
        display: block;
        z-index: 7;
      }
    </style>

<!----------------Page Advance---------------------->
<%--     
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>
      </div>
</asp:Content>
     
