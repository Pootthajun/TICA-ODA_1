﻿<%@ Page Title="" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmInKind.aspx.vb" Inherits="frmInKind" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>In Kind | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>In Kind (การสนับสนุนอย่างอื่น)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li class="active">In Kind</li>
            </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>

                                        <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                             <i class="fa fa-plus"></i>Add In Kind
                                        </asp:LinkButton>
                                        <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                    </p>

                                    <asp:Panel ID="pnlAdSearch" runat="server">
                                        <br />
                                        <div class="col-sm-1">
                                        </div>

                                        <table>
                                            <tr>
                                                <td>In Kind
                                                    <br />
                                                    (การสนับสนุนอย่างอื่น)</td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหา In kind" CssClass="form-control"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" height="50">Status (สถานะ) :</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                        <asp:ListItem Value="Y">Activate</asp:ListItem>
                                                        <asp:ListItem Value="N">Disabled</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="50px"></td>
                                                <td>
                                                    <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"> ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="col-md-8">
                                        <h4 class="text-primary">พบทั้งหมด :
                                            <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                            รายการ</h4>
                                    </div>
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-gray">
                                                <th>In Kind Eng(การสนับสนุนอย่างอื่น)</th>
                                                <th>In Kind Thai(การสนับสนุนอย่างอื่น)</th>
                                                <th style="width: 11%;">Status (สถานะ)</th>
                                                <th style="width: 120px;" id="HeadEdit" runat="server">Tools (เครื่องมือ)</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound" OnItemCommand="rptList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("inkind_name") %>'></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblDetail" runat="server" Text='<%#Eval("detail") %>'></asp:Label></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social">
                             <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                             <i class="fa fa-times"></i>Disabled
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td id="ColEdit" runat="server">
                                                            <center>
                                                            <div class="btn-group">
                                                                
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fa fa-navicon text-green"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <%--<li id="liview" runat="server"><a href="javascript:;" onclick='btnViewClick(<%#Eval("ID") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>--%>
                                                                    <li id="liedit" runat="server">
                                                                        <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                    <i class="fa fa-pencil text-blue"></i> แก้ไข
                                                                        </asp:LinkButton></li>
                                                                    <li class="divider"></li>
                                                                    <li id="lidelete" runat="server">
                                                                  
                                                                       
                                    <asp:LinkButton ID="btnDelete" runat="server"  OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' 
                                    CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete">
                                    <i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton></li>
                                                                </ul>
                                                            </div></center>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>


                                        </tbody>
                                    </table>

                                </div>
                                <!-- /.box-body -->
                                <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                            </div>
                            <!-- /.box -->


                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

    <!-- jQuery 2.1.4 -->
    <%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>
</asp:Content>

