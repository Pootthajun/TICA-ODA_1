﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmActivityExPlan.aspx.vb" Inherits="frmActivityExPlan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<%@ Register Src="~/usercontrol/UCExpenseDetailPlanGroup.ascx" TagName="UCExpenseDetailPlanGroup" TagPrefix="uc8" %>
<%@ Register Src="~/usercontrol/UCExpense.ascx" TagName="UCExpense" TagPrefix="uc7" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
    <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tb-fix {
            width: 100%;
            height: 350px;
            /*overflow-x: scroll;*/
            /*margin-left:245px;*/
            /*overflow-y: visible;*/
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }
    </style>
</head>
<body>
    <br />
    <form id="form1" runat="server">
        <div class="content-wrapper" style="margin-left: 0px;">
            <section class="content-header">
                <h1>Expense (ค่าใช้จ่าย)
                </h1>
            </section>
            <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
            <asp:UpdatePanel ID="udpList" runat="server">
                <ContentTemplate>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white">
                                        <asp:Label ID="lblRecipience" runat="server" Text="0" Visible="false"></asp:Label>
                                        <asp:Label ID="lblHeaderId" runat="server" Text="0" Visible="false"></asp:Label>

                                        <table id="example2" class="table table-bordered">
                                            <tbody>
                                                <tr class="bg-info">
                                                    <th style="width: 150px" colspan="4">
                                                        <h4 class="text-blue"><b>แผนงบประมาณของ 
                                                            <asp:Label ID="txtNameRecipience" runat="server" Text="Label"></asp:Label>
                                                        </b></h4>
                                                    </th>
                                                </tr>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:Label ID="lblactivityid" runat="server" Text="0" Visible="false"></asp:Label>
                                                                <uc7:UCExpense runat="server" ID="UCExpense" Visible="false" />
                                                                <uc8:UCExpenseDetailPlanGroup runat="server" ID="UCExpenseDetailPlanGroup" Visible="false" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                    </section>

                </ContentTemplate>
            </asp:UpdatePanel>
            <div style="height: 50px;"></div>
            <div class="modal-footer" style="position: fixed; bottom: 0px; text-align: right; width: 100%;">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-save"></i> Save
                </asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google" Visible="false">
                <i class="fa fa-reply"></i> Cancel
                </asp:LinkButton>
            </div>
        </div>

    </form>
</body>
</html>
