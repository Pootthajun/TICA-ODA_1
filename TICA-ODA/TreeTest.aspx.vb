﻿Imports ODAENG
Imports Constants
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class TreeTest
    Inherits System.Web.UI.Page
    Dim ENG As New ODAENG
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            Dim dt As New DataTable
            dt = ENG.GetOrganizeUnitByNodeID("", OU.Country)

            'Dim dt As DataTable = Me.GetData("SELECT Id, Name FROM VehicleTypes")
            Me.PopulateTreeView(dt, 0, Nothing)
        End If
    End Sub

    Private Sub PopulateTreeView(dtParent As DataTable, parentId As Integer, treeNode As TreeNode)
        For Each row As DataRow In dtParent.Rows
            Dim child As New TreeNode() With {
         .Text = row("name_th").ToString(),
         .Value = row("node_id").ToString()
        }
            If parentId = 0 Then
                TreeView1.Nodes.Add(child)
                'Dim dtChild As DataTable = Me.GetData("SELECT Id, Name FROM VehicleSubTypes WHERE VehicleTypeId = " + child.Value)

                Dim dtChild As New DataTable
                dtChild = ENG.GetOrganizeUnitByNodeID(child.Value, OU.Organize) 'ดึงลูกมาใส่

                PopulateTreeView(dtChild, Integer.Parse(child.Value), child)
            Else
                treeNode.ChildNodes.Add(child)
            End If
        Next
    End Sub

    Private Function GetData(query As String) As DataTable
        Dim dt As New DataTable()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(query)
                Using sda As New SqlDataAdapter()
                    cmd.CommandType = CommandType.Text
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    sda.Fill(dt)
                End Using
            End Using
            Return dt
        End Using
    End Function

End Class
