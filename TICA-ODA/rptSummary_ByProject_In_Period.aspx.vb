﻿


Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptSummary_ByProject_In_Period
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib

    ReadOnly Property ProjectType As Long
        Get
            Try
                Return ddlReportType.SelectedValue
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Sub rptSummary_ByProject_In_Period_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_6")
            a.Attributes.Add("style", "color:#FF8000")
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_Region(ddlRegion)
            Bind_DDL_Country(ddlCountry)
            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = " การให้ความช่วยเหลือแก่ประเทศ/ภูมิภาค "
        Try
            Dim sql As String = ""

            sql += "  --รายงาน การให้ความช่วยเหลือแก่ประเทศ /ภูมิภาคต่างของ ในปี													 " + Environment.NewLine

            sql += "   	SELECT																										 " + Environment.NewLine
            sql += "   		   budget_year								  															 " + Environment.NewLine
            sql += "   		  ,COUNT(Project_id) Count_Project			  															 " + Environment.NewLine
            sql += "   		  ,MIN(start_date) start_date				  															 " + Environment.NewLine
            sql += "   		  ,MAX(end_date) end_date					  															 " + Environment.NewLine
            sql += "   		  ,MIN(start_date_TH) start_date_TH			  															 " + Environment.NewLine
            sql += "   		  ,MAX(end_date_TH) end_date_TH				  															 " + Environment.NewLine
            sql += "   		  ,dbo.Get_Project_Count_Recipient_Int(budget_year,project_type,country_node_id) Recipient_Count   		 " + Environment.NewLine
            sql += "   	      ,project_type									  														 " + Environment.NewLine
            sql += "   	      ,country_node_id								  														 " + Environment.NewLine
            sql += "   	      ,vw_Country_Region.country_region_id								  														 " + Environment.NewLine
            sql += "   	      ,ISNULL(vw_Country_Region.region_name,vw_Country_Region.region_name_th) region_name							  														 " + Environment.NewLine
            sql += "   	      ,_vw_Summary_Aid_Group_By_Country.name_th	country_name_th						  														 " + Environment.NewLine
            sql += "   	      ,SUM(Pay_Amount_Actual) Pay_Amount_Actual																 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,15,country_node_id),0)   Summary_QTY_Bachelor		 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,18,country_node_id),0)   Summary_QTY_Training		 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,6,country_node_id),0)    Summary_QTY_Expert			 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,7,country_node_id),0)    Summary_QTY_Volunteer		 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,10,country_node_id),0)   Summary_QTY_Equipment		 " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Not_In_Count_Recipient_Int(budget_year,project_type,country_node_id),0)   Summary_QTY_Other		 " + Environment.NewLine

            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,2,country_node_id),0)    Summary_QTY_StudyVisit " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,3,country_node_id),0)    Summary_QTY_Meeting " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,4,country_node_id),0)    Summary_QTY_Undergraduate " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,9,country_node_id),0)    Summary_QTY_Mission " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,12,country_node_id),0)   Summary_QTY_Master " + Environment.NewLine
            sql += "            ,ISNULL(dbo.Get_Project_Componemt_Count_Recipient_Int(budget_year,project_type,14,country_node_id),0)   Summary_QTY_Diploma " + Environment.NewLine
            sql += "   	  FROM _vw_Summary_Aid_Group_By_Country																		 " + Environment.NewLine
            sql += "   	  LEFT JOIN vw_Country_Region ON vw_Country_Region.node_id=	_vw_Summary_Aid_Group_By_Country.country_node_id	 " + Environment.NewLine


            If (ProjectType > -1) Then
                sql += " WHERE project_type=" & ProjectType & Environment.NewLine
                Title += " ประเภทโครงการ : " & ddlReportType.SelectedItem.ToString
            Else
                sql += " WHERE 0=1  " + Environment.NewLine
            End If

            If ProjectType < 2 Then
                If (ddlBudgetYear.SelectedIndex > 0) Then
                    sql += " AND  budget_year = '" & ddlBudgetYear.SelectedValue & "' " + Environment.NewLine
                    Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
                End If
            End If

            If (ddlCountry.SelectedIndex > 0) Then
                sql += "  AND convert(bigint,country_node_id) = " & ddlCountry.SelectedValue & "   " + Environment.NewLine
                Title += " ของประเทศ " & ddlCountry.SelectedItem.ToString()
            End If

            If (ddlRegion.SelectedIndex > 0) Then
                sql += "  AND convert(bigint,country_region_id) = " & ddlRegion.SelectedValue & "  " + Environment.NewLine
                Title += " ส่วนภูมิภาค " & ddlRegion.SelectedItem.ToString()
            End If


            sql += "   	  GROUP BY 											  														 " + Environment.NewLine
            sql += "   		   budget_year									  														 " + Environment.NewLine
            sql += "   	      ,project_type									  														 " + Environment.NewLine
            sql += "   	      ,_vw_Summary_Aid_Group_By_Country.name_th										  														 " + Environment.NewLine
            sql += "   		  ,country_node_id	,vw_Country_Region.country_region_id  ,vw_Country_Region.region_name,vw_Country_Region.region_name_th								  														 " + Environment.NewLine

            sql += "  	ORDER BY budget_year DESC ,_vw_Summary_Aid_Group_By_Country.name_th   																						 " + Environment.NewLine

            DT = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Summary_ByProject_In_Period_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()


        If (DT.Rows.Count > 0) Then
            lblCount_Project.Text = Convert.ToDecimal(DT.Compute("SUM(Count_Project)", "")).ToString("#,##0")
            lblBachelor_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Bachelor)", "")).ToString("#,##0")
            lblTraining_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Training)", "")).ToString("#,##0")
            lblExpert_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Expert)", "")).ToString("#,##0")
            lblVolunteer_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Volunteer)", "")).ToString("#,##0")
            lblEquipment_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Equipment)", "")).ToString("#,##0")
            'lblOther_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Other)", "")).ToString("#,##0")

            lblStudyVisit_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_StudyVisit)", "")).ToString("#,##0")
            lblMeeting_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Meeting)", "")).ToString("#,##0")
            lblUndergraduate_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Undergraduate)", "")).ToString("#,##0")
            lblMission_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Mission)", "")).ToString("#,##0")
            lblMaster_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Master)", "")).ToString("#,##0")
            lblDiploma_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_QTY_Diploma)", "")).ToString("#,##0")

            lblAmout_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Summary_ByProject_In_Period") = DT

        Pager.SesssionSourceName = "Search_Summary_ByProject_In_Period"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If


        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblCount_Project As Label = DirectCast(e.Item.FindControl("lblCount_Project"), Label)
        Dim lblBachelor As Label = DirectCast(e.Item.FindControl("lblBachelor"), Label)
        Dim lblTraining As Label = DirectCast(e.Item.FindControl("lblTraining"), Label)
        Dim lblExpert As Label = DirectCast(e.Item.FindControl("lblExpert"), Label)
        Dim lblVolunteer As Label = DirectCast(e.Item.FindControl("lblVolunteer"), Label)
        Dim lblEquipment As Label = DirectCast(e.Item.FindControl("lblEquipment"), Label)
        'Dim lblOther As Label = DirectCast(e.Item.FindControl("lblOther"), Label)
        Dim lblAmout As Label = DirectCast(e.Item.FindControl("lblAmout"), Label)


        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If


        If Not IsDBNull(e.Item.DataItem("region_name")) Then
            lblCountry.Text = e.Item.DataItem("country_name_th").ToString & " /" & e.Item.DataItem("region_name").ToString
        Else
            lblCountry.Text = e.Item.DataItem("country_name_th").ToString
        End If

        If e.Item.DataItem("Count_Project") > 0 Then
            lblCount_Project.Text = Convert.ToDecimal(e.Item.DataItem("Count_Project")).ToString("#,##0")
        End If

        If e.Item.DataItem("Summary_QTY_Bachelor") > 0 Then
            lblBachelor.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Bachelor")).ToString("#,##0")
        End If

        If e.Item.DataItem("Summary_QTY_Training") > 0 Then
            lblTraining.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Training")).ToString("#,##0")
        End If

        If e.Item.DataItem("Summary_QTY_Expert") > 0 Then
            lblExpert.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Expert")).ToString("#,##0")
        End If

        If e.Item.DataItem("Summary_QTY_Volunteer") > 0 Then
            lblVolunteer.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Volunteer")).ToString("#,##0")
        End If

        If e.Item.DataItem("Summary_QTY_Equipment") > 0 Then
            lblEquipment.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Equipment")).ToString("#,##0")
        End If

        'If e.Item.DataItem("Summary_QTY_Other") > 0 Then
        '    lblOther.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Other")).ToString("#,##0")
        'End If
        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblAmout.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If


        '-----เพิ่มอีก 6 คอลัมน์
        Dim lblStudyVisit As Label = DirectCast(e.Item.FindControl("lblStudyVisit"), Label)
        Dim lblMeeting As Label = DirectCast(e.Item.FindControl("lblMeeting"), Label)
        Dim lblUndergraduate As Label = DirectCast(e.Item.FindControl("lblUndergraduate"), Label)
        Dim lblMission As Label = DirectCast(e.Item.FindControl("lblMission"), Label)
        Dim lblMaster As Label = DirectCast(e.Item.FindControl("lblMaster"), Label)
        Dim lblDiploma As Label = DirectCast(e.Item.FindControl("lblDiploma"), Label)

        If e.Item.DataItem("Summary_QTY_StudyVisit") > 0 Then
            lblStudyVisit.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_StudyVisit")).ToString("#,##0")
        End If
        If e.Item.DataItem("Summary_QTY_Meeting") > 0 Then
            lblMeeting.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Meeting")).ToString("#,##0")
        End If
        If e.Item.DataItem("Summary_QTY_Undergraduate") > 0 Then
            lblUndergraduate.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Undergraduate")).ToString("#,##0")
        End If
        If e.Item.DataItem("Summary_QTY_Mission") > 0 Then
            lblMission.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Mission")).ToString("#,##0")
        End If
        If e.Item.DataItem("Summary_QTY_Master") > 0 Then
            lblMaster.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Master")).ToString("#,##0")
        End If
        If e.Item.DataItem("Summary_QTY_Diploma") > 0 Then
            lblDiploma.Text = Convert.ToDecimal(e.Item.DataItem("Summary_QTY_Diploma")).ToString("#,##0")
        End If

        Dim btnDrillDown As Button = e.Item.FindControl("btnDrillDown")
        For i As Integer = 1 To 14
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & btnDrillDown.ClientID & "').click();"
        Next

        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)
        pnlProject.Visible = False
        lblDiploma.Attributes("budget_year") = e.Item.DataItem("budget_year")
        lblDiploma.Attributes("country_node_id") = e.Item.DataItem("country_node_id")


    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)

        Dim lblDiploma As Label = DirectCast(e.Item.FindControl("lblDiploma"), Label)
        Dim rptProject As Repeater = DirectCast(e.Item.FindControl("rptProject"), Repeater)
        AddHandler rptProject.ItemDataBound, AddressOf rptProject_ItemDataBound

        Select Case e.CommandName
            Case "select"
                If pnlProject.Visible Then
                    pnlProject.Visible = False
                    Exit Sub
                End If

                Dim SQL As String = ""
                SQL += "   --รายงาน การให้ความช่วยเหลือแก่ประเทศ /ภูมิภาคต่างของ ในปี	   " & vbLf
                SQL += "    	SELECT										  " & vbLf
                SQL += "    		   budget_year		  " & vbLf
                SQL += " 		  ,Project_id  " & vbLf
                SQL += " 		  ,project_name		    " & vbLf
                SQL += "    		  ,MIN(start_date) start_date		  " & vbLf
                SQL += "    		  ,MAX(end_date) end_date  " & vbLf
                SQL += "    		  ,MIN(start_date_TH) start_date_TH  " & vbLf
                SQL += "    		  ,MAX(end_date_TH) end_date_TH  	  " & vbLf
                SQL += "    	      ,project_type  " & vbLf
                SQL += "    	      ,country_node_id  " & vbLf
                SQL += "    	      ,name_th	country_name_th	  " & vbLf
                SQL += "    	  FROM _vw_Summary_Aid_Group_By_Country	  " & vbLf
                SQL += "       WHERE project_type=" & ProjectType & "  " & vbLf

                If ProjectType < 2 Then
                    If (ddlBudgetYear.SelectedIndex > 0) Then
                        SQL += " AND  budget_year = " & lblDiploma.Attributes("budget_year") + Environment.NewLine
                    End If
                End If
                SQL += " AND  country_node_id ='" & lblDiploma.Attributes("country_node_id") & "'" + Environment.NewLine

                SQL += "    	  GROUP BY 						  " & vbLf
                SQL += "    		   budget_year			  " & vbLf
                SQL += "    	      ,project_type			  " & vbLf
                SQL += "    	      ,name_th			  " & vbLf
                SQL += "    		  ,country_node_id	  " & vbLf
                SQL += " 		  ,Project_id  " & vbLf
                SQL += " 		  ,project_name			    " & vbLf
                SQL += "   	ORDER BY budget_year DESC ,name_th    " & vbLf

                Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                rptProject.DataSource = DT
                rptProject.DataBind()
                pnlProject.Visible = True
        End Select


    End Sub

    Protected Sub rptProject_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblProject_Name As Label = e.Item.FindControl("lblProject_Name")

        Dim lblAllocate_Budget As Label = DirectCast(e.Item.FindControl("lblAllocate_Budget"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        lblProject_Name.Text = e.Item.ItemIndex + 1 & ". " & e.Item.DataItem("project_name").ToString()
        'If Not IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) Then
        '    lblAllocate_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        'End If
        'If Not IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) Then
        '    lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        'End If
        'If Not IsDBNull(e.Item.DataItem("Balance")) Then
        '    lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        'End If
        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        'For i As Integer = 1 To 1
        Dim tdLinkProject1 As HtmlTableCell = e.Item.FindControl("tdLinkProject1")
        tdLinkProject1.Style("cursor") = "pointer"
        tdLinkProject1.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        'Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        End If


    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_ByProject_In_Period.aspx?Mode=EXCEL');", True)
    End Sub

#End Region

    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)

        Dim Sql As String = " Select DISTINCT country_node_id,name_th FROM _vw_Summary_Aid_Group_By_Country WHERE country_node_id IS NOT NULL  ORDER BY name_th    " + Environment.NewLine
        Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("All Country", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th"), DT.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Private Sub ddlReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReportType.SelectedIndexChanged
        If ProjectType > 1 Then
            divSearch_Year.Visible = False
        Else
            divSearch_Year.Visible = True
        End If

    End Sub
End Class


