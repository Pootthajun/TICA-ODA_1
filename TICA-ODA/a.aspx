﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="a.aspx.vb" Inherits="a" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/usercontrol/UCExpenseDetailPlan.ascx" TagPrefix="uc2" TagName="UCExpenseDetailPlan" %>
<%@ Register Src="~/usercontrol/UCExpenseDetail.ascx" TagPrefix="uc3" TagName="UCExpenseDetail" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
    <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tr-top{
            height:45px;
            background-color:lightblue;
            border-color:black;
        }

        .tb-fix {
            width: 100%;
            overflow-x: scroll;
            margin-left: 1px;
            overflow-y: visible;
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }

    </style>
</head>
<body>
    <br />
    <form id="form" runat="server">
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Label runat="server" Text="Label" ID="html"></asp:Label>
        <div id="all_result">No JS Allow!</div>
        


</form>
    <form id="#frm1">
</form>
รวม : <div id="all_result">No JS Allow!</div>
</body>
</html>
<script>
    $(document).ready(function () {
        var i = 1; var cnt = 5; var html = '';
        //create object
        for (i = 1; i <= cnt; i++) {
            html = '<p>Number : ' + i + ' ';
            html += '<input type ="text" name="value[]" id="value_' + i + '" />';
            html += ' * <input type ="text" name="count[]" id="count_' + i + '" />';
            html += ' = <input type ="text" name="result[]" disabled id="result_' + i + '" />';
            html += '</p>';
            $("form").append(html);
        }

        //trigger when type
        $('input[id^="TextBox1"], input[id^="TextBox2"]').keyup(function () {
            //find value1
            var value1 = parseFloat($(this).val());
            //check if is not a number, skip
            if (isNaN(value1)) return false;
            //find type of trigged
            var type = $(this).attr("id").split("_");
            //find number
            var no = parseInt(type[1]);
            //delete number
            type = type[0];
            //find Multiplier
            var value2 = parseFloat($('#' + (type == "value" ? "count" : "value") + "_" + no).val());
            //check if is not a number, skip
            if (isNaN(value2)) return false;
            //chenge value
            $("#result_" + no).val(value1 * value2);
            //set start value
            var all_result = 0;
            //travel all result
            $('input[id^="result"]').each(function () {
                var curr_val = parseFloat($(this).val());
                if (!isNaN(curr_val)) all_result += curr_val;
            });
            //update all value
            $("#all_result").html(all_result);

        });
    });
    </script>