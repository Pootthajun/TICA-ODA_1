﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditStructure.aspx.vb" Inherits="frmEditStructure" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Organization Structure | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Organization Structure (โครงสร้างองค์กร)
          </h1>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="frmStructure.aspx"> Organization Structure</a></li>
            <li class="active">Add Organization Structure</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-gray">
                  <h4 class="box-title">Add Agency</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <table class="table table-bordered">
                 
                        <tr>
                           <td colspan="2"><h4 class="box-title"><a href="#">Country </a>> Thailand International Development Cooperation Agency</h4></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">ชื่อหน่วยงาน :</b></th>
                          <td><div class="col-sm-11"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-building-o"></i>
                              </div>
                              <input class="form-control" id="AgencyNameTH" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Agency Name :</b></th>
                          <td><div class="col-sm-11"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-building-o"></i>
                              </div>
                              <input class="form-control" id="AgencyNameEN" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Location (ที่ตั้ง) :</b></th>
                          <td><div class="col-sm-11"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                          <td><div class="col-sm-11"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                              </div>
                              <input class="form-control" id="phone" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Fax. (โทรสาร) :</b></th>
                          <td><div class="col-sm-11"><div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-fax"></i>
                              </div>
                              <input class="form-control" id="Fax" placeholder="">
                            </div></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Website :</b></th>
                          <td><div class="col-sm-11"><input class="form-control" id="Website" placeholder=""></div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td><div class="col-sm-11"><textarea class="form-control" rows="3" placeholder=""></textarea></div></td>
                        </tr>
                       <tr>
                          <th style="width: 250px"><b class="pull-right">Agency Type (ประเภทหน่วยงาน) :</b></th>
                           <td>
                           <div class="form-group">
                            <label>
                              <input type="checkbox" class="minimal" checked>
                                Funding Agency
                            </label><br />
                            <label>
                              <input type="checkbox" class="minimal">
                                Executing Agency
                            </label><br />
                            <label>
                              <input type="checkbox" class="minimal">
                                Implementing Agency 
                            </label>
                          </div></td>
                       </tr>
                       <tr> 
                        <th style="width: 250px"><b class="pull-right">Active Status :</b></th>
                        <td><div class="col-sm-9">
                        <label><input type="checkbox" class="minimal-red" checked></label></div></td>
                        </tr>
                          </table>
                        <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>  
                        </div><!-- /.box-footer -->
                 </div>
              </div><!-- /.box -->
            </div><!-- /.col -->

        </section><!-- /.content -->
    </div>
       

<!----------------Page Advance---------------------->  
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>