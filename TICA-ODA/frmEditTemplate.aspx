﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmEditTemplate.aspx.vb" Inherits="frmEditTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Template Form | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Template Form (รูปแบบ)</h1>
            <ol class="breadcrumb">
                <li><a href="frmProjectExpense.aspx"><i class="fa fa-dollar"></i>Finance</a></li>      
                <li><a href="frmTemplate.aspx">Template Form</a></li>
                <li class="active">
                    <asp:Label ID="lblEditMode" runat="server" Text=""></asp:Label>
                    Template Form</li>
            </ol>
        </section>

        <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="card bg-white">
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12 form-horizontal">
                              <!-- Horizontal Form -->
                              <div class="box">
                                  <div class="box-header with-border bg-blue-gradient">
                                      <h4 class="box-title">
                                          <asp:Label ID="lblEditMode2" runat="server"></asp:Label>
                                          Template Form</h4>
                                  </div>
               
                                  <div class="box-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputname" class="col-sm-3 control-label"><span style="color:red">*</span> Template Name (ชื่อรูปแบบ) :</label>
                                            <div class="col-sm-4">                          
                                                <asp:TextBox ID="txtNames" runat="server" CssClass="form-control" placeholder="" MaxLength="250"></asp:TextBox>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <%--<div class="row">
                                        <div class="form-group">
                                            <label for="inputname" class="col-sm-3 control-label">Search (ค้นหา) :</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">                                                    
                                                    <asp:TextBox CssClass="form-control m-b" ID="txtSearch" runat="server" placeholder="ค้นหาจากกลุ่มค่าใช้จ่ายหรือกลุ่มค่าใช้จ่ายย่อย"></asp:TextBox>                                                    
                                                    <div class="input-group-addon" id="btnSearch" runat="server"><i class="fa fa-search"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

                                    <!-- Table -->
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-gray">    
                                                <th style="width:46px;"></th>                                            
                                                <th>Expense Group (กลุ่มค่าใช้จ่าย)</th>
                                                <th style="width:46px;"></th>
                                                <th>Sub Expense (กลุ่มค่าใช้จ่ายย่อย)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Label ID="lblSub" runat="server" Visible="false"></asp:Label>
                                            <asp:Repeater ID="rptList" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <asp:Label ID="lblID" runat="server" Visible="false" ></asp:Label>
                                                        <td data-title="Select">
                                                            <asp:Label ID="lblGroupID" runat="server" visible ="false"></asp:Label>
                                                             <asp:CheckBox ID="chkAll" runat="server" CommandName ="chkAll" AutoPostBack="true" visible ="false"/>
                                                             <asp:ImageButton ID="checkAll" runat="server"  ImageUrl="images/none.png" CommandArgument='<%# Eval("expense_group_id") %>' CommandName ="checkAll" AutoPostBack="true"/>
                                                        </td>
                                                        <td data-title="Group Expense">                                                            
                                                            <asp:Label ID="lblGroupName" runat="server"></asp:Label></td>
                                                        <td data-title="Select">
                                                            <asp:CheckBox ID="chkCheck" runat="server" visible ="false"/>
                                                             <asp:ImageButton ID="check" runat="server"  ImageUrl="images/none.png" CommandArgument='<%# Eval("id") %>' CommandName ="check" AutoPostBack="true" visible ="true"/>
                                                        </td>
                                                           
                                                        <td data-title="Sub Expense">                                                            
                                                            <asp:Label ID="lblNames" runat="server"></asp:Label></td>    
                                                    </tr>
                                                    
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>

                                      <table id="gg" class="table table-bordered table-hover" style="display:none">
                                        <thead>
                                            <tr class="bg-gray">    
                                                <th>Group</th>                                            
                                                <th>Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="Repeater1" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td> <asp:Label ID="lblGroupcheck" runat="server"></asp:Label> </td> 
                                                        <td> <asp:Label ID="lblCount" runat="server"></asp:Label> </td> 
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>

                                    <!-- End Table -->
                                </div>                                
                                                                    
                                  <%--Footer--%>
                                  <div class="box-footer">
                                      <div class="col-sm-9"></div>
                                      <div class="col-lg-8"></div>
                                      <div class="col-lg-2">
                                          <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" >
                                              <i class="fa fa-save"></i>Save</asp:LinkButton>
                                      </div>
                                      <div class="col-lg-2">
                                          <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" >
                                              <i class="fa fa-reply"></i> Cancel</asp:LinkButton>
                                      </div>
                                  </div>
               
                              </div><!-- /.box -->
                            </div><!-- /.col -->
                          </div>
                    </section>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

