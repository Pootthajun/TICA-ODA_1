﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptAidByCountrySummary_ForAdmin.aspx.vb" Inherits="rptAidByCountrySummary_ForAdmin" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <%-- <h1>รายงานการให้ความช่วยเหลือรายประเทศ  </h1>--%>
            <h1>รายงานการให้ความช่วยเหลือแก่ต่างประเทศ </h1>

            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">รายงานการให้ความช่วยเหลือแก่ต่างประเทศ </li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 30px;"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"><span style="color: red;">*</span>    Report Type :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                        <asp:ListItem Value="-1" Text="...." Selected></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Project"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Non Project"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Loan"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Contribution"></asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6" id="divSearch_Year" runat="server" >
                                                <label for="inputname" class="col-sm-3 control-label line-height">Budget Year :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width:80%">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Country :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 80%">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Project :</label>
                                                <div class="col-sm-9">

                                                    <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากชื่อโครงการ" CssClass="form-control"  Style="width: 80%"></asp:TextBox>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">Start/End Date :</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="เริ่มต้น" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label for="inputname" class="col-sm-2 control-label line-height">To</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุด" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body" style ="overflow-y :auto;">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th rowspan="2" style="vertical-align: middle;">Project<br />
                                                        (โครงการ)</th>
                                                    <th rowspan="2" style="vertical-align: middle;">Country<br />
                                                        (ประเทศ)</th>
                                                    <th colspan="11">Component<br />
                                                        (ประเภทความร่วมมือ)</th>
                                                    <th rowspan="2" style="vertical-align: middle;">Amount<br />
                                                        (มูลค่า)</th>
                                                </tr>
                                                <tr class="bg-gray">
                                                    <th title ="ทุนศึกษา">Bachelor</th>
                                                    <th title ="ทุนฝึกอบรม">Training</th>
                                                    <th title ="ผู้เชี่ยวชาญ">Expert</th>
                                                    <th title ="อาสาสมัคร">Volunteer</th>
                                                    <th title ="วัสดุอุปกรณ์">Equipment</th>

                                                    <th title ="เยี่ยมชมการศึกษา">Study Visit</th>
                                                    <th title ="จัดการประชุม">Meeting</th>
                                                    <th title ="ระดับปริญญาตรี">Undergraduate</th>
                                                    <th title ="">Mission</th>
                                                    <th title ="">Master</th>
                                                    <th title ="ประกาศนียบัตร">Diploma</th>                       
                                                    
                                                   <%-- <th>Other<br />
                                                        (อื่นๆ)</th>--%>
                                                </tr>


                                                <%--ทุนศึกษา	ทุนฝึกอบรม	ผู้เชี่ยวชาญ	อาสาสมัคร	วัสดุอุปกรณ์
                                                15 Bachelor	
                                                18 Training	 ....
                                                6 Expert	
                                                7 Volunteer	
                                                10  Equipment--%>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style="background-color: ivory;">
                                                            <td data-title="Budget year" colspan="14" style="text-align: center">
                                                                <b>ปี 
                                                                    <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr2" runat="server"  >
                                                            <td data-title="Project" colspan="14" style="text-align: left"  id="tdLinkProject1" runat="server">
                                                                <asp:Label ID="lblProject" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblPlanDate" runat="server" ForeColor="Blue"></asp:Label>
                                                                <a  ID="lnkSelectProject" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--<td data-title="Project">
                                                                

                                                            </td>--%>
                                                            <td data-title="Country" colspan ="2" style =" padding-left :100px;">
                                                                <asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                                                            <td data-title="Bachelor" style="text-align: right; ">
                                                                <asp:Label ID="lblBachelor" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Training" style="text-align: right;">
                                                                <asp:Label ID="lblTraining" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Expert" style="text-align: right;">
                                                                <asp:Label ID="lblExpert" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Volunteer" style="text-align: right; ">
                                                                <asp:Label ID="lblVolunteer" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Equipment" style="text-align: right;">
                                                                <asp:Label ID="lblEquipment" runat="server" ForeColor="black"></asp:Label></td>

                                                            <td data-title="Study Visit" style="text-align: right;">
                                                                <asp:Label ID="lblStudyVisit" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Meeting" style="text-align: right;">
                                                                <asp:Label ID="lblMeeting" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Undergraduate" style="text-align: right;">
                                                                <asp:Label ID="lblUndergraduate" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Mission" style="text-align: right; ">
                                                                <asp:Label ID="lblMission" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Master" style="text-align: right;">
                                                                <asp:Label ID="lblMaster" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Diploma" style="text-align: right;">
                                                                <asp:Label ID="lblDiploma" runat="server" ForeColor="black"></asp:Label></td>




                                                           <%-- <td data-title="Other" style="text-align: right; min-width: 100px;">
                                                                <asp:Label ID="lblOther" runat="server" ForeColor="black" Text="0.00"></asp:Label></td>--%>

                                                            <td data-title="Amount" style="text-align: right; min-width: 100px;"><b>
                                                                <asp:Label ID="lblAmout" runat="server" ForeColor="black"></asp:Label></b></td>

                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="Total" style="text-align: center;" colspan="2"><b>Total</b></td>
                                                        <td data-title="Bachelor" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBachelor_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Training" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblTraining_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Expert" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblExpert_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Volunteer" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblVolunteer_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Equipment" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblEquipment_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                       <%-- <td data-title="Other" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblOther_Sum" runat="server" ForeColor="black" Text="0.00"></asp:Label></b></td>--%>
                                                        <td data-title="Study Visit" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblStudyVisit_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Meeting" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblMeeting_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Undergraduate" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblUndergraduate_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Mission" style="text-align: center; text-decoration: underline; ">
                                                                <b><asp:Label ID="lblMission_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Master" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblMaster_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Diploma" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblDiploma_Sum" runat="server" ForeColor="black"></asp:Label></b></td>

                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAmout_Sum" runat="server" ForeColor="black"></asp:Label></b></td>


                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


</asp:Content>


