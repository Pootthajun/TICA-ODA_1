﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditAgencyInformation.aspx.vb" Inherits="frmEditUserInformationAgency" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Agency Information | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Agency Information (รายละเอียดข้อมูลหน่วยงาน)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="#"> Agency Information</a></li>
            <li class="active">Edit Agency Information</li>
          </ol>
        </section>

          <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title">Edit Agency Information</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputnameth" class="col-sm-3 control-label">ชื่อหน่วยงาน :</label>
                      <div class="col-sm-8">
                        
                          <asp:TextBox ID="txtGOVTH" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="form-group">
                      <label for="inputnameen" class="col-sm-3 control-label">Agency Name :</label>
                      <div class="col-sm-8">

                        <asp:TextBox ID="txtGOVEN" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputDirector" class="col-sm-3 control-label">Division / Part (ส่วน/กอง) :</label>
                      <div class="col-sm-8">
                       <asp:TextBox ID="txtDivision" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>
                         <div class="form-group">
                      <label for="inputDirector" class="col-sm-3 control-label">Ministry (กระทรวง) :</label>
                      <div class="col-sm-8">
                       <asp:TextBox ID="txtMinistry" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="box-header with-border">
                         <h4 class="box-title text-primary">Location (ที่ตั้ง)</h4>
                    </div><!-- /.box-header --> <br />
                    <div class="form-group">
                      <label for="building" class="col-sm-2 control-label">No./ building<br />(เลขที่/อาคาร) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtBuilding" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                      <label for="road" class="col-sm-2 control-label">Road  (ถนน) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtRoad" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="district" class="col-sm-2 control-label">Parish (ตำบล/แขวง) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtTambon" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label for="prefecture" class="col-sm-2 control-label">District (อำเภอ) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtDistrict" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="county" class="col-sm-2 control-label">Province (จังหวัด) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label for="Zipcode" class="col-sm-2 control-label">Postal Code (ไปรษณีย์) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="phone" class="col-sm-2 control-label">Telephone (โทรศัพท์) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label for="fax" class="col-sm-2 control-label">Fax (โทรสาร) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="website" class="col-sm-2 control-label">Website (เว็บไซต์) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtWebSite" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label for="fax" class="col-sm-2 control-label">Email (อีเมล์) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ></asp:TextBox>
                    </div>
                  </div>
                      <br />
                      <br />
                       <div class="clearfix"></div>
                      <div class="form-group">
                       <label for="fax" class="col-sm-2 control-label">Note (หมายเหตุ) :</label>
                      <div class="col-sm-4">
                        <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px" Width ="860px"></asp:TextBox>
                    </div>
                    </div>
                  
                  </div><!-- /.box-body -->                
                                       
                  
                    <div class="form-group box-footer">
                      <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                      <div class="col-sm-9">
                     <label><asp:CheckBox ID="chkActiveStatus" runat="server" CssClass="minimal"/></label>
                      </div>
                                      <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                     
                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                            <i class="fa fa-save"></i>Save
                        </asp:LinkButton>
                    </div>
                    <div class="col-sm-2">
                      
                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                            <i class="fa fa-reply"></i>Cancel
                        </asp:LinkButton>
                    </div>  
                    </div>
                        
                 </div>
              <!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
                  </ContentTemplate>
        </asp:UpdatePanel>
    </div>
 


<!----------------Page Advance---------------------->
        
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />  

</asp:Content>
