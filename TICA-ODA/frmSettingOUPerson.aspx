﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmSettingOUPerson.aspx.vb" Inherits="frmSettingOUPerson" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Src="~/usercontrol/UCFormOUPerson.ascx" TagPrefix="uc1" TagName="UCFormOUPerson" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>OU Person</title>
    <style>
        .box {
            border: 0 !important;
        }

        .trHead {
            height: 42px;
            text-align: left;
            font-weight: bold;
            background-color: #ebedf0;
        }

        input[type=radio] {
            visibility: hidden;
            position: relative;
            margin-left: 5px;
            width: 20px;
            height: 20px;
            cursor: pointer;
        }

            input[type=radio]:before {
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                content: "";
                visibility: visible;
                position: absolute;
                border: 2px solid #09a1d8;
                border-radius: 50%;
            }

            input[type=radio]:checked:before {
                background-color: #44cbfc;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Person
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li><a href="frmSettingOUPerson.aspx">OU Person</a></li>
                <li class="active">
                    <asp:Label ID="lblEditMode" runat="server" Text="Add "></asp:Label>OU Person</li>

            </ol>
        </section>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUserList" runat="server">
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social">
                                        <i class="fa fa-plus"></i>Add Person
                                            </asp:LinkButton>
                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <br />
                                            <div class="col-sm-1">
                                            </div>

                                            <table>
                                                <tr>
                                                    <td>Person
                                                <br />
                                                        (ผู้ใช้งาน)</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาผู้ใช้งาน" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Status :</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                            <asp:ListItem Value="Y">Ready</asp:ListItem>
                                                            <asp:ListItem Value="N">Disabled</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td width="50px"></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"> ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancelSearch" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                                รายการ
                                            </h4>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>ชื่อเข้าระบบ</th>
                                                    <th>ชื่อ - สกุล</th>
                                                    <th>หน่วยงาน</th>
                                                    <th style="width: 11%;">Status (สถานะ)</th>
                                                    <th style="width: 11%;" class="tools" id="HeadEdit" runat="server">Tool (เครื่องมือ)</th>
                                                    <th id="HeadColDelete" runat="server" class="tools" visible="false">Delete (ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblUsername" runat="server" Text=' <%#Eval("username") %> '></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblName" runat="server" Text=' <%#Eval("name_th") %> '></asp:Label>
                                                                <asp:Label ID="lblNodeID" runat="server" Visible="false" Text='<%#Eval("node_id") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOrgName" runat="server" Text=' <%#Eval("org_name") %> '></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social">
                                                            <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                                                            <i class="fa fa-times"></i>Disabled
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td id="ColEdit" runat="server">
                                                                <center>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fa fa-navicon text-green"></i>
                                                                </button>

                                                                <ul class="dropdown-menu pull-right">
                                                                        <li  id="liedit" runat="server">
                                                                        <asp:LinkButton ID="btnEdit" runat="server"  CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                                                        <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                    </li>
                                                                    <li class="divider"></li>
                                                                    <li id="lidelete" runat="server">
                                                                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' CommandArgument='<%# Eval("node_id") %>' CommandName="cmdDelete">
                                                                            <i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </center>
                                                            </td>
                                                            <td id="ColDelete" runat="server" visible="false"></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                                </div>
                                <!-- /.box -->


                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUserEdit" runat="server" Visible="false">
                    <!-- Main content -->
                    <section class="content">
                        <!-- START CUSTOM TABS -->
                        <uc1:UCFormOUPerson runat="server" ID="UCFormOUPerson" ShowLoginInfo="true" />
                        <!-- END CUSTOM TABS -->
                    </section>


                    <div class="box-footer">
                        <div class="col-sm-9"></div>
                        <div class="col-lg-8"></div>
                        <div class="col-lg-2">
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save</asp:LinkButton>
                        </div>
                        <div class="col-lg-2">
                            <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google">
                                <i class="fa fa-reply"></i> Cancel   
                            </asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

