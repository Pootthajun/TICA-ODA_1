﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmRecipient_Passport.aspx.vb" Inherits="frmRecipient_Passport" %>


<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Recipient | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Recipient (รายชื่อผู้รับทุน)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Manage Recipient</a></li>
                <li class="active">Recipient </li>
            </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>

                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">

                                <asp:Panel ID="pnlList" runat="server">
                                    <div class="box-header">
                                        <p>

                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height">Project Type :</label>
                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlProject_Type" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                            <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                                                            <asp:ListItem Value="0">Project</asp:ListItem>
                                                            <asp:ListItem Value="1">Non Project</asp:ListItem>
                                                            <asp:ListItem Value="2">Loan</asp:ListItem>
                                                            <asp:ListItem Value="3">Contribuition</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height">โครงการ :</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากโครงการที่เปิดรับสมัคร" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height">กิจกรรม :</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtSearch_Activity" runat="server" placeholder="ค้นหาจากกิจกรรมที่เปิดรับสมัคร" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height">ประเทศ :</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtSearch_Country" runat="server" placeholder="ค้นหาจากประเทศผู้มัคร" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">Start/End Date:</label>
                                                    <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="วันเริ่มต้น" Width="120px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <label for="inputname" class="col-sm-2 control-label line-height">To</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุดโครงการ" Width="120px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height">สถานะ :</label>
                                                    <div class="col-sm-9 ">
                                                        <asp:DropDownList ID="ddlStatus_Recipient" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                            <asp:ListItem Value="-2" Text="...."></asp:ListItem>
                                                            <asp:ListItem Value="-1" Text="ยังไม่ได้พิจารณา" Selected></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="ไม่ผ่านการคัดเลือก"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="ผ่านการคัดเลือก"></asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                    <div class="col-sm-9 ">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                    <div class="col-sm-9 ">
                                                        <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                        <p class="pull-right" style="margin-right: 10px;">
                                                            <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                                <i class="fa fa-search"></i>
                                                                <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                            </asp:LinkButton>
                                                        </p>

                                                    </div>
                                                </div>

                                            </div>







                                        </asp:Panel>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">ผู้รับทุน :
                                            <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                            </h4>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <%--                                          Name - Surname
                                                Country
                                                Course
                                                Start - End Date
                                                Institute
                                                Agency
                                                Agency Address
                                                Position
                                                Home Address
                                                Student ID
                                                Passport ID
                                                Expiry Date of Insurance
                                                Expiry Date of VISA
                                                Expiry Date of Passport
                                                Date Of Birth
                                                E - Mail
                                                Telephone
                                                Status
                                                Remarks/Detail--%>
                                                    <th style="width: 200px">Course</th>
                                                    <th style="width: 150px">Name - Surname</th>
                                                    <th>Country</th>
                                                    <th>Project</th>

                                                    <th>Passport ID</th>
                                                    <th>Expiry Date of Insurance</th>
                                                    <th>Expiry Date of VISA</th>
                                                    <th>Expiry Date of Passport</th>
                                                    <th>Date Of Birth</th>
                                                    <th>Contact</th>
                                                    <th>Tool</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <asp:Repeater ID="rptList" runat="server">
                                                        <ItemTemplate>
                                                            <tr>

                                                                <td>
                                                                    <asp:Label ID="lblCourse" runat="server"></asp:Label><br />
                                                                    <asp:Label ID="lblPeriod" runat="server" Font-Size="13px" ForeColor="Blue"></asp:Label>

                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCandidate_Name" runat="server"></asp:Label>
                                                                    <asp:Label ID="lblCandidate_ID" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblProject" runat="server"></asp:Label></td>
                                                                <%--<td></td>--%>

                                                                <td>
                                                                    <asp:Label ID="lblPassport_ID" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDate_Insurance" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDate_VISA" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDate_Passport" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDate_Of_Birth" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <ul>
                                                                        <li>E - Mail  :
                                                                            <asp:Label ID="lblEMail" runat="server"></asp:Label></li>
                                                                        <li>Telephone :
                                                                            <asp:Label ID="lblTelephone" runat="server"></asp:Label></li>
                                                                        <li>Status    :
                                                                            <asp:Label ID="lblStatus" runat="server"></asp:Label></li>
                                                                    </ul>
                                                                </td>



                                                                <td>
                                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit">
                                                                            <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                                </asp:Panel>

                                <asp:Panel ID="pnlEdit" runat="server">
                                    <div class="box-header">
                                        <div class="col-md-8">
                                            <h4 class="text-primary">Couse :
                                            <b>
                                                <asp:Label ID="lblActivty" runat="server" Text=""></asp:Label></b>
                                            </h4>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                             <h4 class="text-primary">Project :
                                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                             </h4>
                                            <br />
                                            <asp:Label ID="lblPeriodActivity" runat="server" Font-Size="13px" ForeColor="Blue"></asp:Label>

                                        </div>
                                    </div>


                                    <table class="table table-bordered">
                                        <tr class="bg-info">
                                            <td colspan="2">
                                                <h4 class="box-title">Personal Information</h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>

                                                        <asp:TextBox ID="txtPersonNameTH" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtNodeID" runat="server" Style="display: none"></asp:TextBox>
                                                        <asp:TextBox ID="txtparentNodeID" runat="server" Style="display: none"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Firstname - Lastname : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPersonNameEN" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Country : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr id="trOranization" runat="server" visible="false">
                                            <th style="width: 180px"><b class="pull-right">Organization : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-university"></i>
                                                        </div>
                                                        <asp:DropDownList ID="ddlExecutingOrganize" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Institute : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtInstitute" runat="server" CssClass="form-control"></asp:TextBox>


                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <th style="width: 180px"><b class="pull-right">Agency : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtAgency" runat="server" CssClass="form-control"></asp:TextBox>


                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Agency Address : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtAgency_Address" runat="server" CssClass="form-control"></asp:TextBox>


                                                    </div>
                                                </div>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPersonTelephone" CssClass="form-control" runat="server" MaxLength="9"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Status :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                        <asp:RadioButton ID="rdLearning" runat="server" GroupName="a" Text="กำลังศึกษา" />
                                                        <asp:RadioButton ID="rdDegree" runat="server" GroupName="a" Text="จบการศึกษา" />
                                                        <asp:RadioButton ID="rdRetire" runat="server" GroupName="a" Text="ไม่จบการศึกษา" />


                                                        <%--                                                        <center>
                                            <asp:RadioButton  id ="rdLearning" runat="server" groupname="a" ></asp:RadioButton>
                                            <label for="rdUpdate" class="label" >กำลังศึกษา</label>
                                        </center>
                                                        <center>
                                            <asp:RadioButton id ="rdDegree" runat="server" groupname="a"></asp:RadioButton>
                                            <label for="rdUpdate" class="label">จบการศึกษา</label>
                                        </center>
                                                        <center>
                                            <asp:RadioButton id ="rdRetire" runat="server" groupname="a"></asp:RadioButton>
                                            <label for="rdUpdate" class="label">ไม่จบการศึกษา</label>
                                        </center>--%>
                                                        <%--case -1 when  0 then 'กำลังศึกษา' when 1 then 'จบการศึกษา' when 2 then 'ไม่จบการศึกษา' end [status]--%>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                        <%--Description--%>
                                    <%--    <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Description (รายละเอียด) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>--%>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Position :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPosition" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Home Address :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtHome_Address" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                        <tr class="bg-info">
                                            <td colspan="2">
                                                <h4 class="box-title">Expiry Date</h4>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <th style="width: 180px"><b class="pull-right">Id Card : <span style="color: red">*</span></b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPersonIDCard" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Passport Id :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPersonPassportID" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Expiry Date of Insurance :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>

                                                        <asp:TextBox CssClass="form-control m-b" ID="txtDate_Insurance" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtDate_Insurance" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Expiry Date of VISA :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>

                                                        <asp:TextBox CssClass="form-control m-b" ID="txtDate_VISA" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtDate_VISA" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">Expiry Date of Passport :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>

                                                        <asp:TextBox CssClass="form-control m-b" ID="txtDate_Passport" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtDate_Passport" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </td>
                                        </tr>


                                       <%-- <tr>
                                            <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>

                                                        <asp:TextBox CssClass="form-control m-b" ID="txtPersonBirthDay" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtPersonBirthDay" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                                            <td>
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </div>
                                                        <asp:TextBox ID="txtPersonEmail" CssClass="form-control" runat="server" Enabled ="false" ></asp:TextBox>
                                                        
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>



 

                                    </table>


                                    <div class="box-footer">
                                        <div class="col-sm-9"></div>
                                        <div class="col-lg-8"></div>
                                        <div class="col-lg-2">
                                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" ><i class="fa fa-save"></i>Save</asp:LinkButton>
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" >
                       <i class="fa fa-reply"></i> Cancel

                                            </asp:LinkButton>


                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                            <!-- /.box -->
                            <%--Footer--%>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script src="plugins/fastclick/fastclick.min.js"></script>

</asp:Content>



