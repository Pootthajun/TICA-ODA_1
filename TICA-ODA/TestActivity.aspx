﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="TestActivity.aspx.vb" Inherits="TestActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <style>
        .ActivityAll {
        
        
        }

        .ActivityAll thead tr th {
        
        
        }

        .ActivityAll tr td .z {
        
        
        }

    </style>
    <table style="width:10000px;" class="ActivityAll">
    <thead>
    <tr>
        <th style="width:300px"  class="tdbordertb tdborderl tdhearderinfo">Activity Name</th>
        <th style="width:100px" class="tdbordertb tdhearderinfo">Start Date</th>
        <th style="width:100px" class="tdbordertb tdhearderinfo">End Date</th>
        <th style="width:100px;" class="tdbordertb tdhearderinfo">Duration</th>
        <th style="width:100px;" class="tdbordertb tdhearderinfo">Budget</th>
        <th style="width:30px;" class="tdbordertb tdborderr tdhearderinfo">
            <center>
                <a href="#" onclick="fnClickButton();">
                    <img id="btnOpenModal"><i class="fa fa-plus text-success"></i>
                </a>
            </center>
        </th>
        <th style="width:100px" class="tdborder tdhearder">Nov 2015</th>
        <th style="width:100px" class="tdborder tdhearder">Dec 2015</th>
        <th style="width:100px" class="tdborder tdhearder">Jan 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Feb 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Mar 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Apr 2016</th>
        <th style="width:100px" class="tdborder tdhearder">May 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Jun 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Jul 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Aug 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Sep 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Oct 2016</th>
        <th style="width:100px" class="tdborder tdhearder">Nov 2016</th>
        <th></th>
    </tr>
    </thead>
    <tbody><tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt1)" onmouseout="RestoreBackgroundColor(this,txt1)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span id="Node1" onclick="CollapExpandNode(ChildNode1, 'img1');"><img id="img1" src="dist/img/toggle-expand-icon.png"></span>
            <input type="text" id="txt1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/07/2015</td>
        <td class="tdbordertb">10/04/2016</td>
        <td class="tdbordertb">284</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress">&nbsp;</td>
        <td class="tdinprogress"></td>
        <td class="z"></td>
    </tr>
</tbody><tbody id="ChildNode1">
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt2)" onmouseout="RestoreBackgroundColor(this,txt2)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" id="txt2" value="1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/07/2015</td>
        <td class="tdbordertb">10/04/2016</td>
        <td class="tdbordertb">284</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody>
    <tbody><tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt3)" onmouseout="RestoreBackgroundColor(this,txt3)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span id="Node3" onclick="CollapExpandNode(ChildNode3, 'img3');"><img id="img3" src="dist/img/toggle-expand-icon.png"></span>
            <input type="text" id="txt3" value="กิจกรรมที่ 2 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/12/2015</td>
        <td class="tdbordertb">10/05/2016</td>
        <td class="tdbordertb">161</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody><tbody id="ChildNode3">
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt4)" onmouseout="RestoreBackgroundColor(this,txt4)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span id="Node4" onclick="CollapExpandNode(ChildNode4, 'img4');"><img id="img4" src="dist/img/toggle-expand-icon.png"></span>
            <input type="text" id="txt4" value="2.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/12/2015</td>
        <td class="tdbordertb">10/03/2016</td>
        <td class="tdbordertb">100</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody><tbody id="ChildNode4">
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt6)" onmouseout="RestoreBackgroundColor(this,txt6)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" id="txt6" value="2.1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/12/2015</td>
        <td class="tdbordertb">10/01/2016</td>
        <td class="tdbordertb">40</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt7)" onmouseout="RestoreBackgroundColor(this,txt7)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" id="txt7" value="2.1.2 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/01/2016</td>
        <td class="tdbordertb">10/03/2016</td>
        <td class="tdbordertb">69</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody>
    <tbody><tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt5)" onmouseout="RestoreBackgroundColor(this,txt5)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span id="Node5" onclick="CollapExpandNode(ChildNode5, 'img5');"><img id="img5" src="dist/img/toggle-expand-icon.png"></span>
            <input type="text" id="txt5" value="2.2 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/03/2016</td>
        <td class="tdbordertb">10/05/2016</td>
        <td class="tdbordertb">70</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody><tbody id="ChildNode5">
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt8)" onmouseout="RestoreBackgroundColor(this,txt8)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span id="Node8" onclick="CollapExpandNode(ChildNode8, 'img8');"><img id="img8" src="dist/img/toggle-expand-icon.png"></span>
            <input type="text" id="txt8" value="2.2.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/03/2016</td>
        <td class="tdbordertb">10/05/2016</td>
        <td class="tdbordertb">70</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody><tbody id="ChildNode8">
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt9)" onmouseout="RestoreBackgroundColor(this,txt9)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" id="txt9" value="2.2.1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/03/2016</td>
        <td class="tdbordertb">10/04/2016</td>
        <td class="tdbordertb">40</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
    <tr ondblclick="fnClickButton();" onmouseover="ChangeBackgroundColor(this,txt10)" onmouseout="RestoreBackgroundColor(this,txt10)" style="cursor: default; background-color: rgb(255, 255, 255);">
        <td class="tdbordertb tdborderl">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" id="txt10" value="2.2.1.3 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="30" style="border: none; cursor: default; background-color: rgb(255, 255, 255);" readonly="">
        </td>
        <td class="tdbordertb">01/04/2016</td>
        <td class="tdbordertb">10/05/2016</td>
        <td class="tdbordertb">39</td>
        <td class="tdbordertb">10000</td>
        <td class="tdbordertb tdborderr">
            <center>
                <div class="btn-group" data-toggle="tooltip" title="" data-original-title="เครื่องมือ">
                    <button type="button" class="btn btn-Default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-navicon text-green"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li> <a href="#"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                        <li> <a href="#"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                        <li class="divider"></li>
                        <li> <a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                    </ul>
                </div> 
            </center>
        </td>
        <td class="tdinprogress"></td>
        <td class="tdinprogress"></td>
        <td class="tdallprogress"></td>
    </tr>
</tbody>


</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
</asp:Content>

