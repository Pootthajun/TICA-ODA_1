﻿<%@ Page Title="" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmSubSector.aspx.vb" Inherits="frmSubSector" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Sub Sector | ODA</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Sub Sector (สาขาย่อย)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li class="active">Sub Sector</li>
            </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>
                                        <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                             <i class="fa fa-plus"></i>Add Sub Sector
                                        </asp:LinkButton>

                                        <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                    </p>

                                    <asp:Panel ID="pnlAdSearch" runat="server">
                                        <br />
                                        <div class="col-sm-1"></div>
                                        <table>
                                            <tr>
                                                <td width="150px" height="50">Sector (สาขา)</td>
                                                <td colspan="4">
                                                    <asp:DropDownList ID="ddlPurposeCategory" runat="server" CssClass="form-control select2" Style="width: 100%;">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150px" height="50">Name (ชื่อ)</td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาชื่อสาขาย่อย" CssClass="form-control"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150px" height="50">DAC5code</td>
                                                <td>
                                                    <asp:TextBox ID="TxtDAC" runat="server" placeholder="DAC5code" CssClass="form-control" MaxLength="6" runnat="server"></asp:TextBox></td>
                                                <td width="150px" style="text-align: center">CRScode</td>
                                                <td>
                                                    <asp:TextBox ID="TxtCRS" runat="server" placeholder="CRScode" CssClass="form-control" MaxLength="6" runnat="server"></asp:TextBox></td>
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="col-md-8">
                                        <h4 class="text-primary">พบทั้งหมด :
                                            <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                                            รายการ</h4>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white">
                                    <%-- height="500px" ScrollBars="Auto"--%>
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>Name (ชื่อ)</th>
                                                    <th>Sector (สาขา)</th>
                                                    <th>DAC5code</th>
                                                    <th>CRScode</th>
                                                    <th style="width: 11%;">Status (สถานะ)</th>
                                                    <th style="width: 11%;" class="tools" id="HeadEdit" runat="server">Tool (เครื่องมือ)</th>
                                                    <th id="HeadColDelete" runat="server" class="tools" visible="false">Delete (ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound" OnItemCommand="rptList_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblName" Width="200px" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("CategoryID") %>'></asp:Label></td>
                                                            <td>
                                                                <center><asp:Label ID="lblDAC5code" runat="server" Text='<%#Eval("DAC5code") %>'></asp:Label></center>
                                                            </td>
                                                            <td>
                                                                <center><asp:Label ID="lblCRScode" runat="server" Text='<%#Eval("CRScode") %>'></asp:Label></center>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social">
                             <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                             <i class="fa fa-times"></i>Disabled
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td id="ColEdit" runat="server">
                                                                <center>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>

                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li id="liedit" runat="server">
                                                                             <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                                      <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li id="lidelete" runat="server">
                                                                             <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                        CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                              </center>
                                                            </td>
                                                            <td id="ColDelete" runat="server" Visible="false" >
                                                                
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>



                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- /.box-body -->
                                </asp:Panel>

                                <!-- PageNavigation -->
                                <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />


                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
