﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization
Partial Class rptBudget_Group_L2
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Private Sub rptBudget_Group_L2_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_03_1")
            a.Attributes.Add("style", "color:#FF8000")

            BindList()
        End If
    End Sub

    Private Sub BindList()
        Dim filter As String = ""
        Dim Title As String = " รายงานการจัดสรรงบประมาณ "
        Dim sql As String = ""

        sql += "  -- รายงานการจัดสรรงงประมาณ  Project/Non Project  L2    " + Environment.NewLine
        sql += "             Select  DISTINCT TB_Activity_Budget.budget_year     " + Environment.NewLine
        sql += "    		,TB_Activity.id Activity_id				           " + Environment.NewLine
        sql += "       		,TB_Activity.activity_name 						   " + Environment.NewLine
        sql += "      		,(Select SUM(pay_Amount_Plan) pay_Amount_Plan FROM TB_Activity_Expense_Plan_Detail WHERE TB_Activity_Expense_Plan_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Plan	   " + Environment.NewLine
        sql += "      		,(Select SUM(pay_Amount_Actual) pay_Amount_Actual FROM TB_Activity_Expense_Actual_Detail WHERE TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Actual	   " + Environment.NewLine
        sql += "   " + Environment.NewLine
        sql += "      --Group Budget -----						   " + Environment.NewLine
        sql += "       		,TB_Budget_Sub.budget_group_id				   " + Environment.NewLine
        sql += "       		,TB_Budget_Group.group_name					   " + Environment.NewLine
        sql += "       		,TB_Activity_Budget.budget_sub_id			   " + Environment.NewLine
        sql += "       		,TB_Budget_Sub.sub_name							   " + Environment.NewLine
        sql += "       		From TB_Activity                                " + Environment.NewLine
        sql += "               Left Join TB_Activity_Budget On TB_Activity_Budget.activity_id=TB_Activity.id	   " + Environment.NewLine
        sql += "               Left Join TB_Activity_Expense_Header On TB_Activity_Expense_Header.activity_id=TB_Activity.id			               " + Environment.NewLine
        sql += "               Left Join TB_Activity_Expense_Actual_Detail On TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id	   " + Environment.NewLine
        sql += "               Left Join TB_Project On TB_Project.id= TB_Activity.project_id														   " + Environment.NewLine
        sql += "               INNER Join TB_Budget_Sub On TB_Budget_Sub.id=TB_Activity_Budget.budget_sub_id										   " + Environment.NewLine
        sql += "               INNER Join TB_Budget_Group On TB_Budget_Group.id=TB_Budget_Sub.budget_group_id		   								   " + Environment.NewLine
        sql += "               WHERE  Project_Type In (0, 1) And ISNULL(TB_Activity.Is_Folder,0)=0	                                                   " + Environment.NewLine

        sql += "            And  " + Environment.NewLine
        sql += "            budget_year = 2560 " + Environment.NewLine
        sql += "            And " + Environment.NewLine
        sql += "            TB_Budget_Sub.budget_group_id = 1 " + Environment.NewLine
        sql += "            And " + Environment.NewLine
        sql += "            TB_Activity_Budget.budget_sub_id = 3 " + Environment.NewLine




        'ต้องแยก Loan / Contribuite
        'sql += "  Select * FROM _vw_Allocate_Budget WHERE 1=1 "

        '    If ddlBudgetYear.SelectedIndex > 0 Then
        '        sql += " And budget_year = '" + ddlBudgetYear.SelectedValue.ToString + "'" + Environment.NewLine
        '    End If

        '    If ddlBudgetGroup.SelectedIndex > 0 Then
        '        sql += " and budget_group_id = " + ddlBudgetGroup.SelectedValue + Environment.NewLine
        '    End If

        '    sql += " order by budget_year DESC,group_name,sub_name "


        Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        'lblReportName.Text = ""
        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_Budget_Group_L2_Title") = lblTotalRecord.Text


        If (DT.Rows.Count > 0) Then
            Dim DT_Fill As New DataTable
            DT.DefaultView.RowFilter = "Amount_Budget IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblAmount_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Amount_Budget)", "")).ToString("#,##0.00")
            End If

            DT.DefaultView.RowFilter = "Pay_Amount_Plan IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblAllocate_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            End If
            DT.DefaultView.RowFilter = "Pay_Amount_Actual IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblPay_Amount_Actual_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            End If
            DT.DefaultView.RowFilter = "Balance IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblBalance_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Balance)", "")).ToString("#,##0.00")
            End If

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Budget_Group_L2") = DT

        Pager.SesssionSourceName = "Search_Budget_Group_L2"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblsub_name As Label = DirectCast(e.Item.FindControl("lblsub_name"), Label)

        Dim lblAmount_Budget As Label = DirectCast(e.Item.FindControl("lblAmount_Budget"), Label)
        Dim lblAllocate_Budget As Label = DirectCast(e.Item.FindControl("lblAllocate_Budget"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trgroup_name As HtmlTableRow = e.Item.FindControl("trgroup_name")
        Dim lblgroup_name As Label = e.Item.FindControl("lblgroup_name")

        ''--------Lastbudget_year------------
        'If Lastsub_name <> "ปี " + e.Item.DataItem("budget_year").ToString + " : " + e.Item.DataItem("group_name").ToString Then
        '    Lastsub_name = "ปี " + e.Item.DataItem("budget_year").ToString + " : " + e.Item.DataItem("group_name").ToString
        '    lblgroup_name.Text = Lastsub_name
        '    trgroup_name.Visible = True

        'Else
        '    trgroup_name.Visible = False
        'End If

        lblsub_name.Text = e.Item.DataItem("sub_name").ToString

        If Convert.IsDBNull(e.Item.DataItem("Amount_Budget")) = False Then
            lblAmount_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Amount_Budget")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
            lblAllocate_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


    End Sub


End Class
