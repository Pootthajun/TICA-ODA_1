﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class frmDashbord_Cooperation
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Public Property DT_DataList As DataTable
        Get
            Try
                Return Session("DT_DataList")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("DT_DataList") = value
        End Set
    End Property

    Private Property Cooperation_ID() As Integer
        Get
            If IsNumeric(ViewState("Cooperation_ID")) Then
                Return ViewState("Cooperation_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Cooperation_ID") = value
        End Set
    End Property

    Private ReadOnly Property Field_Data() As String
        Get
            Dim Pie_cptype_name As String = ""
            Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
            If (DT_Cooperation.Rows.Count > 0) Then

                DT_Cooperation.DefaultView.RowFilter = "id=" & Cooperation_ID
                If (DT_Cooperation.DefaultView.Count > 0) Then

                    Pie_cptype_name = DT_Cooperation.DefaultView(0).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "")
                End If
            End If
            Return "SUM_Payment_Actual_" & Pie_cptype_name

        End Get
    End Property

    Private Sub frmProjectStatus_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuProjectStatus")
        li.Attributes.Add("class", "active")
        If IsPostBack = False Then

            Cooperation_ID = Request.QueryString("Cooperation_ID")
            Bind_Cooperation_Master_Col()
            BindData_Summary_Cooperation()

        End If


        'BindTop5Project()
        'BindTop5Component()
    End Sub

    Function Get_Cooperation_Master() As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "SELECT id,cptype_id,cptype_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationtype WHERE ACTIVE_STATUS='Y' Order by sort  "
        DT = SqlDB.ExecuteTable(SQL)

        Return DT
    End Function

    Private Sub Bind_Cooperation_Master_Col()
        'Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        'rptCol.DataSource = DT_Cooperation
        'rptCol.DataBind()


    End Sub



    Private Sub BindData_Summary_Cooperation()
        Dim DT As New DataTable
        Dim SQL As String = ""

        '------ข้อมูล Cooperation จาก Master เพื่อสร้างเป็น Collumn------
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()


        DT = SqlDB.ExecuteTable(SQL)
        Dim str_Field_SUM As String = ""

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  Select *, (" & str_Field_SUM & ")  SUM_Total FROM (  " & vbNewLine

        '----------------------------------------------------------
        SQL &= "  Select  ID, regionoda_name  " & vbNewLine
        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ") SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & "          " & vbNewLine

            Next
        End If
        SQL &= "    FROM (   " & vbNewLine
        '---------------------------------------------------------

        SQL &= "  Select																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  --//---Loop เพื่อหา cooperation_type_id สร้าง คอลัมน์																				   " & vbNewLine

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,Case When cooperation_type_id =" & DT_Cooperation.Rows(i).Item("id") & " 																							 " & vbNewLine
                SQL &= "        Then SUM(SUM_Payment_Actual)																							 " & vbNewLine
                SQL &= "  	  Else 0																													 " & vbNewLine
                SQL &= "  	  End SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " 																									 " & vbNewLine

                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  																																 " & vbNewLine
        SQL &= "  FROM TB_Regionzoneoda																											 " & vbNewLine
        SQL &= "  LEFT JOIN 																													 " & vbNewLine
        SQL &= "  			 (																													 " & vbNewLine
        SQL &= "  			  Select vw_Activity.project_id																						 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.start_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.end_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.cooperation_type_id																				 " & vbNewLine
        SQL &= "  			  ,Data_Project_Recipience_RegionZone.*  FROM (																		 " & vbNewLine
        SQL &= "  			  --====== กิจกรรมและจำนวนเงิน ของ Region Zone ============															   " & vbNewLine
        SQL &= "  			  Select TB_Activity_Expense_Header.Activity_id, Data_Activity_Expense_Actual.* 									 " & vbNewLine
        SQL &= "  				  ,(Select dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')) node_id_OU 								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode(vw_ou.node_id ,'TB_OU_Country')) name_th_OU  								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode_eng(vw_ou.node_id ,'TB_OU_Country')) name_en_OU  							 " & vbNewLine
        SQL &= "  				  ,TB_OU_CountryZone.country_zone_id , TB_Regionzoneoda.regionoda_name											 " & vbNewLine
        SQL &= "  			  FROM (																											 " & vbNewLine
        SQL &= "  			  select Header_id,SUM(Pay_Amount_Actual) SUM_Payment_Actual   ,Recipience_id										 " & vbNewLine
        SQL &= "  			  from TB_Activity_Expense_Actual_Detail																			 " & vbNewLine
        SQL &= "  			  GROUP BY Header_id,Recipience_id																					 " & vbNewLine
        SQL &= "  			  ) AS Data_Activity_Expense_Actual																					 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Activity_Expense_Header ON TB_Activity_Expense_Header.id=Data_Activity_Expense_Actual.Header_id		 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  -- หา ผู้รับทุนอยู่ region zone อะไร																					  " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_ou ON Data_Activity_Expense_Actual.Recipience_id = vw_ou.node_id										 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_OU_CountryZone ON TB_OU_CountryZone.country_id =dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')			 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Regionzoneoda ON TB_Regionzoneoda.id = TB_OU_CountryZone.country_zone_id								 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  WHERE TB_Activity_Expense_Header.Activity_id IS NOT NULL 															 " & vbNewLine
        SQL &= "  			  ) AS Data_Project_Recipience_RegionZone 																			 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_Activity ON vw_Activity.activity_id = Data_Project_Recipience_RegionZone.Activity_id					 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_AllProject ON vw_AllProject.id =vw_Activity.project_id 												 " & vbNewLine
        SQL &= "  			  																													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  ) AS Data_Project_Recipience_RegionZone  ON Data_Project_Recipience_RegionZone.country_zone_id = TB_Regionzoneoda.id			 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  GROUP BY 																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name,cooperation_type_id														 " & vbNewLine

        SQL &= "  )AS TB_SUM  Group by id,regionoda_name  " & vbNewLine
        SQL &= "  )AS TB   " & vbNewLine
        SQL &= "  Order by  regionoda_name  " & vbNewLine


        If (DT_Cooperation.Rows.Count > 0) Then
            SQL &= "  "
        End If


        DT = SqlDB.ExecuteTable(SQL)
        'Session("DT_Data_Summary_Cooperation") = DT
        DT_DataList = DT

        BindTotal()
        BindPerTotal()

        rptList.DataSource = DT
        rptList.DataBind()





    End Sub

    Private Sub BindTotal()

        Dim DT As DataTable = DT_DataList()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()

        Dim DT_Data As New DataTable
        Dim DR As DataRow
        Dim XmyAL As New ArrayList()

        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")
        If (DT.Rows.Count > 0) Then


            If (DT_Cooperation.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    DR = DT_Data.NewRow
                    DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", "")).ToString("#,##0.00")
                    DT_Data.Rows.Add(DR)

                Next

            End If

            '-------Parameter------
            Dim Pie_cptype_name As String = ""

            If (DT_Cooperation.DefaultView.Count > 0) Then
                DT_Cooperation.DefaultView.RowFilter = "id=" & Cooperation_ID
                Pie_cptype_name = DT_Cooperation.DefaultView(0).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "")
                'lbl_Title_Collumn.Text = DT_Cooperation.DefaultView(0).Item("cptype_name").ToString() & " By Sub-Region"
                lbl_Title_Collumn.Text = DT_Cooperation.DefaultView(0).Item("cptype_name").ToString()
            End If


            '---% row ของ sub-region        
            Dim myAL As New ArrayList()
            Dim YmyAL As New ArrayList()

            If (DT.Rows.Count > 0) Then
                For i As Integer = 0 To DT.Rows.Count - 1
                    myAL.Add(Convert.ToDecimal(Convert.ToDecimal((DT.Rows(i).Item("SUM_Payment_Actual_" & Pie_cptype_name) / (DT.Compute("SUM(" & Field_Data.ToString() & ")", ""))) * 100).ToString("#,##0.00")))
                    XmyAL.Add(DT.Rows(i).Item("regionoda_name"))
                    YmyAL.Add(Convert.ToDecimal(Convert.ToDecimal(DT.Rows(i).Item("SUM_Payment_Actual_" & Pie_cptype_name)).ToString("#,##0.00")))

                Next

                lbl_Footer_Tatal.Text = Convert.ToDecimal(DT.Compute("SUM(" & Field_Data.ToString() & ")", "")).ToString("#,##0.00")



            End If


            'rptTotal.DataSource = DT_Data
            'rptTotal.DataBind()

            '----bind Chart Sub-region----
            If (DT.Rows.Count > 0) Then
                Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())

                Dim XValue_ As String() = CType(XmyAL.ToArray(GetType(String)), String())
                Dim YValue_ As Decimal() = CType(YmyAL.ToArray(GetType(Decimal)), Decimal())

                '---------Collumn----------------
                Chart_Sub_region.Series("Series_Sub_region").Points.DataBindY(YValue)

                '---------Collumn----------------
                ChartByCooperation.Series("Series_ChartByCooperation").Points.Clear()
                ChartByCooperation.Series("Series_ChartByCooperation").Points.DataBindXY(XValue_, YValue_)
                Dim ArrPastel() As String = {"#87CEEB", "#32CD32", "#BA55D3", "#F08080", "#4682B4", "#9ACD32", "#40E0D0", "#FF69B4", "#F0E68C", "#D2B48C", "#8FBC8B", "#6495ED", "#DDA0DD", "#5F9EA0", "#FFDAB9", "#FFA07A"}


                For i As Integer = 0 To DT.Rows.Count - 1
                    Dim Value_SUM_Pay_Amount_Actual As Double = DT.Rows(i).Item("SUM_Payment_Actual_" & Pie_cptype_name)
                    '-------------Pie-------------------


                    Chart_Sub_region.Series("Series_Sub_region").Points(i).LegendText = DT.Rows(i).Item("regionoda_name").ToString()
                    Chart_Sub_region.Series("Series_Sub_region").Points(i).ToolTip = DT.Rows(i).Item("regionoda_name").ToString() & " : " & Convert.ToDecimal(YValue(i)).ToString("#,##0.00") & " %"
                    'Chart_Sub_region.Series("Series_Sub_region").Points(i).ToolTip = DT.Rows(i).Item("regionoda_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " ฿"
                    If (Value_SUM_Pay_Amount_Actual > 0) Then
                        Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = DT.Rows(i).Item("regionoda_name").ToString() & vbCrLf & YValue(i) & " % "
                    Else
                        Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = ""
                    End If

                    Chart_Sub_region.Series("Series_Sub_region").YValueType = DataVisualization.Charting.ChartValueType.Auto

                    Chart_Sub_region.Series("Series_Sub_region").Points(i)("PieLineColor") = "Black"
                    'Chart_Sub_region.Series("Series_Sub_region").Points(i)("PieLineColor") = ArrPastel(i).ToString()

                    Chart_Sub_region.Series("Series_Sub_region")("PieLabelStyle") = "Outside"
                    'Chart_Sub_region.ChartAreas(0).Area3DStyle.Enable3D = True
                    'Chart_Sub_region.ChartAreas(0).Area3DStyle.Inclination = 40


                    '-------------Collume---------------

                    ChartByCooperation.Series("Series_ChartByCooperation").Points(i).ToolTip = DT.Rows(i).Item("regionoda_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " ฿"
                    'If (YValue_(i) > 0) Then
                    '    ChartByCooperation.Series("Series_ChartByCooperation").Points(i).Label = YValue_(i) & " บาท"
                    'Else
                    '    ChartByCooperation.Series("Series_ChartByCooperation").Points(i).Label = ""
                    'End If

                    ChartByCooperation.Series("Series_ChartByCooperation").Points(i).Color = System.Drawing.ColorTranslator.FromHtml(ArrPastel(i))

                Next

            End If


        End If
    End Sub

    Private Sub BindPerTotal()

        Dim DT As DataTable = DT_DataList()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        Dim DT_Data As New DataTable
        Dim DR As DataRow
        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")
        DT_Data.Columns.Add("Amount_Actual")
        If (DT.Rows.Count > 0) Then

            Dim myAL As New ArrayList()
            If (DT_Cooperation.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    DR = DT_Data.NewRow
                    DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", "")) / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")
                    DR("Amount_Actual") = Convert.ToDecimal((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", ""))).ToString("#,##0.00")

                    DT_Data.Rows.Add(DR)
                    myAL.Add(Convert.ToDecimal(DR("SUM_Pay_Amount_Actual")))

                Next
            End If

            'rptPerTotal.DataSource = DT_Data
            'rptPerTotal.DataBind()

            Dim Get_Color_Pie As String = ""
            '----bind Chart Programme----
            If (DT_Data.Rows.Count > 0) Then

                lbl_Per_Footer_Tatal.Text = Convert.ToDecimal(((DT.Compute("SUM(" & Field_Data.ToString() & ")", "")) / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")


                Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())
                'Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}
                ChartByProgramme.Series("Series1").Points.DataBindY(YValue)
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    Dim Value_SUM_Pay_Amount_Actual As Double = DT_Data.Rows(i).Item("SUM_Pay_Amount_Actual")
                    ChartByProgramme.Series("Series1").Points(i).LegendText = DT_Cooperation.Rows(i).Item("cptype_name").ToString()
                    ChartByProgramme.Series("Series1").Points(i).ToolTip = DT_Cooperation.Rows(i).Item("detail").ToString() & " : " & Convert.ToDecimal(DT_Data.Rows(i).Item("Amount_Actual")).ToString("#,##0.00") & " ฿"
                    If (Value_SUM_Pay_Amount_Actual > 0) Then
                        ChartByProgramme.Series("Series1").Points(i).Label = DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " % "
                    Else
                        ChartByProgramme.Series("Series1").Points(i).Label = ""
                    End If

                    ChartByProgramme.Series("Series1").YValueType = DataVisualization.Charting.ChartValueType.Auto


                    '===========



                Next

            End If

        End If

    End Sub



    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim lbl_regionoda_name As Label = e.Item.FindControl("lbl_regionoda_name")
        Dim lblTotal_Row As Label = e.Item.FindControl("lblTotal_Row")
        Dim td_color_Legends As HtmlTableCell = e.Item.FindControl("td_color_Legends")


        Dim ArrPastel() As String = {"#87CEEB", "#32CD32", "#BA55D3", "#F08080", "#4682B4", "#9ACD32", "#40E0D0", "#FF69B4", "#F0E68C", "#D2B48C", "#8FBC8B", "#6495ED", "#DDA0DD", "#5F9EA0", "#FFDAB9", "#FFA07A"}
        td_color_Legends.Style("background-color") = ArrPastel(e.Item.ItemIndex).ToString()

        lbl_regionoda_name.Text = e.Item.DataItem("regionoda_name").ToString()

        If Convert.IsDBNull(e.Item.DataItem(Field_Data.ToString())) = False Then
            lblTotal_Row.Text = Convert.ToDecimal(e.Item.DataItem(Field_Data.ToString())).ToString("#,##0.00")
        End If

    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmProjectStatus.aspx")
    End Sub
End Class
