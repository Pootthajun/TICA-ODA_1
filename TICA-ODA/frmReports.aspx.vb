﻿
Partial Class frmReports
    Inherits System.Web.UI.Page

    Private Sub frmReports_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")


        Label1.Visible = False
        TextBox1.Visible = False
        If IsPostBack = False Then

        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)

        'Response.Redirect("Preview.aspx")
    End Sub

    Protected Sub pDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim value As String = CType(sender, DropDownList).SelectedValue
        If value = 1 Then
            txtSearch.Visible = True
            TextBox1.Visible = False
            LableSearch.Text = "ชื่อแผน"
            ShowData.Text = "<table id = ""example2"" Class=""table table-bordered table-hover"">" +
                            "<thead><tr Class=""bg-gray""><th>ชื่อโครงการ</th>" +
                            "<th style=""width:260px;"">ระยะเวลาในการดำเนินโครงการ</th>" +
                            "<th style=""width:160px;"">งบจัดสรรค์</th>" +
                            "<th style=""width:160px;"">ค่าใช้จ่าย</th>" +
                            "<th style=""width:160px;"">คงเหลือ</th>" +
                            "</tr></thead><tbody><ItemTemplate>" +
                            "<tr><td></td>" +
                            "<td Class=""Date""></td>" +
                            "<td Class=""date""></td>" +
                            "<td Class=""date""></td>" +
                            "<td Class=""date""></td>" +
                            "</tr></ItemTemplate></tbody></table>"
        End If
        If value = 2 Then
            txtSearch.Visible = True
            TextBox1.Visible = False
            LableSearch.Text = "ชื่อโครงการ"
            Label1.Visible = False
            ShowData.Text = "<table id = ""example2"" Class=""table table-bordered table-hover"">" +
                            "<thead><tr Class=""bg-gray""><th>ชื่อกิจกรรม</th>" +
                            "<th style=""width:160px;"">วันเริ่มต้น</th>" +
                            "<th style=""width:160px;"">วันสิ้นสุด</th>" +
                            "<th style=""width:160px;"">งบประมาณที่ได้รับ</th>" +
                            "<th style=""width:160px;"">งบประมาณคงเหลือ</th>" +
                            "</tr></thead><tbody><ItemTemplate>" +
                            "<tr><td></td>" +
                            "<td Class=""Date""></td>" +
                            "<td Class=""date""></td>" +
                            "<td Class=""Date""></td>" +
                            "<td Class=""date""></td>" +
                            "</tr></ItemTemplate></tbody></table>"
        End If
        If value = 3 Then
            txtSearch.Visible = True
            TextBox1.Visible = True
            LableSearch.Text = "ปีงบประมาณ"
            Label1.Visible = True
            Label1.Text = "ผู้รับทุน"
            ShowData.Text = "<table id = ""example2"" Class=""table table-bordered table-hover"">" +
                            "<thead><tr Class=""bg-gray"" style=""width:220px;""><th>ผู้รับทุน</th>" +
                            "<th style=""width:170px;"">วันที่ได้รับงบประมาณ</th>" +
                            "<th style=""width:170px;"">ปีงบปรมาณ</th>" +
                            "<th style=""width:220px;"">งบประมาณที่ได้รับ</th>" +
                            "</tr></thead><tbody><ItemTemplate>" +
                            "<tr><td></td>" +
                            "<td Class=""Date""></td>" +
                            "<td Class=""date""></td>" +
                            "</tr></ItemTemplate></tbody></table>"
        End If
        If value = 4 Then
            txtSearch.Visible = True
            TextBox1.Visible = True
            LableSearch.Text = "ชื่อโครงการ"
            Label1.Visible = True
            Label1.Text = "ผู้รับเงิน"
            ShowData.Text = "<table id = ""example2"" Class=""table table-bordered table-hover"">" +
                            "<thead><tr Class=""bg-gray""><th>ชื่อกิจกรรม</th>" +
                            "<th style=""width:160px;"">ผู้รับเงิน</th>" +
                            "<th style=""width:160px;"">วันที่รับเงิน</th>" +
                            "<th style=""width:160px;"">ชนิดค่าใช้จ่าย</th>" +
                            "<th style=""width:160px;"">จำนวนเงินที่จ่าย</th>" +
                            "</tr></thead><tbody><ItemTemplate>" +
                            "<tr><td></td>" +
                            "<td Class=""Date""></td>" +
                            "<td Class=""date""></td>" +
                            "<td Class=""date""></td>" +
                            "<td Class=""date""></td>" +
                            "</tr></ItemTemplate></tbody></table>"
        End If
    End Sub
End Class
