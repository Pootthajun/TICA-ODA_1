﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidSummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptAgencyByAidEveryYear_Page")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptAgencyByAidEveryYear_Page") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_foreign")
        li_mnuReports_foreign.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptAgencyByAidEveryYear")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            ProjectType = 0  ' เนื่องจากเฉพาะประเภทของ Project Type=0
            BindList()
            BL.Bind_DDL_Year(ddlProject_Year, True)


        End If

    End Sub

    Public Function GetProjectList(project_type As Integer) As DataTable

        Dim dt As New DataTable
        Dim Title As String = ""
        Dim sql As String = ""


        Try
            sql = "SELECT  ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq,* FROM _vw_Agency_Year_Pay_Actual " + Environment.NewLine
            sql += " where 1=1  " + Environment.NewLine
            If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
                sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
                Title += " ปีงบประมาณ " & ddlProject_Year.SelectedValue.ToString
            End If

            If Not String.IsNullOrEmpty(txtSearch_Agency.Text) Then
                sql += " and name_th like '%" + txtSearch_Agency.Text + "%' " + Environment.NewLine
            End If

            sql += "ORDER BY budget_year DESC"


            'sql = "select ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq,budget_year,T2.name_th" + Environment.NewLine
            'sql += ",count(T2.project_id) as Project_Amount,SUM(convert(decimal(18,2),T1.Disbursement_For_Country)) as Disbursed_Amount" + Environment.NewLine
            'sql += "from vw_Summary_Aid T1 left join" + Environment.NewLine
            'sql += "(select  Pro.id as project_id,Pro.funding_agency_id,Agn.name_th,Agn.active_status from TB_Project Pro inner join TB_OU_Organize Agn on Pro.funding_agency_id = Agn.node_id) T2 on T1.project_id =T2.project_id" + Environment.NewLine
            'sql += "where 1=1 and isnull(T2.project_id,'') <>'' " + Environment.NewLine

            'If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
            '    sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
            'End If

            'If Not String.IsNullOrEmpty(txtSearch_Agency.Text) Then
            '    sql += " and name_th like '%" + txtSearch_Agency.Text + "%' " + Environment.NewLine
            'End If

            'sql += "group by budget_year,T2.name_th" + Environment.NewLine
            'sql += "order by budget_year DESC"


            dt = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " หน่วยงาน"
            End If

            Session("Search_AgencyByAidEveryYear_Title") = lblTotalRecord.Text

        Catch ex As Exception
        End Try
        Return dt

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetProjectList(ProjectType)

        If (DT.Rows.Count > 0) Then
            lblProject_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Amount)", "")).ToString("#,##0")
            lblDisbursed_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Disbursed_Amount)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("Search_AgencyByAidEveryYear") = DT


        AllData = DT
        Pager.SesssionSourceName = "rptAgencyByAidEveryYear_Page"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblAgency_name As Label = DirectCast(e.Item.FindControl("lblAgency_name"), Label)
        Dim lblProject_Amount As Label = DirectCast(e.Item.FindControl("lblProject_Amount"), Label)
        Dim lblDisbursed_Amount As Label = DirectCast(e.Item.FindControl("lblDisbursed_Amount"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True

        Else
            trbudget_year.Visible = False
        End If

        lblNo.Text = e.Item.DataItem("seq").ToString
        lblAgency_name.Text = e.Item.DataItem("name_th").ToString
        lblProject_Amount.Text = e.Item.DataItem("Project_Amount").ToString
        If Convert.IsDBNull(e.Item.DataItem("Disbursed_Amount")) = False Then
            lblDisbursed_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Disbursed_Amount")).ToString("#,##0.00")
        End If


        Dim btnDrillDown As Button = e.Item.FindControl("btnDrillDown")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & btnDrillDown.ClientID & "').click();"
        Next
        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)
        pnlProject.Visible = False

        lblAgency_name.Attributes("budget_year") = e.Item.DataItem("budget_year")
        If Not IsDBNull(e.Item.DataItem("funding_agency_id")) Then

            lblAgency_name.Attributes("funding_agency_id") = e.Item.DataItem("funding_agency_id")

        Else
            lblAgency_name.Attributes("funding_agency_id") = ""
        End If

    End Sub


    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)

        Dim lblAgency_name As Label = DirectCast(e.Item.FindControl("lblAgency_name"), Label)
        Dim rptProject As Repeater = DirectCast(e.Item.FindControl("rptProject"), Repeater)
        AddHandler rptProject.ItemDataBound, AddressOf rptProject_ItemDataBound

        Select Case e.CommandName
            Case "select"
                If pnlProject.Visible Then
                    pnlProject.Visible = False
                    Exit Sub
                End If

                Dim SQL As String = ""
                SQL += " SELECT  ROW_NUMBER() OVER(ORDER BY budget_year DESC) As Seq, * FROM _vw_Agency_Year_Pay_Actual_Project WHERE budget_year=" & lblAgency_name.Attributes("budget_year")
                If lblAgency_name.Attributes("funding_agency_id") <> "" Then
                    SQL += " AND funding_agency_id=" & lblAgency_name.Attributes("funding_agency_id")
                Else
                    SQL += " AND funding_agency_id IS NULL "
                End If
                SQL += " ORDER BY project_name "
                Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                rptProject.DataSource = DT
                rptProject.DataBind()
                pnlProject.Visible = True
        End Select


    End Sub

    Protected Sub rptProject_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblProject_Name As Label = e.Item.FindControl("lblProject_Name")
        Dim lblAmount_Actual As Label = e.Item.FindControl("lblAmount_Actual")

        lblProject_Name.Text = e.Item.ItemIndex + 1 & ". " & e.Item.DataItem("project_name").ToString()
        If Not IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) Then
            lblAmount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If
        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        For i As Integer = 1 To 2
            Dim tdLinkProject As HtmlTableCell = e.Item.FindControl("tdLinkProject" & i)
            tdLinkProject.Style("cursor") = "pointer"
            tdLinkProject.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        End If


    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAgencyByAidEveryYear.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAgencyByAidEveryYear.aspx?Mode=EXCEL');", True)
    End Sub

#End Region

End Class
