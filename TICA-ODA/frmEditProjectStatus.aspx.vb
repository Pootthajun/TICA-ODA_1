﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Public Class frmEditProjectStatus
    Inherits System.Web.UI.Page
    Public Count As Integer = 1

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            txtName.Focus()
            BindTop5Data()
            If Session("SESSION_ID") <> "" Then
                lblTitle1.Text = "Edit"
                lblTitle2.Text = "Edit"
                DisplayEditData()
            Else
                lblTitle1.Text = "Add"
                lblTitle2.Text = "Add"
            End If

        End If
    End Sub
    Protected Sub DisplayEditData()

        Dim tran As New TransactionDB
        Dim lnq As New tbPstatusLinqDB
        Dim sql As String = "SELECT  id,pstatus_id,pstatus_name,ACTIVE_STATUS FROM web_pstatus WHERE id=" & Session("SESSION_ID")

        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sql, tran.Trans, p)

        If dt.Rows.Count > 0 Then
            tran.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("pstatus_name").ToString() <> "" Then
                    txtName.Text = dt(i)("pstatus_name").ToString()

                    If dt.Rows(i)("ACTIVE_STATUS").ToString() = "Y" Then
                        chkACTIVE_STATUS.Checked = True
                    Else
                        chkACTIVE_STATUS.Checked = False
                    End If

                End If
            Next

        Else
            tran.RollbackTransaction()
            Dim _err = tran.ErrorMessage()
        End If
        lnq = Nothing


    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)

        Session.Remove("SESSION_ID")
        Response.Redirect("frmMTProjectStatus.aspx")
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNumber As Label = DirectCast(e.Item.FindControl("lblNumber"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

        lblNumber.Text = Count.ToString()
        lblName.Text = drv("pstatus_name").ToString()
        Count += 1
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
    Protected Sub ClearTextBox()
        txtName.Text = ""

    End Sub

    Protected Sub btnSave_Click1(sender As Object, e As EventArgs)

        If txtName.Text <> "" Then

            Dim tran As New TransactionDB
            Dim lnq As New tbPstatusLinqDB
            Dim ret As ExecuteDataInfo
            Dim strID As String = Session("SESSION_ID")
            Dim chkPK As Boolean = lnq.ChkDataByPK(strID, tran.Trans)
            If chkPK = True Then
                lnq.PSTATUS_NAME = (txtName.Text)

                If chkACTIVE_STATUS.Checked = True Then
                    lnq.ACTIVE_STATUS = "Y"
                Else
                    lnq.ACTIVE_STATUS = "N"
                End If

                ret = lnq.UpdateData(UserName, tran.Trans)

            Else
                lnq.PSTATUS_NAME = (txtName.Text)

                If chkACTIVE_STATUS.Checked = True Then
                    lnq.ACTIVE_STATUS = "Y"
                Else
                    lnq.ACTIVE_STATUS = "N"
                End If

                ret = lnq.InsertData(UserName, tran.Trans)
            End If

            If ret.IsSuccess = True Then
                tran.CommitTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmMTProjectStatus.aspx';", True)

            Else
                tran.RollbackTransaction()
                Alert("ไม่สามารถบันทึกข้อมูลได้")
            End If
            lnq = Nothing
            Session.Remove("SESSION_ID")
        Else
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
        End If
    End Sub
    Protected Sub BindTop5Data()
        Dim sql As String = "SELECT TOP 5 id,pstatus_id,pstatus_name FROM web_pstatus ORDER BY id DESC"
        Dim trans As New TransactionDB
        Dim lnq As New tbPstatusLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sql, trans.Trans, p)



        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            rptList.DataSource = dt
            rptList.DataBind()
        Else
            trans.RollbackTransaction()
            Dim err = lnq.ErrorMessage()
        End If
        lnq = Nothing
    End Sub
End Class