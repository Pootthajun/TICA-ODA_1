﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidSummary_Project_NonProject
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
        li_mnuReports_foreign.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_8_2")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            Bind_DDL_Country(ddlCountry)
            BindList()
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_Region(ddlRegion)
            BL.Bind_DDL_RegionZone(ddlRegionzone)
        End If

    End Sub



    Private Sub BindList()

        Dim Title As String = ""
        Dim Filter As String = ""
        Dim sql As String = ""

        sql += " Select * FROM _vw_Summary_Aid_Group_Country_By_Type                       " + Environment.NewLine


            If (ddlBudgetYear.SelectedIndex > 0) Then
                Filter += "   budget_year = '" & ddlBudgetYear.SelectedValue & "'  AND " + Environment.NewLine
                Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            Else

            End If

            If (ddlCountry.SelectedIndex > 0) Then
                Filter += "   convert(bigint,country_node_id) = " & ddlCountry.SelectedValue & "  AND " + Environment.NewLine
                Title += " การให้ความช่วยเหลือแก่ประเทศ " & ddlCountry.SelectedItem.ToString()
            Else
            End If

            If (ddlRegion.SelectedIndex > 0) Then
                Filter += "   region_id = " & ddlRegion.SelectedValue & "  AND " + Environment.NewLine
                Title += " ภูมิภาค " & ddlRegion.SelectedItem.ToString()
            Else
            End If

            If (ddlRegionzone.SelectedIndex > 0) Then
                Filter += "   Regionzone_id = " & ddlRegionzone.SelectedValue & "  AND " + Environment.NewLine
                Title += " กลุ่ม " & ddlRegionzone.SelectedItem.ToString()
            Else
            End If


            If Filter <> "" Then
                sql += " WHERE " & Filter.Substring(0, Filter.Length - 6) & vbLf
            End If

            sql += "  ORDER BY   budget_year DESC,country_name_th "


            Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_AidSummary_Project_NonProject_Title") = lblTotalRecord.Text


            If (DT.Rows.Count > 0) Then
            lblProject_Bachelor.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Bachelor)", "")).ToString("#,##0.00")
            lblProject_Training.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Training)", "")).ToString("#,##0.00")
            lblProject_Expert.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Expert)", "")).ToString("#,##0.00")
            lblProject_Volunteer.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Volunteer)", "")).ToString("#,##0.00")
            lblProject_Equipment.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Equipment)", "")).ToString("#,##0.00")
            lblProject_Other.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Other)", "")).ToString("#,##0.00")
            lblProject_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Project_Amount)", "")).ToString("#,##0.00")


            lblNonProject_Bachelor.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Bachelor)", "")).ToString("#,##0.00")
            lblNonProject_Training.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Training)", "")).ToString("#,##0.00")
            lblNonProject_Expert.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Expert)", "")).ToString("#,##0.00")
            lblNonProject_Volunteer.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Volunteer)", "")).ToString("#,##0.00")
            lblNonProject_Equipment.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Equipment)", "")).ToString("#,##0.00")
            lblNonProject_Other.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Other)", "")).ToString("#,##0.00")
            lblNonProject_Amout.Text = Convert.ToDecimal(DT.Compute("SUM(NonProject_Amount)", "")).ToString("#,##0.00")


            lblTotal_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")



            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("Search_AidSummary_Project_NonProject") = DT

        Pager.SesssionSourceName = "Search_AidSummary_Project_NonProject"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblProject_Bachelor As Label = DirectCast(e.Item.FindControl("lblProject_Bachelor"), Label)
        Dim lblProject_Training As Label = DirectCast(e.Item.FindControl("lblProject_Training"), Label)
        Dim lblProject_Expert As Label = DirectCast(e.Item.FindControl("lblProject_Expert"), Label)
        Dim lblProject_Volunteer As Label = DirectCast(e.Item.FindControl("lblProject_Volunteer"), Label)
        Dim lblProject_Equipment As Label = DirectCast(e.Item.FindControl("lblProject_Equipment"), Label)
        Dim lblProject_Other As Label = DirectCast(e.Item.FindControl("lblProject_Other"), Label)
        Dim lblProject_Amount As Label = DirectCast(e.Item.FindControl("lblProject_Amount"), Label)

        Dim lblNonProject_Bachelor As Label = DirectCast(e.Item.FindControl("lblNonProject_Bachelor"), Label)
        Dim lblNonProject_Training As Label = DirectCast(e.Item.FindControl("lblNonProject_Training"), Label)
        Dim lblNonProject_Expert As Label = DirectCast(e.Item.FindControl("lblNonProject_Expert"), Label)
        Dim lblNonProject_Volunteer As Label = DirectCast(e.Item.FindControl("lblNonProject_Volunteer"), Label)
        Dim lblNonProject_Equipment As Label = DirectCast(e.Item.FindControl("lblNonProject_Equipment"), Label)
        Dim lblNonProject_Other As Label = DirectCast(e.Item.FindControl("lblNonProject_Other"), Label)
        Dim lblNonProject_Amout As Label = DirectCast(e.Item.FindControl("lblNonProject_Amout"), Label)

        Dim lblTotal_Amount As Label = DirectCast(e.Item.FindControl("lblTotal_Amount"), Label)

        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True
        Else
            trbudget_year.Visible = False
        End If

        lblProject.Text = e.Item.DataItem("country_name_th").ToString

        If Convert.IsDBNull(e.Item.DataItem("Project_Bachelor")) = False Then
            lblProject_Bachelor.Text = Convert.ToDecimal(e.Item.DataItem("Project_Bachelor")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Training")) = False Then
            lblProject_Training.Text = Convert.ToDecimal(e.Item.DataItem("Project_Training")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Expert")) = False Then
            lblProject_Expert.Text = Convert.ToDecimal(e.Item.DataItem("Project_Expert")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Volunteer")) = False Then
            lblProject_Volunteer.Text = Convert.ToDecimal(e.Item.DataItem("Project_Volunteer")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Equipment")) = False Then
            lblProject_Equipment.Text = Convert.ToDecimal(e.Item.DataItem("Project_Equipment")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Other")) = False Then
            lblProject_Other.Text = Convert.ToDecimal(e.Item.DataItem("Project_Other")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Project_Amount")) = False Then
            lblProject_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Project_Amount")).ToString("#,##0.00")
        End If



        '=====Non Project============

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Bachelor")) = False Then
            lblNonProject_Bachelor.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Bachelor")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Training")) = False Then
            lblNonProject_Training.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Training")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Expert")) = False Then
            lblNonProject_Expert.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Expert")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Volunteer")) = False Then
            lblNonProject_Volunteer.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Volunteer")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Equipment")) = False Then
            lblNonProject_Equipment.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Equipment")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Other")) = False Then
            lblNonProject_Other.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Other")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Amount")) = False Then
            lblNonProject_Amout.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Amount")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("NonProject_Amount")) = False And Convert.IsDBNull(e.Item.DataItem("Project_Amount")) = False Then
            lblTotal_Amount.Text = Convert.ToDecimal(e.Item.DataItem("NonProject_Amount") + e.Item.DataItem("Project_Amount")).ToString("#,##0.00")
        End If

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click


        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidSummary_Project_NonProject.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidSummary_Project_NonProject.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub ddlRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRegion.SelectedIndexChanged

        If (ddlRegion.SelectedIndex > 0) Then
            BL.Bind_DDL_RegionZone_By_Region(ddlRegionzone, ddlRegion.SelectedValue)
        Else
            BL.Bind_DDL_RegionZone(ddlRegionzone)
        End If
    End Sub


    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)

        Dim Sql As String = " Select DISTINCT country_node_id,country_name_th FROM _vw_Summary_Aid_Group_Country_By_Type ORDER BY country_name_th    " + Environment.NewLine
        Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("country_name_th"), DT.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub


End Class
