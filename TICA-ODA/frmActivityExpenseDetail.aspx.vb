﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class ActivityExpenseDetail
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectExpenseData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectExpenseData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property


    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property


    Public Property type As String
        'view/edit
        Get
            Try
                Return ViewState("type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("type") = value
        End Set
    End Property

    Public Property ActivityID As Long
        Get
            Try
                Return ViewState("ActivityID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ActivityID") = value
        End Set
    End Property

    Public Property PID As Long
        Get
            Try
                Return ViewState("PID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("PID") = value
        End Set
    End Property

    Public Property HeaderId As Long
        Get
            Try
                Return ViewState("HeaderId")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("HeaderId") = value
        End Set
    End Property

    Private Sub frmRename_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            mode = Request.QueryString("mode").ToString()
            ActivityID = CInt(Request.QueryString("ActivityID"))
            HeaderId = CInt(Request.QueryString("HeaderId"))
            PID = CInt(Request.QueryString("PID"))
            type = CInt(Request.QueryString("type"))
            BindList(ActivityID)
            SetExpenseInfo(ActivityID)
        End If
    End Sub

    Sub SetExpenseInfo(activity_id As String)
        txtPaymentDate.Text = ""
        If mode = "Add" Then
            Dim dtpj As New DataTable
            dtpj = BL.GetProjectInfoByIDAllType(activity_id)

            If dtpj.Rows.Count > 0 Then
                txtProjectName.Text = dtpj.Rows(0)("project_name").ToString
                txtActivitytName.Text = dtpj.Rows(0)("activity_name").ToString
            End If
        ElseIf mode = "Edit" Then
            Dim dtpj As New DataTable
            dtpj = BL.GetHeaderData(activity_id, HeaderId)

            If dtpj.Rows.Count > 0 Then
                txtProjectName.Text = dtpj.Rows(0)("project_name").ToString
                txtActivitytName.Text = dtpj.Rows(0)("activity_name").ToString
                'txtPaymentDate.Text = dtpj.Rows(0)("Payment_Date_Plan").ToString
                'If txtPaymentDate.Text <> "" Then
                'txtPaymentDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                'End If
                ' txtDetail.Text = dtpj.Rows(0)("Payment_Detail").ToString
            End If
        End If


    End Sub

    Sub BindList(activity_id As String)
        Dim dt As New DataTable
        dt = BL.GetProjectActivityWithRecipince(activity_id)


        rptList.DataSource = dt
        rptList.DataBind()

        AllData = dt
        Pager.SesssionSourceName = "ProjectExpenseData"
        Pager.RenderLayout()
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim UCdt As usercontrol_UCExpenseDetail = DirectCast(e.Item.FindControl("UCExpenseDetail"), usercontrol_UCExpenseDetail)
        Dim EditExpenseID As Long = 0

        Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

        Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

        Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)

        Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
        If rt = "G" Then
            Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
            If dt.Rows.Count > 0 Then
                UCdt.TemplateID = Template_id
                UCdt.setCol_rpt(Template_id)
                UCdt.setHead_rpt(Template_id)
                UCdt.EditExpenseID = HeaderId
                UCdt.ActivityID = ActivityID
                UCdt.setPlan_rpt(ActivityID, Template_id, HeaderId)
                UCdt.DetailDT = dt
            Else
                UCdt.Visible = False
            End If
        ElseIf rt = "R" Then
            Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
            If dt.Rows.Count > 0 Then
                UCdt.TemplateID = Template_id
                UCdt.setCol_rpt(Template_id)
                UCdt.setHead_rpt(Template_id)
                UCdt.EditExpenseID = HeaderId
                UCdt.ActivityID = ActivityID
                UCdt.setPlan_rpt(ActivityID, Template_id, HeaderId)
                UCdt.DetailDT = dt
            Else
                UCdt.Visible = False
            End If
        End If




    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '## Validate

            If txtPaymentDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่ต้องจ่าย');", True)
                Exit Sub
            End If

            Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            With lnqAEH
                .ID = HeaderId
                .ACTIVITY_ID = ActivityID
                '.PAYMENT_DATE_PLAN = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
                .PAYMENT_DETAIL = txtDetail.Text
            End With

            Dim dt As New DataTable
            With dt
                .Columns.Add("seq")
                .Columns.Add("activity_id")
                .Columns.Add("recipince_id")
                .Columns.Add("recipince_type")
                .Columns.Add("expense_sub_id")
                .Columns.Add("amount")
                .Columns.Add("Pay_Amount_Actual")
            End With

            Dim ret As New ProcessReturnInfo
            ' ret = BL.SaveExpenseDetail(lnqAEH, dt, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
                If type = "2" Then
                    Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                ElseIf type = "3" Then
                    Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                Else
                    Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                End If
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            End If

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If type = "2" Then
            Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "3" Then
            Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        Else

            Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
        End If

    End Sub
End Class
