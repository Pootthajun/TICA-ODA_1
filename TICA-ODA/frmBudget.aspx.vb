﻿Imports System.Data
Imports System.Globalization
Imports Constants
Partial Class frmBudget1
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim ChkYear As String = 0
    Public Property AllData As DataTable
        Get
            Try
                Return Session("BudgetPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("BudgetPage") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuAllocatedBudget")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aBudget")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_BudgetGroup(ddlGroupBudget)
            BindList("", "", "", "")
            myframe.Attributes.Add("src", "about: blank")
            Authorize()
            pnlAdSearch.Visible = False
        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If

        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Incoming_Budget & "' and isnull(is_save,'N') ='Y'")
        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            ColEdit.Visible = True
            thEdit.Visible = True
            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                ColEdit.Visible = False
                thEdit.Visible = False
                liedit.Visible = False
                lidelete.Visible = False
            End If
        Next


    End Sub

    Private Sub BindList(txtSearch As String, year As String, DateSearchS As String, DateSearchE As String)
        Dim Title As String = ""
        Dim DT As DataTable

        If ddlBudgetYear.SelectedIndex > 0 Then
            Title &= " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
        End If

        If ddlGroupBudget.SelectedIndex > 0 Then
            Title &= " กลุ่มงบประมาณ " & ddlGroupBudget.SelectedItem.ToString
        End If

        If DateSearchS <> "" And DateSearchE <> "" Then
            If Validate() = False Then
                Exit Sub
            End If
            Dim date1 As DateTime = DateTime.ParseExact(DateSearchS, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            Dim date2 As DateTime = DateTime.ParseExact(DateSearchE, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            'ใช้ค้นหาช่วงวันที่ " AND CONVERT(VARCHAR,b.received_date,112) Between dbo.GetDateFormatSearch(@_DateSearchS) AND dbo.GetDateFormatSearch(@_DateSearchE)"

            DT = BL.GetList_BudgetDetail(0, ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, StartDate, EndDate) 'Test

            If StartDate <> "" And EndDate <> "" Then
                Title &= " วันที่ได้รับงบประมาณ " & StartDate & " ถึงวันที่ " & EndDate
            End If

        Else
            DT = BL.GetList_BudgetDetail(0, ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, "", "")
        End If


        rptList.DataSource = DT
        rptList.DataBind()

        lblTotalList.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalList.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalList.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_Budget_Title") = lblTotalList.Text



        Session("Search_Budget") = DT

        Pager.SesssionSourceName = "Search_Budget"
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblBudgetYear As Label = DirectCast(e.Item.FindControl("lblBudgetYear"), Label)
        Dim lblBG As Label = DirectCast(e.Item.FindControl("lblBG"), Label)
        Dim lblCreatedDate As Label = DirectCast(e.Item.FindControl("lblCreatedDate"), Label)
        Dim lblBudget As Label = DirectCast(e.Item.FindControl("lblBudget"), Label)
        Dim lblBS As Label = DirectCast(e.Item.FindControl("lblBS"), Label)
        Dim footer As Label = DirectCast(e.Item.FindControl("footer"), Label)
        Dim footer1 As Label = DirectCast(e.Item.FindControl("footer1"), Label)

        'Dim Chk1 As String = e.Item.DataItem("budget_year").ToString
        lblBudgetYear.Text = e.Item.DataItem("budget_year").ToString
        'If ChkYear = Chk1 Then
        '    lblBudgetYear.Text = ""
        'Else

        '    ChkYear = e.Item.DataItem("budget_year").ToString
        '    'footer.Text = "<tr class=""bg-gray""><td style=""border:hidden""><center>งบประจำปี  " + ChkYear + "</center></td><td style=""border:hidden""></td><td style=""border:hidden""></td><td style=""border:hidden""></td><td style=""border:hidden""></td><td style=""border:hidden""></td></tr>"
        'End If


        lblBG.Text = e.Item.DataItem("group_name").ToString

        If Not IsDBNull(e.Item.DataItem("str_received_date")) Then
            lblCreatedDate.Text = e.Item.DataItem("str_received_date").ToString
        End If
        If Not IsDBNull(e.Item.DataItem("amount")) Then
            lblBudget.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
        End If
        lblBS.Text = e.Item.DataItem("sub_name").ToString
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Validate() Then
            BindList(ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, txtStartDate.Text, txtEndDate.Text)
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("frmBudgetDetail.aspx?id=0" & " &mode=" & "add")
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

    End Sub
    Protected Sub btnView_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Response.Redirect("frmBudgetDetail.aspx?id=" & _id & " &mode=" & "view")
    End Sub
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Response.Redirect("frmBudgetDetail.aspx?id=" & _id & " &mode=" & "edit")
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim _id As String = txtClickID.Text
        Dim ret As ProcessReturnInfo
        ret = BL.DeleteBudgetDetail(_id)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
        Else
            BindList(ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, txtStartDate.Text, txtEndDate.Text) 'Test
        End If
    End Sub

    Private Sub ddlBudgetYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBudgetYear.SelectedIndexChanged
        BindList(ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, txtStartDate.Text, txtEndDate.Text) 'Test
        pnlAdSearch.Visible = True
    End Sub

    Protected Sub ddlGroupBudget_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroupBudget.SelectedIndexChanged
        BindList(ddlGroupBudget.SelectedValue, ddlBudgetYear.SelectedValue, txtStartDate.Text, txtEndDate.Text) 'Test
        pnlAdSearch.Visible = True
    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


    'Function GetParameter(Reportformat As String) As String
    '    Dim para As String = "?ReportName=rptIncommingBudget"
    '    para += "&ReportFormat=" & Reportformat
    '    para += "&BudgetYear=" & ddlBudgetYear.SelectedValue
    '    para += "&TextSearch=" & ddlGroupBudget.SelectedValue
    '    Return para
    'End Function

    'Private Sub aPDF_ServerClick(sender As Object, e As EventArgs) Handles aPDF.ServerClick
    '    myframe.Attributes.Add("src", "about: blank")

    '    Dim para As String = GetParameter("PDF")
    '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
    'End Sub

    'Private Sub aEXCEL_ServerClick(sender As Object, e As EventArgs) Handles aEXCEL.ServerClick
    '    Dim para As String = GetParameter("EXCEL")
    '    'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
    '    myframe.Attributes.Add("src", "reports/frmPreviewReport.aspx" + para)
    'End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            Try
                Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                pnlAdSearch.Visible = True
                ret = False
                Exit Function
            End Try
        End If

        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
                alertmsg("กรุณาระบุวันที่เริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End If
            End  If

            If txtStartDate.Text <> "" Then
            If txtEndDate.Text = "" Then
                alertmsg("กรุณาระบุวันที่สิ้นสุด")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End If
        End If

        If txtEndDate.Text <> "" Then
            If txtStartDate.Text = "" Then
                alertmsg("กรุณาระบุวันที่เริ่มต้น")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End If
        End If
        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        BindList("", "", "", "")
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Function clearFormSearch()
        ddlBudgetYear.SelectedValue = ""
        ddlGroupBudget.SelectedValue = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
    End Function
End Class
