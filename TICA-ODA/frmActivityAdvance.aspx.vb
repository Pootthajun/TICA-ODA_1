﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Partial Class frmActivityAdvance
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property

    Public Property ActivityID As Long
        Get
            Try
                Return ViewState("ActivityID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ActivityID") = value
        End Set
    End Property

    Public Property Advance_ID As Long
        Get
            Try
                Return ViewState("Advance_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("Advance_ID") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Private Sub frmActivityAdvance_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ProjectID = CInt(Request.QueryString("PID"))
            ActivityID = CInt(Request.QueryString("ActivityID"))
            Advance_ID = CInt(Request.QueryString("Advance_ID"))
            SetHeader()
            mode = Request.QueryString("mode").ToString()
            If mode = "view" Then
                SetControlToViewMode(True)
            Else
                SetControlToViewMode(False)
            End If

        End If
    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)

        UCActivityAdvanceExpense.SetToViewMode(IsView)

    End Sub




    Public Sub SetHeader()

        Dim Sql As String = " "
        Sql &= "   Select  vw_Activity.project_name ,vw_Activity.activity_name  , TB_Activity_Advance.* ,TB_NAME,name_th_OUTB_OU_Country,name_th_OUTB_OU_Organize,name_th_OUTB_OU_Person " & vbLf
        Sql &= "   from TB_Activity_Advance  " & vbLf
        Sql &= "   Left Join vw_Activity ON vw_Activity.activity_id=TB_Activity_Advance.activity_id " & vbLf
        Sql &= "   LEFT JOIN vw_Overview_OU ON vw_Overview_OU.node_id=TB_Activity_Advance.Borrower_Node_ID " & vbLf
        Sql &= "   WHERE  Advance_ID =" & Advance_ID

        'Sql &= "   Select  vw_Activity.project_name ,vw_Activity.activity_name  , TB_Activity_Advance.* ,vw_ou.name_th,vw_ou.name_en " & vbLf
        'Sql &= "   from TB_Activity_Advance  " & vbLf
        'Sql &= "   Left Join vw_Activity ON vw_Activity.activity_id=TB_Activity_Advance.activity_id " & vbLf
        'Sql &= "   LEFT JOIN vw_ou ON vw_ou.node_id=TB_Activity_Advance.Borrower_Node_ID " & vbLf
        'Sql &= "   WHERE  Advance_ID =" & Advance_ID

        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblProjectName.Text = DT.Rows(0).Item("project_name").ToString()
            lblActivitytName.Text = DT.Rows(0).Item("activity_name").ToString()


            Select Case DT.Rows(0).Item("TB_NAME").ToString()
                Case "TB_OU_Country"
                    lblBorrowwer.Text = " ยืมโดยประเทศ : " & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()
                Case "TB_OU_Organize"
                    lblBorrowwer.Text = " ยืมโดยหน่วยงาน : " & DT.Rows(0).Item("name_th_OUTB_OU_Organize").ToString()
                    lblBorrowwer_Detail.Text = " ประเทศ" & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()

                Case "TB_OU_Person"
                    lblBorrowwer.Text = " ยืมโดย : " & DT.Rows(0).Item("name_th_OUTB_OU_Person").ToString()
                    lblBorrowwer_Detail.Text = " สังกัดหน่วยงาน : " & DT.Rows(0).Item("name_th_OUTB_OU_Organize").ToString() & "   ประเทศ" & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()

            End Select

        End If

        Dim Sql_List As String = " "
        Sql_List &= "   Select * from TB_Activity_Advance_Expense  " & vbLf
        Sql_List &= "   WHERE  Advance_ID =" & Advance_ID
        Sql_List &= "  ORDER BY Advance_Expense_Date "
        Dim DA_List As New SqlDataAdapter(Sql_List, BL.ConnectionString)
        Dim DT_List As New DataTable
        DA_List.Fill(DT_List)
        UCActivityAdvanceExpense.AdvanceDT = DT_List

    End Sub




#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAdvance_Expense_Detail.aspx?Mode=PDF&Advance_ID=" & Advance_ID & "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAdvance_Expense_Detail.aspx?Mode=EXCEL&Advance_ID=" & Advance_ID & "');", True)
    End Sub

#End Region









End Class
