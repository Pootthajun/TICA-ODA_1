﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="frmEditPlan.aspx.vb" Inherits="frmEditPlan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Plan and policies | ODA</title>

    <style>
        .calender-top {
            top: -8px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Plan and policies (นโยบายและแผนงาน) 
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
                <li><a href="frmPlan.aspx">Plan and policies</a></li>
                <li class="active">
                    <asp:Label ID="lblEditMode" runat="server" Text=""></asp:Label>
                    Plan and policies</li>
            </ol>
        </section>

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <!-- Main content -->
                <section class="content">
                    <br />
                    <!-- START CUSTOM TABS -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal Form -->
                            <div class="box">
                                <div class="box-header with-border bg-blue-gradient">
                                    <h4 class="box-title">
                                        <asp:Label ID="lblEditMode2" runat="server" Text=""></asp:Label>
                                        Plan and policies</h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Sector" class="col-sm-3 control-label">Plan and policies Name
                                                <br/><span style="color:red"></span></label>
                                            <div class="col-sm-7">

                                                <asp:TextBox ID="txtNames" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <br />
                                            <br />
                                        </div>

                                        <div class="form-group">
                                            <label for="Sector" class="col-sm-3 control-label">ชื่อนโยบายและแผนงาน
                                                <br/> <span style="color:red"></span></label>
                                            <div class="col-sm-7">

                                                <asp:TextBox ID="txtNames_TH" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <br />
                                            <br />
                                        </div>

                                        <div class="clearfix"></div>

                                        <%-- Calendar --%>
                                            <div class="form-group">
                                                <label for="Sector" class="col-sm-3 control-label">Start/End Date 
                                                    <br/>(วันเริ่มต้น/สิ้นสุด) : <span style="color:red">*</span></label>      
                                            </div>
                                            <div class="col-sm-3 calender-top">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder=""></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                        Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                </div>           
                                            </div>
                                            <div class="col-sm-1"> TO (ถึง)</div>
                                            <div class="col-sm-3 calender-top">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder=""></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                        Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                </div>           
                                            </div>

                                        <div class="clearfix"></div>

                                        <%-- Active Status --%>
                                        <div class="form-group">
                                            <br />
                                            <label for="inputname" class="col-sm-3 control-label">Active Status :</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" Checked="true" /></label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <div class="col-sm-8"></div>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                                                <i class="fa fa-save"></i>Save
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                                                <i class="fa fa-reply"></i>Cancel
                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                    <!-- /.box-footer -->

                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->

                    </div>
                    <!-- /.row -->
                    <!-- END CUSTOM TABS -->


                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


</asp:Content>



