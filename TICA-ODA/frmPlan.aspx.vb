﻿
Imports System.Data
Imports System.Globalization
Imports Constants
Partial Class frmPlan
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    'Page Navigation
    Public Property AllData As DataTable
        Get
            Try
                Return Session("PlanData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("PlanData") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aPlan")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            Authorize()
            pnlAdSearch.Visible = False
        End If

    End Sub

#Region "AuthorizeDT"

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Plan & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnStatusReady As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusReady"), LinkButton)
            Dim btnStatusDisabled As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusDisabled"), LinkButton)
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnStatusReady.Enabled = False
                btnStatusDisabled.Enabled = False
                HeadEdit.Visible = False
                ColEdit.Visible = False
            End If
        Next
    End Sub

#End Region


    Private Sub BindList()
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Validate() Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim DT As DataTable = BL.GetList_Plan(0, txtSearch.Text, StartDate, EndDate, ddlStatus.SelectedValue)

                rptList.DataSource = DT
                rptList.DataBind()
                lblTotalList.Text = DT.DefaultView.Count
                'Page Navigation
                AllData = DT
                Pager.SesssionSourceName = "PlanData"
                Pager.RenderLayout()

            End If
        Else
            Dim DT As DataTable = BL.GetList_Plan(0, txtSearch.Text, "", "", ddlStatus.SelectedValue)

            rptList.DataSource = DT
            rptList.DataBind()
            lblTotalList.Text = DT.DefaultView.Count
            'Page Navigation
            AllData = DT
            Pager.SesssionSourceName = "PlanData"
            Pager.RenderLayout()
        End If
    End Sub

    'Page Navigation
    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

#Region "Repeater"
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblPlanName As Label = DirectCast(e.Item.FindControl("lblPlanName"), Label)
        Dim lblPlanNameTh As Label = DirectCast(e.Item.FindControl("lblPlanNameTh"), Label)
        Dim lblStartDate As Label = DirectCast(e.Item.FindControl("lblStartDate"), Label)
        Dim lblEndDate As Label = DirectCast(e.Item.FindControl("lblEndDate"), Label)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        lblPlanName.Text = e.Item.DataItem("plan_name").ToString
        lblPlanNameTh.Text = e.Item.DataItem("plan_name_th").ToString
        lblStartDate.Text = e.Item.DataItem("str_start_date").ToString
        lblEndDate.Text = e.Item.DataItem("str_end_date").ToString


        Dim A As String = e.Item.DataItem("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False

        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Edit" Then
            Response.Redirect("frmEditPlan.aspx?id=" & e.CommandArgument)
        ElseIf e.CommandName = "Delete" Then
            Dim ret As Boolean = BL.DeletePlan(e.CommandArgument)
            If ret Then
                BindList()
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถลบข้อมูลได้');", True)
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllY(e.CommandArgument, "TB_Plan")
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
                BindList()
            End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllN(e.CommandArgument, "TB_Plan")
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('แก้ไขสถานะเรียบร้อยแล้ว');", True)
                BindList()
            End If
        End If

    End Sub
#End Region

#Region "Button"
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("frmEditPlan.aspx")
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Function clearFormSearch()
        txtSearch.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        ddlStatus.SelectedValue = ""
    End Function

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            Try
                Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End Try
        End If

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("กรุณาระบุวันที่เริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
            ret = False
            pnlAdSearch.Visible = True
            Exit Function
        End If

        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        btnSearch.Focus()
    End Sub
#End Region

End Class
