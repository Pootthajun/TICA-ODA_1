﻿<%@ Page Title="" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditCooperationType.aspx.vb" Inherits="frmEditCooperationType" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Cooperation Type | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <br /><br />

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cooperation Type (ประเภทความร่วมมือ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
            <li><a href="frmCooperationType.aspx"> Cooperation Type</a></li>
            <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> Cooperation Type</li>
          </ol>
        </section>

        <!-- Main content -->
         <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title"><asp:Label ID="lblEditMode2" runat="server" Text="Label"></asp:Label> Cooperation Type </h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Cooperation Type <br /> (ประเภทความร่วมมือ) :<span style="color:red">*</span></label>
                      <div class="col-sm-9">
                          
                       <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Description English <br /> (รายละเอียดภาษาอังกฤษ) :</label>
                      <div class="col-sm-9">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" placeholder="" ></asp:TextBox>
                      </div>
                    </div>
              <!-- /.box -->
                        
                  <div class="clearfix"></div>
                       <br />

                      <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Description Thai<br /> (รายละเอียดภาษาไทย) :</label>
                      <div class="col-sm-9">
                        <asp:TextBox ID="txtDescriptionth" runat="server" CssClass="form-control" placeholder="" ></asp:TextBox>
                      </div>
                    </div>
              <!-- /.box -->
                        
                  <div class="clearfix"></div>
                       <br />
                      
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Active Status :</label>
                      <div class="col-sm-9">
                     <label><asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" checked="true"/></label>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                   <div class="col-sm-9"></div>
                    <div class="col-lg-8"></div><div class="col-lg-2">
                       <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click"><i class="fa fa-save"></i>Save</asp:LinkButton>
                                                </div>
                    <div class="col-lg-2">
                   <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                       <i class="fa fa-reply"></i> Cancel

                   </asp:LinkButton>
                   

                    </div>
                  </div><!-- /.box-footer -->


                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
                 </ContentTemplate>
        </asp:UpdatePanel>
        </div>
      
<!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />    
  
</asp:Content>

