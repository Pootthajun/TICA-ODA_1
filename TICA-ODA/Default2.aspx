﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="keywords" content="jquery,ui,easy,easyui,web">
	<meta name="description" content="easyui help you build your web page easily!">
	<title>Complex TreeGrid - jQuery EasyUI Demo</title>
	<%--<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/demo/demo.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>--%>

       <link href="Scripts/treegrid/demo.css" rel="stylesheet" />
    <link href="Scripts/treegrid/easyui.css" rel="stylesheet" />
    <link href="Scripts/treegrid/icon.css" rel="stylesheet" />
    <script src="Scripts/treegrid/jquery-1.6.1.min.js"></script>
    <script src="Scripts/treegrid/jquery.easyui.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">


<h2>Complex TreeGrid Demo</h2>
	<div class="demo-info" style="margin-bottom:10px">
		<div class="demo-tip icon-tip">&nbsp;</div>
		<div>Using TreeGrid to show complex reports.</div>
	</div>
	


 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Project
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-area-chart"></i>Dashboard</a></li>
                <li><a href="#">Project Status</a></li>
                <li class="active">Project</li>
            </ol>
        </section>
        <br />


<div class="row">


	





            <div class="col-md-12">

              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-alt"></i> Infomation(ข้อมูลโครงการ)</a></li>
                  <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-th-list"></i> Activity(กิจกรรม)</a></li>
                  <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-users"></i> Recipience(ผู้รับทุน)</a></li>
                    <li class="dropdown pull-right" data-toggle="tooltip" title="เครื่องมือ">
                    <a class="dropdown-toggle text-muted" data-toggle="dropdown" href="#">
                      <i class="fa fa-gear"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmDetailActivityProject1.aspx"><i class="fa fa-edit text-blue"></i> Edit</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmPreviewProject.aspx"><i class="fa fa-print text-blue"></i> Print</a></li>
                      <li role="presentation" class="divider"></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash text-red"></i> Delete</a></li>
                    </ul>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">




                  

                    
                    <div class="box-body">
                      <table class="table table-bordered">
                       <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Informations (ข้อมูลทั่วไป) </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Project Title (ชื่อโครงการ) :</b></th>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556" disabled></textarea></td>
                        </tr>

                        <tr>
                          <th style="width: 250px"><b class="pull-right">Objectives (วัตถุประสงค์) :</b></th>
                          <td class="text-primary">
                            <textarea class="form-control" rows="4" placeholder="1. เพื่อพัฒนาบุคลากรของวิทยาลัยศิลปศึกษา ให้สำเร็จการศึกษาใน ป.โท และมีความรู้เฉพาะทางในด้านศิลปดนตรี และศิลปกรรม
	2. เพื่อให้การสนับสนุนอุปกรณ์ด้านศิลปกรรมและศิลปดนตรี ตามความจำเป็นและเหมาะสม 
	3. เพื่อให้วิทยาลัยมีความพร้อมในการเปิดการเรียนการสอนในระดับปริญญาตรี" disabled></textarea></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Description (รายละเอียด) :</b></th>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="หน่วยงานดำเนินงาน   
ฝ่ายไทย  :    มหาวิทยาลัยศรีนครินทรวิโรฒ    
ฝ่ายลาว   :    วิทยาลัยศิลปศึกษา กระทรวงศึกษาธิการและการกีฬา 
ระยะเวลาโครงการ  2 ปี ( 2012 – 2013)   ขยายระยะเวลาโครงการให้ดำเนินการในกิจกรรมที่ยังไม่แล้วเสร็จให้  สำเร็จ ภายในปี 2016  (ตามแผนงานด้านการศึกษาระยะที่ 2   ( 3 ปี  2014-2016) " disabled></textarea></td>
                        </tr>
                        
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Start/End Date </b><br /><b class="pull-right">(วันเริ่มต้น/สิ้นสุดโครงการ) :</b></th>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation" placeholder="24/03/2013 - 31/05/2015" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 280px"><b class="pull-right">Allocated  Budget </b><br /><b class="pull-right">(จัดสรรงบประมาณ) :</b></th>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Budget" placeholder="10,000,000.00 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Type Budget </b><br /><b class="pull-right">(ประเภทงบประมาณ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (N/A)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Cooperation Framework  </b><br /><b class="pull-right">(กรอบความร่วมมือ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (N/A)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Cooperation Type </b><br /><b class="pull-right">(ประเภทความร่วมมือ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (Bilateral)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                              </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">OECD Aid Type (ประเภท OECD):</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">N/A</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                  
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Location (พื้นที่โครงการ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">   
                                <input class="form-control" type="text" placeholder="" disabled>
                              </div></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Project Component </b><br /><b class="pull-right">(ประเภทความช่วยเหลือ) :</b></th>
                          <td class="text-primary">
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Component (ประเภทความช่วยเหลือ)</th>
                                  <th style="width: 340px" colspan="2">Budget (บาท)</th>
                                </tr>
                                <tr>
                                  <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Master</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                <td><div class="input-group">
                                 <div class="input-group-addon">
                                   <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Amount1" placeholder="884,802.00 บาท" disabled>
                               </div></td>
                              <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่มประภทความช่วยเหลือ"><i class="fa fa-plus"></i></a></td>
                            </tr>
                            <tr>
                                  <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                <td colspan="2">
                                    884,802.00 บาท</td>
                                </tr>
                         </table></td>
                        </tr>

                         <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Agency (หน่วยงาน) </b></h5></th>
                        </tr>
                        
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Funding Agency </b><br /><b class="pull-right">(หน่วยงานให้ความช่วยเหลือ) :</b></th>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">MINISTRY OF FOREIGN AFFAIRS</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Executing Agency </b><br /><b class="pull-right">(หน่วยงานดำเนินการ) :</b></th>
                          <td class="text-primary">
                          <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Thailand International Development Cooperation Agency (TICA)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group -->
                          </td>
                        </tr>
                        <tr>
                        <th style="width: 250px"><b class="pull-right">Implementing Agency </b><br /><b class="pull-right">(หน่วยงานดำเนินการ) :</b></th>
                      
                          <td class="text-primary">
                              <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Country (ประเทศ)</th>
                                  <th style="width: 350px" colspan="2">Agency (หน่วยงาน)</th>
                                </tr>
                             <tr>
                              <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Thailand</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                               <td><select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">มหาวิทยาลัยศรีนครินทรวิโรฒ</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                  <td style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่ม"><i class="fa fa-plus"></i></a></td>
                                </tr>
                              <tr>
                              <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Lao</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                               <td><select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">วิทยาลัยศิลปศึกษา กระทรวงศึกษาธิการและการกีฬา</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                  <td style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่ม"><i class="fa fa-plus"></i></a></td>
                                </tr>
                           </table>
                         </td>
                        </tr>

                         
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Co-Funding (ผู้ร่วมให้เงินทุน) :</b></th>
                          <td class="text-primary">
                            <table class="table table-bordered">
                                <tr class="bg-gray">
                                  <th>Country (ประเทศ)</th>
                                  <th style="width: 340px" colspan="2">Amount (จำนวนเงิน)</th>
                                </tr>
                                <tr>
                                  <td>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select></td>
                                <td><div class="input-group">
                                 <div class="input-group-addon">
                                   <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Amount2" placeholder="0.00 บาท" disabled>
                               </div></td>
                               <td style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="เพิ่มประภทความช่วยเหลือ"><i class="fa fa-plus"></i></a></td>
                                </tr>
                           </table></td>
                        </tr>
                        
                        
                        <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h5><b class="text-blue">Other Detail (รายละเอียดอื่นๆ) </b></h5></th>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Assistant (ผู้ช่วยโครงการ) :</b></th>
                          <td class="text-primary">
                                <select class="form-control select2" multiple="multiple" data-placeholder="-" style="width: 100%;" disabled>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                             </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Contact Person (ข้อมูลผู้ติดต่อ) :</b></th>
                              <td class="text-primary">
                                  <table class="table table-bordered">
                                    <tr>
                                      <th style="width: 150px" class="bg-gray"><p class="pull-right">Name (ชื่อ) :</p></th>
                                      <td>
                                        <div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-user"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                     </tr>
                                     <tr>
                                      <th style="width: 50px" class="bg-gray"><p class="pull-right">Position (ตำแหน่ง):</p></th>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-building"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px" class="bg-gray"><p class="pull-right">Phone (โทรศัพท์) :</p></th>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-phone"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="022035000" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px" class="bg-gray"><p class="pull-right">Fax (โทรสาร) :</p></th>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-fax"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <th style="width: 50px" class="bg-gray"><p class="pull-right">Email :</p></th>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-list-alt"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                   </table>
                               </td>
                        </tr>
                        <tr>
                          <th style="width: 150px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="-" disabled></textarea></td>
                        </tr>
                        <tr>
                          <th style="width: 150px"><b class="pull-right">File input (เพิ่มไฟล์) :</b></th>
                          <td class="text-primary"><input type="file" id="exampleInputFile"><br />
                                                   <input type="file" id="exampleInputFile"><br />
                                                   <input type="file" id="exampleInputFile">
                          </td>
                        </tr>
                        <tr>
                          <th style="width: 250px"><b class="pull-right">Transfer Project To </b><br /><b class="pull-right">(มอบหมายโครงการให้) :</b></th>
                          <td class="text-primary"> 
                             <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                              </div>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                      </table>

                    <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                    </div>
                    </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->
                  
                <div class="tab-pane" id="tab_2">

                 <%-- <div id="gantt_here" style='width:100%; height:480px;'></div>  
                  
                  <br />

                      <div class='gantt_cal_ltitle'><span class='gantt_mark'>&nbsp;</span><span class='gantt_time'></span><span class='gantt_title'></span><div class='gantt_cancel_btn'></div>
                      
                      </div>
                      <div class='gantt_cal_larea'></div>

                   <script src="dist/Gantter/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
                   <link rel="stylesheet" href="dist/Gantter/codebase/skins/dhtmlxgantt_meadow.css" type="text/css" media="screen" title="no title" charset="utf-8">

                    <script type="text/javascript" src="dist/Gantter/common/testdata.js"></script>
                    
                        <script type="text/javascript">
	                    gantt.init("gantt_here");

	                    gantt.templates.rightside_text = function(start, end, task){
		                    return "ID: #" + task.id;
	                    };

	                    gantt.templates.leftside_text = function(start, end, task){
		                    return task.duration + " days";
	                    };
	                    gantt.parse(demo_tasks);
                    </script>

                    <script type="text/javascript">
                            var demo_tasks = {
                                "data": [
                                    { "id": 10, "text": "กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย", "start_date": "", "end_date": "", "duration": "", "budget": "100000", "progress": 0.6, "open": true },
                
                                    { "id": 11, "text": "1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท.", "start_date": "03-10-2016", "end_date": "24-10-2017", "duration": "40", "budget": "8100000", "parent": "10", "progress": 1, "open": true },
                                    { "id": 12, "text": "1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน", "start_date": "10-10-2016", "end_date": "11-10-2016", "duration": "14", "budget": "3700000", "parent": "10", "progress": 0.2, "open": true },


                                    { "id": 13, "text": "กิจกรรมที่ 2 แผนงานถ่ายทอดความรู้     ", "start_date": "", "end_date": "", "duration": "", "budget": "100000", "progress": 0.6, "open": true },
                                    { "id": 14, "text": "2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี  ", "start_date": "03-10-2016", "end_date": "13-10-2018", "duration": "24", "budget": "1900000", "parent": "13", "progress": 0.5, "open": true },

                                    { "id": 15, "text": "กิจกรรมที่ 3 แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด  ", "start_date": "", "start_date": "", "duration": "", "budget": "2700000", "progress": 0.6, "open": true },

                                    { "id": 16, "text": "กิจกรรมที่ 4 แผนงานติดตามและประเมินผล", "start_date": "", "start_date": "", "duration": "", "budget": "2700000", "progress": 0.6, "open": true },
                                    { "id": 17, "text": "4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง", "start_date": "29-09-2016", "end_date": "29-09-2017", "duration": "4", "parent": "16", "budget": "2700000", "progress": 0.1, "open": true },
                                    { "id": 18, "text": "4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา", "start_date": "29-09-2016", "end_date": "29-09-2017", "duration": "4", "parent": "16", "budget": "2700000", "progress": 0.1, "open": true },

                                ]
                            };

                            gantt.init("gantt_here");

                            gantt.parse(demo_tasks);


                        </script>--%>
               

                    <%----------------------- Modal -------------------------%>

                       <%-- <form id="form1" runat="server" >
                        <asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>--%>
                       
                        
                         <%--</form>--%>
                  </div><!-- /.tab-pane -->


                  <div class="tab-pane" id="tab_3">
                <%--  <div class="box-header with-border">
                                
                             <div class="col-lg-5"><br />
                                <h5 class="text-primary"><b>พบทั้งหมด 4 รายการ</b></h5>
                             </div>

                            <div class="col-lg-5">
                                <div class="input-group margin">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search" data-toggle="tooltip" title="ค้นหา"></i></button>
                                </span>
                              </div><!-- /input-group -->
                           </div>
                           <div class="col-lg-2">
                                <div class="input-group margin">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                    <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                    <i class="fa fa-magic"></i>
                                    </button></a>
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       <div id="collap1" class="panel-collapse collapse">
                           
                        <div class="box-header with-border">
                            <div class="box-body">
                                <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Student ID :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>

                                       <label for="Sector" class="col-sm-2 control-label">Country :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                   </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Course (หลักสูตร) :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                  
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                      <label for="inputname" class="col-sm-2 control-label">Status :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>


                                  </div><!-- /.box-body -->
                                  </div>
                                </form>
                               
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                           
                        </div>
                        </div>
                      </div>--%>

                 <table class="table">
                      <tr class="bg-gray text-center">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                      <th>Start-End Date</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                  </table>
                
                   </div><!-- /.tab-content -->
              
                </div><!-- nav-tabs-custom -->

              </div><!-- /.col -->
            </div>
          </div>


      
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <input type="button" id="btnOpenModal" onclick="fnClickButton();" value="JSButton" />
            <asp:TextBox ID="txtTextData" runat="server"></asp:TextBox>


            <asp:Button ID="btnServerModal" runat="server" Text="OpenJSButton"></asp:Button>

            <asp:Panel CssClass="modal" ID="pnlDialog" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active bg-aqua"><a href="#Info" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                    <li class="bg-aqua"><a href="#Allocated" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                    <li class="bg-aqua"><a href="#Recipient" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="Info">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Course (หลักสูตร) :</b></th>
                                                <td class="text-primary">
                                                    <asp:TextBox class="form-control" Rows="3" ID="txtDialogData" runat="server" placeholder="การประชุมคณะกรรมการดำเนินการโครงการฯฝ่ายไทยและกัมพูชา" TextMode="MultiLine"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Description (รายละเอียด) :</b></th>
                                                <td class="text-primary">
                                                    <textarea class="form-control" rows="3" placeholder="มีผู้รับทุนเป็นคณะผู้บริหาร ครู นักเรียนและประชาชนในชุมชนบริเวณใกล้เคียงวิทยาลัย กำปงเฌอเตียล ราชอาณาจักรกัมพูชา"></textarea></td>
                                            </tr>

                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Sector (สาขา) :</b></th>
                                                <td class="text-primary">
                                                    <select class="form-control select2" style="width: 100%;">
                                                        <option selected="selected">-</option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                    </select></td>
                                            </tr>

                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Sub Sector (สาขาย่อย) :</b></th>
                                                <td class="text-primary">
                                                    <select class="form-control select2" style="width: 100%;">
                                                        <option selected="selected">-</option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">ประกาศรับสมัครผู้รับทุน :</b></th>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-sm-9">
                                                            <label>
                                                                <input type="checkbox" class="minimal"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="tab-pane" id="Allocated">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Plan(Start/End Date) </b>
                                                    <br />
                                                    <b class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด) :</b></th>
                                                <td class="text-primary">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="reservation" placeholder="10/01/2015-15/01/2015">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Actual (Start/End Date) </b>
                                                    <br />
                                                    <b class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</b></th>
                                                <td class="text-primary">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="reservation1" placeholder="21/01/2015-21/01/2015">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Commitment Budget </b>
                                                    <br />
                                                    <b class="pull-right">(จัดสรรงบประมาณ) :</b></th>
                                                <td class="text-success">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-dollar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="BudgetActivity" placeholder="2,000,000,000 บาท">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Disbursement (จ่ายจริง) :</b></th>
                                                <td class="text-success">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-dollar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="Disbursement" placeholder="0 บาท">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Administrative </b>
                                                    <br />
                                                    <b class="pull-right">(ค่าบริหารจัดการ):</b></th>
                                                <td class="text-success">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-dollar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="Administrative" placeholder="0 บาท">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>

                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">In kind</b><br />
                                                    <b class="pull-right">(ความช่วยเหลือด้านอื่นๆ):</b></th>
                                                <td>
                                                    <select class="form-control select2" style="width: 100%;">
                                                        <option selected="selected">Select</option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                        <option></option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Amount (จำนวนเงิน) :</b></th>
                                                <td class="text-success">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-dollar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="Inkind" placeholder="0 บาท">
                                                    </div>
                                                    <!-- /.input group -->
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                    <!-- /.tab-pane -->

                                    <div class="tab-pane" id="Recipient">

                                        <table class="table table-bordered">
                                            <tr>
                                                <td></td>
                                                <td><a href="frmRecipentTotalProject.aspx" target="_blank"><span class="badge bg-yellow" data-toggle="tooltip" title="ข้อมูลรายชื่อผู้รับทุน">Recipient (ผู้รับทุน)</span></a><br />
                                                    <a href="frmDetaiAllocated1.aspx" target="_blank"><span class="badge bg-green" data-toggle="tooltip" title="ข้อมูลงบประมาณ">Allocated (จัดสรรงบประมาณ)</span></a></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Recipient Country</b><br />
                                                    <b class="pull-right">(ประเทศที่รับทุน) :</b></th>
                                                <td class="text-primary">
                                                    <div class="col-sm-12">
                                                        <select class="form-control select2" multiple="multiple" data-placeholder="Cambodia" style="width: 100%;">
                                                            <option>Thailand</option>
                                                            <option>Lao</option>
                                                            <option>Malaysia</option>
                                                            <option></option>
                                                            <option>Vietnam</option>
                                                            <option>Singapore</option>
                                                            <option>Chinese</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 180px"><b class="pull-right">Benificiary</b><br />
                                                    <b class="pull-right">(ผู้รับผลประโยชน์) :</b></th>
                                                <td class="text-primary">
                                                    <textarea class="form-control" rows="3" placeholder=""></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- nav-tabs-custom -->

                            <div class="modal-footer">
                                <asp:LinkButton ID="btnPersonSave" runat="server" CssClass="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnPersonCancel" runat="server" CssClass="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>


            <script type="text/javascript">
                function fnClickButton() {
                    alert("AAAAA");
                    return false;

                    document.getElementById('<%= btnServerModal.ClientID %>').click();
                                        }

            </script>
        </ContentTemplate>

    </asp:UpdatePanel>
      </div>
      <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>
</asp:Content>

