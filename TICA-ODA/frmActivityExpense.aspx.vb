﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports Constants

Partial Class frmActivityExpense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectExpenseData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectExpenseData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property


    Public Property type As String
        'view/edit
        Get
            Try
                Return ViewState("type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("type") = value
        End Set
    End Property

    Public Property ActivityID As Long
        Get
            Try
                Return ViewState("ActivityID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ActivityID") = value
        End Set
    End Property

    Public Property PID As Long
        Get
            Try
                Return ViewState("PID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("PID") = value
        End Set
    End Property

    Public Property HeaderId As Long
        Get
            Try
                Return ViewState("HeaderId")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("HeaderId") = value
        End Set
    End Property

    Public Property Recipient_Type As String
        Get
            Try
                Return ViewState("Recipient_Type")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            ViewState("Recipient_Type") = value
        End Set
    End Property


    Private Sub frmActivityExpense_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            mode = Request.QueryString("mode").ToString()
            ActivityID = CInt(Request.QueryString("ActivityID"))
            HeaderId = CInt(Request.QueryString("HeaderId"))
            PID = CInt(Request.QueryString("PID"))
            type = CInt(Request.QueryString("type"))
            BindList(ActivityID)
            lnkTempRecord_Back.Visible = False
            PanelListTempRecord.Visible = False
            btnSave.Visible = True
            btnCancel.Visible = True


            If mode = "view" Then
                SetControlToViewMode(True)
            Else
                SetControlToViewMode(False)
            End If
            Authorize()
        Else
            If mode <> "view" Then
                If lnkTempRecord_Back.Visible Then
                    btnSave.Visible = False
                    btnCancel.Visible = False
                Else
                    btnSave.Visible = True
                    btnCancel.Visible = True
                End If
            End If


        End If
    End Sub

    'Private Sub Tang()
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Tang", "Tang();", True)
    'End Sub

#Region "AuthorizeDT"

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property
    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property
    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        ProjectType = CInt(Request.QueryString("type"))
        Dim tmpdr() As DataRow
        If ProjectType = Constants.Project_Type.Project Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Project & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.NonProject Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Non_Project & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.Loan Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Loan & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.Contribuition Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Contribution & "' and isnull(is_save,'N') ='Y'")
        End If

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then



        End If


    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)
        txtPaymentStartDate.Enabled = Not IsView
        txtPaymentEndDate.Enabled = Not IsView

        lnkTempRecord_Plan.Visible = Not IsView
        lnkTempRecord_Actual.Visible = Not IsView

        btnSave.Enabled = Not IsView
        btnSave.Visible = Not IsView

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim PanelBudget As Panel = DirectCast(rptList.Items(i).FindControl("PanelBudget"), Panel)
            Dim PanelExpense As Panel = DirectCast(rptList.Items(i).FindControl("PanelExpense"), Panel)
            Dim UCActivityExpensePlan As usercontrol_UCActivityExpensePlan = DirectCast(rptList.Items(i).FindControl("UCActivityExpensePlan"), usercontrol_UCActivityExpensePlan)
            Dim UCActivityExpenseActual As usercontrol_UCActivityExpenseActual = DirectCast(rptList.Items(i).FindControl("UCActivityExpenseActual"), usercontrol_UCActivityExpenseActual)

            PanelBudget.Enabled = Not IsView
            PanelExpense.Enabled = Not IsView
            UCActivityExpensePlan.SetToViewMode(IsView)
            UCActivityExpenseActual.SetToViewMode(IsView)

        Next


    End Sub

#End Region





    Sub BindList(activity_id As String)

        txtPaymentStartDate.Text = ""
        txtPaymentEndDate.Text = ""
        Dim dtpj As New DataTable
        dtpj = BL.GetHeaderData(activity_id, HeaderId)

        If dtpj.Rows.Count > 0 Then
            txtProjectName.Text = dtpj.Rows(0)("project_name").ToString
            If dtpj.Rows(0)("activity_name").ToString <> "Null" Then
                Dim Acname As String = dtpj.Rows(0)("activity_name").ToString
                txtActivitytName.Text = "ส่วนการเงินของ " + Acname + " จาก "
            End If

            If Not IsDBNull(dtpj.Rows(0)("Payment_Date_Start")) Then
                txtPaymentStartDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_Start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If Not IsDBNull(dtpj.Rows(0)("Payment_Date_End")) Then
                txtPaymentEndDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_End")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            'txtPaymentStartDate.Text = dtpj.Rows(0)("Payment_Date_Start").ToString
            'If txtPaymentStartDate.Text <> "" Then

            '    'txtPaymentStartDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_Start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            '    txtPaymentStartDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_Start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))

            'End If
            'txtPaymentEndDate.Text = dtpj.Rows(0)("Payment_Date_End").ToString
            '    If txtPaymentEndDate.Text <> "" Then
            '        txtPaymentEndDate.Text = Convert.ToDateTime(dtpj.Rows(0)("Payment_Date_End")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            '    End If
        End If
        Dim dt As New DataTable

        Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)
        Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
        Recipient_Type = rt
        If rt = "R" Then
            dt = BL.GetActivityExpenseDataIntoTypeR(activity_id, HeaderId)
        ElseIf rt = "C" Then
            dt = BL.GetActivityExpenseDataIntoTypeC(activity_id, HeaderId)
        ElseIf rt = "O" Then
            dt = BL.GetActivityExpenseDataIntoTypeO(activity_id, HeaderId)
        ElseIf rt = "G" Then
            dt = BL.GetActivityExpenseDataIntoTypeG(activity_id, HeaderId)
        ElseIf rt = "M" Then
            dt = BL.GetActivityExpenseDataIntoTypeM(activity_id, HeaderId)
        End If


        rptList.DataSource = dt
        rptList.DataBind()

        AllData = dt
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblbudget As Label = DirectCast(e.Item.FindControl("lblbudget"), Label)
        Dim lblExpense As Label = DirectCast(e.Item.FindControl("lblExpense"), Label)
        Dim lblTotal As Label = DirectCast(e.Item.FindControl("lblTotal"), Label)
        Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)
        Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
        Dim img As HtmlImage = DirectCast(e.Item.FindControl("imgicon"), HtmlImage)
        Dim tdBudget As HtmlTableCell = DirectCast(e.Item.FindControl("tdBudget"), HtmlTableCell)
        Dim tdExpense As HtmlTableCell = DirectCast(e.Item.FindControl("tdExpense"), HtmlTableCell)

        Dim lblcountryid As String = e.Item.DataItem("country_id").ToString
        img.Src = ""
        If Recipient_Type <> "M" Then
            img.Src = "~/Flag/" & lblcountryid & ".png"
        End If
        lblName.Text = e.Item.DataItem("Name").ToString
        lblbudget.Text = "งบประมาณ " + Convert.ToDecimal(e.Item.DataItem("budget")).ToString("#,##0.00") + " บาท"
        lblExpense.Text = "ค่าใช้จ่าย " + Convert.ToDecimal(e.Item.DataItem("Expense")).ToString("#,##0.00") + " บาท"
        Dim sum As Double = e.Item.DataItem("budget") - e.Item.DataItem("Expense")
        lblTotal.Text = "คงเหลือ " + Convert.ToDecimal(sum).ToString("#,##0.00") + " บาท"
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        'ใช้ในเครื่อง 88
        Dim tdBudget As HtmlTableCell = DirectCast(e.Item.FindControl("tdBudget"), HtmlTableCell)
        Dim tdExpense As HtmlTableCell = DirectCast(e.Item.FindControl("tdExpense"), HtmlTableCell)
        Dim lblbudget As Label = DirectCast(e.Item.FindControl("lblbudget"), Label)
        Dim lblExpense As Label = DirectCast(e.Item.FindControl("lblExpense"), Label)

        tdExpense.Style("Background-color") = ""
        tdBudget.Style("Background-color") = ""
        lblbudget.Style("color") = "black"
        lblExpense.Style("color") = "black"

        Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
        With lnqAEH
            .ID = HeaderId
            .ACTIVITY_ID = ActivityID

            If txtPaymentStartDate.Text <> "" Then
                Dim a As String = txtPaymentStartDate.Text
                Dim d As String() = a.Split("/")
                Dim y As Integer = Convert.ToInt32(d(2)) - 543
                If y < 1997 Then
                    y += 543
                End If
                Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
            End If
            If txtPaymentEndDate.Text <> "" Then
                Dim endtext As String = txtPaymentEndDate.Text
                Dim splitEnd As String() = endtext.Split("/")
                Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
                If setEndyear < 1997 Then
                    setEndyear += 543
                End If
                Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End If
        End With
        Dim ret As New ProcessReturnInfo
        ret = BL.SaveHeader(lnqAEH, UserName)


        Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)
        Dim Template_id As Integer
        If dtTemplate.Rows.Count > 0 Then
            Template_id = dtTemplate.Rows(0)("template_id").ToString()
        Else
            Template_id = 0
        End If

        If e.CommandName = "budget" Then

            tdBudget.Style("Background-color") = "#1766a9"
            lblbudget.Style("color") = "white"

            Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
            Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)

            Dim EditExpenseID As Long = 0



            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)
            If Recip.Rows.Count > 0 Then



                Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
                Session("Recipient_Type") = rt

                Dim UCdt As usercontrol_UCActivityExpensePlan = DirectCast(e.Item.FindControl("UCActivityExpensePlan"), usercontrol_UCActivityExpensePlan)
                If rt = "G" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                ElseIf rt = "C" Then
                    Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                ElseIf rt = "R" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                ElseIf rt = "O" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceOrganize(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If

                ElseIf rt = "M" Then
                    Dim dt As DataTable = BL.GetRecipinceMultilateral(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                End If

            End If

            PanelExpense.Visible = False
            PanelBudget.Visible = True
        ElseIf e.CommandName = "expense" Then

            tdExpense.Style("Background-color") = "#1766a9"
            lblExpense.Style("color") = "white"

            Dim PanelExpense As Panel = DirectCast(e.Item.FindControl("PanelExpense"), Panel)
            Dim PanelBudget As Panel = DirectCast(e.Item.FindControl("PanelBudget"), Panel)

            Dim EditExpenseID As Long = 0

            'Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)

            'Dim Template_id As Long = dtTemplate.Rows(0)("template_id").ToString()

            Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)
            If Recip.Rows.Count > 0 Then

                Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
                Dim UCdt As usercontrol_UCActivityExpenseActual = DirectCast(e.Item.FindControl("UCActivityExpenseActual"), usercontrol_UCActivityExpenseActual)
                If rt = "G" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceGroup(ActivityID)
                    'Dim reid As String
                    'reid = BL.GetCountry_ById(ID)
                    'lblRecipience.Text = reid
                    'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                ElseIf rt = "C" Then
                    Dim dt As DataTable = BL.GetRecipinceCountry(ActivityID)
                    'Dim reid As String
                    'reid = BL.GetCountryRe_ById(ID)
                    'lblRecipience.Text = reid
                    'txtNameRecipience.Text = "ประเทศ" + BL.GetCountryName_ByCountryID(reid)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If

                ElseIf rt = "R" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceIndividual(ActivityID)
                    'Dim reid As String
                    'reid = BL.GetRecipience_ById(ID)
                    'lblRecipience.Text = reid
                    'txtNameRecipience.Text = BL.GetRecipienceName_ById(reid)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If
                ElseIf rt = "O" Then
                    Dim dt As DataTable = BL.GetActivityRecipinceOrganize(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If

                ElseIf rt = "M" Then
                    Dim dt As DataTable = BL.GetRecipinceMultilateral(ActivityID)
                    If dt.Rows.Count > 0 Then
                        UCdt.TemplateID = Template_id
                        UCdt.RecipienceID = e.CommandArgument
                        UCdt.ActivityID = ActivityID
                        UCdt.HeaderID = HeaderId
                        UCdt.UserName = UserName
                        UCdt.Type = type
                        UCdt.PID = PID
                        UCdt.SetHead(ActivityID)
                        UCdt.SetDataRpt(HeaderId)
                        UCdt.SetDataRptSum()
                        UCdt.ComponentDT = dt
                        UCdt.Visible = True
                    Else
                        UCdt.Visible = False
                    End If

                End If

            End If

            PanelExpense.Visible = True
            PanelBudget.Visible = False
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '## Validate

            If txtPaymentStartDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่เริ่มต้น');", True)
                Exit Sub
            End If
            If txtPaymentEndDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่สิ้นสุด');", True)
                Exit Sub
            End If
            'ใช้ในเครื่อง 88
            Dim a As String = txtPaymentStartDate.Text
            'แยกวัน เดือน ปี
            Dim d As String() = a.Split("/")
            'แปลง ค.ศ.เป็น พ.ศ.
            Dim y As Integer = Convert.ToInt32(d(2)) - 543
            If y < 1997 Then
                y += 543
            End If
            'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
            Dim z As String = ((d(0) & "/") + d(1) & "/") + y.ToString()

            Dim endtext As String = txtPaymentEndDate.Text
            'แยกวัน เดือน ปี
            Dim splitEnd As String() = endtext.Split("/")
            'แปลง ค.ศ.เป็น พ.ศ.
            Dim setEndyear As Integer = Convert.ToInt32(splitEnd(2)) - 543
            If setEndyear < 1997 Then
                setEndyear += 543
            End If
            'มาจัดรูปแบบใหม่ว่าต้องการแบบไหน
            Dim textend As String = ((splitEnd(0) & "/") + splitEnd(1) & "/") + setEndyear.ToString()

            Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            With lnqAEH
                .ID = HeaderId
                .ACTIVITY_ID = ActivityID
                .PAYMENT_DATE_START = Converter.StringToDate(z, "dd/MM/yyyy")
                .PAYMENT_DATE_END = Converter.StringToDate(textend, "dd/MM/yyyy")
            End With

            Dim ret As New ProcessReturnInfo
            ret = BL.SaveHeader(lnqAEH, UserName)
            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
                If type = "2" Then
                    Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                ElseIf type = "3" Then
                    Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                ElseIf type = "1" Or type = "0" Then
                    Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=palnex" & "&type=" & type & "&mo=" & ActivityID)
                Else
                    Response.Redirect("frmOverduePlan.aspx")
                End If
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            End If

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If type = "2" Then
            Response.Redirect("frmLoan_Detail_Activity.aspx?id=" & PID & "&mode=" & mode & "&back=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "3" Then
            Response.Redirect("frmContribuiltion_Detail_Activity.aspx?id=" & PID & "&mode=" & mode & "&back=palnex" & "&type=" & type & "&mo=" & ActivityID)
        ElseIf type = "1" Or type = "0" Then
            Response.Redirect("frmProject_Detail_Activity.aspx?id=" & PID & "&mode=" & mode & "&back=palnex" & "&type=" & type & "&mo=" & ActivityID)
        Else
            Response.Redirect("frmOverduePlan.aspx")
        End If

    End Sub

    Public Sub SendData()
        Dim dtTemplate As DataTable = BL.GetTemplateActivity(ActivityID)
        Dim Template_id As Integer
        If dtTemplate.Rows.Count > 0 Then
            Template_id = dtTemplate.Rows(0)("template_id").ToString()
        Else
            Template_id = 0
        End If

        UCActivityExpenseAVGRecord.TemplateID = Template_id
        Dim Recip As DataTable = BL.GetRecipinceType(ActivityID)
        Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
        Dim dt As New DataTable
        If rt = "R" Then
            dt = BL.GetActivityExpenseDataIntoTypeR(ActivityID, HeaderId)
        ElseIf rt = "C" Then
            dt = BL.GetActivityExpenseDataIntoTypeC(ActivityID, HeaderId)
        ElseIf rt = "O" Then
            dt = BL.GetActivityExpenseDataIntoTypeO(ActivityID, HeaderId)
        ElseIf rt = "G" Then
            dt = BL.GetActivityExpenseDataIntoTypeG(ActivityID, HeaderId)
        End If

        'UCActivityExpenseAVGRecord.SetHead(ActivityID, HeaderId)
        UCActivityExpenseAVGRecord.SetDataRptSum()
    End Sub

    Private Sub lnkTempRecord_Plan_Click(sender As Object, e As EventArgs) Handles lnkTempRecord_Plan.Click

        pnlFooter_btnSave.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False

        UCActivityExpenseAVGRecord.SetHead(ActivityID, HeaderId, "Plan")


        PanelListPlan.Visible = False
        PanelListTempRecord.Visible = True

        lnkTempRecord_Plan.Visible = False
        lnkTempRecord_Actual.Visible = False

        lnkTempRecord_Back.Visible = True


        lblTitle_TempRecord.Text = "เพิ่มรายการข้อมูลงบประมาณ"

    End Sub

    Private Sub lnkTempRecord_Back_Click(sender As Object, e As EventArgs) Handles lnkTempRecord_Back.Click

        PanelListPlan.Visible = True
        PanelListTempRecord.Visible = False

        lnkTempRecord_Plan.Visible = True
        lnkTempRecord_Actual.Visible = True
        lnkTempRecord_Back.Visible = False

        pnlFooter_btnSave.Visible = True
        btnSave.Visible = True
        btnCancel.Visible = True
        BindList(ActivityID)
    End Sub

    Private Sub lnkTempRecord_Actual_Click(sender As Object, e As EventArgs) Handles lnkTempRecord_Actual.Click

        pnlFooter_btnSave.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False

        UCActivityExpenseAVGRecord.SetHead(ActivityID, HeaderId, "Actual")


        PanelListPlan.Visible = False
        PanelListTempRecord.Visible = True

        lnkTempRecord_Plan.Visible = False
        lnkTempRecord_Actual.Visible = False

        lnkTempRecord_Back.Visible = True

        lblTitle_TempRecord.Text = "เพิ่มรายการข้อมูลค่าใช้จ่าย"

    End Sub
End Class
