﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmProject - Copy.aspx.vb" Inherits="frmProject" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Project | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <asp:Label ID="lblHeadProjectType" runat="server" Text=""></asp:Label>
            </h1>
            <ol class="breadcrumb">
                <li><a href="frmProjectStatus.aspx"><i class="fa fa-area-chart"></i>Overall</a></li>
                <li class="active">
                    <asp:Label ID="lblProjectType" runat="server" Text=""></asp:Label>
                </li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                                <i class="fa fa-plus"></i>
                                                <asp:Label ID="lblAddProjectType" runat="server"></asp:Label>
                                            </asp:LinkButton>
                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <div class="col-sm-1">
                                            </div>
                                            <table>
                                                <tr>
                                                    <td width="200px" height="50">Project Code : </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtSearchID" runat="server" placeholder="รหัสโครงการ" CssClass="form-control"></asp:TextBox></td>
                                                </tr>

                                                <tr>
                                                    <td width="200px" height="50">Project name :</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="ชื่อโครงการ" CssClass="form-control"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Budget : </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtSearchBudgetStart" runat="server" Style="text-align: right;" CssClass="form-control" AutoPostBack="true" placeholder="จำนวนงบประมาณเริ่มต้น" MaxLength="12"></asp:TextBox>
                                                    </td>
                                                    <td width="50px" style="text-align: center">- </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtSearchBudgetEnd" runat="server" Style="text-align: right;" CssClass="form-control" AutoPostBack="true" placeholder="จำนวนงบประมาณสิ้นสุด" MaxLength="12"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Project Time : </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>

                                                    </td>
                                                    <td width="50px" style="text-align: center">- </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Beneticiary Country : </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtserachCountry" runat="server" placeholder="ประเทศผู้ได้รับทุน" CssClass="form-control"></asp:TextBox></td>
                                                    <td></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td  width="200px" height="50">Balance Status : </td>
                                                    <td><asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All Balance</asp:ListItem>
                                                        <asp:ListItem Value="0">Finish</asp:ListItem>
                                                        <asp:ListItem Value="1">UnFinish</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"> ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                            

                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ</h4>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-xs-6 col-sm-10 ">
                                                
                                            </div>

                                            <div class="col-xs-6 col-sm-2">
                                                
                                            </div>
                                        </div>

                                    </div>
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="project">Project ID<br />
                                                        (รหัส)</th>
                                                    <th>Description<br />
                                                        (รายละเอียด)</th>
                                                    <th>Budget<br />
                                                        (งบประมาณ)</th>
                                                    <th>Disbursement<br />
                                                        (จ่ายจริง)</th>
                                                    <th>Balance<br />
                                                        (คงเหลือ)</th>
                                                    <th class="tools">Tools<br />
                                                        (เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Project ID" class="center">
                                                                <asp:Label ID="lblProjectID" runat="server"></asp:Label></td>
                                                            <td data-title="Description" id="td" runat="server">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblPlanDate" runat="server" ForeColor="Blue"></asp:Label>

                                                            </td>
                                                            <td data-title="Budget" style="text-align: right;" width="120;">
                                                                <asp:Label ID="lblSUM_Budget" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Disbursement" style="text-align: right;" width="120;">
                                                                <asp:Label ID="lblDisbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Balance" style="text-align: right;" width="120;">
                                                                <asp:Label ID="lblBalance" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="center" width="4%">

                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li id="liview" runat="server"  Visible ="false" ><a href="javascript:;" onclick='btnViewClick(<%#Eval("ID") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>
                                                                        <li id="liedit" runat="server"><a href="javascript:;" onclick='btnEditClick(<%#Eval("ID") %>)'><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                                                                        <li class="divider"></li>
                                                                        <li id="lidelete" runat="server"><a href="javascript:;" onclick='btnDeleteClick(<%#Eval("ID") %>)'><i class="fa fa-remove text-red"></i>ลบ</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="No" class="center" id="tr1" runat="server" visible="false"></td>
                                                        <td style="text-align: center;"><b>Total</b></td>

                                                        <td style="text-align: right; text-decoration: underline;"></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblDisbursement_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBalance_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td></td>

                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                    <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                    <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>
</asp:Content>

