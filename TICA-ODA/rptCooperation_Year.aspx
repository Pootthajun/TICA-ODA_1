﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptCooperation_Year.aspx.vb" Inherits="rptCooperation_Year" %>



<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานตารางแสดงความร่วมมือไตรภาคี </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">รายงานตารางแสดงความร่วมมือไตรภาคี</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top:30px;*/"></div>
                                        <div class="row">
                                           
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">ประเภทความร่วมมือ : <span style ="color :red;">*</span></label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlCoperation_Type" runat="server" CssClass="form-control select2" Style="width: 80%">
                                                       
                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">ประเทศ / แหล่งคู่ร่วมมือ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_OU" runat="server" placeholder="ค้นหาจากชื่อประเทศ / แหล่งคู่ร่วมมือ" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>

                                        <div class="row"> 
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">กิจกรรม :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Activity" runat="server" placeholder="ค้นหาจากชื่อกิจกรรม" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">ประเภท :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Aid_Type" runat="server" placeholder="ค้นหาจากชื่อประเภท" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        <div class="row">
                                           
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">ประเทศ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Country" runat="server" placeholder="ค้นหาจากประเทศผู้รับทุน" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                 <%--<label for="inputname" class="col-sm-3 control-label line-height">ประเทศรับความช่วยเหลือ :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Recipience" runat="server" placeholder="ค้นหาจากชื่อประเทศรับความช่วยเหลือ" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>--%>
                                             </div>
                                        </div>
                                        

                                        <div class="row" style="margin-top:-40px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>



                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                          <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div>
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>ประเทศ / แหล่งคู่ร่วมมือ</th>                                                 
                                                    <th>ลำดับ</th>
                                                    <th>ชื่อกิจกรรม</th>
                                                    <th>ประเภทกิจกรรม</th>
                                                    <th>ประเทศผู้รับความช่วยเหลือ</th>
                                                    <th>จำนวนทุน</th>
                                                    <th>มูลค่าที่ไทยรับผิดชอบ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="funding_agency_TH" id="td1" runat="server" ><asp:Label ID="lblfunding_agency_TH" runat="server"></asp:Label></td>
                                                            <td data-title="Seq" style="text-align :center ;" id="td2" runat="server" ><asp:Label ID="lblSeq" runat="server"></asp:Label></td>
                                                            <td data-title="sector name" id="td3" runat="server" ><asp:Label ID="lblActivity_Name" runat="server"></asp:Label></td>
                                                            <td data-title="Component Name" id="td4" runat="server" ><asp:Label ID="lbl_Aid_Type_Name" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Recipience"   id="td5" runat="server" ><asp:Label ID="lblRecipience_List" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="QTY" style="text-align :center ;" id="td6" runat="server" ><asp:Label ID="lblQTY_Recipience" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Amount" style="text-align :right ;" id="td7" runat="server" ><b><asp:Label ID="lblAmount" runat="server" ForeColor="black"></asp:Label></b>
                                                                <a  ID="lnkSelectProject" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>

                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                           <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >
                                            <td  data-title="Total" style="text-align :center ;" colspan="5" ><b>Sub Total</b></td>
                                            <td data-title="QTY Sum" style="text-align :center  ;text-decoration: underline;" ><b><asp:Label ID="lblQTY_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                            <td data-title="Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblAmount_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                                                       

										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
     
</asp:Content>







