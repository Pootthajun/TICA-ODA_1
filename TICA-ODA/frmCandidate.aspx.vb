﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization
Partial Class frmCandidate
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptProject")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            pnlAdSearch.Visible = False
        End If

    End Sub

    Private Sub BindList()


        Dim DT As New DataTable
        Try
            Dim Sql As String = ""

            Sql &= " SELECT * FROM vw_Activity_Recipient_Candidate "
            Sql &= " WHERE 1=1  " + Environment.NewLine

            If (ddlProject_Type.SelectedValue > -1) Then
                Sql &= "   And project_type =  " & ddlProject_Type.SelectedValue & " " + Environment.NewLine
                Title += " ประเภทโครงการ : " & ddlProject_Type.SelectedItem.ToString()
            Else
                Title += " โครงการทั้งหมด "
            End If

            If (txtSearch_Project.Text <> "") Then
                Sql &= "  AND project_name Like '%" & txtSearch_Project.Text & "%'     " + Environment.NewLine
                Title += " ของ " & txtSearch_Project.Text
            End If

            If (txtSearch_Activity.Text <> "") Then
                Sql &= "  AND project_name Like '%" & txtSearch_Activity.Text & "%'     " + Environment.NewLine
                Title += " กิจกรรม : " & txtSearch_Activity.Text
            End If

            If (txtSearch_Country.Text <> "") Then
                Sql &= "  AND ( OU_name_th Like '%" & txtSearch_Country.Text & "%' OR " + Environment.NewLine
                Sql &= "  OU_name_en Like '%" & txtSearch_Country.Text & "%')    " + Environment.NewLine
                Title += " ประเทศ " & txtSearch_Country.Text
            End If

            If (ddlStatus_Recipient.SelectedValue > -2) Then
                Sql &= "   And Status_Candidate = " & ddlStatus_Recipient.SelectedValue & "" + Environment.NewLine
                Title += " สถานะผู้สมัคร : " & ddlStatus_Recipient.SelectedItem.ToString()
            Else
                Title += " ผู้สมัครทั้งหมด "
            End If

            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = Convert.ToDateTime(txtStartDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = Convert.ToDateTime(txtEndDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                'Sql &= " AND start_date  Between '" & StartDate & "' And '" & EndDate & "' " + Environment.NewLine

                Sql &= " AND ( start_date  >= '" & StartDate & "' " + Environment.NewLine
                Sql &= " AND end_date  <= '" & EndDate & "'  ) " + Environment.NewLine

                Title += "ตั้งแต่วันที่" & txtStartDate.Text & "ถึงวันที่" & txtEndDate.Text

            End If

            Sql &= "  ORDER BY project_type,project_Name ,Activity_Name, Candidate_created_date  " + Environment.NewLine

            Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnectionString)

            DA.Fill(DT)


            lblCount.Text = Title
            If DT.Rows.Count = 0 Then
                lblCount.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblCount.Text &= "  " & FormatNumber(DT.Rows.Count, 0) & " คน"
            End If

            Session("Search_Candidate_Title") = lblCount.Text

        Catch ex As Exception
            Dim Msg As String = ex.Message
        End Try



        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Candidate_List") = DT


        Pager.SesssionSourceName = "rptProjectPage"
        Pager.RenderLayout()
    End Sub



    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim check As ImageButton = e.Item.FindControl("check")
        Select Case e.CommandName
            Case "Select"
                If check.ImageUrl = "images/none.png" Then
                    check.ImageUrl = "images/check.png"
                Else
                    check.ImageUrl = "images/none.png"
                End If
        End Select
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblCourse As Label = DirectCast(e.Item.FindControl("lblCourse"), Label)
        Dim lblCandidate_Name As Label = DirectCast(e.Item.FindControl("lblCandidate_Name"), Label)
        Dim lblCandidate_ID As Label = DirectCast(e.Item.FindControl("lblCandidate_ID"), Label)
        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblPeriod As Label = DirectCast(e.Item.FindControl("lblPeriod"), Label)
        Dim lblInstitute As Label = DirectCast(e.Item.FindControl("lblInstitute"), Label)
        Dim lblAgency As Label = DirectCast(e.Item.FindControl("lblAgency"), Label)
        Dim lblAgency_Address As Label = DirectCast(e.Item.FindControl("lblAgency_Address"), Label)
        Dim lblStatus_Candidate As Label = DirectCast(e.Item.FindControl("lblStatus_Candidate"), Label)

        Dim check As ImageButton = e.Item.FindControl("check")

        lblCourse.Text = e.Item.DataItem("activity_name").ToString
        lblCandidate_Name.Text = e.Item.DataItem("name_th").ToString
        lblCandidate_ID.Text = e.Item.DataItem("node_id").ToString
        lblCountry.Text = e.Item.DataItem("OU_name_th").ToString
        lblProject.Text = e.Item.DataItem("project_name").ToString
        lblPeriod.Text = "Period :" & e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString
        lblInstitute.Text = ""
        lblAgency.Text = e.Item.DataItem("Agency").ToString
        lblAgency_Address.Text = e.Item.DataItem("Agency_Address").ToString
        lblStatus_Candidate.Text = e.Item.DataItem("StatusName_Candidate").ToString

        If e.Item.DataItem("Status_Candidate") = -1 Then
            check.Visible = True
        Else
            check.Visible = False
        End If


    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim DT_Current As DataTable = Current_Data()
        If DT_Current.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณาเลือกผู้รับทุนอย่างน้อย 1 ราย');", True)
            Exit Sub
        End If

        For i As Integer = 0 To DT_Current.Rows.Count - 1

            '----Save--- 
            Dim Sql As String = " select * from TB_Recipience_Person where node_id=" & DT_Current.Rows(i).Item("node_id")
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR As DataRow
            If (DT.Rows.Count = 1) Then
                DR = DT.Rows(0)
                DR("is_recipient") = True
                DR("Accept_Date") = Now
            End If
            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)

                '----INSERT OU--- 
                Dim Sql_OU As String = " "
                Sql_OU &= " INSERT INTO TB_OU_Person "
                'Sql_OU &= " Select  * From TB_Recipience_Person Where node_id =" & DT_Current.Rows(i).Item("node_id")

                Sql_OU &= " Select   node_id, prefix_th_id, name_th, prefix_en_id, name_en, parent_id, url, id_card " & vbLf
                Sql_OU &= "       , passport_no, expired_date_passport, birthdate, username, password, created_by, created_date " & vbLf
                Sql_OU &= "       , updated_by, updated_date, person_type, active_status, is_recipient, Accept_Date " & vbLf
                Sql_OU &= " From TB_Recipience_Person Where node_id =" & DT_Current.Rows(i).Item("node_id")


                Dim DA_OU As New SqlDataAdapter(Sql_OU, BL.ConnectionString)
                Dim DT_OU As New DataTable
                DA_OU.Fill(DT_OU)

            Catch ex As Exception
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('" & ex.Message.ToString() & "');", True)
                Exit Sub
            End Try
        Next

        BindList()
    End Sub

    Private Sub btnUnAccept_Click(sender As Object, e As EventArgs) Handles btnUnAccept.Click
        Dim DT_Current As DataTable = Current_Data()
        If DT_Current.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณาเลือกผู้รับทุนอย่างน้อย 1 ราย');", True)
            Exit Sub
        End If

        For i As Integer = 0 To DT_Current.Rows.Count - 1

            '----Save--- 
            Dim Sql As String = " select * from TB_Recipience_Person where node_id=" & DT_Current.Rows(i).Item("node_id")
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR As DataRow
            If (DT.Rows.Count = 1) Then
                DR = DT.Rows(0)
                DR("is_recipient") = False
            End If
            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('" & ex.Message.ToString() & "');", True)
                Exit Sub
            End Try
        Next

        BindList()
    End Sub

    Function Current_Data() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("node_id")
        Dim DR As DataRow
        For Each Item As RepeaterItem In rptList.Items

            Dim check As ImageButton = Item.FindControl("check")
            Dim lblCandidate_ID As Label = Item.FindControl("lblCandidate_ID")
            If check.ImageUrl = "images/check.png" Then
                DR = DT.NewRow
                DR("node_id") = lblCandidate_ID.Text
                DT.Rows.Add(DR)
            End If
        Next

        Return DT

    End Function

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        If pnlAdSearch.Visible = True Then
            pnlAdSearch.Visible = False
        Else
            pnlAdSearch.Visible = True
        End If

    End Sub


End Class
