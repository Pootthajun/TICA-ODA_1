﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Partial Class frmProject_Detail_Activity
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

#Region "Property"
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property

    Public Property DisplayActivity_Type As String
        'month/year
        Get
            Try
                Return ViewState("DisplayActivity_Type")
            Catch ex As Exception
                Return "month"
            End Try
        End Get
        Set(value As String)
            ViewState("DisplayActivity_Type") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property
    Public Property back As String
        'view/edit
        Get
            Try
                Return ViewState("back")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("back") = value
        End Set
    End Property

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property mo As Long
        Get
            Try
                Return ViewState("mo")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("mo") = value
        End Set
    End Property

    Public Property ActivityID_COPY As Long
        Get
            Try
                Return Session("ActivityID_COPY")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            Session("ActivityID_COPY") = value
        End Set
    End Property

#End Region


    Private Sub frmProjectDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuProjectStatus")
        li.Attributes.Add("class", "active")


        If Me.IsPostBack = False Then
            Try
                clearActivity()
                ClearForm()
                EditProjectID = CInt(Request.QueryString("ID"))
                mode = Request.QueryString("mode").ToString()
                If Not IsNothing(Request.QueryString("back")) Then
                    back = Request.QueryString("back").ToString()
                End If

            Catch ex As Exception
                EditProjectID = 0
            End Try

            CurrentTab = Tab.Activity

            If back = "palnex" Then
                mo = CInt(Request.QueryString("mo"))
                pnlActivityList.Visible = False
                pnlActivityDetail.Visible = True
                'UCActivityTabProject1.EditActivity(mo)
                Session("back") = back

                If ActivityID_COPY <> 0 Then
                    btnPaste.Visible = True
                Else
                    btnPaste.Visible = False
                End If

                btnBack.Visible = True
                btnAdd.Visible = pnlActivityList.Visible
            Else
                pnlActivityList.Visible = True
                pnlActivityDetail.Visible = False
                btnPaste.Visible = False
                btnBack.Visible = False
                btnAdd.Visible = pnlActivityList.Visible
            End If



            ' Project Type
            ProjectType = CInt(Request.QueryString("type"))
            If ProjectType = Constants.Project_Type.Project Then
                lblProjectType.Text = "Project"
                lblNavProjectType.Text = "Project"
                lblHeadProjectType.Text = "Project"
            End If

            If ProjectType = Constants.Project_Type.NonProject Then
                lblProjectType.Text = "Non Project"
                lblNavProjectType.Text = "Non Project"
                lblHeadProjectType.Text = "Non Project"
            End If


            If EditProjectID <> 0 Then
                SetProjectInfoByID(EditProjectID)
                If mode = "view" Then
                    SetControlToViewMode(True)
                Else
                    SetControlToViewMode(False)
                End If
            End If
        Else
            RestoreJQueryUI()
        End If

        btnTabActivity.Enabled = False
        If txtProjectID.Text.Trim <> "" Then
            btnTabActivity.Enabled = True
        End If


    End Sub


#Region "Event"



    Private Sub nav_click(sender As Object, e As EventArgs) Handles nav.ServerClick
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub



#End Region

#Region "Sub&Function"

    Private Sub SetProjectInfoByID(id As String)

        Try
            Dim dtProject As New DataTable
            dtProject = BL.GetProjectInfoByID(id)

            '##tabProjectDetail##
            If dtProject.Rows.Count > 0 Then
                Dim _project_id As String = ""
                Dim _project_name As String = ""
                Dim _objective As String = ""
                Dim _description As String = ""
                Dim _start_date As String = ""
                Dim _end_date As String = ""
                Dim _allocate_budget As String = ""
                'Dim _budgetyear As String = ""
                'Dim _budget_group_id As String = ""
                'Dim _budget_sub_id As String = ""
                Dim _cooperation_framework_id As String = ""
                Dim _cooperation_type_id As String = ""
                Dim _oecd_aid_type_id As String = ""
                Dim _location As String = ""
                Dim _funding_agency_id As String = ""
                Dim _executing_agency_id As String = ""
                Dim _remark As String = ""
                Dim _transfer_project_to As String = ""
                Dim _assistant As String = ""

                With dtProject
                    _project_id = .Rows(0)("project_id").ToString()
                    _project_name = .Rows(0)("project_name").ToString()
                    _objective = .Rows(0)("objective").ToString()
                    _description = .Rows(0)("description").ToString()
                    _start_date = Convert.ToDateTime(.Rows(0)("start_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _end_date = Convert.ToDateTime(.Rows(0)("end_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _allocate_budget = .Rows(0)("allocate_budget").ToString()
                    '_budgetyear = .Rows(0)("budgetyear").ToString()
                    '_budget_group_id = .Rows(0)("budget_group_id").ToString()
                    '_budget_sub_id = .Rows(0)("budget_sub_id").ToString()
                    _cooperation_framework_id = .Rows(0)("cooperation_framework_id").ToString()
                    _cooperation_type_id = .Rows(0)("cooperation_type_id").ToString()
                    _oecd_aid_type_id = .Rows(0)("oecd_aid_type_id").ToString()
                    _location = .Rows(0)("location").ToString()
                    _funding_agency_id = .Rows(0)("funding_agency_id").ToString()
                    _executing_agency_id = .Rows(0)("executing_agency_id").ToString
                    _remark = .Rows(0)("remark").ToString
                    _transfer_project_to = .Rows(0)("transfer_project_to").ToString
                    _assistant = .Rows(0)("assistant").ToString
                End With

                txtProjectID.Text = _project_id
                txtProjectName.Text = _project_name


            End If

            UCActivityTreeList1.GenerateActivityList(id, mode)


            SetActivity(EditProjectID)

            UCActivityTabProject1.ProjectID = id
            UCActivityTabProject1.ActivityMode = mode
        Catch ex As Exception
            alertmsg(ex.Message.ToString)
        End Try
    End Sub

    Private Sub SetActivity(project_id As String)
        Try
            Dim str As New StringBuilder
            str = BL.SetActivity(project_id, "", "view", "year", "")
            ltActivity.Text = str.ToString()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)
        txtProjectID.Enabled = Not IsView
        txtProjectName.Enabled = Not IsView
        UCActivityTreeList1.SetViewMode = IsView
        UCActivityTabProject1.SetControlToViewMode(IsView)
        If mode = "edit" Then
            UCActivityTabProject1.EditActivity(mo)
        Else
            UCActivityTabProject1.ViewActivity(mo)
        End If


    End Sub

    Private Sub ClearForm()
        '##tabProjectDetail##
        EditProjectID = 0
        txtProjectID.Text = ""
        txtProjectName.Text = ""

    End Sub


    Private Sub clearActivity()
        Dim sql As String = ""
        Dim dt As New DataTable
        sql = "select * from TB_Activity where active_status = '' and	created_date < @_DateTime"
        Dim p(1) As SqlParameter
        Dim a As String = DateTime.Now.ToString("yyyy-MM-dd")
        p(0) = SqlDB.SetText("@_DateTime", a)
        dt = SqlDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim id As String = dt.Rows(i)("id").ToString
                BL.DeleteActivity(id)
            Next
        End If
    End Sub

    Private Sub RestoreJQueryUI()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RestoreJQueryUI", "restoreJQueryUI();", True)
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub



#End Region

#Region "Tab"
    Protected Enum Tab
        Unknown = 0
        ProjectDetail = 1
        Activity = 2
        Recipient = 3
        ActActDetail = 4
        ActExpense = 5
        ActReceipent = 6
        ActPeriod = 7
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case tabActivity.Visible
                    Return Tab.Activity
                Case Else
                    Return Tab.Unknown
            End Select
        End Get
        Set(value As Tab)
            tabActivity.Visible = False
            btnAdd.Visible = False

            liTabProjectDetail.Attributes("class") = ""
            liTabActivity.Attributes("class") = ""

            Select Case value

                Case Tab.Activity
                    tabActivity.Visible = True
                    liTabActivity.Attributes("class") = "active"
                    'btnAdd.Visible = True

                Case Else
            End Select
        End Set
    End Property

    'Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabProjectDetail.Click, btnTabActivity.Click
    '    Select Case True
    '        Case Equals(sender, btnTabProjectDetail)
    '            CurrentTab = Tab.ProjectDetail
    '        Case Equals(sender, btnTabActivity)
    '            CurrentTab = Tab.Activity

    '        Case Else
    '    End Select
    'End Sub



    Private Sub UCActivityTreeList1_AddChildNode(ParentID As Long) Handles UCActivityTreeList1.AddChildNode
        UCActivityTabProject1.AddActivity(ParentID)

        pnlActivityList.Visible = False
        pnlActivityDetail.Visible = True
        btnAdd.Visible = False
        If ActivityID_COPY <> 0 Then
            btnPaste.Visible = True
        End If
        btnBack.Visible = True
    End Sub

    Private Sub UCActivityTreeList1_EditChildNode(ActivityID As Long) Handles UCActivityTreeList1.EditChildNode
        UCActivityTabProject1.EditActivity(ActivityID)

        pnlActivityList.Visible = False
        pnlActivityDetail.Visible = True
        btnAdd.Visible = False
        If ActivityID_COPY <> 0 Then
            btnPaste.Visible = True
        End If
        btnBack.Visible = True
    End Sub

    Private Sub UCActivityTreeList1_ViewChildNode(ActivityID As Long) Handles UCActivityTreeList1.ViewChildNode
        UCActivityTabProject1.ViewActivity(ActivityID)

        pnlActivityList.Visible = False
        pnlActivityDetail.Visible = True
        btnAdd.Visible = False
        btnPaste.Visible = False

        btnBack.Visible = True
    End Sub

    Private Sub UCActivityTabProject1_SaveActivityComplete() Handles UCActivityTabProject1.SaveActivityComplete
        UCActivityTreeList1.GenerateActivityList(EditProjectID, mode)

        pnlActivityList.Visible = True
        pnlActivityDetail.Visible = False
        btnAdd.Visible = True
        btnPaste.Visible = False

        btnBack.Visible = False
    End Sub


    Private Sub UCActivityTabProject1_CancelAct() Handles UCActivityTabProject1.CancelAct

        pnlActivityList.Visible = True
        pnlActivityDetail.Visible = False
        btnAdd.Visible = True
        btnPaste.Visible = False
        btnBack.Visible = False
        SetProjectInfoByID(EditProjectID)
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        pnlActivityList.Visible = True
        pnlActivityDetail.Visible = False
        btnAdd.Visible = True
        btnBack.Visible = False
        btnPaste.Visible = False
        SetProjectInfoByID(EditProjectID)
        Session("ActivityID_COPY") = 0
    End Sub



#End Region

#Region " Print Button"

    Function GetParameter(Reportformat As String) As String
        Dim para As String = "&ReportName=rptProjectDetail"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectID=" & EditProjectID

        If ProjectType = Constants.Project_Type.Project Then
            para += "&ProjectTypeName=Project"
        End If
        If ProjectType = Constants.Project_Type.NonProject Then
            para += "&ProjectTypeName=Non Project"
        End If

        Return para
    End Function



#End Region



#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProjectDetail.aspx?Mode=PDF" + para + "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProjectDetail.aspx?Mode=EXCEL" + para + "');", True)
    End Sub

#End Region

#Region "Navigator"
    Private Sub btnTabProjectDetail_Click(sender As Object, e As EventArgs) Handles btnTabProjectDetail.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Info.aspx?" & Param & "';", True)
    End Sub

    Private Sub btnTabActivity_Click(sender As Object, e As EventArgs) Handles btnTabActivity.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Activity.aspx?" & Param & "';", True)
    End Sub

    Private Sub btnPaste_Click(sender As Object, e As EventArgs) Handles btnPaste.Click

        '----ตรวจสอบ Activity---
        If (ActivityID_COPY > 0) Then

            '---ถ้ามี Activity ที่คัดลอกมา ก็ให้วางข้อมูล ดังนี้ 
            UCActivityTabProject1.CopyActivity()

            'Title(หัวข้อกิจกรรม)
            'ระยะเวลา
            'ข้อมูลผู้รับทุน
            'Group Aid(ประเภทความช่วยเหลือ)
            Session("ActivityID_COPY") = 0
            btnPaste.Visible = False

        Else
            Exit Sub
        End If
    End Sub


#End Region



    Private Sub btnAddAct_Click(sender As Object, e As EventArgs) Handles btnAddAct.Click
        UCActivityTabProject1.AddActivity(0)

        pnlActivityList.Visible = False
        pnlActivityDetail.Visible = True
        btnAdd.Visible = False
        If ActivityID_COPY <> Nothing Then
            btnPaste.Visible = True
        End If
        btnBack.Visible = True
    End Sub

    '--- Add Folder
    Private Sub btnAddFolder_Click(sender As Object, e As EventArgs) Handles btnAddFolder.Click

        pnlFolder.Visible = True
        txtFolder_Name.Text = ""
    End Sub


    Private Sub lnkDialogCancel_Click(sender As Object, e As EventArgs) Handles lnkDialogCancel.Click
        pnlFolder.Visible = False
    End Sub

    Private Sub lnkDialogSave_Click(sender As Object, e As EventArgs) Handles lnkDialogSave.Click
        If txtFolder_Name.Text.Trim = "" Then
            alertmsg("กรุณากรอกชื่อ Folder")
            Exit Sub
        End If
        '----Save--- 
        Dim Sql As String = " select * from TB_Activity where 0=1"
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If (DT.Rows.Count = 0) Then
            DR = DT.NewRow
            DR("project_id") = EditProjectID
            DR("parent_id") = 0
            DT.Rows.Add(DR)

        Else
            DR = DT.Rows(0)
        End If
        DR("activity_name") = txtFolder_Name.Text
        DR("Is_Folder") = True
        DR("notify") = "Y"
        DR("active_status") = "1"
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ex.Message)

            Exit Sub
        End Try




        pnlFolder.Visible = False

        pnlActivityList.Visible = True
        pnlActivityDetail.Visible = False
        btnAdd.Visible = True
        btnBack.Visible = False
        btnPaste.Visible = False
        SetProjectInfoByID(EditProjectID)
        'UCActivityTabProject1.CancelAct()
    End Sub



End Class
