﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Partial Class frmBudgetDetail1
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

#Region "_Property"
    Public Property AllData As DataTable
        Get
            Try
                Return Session("BudgetData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("BudgetData") = value
        End Set
    End Property

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property EditBudgetID As Long
        Get
            Try
                Return ViewState("BudgetID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("BudgetID") = value
        End Set
    End Property

#End Region

    Protected Sub frmBudgetDetail1_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Try
                EditBudgetID = CInt(Request.QueryString("ID"))
                mode = Request.QueryString("mode").ToString()
            Catch ex As Exception
                EditBudgetID = 0
            End Try

            BL.Bind_DDL_BudgetGroup(ddlGroupBudget)
            'ddlBudgetGroup_SelectedIndexChanged(sender, e)
            BL.Bind_DDL_SubBudget(dllSubBudget, ddlGroupBudget.SelectedValue)
            BL.Bind_DDL_Year(ddlBudgetYear)
            SetBudgetInfo(EditBudgetID)
            BL.SetTextDblKeypress(txtamount)
        End If

    End Sub

    Sub SetBudgetInfo(EditBudgetID As String)
        txtamount.Text = ""
        txtProvideBy.Text = ""
        txtReceivedDate.Text = ""
        txtDescription.Text = ""

        BL.Bind_DDL_Year(ddlBudgetYear)

        If mode = "add" Then

        Else
            Dim dt1 As New DataTable
            dt1 = BL.GetList_BudgetDetail1(EditBudgetID, "")

            If dt1.Rows.Count > 0 Then

                'Dim _amount As String = Convert.ToDecimal(dt1.Rows(0)("amount")).ToString("#,##0.00")
                ddlBudgetYear.SelectedValue = dt1.Rows(0)("budget_year").ToString

                If Not IsDBNull(dt1.Rows(0)("amount")) Then
                    txtamount.Text = Convert.ToDecimal(dt1.Rows(0)("amount")).ToString("#,##0.00")

                End If
                txtProvideBy.Text = dt1.Rows(0)("provide_by").ToString
                If Not IsDBNull(dt1.Rows(0)("amount")) Then
                    txtReceivedDate.Text = Convert.ToDateTime(dt1.Rows(0)("received_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                End If



                txtDescription.Text = dt1.Rows(0)("description").ToString()
                BL.Bind_DDL_BudgetGroup(ddlGroupBudget)
                ddlGroupBudget.SelectedValue = dt1.Rows(0)("group_id").ToString()
                Dim GB As String = dt1.Rows(0)("group_id").ToString()
                BL.Bind_DDL_SubBudget(dllSubBudget, GB)
                dllSubBudget.SelectedValue = dt1.Rows(0)("sub_id").ToString()
                Authorize()
            End If
        End If
    End Sub


    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            ddlGroupBudget.Enabled = False
            ddlBudgetYear.Enabled = False
            dllSubBudget.Enabled = False
            txtamount.Enabled = False
            txtDescription.Enabled = False
            txtProvideBy.Enabled = False
            txtReceivedDate.Enabled = False

        End If
    End Sub




    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '## Validate
            If Validate() = False Then
                Exit Sub
            End If
            'Dim Amount As Integer = txtamount.Text
            'Dim lnqBD As New TbBudgetDetailLinqDB
            'With lnqBD
            '    .AMOUNT = Amount
            '    .BUDGET_SUB_ID = dllSubBudget.SelectedValue
            'End With

            Dim _budgetyear As String = ddlBudgetYear.SelectedValue.ToString()
            '**********
            Dim lnqBudget As New LinqDB.TABLE.TbBudgetLinqDB
            With lnqBudget
                .ID = EditBudgetID
                .PROVIDE_BY = txtProvideBy.Text
                .RECEIVED_DATE = Converter.StringToDate(txtReceivedDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtReceivedDate.Text
                .DESCRIPTION = txtDescription.Text
                .BUDGET_YEAR = _budgetyear
                '.RECORD = ""
            End With
            '**********

            Dim ret As New ProcessReturnInfo
            'ret = BL.SaveBudgetDetail1(lnqBD, EditBudgetID, 0, UserName, lnqBudget)
            ret = BL.SaveBudgetDetail1(dllSubBudget.SelectedValue, txtamount.Text, EditBudgetID, 0, UserName, lnqBudget)

            '----Save--- 
            Dim Sql As String = " select * from TB_Budget_Detail WHERE id=" & ret.ID
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR As DataRow
            If (DT.Rows.Count = 0) Then
                DR = DT.NewRow
                DT.Rows.Add(DR)
            Else
                DR = DT.Rows(0)
            End If
            DR("budget_id") = ret.ID
            DR("budget_sub_id") = dllSubBudget.SelectedValue
            DR("amount") = Convert.ToDouble(txtamount.Text.Replace(",", ""))
            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                Alert("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End Try


            If ret.IsSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmBudget.aspx';", True)
            Else
                Alert("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End If

        Catch ex As Exception
            Alert("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try
    End Sub


    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtReceivedDate.Text.Trim = "" Then
            Alert("กรุณาระบุวันที่ได้รับงบประมาณ")
            ret = False
        End If
        If txtamount.Text.Trim = "" Then
            Alert("กรุณาระบุงบประมาณ")
            ret = False
        End If
        If txtProvideBy.Text.Trim = "" Then
            Alert("กรุณาระบุผู้มอบงบประมาณ")
            ret = False
        End If
        If ddlGroupBudget.SelectedValue = "" Then
            Alert("กรุณาระบุกลุ่มงบประมาณ")
            ret = False
        End If
        If ddlGroupBudget.SelectedValue = "" Then
            Alert("กรุณาระบุงบประมาณย่อย")
            ret = False
        End If
        Try
            Converter.StringToDate(txtReceivedDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtReceivedDate.Text
        Catch ex As Exception
            Alert("กรุณาตรวจสอบวันที่")
            ret = False
        End Try


        Return ret
    End Function

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Sub btnCancel_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmBudget.aspx")
    End Sub
    Private Sub ddlBudgetGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroupBudget.SelectedIndexChanged
        BL.Bind_DDL_SubBudget(dllSubBudget, ddlGroupBudget.SelectedValue)
    End Sub
End Class
