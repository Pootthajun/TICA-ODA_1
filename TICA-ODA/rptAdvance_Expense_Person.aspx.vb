﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAdvance_Expense_Person
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
        li_mnuReports_foreign.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_01")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            BL.Bind_DDL_BudgetGroup(ddlSearch_group)

        End If

    End Sub



    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""
            sql &= "    Select vw_Advance_Expense.*   " + Environment.NewLine
            sql &= "    ,vw_AllProject.project_type  " + Environment.NewLine
            sql &= "    FROM  vw_Advance_Expense " + Environment.NewLine
            sql &= "    LEFT JOIN vw_AllProject ON vw_AllProject.id=vw_Advance_Expense.Project_ID " + Environment.NewLine

            sql &= "    WHERE TB_NAME='TB_OU_Person'  " + Environment.NewLine

            If (txtSearch_Project_Name.Text <> "") Then
                filter &= "  vw_Advance_Expense.project_name Like '%" & txtSearch_Project_Name.Text & "%'  AND " + Environment.NewLine
                Title += " โครงการ: " & txtSearch_Project_Name.Text
            End If
            If (txtSearch_Borrower_Name.Text <> "") Then
                filter &= "  vw_Advance_Expense.name_th Like '%" & txtSearch_Borrower_Name.Text & "%'  AND " + Environment.NewLine
                Title += " ยืมโดย: " & txtSearch_Borrower_Name.Text
            End If
            If (txtSearch_Advance_Detail.Text <> "") Then
                filter &= "  vw_Advance_Expense.Advance_Detail Like '%" & txtSearch_Advance_Detail.Text & "%'  AND " + Environment.NewLine
                Title += " เพื่อวัตถุประสงค์: " & txtSearch_Advance_Detail.Text
            End If

            If (ddlSearch_group.SelectedIndex > 0) Then
                filter &= "  vw_Advance_Expense.budget_group_id =" & ddlSearch_group.SelectedValue & "  AND " + Environment.NewLine
                Title += " ประเภทงบประมาณ: " & ddlSearch_group.SelectedItem.ToString()
            End If

            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                filter &= "  CONVERT(VARCHAR,vw_Advance_Expense.Advance_Date,112) Between dbo.GetDateFormatSearch('" & StartDate & "') AND dbo.GetDateFormatSearch('" & EndDate & "')   AND " + Environment.NewLine

                Title += " ระหว่างวันที่ " & txtStartDate.Text & " ถึงวันที่ " & txtEndDate.Text

            End If

            If (txtSearch_Advance_No.Text <> "") Then
                filter &= "  vw_Advance_Expense.Advance_No Like '%" & txtSearch_Advance_No.Text & "%'  AND " + Environment.NewLine
                Title += " เอกสารเงินยืมเลขที่: " & txtSearch_Advance_No.Text
            End If


            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) + Environment.NewLine
            End If

            sql &= "order by vw_Advance_Expense.project_name,vw_Advance_Expense.Advance_Date   " + Environment.NewLine

            DT = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_rptAdvance_Expense_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function


    Private Sub BindList()


        Dim DT As DataTable = GetList()
        If (DT.Rows.Count > 0) Then
            lblAmout_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Amount)", "")).ToString("#,##0.00")
            lblAmout_Expense_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(SUM_Expense)", "")).ToString("#,##0.00")
            lblCash_Back_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Cash_Back)", "")).ToString("#,##0.00")
            lblBalance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If


        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_rptAdvance_Expense") = DT

        Pager.SesssionSourceName = "Search_rptAdvance_Expense"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim LastProject As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblproject_name As Label = DirectCast(e.Item.FindControl("lblproject_name"), Label)
        Dim lblname_th As Label = DirectCast(e.Item.FindControl("lblname_th"), Label)
        Dim lblAdvance_Detail As Label = DirectCast(e.Item.FindControl("lblAdvance_Detail"), Label)
        Dim lblactivity_name As Label = DirectCast(e.Item.FindControl("lblactivity_name"), Label)
        Dim lblbudget_group_Name As Label = DirectCast(e.Item.FindControl("lblbudget_group_Name"), Label)
        Dim lblAdvance_Date As Label = DirectCast(e.Item.FindControl("lblAdvance_Date"), Label)

        Dim lblAdvance_Due_Date As Label = DirectCast(e.Item.FindControl("lblAdvance_Due_Date"), Label)
        Dim lblAmout As Label = DirectCast(e.Item.FindControl("lblAmout"), Label)
        Dim lblAmout_Expense As Label = DirectCast(e.Item.FindControl("lblAmout_Expense"), Label)
        Dim lblCash_Back As Label = DirectCast(e.Item.FindControl("lblCash_Back"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        Dim lblAdvance_No As Label = DirectCast(e.Item.FindControl("lblAdvance_No"), Label)
        Dim trproject_name As HtmlTableRow = e.Item.FindControl("trproject_name")

        '--------Lastbudget_year------------
        If LastProject <> e.Item.DataItem("project_name").ToString Then
            LastProject = e.Item.DataItem("project_name").ToString
            lblproject_name.Text = LastProject
            trproject_name.Visible = True
        Else
            trproject_name.Visible = False
        End If

        'lblproject_name.Text = e.Item.DataItem("project_name").ToString
        lblname_th.Text = e.Item.DataItem("name_th").ToString
        lblAdvance_Detail.Text = e.Item.DataItem("Advance_Detail").ToString
        lblactivity_name.Text = e.Item.DataItem("activity_name").ToString
        lblbudget_group_Name.Text = e.Item.DataItem("budget_group_Name").ToString
        If Convert.IsDBNull(e.Item.DataItem("Advance_Date")) = False Then
            lblAdvance_Date.Text = GL.ReportThaiDate(e.Item.DataItem("Advance_Date"))
        End If
        'lblAdvance_No.Text = e.Item.DataItem("Advance_No").ToString



        If Convert.IsDBNull(e.Item.DataItem("Due_Date")) = False Then
            lblAdvance_Due_Date.Text = GL.ReportThaiDate(e.Item.DataItem("Due_Date"))
        End If
        If Convert.IsDBNull(e.Item.DataItem("Amount")) = False Then
            lblAmout.Text = Convert.ToDecimal(e.Item.DataItem("Amount")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("SUM_Expense")) = False Then
            lblAmout_Expense.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Expense")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Cash_Back")) = False Then
            lblCash_Back.Text = Convert.ToDecimal(e.Item.DataItem("Cash_Back")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


        '============Click To Project===============

        Dim lnkSelectProject As HtmlAnchor = e.Item.FindControl("lnkSelectProject")
        Dim tdLinkProject1 As HtmlTableCell = e.Item.FindControl("tdLinkProject1")
        tdLinkProject1.Style("cursor") = "pointer"
        tdLinkProject1.Attributes("onClick") = "document.getElementById('" & lnkSelectProject.ClientID & "').click();"

        Dim lnkSelectActivity As HtmlAnchor = e.Item.FindControl("lnkSelectActivity")
        For i As Integer = 1 To 8
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & lnkSelectActivity.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Or (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelectProject.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmProject_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelectProject.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmLoan_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        Else
            lnkSelectProject.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmContribuiltion_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        End If




    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAdvance_Expense.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAdvance_Expense.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class

