﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidSummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptSummary_AidPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptSummary_AidPage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptAidSummary")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            ProjectType = 0  ' เนื่องจากเฉพาะประเภทของ Project Type=0
            BindList()
            BL.Bind_DDL_Year(ddlProject_Year, True)


            Bind_DDL_Country(ddlProject_Country)
            Bind_DDL_sector(ddlProject_Sector)

        End If

    End Sub


    Public Function GetProjectList(project_type As Integer) As DataTable

        Dim dt As New DataTable
        Dim Title As String = ""
        Dim sql As String = ""
        Try
            sql += " SELECT budget_year,TB.Project_id,TB.project_name,TB.project_type,sector_id,TB_Purposecat.perposecat_name,country_node_id,vw_ou.name_th country_name_th,vw_ou.name_en country_name_en				 " + Environment.NewLine
            sql += "       ,ISNULL(SUM(Pay_Amount_Actual),0) Pay_Amount_Actual																									 " + Environment.NewLine
            sql += "       ,TB_Project.project_id Project_code																									 " + Environment.NewLine
            sql += "  FROM																																						 " + Environment.NewLine
            sql += " (																																							 " + Environment.NewLine
            sql += " 	SELECT TB_Activity_Budget.budget_year,_vw_Activity_Pay_Amount_Actual.* 																					 " + Environment.NewLine
            sql += " 	FROM _vw_Activity_Pay_Amount_Actual 																													 " + Environment.NewLine
            sql += " 	INNER JOIN TB_Activity_Budget ON TB_Activity_Budget.activity_id = _vw_Activity_Pay_Amount_Actual.Activity_id											 " + Environment.NewLine
            sql += " )AS TB																																						 " + Environment.NewLine
            sql += " LEFT JOIN TB_Purposecat ON TB_Purposecat.id=TB.sector_id																									 " + Environment.NewLine
            sql += " LEFT JOIN vw_ou ON vw_ou.node_id=TB.country_node_id																										 " + Environment.NewLine
            sql += " LEFT JOIN TB_Project  ON TB_Project.id =TB.Project_id																										 " + Environment.NewLine

            sql += " WHERE 1=1																										 " + Environment.NewLine

            If Not String.IsNullOrEmpty(ddlProject_Year.SelectedValue.ToString) Then
                sql += " and budget_year = '" + ddlProject_Year.SelectedValue.ToString + "'" + Environment.NewLine
            End If
            If Not String.IsNullOrEmpty(ddlProject_Country.SelectedValue.ToString) Then
                sql += " and country_node_id = '" + ddlProject_Country.SelectedValue.ToString + "'" + Environment.NewLine
            End If
            If Not String.IsNullOrEmpty(ddlProject_Sector.SelectedValue.ToString) Then
                sql += " and sector_id = '" + ddlProject_Sector.SelectedValue.ToString + "'" + Environment.NewLine
            End If
            If Not String.IsNullOrEmpty(txtSearch_Project.Text) Then
                sql += " and ( TB.project_name like '%" + txtSearch_Project.Text + "%' " + Environment.NewLine
                sql += "       OR TB_Project.project_id like '%" + txtSearch_Project.Text + "%' )" + Environment.NewLine

            End If


            sql += " GROUP BY budget_year ,TB.Project_id,TB.project_name,TB.project_type,sector_id,TB_Purposecat.perposecat_name,country_node_id,vw_ou.name_th,vw_ou.name_en,TB_Project.project_id				 " + Environment.NewLine
            sql += " ORDER BY budget_year ,country_node_id,TB_Purposecat.perposecat_name,project_name																							 " + Environment.NewLine


            dt = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " รายการ"
            End If



        Catch ex As Exception
        End Try
        Return dt

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetProjectList(ProjectType)

        If (DT.Rows.Count > 0) Then
            Dim CountCountry As Object = 0
            Dim Sector_Sum As Object = 0
            Dim Project_Sum As Object = 0
            CountCountry = Convert.ToInt32(DT.DefaultView.ToTable(True, "country_node_id").Compute("count(country_node_id)", "")).ToString("#,##0")
            Sector_Sum = Convert.ToInt32(DT.DefaultView.ToTable(True, "sector_id").Compute("count(sector_id)", "")).ToString("#,##0")
            Project_Sum = Convert.ToInt32(DT.DefaultView.ToTable(True, "project_name").Compute("count(project_name)", "")).ToString("#,##0")

            lblAmout_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")

            lblTotalRecord.Text &= " ข้อมูลของ " & CountCountry & " ประเทศ"
            lblTotalRecord.Text &= " " & Sector_Sum & " สาขา"
            lblTotalRecord.Text &= " ภายใต้ " & Project_Sum & " โครงการ"

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("Search_Summary_Aid") = DT
        Session("Search_Summary_Aid_Title") = lblTotalRecord.Text

        AllData = DT
        Pager.SesssionSourceName = "rptSummary_AidPage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim btnSelect As Button = e.Item.FindControl("Button")

        Dim _project_id As String = lblProject.Attributes("project_id")
        Dim _project_type As String = lblProject.Attributes("project_type")


        Select Case e.CommandName
            Case "select"

                If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then
                    Response.Redirect("frmProject_Detail_Info.aspx?id=" & _project_id & " &mode=" & "view&type=" & _project_type)
                End If
                If ProjectType = Constants.Project_Type.Loan Then
                    Response.Redirect("frmLoan_Detail_Info.aspx?id=" & _project_id & " &mode=" & "view&type=" & _project_type)
                End If
                If ProjectType = Constants.Project_Type.Contribuition Then
                    Response.Redirect("frmContribuiltion_Detail_Info.aspx?id=" & _project_id & " &mode=" & "view&type=" & _project_type)
                End If

        End Select
    End Sub


    Dim Lastbudget_year As String = ""
    Dim LastCountry As String = ""
    Dim LastSector As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblSector As Label = DirectCast(e.Item.FindControl("lblSector"), Label)
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblAmout As Label = DirectCast(e.Item.FindControl("lblAmout"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        'Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        'For i As Integer = 1 To 5
        '    Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
        '    td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        'Next

        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        For i As Integer = 1 To 5
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        Next

        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        End If
        '-------------------------------------------------------

        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
        Dim td3 As HtmlTableCell = e.Item.FindControl("td3")

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True
        Else
            trbudget_year.Visible = False
        End If

        '--------LastCountry------------
        If LastCountry <> e.Item.DataItem("country_name_th").ToString & e.Item.DataItem("budget_year").ToString Then
            LastCountry = e.Item.DataItem("country_name_th").ToString & e.Item.DataItem("budget_year").ToString
            lblCountry.Text = e.Item.DataItem("country_name_th").ToString
            LastSector = ""
        Else
            LastSector = ""
            td2.Attributes("style") &= DuplicatedStyleTop
        End If

        '--------LastSector------------
        If LastSector <> e.Item.DataItem("perposecat_name").ToString Then
            If (e.Item.DataItem("perposecat_name").ToString <> e.Item.DataItem("country_name_th").ToString) Then
                If Not String.IsNullOrEmpty(e.Item.DataItem("country_name_th").ToString()) Then
                    lblSector.Text = e.Item.DataItem("perposecat_name").ToString().Replace(e.Item.DataItem("country_name_th").ToString(), "").Trim()
                Else
                    lblSector.Text = e.Item.DataItem("perposecat_name").ToString()
                End If
            Else
                lblSector.Text = e.Item.DataItem("perposecat_name").ToString
            End If
            LastSector = e.Item.DataItem("perposecat_name").ToString
        Else
            td3.Attributes("style") &= DuplicatedStyleTop
        End If


        lblNo.Text = e.Item.ItemIndex + 1
        lblProject.Text = e.Item.DataItem("Project_code").ToString & ": " & e.Item.DataItem("project_name").ToString

        lblProject.Attributes("project_id") = e.Item.DataItem("project_id")
        lblProject.Attributes("project_type") = e.Item.DataItem("project_type")


        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblAmout.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        'btnSelect.CommandArgument = e.Item.DataItem("project_id")

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidSummary.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidSummary.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)
        Dim DT As DataTable = Session("Search_Summary_Aid").DefaultView.ToTable(True, "country_node_id", "country_name_th")
        DT.DefaultView.RowFilter = "country_node_id IS NOT NULL"
        DT = DT.DefaultView.ToTable
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("country_name_th"), DT.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Bind_DDL_sector(ByRef ddl As DropDownList)
        Dim DT As DataTable = Session("Search_Summary_Aid").DefaultView.ToTable(True, "sector_id", "perposecat_name")

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Sector", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("perposecat_name"), DT.Rows(i).Item("sector_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

End Class
