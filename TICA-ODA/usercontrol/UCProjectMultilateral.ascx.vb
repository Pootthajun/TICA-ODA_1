﻿
Imports System.Data

Partial Class UCProjectMultilateral
    Inherits System.Web.UI.UserControl

    Dim BL As New ODAENG

    Public Event EditBudgetCon()

    Dim _MultilateralDT As DataTable
    Public Property MultilateralDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _MultilateralDT = value

            rptList.DataSource = _MultilateralDT
            rptList.DataBind()
        End Set
    End Property


    Private Sub UCProjectMultilateral_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            btnAdd_Click(sender, e)
        End If
    End Sub

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            ddlMultilateral.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub


    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("Multilateral_ID") = ""
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Multilateral_ID")


        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim lblid As Label = DirectCast(rptList.Items(i).FindControl("lblid"), Label)
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("Multilateral_ID") = ddlMultilateral.SelectedValue
            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim ddlMultilateral As DropDownList = DirectCast(e.Item.FindControl("ddlMultilateral"), DropDownList)
        Dim btnDelete As Button = DirectCast(e.Item.FindControl("btnDelete"), Button)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblid"), Label)

        BL.Bind_DDL_Multilateral(ddlMultilateral)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlMultilateral.SelectedValue = e.Item.DataItem("Multilateral_ID").ToString

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then
            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Multilateral_ID")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim lblid As Label = DirectCast(rptList.Items(i).FindControl("lblid"), Label)
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)

            If lblid.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("Multilateral_ID") = ddlMultilateral.SelectedValue


                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function
End Class

