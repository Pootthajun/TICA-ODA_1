﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCCoFunding.ascx.vb" Inherits="usercontrol_UCCoFunding" %>
<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                <th>Country (ประเทศ)</th>
                <th>Amount (จำนวนเงิน)</th>
                <th style="width: 50px">Delete(ลบ)</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td data-title="Country (ประเทศ)"  style ="padding:0px;">
                             <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 100%; border:none;">
                             </asp:DropDownList>
                             <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td data-title="Budget (บาท)" id="td" runat="server"  style ="padding:0px;">
                           <asp:TextBox  ID="txtAmount" runat="server" CssClass="form-control" style="text-align: right;  " placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                        </td>
                        <td data-title="Delete" id="ColDelete" runat="server"  style ="padding:0px;">
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>

                             <%--<div class="btn-group" data-toggle="tooltip" title="Add Activity">
                                                <asp:LinkButton ID="btnAddActivity" runat="server" CssClass="btn btn-bricky"><i class="fa-trash-o"></i></asp:LinkButton>
                             </div>--%>

                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
        <asp:Panel ID="pnlFooter" runat="server">
        <tfoot>
            <tr>
                <td style="background-color:darkkhaki;text-align: center ;">
                    <b>รวม</b>
                </td>
                <td style="background-color:darkkhaki;text-align: right;">
                     <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                </td>
            </tr>

        </tfoot>
        </asp:Panel>
    </table>
    

    <div class="row" style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>