﻿Imports System.Data
Partial Class usercontrol_UCProjectCountryRe
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG

    Public Event EditBudget()

    Dim _ComponentDT As DataTable
    Public Property ComponentDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _ComponentDT = value
            If (_ComponentDT.Rows.Count > 0) Then
                pnlFooter.Visible = True
            Else
                pnlFooter.Visible = False
            End If

            rptList.DataSource = _ComponentDT
            rptList.DataBind()
            txtExpand_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public ReadOnly Property TotalBudget As Decimal
        Get
            Return CDec(lblTotal.Text)
        End Get
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)
            ddlCountry.Enabled = Not IsEnable
            txtExpand.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub



    Protected Sub txtExpand_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Decimal = 0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
                _total += CDec(txtExpand.Text)
            Catch ex As Exception
            End Try
        Next

        If (rptList.Items.Count > 0) Then
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        lblTotal.Text = _total.ToString("#,##0")

        RaiseEvent EditBudget()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)

        Dim dt As DataTable = GetDataCountryRe_FromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        'dr("component_id") = ""
        dr("amout_person") = 0
        dt.Rows.Add(dr)


        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()

    End Sub

    Function SetDataRpt(Activity_id As String) As DataTable
        Dim dt As New DataTable
        dt = BL.GetDataCountryRecipent_FromQurey(Activity_id)

        dt.Columns.Add("no", GetType(Long))

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()

        lblActivityID.Text = Activity_id

        Return dt
    End Function


    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_node_id")
        dt.Columns.Add("amout_person", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow
            dr("id") = lblID.Text
            dr("country_node_id") = ddlCountry.SelectedValue
            If txtExpand.Text = "" Then
                dr("amout_person") = 0
            Else
                dr("amout_person") = txtExpand.Text
            End If

            dt.Rows.Add(dr)

        Next
        txtExpand_TextChanged(Nothing, Nothing)
        Return dt
    End Function

    Function GetDataCountryRe_FromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("country_node_id")
        dt.Columns.Add("amout_person", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            dr("country_node_id") = ddlCountry.SelectedValue
            If txtExpand.Text = "" Then
                dr("amout_person") = 0
            Else
                dr("amout_person") = txtExpand.Text
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim txtExpand As TextBox = DirectCast(e.Item.FindControl("txtExpand"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)

        BL.Bind_DDL_Country(ddlCountry)
        BL.SetTextIntKeypress(txtExpand)

        lblNo.Text = e.Item.DataItem("no").ToString
        lblID.Text = e.Item.DataItem("id").ToString
        ddlCountry.SelectedValue = e.Item.DataItem("country_node_id").ToString
        txtExpand.Text = Convert.ToDecimal(e.Item.DataItem("amout_person")).ToString("#,##0")


    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then

            'Dim _id As String = e.CommandArgument
            'Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            'rptList.DataSource = dt
            'rptList.DataBind()

            Dim dt As DataTable = GetDataFromRpt(rptList)
            dt.Rows.RemoveAt(e.Item.ItemIndex)
            dt.Columns.Add("no", GetType(Long))

            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList.DataSource = dt
            rptList.DataBind()
            txtExpand_TextChanged(Nothing, Nothing)

        End If
    End Sub


    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_node_id")
        dt.Columns.Add("amout_person")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)

            If lblID.Text <> id Then
                dr = dt.NewRow
                dr("id") = lblID.Text
                dr("country_node_id") = ddlCountry.SelectedValue
                If txtExpand.Text = "" Then
                    dr("amout_person") = 0
                Else
                    dr("amout_person") = txtExpand.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        dt.Columns.Add("no", GetType(Long))

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next

        rptList.DataSource = dt
        rptList.DataBind()
        txtExpand_TextChanged(Nothing, Nothing)
        Return dt
    End Function
End Class
