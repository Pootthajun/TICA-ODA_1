﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCprojectRecipience.ascx.vb" Inherits="usercontrol_UCprojectRecipience" %>
<asp:UpdatePanel ID="udpList" runat="server">
    <ContentTemplate>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-gray">
                        <th>Country Recipient (ประเทศผู้รับทุน)</th>
                        <th>Amount (จำนวน)</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptCountRe" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center" width="150px">
                                    <asp:Label ID="lblCountryPic" runat="server"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:Label ID="lblCountRe" runat="server"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <br />
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-gray">
                        <th>ครั้งที่</th>
                        <th>Recipient (ผู้รับทุน)</th>
                        <th>Country Recipient(ประเทศผู้รับทุน)</th>
                        <th style="width: 50px">ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>

                    <asp:Repeater ID="rptList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-title="ครั้งที่" style="width: 100px; text-align: center;">
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </td>
                                <td data-title="Recipient (ผู้รับทุน)" style="padding: 0px;">
                                    <asp:UpdatePanel ID="udpList" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlRecipient" runat="server" CssClass="form-control select2" Style="width: 100%; border: none;" OnSelectedIndexChanged="ddlRecipient_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlRecipient" EventName="SelectedIndexChanged" />
                                     </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                </td>
                                <td data-title="Country Recipient(ประเทศผู้รับทุน)" id="td" runat="server" style="padding: 0px; width: 30%;">
                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 100%; border: none;">
                                    </asp:DropDownList>
                                </td>
                                <td data-title="Delete" id="ColDelete" runat="server" style="padding: 0px;">
                                    <center>
                                <asp:Button  ID="btnDelete" runat="server" Text="ลบ" w CssClass="btn btn-bricky" width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');" />
                            </center>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>

            </table>


            <div class="row">
                <center>
            <div class="col-sm-12"  style="margin-top:10px;">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary" />
            </div>
        </center>
            </div>
        </div>
       <%-- <script>
            function bdCountryChange(btn) {
                document.getElementById(btn).click();
                return true;
            }

        </script>--%>


    </ContentTemplate>
</asp:UpdatePanel>
