﻿Imports System.Data
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Partial Class usercontrol_UCFormOUPerson
    Inherits System.Web.UI.UserControl
    Dim ENG As New ODAENG
    Dim strNodeID As String
    Dim BL As New ODAENG
    Public WriteOnly Property ParentNodeID As String
        Set(value As String)
            txtparentNodeID.Text = value
        End Set
    End Property
    Public WriteOnly Property NodeID As String
        Set(value As String)
            txtNodeID.Text = value
        End Set
    End Property
    Public WriteOnly Property ShowLoginInfo As Boolean
        Set(value As Boolean)
            pnlUserInfo.Visible = value
            trOranization.Visible = value
        End Set
    End Property

    Public Sub BindDDLExecutingOrganize()
        ENG.Bind_DDL_OrganizeByCountry(ddlExecutingOrganize, "194")
    End Sub

    Private Sub usercontrol_UCFormOUPerson_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ENG.SetTextIntKeypress(txtPersonIDCard)

        End If
    End Sub


    Public Sub FillInDataPerson(PersonNodeID As String)
        'PNLmyModalPerson.Visible = True

        If PersonNodeID <> "" Then
            strNodeID = PersonNodeID
            Dim dtP As New DataTable
            dtP = ENG.GetPersonInfoByID(PersonNodeID)

            If dtP.Rows.Count > 0 Then
                txtNodeID.Text = dtP(0)("node_id")
                txtparentNodeID.Text = dtP(0)("parent_id")
                ENG.Bind_DDL_OrganizeByCountry(ddlExecutingOrganize, dtP(0)("Country_id"))
                Try
                    ddlExecutingOrganize.SelectedValue = dtP(0)("parent_id")
                Catch ex As Exception
                    ddlExecutingOrganize.SelectedIndex = -1
                End Try

                txtPersonNameTH.Text = dtP(0)("name_th").ToString()
                txtPersonNameEN.Text = dtP(0)("name_en").ToString()
                txtPersonIDCard.Text = dtP(0)("id_card").ToString()
                txtPersonPassportID.Text = dtP(0)("passport_no").ToString()
                If dtP(0)("birthdate").ToString <> "" Then
                    txtPersonBirthDay.Text = Convert.ToDateTime(dtP(0)("birthdate").ToString).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                txtPersonTelephone.Text = dtP(0)("telephone").ToString()
                txtPersonEmail.Text = dtP(0)("email").ToString()
                txtPersonNote.Text = dtP(0)("description").ToString()

                If dtP(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS_Person.Checked = True
                Else
                    chkACTIVE_STATUS_Person.Checked = False
                End If

                If dtP(0)("person_type").ToString() = "1" Then
                    RaRecipient.Checked = True
                Else
                    RaAssistant.Checked = True
                End If

                If pnlUserInfo.Visible = True Then
                    If Convert.IsDBNull(dtP(0)("username")) = False Then
                        txtUsername.Text = dtP(0)("username")
                        txtUsername.Enabled = False  'ไม่ได้แก้ไข Username
                    End If
                    UCFormAuthorize.BindMenu(txtNodeID.Text)
                End If

            End If
        End If
    End Sub

    Private Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub


    Public Function SavePerson(Username As String, IsSaveAuthorize As Boolean) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        If txtPersonNameTH.Text = "" Then
            Alert("กรุณากรอกชื่อภาษาไทย")
            txtPersonNameTH.Focus()

            ret.IsSuccess = False
            Return ret
        End If

        If txtPersonNameEN.Text = "" Then
            Alert("กรุณากรอกชื่อภาษาอังกฤษ")
            txtPersonNameEN.Focus()

            ret.IsSuccess = False
            Return ret
        End If

        If pnlUserInfo.Visible = True Then
            If ddlExecutingOrganize.SelectedValue = "0" Then
                Alert("กรุณาเลือกหน่วยงาน")
                txtPersonNameEN.Focus()

                ret.IsSuccess = False
                Return ret
            End If
        End If

        'เช็ค ID Card กับ Passport ID ซ้ำกัน
        Dim chkDupIdcardPassport As Boolean = ENG.CheckDupplicateIDCardPassport(txtPersonIDCard.Text.Trim, txtPersonPassportID.Text.Trim, txtNodeID.Text.Trim)
        If chkDupIdcardPassport = True Then
            Alert("กรุณากรอกข้อมูลใหม่ IDCard หรือ Passport ID ซ้ำ")
            ret.IsSuccess = False
            Return ret
        End If
        If RaRecipient.Checked = False And RaAssistant.Checked = False Then
            Alert("กรุณาระบุประเภทบุคคล")
            ret.IsSuccess = False
            Return ret
        End If

        If pnlUserInfo.Visible = True Then
            If txtUsername.Text.Trim = "" Then
                Alert("กรุณากรอก Username")
                ret.IsSuccess = False
                Return ret
            End If

            If txtNodeID.Text.Trim = "" Then
                'กรณีเพิ่มคนใหม่ให้ตรวจสอบรหัสผ่าน
                If txtPassword.Text.Trim = "" Then
                    Alert("กรุณากรอก Password")
                    ret.IsSuccess = False
                    Return ret
                End If
            End If

            If txtPassword.Text <> txtConfirmPassword.Text Then
                Alert("การยืนยันรหัสผ่านไม่ถูกต้อง")
                ret.IsSuccess = False
                Return ret
            End If
        End If

        Dim linqPerson As New TbOuPersonLinqDB
        linqPerson.NODE_ID = IIf(txtNodeID.Text.Trim = "", ENG.GetOUNodeID(), txtNodeID.Text)
        linqPerson.PARENT_ID = txtparentNodeID.Text.Trim
        linqPerson.NAME_TH = txtPersonNameTH.Text.Trim.Replace("'", "''")
        linqPerson.NAME_EN = txtPersonNameEN.Text.Trim.Replace("'", "''")
        If txtPersonBirthDay.Text <> "" Then
            linqPerson.BIRTHDATE = Converter.StringToDate(txtPersonBirthDay.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End If
        linqPerson.ID_CARD = txtPersonIDCard.Text.Trim.Replace("'", "''")
        linqPerson.PASSPORT_NO = txtPersonPassportID.Text.Trim.Replace("'", "''")
        linqPerson.ACTIVE_STATUS = Convert.ToChar(IIf(chkACTIVE_STATUS_Person.Checked, "Y", "N"))

        If pnlUserInfo.Visible = True Then
            linqPerson.USERNAME = txtUsername.Text

            If txtNodeID.Text = "" Then
                'กรณีเพิ่มใหม่ก็ให้บันทึก Password เลย
                linqPerson.PASSWORD = ENG.EncryptPss(txtPassword.Text)
            Else
                'กรณีแก้ไข ให้เช็คว่ามีการเปลี่ยน Password ด้วยมั้ย ถ้าใช่ก็บันทึก Password เลย
                If txtPassword.Text.Trim <> "" Then
                    linqPerson.PASSWORD = ENG.EncryptPss(txtPassword.Text)
                End If
            End If
        End If

        Dim dtP As New DataTable
        dtP.Columns.Add("DTPerson", GetType(TbOuContactLinqDB))
        Dim conLnq As New TbOuContactLinqDB
        conLnq.PARENT_TYPE = (Constants.Contact_Parent_Type.Person)
        conLnq.TELEPHONE = txtPersonTelephone.Text.Trim
        conLnq.EMAIL = txtPersonEmail.Text.Trim
        conLnq.DESCRIPTION = txtPersonNote.Text.Trim

        dtP.Rows.Add(conLnq)

        Dim persontype As Integer
        If RaAssistant.Checked = True Then
            persontype = 0
        ElseIf RaRecipient.Checked = True Then
            persontype = 1
        End If

        ret = ENG.SaveOUPerson(linqPerson, dtP, persontype, Username, IsSaveAuthorize)
        If ret.IsSuccess = True Then
            If pnlUserInfo.Visible = True Then
                UCFormAuthorize.NodeID = linqPerson.NODE_ID
                ret = UCFormAuthorize.SaveAuthorize(Username)
            End If
        End If

        If txtPassword.Text.Trim <> "" Then
            '---Save Template---
            Dim Sql As String = " select * from TB_OU_Person where node_id ='" & linqPerson.NODE_ID & "'"
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow
            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If
            DR_Temp("passwordNotConvert") = txtPassword.Text.Trim
            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
            End Try
        End If

        Return ret
    End Function

    Public Sub ClearData()
        'Clear Data Person
        'lblPersonCountryName.Text = ""
        'lblPersonOrganizeName.Text = ""
        'lblPersonSubOrganizeName.Text = ""

        txtPersonNameEN.Text = ""
        txtPersonNameTH.Text = ""
        txtPersonIDCard.Text = ""
        txtPersonPassportID.Text = ""
        txtPersonBirthDay.Text = ""
        txtPersonTelephone.Text = ""
        txtPersonEmail.Text = ""
        txtPersonNote.Text = ""
        chkACTIVE_STATUS_Person.Checked = True
        RaRecipient.Checked = False
        RaAssistant.Checked = True
        txtNodeID.Text = ""

        BindMenuList("")
    End Sub

    Public Sub BindMenuList(node_id As String)
        UCFormAuthorize.BindMenu(node_id)
    End Sub

    Private Sub ddlExecutingOrganize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlExecutingOrganize.SelectedIndexChanged
        If ddlExecutingOrganize.SelectedIndex > -1 Then
            txtparentNodeID.Text = ddlExecutingOrganize.SelectedValue
        End If
    End Sub
End Class
