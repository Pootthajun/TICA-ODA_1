﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityBudget.ascx.vb" Inherits="usercontrol_UCActivityBudget" %>

        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-gray">
                        <th>Budget Year<br />(ปีงบประมาณ)</th>
                        <th>Type Budget<br />(ประเภทงบประมาณ)</th>
                        <th>Sub Budget<br />(งบประมาณ)</th>
                        <th>Amount<br />(บาท)</th>
                        <th>Delete<br />(ลบ)</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-title="Budget Year <br /> (ปีงบประมาณ)" Style="width: 12%">
                                     <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" >
                                     </asp:DropDownList>
                                     <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                </td>
                                <td data-title="Type Budget(ประเภทงบประมาณ)" id="td" runat="server" Style="width: 25%">
                                    <asp:DropDownList ID="ddlBudgetGroup" runat="server" OnSelectedIndexChanged="ddlBudgetGroup_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control select2">
                                     </asp:DropDownList>
                                </td>
                                <td data-title="Sub Budget(งบประมาณ)" id="td1" runat="server" Style="width: 35%">
                                    <asp:DropDownList ID="ddlSubBudget" runat="server" CssClass="form-control select2">
                                     </asp:DropDownList>
                                </td>
                                <td data-title="Amount (บาท)" id="td2" runat="server" Style="width: 20%">
                                    <asp:TextBox ID="txtAmount" runat="server" style="text-align: right;" CssClass="form-control" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                                </td>
                                <td data-title="Delete" id="ColDelete" runat="server" Style="width: 12%">
                                    <center>
                                        <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');" />
                                    </center>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>

            </table>
             <div class="row">
              <div class="col-sm-7" style="text-align:center">
              <b>รวม</b>
              </div>
               <div class="col-sm-5">
                <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
              </div>
             </div>

            <div class="row">
                <center>
                    <div class="col-sm-12">
                        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                            CssClass="btn btn-primary" />
                    </div>
                </center>
            </div>
        </div>
