﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCFormOUPerson.ascx.vb" Inherits="usercontrol_UCFormOUPerson" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/usercontrol/UCFormAuthorize.ascx" TagPrefix="uc1" TagName="UCFormAuthorize" %>


<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered">
                    <tr class="bg-info">
                        <td colspan="2">
                            <h4 class="box-title">Personal Information</h4>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย : <span style="color: red">*</span></b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>

                                    <asp:TextBox ID="txtPersonNameTH" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:TextBox ID="txtNodeID" runat="server" style="display:none"></asp:TextBox>
                                    <asp:TextBox ID="txtparentNodeID" runat="server" style="display:none"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th style="width: 180px"><b class="pull-right">Firstname - Lastname : <span style="color: red">*</span></b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <asp:TextBox ID="txtPersonNameEN" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Id Card : <span style="color: red">*</span></b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                    <asp:TextBox ID="txtPersonIDCard" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Passport Id :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                    <asp:TextBox ID="txtPersonPassportID" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr id="trOranization" runat="server" visible="false" >
                        <th style="width: 180px"><b class="pull-right">Organization : <span style="color: red">*</span></b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </div>
                                    <asp:DropDownList ID="ddlExecutingOrganize" runat="server" CssClass="form-control select2" Style="width: 100%">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <%--<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                    <asp:TextBox ID="txtPersonBirthDay" CssClass="form-control" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'" MaxLength="8"></asp:TextBox>--%>

                                    <asp:TextBox CssClass="form-control m-b" ID="txtPersonBirthDay" runat="server" ></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtPersonBirthDay" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <asp:TextBox ID="txtPersonTelephone" CssClass="form-control" runat="server" MaxLength="9"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <asp:TextBox ID="txtPersonEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Style="color: red"
                                        ControlToValidate="txtPersonEmail" Display="Dynamic"
                                        ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง!"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                                    </asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Note (หมายเหตุ) :</b></th>
                        <td>
                            <div class="col-sm-12">

                                <asp:TextBox ID="txtPersonNote" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Person Type :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <label>
                                    <asp:RadioButton ID="RaAssistant" runat="server" GroupName="a" Checked="true" /> ผู้ช่วยเหลือโครงการ &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:RadioButton ID="RaRecipient" runat="server" GroupName="a" />ผู้รับทุน
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 180px"><b class="pull-right">Active Status :</b></th>
                        <td>
                            <div class="col-sm-12">
                                <label>
                                    <asp:CheckBox ID="chkACTIVE_STATUS_Person" runat="server" Checked="true" />
                                </label>
                            </div>
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="pnlUserInfo" runat="server" Visible="true" >
                    <table class="table table-bordered" >
                        <tr class="bg-info">
                            <td colspan="2">
                                <h4 class="box-title">Login Information</h4>
                            </td>
                        </tr>

                        <tr>
                            <th style="width: 180px"><b class="pull-right">Username : <span style="color: red">*</span></b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>

                                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Password : <span style="color: red">*</span></b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-key"></i>
                                        </div>

                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 180px"><b class="pull-right">Confirm Password : <span style="color: red">*</span></b></th>
                            <td>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-key"></i>
                                        </div>

                                        <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table class="table table-bordered" >
                        <tr class="bg-info">
                            <td >
                                <h4 class="box-title">User Authorization</h4>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <uc1:UCFormAuthorize runat="server" ID="UCFormAuthorize" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                        
            </div>
                    
        </div>
    </div>
</div>
