﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class UCActivityAdvanceExpense
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG
    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property


    Dim _AdvanceDT As DataTable
    Public Property AdvanceDT As DataTable
        Get
            Return Current_Data(rptList)
        End Get
        Set(value As DataTable)
            _AdvanceDT = value

            ImplementJavaMoneyText(txtAmount_Borrow)
            ImplementJavaMoneyText(txtAmount_Return)
            trDue_Date.Visible = False
            '-----Set Data----
            Dim Sql As String = " "
            Sql &= "    Select  TB_Activity_Advance.* ,TB_NAME  from TB_Activity_Advance  " & vbLf
            Sql &= "    LEFT JOIN vw_Overview_OU ON vw_Overview_OU.node_id=TB_Activity_Advance.Borrower_Node_ID  "

            Sql &= "   WHERE  TB_Activity_Advance.Advance_ID =" & CInt(Request.QueryString("Advance_ID"))
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then

                Select Case DT.Rows(0).Item("TB_NAME").ToString()

                    Case "TB_OU_Organize"
                        trDue_Date.Visible = False
                    Case "TB_OU_Person"
                        trDue_Date.Visible = True
                    Case Else
                        trDue_Date.Visible = False
                End Select


                '----ครบกำหนด
                If Not IsDBNull(DT.Rows(0).Item("Due_Date")) Then
                    txtDue_Date.Text = Convert.ToDateTime(DT.Rows(0).Item("Due_Date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                '----ยืม
                If Not IsDBNull(DT.Rows(0).Item("Advance_Date")) Then
                    txtDate_Borrow.Text = Convert.ToDateTime(DT.Rows(0).Item("Advance_Date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                txtDetail_Borrow.Text = DT.Rows(0).Item("Advance_Detail").ToString()
                txtDetailNo_Borrow.Text = DT.Rows(0).Item("Advance_No").ToString()

                If Not IsDBNull(DT.Rows(0).Item("Amount")) Then
                    txtAmount_Borrow.Text = Convert.ToDecimal(DT.Rows(0).Item("Amount")).ToString("#,##0.00")
                End If

                '----ใช้จ่าย
                rptList.DataSource = _AdvanceDT
                rptList.DataBind()

                '----คืน
                If Not IsDBNull(DT.Rows(0).Item("Cash_Back_Date")) Then
                    txtDate_Return.Text = Convert.ToDateTime(DT.Rows(0).Item("Cash_Back_Date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                End If
                txtDetail_Return.Text = DT.Rows(0).Item("Cash_Back_Detail").ToString()
                txtDetailNo_Return.Text = DT.Rows(0).Item("Cash_Back_No").ToString()

                If Not IsDBNull(DT.Rows(0).Item("Cash_Back")) Then
                    txtAmount_Return.Text = Convert.ToDecimal(DT.Rows(0).Item("Cash_Back")).ToString("#,##0.00")
                End If
            End If

            txtAmount_TextChanged(Nothing, Nothing)

        End Set
    End Property

    Public Sub SetToViewMode(IsEnable As Boolean)
        mode = Request.QueryString("mode").ToString()
        btnAdd.Visible = Not IsEnable
        btnSave.Visible = Not IsEnable
        pnlAdvance.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnDelete_Expense As Button = DirectCast(rptList.Items(i).FindControl("btnDelete_Expense"), Button)
            btnDelete_Expense.Visible = Not IsEnable
        Next
    End Sub

    Private Sub page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then

        End If
    End Sub

    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs)

        Dim DT_Data As DataTable = Current_Data(rptList)

        Dim Balance As Decimal = 0.0
        Dim Expense As Decimal = 0.0
        Dim SUM_Expense As Object
        If DT_Data.Rows.Count > 0 Then
            DT_Data.DefaultView.RowFilter = "Amount Is Not NULL"
            SUM_Expense = DT_Data.DefaultView.ToTable.Compute("SUM(Amount)", "")
        End If

        If txtAmount_Borrow.Text = "" Then
            txtAmount_Borrow.Text = 0.0
        End If
        If txtAmount_Return.Text = "" Then
            txtAmount_Return.Text = 0.0
        End If
        If Not IsDBNull(SUM_Expense) Then
            Expense = SUM_Expense
        End If
        Balance = (Convert.ToDecimal(txtAmount_Borrow.Text)) - (Expense + Convert.ToDecimal(txtAmount_Return.Text))


        lblTotal.Text = Convert.ToDecimal(Balance).ToString("#,##0.00")
    End Sub



    Function Current_Data(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Advance_Expense_ID")
        dt.Columns.Add("Advance_Expense_Date", GetType(DateTime))
        dt.Columns.Add("Advance_Expense_Detail")
        dt.Columns.Add("Advance_Expense_No")
        dt.Columns.Add("Amount", GetType(Decimal))

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1

            Dim txtDate_Expense As TextBox = DirectCast(rptList.Items(i).FindControl("txtDate_Expense"), TextBox)
            Dim txtDetail_Expense As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail_Expense"), TextBox)
            Dim txtDetailNo_Expense As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo_Expense"), TextBox)
            Dim txtAmount_Expense As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount_Expense"), TextBox)
            Dim lblID_Expense As Label = DirectCast(rptList.Items(i).FindControl("lblID_Expense"), Label)

            dr = dt.NewRow
            dr("Advance_Expense_ID") = lblID_Expense.Text
            If txtDate_Expense.Text <> "" Then
                dr("Advance_Expense_Date") = Converter.StringToDate(txtDate_Expense.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            dr("Advance_Expense_Detail") = txtDetail_Expense.Text
            dr("Advance_Expense_No") = txtDetailNo_Expense.Text
            If txtAmount_Expense.Text <> "" Then
                dr("Amount") = CDbl(txtAmount_Expense.Text)
            End If

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim txtDate_Expense As TextBox = DirectCast(e.Item.FindControl("txtDate_Expense"), TextBox)
        Dim txtDetail_Expense As TextBox = DirectCast(e.Item.FindControl("txtDetail_Expense"), TextBox)
        Dim txtDetailNo_Expense As TextBox = DirectCast(e.Item.FindControl("txtDetailNo_Expense"), TextBox)
        Dim txtAmount_Expense As TextBox = DirectCast(e.Item.FindControl("txtAmount_Expense"), TextBox)
        Dim lblID_Expense As Label = DirectCast(e.Item.FindControl("lblID_Expense"), Label)
        Dim btnDelete_Expense As Button = DirectCast(e.Item.FindControl("btnDelete_Expense"), Button)

        ImplementJavaMoneyText(txtAmount_Expense)
        lblID_Expense.Text = e.Item.DataItem("Advance_Expense_ID")
        If Not IsDBNull(e.Item.DataItem("Advance_Expense_Date")) Then
            txtDate_Expense.Text = Convert.ToDateTime(e.Item.DataItem("Advance_Expense_Date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End If
        txtDetail_Expense.Text = e.Item.DataItem("Advance_Expense_Detail").ToString()
        txtDetailNo_Expense.Text = e.Item.DataItem("Advance_Expense_No").ToString()
        If Not IsDBNull(e.Item.DataItem("Amount")) Then
            txtAmount_Expense.Text = Convert.ToDecimal(e.Item.DataItem("Amount")).ToString("#,##0.00")
        End If

        btnDelete_Expense.CommandArgument = e.Item.DataItem("Advance_Expense_ID")
    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim btnDelete_Expense As Button = DirectCast(e.Item.FindControl("btnDelete_Expense"), Button)
        If e.CommandName = "Delete" Then

            Dim dt As DataTable = Current_Data(rptList)
            dt.Rows.RemoveAt(e.Item.ItemIndex)
            rptList.DataSource = dt
            rptList.DataBind()

            txtAmount_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = Current_Data(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("Advance_Expense_ID") = dt.Rows.Count + 1
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        '-----Validate
        If trDue_Date.Visible = True Then
            If txtDue_Date.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('เลือกวันที่ ครบกำหนด');", True)
                Exit Sub
            End If
        End If


        If txtDate_Borrow.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('เลือกวันที่ รายการเงินยืมทดรอง');", True)
            Exit Sub
        End If
        If txtDetail_Borrow.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรอกรายละเอียด รายการเงินยืมทดรอง');", True)
            Exit Sub
        End If
        If txtAmount_Borrow.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรอกจำนวนเงิน รายการเงินยืมทดรอง');", True)
            Exit Sub
        End If

        If Convert.ToDecimal(txtAmount_Borrow.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ตรวจสอบจำนวนเงินต้องมากกว่า 0 รายการเงินยืมทดรอง');", True)
            Exit Sub
        End If

        Dim DT_Data As DataTable = Current_Data(rptList)
        DT_Data.DefaultView.RowFilter = "Advance_Expense_Date IS NULL OR Amount IS NULL OR Advance_Expense_Detail IS NULL"
        If DT_Data.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ตรวจสอบข้อมูล วันที่ รายละเอียด จำนวนเงิน รายการใช้จ่ายในตารางให้ครบถ้วน');", True)
            Exit Sub
        End If

        If txtDate_Return.Text <> "" And txtAmount_Return.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรอกจำนวนเงินคืน รายการเงินคืน');", True)
            Exit Sub
        End If

        '-----Save Header
        Dim Sql As String = " select * from TB_Activity_Advance where Advance_ID= " & CInt(Request.QueryString("Advance_ID"))
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR_Temp As DataRow
        If (DT.Rows.Count > 0) Then
            DR_Temp = DT.Rows(0)
            If trDue_Date.Visible Then
                '--ครบกำหนด
                DR_Temp("Due_Date") = Converter.StringToDate(txtDue_Date.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            '--ยืม
            DR_Temp("Advance_Date") = Converter.StringToDate(txtDate_Borrow.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            DR_Temp("Advance_Detail") = txtDetail_Borrow.Text
            DR_Temp("Advance_No") = txtDetailNo_Borrow.Text
            DR_Temp("Amount") = txtAmount_Borrow.Text.Replace(",", "")
            '--คืน

            If txtDate_Return.Text <> "" Then
                DR_Temp("Cash_Back_Date") = Converter.StringToDate(txtDate_Return.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            Else
                DR_Temp("Cash_Back_Date") = DBNull.Value
            End If
            DR_Temp("Cash_Back_Detail") = txtDetail_Return.Text
            DR_Temp("Cash_Back_No") = txtDetailNo_Return.Text
            DR_Temp("Cash_Back") = txtAmount_Return.Text.Replace(",", "")

        End If
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            Exit Sub
        End Try
        '-----Clear Detail
        '-----ลบรายการ Detail ค่าใช้จ่ายเงินยืม
        Dim Sql_Del As String = " "
        Sql_Del &= "   DELETE FROM TB_Activity_Advance_Expense "
        Sql_Del &= "   WHERE  Advance_ID =" & CInt(Request.QueryString("Advance_ID"))
        Dim DA_Del As New SqlDataAdapter(Sql_Del, BL.ConnectionString)
        Dim DT_Del As New DataTable
        DA_Del.Fill(DT_Del)

        '-----Save Detail
        Sql = " select * from TB_Activity_Advance_Expense where 0=1"
        DA = New SqlDataAdapter(Sql, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        Dim New_ID As Integer = BL.GetNew_ID("TB_Activity_Advance_Expense", "Advance_Expense_ID")
        For i As Integer = 0 To DT_Data.Rows.Count - 1
            DR_Temp = DT.NewRow
            '--ใช้จ่าย
            DR_Temp("Advance_Expense_ID") = New_ID + i
            DR_Temp("Advance_ID") = CInt(Request.QueryString("Advance_ID"))
            DR_Temp("Advance_Expense_Date") = DT_Data.Rows(i)("Advance_Expense_Date")
            DR_Temp("Advance_Expense_Detail") = DT_Data.Rows(i).Item("Advance_Expense_Detail").ToString()
            DR_Temp("Advance_Expense_No") = DT_Data.Rows(i).Item("Advance_Expense_No").ToString()
            DR_Temp("Amount") = DT_Data.Rows(i).Item("Amount")

            DT.Rows.Add(DR_Temp)
        Next

        cmd = New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('บันทึกเรียบร้อย');", True)

            Response.Redirect("frmProject_Detail_Activity.aspx?id=" & CInt(Request.QueryString("PID")) & "&mode=palnex" & "&type=" & CInt(Request.QueryString("type")) & "&mo=" & CInt(Request.QueryString("ActivityID")), False)





        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
            Exit Sub
        End Try

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("frmProject_Detail_Activity.aspx?id=" & CInt(Request.QueryString("PID")) & "&mode=" & mode & "&back=palnex" & "&type=" & CInt(Request.QueryString("type")) & "&mo=" & CInt(Request.QueryString("ActivityID")))

    End Sub
End Class
