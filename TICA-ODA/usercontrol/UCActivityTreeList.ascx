﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityTreeList.ascx.vb" Inherits="UCActivityTreeList" %>

<%@ Register src="UCActivityTreeNode.ascx" tagname="UCActivityTreeNode" tagprefix="uc1" %>


<asp:Label ID="lblProjectID" runat="server" Text="0" Visible="false"></asp:Label>
<asp:Label ID="lblActivityMode" runat="server" Text="" Visible="false"></asp:Label>
<%--<table style='width:1000px;' >--%>
<table style='width: 100%;' >
    <thead>
        <tr>
            <th style = 'width:300px' Class='tdbordertb tdborderl tdhearderinfo'>Activity Name</th>
            <th style = 'width:80px'  Class='tdbordertb tdhearderinfo'>Start Date</th>
            <th style = 'width:80px'  Class='tdbordertb tdhearderinfo'>End Date</th>
            <th style = 'width:60px;' Class='tdbordertb tdhearderinfo' id="thDuration" runat="server" visible ="false" >Duration</th>
            <th style = 'width:60px;' Class='tdbordertb tdhearderinfo'>Expense</th>
            <th style = 'width:80px;' Class='tdbordertb tdhearderinfo'>Budget</th>
           <%-- <td style = 'width:15px;' Class='tdbordertb tdborderr tdhearderinfo' id="tdHAdd" runat="server" >&nbsp;</td>
            <td style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo' >&nbsp;</td>
            <td style = 'width:30px;' Class='tdbordertb tdborderr tdhearderinfo' id="tdHDelete" runat="server" >&nbsp;</td>--%>
             


<%--<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Jan</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Feb</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Mar</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Apr</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >May</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Jun</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Jul</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Aug</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Sep</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Oct</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Nov</th>
<th style = ' border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Dec</th>
--%>






<%--            <asp:Repeater ID="rptCol" runat="server" Visible ="true" >
                                                <ItemTemplate>
                                                    <th id="th_radius_2" runat ="server" >
                                                        <asp:Label ID="lbl_Col_id" runat="server" Visible="false" ></asp:Label>

                                                        <asp:Label ID="lbl_Col_Name" runat="server" ></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>--%>


            <th style = 'width:70px;border-left-width: 1px; border-left-style: outset;' Class="tdbordertb tdborderr tdhearderinfo"  >Action</th>



        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="rpt" runat="server">
            <ItemTemplate>
                 <uc1:UCActivityTreeNode ID="UCActivityTreeNode1" runat="server" 
                     OnImageModeClick="ImageModeClick"
                     OnAddSubActivity="AddSubActivity"
                     OnEditActivity="EditActivity"
                     OnDeleteActivity="DeleteActivity"
                     OnViewActivity="ViewActivity" />
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
        </asp:Repeater>
    </tbody>                    
</table>

<script type="text/javascript">
// Specify the normal table row background color
//   and the background color for when the mouse 
//   hovers over the table row.

var TableBackgroundNormalColor = "#ffffff";
var TableBackgroundMouseoverColor = "#F2F2F2";

// These two functions need no customization.
function ChangeBackgroundColor(row,text) {
    row.style.backgroundColor = TableBackgroundMouseoverColor;
    text.style.backgroundColor = TableBackgroundMouseoverColor;
}

function RestoreBackgroundColor(row, text) {
    row.style.backgroundColor = TableBackgroundNormalColor;
    text.style.backgroundColor = TableBackgroundNormalColor;
}


</script>

<div class="clear">
</div>