﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCProjectCountryRe.ascx.vb" Inherits="usercontrol_UCProjectCountryRe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                 <th>ครั้งที่</th>
                <th>ประเทศที่รับทุน (Country of Recipient)</th>
                <th>จำนวนผู้รับทุน</th>
                <th style="width: 50px">Delete(ลบ)</th>
            </tr>
        </thead>
        <tbody>
              <asp:Label ID="lblActivityID" runat="server" Text ="0" Visible="false"></asp:Label>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td data-title="ครั้งที่" style="width:100px;padding:0px; vertical-align:middle; text-align:center;" >
                             <asp:Label ID="lblNo" runat="server" ></asp:Label>
                              <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td data-title="ประเทศที่รับทุน (Country of Recipient)" style ="padding:0px;">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 100%">
                            </asp:DropDownList>
                           <%-- <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>--%>
                        </td>
                        <td data-title="จำนวนผู้รับทุน (Number of recipients)" id="td" runat="server" width="200px;" style ="padding:0px;">
                            <asp:TextBox ID="txtExpand" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" AutoPostBack="true" OnTextChanged="txtExpand_TextChanged" MaxLength="12"></asp:TextBox>
                           <%-- <asp:Label ID="lblTotalAmount" runat="server" Text=" dssds" Visible="false"></asp:Label>--%>
                        </td>
                        <td data-title="Delete" id="ColDelete" runat="server" style ="padding:0px;">
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    <asp:Panel ID="pnlFooter" runat="server">
            <tfoot>
                <tr>
                    <td style="background-color:darkkhaki;text-align: center ;" colspan="2">
                        <b>รวม</b>
                    </td>
                    <td style="background-color:darkkhaki;text-align: right;">
                         <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                    </td>
                </tr>

            </tfoot>
            </asp:Panel>
    </table>


    <div class="row"  style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม" 
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>
