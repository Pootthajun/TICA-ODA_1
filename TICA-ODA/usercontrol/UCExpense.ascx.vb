﻿Imports System.Data
Partial Class usercontrol_UCExpense
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Dim status As String
    Dim dtDtial As DataTable
    Dim dtTemplate As DataTable


    Public Event EditBudget()

    Dim _ComponentDT As DataTable
    Public Property ComponentDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _ComponentDT = value
            If (_ComponentDT.Rows.Count > 0) Then
                'pnlFooter.Visible = True
            Else
                'pnlFooter.Visible = False
            End If

            'rptList.DataSource = _ComponentDT
            'rptList.DataBind()

        End Set
    End Property

    Dim _ExpenstListDT As DataTable
    Public Property ExpenstListDT As DataTable
        Get
            Return GetDataFromRptcol(rptList)
        End Get
        Set(value As DataTable)
            _ExpenstListDT = value
            If (_ExpenstListDT.Rows.Count > 0) Then
                'pnlFooter.Visible = True
            Else
                'pnlFooter.Visible = False
            End If

        End Set
    End Property
    Dim _RecipienceID As String
    Public WriteOnly Property RecipienceID As String
        Set(value As String)
            _RecipienceID = value
        End Set
    End Property

    Dim _ActivityID As String
    Public WriteOnly Property ActivityID As String
        Set(value As String)
            _ActivityID = value
        End Set
    End Property

    Dim _HeaderID As String
    Public WriteOnly Property HeaderID As String
        Set(value As String)
            _HeaderID = value
        End Set
    End Property

    Dim _TemplateID As String
    Public WriteOnly Property TemplateID As String
        Set(value As String)
            _TemplateID = value
        End Set
    End Property



    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)
            ddlCountry.Enabled = Not IsEnable
            txtExpand.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub




    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        status = "1"
        Dim dt As DataTable = GetDataCountryRe_FromRpt(rptList)
        dtDtial = GetDataFromRptcol(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        Dim idface As Integer
        For i As Integer = 0 To dt.Rows.Count - 1
            idface = dt.Rows(i)("id")
        Next
        dr("id") = idface + 1
        dt.Rows.Add(dr)

        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next
        For j As Integer = 0 To dt.Rows.Count - 1
            Dim swdate = dt.Rows(j)("Payment_Date_Plan").ToString()
            If swdate <> "" Then
                dt.Rows(j)("Payment_Date_Plan") = Converter.StringToDate(swdate.Trim, "dd/MM/yyyy")
            End If
        Next

        dtTemplate = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(lblTemplateId.Text))

        Dim id As Integer = idface + 1
        Dim drt As DataRow

        For j As Integer = 0 To dtTemplate.Rows.Count - 1
            drt = dtDtial.NewRow
            drt("Activity_Expense_Plan_id") = id.ToString
            drt("Expense_Sub_Activity_id") = dtTemplate.Rows(j).Item("id").ToString
            drt("Pay_Amount_Plan") = 0
            dtDtial.Rows.Add(drt)
        Next

        rptList.DataSource = dt
        rptList.DataBind()

    End Sub

    Function SetHead(Activity_id As Double) As DataTable
        Dim dt_col As New DataTable
        Dim template_id As Double = BL.GetList_TemplateByActivity(GL.ConvertCINT(Activity_id))
        lblTemplateId.Text = template_id.ToString
        dt_col = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(template_id))
        RptHead.DataSource = dt_col
        RptHead.DataBind()
    End Function

    Function SetDataRpt(Header_id As String) As DataTable

        lblActivityID1.Text = _ActivityID
        lblHeaderID.Text = _HeaderID
        lblTemplate.Text = _TemplateID
        lblRecipience.Text = _RecipienceID

        Dim dt As New DataTable
        dt = BL.GetDataExpensePlanList_FromQurey(Header_id, _RecipienceID)
        If dt.Rows.Count > 0 Then

            dt.Columns.Add("no", GetType(Long))

            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("no") = i + 1
            Next

            rptList.DataSource = dt
            rptList.DataBind()

        End If
        ' lblActivityID.Text = Header_id
        Return dt
    End Function

    Function SetDataForGetRpt(Aepi As String) As DataTable

        Dim dt As New DataTable

        dt.Columns.Add("amount")
        dt.Columns.Add("expense_sub_id")
        dt.Columns.Add("aepi")

        Dim dr As DataRow
        For j As Integer = 0 To dtDtial.Rows.Count - 1

            dr = dt.NewRow
            Dim ID As String = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
            If ID = Aepi Then
                dr("amount") = dtDtial.Rows(j).Item("Pay_Amount_Plan").ToString
                dr("expense_sub_id") = dtDtial.Rows(j).Item("Expense_Sub_Activity_id").ToString
                dr("aepi") = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
                dt.Rows.Add(dr)
            End If

        Next

        Return dt
    End Function

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Plan")
        dt.Columns.Add("Pay_Plan_Detail")
        dt.Columns.Add("Payment_Plan_Detail")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            dr("Payment_Date_Plan") = txtPaymentDate.Text
            dr("Pay_Plan_Detail") = txtDetail.Text
            dr("Payment_Plan_Detail") = txtDetailNo.Text

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Function GetDataFromRptcol(rpt As Repeater) As DataTable
        Dim dte As New DataTable
        dte.Columns.Add("Header_id")
        dte.Columns.Add("Recipience_id")
        dte.Columns.Add("Expense_Sub_Activity_id")
        dte.Columns.Add("Pay_Amount_Plan")
        dte.Columns.Add("Activity_Expense_Plan_id")

        Dim dre As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim rptColItem As Repeater = DirectCast(rptList.Items(i).FindControl("rptColItem"), Repeater)

            For j As Integer = 0 To rptColItem.Items.Count - 1
                Dim lblitem_subexpenseid As Label = DirectCast(rptColItem.Items(j).FindControl("lblitem_subexpenseid"), Label)
                Dim txtCol2 As TextBox = DirectCast(rptColItem.Items(j).FindControl("txtCol2"), TextBox)
                dre = dte.NewRow
                dre("Header_id") = lblitem_subexpenseid.Text
                dre("Recipience_id") = lblitem_subexpenseid.Text
                dre("Expense_Sub_Activity_id") = lblitem_subexpenseid.Text
                If txtCol2.Text <> "" Then
                    dre("Pay_Amount_Plan") = txtCol2.Text
                Else
                    dre("Pay_Amount_Plan") = 0
                End If
                dre("Activity_Expense_Plan_id") = lblID.Text


                dte.Rows.Add(dre)
            Next
        Next

        Return dte
    End Function

    Function GetDataCountryRe_FromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Plan")
        dt.Columns.Add("Pay_Plan_Detail")
        dt.Columns.Add("Payment_Plan_Detail")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            dr("Payment_Date_Plan") = txtPaymentDate.Text
            dr("Pay_Plan_Detail") = txtDetail.Text
            dr("Payment_Plan_Detail") = txtDetailNo.Text

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim txtDetail As TextBox = DirectCast(e.Item.FindControl("txtDetail"), TextBox)
        Dim txtDetailNo As TextBox = DirectCast(e.Item.FindControl("txtDetailNo"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim rptColItem As Repeater = DirectCast(e.Item.FindControl("rptColItem"), Repeater)
        Dim txtPaymentDate As TextBox = DirectCast(e.Item.FindControl("txtPaymentDate"), TextBox)


        lblID.Text = e.Item.DataItem("id").ToString
        txtPaymentDate.Text = e.Item.DataItem("Payment_Date_Plan").ToString
        If txtPaymentDate.Text <> "" Then
            txtPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End If
        txtDetail.Text = e.Item.DataItem("Pay_Plan_Detail").ToString
        txtDetailNo.Text = e.Item.DataItem("Payment_Plan_Detail").ToString

        AddHandler rptColItem.ItemDataBound, AddressOf rptColItem_ItemDataBound
        Dim dt As DataTable
        If status = "1" Then
            dt = SetDataForGetRpt(lblID.Text)
        Else
            dt = BL.GetList_SubExpenseDetailPlanList(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblRecipience.Text, lblID.Text)
        End If
        rptColItem.DataSource = dt
        rptColItem.DataBind()

    End Sub

    Protected Sub rptColItem_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim txtCol2 As TextBox = DirectCast(e.Item.FindControl("txtCol2"), TextBox)
        Dim lblitem_subexpenseid As Label = DirectCast(e.Item.FindControl("lblitem_subexpenseid"), Label)
        Dim Pay_Amount_Plan As Label = DirectCast(e.Item.FindControl("Pay_Amount_Plan"), Label)


        BL.SetTextDblKeypress(txtCol2)


        txtCol2.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
        'If _Type = "view" Then
        '    txtCol.Enabled = False
        'End If
        If txtCol2.Text = 0 Then
            txtCol2.Text = ""
        End If

        lblitem_subexpenseid.Text = e.Item.DataItem("expense_sub_id").ToString
        'Pay_Amount_Plan.Text = e.Item.DataItem("Pay_Amount_Plan").ToString
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "Delete" Then

            Dim dt As DataTable = GetDataFromRpt(rptList)
            dtDtial = GetDataFromRptcol(rptList)
            dt.Rows.RemoveAt(e.Item.ItemIndex)

            Dim dte As New DataTable
            dte.Columns.Add("Header_id")
            dte.Columns.Add("Recipience_id")
            dte.Columns.Add("Expense_Sub_Activity_id")
            dte.Columns.Add("Pay_Amount_Plan")
            dte.Columns.Add("Activity_Expense_Plan_id")

            Dim dre As DataRow

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim id = dt.Rows(i)("id").ToString()
                For j As Integer = 0 To dtDtial.Rows.Count - 1
                    If id = dtDtial.Rows(j)("Activity_Expense_Plan_id").ToString() Then
                        dre = dte.NewRow
                        dre("Header_id") = dtDtial.Rows(j)("Header_id").ToString()
                        dre("Recipience_id") = dtDtial.Rows(j)("Recipience_id").ToString()
                        dre("Expense_Sub_Activity_id") = dtDtial.Rows(j)("Expense_Sub_Activity_id").ToString()
                        dre("Pay_Amount_Plan") = dtDtial.Rows(j)("Pay_Amount_Plan").ToString()
                        dre("Activity_Expense_Plan_id") = dtDtial.Rows(j)("Activity_Expense_Plan_id").ToString()


                        dte.Rows.Add(dre)
                    End If
                Next
            Next

            dtDtial = dte
            status = "1"
            For j As Integer = 0 To dt.Rows.Count - 1
                Dim swdate = dt.Rows(j)("Payment_Date_Plan").ToString()
                If swdate <> "" Then
                    dt.Rows(j)("Payment_Date_Plan") = Converter.StringToDate(swdate.Trim, "dd/MM/yyyy")
                End If
            Next

            rptList.DataSource = dt
            rptList.DataBind()
        End If
    End Sub

    Private Sub RptHead_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptHead.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lbHeader As Label = DirectCast(e.Item.FindControl("lbHeader"), Label)

        lbHeader.Text = e.Item.DataItem("sub_name").ToString
    End Sub

End Class
