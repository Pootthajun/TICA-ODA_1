﻿Imports System.Data
Imports LinqDB.ConnectDB

Public Class UCActivityTreeList
    Inherits System.Web.UI.UserControl

    Public Event AddChildNode(ParentID As Long)
    Public Event EditChildNode(ActivityID As Long)
    Public Event ViewChildNode(ActivityID As Long)

    Dim BL As New ODAENG

    Public ReadOnly Property ProjectID As Long
        Get
            Return lblProjectID.Text
        End Get
    End Property
    Public ReadOnly Property ActivityMode As String
        Get
            Return lblActivityMode.Text
        End Get
    End Property

    Public WriteOnly Property SetViewMode As Boolean
        Set(value As Boolean)
            'tdHAdd.Visible = Not value
            'tdHDelete.Visible = Not value
        End Set
    End Property



    Public ReadOnly Property ProjectType As Long
        Get
            Return CInt(Request.QueryString("type"))
        End Get
    End Property

    Public ReadOnly Property CurrrentDocumentsData() As DataTable
        Get
            Dim DT As DataTable = BlankNodesData()
            For Each Item As RepeaterItem In rpt.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim DR As DataRow = DT.NewRow
                Dim NodeItem As UCActivityTreeNode = Item.FindControl("UCActivityTreeNode1")
                DR("id") = NodeItem.ActivityID
                DR("activity_name") = NodeItem.ActivityName
                DR("parent_id") = NodeItem.ParentID
                DR("Level") = NodeItem.NodeLevel
                DR("duration") = NodeItem.Duration
                DR("actual_start") = Converter.StringToDate(NodeItem.PlanStart, "dd/MM/yyyy")
                DR("actual_end") = Converter.StringToDate(NodeItem.PlanEnd, "dd/MM/yyyy")
                DR("IsExpanded") = NodeItem.IsExpand
                DR("expense") = NodeItem.Expense
                DR("commitment_budget") = NodeItem.CommitmentBudget
                DR("Child") = NodeItem.ChildQty
                DT.Rows.Add(DR)
            Next
            Return DT
        End Get
    End Property

    Private Function BlankNodesData() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("id", GetType(Long))
        DT.Columns.Add("activity_name")
        DT.Columns.Add("parent_id", GetType(Long))
        DT.Columns.Add("actual_start", GetType(Date))
        DT.Columns.Add("actual_end", GetType(Date))
        DT.Columns.Add("duration", GetType(Integer))
        DT.Columns.Add("commitment_budget", GetType(Decimal))
        DT.Columns.Add("Level", GetType(Integer))
        DT.Columns.Add("IsExpanded", GetType(Boolean))
        DT.Columns.Add("Child", GetType(Integer))
        DT.Columns.Add("expense", GetType(Decimal))
        DT.Columns.Add("childs_budget", GetType(Decimal))
        DT.Columns.Add("disbursement_Name")
        DT.Columns.Add("Is_Folder", GetType(Boolean))

        Return DT
    End Function

    Public Sub GenerateActivityList(ProjectID As Long, ActivityMode As String)

        BL.GetTreeList_Project_Activity_Budged(ProjectID)

        Dim ret As New DataTable
        Try
            lblProjectID.Text = ProjectID
            lblActivityMode.Text = ActivityMode

            'Bind_Col_Master_Year()

            Dim dt As New DataTable
            dt = BL.GetProjectActivityList(ProjectID)
            If dt.Rows.Count > 0 Then


                ret = BlankNodesData()

                'เริ่มต้นที่ ParentID=0
                dt.DefaultView.RowFilter = "parent_id=0"

                Dim pDt As New DataTable
                pDt = dt.DefaultView.ToTable.Copy()
                If pDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, pDt, dt, 0, 0)
                End If
                pDt.Dispose()

                If ret.Rows.Count > 0 Then
                    rpt.DataSource = ret
                    rpt.DataBind()
                End If

            Else
                ret = BlankNodesData()
                rpt.DataSource = ret
                rpt.DataBind()
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub GenerateActivityListLone(ProjectID As Long, ActivityMode As String)

        BL.GetTreeList_Lone_Contribuition_Activity_Budged(ProjectID)

        Dim ret As New DataTable
        Try
            lblProjectID.Text = ProjectID
            lblActivityMode.Text = ActivityMode

            Dim dt As New DataTable
            dt = BL.GetProjectActivityListLone(ProjectID)
            If dt.Rows.Count > 0 Then
                ret = BlankNodesData()

                'เริ่มต้นที่ ParentID=0
                dt.DefaultView.RowFilter = "parent_id=0"

                Dim pDt As New DataTable
                pDt = dt.DefaultView.ToTable.Copy()
                If pDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, pDt, dt, 0, 0)
                End If
                pDt.Dispose()

                If ret.Rows.Count > 0 Then
                    rpt.DataSource = ret
                    rpt.DataBind()
                End If

            Else
                ret = BlankNodesData()
                rpt.DataSource = ret
                rpt.DataBind()
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Function GenerateSubChildNode(ret As DataTable, pDt As DataTable, dt As DataTable, ParentID As Long, NodeLevel As Integer) As DataTable

        For i As Integer = 0 To pDt.Rows.Count - 1
            Try
                Dim dr As DataRow = pDt.Rows(i)
                Dim rdr As DataRow = ret.NewRow
                Dim _ActivityID As String = dr("id")
                Dim _parent_id As String = dr("parent_id")
                rdr("id") = _ActivityID
                rdr("activity_name") = dr("activity_name")
                rdr("parent_id") = _parent_id
                rdr("Is_Folder") = dr("Is_Folder")

                If Not IsDBNull(dr("actual_start")) Then
                    rdr("actual_start") = Convert.ToDateTime(dr("actual_start"))
                Else
                    rdr("actual_start") = DBNull.Value
                End If
                If Not IsDBNull(dr("actual_end")) Then
                    rdr("actual_end") = Convert.ToDateTime(dr("actual_end"))
                Else
                    rdr("actual_end") = DBNull.Value
                End If

                If Not IsDBNull(dr("duration")) Then
                    rdr("duration") = dr("duration")
                Else
                    rdr("duration") = DBNull.Value
                End If


                '-----ถ้าเป็น Folder 
                If Not IsDBNull(dr("expense")) Then
                    rdr("expense") = BL.Sum_Child_Project_Actual_Expense(dr("id"))
                Else
                    rdr("expense") = DBNull.Value
                End If

                If Not IsDBNull(dr("commitment_budget")) Then
                    rdr("commitment_budget") = dr("commitment_budget")
                Else
                    rdr("commitment_budget") = DBNull.Value
                End If
                rdr("Level") = NodeLevel
                rdr("IsExpanded") = False

                If Not IsDBNull(dr("childs_budget")) Then
                    rdr("childs_budget") = dr("childs_budget")
                Else
                    rdr("childs_budget") = Nothing
                End If

                If ProjectType = 2 Then
                    rdr("disbursement_Name") = dr("disbursement_Name")
                End If

                dt.DefaultView.RowFilter = "parent_id=" & _ActivityID
                Dim cDt As New DataTable
                cDt = dt.DefaultView.ToTable.Copy()
                rdr("Child") = cDt.Rows.Count

                rdr("childs_budget") = cDt.Compute("SUM(childs_budget)", "parent_id=" & _ActivityID)

                ret.Rows.Add(rdr)

                If cDt.Rows.Count > 0 Then
                    ret = GenerateSubChildNode(ret, cDt, dt, _parent_id, NodeLevel + 1)
                End If
                cDt.Dispose()
            Catch ex As Exception
                Dim Msg As String = ex.Message
            End Try

        Next

        Return ret
    End Function

    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim NodeItem As UCActivityTreeNode = e.Item.FindControl("UCActivityTreeNode1")
                NodeItem.ActivityID = e.Item.DataItem("id")
                NodeItem.ActivityName = e.Item.DataItem("activity_name")
                NodeItem.ParentID = e.Item.DataItem("parent_id")
                NodeItem.NodeLevel = e.Item.DataItem("Level")
                NodeItem.IsExpand = e.Item.DataItem("IsExpanded")
                If e.Item.DataItem("Is_Folder") Then
                    NodeItem.Is_Folder = "<i class='fa fa-folder text-info'></i>"
                Else
                    NodeItem.Is_Folder = "<i class='fa fa-font text-info' style='font-size: 9pt;' ></i>"
                End If
                If Not IsDBNull(e.Item.DataItem("duration")) Then
                    NodeItem.Duration = e.Item.DataItem("duration")
                End If
                If Not IsDBNull(e.Item.DataItem("Child")) Then
                    NodeItem.ChildQty = e.Item.DataItem("Child")
                End If
                If Not IsDBNull(e.Item.DataItem("expense")) Then
                    NodeItem.Expense = e.Item.DataItem("expense")
                End If
                If Not IsDBNull(e.Item.DataItem("commitment_budget")) Then
                    NodeItem.CommitmentBudget = e.Item.DataItem("commitment_budget")
                End If
                NodeItem.Project_ID = ProjectID

                '----สำหรับ Loan---
                If ProjectType = 2 Then
                    NodeItem.Disbursetment_Type = e.Item.DataItem("disbursement_Name").ToString()
                End If

                If Convert.IsDBNull(e.Item.DataItem("actual_start")) = False Then
                    NodeItem.PlanStart = Convert.ToDateTime(e.Item.DataItem("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    NodeItem.Year_MIN = Convert.ToDateTime(e.Item.DataItem("actual_start")).ToString("yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                If Convert.IsDBNull(e.Item.DataItem("actual_end")) = False Then
                    NodeItem.PlanEnd = Convert.ToDateTime(e.Item.DataItem("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    NodeItem.Year_MAX = Convert.ToDateTime(e.Item.DataItem("actual_end")).ToString("yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If

                '------------

                'If Not IsDBNull(e.Item.DataItem("commitment_budget")) And Not IsDBNull(e.Item.DataItem("childs_budget")) Then
                '    Dim _commitment_budget As Decimal = DirectCast(e.Item.DataItem("commitment_budget"), Decimal)
                '    Dim _childs_budget As Decimal = DirectCast(e.Item.DataItem("childs_budget"), Decimal)
                '    If _commitment_budget = _childs_budget Then
                '        NodeItem.SetBudgetColor(Drawing.Color.Green)
                '    Else
                '        If e.Item.DataItem("Child").ToString = "0" Then
                '            NodeItem.SetBudgetColor(Drawing.Color.Green)
                '        Else
                '            NodeItem.SetBudgetColor(Drawing.Color.Red)
                '            NodeItem.SetToolTipCommitmentBudget("ผลรวมไม่เท่ากัน")
                '        End If
                '    End If
                'End If



                If NodeItem.NodeLevel > 0 Then
                    e.Item.Visible = False
                End If

                If lblActivityMode.Text = "view" Then
                    NodeItem.SetViewMode = True
                End If

        End Select
    End Sub

#Region "Node Event"
    Protected Sub ImageModeClick(sender As UCActivityTreeNode, IsExpand As Boolean)
        SetExpandCollapt(sender.ActivityID, IsExpand)
    End Sub

    Private Sub SetExpandCollapt(ParentID As Long, IsExpand As Boolean)
        For Each grv As RepeaterItem In rpt.Items
            Dim uc As UCActivityTreeNode = grv.FindControl("UCActivityTreeNode1")
            If uc.ParentID = ParentID Then
                If IsExpand = True Then
                    grv.Visible = True
                Else
                    grv.Visible = False

                    If uc.ChildQty > 0 Then
                        uc.IsExpand = False
                        uc.ImageMode.ImageUrl = uc.ImageCollapsed
                        SetExpandCollapt(uc.ActivityID, IsExpand)
                    End If
                End If
            End If
        Next
    End Sub

    Protected Sub AddSubActivity(sender As UCActivityTreeNode)
        RaiseEvent AddChildNode(sender.ActivityID)
    End Sub

    Protected Sub EditActivity(sender As UCActivityTreeNode)
        'UCActivityTabProject1.EditActivity(sender.ActivityID)
        RaiseEvent EditChildNode(sender.ActivityID)
    End Sub

    Protected Sub ViewActivity(sender As UCActivityTreeNode)
        'UCActivityTabProject1.viewActivity(sender.ActivityID)
        RaiseEvent ViewChildNode(sender.ActivityID)
    End Sub





    Protected Sub DeleteActivity(ActivityID As Long)
        Dim ret As New ProcessReturnInfo
        ret = BL.DeleteActivity(ActivityID)
        Dim Type As DataTable = BL.FindType(lblProjectID.Text)
        Dim ty As String = Convert.ToDecimal(Type.Rows(0)("project_type")).ToString()
        If ret.IsSuccess Then
            alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
            If ty = "2" Or ty = "3" Then
                GenerateActivityListLone(lblProjectID.Text, lblActivityMode.Text)
            ElseIf ty = "0" Or ty = "1" Then
                GenerateActivityList(lblProjectID.Text, lblActivityMode.Text)
            End If

        Else
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
        End If
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub UCActivityTreeList_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub


#End Region





    'Public Sub SetActivity(project_id As String, activity_type As String)
    '    Try
    '        Dim str As New StringBuilder
    '        str = BL.SetActivity(ProjectID, "", "view", "year", "")
    '        ltActivity.Text = str.ToString()
    '    Catch ex As Exception

    '    End Try
    'End Sub


    '========Col==================

    '------Get Min Max Rank Project Activity------
    Public Sub Get_Rank_Activity()
        Dim DT As New DataTable

        Dim SQL As String = ""
        SQL &= " Select  DISTINCT * FROM( "
        SQL &= " Select  year(  [actual_start]) Rank_YEAR "
        SQL &= " From [TB_Activity] "
        SQL &= " UNION ALL "
        SQL &= "   Select  Year([actual_end])     Rank_YEAR  "
        SQL &= " From [TB_Activity] "
        SQL &= " ) As TB "
        SQL &= "  ORDER BY Rank_YEAR "

        Dim DT_Year As New DataTable
        Dim DR As DataRow
        DT = SqlDB.ExecuteTable(SQL)
        If (DT.Rows.Count > 0) Then
            If (DT.Rows.Count = 1) Then
                DR = DT_Year.NewRow
                DR("Year") = DT.Rows(0).Item("Year")
                DT_Year.Rows.Add(DR)
            Else
                For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
                    DR = DT_Year.NewRow
                    DR("Year") = i
                    DT_Year.Rows.Add(DR)
                Next
            End If
        End If



        'DT_Year.Columns.Add("Year")
        'If (DT.Rows.Count > 0) Then
        '    For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
        '        DR = DT_Year.NewRow
        '        DR("Year") = i
        '        DT_Year.Rows.Add(DR)
        '    Next
        'End If


    End Sub

    Function Get_Col_Year() As DataTable
        'Dim DT As New DataTable

        'Dim DT_Year As New DataTable

        'Dim DR As DataRow
        'DT_Year.Columns.Add("Year")
        ''DR = DT.NewRow(0)
        ''DR("Year") = 2012

        ''DR = DT.NewRow(0)
        ''DR("Year") = 2012
        ''DT.Rows.Add(DR)

        ''DR = DT.NewRow(1)
        ''DR("Year") = 2018
        ''DT.Rows.Add(DR)

        'Dim SQL As String = "SELECT 2012 Year Union All SELECT 2017 Year "
        'DT = SqlDB.ExecuteTable(SQL)
        'If (DT.Rows.Count > 0) Then
        '    For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
        '        DR = DT_Year.NewRow
        '        DR("Year") = i
        '        DT_Year.Rows.Add(DR)
        '    Next
        'End If


        Dim DT As New DataTable

        Dim SQL As String = ""
        SQL &= " Select  DISTINCT * FROM( "
        SQL &= " Select  year(  [actual_start]) Year "
        SQL &= " From [TB_Activity] where project_id=" & ProjectID & " "
        SQL &= " UNION ALL "
        SQL &= "   Select  Year([actual_end])     Year  "
        SQL &= " From [TB_Activity] where project_id=" & ProjectID & " "
        SQL &= " ) As TB "
        SQL &= "  ORDER BY Year "

        Dim DT_Year As New DataTable
        Dim DR As DataRow
        DT_Year.Columns.Add("Year")
        DT = SqlDB.ExecuteTable(SQL)
        If (DT.Rows.Count > 0) Then
            If (DT.Rows.Count = 1) Then
                DR = DT_Year.NewRow
                DR("Year") = DT.Rows(0).Item("Year")
                DT_Year.Rows.Add(DR)
            Else
                For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
                    DR = DT_Year.NewRow
                    DR("Year") = i
                    DT_Year.Rows.Add(DR)
                Next
            End If
        End If
        Return DT_Year
    End Function

    'Private Sub Bind_Col_Master_Year()
    '    Dim DT_Cooperation As DataTable = Get_Col_Year()
    '    rptCol.DataSource = DT_Cooperation
    '    rptCol.DataBind()


    'End Sub


    'Private Sub rptCol_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCol.ItemDataBound
    '    Dim lbl_Col_id As Label = e.Item.FindControl("lbl_Col_id")
    '    Dim lbl_Col_Name As Label = e.Item.FindControl("lbl_Col_Name")
    '    Dim th_radius_2 As HtmlTableCell = e.Item.FindControl("th_radius_2")

    '    'Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}

    '    'th_radius_2.Style("background-color") = ArrBrightPastel(e.Item.ItemIndex).ToString()
    '    'th_radius_2.Style("border-radius") = "30px 30px 0px 0px"
    '    'th_radius_2.Style("height") = "30px"

    '    lbl_Col_id.Text = e.Item.DataItem("Year")
    '    lbl_Col_Name.Text = e.Item.DataItem("Year").ToString()

    'End Sub

End Class
