﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityTabLoan.ascx.vb" Inherits="usercontrol_UCActivityTabLoan" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/usercontrol/UCDisbursementLoan.ascx" TagPrefix="uc1" TagName="UCDisbursementLoan" %>
<%@ Register Src="~/usercontrol/UCProjectCountryRe.ascx" TagName="UCProjectCountryRe" TagPrefix="uc2" %>
<%@ Register Src="~/usercontrol/UCprojectRecipience.ascx" TagName="UCprojectRecipience" TagPrefix="uc3" %>
<%@ Register Src="~/usercontrol/UCProjectCountry.ascx" TagName="UCProjectCountry" TagPrefix="uc4" %>

<script type="text/javascript">
        function confirmSelection() {
            if (confirm('คำเตือน : เนื่องจากมีการเบิกจ่ายให้กับประเทศ / ผู้รับทุนแล้ว หากมีการเปลี่ยนกลุ่มผู้รับทุน ระบบจะเคลียร์ข้อมูลการเบิกจ่ายก่อนหน้าทั้งหมด \n\nยืนยันการแปลี่ยนกลุ่มและเคลียข้อมูลการเบิกจ่าย ?')) {
                document.getElementById('<%=hdnRadioButtonSelector.ClientID %>').click();
                return true;
            }
            document.getElementById('<%=btn_ReturnFalse.ClientID %>').click();
                return false;
        }
    </script>
<style>
    .cost-uc {
        padding-right: 8.3% !important;
    }

        .cost-uc span {
            padding-left: 30px;
        }
</style>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlAdd" runat="server">
            <div class="modal-header">
                <div class="col-sm-8" style="margin-left: -15px; margin-bottom: 15px;">
                    <%--<b>*Title (หัวข้อกิจกรรม)</b>--%>
                    <b><span style="color: red;">* </span>Title (หัวข้อกิจกรรม)</b>
                    <asp:TextBox ID="txtActivityName" runat="server" CssClass="form-control m-b" Style="width: 100%;"></asp:TextBox>
                    <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="lblParentActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="lblProjectID" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="lblActivityMode" runat="server" Text="" Visible="false"></asp:Label>
                </div>

                <div class="col-sm-4">
                    <b><span style="color: red;">* </span>Group Aid (ประเภทความช่วยเหลือ)</b>
                    <asp:DropDownList ID="ddlSelectReport" runat="server" CssClass="form-control select2" Style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="lblGroup_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Group Aid</asp:ListItem>
                        <asp:ListItem Value="1">Component (ประเภทการให้ทุน)</asp:ListItem>
                        <asp:ListItem Value="2">In kind (ความช่วยเหลือด้านอื่นๆ)</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <asp:Panel ID="pnlTab" runat="server" Visible="false">
                        <ul class="nav nav-tabs">
                            <li class="" id="liTabActActDetail" runat="server">
                                <asp:LinkButton ID="btnTabActActDetail" runat="server">
                                        <div class="iti-flag gb" style="float:left; margin-top:3px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-list-alt"></i>ข้อมูลกิจกรรม</span></h5>
                                </asp:LinkButton>
                            </li>
                            <li class="" id="liTabActExpense" runat="server">
                                <asp:LinkButton ID="btnTabActExpense" runat="server">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>ข้อมูลค่าใช้จ่าย</span></h5>
                                </asp:LinkButton>
                            </li>
                            <%--<li class="" id="liTabActReceipent" runat="server">
                                        <asp:LinkButton ID="btnTabActReceipent" runat="server">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></h5>
                                        </asp:LinkButton>
                                    </li>--%>
                        </ul>
                    </asp:Panel>

                    <!--TabActActDetail-->
                    <asp:Panel ID="TabActActDetail" runat="server">

                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 180px; text-align: right;">
                                    <p><span style="color: red;">* </span>Sector (สาขา) :</p>
                                </td>
                                <td class="">
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <!-- /.form-group -->
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <p><span style="color: red;">* </span>Sector Type (สาขาย่อย) :</p>
                                </td>
                                <td class="">
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlSubSector" runat="server" CssClass="form-control select2" Style="width: 100%">
                                        </asp:DropDownList>
                                    </div>
                                    <!-- /.form-group -->
                                </td>
                            </tr>
                            <tr runat="server" id ="traid">
                                <td style="width: 200px">
                                    <p class="pull-right">
                                        </b>
                                                <br />
                                        <p class="pull-right"><span style="color: red;">* </span>type aid(ความช่วยเหลือย่อย) :</p>
                                </td>
                                <td>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlComponent" runat="server" CssClass="form-control select2" Style="width: 50%">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlInkind" runat="server" CssClass="form-control select2" Style="width: 50%">
                                        </asp:DropDownList>
                                        <%--<uc2:UCProjectComponent runat="server" ID="UCProjectComponent" />--%>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trPost" runat="server" visible ="false"  >
                                <td style="width: 200px">
                                    <asp:Label ID="Label1" runat="server"></asp:Label>

                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            <asp:CheckBox ID="chkActive" runat="server" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered">
                            <tr class="bg-info">
                                <th colspan="2">
                                    <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>ระยะเวลา</span></b></h5>
                                </th>
                            </tr>
                            <tr>
                                <td style="width: 200px; text-align: right;">
                                    <p class="pull-right">Plan(Start/End Date) </p>
                                    <p class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด) :</p>
                                </td>
                                <td>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtPlanActStart" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtPlanActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </div>
                                        <!-- /.input group -->

                                    </div>
                                    <div class="col-sm-1" style="margin-left: -15px;">
                                        <h5>
                                            <p class="text-black">(TO)ถึง</p>
                                            <h5></h5>
                                        </h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtplanActEnd" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtplanActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" style="margin-left: -15px;">
                                        <%--<label > day, week, month, year</label>--%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    <p class="pull-right">
                                        <span style="color: red;">*</span>
                                        Actual (Start/End Date) </b>
                                                <br />
                                        <p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p>
                                </td>
                                <td>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtActualActStart" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtActualActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </div>
                                        <!-- /.input group -->

                                    </div>
                                    <div class="col-sm-1" style="margin-left: -15px;">
                                        <h5>
                                            <p class="text-black">(TO)ถึง</p>
                                            <h5></h5>
                                        </h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtActualActEnd" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtActualActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" style="margin-left: -15px;">
                                        <%--<label > day, week, month, year</label>--%>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>

                    <%--TabActReceipent--%>
                    <asp:Panel ID="TabActRecipent" runat="server">
                        <table class="table table-bordered">
                            <asp:Panel ID="pnlRecipient_PSN" runat="server" Visible="false">
                                <asp:ListBox ID="ctlSelectRecipient" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                    data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                <asp:TextBox ID="txtRecipient" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                <asp:ListBox ID="ctlSelectRecipientCountry" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                    data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                <asp:TextBox ID="txtRecipientCountry" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                <asp:TextBox ID="txtBenificiary" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>


                            </asp:Panel>

                            <tr class="bg-info">
                                <th colspan="2">
                                    <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></b></h5>
                                </th>
                            </tr>
                            <tr>
                                    <td style="width: 210px; text-align: right">
                                         <asp:Button Text="hiddenButton" ID="hdnRadioButtonSelector" runat="server"  Visible ="false"  
                                        OnClick="hdnRadioButtonSelector_Click" />
                                        <asp:Button Text="hiddenButton" ID="btn_ReturnFalse" runat="server"   Style=" display :none;"
                                        /> 
                                        <asp:RadioButtonList ID="RadioSelectRe" runat="server"  Style="text-align: left"  AutoPostBack ="true"     >
                                            <asp:ListItem Selected="true" Text="Country Recipient(ระยะสั้น)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Recipient(ระยะยาว)" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Country(ประเทศ)" Value="3"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                <td>
                                    <uc2:UCProjectCountryRe runat="server" ID="UCProjectCountryRe" />
                                    <uc3:UCprojectRecipience runat="server" ID="UCprojectRecipience" Visible="false" />
                                    <uc4:UCProjectCountry runat="server" ID="UCProjectCountry" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <!--TabActExpense-->
                    <asp:Panel ID="TabActExpense" runat="server">

                        <%--Loan Budget--%>
                        <table class="table table-bordered">
                            <tr class="bg-info">
                                <th colspan="4">
                                    <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>ข้อมูลค่าใช้จ่าย</span></b></h5>
                                </th>
                            </tr>

                            <tr>
                                <td style="width: 200px; text-align: right;">
                                    <p>Loan Budget </p>
                                    <p class="pull-right">(งบประมาณเงินกู้) :</p>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <div class="input-group">

                                                <asp:TextBox ID="txtActGantCommitment" runat="server" Width="150px" CssClass="form-control cost" placeholder="" MaxLength="12"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">บาท</p>

                                            </h5>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </td>

                                <td style="width: 200px; text-align: right;">
                                    <p>Unallocated</p>
                                    <p class="pull-right">(งบประมาณที่ยังไม่ได้จัดสรรค์) :</p>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <div class="input-group">

                                                <asp:TextBox ID="txtbutged" runat="server" CssClass="form-control cost" placeholder="" MaxLength="12" Style="width: 150px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">บาท</p>

                                            </h5>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </td>
                                <!-- /.form group -->
                            </tr>
                                <tr id="tr_Error_compare" runat="server" >
                                    <td style="width: 200px; text-align: right;">
                                        <p> <b style="color :orangered;">หมายเหตุ </b></p> 
                                    </td>
                                    <td colspan ="3">
                                        <div class="col-sm-12">
                                            <b><asp:Label ID="lblError_compare" runat="server" Style="color :orangered;" ></asp:Label></b>
                                        </div>
                                    </td>
                                </tr>
                            <%----------------------------------------------------------------------%>

                            <%--Disbursement--%>
                            <tr>
                                <uc1:UCDisbursementLoan runat="server" ID="UCDisbursementLoan" Visible="false" />

                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Disbursement Type</p>
                                        <p class="pull-right">(ประเภทเงิน) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlSelectDisbursement" runat="server" CssClass="form-control select2" Style="width: 200px;" AutoPostBack="True">
                                                <asp:ListItem Value="">Select Disbursement Type</asp:ListItem>
                                                <asp:ListItem Value="1">Loan (เงินให้ยืม)</asp:ListItem>
                                                <asp:ListItem Value="2">Grant (เงินให้เปล่า)</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Template</p>
                                        <p class="pull-right">(รูปแบบการชำระเงิน) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="lbltemplate" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary" />
                                        </div>
                                    </td>

                                </tr>
                                

                                <td colspan="4">
                                    <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                                        <thead>
                                            <tr class="bg-gray">
                                                <th style="width: 19%;">Budget Date 
                                                        <br />
                                                    (วันที่จัดสรร)</th>
                                                <th style="width: 19%;">Payment Date
                                                        <br />
                                                    (วันที่จ่ายจริง)</th>
                                                <th style="width: 18%;">Budget
                                                        <br />
                                                    (งบประมาณ)</th>
                                                <th style="width: 18%;">Expense
                                                        <br />
                                                    (ค่าใช้จ่าย)</th>
                                                <th style="width: 18%;">Budget Total
                                                        <br />
                                                    (งบประมาณคงเหลือ)</th>
                                                <th class="tools">tools</br>(เครื่องมือ)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptList" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td data-title="Project ID" id="td1" style="text-align: center;" runat="server">
                                                            <asp:Label ID="lbldatePlan" runat="server"></asp:Label>
                                                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label></td>
                                                        <td data-title="Project Name" style="text-align: center;">
                                                            <asp:Label ID="lbldateActual" runat="server"></asp:Label></td>
                                                        <td data-title="Record" style="text-align: right; background-color: lightblue;">
                                                            <asp:Label ID="lblpayPlan" runat="server"></asp:Label></td>
                                                        <td data-title="Payment Date" runat="server" id="tdPayActual"  style="text-align: right;">
                                                            <asp:Label ID="lblpayActual" runat="server"></asp:Label>
                                                        </td>
                                                        <td data-title="Payment Date" style="text-align: right;" runat="server" id="td2">
                                                            <asp:Label ID="lblpayTotal" runat="server"></asp:Label>
                                                        </td>
                                                        <td data-title="Edit" id="ColEdit" runat="server" class="center">
                                                            <div class="btn-group">
                                                                <button type="button" data-toggle="dropdown" style="height: 25px; width: 35px;">
                                                                    <i class="fa fa-navicon text-green"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right">
                                                          <li id="liview" runat="server"  ><asp:LinkButton ID="btnView" CommandName ="cmdView" runat="server" CommandArgument='<%# Eval("id") %>'  Text="" CssClass="fa fa-search text-blue" > ดูรายละเอียด</asp:LinkButton></li>
                                                                    <li id="liedit" runat="server">
                                                                        <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                                      <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                    </li>
                                                                    <li id="lidelete" runat="server">
                                                                        <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                            CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                        <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                            <tfoot style="background-color: LemonChiffon;">
                                                <tr id="trFooter_Qty" runat="server">

                                                    <td style="text-align: center;" colspan="2"><b>Total</b></td>

                                                    <td style="text-align: right; text-decoration: underline;"><b>
                                                        <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                    <td style="text-align: right; text-decoration: underline;"><b>
                                                        <asp:Label ID="lblDisbursement_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                    <td style="text-align: right; text-decoration: underline;"><b>
                                                        <asp:Label ID="lblpayTotalSum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </asp:Panel>

                                    </table>
                                </td>

                            </tr>
                        </table>

                    </asp:Panel>

                </div>
            </div>

                        <asp:Panel CssClass="modal" ID="pnlAddRound" runat="server"  Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog" style=" width: 700px;margin-top:150px;" >

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">                                
                                <h4 class="modal-title">เพิ่มแผนข้อมูลค่าใช้จ่าย</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    

                                    <tr> 
                                        <td style="margin-left: 20px;">Plan start (กำหนดแผนค่าใช้จ่าย) :<span style="color: red">*</span>

                                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentStartDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            To (ถึง) :<span style="color: red">*</span>


                                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentEndDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>

                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                            <asp:Button ID="btnLoad" runat="server" ClientIDMode="Static" Text="Test" Style="display: none;" />
                                                        </td>
                                    </tr>

                                 </table>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="lnkDialogSave" runat="server" CssClass="btn btn-success">
                              Contunues >>
                                </asp:LinkButton><asp:LinkButton ID="lnkDialogCancel" runat="server" CssClass="btn btn-google">
                            Cancel
                                </asp:LinkButton>
                                
                                
                            </div>
                        </div>
                    </div>
                </asp:Panel>


            <!-- nav-tabs-custom -->
            <div class="modal-footer">
                <asp:LinkButton ID="btnSaveAct" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                </asp:LinkButton>
                <asp:LinkButton ID="btnCancelAct" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                </asp:LinkButton>
            </div>

        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
