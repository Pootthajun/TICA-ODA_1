﻿Imports System.Data
Imports LinqDB.ConnectDB
Partial Class usercontrol_UCFormAuthorize
    Inherits System.Web.UI.UserControl

    Dim BL As New ODAENG

    'Protected ReadOnly Property UserName As String
    '    Get
    '        Try
    '            Return Session("UserName")
    '        Catch ex As Exception
    '            Return "Administrator"
    '        End Try
    '    End Get
    'End Property

    Public WriteOnly Property NodeID As String
        Set(value As String)
            lblNodeID.text = value
        End Set
    End Property

    Private Sub usercontrol_UCFormAuthorize_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub



    Public Sub BindMenu(node_id As String)
        lblNodeID.Text = node_id
        Dim dt As New DataTable
        dt = BL.GetList_Menu(node_id)

        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim trHead As HtmlTableRow = DirectCast(e.Item.FindControl("trHead"), HtmlTableRow)
        Dim lblMenuName As Label = DirectCast(e.Item.FindControl("lblMenuName"), Label)
        Dim lblMenuID As Label = DirectCast(e.Item.FindControl("lblMenuID"), Label)
        Dim lblMenuType As Label = DirectCast(e.Item.FindControl("lblMenuType"), Label)
        Dim rdUpdate As RadioButton = DirectCast(e.Item.FindControl("rdUpdate"), RadioButton)
        Dim rdView As RadioButton = DirectCast(e.Item.FindControl("rdView"), RadioButton)
        Dim rdNa As RadioButton = DirectCast(e.Item.FindControl("rdNa"), RadioButton)

        'chkSelect.AutoPostBack = True
        'chkView.AutoPostBack = True
        'chkNA.AutoPostBack = True

        'AddHandler chkSelect.CheckedChanged, AddressOf chkSelectChange

        Dim menu_type As String = e.Item.DataItem("menu_type").ToString
        Dim menu_name As String = e.Item.DataItem("menu_name").ToString
        lblMenuName.Text = IIf(menu_type = "g", menu_name, "&nbsp;&nbsp;&nbsp;&nbsp;" & menu_name)
        lblMenuID.Text = e.Item.DataItem("id").ToString
        lblMenuType.Text = menu_type

        rdUpdate.Checked = IIf(e.Item.DataItem("is_save").ToString = "Y", True, False)
        rdView.Checked = IIf(e.Item.DataItem("is_view").ToString = "Y", True, False)
        rdNa.Checked = IIf(e.Item.DataItem("is_na").ToString = "Y", True, False)

        If menu_type = "g" Then
            'trHead.Attributes("class") = "trHead"
            trHead.BgColor = "#ebedf0"
            trHead.Height = "42px"
            lblMenuName.Font.Bold = True
            rdUpdate.Visible = False
            rdView.Visible = False
            rdNa.Visible = False
        End If
    End Sub

    Public Function SaveAuthorize(UserName As String) As ProcessReturnInfo
        Dim ret As New ProcessReturnInfo
        Dim trans As New TransactionDB
        Try
            Dim dt As New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("menu_name")
            dt.Columns.Add("is_save")
            dt.Columns.Add("is_view")
            dt.Columns.Add("is_na")

            Dim dr As DataRow
            For i As Integer = 0 To rptList.Items.Count - 1
                Dim lblMenuName As Label = DirectCast(rptList.Items(i).FindControl("lblMenuName"), Label)
                Dim lblMenuID As Label = DirectCast(rptList.Items(i).FindControl("lblMenuID"), Label)
                Dim lblMenuType As Label = DirectCast(rptList.Items(i).FindControl("lblMenuType"), Label)
                Dim rdUpdate As RadioButton = DirectCast(rptList.Items(i).FindControl("rdUpdate"), RadioButton)
                Dim rdView As RadioButton = DirectCast(rptList.Items(i).FindControl("rdView"), RadioButton)
                Dim rdNa As RadioButton = DirectCast(rptList.Items(i).FindControl("rdNa"), RadioButton)

                dr = dt.NewRow
                If lblMenuType.Text = "m" Then
                    dr("id") = lblMenuID.Text
                    dr("menu_name") = lblMenuName.Text
                    dr("is_save") = IIf(rdUpdate.Checked, "Y", "N")
                    dr("is_view") = IIf(rdView.Checked, "Y", "N")
                    dr("is_na") = IIf(rdNa.Checked, "Y", "N")
                    dt.Rows.Add(dr)
                End If
            Next

            ret = BL.SaveAuthorize(dt, lblNodeID.Text, UserName)

        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
            ret.IsSuccess = False
            ret.ErrorMessage = "Exception " & ex.Message
        End Try

        Return ret
    End Function

    'Private Sub btnCancle_Click(sender As Object, e As System.EventArgs) Handles btnCancle.Click

    'End Sub

    Protected Sub alertmsg(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub




End Class
