﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityTabProject.ascx.vb" Inherits="usercontrol_UCActivityTabProject" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<%@ Register src="UCProjectComponent.ascx" tagname="UCProjectComponent" tagprefix="uc2" %>
<%@ Register src="UCInkind.ascx" tagname="UCInkind" tagprefix="uc3" %>--%>
<%@ Register Src="UCActivityBudget.ascx" TagName="UCActivityBudget" TagPrefix="uc4" %>
<%@ Register Src="UCProjectDisbursement.ascx" TagName="UCProjectDisbursement" TagPrefix="uc5" %>
<%@ Register Src="~/usercontrol/UCProjectAdministrativeCost.ascx" TagName="UCProjectAdministrativeCost" TagPrefix="uc6" %>
<%@ Register Src="~/usercontrol/UCProjectCountryRe.ascx" TagName="UCProjectCountryRe" TagPrefix="uc7" %>
<%@ Register Src="~/usercontrol/UCprojectRecipience.ascx" TagName="UCprojectRecipience" TagPrefix="uc8" %>
<%@ Register Src="~/usercontrol/UCProjectCountry.ascx" TagName="UCProjectCountry" TagPrefix="uc9" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc10" TagName="PageNavigation" %>

<script type="text/javascript">
    function confirmSelection() {
        if (confirm('คำเตือน : เนื่องจากมีการเบิกจ่ายให้กับประเทศ / ผู้รับทุนแล้ว หากมีการเปลี่ยนกลุ่มผู้รับทุน ระบบจะเคลียร์ข้อมูลการเบิกจ่ายก่อนหน้าทั้งหมด \n\nยืนยันการแปลี่ยนกลุ่มและเคลียข้อมูลการเบิกจ่าย ?')) {
            document.getElementById('<%=hdnRadioButtonSelector.ClientID %>').click();
            return true;
        }
        document.getElementById('<%=btn_ReturnFalse.ClientID %>').click();
        return false;
    }
</script>


<style type="text/css">
    .gridResult tr td {
        background-color: white;
        cursor: pointer;
    }

    .gridResult tr:hover td {
        background-color: #ccccff;
    }
</style>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">


    <ContentTemplate>

        <asp:Panel ID="pnlAdd" runat="server">
            <div class="modal-header">
                <div class="col-sm-8" style="margin-left: -15px; margin-bottom: 15px;">
                    <%--<b>*Title (หัวข้อกิจกรรม)</b>--%>
                    <b><span style="color: red;">* </span>Title (หัวข้อกิจกรรม)</b>
                    <asp:TextBox ID="txtActName" runat="server" CssClass="form-control m-b" Style="width: 100%;"></asp:TextBox>
                </div>


                <div class="col-sm-4">
                    <b><span style="color: red;">* </span>Group Aid (ประเภทความช่วยเหลือ)</b>
                    <asp:DropDownList ID="ddlSelectReport" runat="server" CssClass="form-control select2" Style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="lblGroup_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Group Aid</asp:ListItem>
                        <asp:ListItem Value="1">Component (ประเภทการให้ทุน)</asp:ListItem>
                        <asp:ListItem Value="2">In kind (ความช่วยเหลือด้านอื่นๆ)</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <br />
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <asp:Panel ID="pnlTab" runat="server" Visible="false">
                        <ul class="nav nav-tabs">
                            <li class="" id="liTabActActDetail" runat="server">
                                <asp:LinkButton ID="btnTabActActDetail" runat="server" Style="padding: 4px 10px 5px 20px;">
                                        <div class="iti-flag gb" style="float:left; margin-top:3px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-list-alt"></i>ข้อมูลกิจกรรม</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActReceipent" runat="server">
                                <asp:LinkButton ID="btnTabActReceipent" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActPeriod" runat="server">
                                <asp:LinkButton ID="btnTabActPeriod" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>ระยะเวลา</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActExpense" runat="server">
                                <asp:LinkButton ID="btnTabActExpense" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>งบประมาณ</span></h5>
                                </asp:LinkButton>
                            </li>

                            <li class="" id="liTabActDisversment" runat="server">
                                <asp:LinkButton ID="btnTabActDisversment" runat="server" Style="padding: 4px 4px 5px 5px;">
                                        <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-th-list"></i>ค่าใช้จ่าย</span></h5>
                                </asp:LinkButton>
                            </li>

                        </ul>
                    </asp:Panel>
                    <div>
                        <%--TabActActDetail--%>
                        <asp:Panel ID="TabActActDetail" runat="server">
                            <table class="table table-bordered">
                                <%--<asp:TextBox ID="txtActName" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="250"></asp:TextBox>--%>
                                <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblParentActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblProjectID" runat="server" Text="0" Visible="false"></asp:Label>
                                <asp:Label ID="lblActivityMode" runat="server" Text="" Visible="false"></asp:Label>
                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">Description (รายละเอียด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtActDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right"><span style="color: red;">*</span>Sector (สาขา) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>

                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">Sector Type(สาขาย่อย) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlSubSector" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>

                                    </td>
                                </tr>
                                <tr runat="server" id="traid">
                                    <td style="width: 200px">
                                        <p class="pull-right">
                                            </b>
                                                <br />
                                            <p class="pull-right">aid type(ความช่วยเหลือย่อย) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlComponent" runat="server" CssClass="form-control select2" Style="width: 50%">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlInkind" runat="server" CssClass="form-control select2" Style="width: 50%">
                                            </asp:DropDownList>
                                            <%--<uc2:UCProjectComponent runat="server" ID="UCProjectComponent" />--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <asp:Label ID="Label1" runat="server"></asp:Label>

                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <asp:CheckBox ID="chkActive" runat="server" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActReceipent--%>
                        <asp:Panel ID="TabActRecipent" runat="server">
                            <table class="table table-bordered">
                                <asp:Panel ID="pnlRecipient_PSN" runat="server" Visible="false">
                                    <asp:ListBox ID="ctlSelectRecipient" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                        data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                    <asp:TextBox ID="txtRecipient" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                    <asp:ListBox ID="ctlSelectRecipientCountry" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                        data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                    <asp:TextBox ID="txtRecipientCountry" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>

                                    <asp:TextBox ID="txtBenificiary" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>


                                </asp:Panel>

                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-users"></i>ข้อมูลผู้รับทุน</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 210px; text-align: right">
                                        <asp:Button Text="hiddenButton" ID="hdnRadioButtonSelector" runat="server" Visible="false"
                                            OnClick="hdnRadioButtonSelector_Click" />
                                        <asp:Button Text="hiddenButton" ID="btn_ReturnFalse" runat="server" Style="display: none;" />
                                        <asp:RadioButtonList ID="RadioSelectRe" runat="server" Style="text-align: left" AutoPostBack="true">
                                            <asp:ListItem Selected="true" Text="Country Recipient(ระยะสั้น)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Recipient(ระยะยาว)" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Country(ประเทศ)" Value="3"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>

                                        <uc7:UCProjectCountryRe runat="server" ID="UCProjectCountryRe" />
                                        <uc8:UCprojectRecipience runat="server" ID="UCprojectRecipience" Visible="false" />
                                        <uc9:UCProjectCountry runat="server" ID="UCProjectCountry" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActPeriod--%>
                        <asp:Panel ID="TabActPeriod" runat="server">
                            <table class="table table-bordered">
                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>ระยะเวลา</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="pull-right">
                                            Plan(Start/End Date) </b>
                                                <br />
                                            <p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtPlanActStart" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtPlanActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">(TO)ถึง</p>
                                            </h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtplanActEnd" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtplanActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <%--<label class="pull-right"> day, week, month, year</label>--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <p class="pull-right">
                                            <span style="color: red;">*</span>Actual (Start/End Date) </b>
                                                <br />
                                            <p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtActualActStart" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtActualActStart" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                        <div class="col-sm-1">
                                            <h5>
                                                <p class="text-black">(TO)ถึง</p>
                                            </h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <asp:TextBox CssClass="form-control m-b" ID="txtActualActEnd" runat="server" placeholder=""></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server"
                                                    Format="dd/MM/yyyy" TargetControlID="txtActualActEnd" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <%--<label > day, week, month, year</label>--%>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <%--TabActExpense--%>
                        <asp:Panel ID="TabActExpense" runat="server">
                            <table class="table table-bordered">

                                <tr class="bg-info">
                                    <th colspan="4">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Allocated Budget (จัดสรรงบประมาณ)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p>Budget Year </p>
                                        <p class="pull-right">(ปีงบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack ="true" >
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Budget Type</p>
                                        <p class="pull-right">(ประเภทงบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlBudgetGroup" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Sub Budget </p>
                                        <p class="pull-right">(งบประมาณ) :</p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlBudgetSub" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>total budget</p>
                                        <p class="pull-right">(งบประมาณทั้งหมด) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control cost" placeholder="" MaxLength="12" Width="180px"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Unallocated</p>
                                        <p class="pull-right">(งบประมาณที่ยังไม่ได้จัดสรรค์) :</p>
                                    </td>
                                    <td>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbutged" runat="server" CssClass="form-control cost" placeholder="" MaxLength="12" Width="180px"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="tr_Error_compare" runat="server">
                                    <td style="width: 200px; text-align: right;">
                                        <p><b style="color: orangered;">หมายเหตุ </b></p>
                                    </td>
                                    <td colspan="3">
                                        <div class="col-sm-12">
                                            <b>
                                                <asp:Label ID="lblError_compare" runat="server" Style="color: orangered;"></asp:Label></b>
                                        </div>
                                    </td>
                                </tr>




                            </table>
                        </asp:Panel>

                        <%--TabActDisversement--%>
                        <asp:Panel ID="TabActDisversement" runat="server">
                            <table class="table table-bordered">
                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Expenditure plan Costing(แผนการเบิกจ่ายและค่าใช้จ่ายในกิจกรรม)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 200px; text-align: right;">
                                        <p><span style="color: red;">*</span>Template</p>
                                        <p class="pull-right">(รูปแบบการชำระเงิน) :</p>
                                    </td>
                                    <td>
                                        <br />
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="lbltemplate" runat="server" CssClass="form-control select2" Style="width: 100%">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="width: 19%;">Budget Date 
                                                        <br />
                                                        (วันที่จัดสรร)</th>
                                                    <th style="width: 19%;">Payment Date 
                                                        <br />
                                                        (วันที่จ่ายจริง)</th>
                                                    <th style="width: 18%;">Budget
                                                        <br />
                                                        (งบประมาณ)</th>
                                                    <th style="width: 18%;">Expense
                                                        <br />
                                                        (ค่าใช้จ่าย)</th>
                                                    <th style="width: 18%;">Budget Total
                                                        <br />
                                                        (งบประมาณคงเหลือ)</th>
                                                    <th class="tools">tools<br>
                                                        (เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Project ID" id="td1" style="text-align: center;" runat="server">
                                                                <asp:Label ID="lbldatePlan" runat="server"></asp:Label>
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label></td>
                                                            <td data-title="Project Name" style="text-align: center;">
                                                                <asp:Label ID="lbldateActual" runat="server"></asp:Label></td>
                                                            <td data-title="Record" style="text-align: right;">
                                                                <asp:Label ID="lblpayPlan" runat="server"></asp:Label></td>
                                                            <td data-title="Payment Date" style="text-align: right;" runat="server" id="tdPayActual">
                                                                <asp:Label ID="lblpayActual" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Payment Date" style="text-align: right;" runat="server" id="td2">
                                                                <asp:Label ID="lblpayTotal" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="center">
                                                                <div class="btn-group">
                                                                    <button type="button" data-toggle="dropdown" style="height: 25px; width: 35px;">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li id="lipay" runat="server">
                                                                            <asp:LinkButton ID="btnPay" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdPay" Visible="false">
                                                      <i class="fa fa-credit-card text-blue"></i> ชำระค่าใช้จ่าย</asp:LinkButton>
                                                                        </li>
                                                                        <li id="liview" runat="server"  ><asp:LinkButton ID="btnView" CommandName ="cmdView" runat="server" CommandArgument='<%# Eval("id") %>'  Text="" CssClass="fa fa-search text-blue" > ดูรายละเอียด</asp:LinkButton></li>
                                                                        <li id="liedit" runat="server">
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEdit">
                                                      <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                        </li>
                                                                        <li id="lidelete" runat="server">
                                                                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                                CommandArgument='<%# Eval("id") %>' CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td style="text-align: center;" colspan="2"><b>Total</b></td>

                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblDisbursement_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblpayTotalSum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </asp:Panel>

                                        </table>
                                    </td>
                                </tr>


                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Advance (เงินยืมทดรอง เงินจ่ายล่วงหน้า)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table id="tbAdvance" class="table table-bordered table-hover" style="width: 100%">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="width: 140px;">วันที่ยืม</th>
                                                    <th style="width: 140px;">วันครบกำหนด</th>
                                                    <th style="">ผู้ยืม</th>
                                                    <th style="">เลขที่หนังสือ</th>
                                                    <th style="">เงินยืมทดรอง</th>
                                                    <th style="">ยอดใช้จ่าย</th>
                                                    <th style="">เงินคืน</th>
                                                    <th style="">เงินยืมคงเหลือ</th>
                                                    <th class="tools">เครื่องมือ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptAdvance" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center;">
                                                                <asp:Label ID="lblBorrow_Date" runat="server"></asp:Label></td>
                                                             <td style="text-align: center;">
                                                                <asp:Label ID="lblDue_Date" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblBorrower_Node_Name" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblAdvance_No" runat="server"></asp:Label></td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="lblAdvance_Amount" runat="server"></asp:Label></td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="lblAdvance_Expense" runat="server"></asp:Label></td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="lblAdvance_Case_Back" runat="server"></asp:Label></td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="lblAdvance_Balance" runat="server"></asp:Label></td>

                                                            <td data-title="Edit" id="AdvanceEdit" runat="server" class="center">
                                                                <div class="btn-group">
                                                                    <button type="button" data-toggle="dropdown" style="height: 25px; width: 35px;">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li id="liAdvanceView" runat="server"  ><asp:LinkButton ID="btnView" CommandName ="cmdView" runat="server"  Text="" CssClass="fa fa-search text-blue" > ดูรายละเอียด</asp:LinkButton></li>
                                                                        <li id="liAdvanceEdit" runat="server">
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="cmdEdit">
                                                                            <i class="fa fa-pencil text-blue"></i> รายละเอียด</asp:LinkButton>
                                                                        </li>
                                                                        <li id="liAdvancedelete" runat="server">
                                                                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                                CommandName="cmdDelete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooterAdvance" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="tr1" runat="server">

                                                        <td style="text-align: center;" colspan="4"><b>Total</b></td>

                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblSum_Advance_Amount" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblSum_Advance_Expense" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblSum_Advance_Case_Back" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblSum_Advance_Balance" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </asp:Panel>


                                        </table>
                                        <div style="margin-top: 10px; text-align: center;">
                                            <asp:Button ID="btnAddAdvance" runat="server" Text="เพิ่มเงินยืมทดรอง" CssClass="btn btn-primary" />
                                        </div>


                                    </td>
                                </tr>



                                <tr class="bg-info">
                                    <th colspan="2">
                                        <h5 style="margin: 2px;"><b class="text-blue"><span style="cursor: pointer"><i class="fa fa-th-list"></i>Admin Cost (ค่าใช้จ่ายส่วนเกิน)</span></b></h5>
                                    </th>
                                </tr>
                                <tr>
                                    <%--<td style="width: 200px; text-align: right;">
                                        <p>Admincost </p>
                                        <p class="pull-right">(ค่าใช้จ่ายส่วนเกิน) :</p>
                                    </td>--%>
                                    <td colspan="2">
                                        <asp:Panel ID="pnlAdmincost" runat ="server" >
                                        <center>
                                            <uc6:UCProjectAdministrativeCost runat="server" ID="UCProjectAdministrativeCost" />
                                            <asp:Button ID="btnAddRow1" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
                                        </center>
                                            </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                    </div>
                </div>

                <asp:Panel CssClass="modal" ID="pnlAddRound" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog" style="width: 700px; margin-top: 150px;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">เพิ่มแผนข้อมูลค่าใช้จ่าย</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">


                                    <tr>
                                        <td style="margin-left: 20px;">Plan start (กำหนดแผนค่าใช้จ่าย) :<span style="color: red">*</span>

                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentStartDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            To (ถึง) :<span style="color: red">*</span>


                                            <asp:TextBox CssClass="form-control m-b" ID="txtPaymentEndDate" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>

                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtPaymentEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                            <asp:Button ID="btnLoad" runat="server" ClientIDMode="Static" Text="Test" Style="display: none;" />
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="lnkDialogSave" runat="server" CssClass="btn btn-success">
                              Contunues >>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDialogCancel" runat="server" CssClass="btn btn-google">
                            Cancel
                                </asp:LinkButton>


                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <asp:Panel CssClass="modal" ID="pnlAddAdvance" runat="server" Visible="true" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog" style="width: 700px; margin-top: 50px;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">เพิ่มเงินยืมทดรอง</h4>
                            </div>
                            <div class="modal-body">


                                <table class="table table-bordered">


                                    <tr>
                                        <td style="margin-left: 5px;">วันที่ยืม :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtDialogCash_Back_Date" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender7" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtDialogCash_Back_Date" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr id="trDue_Date" runat="server"  >
                                        <td style="margin-left: 5px;">วันครบกำหนด :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtDialogDue_Date" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender9" runat="server"
                                                Format="dd/MM/yyyy" TargetControlID="txtDialogDue_Date" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="margin-left: 5px;">ประเภทผู้ยืม :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:DropDownList ID="ddlDialog_TypeBorrower" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                                <asp:ListItem Value="" Text="--เลือก--"></asp:ListItem>
                                              <%--  <asp:ListItem Value="TB_OU_Country" Text="ประเทศ" ></asp:ListItem>--%>
                                                <asp:ListItem Value="TB_OU_Organize" Text="หน่วยงาน"></asp:ListItem>
                                                <asp:ListItem Value="TB_OU_Person" Text="บุคคล"></asp:ListItem>
                                            </asp:DropDownList>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-left: 5px;">ผู้ยืม :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:Label ID="lblDialogAdvance_Borrower" runat="server"></asp:Label>

                                            <div class="pull-right">
                                                <asp:Label ID="lblDialogAdvance_Borrower_ID" runat="server" Style="display: none;"></asp:Label>
                                                <asp:Button ID="btnAddBorrower" runat="server" Text="เลือกผู้ยืม" CssClass="btn btn-primary" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-left: 5px;">วัตถุประสงค์ :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtDialogAdvance_Detail" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-left: 5px;">เลขที่เอกสารเงินยืม :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtDialogAdvance_No" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="margin-left: 5px;">จำนวนเงิน :<span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox CssClass="form-control m-b" ID="txtDialogAdvance_Amount" runat="server" Style="width: 200px"></asp:TextBox>
                                        </td>
                                    </tr>

                                </table>



                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="lnkDialogAdvance_Save" runat="server" CssClass="btn btn-success">
                              Contunues >> 
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDialogAdvance_Cancel" runat="server" CssClass="btn btn-google">
                            Cancel
                                </asp:LinkButton>


                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <asp:Panel CssClass="modal" ID="dialog_Advance_OU" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog" style="width: 900px;">

                        <div class="modal-content">
                            <div class="modal-header" style="padding: 0.5px; margin-left: 20px;">
                                <h3><i class="icon-user"></i>เลือกผู้ยืม<div class="btn-group pull-right" style="margin-right: 20px; margin-top: -10px;">
                                    <asp:LinkButton ID="lnkAialog_Advance_OU_Close" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                                    </asp:LinkButton>
                                </div>
                                </h3>




                            </div>
                            <div class="modal-body" style="margin-top: -10px;">


                                <div class="row ">

                                    <div class="span6 " id="divCountry" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ประเทศ</label>
                                            <div class="col-sm-3">
                                                <asp:TextBox CssClass="form-control m-b" ID="txtdialog_Advance_OU_SearchCountry" runat="server" AutoPostBack="True" Placeholder="ค้นหาชื่อประเทศ"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6 " id="divOrg" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">หน่วยงาน</label>
                                            <div class="col-sm-3">
                                                <asp:TextBox CssClass="form-control m-b" ID="txtdialog_Advance_OU_SearchOrg" runat="server" AutoPostBack="True" Placeholder="ค้นหาชื่อหน่วยงาน"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="span6 " id="divPerson" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">บุคคล</label>
                                            <div class="col-sm-3">
                                                <asp:TextBox CssClass="form-control m-b" ID="txtdialog_Advance_OU_SearchPerson" runat="server" AutoPostBack="True" Placeholder="ค้นหาชื่อบุคคล"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <%--<div class="row ">

                                  
                                    <div class="span6 ">
                                        
                                    </div>


                                </div>--%>


                                <div class="row-fluid  ">
                                    <div class="portlet-body no-more-tables">
                                        <asp:Label ID="lblTotalSupplier" runat="server" Width="100%" Font-Size="16px" Font-Bold="True" Style="text-align: center;"></asp:Label>
                                        <table class="table table-bordered table-hover  gridResult">

                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="text-align: center;" id="thCountry" runat="server"><i class="icon-bookmark"></i>ประเทศ</th>
                                                    <th style="text-align: center;" id="thOrg" runat="server"><i class="icon-user"></i>หน่วยงาน</th>
                                                    <th style="text-align: center;" id="thPerson" runat="server"><i class="icon-user"></i>บุคคล</th>
                                                    <th style="text-align: center;" id="thOrg_Under" runat="server"><i class="icon-user"></i>หน่วยงานย่อย</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptAdvance_OU" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td id="td1" runat="server" data-title="ประเทศ" style="border-right: 1px solid #eeeeee;">
                                                                <asp:Label ID="lbldialog_Advance_OU_Country" runat="server"></asp:Label></td>
                                                            <td id="td2" runat="server" data-title="หน่วยงาน" style="border-right: 1px solid #eeeeee;">
                                                                <asp:Label ID="lbldialog_Advance_OU_Org" runat="server"></asp:Label></td>
                                                            <td id="td3" runat="server" data-title="บุคคล" style="border-right: 1px solid #eeeeee;">
                                                                <asp:Label ID="lbldialog_Advance_OU_Person" runat="server"></asp:Label></td>
                                                            <td id="td4" runat="server" data-title="หน่วยงานย่อย" style="border-right: 1px solid #eeeeee;">
                                                                <asp:Label ID="lbldialog_Advance_OU_Org_Under" runat="server"></asp:Label></td>

                                                            <asp:Button ID="btndialog_Advance_OU_Select" runat="server" Style="display: none;" CommandName="select" />
                                                            </td>                                                    
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <uc10:PageNavigation runat="server" ID="Pager" PageSize="7" />
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <asp:LinkButton ID="lnkAdd_NewPerson" runat="server" CssClass="btn btn-success">
                              Add Person 
                                    </asp:LinkButton>


                                </div>



                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel CssClass="modal" ID="dialog_New_Person" runat="server" ScrollBars="Auto" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">New Person</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-body">
                                                <table class="table table-bordered">
                                                    <tr class="bg-info">
                                                        <td colspan="2">
                                                            <h4 class="box-title">Personal Information</h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">ชื่อภาษาไทย : <span style="color: red">*</span></b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonNameTH" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">Firstname - Lastname : <span style="color: red">*</span></b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonNameEN" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">Id Card : <span style="color: red"></span></b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-credit-card"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonIDCard" runat="server" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">Passport Id :</b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-credit-card"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonPassportID" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr id="trCountry" runat="server" visible="true">
                                                        <th style="width: 180px"><b class="pull-right">Country : <span style="color: red">*</span></b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-university"></i>
                                                                    </div>
                                                                    <asp:DropDownList ID="ddlExecutingCountry" runat="server" CssClass="form-control select2" AutoPostBack ="true"  Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr id="trOranization" runat="server" visible="true">
                                                        <th style="width: 180px"><b class="pull-right">Organization : <span style="color: red">*</span></b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-university"></i>
                                                                    </div>
                                                                    <asp:DropDownList ID="ddlExecutingOrganize" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">Birthday (วันเกิด) :</b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <%--<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                    <asp:TextBox ID="txtPersonBirthDay" CssClass="form-control" runat="server" data-inputmask="'alias': 'dd/mm/yyyy'" MaxLength="8"></asp:TextBox>--%>

                                                                    <asp:TextBox CssClass="form-control m-b" ID="txtPersonBirthDay" runat="server"></asp:TextBox>
                                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender8" runat="server"
                                                                        Format="dd/MM/yyyy" TargetControlID="txtPersonBirthDay" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">Telephone (โทรศัพท์) :</b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-phone"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonTelephone" CssClass="form-control" runat="server"  ></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 180px"><b class="pull-right">E-mail :</b></th>
                                                        <td>
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-envelope-o"></i>
                                                                    </div>
                                                                    <asp:TextBox ID="txtPersonEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Style="color: red"
                                                                        ControlToValidate="txtPersonEmail" Display="Dynamic"
                                                                        ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง!"
                                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True">
                                                                    </asp:RegularExpressionValidator>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>


                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">

                                <asp:LinkButton ID="btnNewPersonSave" runat="server" CssClass="btn  btn-social btn-success">
                                    <i class="fa fa-save"></i> Save
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnNewPersonCancel" runat="server" CssClass="btn  btn-social btn-google">
                                    <i class="fa fa-reply"></i> Cancel
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>



                <div class="modal-footer">
                    <asp:LinkButton ID="btnSaveAct" runat="server" CssClass="btn  btn-social btn-success">
                            <i class="fa fa-save"></i> Save
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnCancelAct" runat="server" CssClass="btn  btn-social btn-google">
                            <i class="fa fa-reply"></i> Cancel
                    </asp:LinkButton>
                </div>
            </div>

        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>

<uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
