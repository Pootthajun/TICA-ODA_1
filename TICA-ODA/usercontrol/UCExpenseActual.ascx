﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCExpenseActual.ascx.vb" Inherits="usercontrol_UCExpenseActual" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="box-body">

    <div class="tb-fix">
           <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr class="bg-gray">
                    <th>วันที่</th>
                    <th>รายการ</th>
                    <th>เลขที่เอกสาร</th>
                    <asp:Label ID="lblActivityID1" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:Label ID="lblHeaderID" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:Label ID="lblTemplate" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:Label ID="lblRecipience" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:Label ID="lblTemplateId" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:Repeater ID="RptHead" runat="server">
                        <ItemTemplate>
                            <th style="width: 50px">
                                <asp:Label ID="lbHeader" runat="server"></asp:Label></th>
                        </ItemTemplate>
                    </asp:Repeater>
                    <th style="width: 50px">ลบ</th>
                </tr>
            </thead>
            <tbody>
                <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-title="วันที่" style="width:150px; padding: 0px; vertical-align: middle; text-align: center;">
                                <asp:Label ID="lblNo" runat="server">

                                </asp:Label><div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtPaymentDate" runat="server" placeholder=""></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtPaymentDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td style="padding: 0px; width: 200px;">
                                <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td id="td" runat="server" style="padding: 0px; width:80px">
                                <asp:TextBox ID="txtDetailNo" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" AutoPostBack="true" MaxLength="12"></asp:TextBox>
                            </td>
                            <asp:Repeater ID="rptColItem" runat="server" OnItemDataBound="rptColItem_ItemDataBound">
                                <ItemTemplate>
                                    <td style="padding: 0px">
                                        <asp:Label ID="lblitem_subexpenseid" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="Pay_Amount_Plan" runat="server" Visible="false"></asp:Label>
                                        <asp:TextBox ID="txtCol2" MaxLength="10" runat="server" Text="" Style="text-align: right; width:100%; height: 35px" class="cost accounting" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}"></asp:TextBox>
                                    </td>
                                </ItemTemplate>
                            </asp:Repeater>
                                                    <td data-title="Delete" id="ColDelete" runat="server" style ="padding:0px;">
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>

        </table>


        <div class="row" style="margin-top: 5px;">
            <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม" 
                    CssClass="btn btn-primary" />
            </div>
        </center>
        </div>
    </div>
</div>