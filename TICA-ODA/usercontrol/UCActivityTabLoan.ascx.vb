﻿Imports System.Data
Imports Constants
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Partial Class usercontrol_UCActivityTabLoan
    Inherits System.Web.UI.UserControl

    Public Event SaveActivityComplete()
    Public Event CancelAct()

    Dim BL As New ODAENG

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Private ReadOnly Property ActivityID_COPY As Integer
        Get
            Return Session("ActivityID_COPY")
        End Get
    End Property

    Public Property Old_Recipient_Type As String
        Get
            Try
                Return ViewState("Old_Recipient_Type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("Old_Recipient_Type") = value
        End Set
    End Property


    'Public Property ActivityID_COPY As Long
    '    Get
    '        Try
    '            Return Session("ActivityID_COPY")
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    Set(value As Long)
    '        Session("ActivityID_COPY") = value
    '    End Set
    'End Property

    Private Sub usercontrol_UCActivityTabLoan_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ProjectType = CInt(Request.QueryString("type"))
            BL.SetTextDblKeypress(txtActGantCommitment)

            If "edit" = lblActivityMode.Text Then
                If ddlSelectReport.SelectedValue = 1 Then
                    ddlComponent.Visible = True
                    ddlInkind.Visible = False
                    chkActive.Visible = True
                    Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                    traid.Visible = True
                ElseIf ddlSelectReport.SelectedValue = 2 Then
                    ddlComponent.Visible = False
                    ddlInkind.Visible = True
                    chkActive.Visible = False
                    chkActive.Checked = False
                    Label1.Text = ""
                    traid.Visible = True
                Else
                    ddlComponent.Visible = False
                    ddlInkind.Visible = True
                    chkActive.Visible = False
                    Label1.Text = ""
                    traid.Visible = False
                End If
            End If
            Authorize()
        End If
    End Sub

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public WriteOnly Property ProjectID As Long
        Set(value As Long)
            lblProjectID.Text = value
        End Set
    End Property
    Public WriteOnly Property ActivityMode As String
        Set(value As String)
            lblActivityMode.Text = value
        End Set
    End Property

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Public Sub SetControlToViewMode(IsView As Boolean)
        'pnlActivity.Enabled = Not IsView
        txtActivityName.Enabled = Not IsView
        ddlSector.Enabled = Not IsView
        ddlSubSector.Enabled = Not IsView
        ddlSelectReport.Enabled = Not IsView
        ddlInkind.Enabled = Not IsView
        chkActive.Enabled = Not IsView
        '#TabActReceipent
        TabActRecipent.Enabled = Not IsView

        btnApply.Enabled = Not IsView

        ddlSelectDisbursement.Enabled = Not IsView
        '#TabActExpense    
        txtPlanActStart.Enabled = Not IsView
        txtplanActEnd.Enabled = Not IsView
        txtActualActStart.Enabled = Not IsView
        txtActualActEnd.Enabled = Not IsView
        txtActGantCommitment.Enabled = Not IsView
        'txtActGantDisbursement.Enabled = Not IsView
        'txtActGantPaymentDate.Enabled = Not IsView
        'txtActLoanCommitment.Enabled = Not IsView
        'txtActLoanDisbursement.Enabled = Not IsView
        'txtActLoanPaymentDate.Enabled = Not IsView
        'txtActAdministrative.Enabled = Not IsView

        UCDisbursementLoan.SetToViewMode(IsView)

        btnSaveAct.Visible = Not IsView
    End Sub

    Public Sub AddActivity(ParentID As Long)
        ClearActivity()
        lblParentActivityID.Text = ParentID
        pnlAdd.Visible = True
        'CurrentTabAct = TAB.ActActDetail
    End Sub

    Public Sub EditActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        SetAcitivityInfoByID(ActivityID)
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        Next

    End Sub

    Public Sub ViewActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        SetAcitivityInfoByID(ActivityID)
        SetControlToViewMode(True)
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        Next
    End Sub

    Public Sub CopyActivity()
        '--เชคก่อนถ้ากิจกรรมที่ต้องการวางทับ มีข้อมูลการเบิกจ่ายแล้ว ต้องการคัดลอก activity ต้องเคลียร์ข้อมูลก่อน
        Dim DT As DataTable = BL.GetList_Expense2(lblActivityID.Text)
        If (DT.Rows.Count > 0) Then
            alertmsg("เนื่องจากกิจกรรมนี้  มีการกรอกข้อมูลการเบิกจ่ายแล้วจึงไม่สามารถ วางข้อมูลที่คัดลอกลงในกิจกรรมนี้ได้ \n **หากต้องการวางข้อมูล กรุณาลบข้อมูลการเบิกจ่ายก่อน")
            Exit Sub
        End If

        Clear_DataBeforPaste()
        'ActivityID_COPY
        If ActivityID_COPY = Nothing Then Exit Sub

        Dim dtActDetail As New DataTable
        dtActDetail = BL.GetProjectActivityInfoByID(ActivityID_COPY)
        If dtActDetail.Rows.Count > 0 Then

            'Title(หัวข้อกิจกรรม)
            txtActivityName.Text = dtActDetail.Rows(0).Item("ACTIVITY_NAME").ToString().Replace(" (copy)", "") & " (copy)"
            Try
                If dtActDetail.Rows(0).Item("sector_id").ToString() <> "" Then
                    ddlSector.SelectedValue = dtActDetail.Rows(0).Item("sector_id").ToString()
                    ddlSector_SelectedIndexChanged(Nothing, Nothing)
                    If dtActDetail.Rows(0).Item("sub_sector_id").ToString() <> "" Then ddlSubSector.SelectedValue = dtActDetail.Rows(0).Item("sub_sector_id").ToString()
                End If
            Catch ex As Exception
            End Try
            'ระยะเวลา
            If dtActDetail.Rows(0).Item("plan_start").ToString() <> "" Then
                txtPlanActStart.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("plan_end").ToString() <> "" Then

                txtplanActEnd.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("actual_start").ToString() <> "" Then
                txtActualActStart.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("actual_end").ToString() <> "" Then
                txtActualActEnd.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            'ข้อมูลผู้รับทุน

            Select Case dtActDetail.Rows(0).Item("Recipient_Type").ToString()
                Case "G"
                    RadioSelectRe.SelectedValue = 1
                    UCProjectCountryRe.Visible = True
                    UCprojectRecipience.Visible = False
                    UCProjectCountry.Visible = False
                Case "R"
                    RadioSelectRe.SelectedValue = 2
                    UCProjectCountryRe.Visible = False
                    UCprojectRecipience.Visible = True
                    UCProjectCountry.Visible = False
                Case "C"
                    RadioSelectRe.SelectedValue = 3
                    UCProjectCountryRe.Visible = False
                    UCprojectRecipience.Visible = False
                    UCProjectCountry.Visible = True

            End Select

            Dim dtActProjectCountryRe As New DataTable
            dtActProjectCountryRe = UCProjectCountryRe.SetDataRpt(ActivityID_COPY)

            Dim dtUCProjectCountry As New DataTable
            dtUCProjectCountry = UCProjectCountry.SetDataRpt(ActivityID_COPY)

            Dim dtAcProjectRecipience As New DataTable
            dtAcProjectRecipience = UCprojectRecipience.SetDataRpt(ActivityID_COPY)


            'Group Aid(ประเภทความช่วยเหลือ)

            Dim dtComponent As DataTable = BL.GetActivityComponent(ActivityID_COPY)
            If dtComponent.Rows.Count > 0 Then
                '    UCActivityBudget.BudgetDT = dtActivityBudget
                Dim _component_id As String = dtComponent.Rows(0)("component_id").ToString
                ddlComponent.Visible = True
                ddlComponent.SelectedValue = _component_id
                ddlInkind.Visible = False
                ddlSelectReport.SelectedValue = 1
                chkActive.Visible = True
                Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                traid.Visible = True
            End If


            Dim dtInkind As DataTable = BL.GetProjectActivityInkind(ActivityID_COPY)
            If dtInkind.Rows.Count > 0 Then
                '    UCActivityBudget.BudgetDT = dtActivityBudget
                Dim _inkind_id As String = dtInkind.Rows(0)("inkind_id").ToString
                ddlInkind.SelectedValue = _inkind_id
                ddlInkind.Visible = True
                ddlSelectReport.SelectedValue = 2
                ddlComponent.Visible = False
                Label1.Text = ""
                traid.Visible = True
            End If

            alertmsg("วางข้อมูลเรียบร้อย")
        End If


    End Sub

    Private Sub Clear_DataBeforPaste()
        'Title(หัวข้อกิจกรรม)
        'ระยะเวลา
        'ข้อมูลผู้รับทุน
        'Group Aid(ประเภทความช่วยเหลือ)
        Old_Recipient_Type = ""
        '##tabActivity
        '#TabActActDetail
        txtActivityName.Text = ""
        ddlSelectReport.SelectedValue = 0
        BL.Bind_DDL_Perposecat(ddlSector)
        ddlSector_SelectedIndexChanged(Nothing, Nothing)

        BL.Bind_DDL_ExpenseTemplate(lbltemplate)
        BL.Bind_DDL_Component(ddlComponent)
        BL.Bind_DDL_Inkind(ddlInkind)
        ddlInkind.SelectedValue = ""
        ddlComponent.SelectedValue = ""
        Label1.Text = ""
        chkActive.Checked = False
        chkActive.Visible = False

        txtPlanActStart.Text = ""
        txtplanActEnd.Text = ""
        txtActualActStart.Text = ""
        txtActualActEnd.Text = ""

        RadioSelectRe.SelectedValue = 1


        'lblParentActivityID.Text = "0"




        'txtbutged.Text = ""
        'ddlSelectDisbursement.SelectedValue = ""



        ''#TabActExpense
        'txtActGantCommitment.Text = ""
        'txtActGantDisbursement.Text = ""
        'txtActGantPaymentDate.Text = ""
        'txtActLoanCommitment.Text = ""
        'txtActLoanDisbursement.Text = ""
        'txtActLoanPaymentDate.Text = ""
        'txtActAdministrative.Text = ""


        'Dim dtDisbursement As New DataTable
        'With dtDisbursement
        '    .Columns.Add("id")
        '    .Columns.Add("Type_ID")
        '    .Columns.Add("Country_ID")
        '    .Columns.Add("Payment_Date")
        '    .Columns.Add("Amount")
        'End With
        'UCDisbursementLoan.DisbursementDT = dtDisbursement

        Dim dtCountryRe As New DataTable
        With dtCountryRe
            .Columns.Add("id")
            .Columns.Add("country_node_id")
            .Columns.Add("amout_person")
        End With
        UCProjectCountryRe.ComponentDT = dtCountryRe

        Dim dtCountry As New DataTable
        With dtCountry
            .Columns.Add("id")
            .Columns.Add("country_node_id")
        End With
        UCProjectCountry.CountryDT = dtCountry

        Dim dtRecipient As New DataTable
        With dtRecipient
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("recipient_id")
        End With
        UCprojectRecipience.RecipienceDT = dtRecipient
        UCprojectRecipience.Bindata_toRptCountRe()

        'BindList(0)



    End Sub
    'Session("ActivityID_COPY")   '-- Session ที่คัดลอกไว้

    Private Sub SetAcitivityInfoByID(activity_id As String)

        '#TabActActDetail
        Dim dtActDetail As New DataTable
        dtActDetail = BL.GetProjectActivityInfoByID(activity_id)

        Dim _ACTIVITY_NAME As String = ""
        Dim _sector_id As String = ""
        Dim _sub_sector_id As String = ""
        Dim _plan_start As String = ""
        Dim _plan_end As String = ""
        Dim _actual_start As String = ""
        Dim _actual_end As String = ""
        Dim _commitment_budget As String = ""
        Dim _disbursement As String = ""
        Dim _administrative As String = ""
        Dim _commitment_budget_loan As String = ""
        Dim _disbursement_loan As String = ""
        Dim _payment_date_gant As String = ""
        Dim _payment_date_loan As String = ""
        Dim _beneficiary As String = ""
        Dim _parent_id As String = ""
        Dim _disbursement_type As String = ""
        Dim _Recipient_type As String = ""

        If dtActDetail.Rows.Count > 0 Then
            Dim _dr As DataRow = dtActDetail.Rows(0)
            _parent_id = _dr("parent_id").ToString()
            _ACTIVITY_NAME = _dr("ACTIVITY_NAME").ToString()
            _sector_id = _dr("sector_id").ToString()
            _sub_sector_id = _dr("sub_sector_id").ToString()
            If _dr("plan_start").ToString() <> "" Then
                _plan_start = Convert.ToDateTime(_dr("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If _dr("plan_end").ToString() <> "" Then

                _plan_end = Convert.ToDateTime(_dr("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            If _dr("actual_start").ToString() <> "" Then
                _actual_start = Convert.ToDateTime(_dr("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If _dr("actual_end").ToString() <> "" Then
                _actual_end = Convert.ToDateTime(_dr("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            _commitment_budget = Convert.ToDecimal(_dr("commitment_budget")).ToString("#,##0.00")
            _disbursement = _dr("disbursement").ToString()
            _administrative = _dr("administrative").ToString()
            _commitment_budget_loan = _dr("commitment_budget_loan").ToString()
            _disbursement_loan = _dr("disbursement_loan").ToString()
            _Recipient_type = _dr("Recipient_Type").ToString()
            If _dr("payment_date_gant").ToString() <> "" Then
                _payment_date_gant = Convert.ToDateTime(_dr("payment_date_gant")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If _dr("payment_date_loan").ToString() <> "" Then
                _payment_date_loan = Convert.ToDateTime(_dr("payment_date_loan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            _beneficiary = _dr("beneficiary").ToString()
            _Recipient_type = _dr("Recipient_Type").ToString()
            _disbursement_type = _dr("disbursement_type").ToString()



        End If


        lblActivityID.Text = activity_id
        txtActivityName.Text = _ACTIVITY_NAME
        Try
            If _sector_id <> "" Then
                ddlSector.SelectedValue = _sector_id
                ddlSector_SelectedIndexChanged(Nothing, Nothing)
                If _sub_sector_id <> "" Then ddlSubSector.SelectedValue = _sub_sector_id
            End If
        Catch ex As Exception
        End Try
        If _disbursement_type <> "" Then
            ddlSelectDisbursement.SelectedValue = _disbursement_type
        Else
            ddlSelectDisbursement.SelectedValue = ""
        End If

        If _Recipient_type = "G" Then
            RadioSelectRe.SelectedValue = 1
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountry.Visible = False
        ElseIf _Recipient_type = "R" Then
            RadioSelectRe.SelectedValue = 2
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = True
            UCProjectCountry.Visible = False
        ElseIf _Recipient_type = "C" Then
            RadioSelectRe.SelectedValue = 3
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = False
            UCProjectCountry.Visible = True
        End If
        Old_Recipient_Type = RadioSelectRe.SelectedValue

        txtPlanActStart.Text = _plan_start
        txtplanActEnd.Text = _plan_end
        txtActualActStart.Text = _actual_start
        txtActualActEnd.Text = _actual_end
        lblParentActivityID.Text = _parent_id

        '#TabActExpense
        txtActGantCommitment.Text = _commitment_budget
        'txtActGantDisbursement.Text = _disbursement
        'txtActGantPaymentDate.Text = _payment_date_gant
        'txtActLoanCommitment.Text = _commitment_budget_loan
        'txtActLoanDisbursement.Text = _disbursement_loan
        'txtActLoanPaymentDate.Text = _payment_date_loan
        'txtActAdministrative.Text = _administrative

        Dim dtDisbursement As New DataTable
        dtDisbursement = BL.GetProjectActivityDisbursement(activity_id)
        UCDisbursementLoan.DisbursementDT = dtDisbursement

        Dim dtActProjectCountryRe As New DataTable
        dtActProjectCountryRe = UCProjectCountryRe.SetDataRpt(activity_id)

        Dim dtUCProjectCountry As New DataTable
        dtUCProjectCountry = UCProjectCountry.SetDataRpt(activity_id)

        Dim dtAcProjectRecipience As New DataTable
        dtAcProjectRecipience = UCprojectRecipience.SetDataRpt(activity_id)

        Dim dtComponent As DataTable = BL.GetActivityComponent(activity_id)
        If dtComponent.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _component_id As String = dtComponent.Rows(0)("component_id").ToString
            ddlComponent.Visible = True
            ddlComponent.SelectedValue = _component_id
            ddlInkind.Visible = False
            ddlSelectReport.SelectedValue = 1
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        End If



        Dim dtInkind As DataTable = BL.GetProjectActivityInkind(activity_id)
        If dtInkind.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _inkind_id As String = dtInkind.Rows(0)("inkind_id").ToString
            ddlInkind.SelectedValue = _inkind_id
            ddlInkind.Visible = True
            ddlSelectReport.SelectedValue = 2
            ddlComponent.Visible = False
            Label1.Text = ""
            traid.Visible = True
        End If

        Dim dtActemplate As DataTable = BL.GetActivityTemplate(activity_id)
        If dtActemplate.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _template_id As String = dtActemplate.Rows(0)("template_id").ToString
            lbltemplate.SelectedValue = _template_id

        End If
        BindList(activity_id)
    End Sub

    Private Sub ClearActivity()
        '##tabActivity
        '#TabActActDetail
        lblActivityID.Text = "0"
        lblParentActivityID.Text = "0"

        Old_Recipient_Type = ""
        txtActivityName.Text = ""
        lblParentActivityID.Text = "0"
        BL.Bind_DDL_Perposecat(ddlSector)
        ddlSector_SelectedIndexChanged(Nothing, Nothing)
        txtPlanActStart.Text = ""
        txtplanActEnd.Text = ""
        txtActualActStart.Text = ""
        txtActualActEnd.Text = ""
        txtbutged.Text = ""
        ddlSelectDisbursement.SelectedValue = ""

        chkActive.Checked = False
        ddlSelectReport.SelectedValue = 0
        ddlInkind.SelectedValue = ""
        ddlComponent.SelectedValue = ""
        chkActive.Visible = False
        Label1.Text = ""

        BL.Bind_DDL_ExpenseTemplate(lbltemplate)
        BL.Bind_DDL_Component(ddlComponent)
        BL.Bind_DDL_Inkind(ddlInkind)

        '#TabActExpense
        txtActGantCommitment.Text = ""
        'txtActGantDisbursement.Text = ""
        'txtActGantPaymentDate.Text = ""
        'txtActLoanCommitment.Text = ""
        'txtActLoanDisbursement.Text = ""
        'txtActLoanPaymentDate.Text = ""
        'txtActAdministrative.Text = ""


        Dim dtDisbursement As New DataTable
        With dtDisbursement
            .Columns.Add("id")
            .Columns.Add("Type_ID")
            .Columns.Add("Country_ID")
            .Columns.Add("Payment_Date")
            .Columns.Add("Amount")
        End With
        UCDisbursementLoan.DisbursementDT = dtDisbursement

        Dim dtCountryRe As New DataTable
        With dtCountryRe
            .Columns.Add("id")
            .Columns.Add("country_node_id")
            .Columns.Add("amout_person")
        End With
        UCProjectCountryRe.ComponentDT = dtCountryRe

        Dim dtCountry As New DataTable
        With dtCountry
            .Columns.Add("id")
            .Columns.Add("country_node_id")
        End With
        UCProjectCountry.CountryDT = dtCountry

        Dim dtRecipient As New DataTable
        With dtRecipient
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("recipient_id")
        End With
        UCprojectRecipience.RecipienceDT = dtRecipient
        UCprojectRecipience.Bindata_toRptCountRe()

        BindList(0)

        'CurrentTabAct = Tab.ActActDetail
    End Sub

#Region "Tab Activity"
    Protected Property CurrentTabAct As Tab
        Get
            Select Case True
                Case TabActActDetail.Visible
                    Return Tab.ActActDetail
                Case TabActExpense.Visible
                    Return Tab.ActExpense
                    'Case TabActRecipent.Visible
                    '    Return TAB.ActReceipent

            End Select
        End Get
        Set(value As Tab)
            TabActActDetail.Visible = False
            TabActExpense.Visible = False
            'TabActRecipent.Visible = False

            liTabActActDetail.Attributes("class") = ""
            liTabActExpense.Attributes("class") = ""
            'liTabActReceipent.Attributes("class") = ""

            Select Case value
                Case Tab.ActActDetail
                    TabActActDetail.Visible = True
                    liTabActActDetail.Attributes("class") = "active"
                Case Tab.ActExpense
                    TabActExpense.Visible = True
                    liTabActExpense.Attributes("class") = "active"
                    'Case TAB.ActReceipent
                    '    TabActRecipent.Visible = True
                    '    liTabActReceipent.Attributes("class") = "active"
                Case Else
            End Select
        End Set

    End Property

    Private Sub ChangeTabAct(sender As Object, e As System.EventArgs) Handles btnTabActActDetail.Click, btnTabActExpense.Click ', btnTabActReceipent.Click
        Select Case True
            Case Equals(sender, btnTabActActDetail)
                CurrentTabAct = Tab.ActActDetail
            Case Equals(sender, btnTabActExpense)
                CurrentTabAct = Tab.ActExpense
                'Case Equals(sender, btnTabActReceipent)
                '    CurrentTabAct = TAB.ActReceipent
            Case Else
        End Select
    End Sub

    Protected Enum Tab
        ActActDetail = 1
        ActExpense = 2
        ActReceipent = 3
    End Enum
#End Region

#Region "Event Activity "
    Private Function ValidateAct() As Boolean
        Dim ret As Boolean = True

        If ddlSelectDisbursement.SelectedValue = "" Then
            alertmsg("กรุณาระบุประเภทเงิน")
            ret = False
        End If
        If txtActualActStart.Text = "" Then
            alertmsg("กรุณาระบุวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtActualActEnd.Text = "" Then
            alertmsg("กรุณาระบุแผนวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุสาขา(sector)")
            ret = False
        End If

        If ddlSubSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุสาขาย่อย(sector type)")
            ret = False
        End If

        Try
            Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("วันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        Dim DTDisbursement As DataTable = UCDisbursementLoan.DisbursementDT
        For i As Integer = 0 To DTDisbursement.Rows.Count - 1
            Dim type_id As String = DTDisbursement.Rows(i)("type_id").ToString
            Dim country_id As String = DTDisbursement.Rows(i)("Country_ID").ToString
            Dim payment_date As String = DTDisbursement.Rows(i)("payment_date").ToString
            Dim amount As String = DTDisbursement.Rows(i)("Amount").ToString

            If type_id = "" Or country_id = "" Or payment_date = "" Or amount = "" Then
                alertmsg("กรุณาระบุ Disbursement (รายจ่าย) ให้ถูกต้อง")
                ret = False
            End If

        Next

        'Country re ประเทศผู้รับทุน
        Dim DTProjectCountryRe As DataTable = UCProjectCountryRe.ComponentDT
        For i As Integer = 0 To DTProjectCountryRe.Rows.Count - 1
            Dim id As String = DTProjectCountryRe.Rows(i)("id").ToString
            Dim country_node_id As String = DTProjectCountryRe.Rows(i)("country_node_id").ToString
            Dim amout_person As String = DTProjectCountryRe.Rows(i)("amout_person").ToString

            If country_node_id = "" Then
                alertmsg("กรุณาระบุประเทศ ")
                ret = False
            End If

            If amout_person = 0 Then
                alertmsg("กรุณาระบุจำนวนคน ")
                ret = False
            End If

            Dim tempdr() As DataRow
            tempdr = DTProjectCountryRe.Select("country_node_id='" & country_node_id & "' and id <> '" & id & "'")
            If tempdr.Length > 0 Then
                alertmsg("รายชื่อประเทศรับทุนซ้ำ ")
                ret = False
            End If
        Next

        'ผู้รับทุน Recipient
        Dim DTProjectRecipient As DataTable = UCprojectRecipience.RecipienceDT
        For i As Integer = 0 To DTProjectRecipient.Rows.Count - 1
            Dim id As String = DTProjectRecipient.Rows(i)("id").ToString
            Dim recipient_id As String = DTProjectRecipient.Rows(i)("recipient_id").ToString

            If recipient_id = "" Then
                alertmsg("กรุณาระบุผู้รับทุน ")
                ret = False
            End If

            Dim tempdr() As DataRow
            tempdr = DTProjectRecipient.Select("recipient_id ='" & recipient_id & "' and id <> '" & id & "'")
            If tempdr.Length > 0 Then
                alertmsg("รายชื่อผู้รับทุนซ้ำ ")
                ret = False
            End If
        Next

        Return ret
    End Function



    Private Sub btnSaveAct_Click(sender As Object, e As EventArgs) Handles btnSaveAct.Click
        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            '##tabActivity
            '#TabActActDetail
            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActivityName.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                If ddlSelectDisbursement.SelectedValue <> "" Then
                    .DISBURSEMENT_TYPE = ddlSelectDisbursement.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                .ACTIVE_STATUS = "1"

                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If

            End With

            With lnqActivity

                If txtActGantCommitment.Text <> "" Then
                    .COMMITMENT_BUDGET = CDbl(txtActGantCommitment.Text)
                Else
                    .COMMITMENT_BUDGET = 0
                End If
            End With

            Dim DTDisbursement As DataTable = UCDisbursementLoan.DisbursementDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT
            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If
            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If
            Dim ret As New ProcessReturnInfo
            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                ret = New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, DTComponent, Nothing, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                ret = New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, Nothing, DTInkind, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = ret.ID
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try
    End Sub



    Private Sub btnCancelAct_Click(sender As Object, e As EventArgs) Handles btnCancelAct.Click
        pnlAdd.Visible = False

        RaiseEvent CancelAct()

    End Sub

    Private Sub ddlSector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSector.SelectedIndexChanged
        BL.Bind_DDL_Perpose(ddlSubSector, ddlSector.SelectedValue)
    End Sub

    Protected Sub hdnRadioButtonSelector_Click(sender As Object, e As EventArgs) Handles hdnRadioButtonSelector.Click

        '----ต้องเคลีบร์ข้อมูลการเบิกจ่ายก่อน  เนื่องจากมีการเปลี่ยนกลุ่มของผู้รับทุน

        '--เคลียร์ Expense--
        '--เคลียร์ ผู้รับทุน
        BL.DROP_EXPENSE_ACTIVITY(lblActivityID.Text)
        Old_Recipient_Type = RadioSelectRe.SelectedValue

        BindList(lblActivityID.Text)
        SetGroupUserControl()
    End Sub

    Private Sub RadioSelectRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioSelectRe.SelectedIndexChanged

        '---เชคเงื่อนไข ถ้า ข้อมีข้อมูลการเบิกจ่ายแล้ว ให้แจ้งเตือนเมื่อเปลี่ยน
        Dim DT As New DataTable
        DT = BL.GetList_Expense2(lblActivityID.Text)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "confirm", "confirmSelection();", True)

        Else
            BL.DROP_EXPENSE_ACTIVITY(lblActivityID.Text)
            BindList(lblActivityID.Text)
            SetGroupUserControl()
        End If
    End Sub

    Private Sub SetGroupUserControl()
        UCProjectCountryRe.SetDataRpt(lblActivityID.Text)
        UCprojectRecipience.SetDataRpt(lblActivityID.Text)
        UCProjectCountry.SetDataRpt(lblActivityID.Text)

        If RadioSelectRe.SelectedValue = "1" Then
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "2" Then
            UCprojectRecipience.Visible = True
            UCProjectCountryRe.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "3" Then
            UCProjectCountry.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountryRe.Visible = False
        End If

    End Sub


    Protected Sub lblGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim value As String = CType(sender, DropDownList).SelectedValue
        If value = 1 Then
            ddlComponent.Visible = True
            ddlInkind.Visible = False
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        ElseIf value = 2 Then
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            chkActive.Checked = False
            Label1.Text = ""
            traid.Visible = True
        ElseIf value = 0 Then
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            Label1.Text = ""
            traid.Visible = False
        End If
    End Sub
#End Region

    Sub Authorize()
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)


        Next

    End Sub

    Private Sub BindList(activity_id As Long)

        Dim DT As DataTable = BL.GetList_Expense2(activity_id)
        rptList.DataSource = DT
        rptList.DataBind()

        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            Dim SumSum As Decimal = lblBudget_Sum.Text - lblDisbursement_Sum.Text
            lblpayTotalSum.Text = SumSum.ToString("#,##0.00")

            lbltemplate.Enabled = False
            'Dim A As Long = txtActGantCommitment.Text - lblBudget_Sum.Text
            'txtbutged.Text = A.ToString("#,##0.00")
            pnlFooter.Visible = True


            '---Compare---
            If Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")) > 0 Then
                If txtActGantCommitment.Text <> lblBudget_Sum.Text Then
                    lblError_compare.Text = "ตรวจสอบข้อมูลงบประมาณเงินกู้ (Loan Budget) กับ งบประมาณ (Budget) ข้อมูลค่าใช้จ่าย ให้ถูกต้อง"
                    tr_Error_compare.Visible = True
                Else
                    tr_Error_compare.Visible = False
                End If
            Else
                tr_Error_compare.Visible = False
            End If
        Else
            tr_Error_compare.Visible = False
            lblError_compare.Text = ""

            lbltemplate.Enabled = True
            txtbutged.Text = txtActGantCommitment.Text
            pnlFooter.Visible = False
        End If
        txtbutged.Enabled = False
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lbldatePlan As Label = DirectCast(e.Item.FindControl("lbldatePlan"), Label)
        Dim lbldateActual As Label = DirectCast(e.Item.FindControl("lbldateActual"), Label)
        Dim lblpayActual As Label = DirectCast(e.Item.FindControl("lblpayActual"), Label)
        Dim lblpayPlan As Label = DirectCast(e.Item.FindControl("lblpayPlan"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblpayTotal As Label = DirectCast(e.Item.FindControl("lblpayTotal"), Label)
        Dim tdPayActual As HtmlTableCell = e.Item.FindControl("tdPayActual")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")

        lblID.Text = e.Item.DataItem("id").ToString
        lbldatePlan.Text = e.Item.DataItem("Payment_Date_Start").ToString
        lbldateActual.Text = e.Item.DataItem("Payment_Date_End").ToString
        lblpayActual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        lblpayPlan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        Dim Sum As Decimal = lblpayPlan.Text - lblpayActual.Text

        If Sum < 0 Then
            tdPayActual.Attributes.Add("style", "background-color:red; text-align:right;")

            td2.Attributes.Add("style", "background-color:red; text-align:right;")
        End If

        lblpayTotal.Text = Sum.ToString("#,##0.00")

    End Sub


    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "cmdEdit" Then
            '--Save Header-- 
            Validate_Recipient()
            If lblActivityID.Text <> "0" Then
                Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & "&mode=" & "edit" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)
            End If
        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.DeleteExpenseHeaderDetail(e.CommandArgument)
            If ret.IsSuccess Then
                alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
                BindList(lblActivityID.Text)
            Else
                alertmsg(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdView" Then
            '--Save Header-- 
            Validate_Recipient()
            If lblActivityID.Text <> "0" Then
                Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & "&mode=" & "view" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)
            End If
        End If
    End Sub

    Private Sub AddExpense_Click(sender As Object, e As EventArgs) Handles btnApply.Click

        'Validate_Recipient()
        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            '##tabActivity
            '#TabActActDetail
            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActivityName.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                If ddlSelectDisbursement.SelectedValue <> "" Then
                    .DISBURSEMENT_TYPE = ddlSelectDisbursement.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                .ACTIVE_STATUS = "1"

                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If

            End With

            With lnqActivity

                If txtActGantCommitment.Text <> "" Then
                    .COMMITMENT_BUDGET = CDbl(txtActGantCommitment.Text)
                Else
                    .COMMITMENT_BUDGET = 0
                End If
            End With

            Dim DTDisbursement As DataTable = UCDisbursementLoan.DisbursementDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT

            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim ret As New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, DTComponent, Nothing, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim ret As New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, Nothing, DTInkind, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = lblActivityID.Text
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try
        If lblActivityID.Text <> "0" Then

            'Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            'With lnqAEH
            '    .ID = 0
            '    .ACTIVITY_ID = lblActivityID.Text
            'End With

            'Dim tem_id As Long = lbltemplate.SelectedValue
            'Dim HID As Double = BL.SaveHeaderID(lnqAEH, UserName, tem_id)

            'Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & "&mode=" & "Add" & "&HeaderId=" & HID & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

            '-----Clear Dialog
            txtPaymentStartDate.Text = ""
            txtPaymentEndDate.Text = ""
            pnlAddRound.Visible = True
        End If


    End Sub

    Private Sub Validate_Recipient()
        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            '##tabActivity
            '#TabActActDetail
            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActivityName.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                If ddlSelectDisbursement.SelectedValue <> "" Then
                    .DISBURSEMENT_TYPE = ddlSelectDisbursement.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try
                End If

                .ACTIVE_STATUS = "1"

                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If

            End With

            With lnqActivity

                If txtActGantCommitment.Text <> "" Then
                    .COMMITMENT_BUDGET = CDbl(txtActGantCommitment.Text)
                Else
                    .COMMITMENT_BUDGET = 0
                End If
            End With

            Dim DTDisbursement As DataTable = UCDisbursementLoan.DisbursementDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT

            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim ret As New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, DTComponent, Nothing, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim ret As New ProcessReturnInfo
                '------------------------------------------------------------------, DTRecipientPerson, DTRecipentCountry
                ret = BL.SaveProjectActivity(lnqActivity, Nothing, Nothing, DTInkind, Nothing, Nothing, DTDisbursement, Nothing, UserName, DT_ProjectCountryRe, Nothing, Nothing, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = lblActivityID.Text
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try
    End Sub

    'Public Function GetNew_ID(ByRef Table_Name As String, ByRef Collumn_ID As String) As Integer
    '    Dim SQL As String = "SELECT IsNull(MAX(" & Collumn_ID & "),0)+1 FROM " & Table_Name & " "
    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)
    '    Return DT.Rows(0).Item(0)
    'End Function

    Private Sub lnkDialogCancel_Click(sender As Object, e As EventArgs) Handles lnkDialogCancel.Click
        pnlAddRound.Visible = False
    End Sub

    Private Sub lnkDialogSave_Click(sender As Object, e As EventArgs) Handles lnkDialogSave.Click

        '-----Save Header

        If txtPaymentStartDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่เริ่มต้น');", True)
            Exit Sub
        End If
        If txtPaymentEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่สิ้นสุด');", True)
            Exit Sub
        End If


        If lblActivityID.Text <> "0" Then

            Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            With lnqAEH
                .ID = 0
                .ACTIVITY_ID = lblActivityID.Text
                .PAYMENT_DATE_START = Converter.StringToDate(txtPaymentStartDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                .PAYMENT_DATE_END = Converter.StringToDate(txtPaymentEndDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End With

            Dim tem_id As Long = lbltemplate.SelectedValue
            Dim HID As Double = BL.SaveHeaderID(lnqAEH, UserName, tem_id)
            Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & "&mode=" & "Add" & "&HeaderId=" & HID & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

        End If




    End Sub

    Private Sub btn_ReturnFalse_Click(sender As Object, e As EventArgs) Handles btn_ReturnFalse.Click
        RadioSelectRe.SelectedValue = Old_Recipient_Type
        Old_Recipient_Type = RadioSelectRe.SelectedValue
    End Sub

End Class
