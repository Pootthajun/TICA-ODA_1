﻿Imports System.Configuration
Imports System.IO
Imports ServerLinqDB.TABLE
Imports System.Data

Public Class frmRenderFileUpload
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub
        Dim fileContent As HttpPostedFile = Request.Files(0)
        Dim FileFilter As String = Request.Form("FileFilter")
        Dim cf As LinqDB.TABLE.MsSysconfigLinqDB = ODAENG.GetSysconfig(Nothing)
        Dim TempVDOPath As String = cf.TEMP_UPLOAD_PATH & Session("UserName") & "\" '"C:\webroot\TICA-ODA\Temp\"
        Dim UploadVDOPath As String = cf.UPLOAD_PATH & Session("UserName") & "\"


        Dim tmpExt() As String = Split(fileContent.FileName.Trim, ".")
        If tmpExt.Length > 0 Then
            Dim FileExt As String = tmpExt(tmpExt.Length - 1)
            If FileFilter.ToLower.IndexOf(FileExt.ToLower) <= -1 Then
                Session.Remove("UploadTempFileName")
                Response.Write("false|ไม่รองรับไฟล์ที่เลือก")
                Exit Sub
            End If
        End If


        Dim contentsize As Integer = fileContent.ContentLength
        If contentsize > (5 * 1024 * 1024) Then
            Session.Remove("UploadTempFileName")
            Response.Write("false|ขนาดไฟล์เกิน 5 MB")
            Exit Sub
        End If

        Try
            If Directory.Exists(TempVDOPath) = False Then
                Directory.CreateDirectory(TempVDOPath)
            End If

            Dim FilePath As String = TempVDOPath & fileContent.FileName
            If File.Exists(FilePath) = True Then
                Try
                    'Session.Remove("UploadTempFileName")
                    'Response.Write("false|ไฟล์ที่เลือกซ้ำ")

                    File.Delete(FilePath)
                    'Exit Sub
                Catch ex As Exception
                End Try
            End If


            Dim dt As DataTable = BL.CheckDupplicateProjectFile(fileContent.FileName)
            If dt.Rows.Count > 0 Then
                Session.Remove("UploadTempFileName")
                Response.Write("false|ไฟล์ที่เลือกซ้ำ")
                Exit Sub
            End If


            fileContent.SaveAs(FilePath)
            If File.Exists(FilePath) = True Then
                Session("UploadTempFileName") = fileContent.FileName
                Response.Write("true|" & fileContent.FileName)
            Else
                Response.Write("false|" & fileContent.FileName)
            End If
        Catch ex As Exception
            Response.Write("false|Exception " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

End Class