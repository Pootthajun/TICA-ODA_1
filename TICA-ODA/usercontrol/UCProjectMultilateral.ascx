﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCProjectMultilateral.ascx.vb" Inherits="UCProjectMultilateral" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div class="box-body no-padding">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                <th class="data-table-ddl" style="vertical-align:middle;">Multilateral Organizations (องค์กรพหุภาคี)</th>                
                <th style="vertical-align:middle;">Delete(ลบ)</th>
            </tr>
        </thead>
       <tbody>
             <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="data-table-ddl"   style ="padding:0px;">
                            <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>   
                            <asp:DropDownList ID="ddlMultilateral" runat="server" CssClass="form-control select2" Style="width: 100%">
                            </asp:DropDownList>
                        </td>
                        <%--<td   style ="padding:0px;">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" Style="text-align: right" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                        </td>--%>
                        <td class="no-padding"     style ="width: 50px;padding:0px;">
                            <%--<center>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-bricky"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');">
                                    <i Class='fa fa-trash text-danger bin'></i>
                                </asp:LinkButton>
                            </center>--%>
                             <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"   CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
       <%-- <asp:Panel ID="pnlFooter" runat="server">
        <tfoot>
            <tr>
                <td style="background-color:darkkhaki;text-align: center ;" >
                    <b>รวม</b>
                </td>
                <td style="background-color:darkkhaki;text-align: right;">
                     <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                </td>
            </tr>

        </tfoot>
        </asp:Panel>--%>
    </table>

    <div class="row"  style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>
