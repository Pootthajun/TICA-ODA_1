﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityAdvance.ascx.vb" Inherits="UCActivityAdvance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="box-body">

    <div class="tb-fix">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr class="bg-gray">
                    <th>ประเภท</th>
                    <th>วันที่</th>
                    <th>รายการ</th>
                    <th>เลขที่เอกสาร</th>
                    <asp:Label ID="lblRecipience" runat="server" Text="" Visible="false"></asp:Label>
                    <th>จำนวนเงิน</th>
                    <th style="width: 50px">ลบ</th>
                </tr>
            </thead>
            <tbody>
                <%--รายการสำหรับยืม มี 1 ค่าที่ยืม--%>
                <tr>
                    <td data-title="ประเภท" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblType_Row" Text="ยืม" runat="server"></asp:Label>
                    </td>
                    <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblAdvance_Date" runat="server"></asp:Label>
                        <asp:Label ID="lblActivityID" runat="server" Text="0" Visible="false"></asp:Label>
                    </td>
                    <td style="padding: 0px; width: 200px;">
                        <asp:TextBox ID="txtAdvance_Detail" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td id="td" runat="server" style="padding: 0px; width: 80px">
                        <asp:TextBox ID="txtAdvance_DetailNo" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" AutoPostBack="true" MaxLength="12"></asp:TextBox>
                    </td>
                    <td style="padding: 0px">
                        <asp:Label ID="lblAdvance_Pay_Amount_Plan" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td></td>
                </tr>


                <%--รายการใช้จ่ายจากบิล/ใบเสร็จ ต้องกรอกใน Ecpense ด้วย--%>
                <asp:Repeater ID="rptList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-title="ประเภท" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                                <asp:Label ID="lblType_Row" Text="ใช้จ่าย" runat="server"></asp:Label>
                            </td>
                            <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                                <asp:Label ID="lblNo" runat="server">

                                </asp:Label><div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox CssClass="form-control m-b" ID="txtPaymentDate" runat="server" placeholder=""></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Format="dd/MM/yyyy" TargetControlID="txtPaymentDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                </div>
                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td style="padding: 0px; width: 200px;">
                                <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td id="td" runat="server" style="padding: 0px; width: 80px">
                                <asp:TextBox ID="txtDetailNo" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" AutoPostBack="true" MaxLength="12"></asp:TextBox>
                            </td>
                            <td style="padding: 0px">
                                <asp:Label ID="Pay_Amount_Plan" runat="server" Visible="false"></asp:Label>
                                <asp:TextBox ID="txtCol2" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}"></asp:TextBox>
                            </td>
                            <td data-title="Delete" id="ColDelete" runat="server" style="padding: 0px;">
                                <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>




                <%--รายการสำหรับคืน มี 1 ค่าที่คืน--%>
                <tr>
                    <td data-title="ประเภท" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblTypeCash_Back_Row" Text="เงินคืน" runat="server"></asp:Label>
                    </td>
                    <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblCash_Back_Date" runat="server"></asp:Label>
                    </td>
                    <td style="padding: 0px; width: 200px;">
                        <asp:TextBox ID="txtCash_Back_Detail" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td runat="server" style="padding: 0px; width: 80px">
                        <asp:TextBox ID="txtCash_Back_DetailNo" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" AutoPostBack="true" MaxLength="12"></asp:TextBox>
                    </td>
                    <td style="padding: 0px">
                        <asp:TextBox ID="txtCash_Back_Pay_Amount_Plan" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}"></asp:TextBox>
                    </td>
                    <td data-title="Delete" style="padding: 0px;"></td>
                </tr>


                <tr>
                    <td colspan="3">
                        <asp:Button ID="btnAdd" runat="server" Text="เพิ่ม" CssClass="btn btn-primary" />
                        <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="btn btn-success" />
                    </td>
                    <td style="text-align: right;">เงินคงเหลือ</td>
                    <td style="text-align: right;">                                
                        <asp:TextBox ID="txtSum" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px" class="cost accounting" Enabled="false"></asp:TextBox>
                    </td>
                    
                </tr>


            </tbody>

        </table>


        <div class="row" style="margin-top: 5px;">
            <center>
            <div class="col-sm-12"> 
            </div>
        </center>
        </div>
    </div>
</div>
