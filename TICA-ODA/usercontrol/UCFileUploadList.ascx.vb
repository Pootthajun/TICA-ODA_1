﻿Imports System.IO
Imports System.Data
Imports AjaxControlToolkit
Imports System.Text.RegularExpressions

Partial Class usercontrol_UCFileUploadList
    Inherits System.Web.UI.UserControl

    Public WriteOnly Property FileFilter As String
        Set(value As String)
            txtFileFilter.Text = value
        End Set
    End Property

    'Private Sub ful_UploadedComplete(sender As Object, e As AsyncFileUploadEventArgs) Handles ful.UploadedComplete
    '    If e.State = AjaxControlToolkit.AsyncFileUploadState.Success Then
    '        Dim ufFileName As String = ful.FileName

    '        Dim cf As LinqDB.TABLE.MsSysconfigLinqDB = ODAENG.GetSysconfig(Nothing)
    '        Dim TempPath As String = cf.TEMP_UPLOAD_PATH & Session("UserName") & "\"
    '        If Directory.Exists(TempPath) = False Then
    '            Directory.CreateDirectory(TempPath)
    '        End If

    '        Dim FilePath As String = TempPath & ufFileName
    '        If File.Exists(FilePath) = True Then
    '            'ถ้ามีไฟล์เดิมอยู่แล้ว ก็ลบออกก่อน
    '            File.Delete(FilePath)
    '        End If

    '        'บันทึกไฟล์ที่ทำการ Upload
    '        ful.SaveAs(FilePath)

    '        If File.Exists(FilePath) = True Then
    '            Session("UploadTempFileName") = ufFileName
    '        End If
    '    End If

    'End Sub

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnUpload.Enabled = Not IsEnable
        ful.Enabled = Not IsEnable
        For i As Integer = 0 To rptFileList.Items.Count - 1
            Dim ColDelete As HtmlTableCell = rptFileList.Items(i).FindControl("ColDelete")
            Dim lblFileName As Label = rptFileList.Items(i).FindControl("lblFileName")
            If IsEnable = True Then
                ColDelete.Style.Add("display", "none")
                lblFileName.Text = Regex.Replace(lblFileName.Text, “<.*?>|&.*?;”, String.Empty)
            End If
        Next
    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If Session("UploadTempFileName") Is Nothing Then
            Alert("กรุณาเลือกไฟล์")
            Exit Sub
        End If

        If Session("UploadTempFileName").ToString.Trim = "" Then
            Alert("กรุณาเลือกไฟล์")
            Exit Sub
        End If

        Dim FileName As String = Session("UploadTempFileName")

        Dim cf As LinqDB.TABLE.MsSysconfigLinqDB = ODAENG.GetSysconfig(Nothing)
        Dim TempPath As String = cf.TEMP_UPLOAD_PATH & Session("UserName") & "\"

        Dim FilePath As String = TempPath & FileName.Trim
        If File.Exists(FilePath) = True Then
            Dim DT As DataTable = CurrentFileList()

            DT.DefaultView.RowFilter = "original_file_name='" & FileName & "'"
            If DT.DefaultView.Count > 0 Then
                Alert("ไฟล์ที่เลือกซ้ำ")
                Exit Sub
            End If
            DT.DefaultView.RowFilter = ""

            Dim dr As DataRow = DT.NewRow
            dr("file_path") = FilePath
            dr("original_file_name") = FileName
            dr("file_id") = 0
            DT.Rows.Add(dr)

            rptFileList.DataSource = DT
            rptFileList.DataBind()
            Session.Remove("UploadTempFileName")
        End If
    End Sub

    Protected Function CurrentFileList() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("file_path")
        DT.Columns.Add("original_file_name")
        DT.Columns.Add("file_id")

        For Each Item As RepeaterItem In rptFileList.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim DR As DataRow = DT.NewRow
            Dim lblFileName As Label = Item.FindControl("lblFileName")
            Dim lblFilePath As Label = Item.FindControl("lblFilePath")
            Dim lblFileID As Label = Item.FindControl("lblFileID")

            DT.Rows.Add(DR)
            If lblFileName.Text.Trim <> "" Then
                DR("file_path") = lblFilePath.Text    'Fullname
                DR("original_file_name") = lblFileName.Text   'File name only
                DR("file_id") = lblFileID.Text
            End If

            DT.AcceptChanges()
        Next

        Return DT
    End Function


    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub rptFileList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptFileList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblFileName As Label = e.Item.FindControl("lblFileName")
        Dim lblFilePath As Label = e.Item.FindControl("lblFilePath")
        Dim lblFileID As Label = e.Item.FindControl("lblFileID")
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim ColDelete As HtmlTableCell = e.Item.FindControl("ColDelete")

        lblNo.Text = e.Item.ItemIndex + 1
        If Not IsDBNull(e.Item.DataItem("file_path")) Then
            lblFilePath.Text = e.Item.DataItem("file_path").ToString
        End If

        lblFileID.Text = e.Item.DataItem("file_id").ToString
        If Not IsDBNull(e.Item.DataItem("original_file_name")) Then
            lblFileName.Text = e.Item.DataItem("original_file_name").ToString
            Dim fInfo As New FileInfo(lblFilePath.Text)

            If lblFileID.Text <> "0" Then
                lblFileName.Text = "<a href='" & ODAENG.GetSysconfig(Nothing).DOWNLOAD_URL & fInfo.Name & "' target='_blank'>"
                lblFileName.Text += e.Item.DataItem("original_file_name").ToString & "</a>"
            End If

        End If

    End Sub

    Public Function GetFileList() As DataTable
        Return CurrentFileList()
    End Function

    Public Sub SetFileList(DT As DataTable)
        If DT.Rows.Count > 0 Then
            rptFileList.DataSource = DT
            rptFileList.DataBind()
        End If
    End Sub

    Public Sub ClearTempFolder(UserName As String)
        Dim cf As LinqDB.TABLE.MsSysconfigLinqDB = ODAENG.GetSysconfig(Nothing)
        Dim TempPath As String = cf.TEMP_UPLOAD_PATH & UserName & "\"
        If Directory.Exists(TempPath) = True Then
            Directory.Delete(TempPath, True)
        End If
    End Sub

    Private Sub rptFileList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptFileList.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim DT As DataTable = CurrentFileList()
                Try
                    Dim ret As New ProcessReturnInfo
                    Dim FileID As Long = Convert.ToInt64(DT.Rows(e.Item.ItemIndex)("file_id"))
                    If FileID > 0 Then
                        Dim oda As New ODAENG
                        ret = oda.DeleteProjectFile(FileID)
                    Else
                        ret.IsSuccess = True
                    End If

                    If ret.IsSuccess = True Then
                        If File.Exists(DT.Rows(e.Item.ItemIndex)("file_path")) = True Then
                            Try
                                File.SetAttributes(DT.Rows(e.Item.ItemIndex)("file_path"), FileAttributes.Normal)
                                File.Delete(DT.Rows(e.Item.ItemIndex)("file_path"))
                            Catch ex As Exception

                            End Try
                        End If

                        DT.Rows(e.Item.ItemIndex).Delete()
                        DT.AcceptChanges()
                    Else
                        Alert(ret.ErrorMessage)
                    End If
                Catch : End Try
                rptFileList.DataSource = DT
                rptFileList.DataBind()
        End Select
    End Sub



End Class
