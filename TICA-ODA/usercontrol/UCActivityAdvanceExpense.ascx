﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCActivityAdvanceExpense.ascx.vb" Inherits="UCActivityAdvanceExpense" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<div class="box-body">
    <asp:Panel ID="pnlAdvance" runat ="server" >
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr id="trDue_Date" runat="server"  class="bg-gray" style="background-color: #1766a9 !important; color: #fdfdfd;">
                <th style="vertical-align :middle ;"><span >วันครบกำหนด</span><span style="color: red;vertical-align :middle ;">*</span></th>
                <th  style ="text-align :left;">
                    <asp:TextBox CssClass="form-control m-b" ID="txtDue_Date" Style="display: unset; width: unset;" runat="server" placeholder=""></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender9" runat="server"
                        Format="dd/MM/yyyy" TargetControlID="txtDue_Date" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>

                </th>
                <th colspan ="4" style="vertical-align :middle ;"><span >รายละเอียด</span></th>
            </tr>



            <tr class="bg-gray" style="background-color: #1766a9 !important; color: #fdfdfd;">
                <th>ประเภท</th>
                <th>วันที่</th>
                <th>วัตถุประสงค์</th>
                <th>เลขที่เอกสาร</th>
                <th>Amount (จำนวนเงิน)</th>
                <th style="width: 50px">Delete(ลบ)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="ประเภท" style="background-color: yellowgreen;">
                    <span style="font-size: 14px;"><b>เงินยืมทดรอง</b></span>
                </td>
                <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                    <asp:Label ID="lblRow" runat="server">

                    </asp:Label><div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox CssClass="form-control m-b" ID="txtDate_Borrow" runat="server" placeholder=""></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                            Format="dd/MM/yyyy" TargetControlID="txtDate_Borrow" PopupPosition="Right"></ajaxToolkit:CalendarExtender>
                    </div>

                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                </td>
                <td style="padding: 0px;">
                    <asp:TextBox ID="txtDetail_Borrow" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td id="td1" runat="server" style="padding: 0px; width: 150px">
                    <asp:TextBox ID="txtDetailNo_Borrow" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" MaxLength="12"></asp:TextBox>
                </td>
                <td data-title="Budget (บาท)" id="td" runat="server" style="padding: 0px; width: 200px;">
                    <asp:TextBox ID="txtAmount_Borrow" runat="server" CssClass="form-control" Style="text-align: right;" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                </td>
                <td data-title="Delete" id="ColDelete" runat="server" style="padding: 0px;">
                    <%--<center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>--%>
                </td>
            </tr>



            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>

                    <tr>
                        <td data-title="ประเภท" style="background-color: lightsteelblue;">
                            <span style="font-size: 14px;"><b>( ใช้จ่าย )</b></span>
                        </td>

                        <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                            <asp:Label ID="lblRow_Expense" runat="server">

                            </asp:Label><div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox CssClass="form-control m-b" ID="txtDate_Expense" runat="server" placeholder=""></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2_Expense" runat="server"
                                    Format="dd/MM/yyyy" TargetControlID="txtDate_Expense" PopupPosition="Right"></ajaxToolkit:CalendarExtender>
                            </div>

                            <asp:Label ID="lblID_Expense" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td style="padding: 0px;">
                            <asp:TextBox ID="txtDetail_Expense" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td id="tdDetail_Expense" runat="server" style="padding: 0px; width: 150px;">
                            <asp:TextBox ID="txtDetailNo_Expense" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" MaxLength="12"></asp:TextBox>
                        </td>
                        <td data-title="Budget (บาท)" id="tdAmount_Expense" runat="server" style="padding: 0px; width: 200px;">
                            <asp:TextBox ID="txtAmount_Expense" runat="server" CssClass="form-control" Style="text-align: right;" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                        </td>
                        <td data-title="Delete" id="ColDelete_Expense" runat="server" style="padding: 0px;">
                            <center>
                                <asp:Button ID="btnDelete_Expense" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandName="Delete"  OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>



                </ItemTemplate>
            </asp:Repeater>


            <tr>
                <td data-title="ประเภท" style="background-color: tan;">
                    <span style="font-size: 14px;"><b>( เงินคืน )</b></span>
                </td>

                <td data-title="วันที่" style="width: 150px; padding: 0px; vertical-align: middle; text-align: center;">
                    <asp:Label ID="lblRow_Return" runat="server">

                    </asp:Label><div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <asp:TextBox CssClass="form-control m-b" ID="txtDate_Return" runat="server" placeholder=""></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2_Return" runat="server"
                            Format="dd/MM/yyyy" TargetControlID="txtDate_Return" PopupPosition="Right"></ajaxToolkit:CalendarExtender>
                    </div>

                    <asp:Label ID="lblID_Return" runat="server" Visible="false"></asp:Label>
                </td>
                <td style="padding: 0px;">
                    <asp:TextBox ID="txtDetail_Return" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
                <td id="tdDetail_Return" runat="server" style="padding: 0px; width: 150px">
                    <asp:TextBox ID="txtDetailNo_Return" runat="server" CssClass="form-control" placeholder="" Style="text-align: center" MaxLength="12"></asp:TextBox>
                </td>
                <td data-title="Budget (บาท)" id="tdAmount_Return" runat="server" style="padding: 0px; width: 200px;">
                    <asp:TextBox ID="txtAmount_Return" runat="server" CssClass="form-control" Style="text-align: right;" placeholder="" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" MaxLength="12"></asp:TextBox>
                </td>
                <td data-title="Delete" id="ColDelete_Return" runat="server" style="padding: 0px;">
                    <%--<center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky"  width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>--%>
                </td>
            </tr>

        </tbody>
        <asp:Panel ID="pnlFooter" runat="server">
            <tfoot>
                <tr>
                    <td colspan="4" style="text-align: right;">
                        <b>เงินคงเหลือ</b>
                    </td>
                    <td style="text-align: right;">
                        <b>
                            <asp:Label ID="lblTotal" runat="server" Text="0" ToolTip="เงินยืมทดรอง – (ใช้จ่าย + เงินคืน)"></asp:Label></b>
                    </td>
                </tr>

            </tfoot>
        </asp:Panel>
    </table>
    </asp:Panel>

    <div class="row" style="margin-top: 5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd"  runat="server" Text="เพิ่มรายการใช้จ่าย"
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>



</div>

<div id="pnlFooter_btnSave" runat="server" class="modal-footer" style="bottom: 0px; text-align: right; width: 100%;">
    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-save"></i> Save
    </asp:LinkButton>
    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google">
                <i class="fa fa-reply"></i> Cancel
    </asp:LinkButton>
</div>


