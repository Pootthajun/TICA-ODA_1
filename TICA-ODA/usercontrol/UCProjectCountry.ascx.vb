﻿Imports System.Data
Partial Class usercontrol_UCProjectCountry
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG


    Dim _CountryDT As DataTable
    Public Property CountryDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _CountryDT = value

            rptList.DataSource = _CountryDT
            rptList.DataBind()

        End Set
    End Property

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("country_node_id")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow
            dr("id") = lblID.Text
            dr("country_node_id") = ddlCountry.SelectedValue

            dt.Rows.Add(dr)

        Next
        Return dt
    End Function

    Function SetDataRpt(Activity_id As String) As DataTable
        Dim dt As New DataTable
        dt = BL.GetDataActivityCountryRecipent_FromQurey(Activity_id)

        rptList.DataSource = dt
        rptList.DataBind()

        lblActivityID.Text = Activity_id

        Return dt
    End Function

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        'Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)

        BL.Bind_DDL_Country(ddlCountry)

        lblID.Text = e.Item.DataItem("id").ToString
        ddlCountry.SelectedValue = e.Item.DataItem("country_node_id").ToString

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = GetDataCountryRe_FromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("country_node_id") = 0
        dt.Rows.Add(dr)

        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataCountryRe_FromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("country_node_id")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            dr = dt.NewRow

            dr("id") = lblID.Text
            dr("country_node_id") = ddlCountry.SelectedValue

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "Delete" Then

            Dim dt As DataTable = GetDataFromRpt(rptList)
            dt.Rows.RemoveAt(e.Item.ItemIndex)


            rptList.DataSource = dt
            rptList.DataBind()
        End If
    End Sub
End Class
