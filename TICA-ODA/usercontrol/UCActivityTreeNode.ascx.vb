﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Public Class UCActivityTreeNode
    Inherits System.Web.UI.UserControl

    Public Event ImageModeClick(sender As UCActivityTreeNode, IsExpand As Boolean)
    Public Event AddSubActivity(sender As UCActivityTreeNode)
    Public Event EditActivity(sender As UCActivityTreeNode)
    Public Event DeleteActivity(ActivityID As Long)
    Public Event ViewActivity(sender As UCActivityTreeNode)

    Dim BL As New ODAENG



    Dim urlExpand As String = "../dist/img/ExpandMore.png"   'แตกโหนด
    Dim urlCollapsed As String = "../dist/img/ChevronRight.png"
    Dim urlNoChild As String = "../dist/img/ExpandNull.png"


    Public Property ActivityID As String
        Get
            Return lblActivityID.Text.Trim
        End Get
        Set(value As String)
            lblActivityID.Text = value
        End Set
    End Property

    Public Property New_Activity As Integer
        Get
            Return ViewState("New_ActivityID")
        End Get
        Set(value As Integer)
            ViewState("New_ActivityID") = value
        End Set
    End Property

    Public Property ActivityName As String
        Get
            Return lblActivityName.Text.Trim
        End Get
        Set(value As String)
            lblActivityName.Text = value
        End Set
    End Property

    Public Property Disbursetment_Type As String
        Get
            Return lblDisbursetment_Type.Text.Trim
        End Get
        Set(value As String)
            lblDisbursetment_Type.Text = value
        End Set
    End Property

    Public Property ParentID As String
        Get
            Return lblParentID.Text
        End Get
        Set(value As String)
            lblParentID.Text = value
        End Set
    End Property
    Public Property Duration As Integer
        Get
            Return CInt(lblDuration.Text)
        End Get
        Set(value As Integer)
            lblDuration.Text = value
        End Set
    End Property

    Public Property Project_ID As Integer
        Get
            Return CInt(lblDuration.Attributes("Project_ID"))
        End Get
        Set(value As Integer)
            lblDuration.Attributes("Project_ID") = value
        End Set
    End Property

    Public Property Year_MIN As Integer
        Get
            Return CInt(lblDuration.Attributes("Year_MIN"))
        End Get
        Set(value As Integer)
            lblDuration.Attributes("Year_MIN") = value
        End Set
    End Property

    Public Property Year_MAX As Integer
        Get
            Return CInt(lblDuration.Attributes("Year_MAX"))
        End Get
        Set(value As Integer)
            lblDuration.Attributes("Year_MAX") = value
        End Set
    End Property

    Public Property ImageMode As System.Web.UI.WebControls.Image
        Get
            Return imgMode
        End Get
        Set(value As System.Web.UI.WebControls.Image)
            imgMode = value
        End Set
    End Property

    Public Property ImageIcon As System.Web.UI.WebControls.Image
        Get
            Return imgIcon
        End Get
        Set(value As System.Web.UI.WebControls.Image)
            imgIcon = value
        End Set
    End Property
    Public ReadOnly Property ImageCollapsed As String
        Get
            Return urlCollapsed
        End Get
    End Property


    Public Property IsExpand As Boolean
        Get
            Return lblExpand.Text
        End Get
        Set(value As Boolean)
            lblExpand.Text = value
        End Set
    End Property
    Public Property Is_Folder As String
        Get
            Return iconTypeRow.InnerHtml
        End Get
        Set(value As String)
            iconTypeRow.InnerHtml = value

            '--เชคสำหรับ เปิด ปิด ปุ่ม Action--
            If value = "<i class='fa fa-folder text-info'></i>" Then
                lblExpense.Style("color") = "teal"
                lblCommitmentBudget.Style("color") = "teal"
                lblExpense.Style("font-weight") = "bold"
                lblCommitmentBudget.Style("font-weight") = "bold"



                likAddActivity.Visible = True
                liAddFolder.Visible = True
                btnAddAct.Visible = True
                '--
                likEditActivity.Visible = True
                likViewActivity.Visible = False
                likCopyActivity.Visible = False
                likDeleteActivity.Visible = True

            Else
                likAddActivity.Visible = False
                liAddFolder.Visible = False
                btnAddAct.Visible = False
                '--
                likEditActivity.Visible = True
                likCopyActivity.Visible = True
                likDeleteActivity.Visible = True

            End If


        End Set
    End Property

    Public Property PlanStart As String
        Get
            Return Converter.StringToDate(lblPlanStart.Text, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End Get
        Set(value As String)
            lblPlanStart.Text = value
        End Set
    End Property

    Public Property PlanEnd As String
        Get
            Return Converter.StringToDate(lblPlanEnd.Text, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End Get
        Set(value As String)
            lblPlanEnd.Text = value
        End Set
    End Property

    Public Property CommitmentBudget As Decimal
        Get
            Return CDec(lblCommitmentBudget.Text)
        End Get
        Set(value As Decimal)
            lblCommitmentBudget.Text = value.ToString("##,0##.00")
        End Set
    End Property

    Public Property Expense As Decimal
        Get
            Return CDec(lblExpense.Text)
        End Get
        Set(value As Decimal)
            lblExpense.Text = value.ToString("##,0##.00")
        End Set
    End Property
    Public Property ChildQty As Integer
        Get
            Return lblChildQty.Text
        End Get
        Set(value As Integer)
            lblChildQty.Text = value

            If value > 0 Then
                imgMode.ImageUrl = urlCollapsed
                imgMode.Style.Add(HtmlTextWriterStyle.Cursor, "cursor")
            Else
                imgMode.Style.Add(HtmlTextWriterStyle.Cursor, "default")
            End If
        End Set
    End Property

    Public Sub SetBudgetColor(color As System.Drawing.Color)
        lblCommitmentBudget.ForeColor = color
    End Sub

    Public Sub SetToolTipCommitmentBudget(strtooltip As String)
        lblCommitmentBudget.ToolTip = strtooltip
    End Sub

    Public Property NodeLevel As Integer
        Get
            Return tdCellIndent.Attributes("Level")
        End Get
        Set(value As Integer)
            tdCellIndent.Attributes("Level") = value
            tdCellIndent.Style("padding-left") = (value * IndextLevelPixel) & "px"
            tdCellIndent.Style("padding-top") = "5px"
            tdCellIndent.Style("padding-bottom") = "5px"
        End Set
    End Property

    Public Property IndextLevelPixel As Integer
        Get
            Return tdCellIndent.Attributes("IndextLevelPixel")
        End Get
        Set(value As Integer)
            tdCellIndent.Attributes("IndextLevelPixel") = value
        End Set
    End Property

    Public Property NodeRepeaterItemIndex As Integer
        Get
            Return lblNodeRepeaterItemIndex.Text
        End Get
        Set(value As Integer)
            lblNodeRepeaterItemIndex.Text = value
        End Set
    End Property

    Public WriteOnly Property SetViewMode As Boolean
        Set(value As Boolean)
            likEditActivity.Visible = Not value
            likViewActivity.Visible = value
            likCopyActivity.Visible = Not value
            'tdAddButton.Visible = Not value
            'tdDeleteButton.Visible = Not value

            likAddActivity.Visible = Not value
            likDeleteActivity.Visible = Not value

        End Set
    End Property

    Private Sub imgMode_Click(sender As Object, e As ImageClickEventArgs) Handles imgMode.Click
        If imgMode.ImageUrl <> urlNoChild Then
            If lblExpand.Text.ToLower = "true" Then
                lblExpand.Text = "false"
                imgMode.ImageUrl = urlCollapsed
            Else
                lblExpand.Text = "true"
                imgMode.ImageUrl = urlExpand
            End If
            RaiseEvent ImageModeClick(Me, lblExpand.Text)
            imgMode.Enabled = True
        Else
            imgMode.Enabled = False
        End If
    End Sub



    Private Sub likEditActivity_Click(sender As Object, e As EventArgs) Handles likEditActivity.Click
        Dim Sql As String = " select ISNULL(Is_Folder,0) Is_Folder ,activity_name from TB_Activity where id=" & ActivityID
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows(0).Item("Is_Folder") Then
            pnlFolder.Visible = True
            txtFolder_Name.Text = DT.Rows(0).Item("activity_name")
        Else
            RaiseEvent EditActivity(Me)
        End If


    End Sub

    Private Sub likDeleteActivity_Click(sender As Object, e As EventArgs) Handles likDeleteActivity.Click
        RaiseEvent DeleteActivity(lblActivityID.Text)
    End Sub

    Private Sub likViewActivity_Click(sender As Object, e As EventArgs) Handles likViewActivity.Click
        RaiseEvent ViewActivity(Me)
    End Sub

    Private Sub likCopyActivity_Click(sender As Object, e As EventArgs) Handles likCopyActivity.Click
        Session("ActivityID_COPY") = lblActivityID.Text

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('คัดลอกกิจกรรม : " & lblActivityName.Text & "');", True)
    End Sub

    Public WriteOnly Property Set_btnCopy As Boolean
        Set(value As Boolean)
            likCopyActivity.Visible = value
        End Set
    End Property


    '------Get Min Max Rank Project Activity------
    'Public Sub Get_Rank_Activity()
    '    Dim DT As New DataTable

    '    Dim SQL As String = ""
    '    SQL &= " Select  DISTINCT * FROM( "
    '    SQL &= " Select  year(  [actual_start]) Rank_YEAR "
    '    SQL &= " From [TB_Activity] "
    '    SQL &= " UNION ALL "
    '    SQL &= "   Select  Year([actual_end])     Rank_YEAR  "
    '    SQL &= " From [TB_Activity] "
    '    SQL &= " ) As TB "
    '    SQL &= "  ORDER BY Rank_YEAR "

    '    Dim DT_Year As New DataTable
    '    Dim DR As DataRow
    '    DT = SqlDB.ExecuteTable(SQL)
    '    If (DT.Rows.Count > 0) Then
    '        If (DT.Rows.Count = 1) Then
    '            DR = DT_Year.NewRow
    '            DR("Year") = DT.Rows(0).Item("Year")
    '            DT_Year.Rows.Add(DR)
    '        Else
    '            For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
    '                DR = DT_Year.NewRow
    '                DR("Year") = i
    '                DT_Year.Rows.Add(DR)
    '            Next
    '        End If
    '    End If


    'End Sub


    Function Get_Col_Year() As DataTable
        Dim DT As New DataTable

        Dim SQL As String = ""
        SQL &= " Select  DISTINCT * FROM( "
        SQL &= " Select  year(  [actual_start]) Year "
        SQL &= " From [TB_Activity]  where project_id=" & Project_ID & " "
        SQL &= " UNION ALL "
        SQL &= "   Select  Year([actual_end])     Year  "
        SQL &= " From [TB_Activity]  where project_id=" & Project_ID & " "
        SQL &= " ) As TB "
        SQL &= "  ORDER BY Year "

        Dim DT_Year As New DataTable
        Dim DR As DataRow
        DT_Year.Columns.Add("Year")
        DT = SqlDB.ExecuteTable(SQL)
        If (DT.Rows.Count > 0) Then
            If (DT.Rows.Count = 1) Then
                DR = DT_Year.NewRow
                DR("Year") = DT.Rows(0).Item("Year")
                DT_Year.Rows.Add(DR)
            Else
                For i As Integer = DT.Rows(0).Item("Year") To DT.Rows(DT.Rows.Count - 1).Item("Year")
                    DR = DT_Year.NewRow
                    DR("Year") = i
                    DT_Year.Rows.Add(DR)
                Next
            End If
        End If

        Return DT_Year
    End Function

    Private Sub lnkDialogCancel_Click(sender As Object, e As EventArgs) Handles lnkDialogCancel.Click
        pnlFolder.Visible = False
        txtFolder_Name.Text = ""
    End Sub

    Private Sub lnkDialogSave_Click(sender As Object, e As EventArgs) Handles lnkDialogSave.Click
        If txtFolder_Name.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณากรอกชื่อ Folder " & "');", True)
            Exit Sub
        End If
        '----Save--- 

        Dim Sql As String = " select * from TB_Activity where id=" & ActivityID

        If Not IsNothing(New_Activity) Then
            If New_Activity = -1 Then
                Sql = " select * from TB_Activity where 0=1"
            End If
        End If
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If (DT.Rows.Count = 0) Then
            DR = DT.NewRow
            DR("project_id") = Project_ID
            DR("parent_id") = ActivityID
            DT.Rows.Add(DR)

        Else
            DR = DT.Rows(0)
        End If
        DR("activity_name") = txtFolder_Name.Text
        DR("Is_Folder") = True
        DR("notify") = "Y"
        DR("active_status") = "1"
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถบันทึกข้อมูลได้ " & "');", True)
            Exit Sub
        End Try

        pnlFolder.Visible = False

        Sql = " select * from TB_Project where id=" & Project_ID
        DA = New SqlDataAdapter(Sql, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        Dim Param As String = "id=" + Project_ID.ToString() + "&mode=edit&type=" + DT.Rows(0).Item("project_type").ToString()

        Select Case DT.Rows(0).Item("project_type").ToString()

            Case 0
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Activity.aspx?" & Param & "';", True)
            Case 1
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Activity.aspx?" & Param & "';", True)

            Case 2
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmLoan_Detail_Activity.aspx?" & Param & "';", True)
            Case 3
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmContribuiltion_Detail_Activity.aspx?" & Param & "';", True)
        End Select

    End Sub

    Private Sub btnAddAct_Click(sender As Object, e As EventArgs) Handles btnAddAct.Click
        RaiseEvent AddSubActivity(Me)
    End Sub

    Private Sub btnAddFolder_Click(sender As Object, e As EventArgs) Handles btnAddFolder.Click
        pnlFolder.Visible = True
        txtFolder_Name.Text = ""
        New_Activity = -1
    End Sub



    'Public Sub Bind_Col_Master_Year()
    '    Dim DT_Cooperation As DataTable = Get_Col_Year()
    '    rptCol.DataSource = DT_Cooperation
    '    rptCol.DataBind()
    'End Sub




    'Private Sub rptCol_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCol.ItemDataBound
    '    Dim lbl_Col_id As Label = e.Item.FindControl("lbl_Col_id")
    '    Dim lbl_Col_Name As Label = e.Item.FindControl("lbl_Col_Name")
    '    Dim th_radius_2 As HtmlTableCell = e.Item.FindControl("th_radius_2")

    'Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}

    'th_radius_2.Style("background-color") = ArrBrightPastel(e.Item.ItemIndex).ToString()
    'th_radius_2.Style("border-radius") = "30px 30px 0px 0px"
    'th_radius_2.Style("height") = "30px"


    '-----------ใส่สี-------------
    'หาปี น้อย-มากสุกของ กิจกรมมนั้นๆ
    'Dim DT As DataTable = Get_Col_Year()
    'Dim Y_Min As Integer = 2015
    'Dim Y_Max As Integer = 2017
    'If (DT.Rows.Count > 0) Then

    'For i As Integer = 0 To DT.Rows.Count - 1
    'If (i >= Get_Col_Year.Rows(i).Item("Year") And Y_Max <= Get_Col_Year.Rows(i).Item("Year")) Then
    '    th_radius_2.Attributes("Class") = "tdinprogress"
    'End If
    'If (e.Item.DataItem("Year") >= Year_MIN And e.Item.DataItem("Year") <= Year_MAX) Then

    ' th_radius_2.Attributes("Class") = "tdinprogress"
    'Else
    '  th_radius_2.Attributes("Class") = ""

    'End If





    'Next
    'End If

    'Class='tdinprogress'


    'lbl_Col_id.Text = e.Item.DataItem("Year")
    'lbl_Col_Name.Text = e.Item.DataItem("Year").ToString()

    ' End Sub

End Class
