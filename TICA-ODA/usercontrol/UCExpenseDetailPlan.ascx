﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCExpenseDetailPlan.ascx.vb" Inherits="usercontrol_UCExpenseDetailPlan" %>

<style>
    .accounting {
        border: none;
        border-bottom: 1px solid #f1f1f1;
        /*background-color: #f1f1f1 !important;*/
    }
</style>

<asp:UpdatePanel ID="udpList" runat="server">
    <ContentTemplate>

        <asp:Label ID="lblPID" runat="server" Text="" Visible="false"></asp:Label>
        <asp:Label ID="lblType" runat="server" Text="" Visible="false"></asp:Label>
        <asp:Label ID="lblHid" runat="server" Text="" Visible="false"></asp:Label>
        <asp:Label ID="lblactid" runat="server" Text="" Visible="false"></asp:Label>
        <div class="box-body">
            <div class="tb-fix">
                <table id="example2" class="table table-bordered table-hover" style="border-collapse: initial">
                    <thead>
                        <tr class="bg-gray text-center" style="border-bottom: 10px solid #ddd">
                            <%-- Add Header Table--%>
                            <asp:Repeater ID="rptHead" runat="server">
                                <ItemTemplate>
                                    <th class="text-center" id="TB_head" runat="server" style="border-bottom: 1px solid #f4f4f4">
                                        <asp:Label ID="lblGroupId" runat="server" Visible="false" Text='<%#Eval("expense_group_id") %>'></asp:Label>
                                        <asp:Label ID="lblCount" runat="server" Visible="false" Text='<%#Eval("item_count") %>'></asp:Label>
                                        <asp:Label ID="lblHead" runat="server" Text='<%#Eval("group_name") %>'></asp:Label>
                                    </th>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%-- End Add Header Table--%>
                        </tr>
                        <tr>
                            <h4 class="text-primary">
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </h4>
                        </tr>
                    </thead>

                    <thead>
                        <tr class="bg-gray text-center">
                            <th class="text-center tb-fix-col-2" style="width: 200px">Recipient
                                <br />
                                (ชื่อผู้รับทุน)</th>
                            <th class="text-center tb-fix-col-2">Type
                                <br />
                                (ประเภทเงิน)</th>
                            <%-- Add Header Table--%>
                            <asp:Repeater ID="rptCol" runat="server">
                                <ItemTemplate>
                                    <th>
                                        <asp:Label ID="lblsubexpenseid" runat="server" Visible="false" Text='<%#Eval("id") %>'></asp:Label>

                                        <asp:Label ID="lblCol" runat="server" Text='<%#Eval("sub_name") %>'></asp:Label>
                                    </th>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                        <%--<tr>
                <h4 class="text-primary">
                <asp:Label ID="lblActivityName" runat="server"></asp:Label>
            </h4>
           </tr>--%>
                    </thead>

                    <tbody>
                        <asp:Repeater ID="rptList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-title="Recipince" style="min-width: 170px; max-width: 170px;">
                                        <asp:Label ID="lblrecipince_type" runat="server" Text="0" Visible="false"></asp:Label>
                                        <asp:Label ID="lblnodeid" runat="server" Text="0" Visible="false"></asp:Label>
                                        <img id="imgicon" runat="server" src="" />
                                        <asp:Label ID="lblname" runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td>แผนงบประมาณ
                                        <asp:LinkButton ID="btnEditPlan" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEditPlan">
                                    <i class="fa fa-pencil text-blue"></i> แก้ไข
                                        </asp:LinkButton>
                                    </td>
                                    <%--Add text box--%>
                                    <asp:Repeater ID="rptColItem" runat="server" OnItemDataBound="rptColItem_ItemDataBound">
                                        <ItemTemplate>
                                            <td style="padding: 0px">
                                                <asp:Label ID="lblitem_subexpenseid" runat="server" Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtCol" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px"  class="cost accounting" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}"></asp:TextBox>
                                                <asp:Label ID="Pay_Amount_Actual" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                                <tr>
                                    <td data-title="Recipince" style="min-width: 170px; max-width: 170px;"></td>
                                    <td>จ่ายจริง                                        
                                        <asp:LinkButton ID="btnEditPay" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdEditPay">
                                    <i class="fa fa-pencil text-blue"></i> แก้ไข
                                        </asp:LinkButton>
                                    </td>
                                    <asp:Repeater ID="rptColItem2" runat="server" OnItemDataBound="rptColItem2_ItemDataBound">
                                        <ItemTemplate>
                                            <%--Add text box--%>
                                            <td style="padding: 0px">
                                                <asp:Label ID="lblitem_subexpenseid" runat="server" Visible="false"></asp:Label>
                                                <asp:TextBox ID="lbamount" MaxLength="10" runat="server" Text="" Style="text-align: right; width: 100%; height: 35px"
                                                    AutoPostBack="true" class="cost accounting" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}"></asp:TextBox>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%--End text box--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>

            <%-- Total --%>
        </div>
    </ContentTemplate>

</asp:UpdatePanel>

<script type="text/javascript">
    function item_count() {
        var count = $('#lblCount').val()

        document.getElementById("TB_head").colSpan = count;
        window.alert("sometext" + count);

    }
</script>

