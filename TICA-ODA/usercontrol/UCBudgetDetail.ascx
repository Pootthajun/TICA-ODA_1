﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCBudgetDetail.ascx.vb" Inherits="usercontrol_UCBudgetDetail" %>
 <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray text-center">
                <th style="width: 50px"class="text-center">No. (ลำดับ)</th>
                <th  class="text-center">Sub Budget (งบประมาณย่อย)</th>
                <th style="width: 150px" class="text-center">Received (ได้รับ)</th>
                <th style="width: 150px" class="text-center">Budget (งบประมาณ)</th>
                <th style="width: 150px" class="text-center">Used (ใช้ไป)</th>
                <th style="width: 150px" class="text-center">Balance (คงเหลือ)</th>
            </tr>
            <tr>
             <h4 class="text-primary">
            <asp:Label ID="lblTypeBudget" runat="server"></asp:Label>
            </h4>
           </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
               
                    <tr>

                        <td data-title=">No.">
                            <asp:Label ID="lblbudget_sub_id" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </td>
                        <td data-title="Sub Budget" id="tdSubBudget" runat="server" >
                            <asp:Label ID="lblSubBudget" runat="server"></asp:Label>
                        </td>
                        <td data-title="Received" id="td2" runat="server"  style="text-align: right;" >
                            <asp:TextBox  ID="txtReceived" runat="server" CssClass="form-control" placeholder="-" AutoPostBack="true" OnTextChanged="txtReceived_TextChanged" MaxLength="12" style="text-align: right"  Enabled ='<%# Eval("id").ToString() <> "-1" %>'></asp:TextBox>
                            <asp:Label ID="lblReceived" runat="server" ></asp:Label>
                        </td>

                        <td data-title="Budget" id="td3" runat="server" style="text-align: right;" >
                            <asp:Label ID="lblBudget" runat="server" ></asp:Label>
                        </td>
                        <td data-title="Used"  style="text-align: right;">
                            <asp:Label ID="lblUsed" runat="server"></asp:Label>
                        </td>
                        <td data-title="Balance" id="td4" runat="server" style="text-align: right;">
                            <asp:Label ID="lblBalance" runat="server" ></asp:Label>
                        </td>

                    </tr>
                    
                </ItemTemplate>
               <%--<FooterTemplate>
                    <table>
                     <tr>
                         <td colspan="3" style="text-align:right">Total</td>
                         <td data-title="Received" id="td5" runat="server">
                         <asp:Label ID="lblTotalReceived" runat="server"></asp:Label>
                         </td>
                         <td data-title="Budget" id="td6" runat="server">
                         <asp:Label ID="lblTotalBudget" runat="server"></asp:Label>
                         </td>
                         <td data-title="Used" id="td7" runat="server">
                         <asp:Label ID="lblTotalUse" runat="server"></asp:Label>
                         </td>
                         <td data-title="Balance" id="td8" runat="server">
                         <asp:Label ID="lblTotalBalance" runat="server"></asp:Label>
                         </td>
                     </tr>
                    </table>
                </FooterTemplate>--%>
            </asp:Repeater>
        </tbody>

    </table>

</div>
  </ContentTemplate>
</asp:UpdatePanel>