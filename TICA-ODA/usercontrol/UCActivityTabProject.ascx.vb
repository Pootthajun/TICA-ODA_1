﻿Imports System.Data
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Partial Class usercontrol_UCActivityTabProject
    Inherits System.Web.UI.UserControl

    Public Event SaveActivityComplete()
    Public Event CancelAct()

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property Old_Recipient_Type As String
        Get
            Try
                Return ViewState("Old_Recipient_Type")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("Old_Recipient_Type") = value
        End Set
    End Property

    Public WriteOnly Property ProjectID As Long
        Set(value As Long)
            lblProjectID.Text = value
        End Set
    End Property
    Public WriteOnly Property ActivityMode As String
        Set(value As String)
            lblActivityMode.Text = value
        End Set
    End Property


    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Private ReadOnly Property ActivityID_COPY As Integer
        Get
            Return Session("ActivityID_COPY")
        End Get
    End Property
    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property
    Private Sub usercontrol_UCActivityTabProject_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ProjectType = CInt(Request.QueryString("type"))
            mode = Request.QueryString("mode").ToString()

            BL.SetTextDblKeypress(txtAmount)
            Authorize()

            '---สำหรับเงินยืม--
            pnlAddAdvance.Visible = False       '--สร้างรอบ
            dialog_Advance_OU.Visible = False   '--เลือกผู้ยืม

        End If

        'If mode = "edit" = lblActivityMode.Text Then

        If mode = "edit" Then

            If ddlSelectReport.SelectedValue = 1 Then
                ddlComponent.Visible = True
                ddlInkind.Visible = False
                chkActive.Visible = True
                Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                traid.Visible = True
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                ddlComponent.Visible = False
                ddlInkind.Visible = True
                chkActive.Visible = False
                chkActive.Checked = False
                Label1.Text = ""
                traid.Visible = True
            Else
                ddlComponent.Visible = False
                ddlInkind.Visible = True
                chkActive.Visible = False
                Label1.Text = ""
                traid.Visible = False
            End If
        End If

        txtBenificiary.Visible = False
        txtRecipientCountry.Visible = False
        ctlSelectRecipientCountry.Visible = False
        txtRecipient.Visible = False
        ctlSelectRecipient.Visible = False

    End Sub

    Public Sub SetControlToViewMode(IsView As Boolean)

        '#TabActActivity
        txtActName.Enabled = Not IsView
        txtActDescription.Enabled = Not IsView
        ddlSelectReport.Enabled = Not IsView
        ddlSector.Enabled = Not IsView
        ddlSubSector.Enabled = Not IsView
        ddlInkind.Enabled = Not IsView
        chkActive.Enabled = Not IsView


        '#TabActExpense        
        'UCProjectComponent.SetToViewMode(IsView)
        'UCInkind.SetToViewMode(IsView)

        txtPlanActStart.Enabled = Not IsView
        txtplanActEnd.Enabled = Not IsView
        txtActualActStart.Enabled = Not IsView
        txtActualActEnd.Enabled = Not IsView
        'txtActCommitmentBudget.Enabled = Not IsView
        'txtActDisbursement.Enabled = Not IsView
        'txtActAdministrative.Enabled = Not IsView

        '#TabActReceipent
        TabActRecipent.Enabled = Not IsView
        'ctlSelectRecipient.Enabled = Not IsView
        'ctlSelectRecipientCountry.Enabled = Not IsView
        txtBenificiary.Enabled = Not IsView

        '#TabExpense
        ddlBudgetYear.Enabled = Not IsView
        ddlBudgetGroup.Enabled = Not IsView
        ddlBudgetSub.Enabled = Not IsView
        txtAmount.Enabled = Not IsView

        btnApply.Enabled = Not IsView
        btnAddAdvance.Enabled = Not IsView
        btnAddRow1.Enabled = Not IsView

        btnSaveAct.Visible = Not IsView




    End Sub

    Public Sub AddActivity(ParentID As Long)
        Session("status") = "add"
        ClearActivity()
        lblParentActivityID.Text = ParentID
        pnlAdd.Visible = True
    End Sub

    Public Sub EditActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        SetAcitivityInfoByID(ActivityID)
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = False
            liedit.Visible = True
            lidelete.Visible = True
        Next
        For i As Integer = 0 To rptAdvance.Items.Count - 1
            Dim liAdvanceView As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvanceView"), HtmlGenericControl)
            Dim liAdvanceEdit As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvanceEdit"), HtmlGenericControl)
            Dim liAdvancedelete As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvancedelete"), HtmlGenericControl)

            liAdvanceView.Visible = False
            liAdvanceEdit.Visible = True
            liAdvancedelete.Visible = True
        Next
        pnlAdmincost.Enabled = True
    End Sub

    Public Sub ViewActivity(ActivityID As Long)
        ClearActivity()
        pnlAdd.Visible = True
        SetAcitivityInfoByID(ActivityID)
        ctlSelectRecipient.Visible = False
        txtRecipient.Visible = True
        txtRecipient.Enabled = False

        ctlSelectRecipientCountry.Visible = False
        txtRecipientCountry.Visible = True
        txtRecipientCountry.Enabled = False

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = True
            liedit.Visible = False
            lidelete.Visible = False
        Next

        For i As Integer = 0 To rptAdvance.Items.Count - 1
            Dim liAdvanceView As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvanceView"), HtmlGenericControl)
            Dim liAdvanceEdit As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvanceEdit"), HtmlGenericControl)
            Dim liAdvancedelete As HtmlGenericControl = DirectCast(rptAdvance.Items(i).FindControl("liAdvancedelete"), HtmlGenericControl)

            liAdvanceView.Visible = True
            liAdvanceEdit.Visible = False
            liAdvancedelete.Visible = False
        Next
        pnlAdmincost.Enabled = False

    End Sub


    Public Sub CopyActivity()
        '--เชคก่อนถ้ากิจกรรมที่ต้องการวางทับ มีข้อมูลการเบิกจ่ายแล้ว ต้องการคัดลอก activity ต้องเคลียร์ข้อมูลก่อน
        Dim DT As DataTable = BL.GetList_Expense2(lblActivityID.Text)
        If (DT.Rows.Count > 0) Then
            alertmsg("เนื่องจากกิจกรรมนี้  มีการกรอกข้อมูลการเบิกจ่ายแล้วจึงไม่สามารถ วางข้อมูลที่คัดลอกลงในกิจกรรมนี้ได้ \n **หากต้องการวางข้อมูล กรุณาลบข้อมูลการเบิกจ่ายก่อน")
            Exit Sub
        End If

        Clear_DataBeforPaste()
        'ActivityID_COPY
        If ActivityID_COPY = Nothing Then Exit Sub

        Dim dtActDetail As New DataTable
        dtActDetail = BL.GetProjectActivityInfoByID(ActivityID_COPY)
        If dtActDetail.Rows.Count > 0 Then



            'Title(หัวข้อกิจกรรม)
            txtActName.Text = dtActDetail.Rows(0).Item("activity_name").ToString().Replace(" (copy)", "") & " (copy)"
            txtActDescription.Text = dtActDetail.Rows(0).Item("description").ToString()
            Try
                If dtActDetail.Rows(0).Item("sector_id").ToString() <> "" Then
                    ddlSector.SelectedValue = dtActDetail.Rows(0).Item("sector_id").ToString()
                    ddlSector_SelectedIndexChanged(Nothing, Nothing)
                    If dtActDetail.Rows(0).Item("sub_sector_id").ToString() <> "" Then ddlSubSector.SelectedValue = dtActDetail.Rows(0).Item("sub_sector_id").ToString()
                End If
            Catch ex As Exception
            End Try
            'ระยะเวลา
            If dtActDetail.Rows(0).Item("plan_start").ToString() <> "" Then
                txtPlanActStart.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("plan_end").ToString() <> "" Then

                txtplanActEnd.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("actual_start").ToString() <> "" Then
                txtActualActStart.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If dtActDetail.Rows(0).Item("actual_end").ToString() <> "" Then
                txtActualActEnd.Text = Convert.ToDateTime(dtActDetail.Rows(0).Item("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            'ข้อมูลผู้รับทุน

            Select Case dtActDetail.Rows(0).Item("Recipient_Type").ToString()
                Case "G"
                    RadioSelectRe.SelectedValue = 1
                    UCProjectCountryRe.Visible = True
                    UCprojectRecipience.Visible = False
                    UCProjectCountry.Visible = False
                Case "R"
                    RadioSelectRe.SelectedValue = 2
                    UCProjectCountryRe.Visible = False
                    UCprojectRecipience.Visible = True
                    UCProjectCountry.Visible = False
                Case "C"
                    RadioSelectRe.SelectedValue = 3
                    UCProjectCountryRe.Visible = False
                    UCprojectRecipience.Visible = False
                    UCProjectCountry.Visible = True

            End Select

            Dim dtActProjectCountryRe As New DataTable
            dtActProjectCountryRe = UCProjectCountryRe.SetDataRpt(ActivityID_COPY)

            Dim dtActProjectCountry As New DataTable
            dtActProjectCountry = UCProjectCountry.SetDataRpt(ActivityID_COPY)

            Dim dtAcProjectRecipience As New DataTable
            dtAcProjectRecipience = UCprojectRecipience.SetDataRpt(ActivityID_COPY)

            'Group Aid(ประเภทความช่วยเหลือ)

            Dim dtComponent As DataTable = BL.GetActivityComponent(ActivityID_COPY)
            If dtComponent.Rows.Count > 0 Then
                '    UCActivityBudget.BudgetDT = dtActivityBudget
                Dim _component_id As String = dtComponent.Rows(0)("component_id").ToString
                ddlComponent.Visible = True
                ddlComponent.SelectedValue = _component_id
                ddlInkind.Visible = False
                ddlSelectReport.SelectedValue = 1
                chkActive.Visible = True
                Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
                traid.Visible = True
            End If


            Dim dtInkind As DataTable = BL.GetProjectActivityInkind(ActivityID_COPY)
            If dtInkind.Rows.Count > 0 Then
                '    UCActivityBudget.BudgetDT = dtActivityBudget
                Dim _inkind_id As String = dtInkind.Rows(0)("inkind_id").ToString
                ddlInkind.SelectedValue = _inkind_id
                ddlInkind.Visible = True
                ddlSelectReport.SelectedValue = 2
                ddlComponent.Visible = False
                Label1.Text = ""
                traid.Visible = True
            End If

            alertmsg("วางข้อมูลเรียบร้อย")
        End If


    End Sub

    Private Sub Clear_DataBeforPaste()
        'Title(หัวข้อกิจกรรม)
        'ระยะเวลา
        'ข้อมูลผู้รับทุน
        'Group Aid(ประเภทความช่วยเหลือ)

        RadioSelectRe.SelectedValue = 1
        Old_Recipient_Type = ""
        '==============================================
        txtActName.Text = ""
        txtActDescription.Text = ""
        BL.Bind_DDL_Perposecat(ddlSector)
        ddlSector_SelectedIndexChanged(Nothing, Nothing)

        chkActive.Checked = False
        ddlSelectReport.SelectedValue = 0
        ddlInkind.SelectedValue = ""
        ddlComponent.SelectedValue = ""
        chkActive.Visible = False
        Label1.Text = ""

        BL.Bind_DDL_ExpenseTemplate(lbltemplate)
        BL.Bind_DDL_Component(ddlComponent)
        BL.Bind_DDL_Inkind(ddlInkind)

        txtPlanActStart.Text = ""
        txtplanActEnd.Text = ""
        txtActualActStart.Text = ""
        txtActualActEnd.Text = ""

        '==============================================

        Dim dtCountryRe As New DataTable
        With dtCountryRe
            .Columns.Add("id")
            .Columns.Add("country_node_id")
            .Columns.Add("amout_person")
        End With
        UCProjectCountryRe.ComponentDT = dtCountryRe

        Dim dtCountry As New DataTable
        With dtCountry
            .Columns.Add("id")
            .Columns.Add("country_node_id")
        End With
        UCProjectCountry.CountryDT = dtCountry

        Dim dtRecipient As New DataTable
        With dtRecipient
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("recipient_id")
        End With
        UCprojectRecipience.RecipienceDT = dtRecipient
        UCprojectRecipience.Bindata_toRptCountRe()
    End Sub
    'Session("ActivityID_COPY")   '-- Session ที่คัดลอกไว้



    Public Sub SetAcitivityInfoByID(activity_id As String)
        BL.Bind_DDL_Incoming_Budget(ddlBudgetYear)

        '#TabActActDetail
        Dim dtActDetail As New DataTable
        dtActDetail = BL.GetProjectActivityInfoByID(activity_id)

        Dim dtActProjectCountryRe As New DataTable
        dtActProjectCountryRe = UCProjectCountryRe.SetDataRpt(activity_id)

        Dim dtActProjectCountry As New DataTable
        dtActProjectCountry = UCProjectCountry.SetDataRpt(activity_id)

        Dim dtAcProjectRecipience As New DataTable
        dtAcProjectRecipience = UCprojectRecipience.SetDataRpt(activity_id)

        Dim dtActAdministrative_Cost As New DataTable
        dtActAdministrative_Cost = UCProjectAdministrativeCost.SetDataRpt_Administrative_Cost(activity_id)

        UCProjectAdministrativeCost.SetToValueActivityId(activity_id)


        Dim DT_Administrative_Cost As New DataTable
        DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
        Dim DT_ProjectCountryRe As New DataTable
        DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
        Dim DT_UCProjectCountry As New DataTable
        DT_UCProjectCountry = UCProjectCountry.CountryDT
        Dim DT_ProjectRecipient As New DataTable
        DT_ProjectRecipient = UCprojectRecipience.RecipienceDT

        Dim _activity_name As String = ""
        Dim _description As String = ""
        Dim _sector_id As String = ""
        Dim _sub_sector_id As String = ""
        Dim _parent_id As String = ""
        Dim _plan_start As String = ""
        Dim _plan_end As String = ""
        Dim _actual_start As String = ""
        Dim _actual_end As String = ""
        Dim _commitment_budget As String = ""
        Dim _disbursement As String = ""
        Dim _administrative As String = ""
        Dim _beneficiary As String = ""
        Dim _notify As String = ""
        Dim _Recipient_type As String = ""

        If dtActDetail.Rows.Count > 0 Then
            Dim _dr As DataRow = dtActDetail.Rows(0)
            _activity_name = _dr("activity_name").ToString()
            _description = _dr("description").ToString()
            _sector_id = _dr("sector_id").ToString()
            _sub_sector_id = _dr("sub_sector_id").ToString()
            _parent_id = _dr("parent_id").ToString()
            _notify = _dr("notify").ToString
            If Session("back") = "palnex" Then
                If _dr("actual_start").ToString = "" Or _dr("actual_end").ToString = "" Then
                    _plan_start = ""
                    _plan_end = ""
                Else
                    _plan_start = Convert.ToDateTime(_dr("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _plan_end = Convert.ToDateTime(_dr("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If

            Else

            End If
            If _dr("plan_start").ToString() <> "" Then
                _plan_start = Convert.ToDateTime(_dr("plan_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            If _dr("plan_end").ToString() <> "" Then
                _plan_end = Convert.ToDateTime(_dr("plan_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            If _dr("actual_start").ToString() <> "" Then
                _actual_start = Convert.ToDateTime(_dr("actual_start")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            If _dr("actual_end").ToString() <> "" Then
                _actual_end = Convert.ToDateTime(_dr("actual_end")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            If _dr("commitment_budget").ToString() <> "" Then
                _commitment_budget = Convert.ToDecimal(_dr("commitment_budget")).ToString("#,##0.00")
            End If
            If _dr("disbursement").ToString() <> "" Then
                _disbursement = Convert.ToDecimal(_dr("disbursement")).ToString("#,##0.00")
            End If
            If _dr("administrative").ToString() <> "" Then
                _administrative = Convert.ToDecimal(_dr("administrative")).ToString("#,##0.00")
            End If
            _beneficiary = _dr("beneficiary").ToString()
            _Recipient_type = _dr("Recipient_Type").ToString()
        End If

        lblActivityID.Text = activity_id
        txtActName.Text = _activity_name
        txtActDescription.Text = _description

        If _Recipient_type = "G" Then
            RadioSelectRe.SelectedValue = 1
            UCProjectCountry.Visible = False
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
        ElseIf _Recipient_type = "R" Then
            RadioSelectRe.SelectedValue = 2
            UCProjectCountry.Visible = False
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = True
        ElseIf _Recipient_type = "C" Then
            RadioSelectRe.SelectedValue = 3
            UCProjectCountry.Visible = True
            UCProjectCountryRe.Visible = False
            UCprojectRecipience.Visible = False
        End If
        Old_Recipient_Type = RadioSelectRe.SelectedValue

        Try
            If _sector_id <> "" Then
                ddlSector.SelectedValue = _sector_id
                ddlSector_SelectedIndexChanged(Nothing, Nothing)
                If _sub_sector_id <> "" Then ddlSubSector.SelectedValue = _sub_sector_id

            End If


        Catch ex As Exception
        End Try

        lblParentActivityID.Text = _parent_id
        chkActive.Checked = IIf(_notify = "Y", True, False)

        Dim dtActemplate As DataTable = BL.GetActivityTemplate(activity_id)
        If dtActemplate.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _template_id As String = dtActemplate.Rows(0)("template_id").ToString
            Try
                lbltemplate.SelectedValue = _template_id
            Catch ex As Exception
                lbltemplate.SelectedIndex = 0
            End Try


        End If

        '#TabActExpense
        Dim dtActivityBudget As DataTable = BL.GetActivityBudget(activity_id)
        If dtActivityBudget.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _budget_year As String = dtActivityBudget.Rows(0)("budget_year").ToString
            Dim _budget_group As String = dtActivityBudget.Rows(0)("budget_group_id").ToString
            Dim _budget_sub As String = dtActivityBudget.Rows(0)("budget_sub_id").ToString
            Dim _amount As String = Convert.ToDecimal(dtActivityBudget.Rows(0)("amount")).ToString("#,##0.00")
            BL.Bind_DDL_Incoming_Budget(ddlBudgetYear, _budget_year)
            'ddlBudgetYear.SelectedValue = _budget_year
            If ddlBudgetYear.SelectedIndex > 0 Then
                BL.Bind_DDL_Incoming_Group(ddlBudgetGroup, _budget_year)
                ddlBudgetGroup.SelectedValue = _budget_group

                If ddlBudgetGroup.SelectedIndex > 0 Then
                    BL.Bind_DDL_Incoming_Sub(ddlBudgetSub, _budget_year, _budget_group)
                    ddlBudgetSub.SelectedValue = _budget_sub
                End If
            End If


            'ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)
            'ddlBudgetSub.SelectedValue = _budget_sub
            txtAmount.Text = _amount
        End If

        '---Compare---
        'If lblError_compare.Text <> lblBudget_Sum.Text Then
        '    lblError_compare.Text = "ตรวจสอบข้อมูลงบประมาณทั้งหมด (total budget) กับ งบประมาณ (Budget) แผนการเบิกจ่าย ให้ถูกต้อง"
        'End If




        Dim dtComponent As DataTable = BL.GetActivityComponent(activity_id)
        If dtComponent.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _component_id As String = dtComponent.Rows(0)("component_id").ToString
            ddlComponent.Visible = True
            ddlComponent.SelectedValue = _component_id
            ddlInkind.Visible = False
            ddlSelectReport.SelectedValue = 1
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        End If



        Dim dtInkind As DataTable = BL.GetProjectActivityInkind(activity_id)
        If dtInkind.Rows.Count > 0 Then
            '    UCActivityBudget.BudgetDT = dtActivityBudget
            Dim _inkind_id As String = dtInkind.Rows(0)("inkind_id").ToString
            ddlInkind.SelectedValue = _inkind_id
            ddlInkind.Visible = True
            ddlSelectReport.SelectedValue = 2
            ddlComponent.Visible = False
            Label1.Text = ""
            traid.Visible = True
        End If

        txtPlanActStart.Text = _plan_start
        txtplanActEnd.Text = _plan_end
        txtActualActStart.Text = _actual_start
        txtActualActEnd.Text = _actual_end

        '#TabActReceipent
        Dim _recipientname As String = ""
        Dim dtRecipientPerson As New DataTable
        dtRecipientPerson = BL.GetActivityRecipincePerson(activity_id)
        For Each Item As ListItem In ctlSelectRecipient.Items
            For i As Integer = 0 To dtRecipientPerson.Rows.Count - 1
                If Item.Value = dtRecipientPerson.Rows(i)("node_id").ToString Then
                    Item.Selected = True
                    _recipientname &= Item.Text & ","
                    Exit For
                End If
            Next
        Next

        Dim _recipientcountryname As String = ""
        Dim dtRecipientCountry As New DataTable
        dtRecipientCountry = BL.GetActivityRecipinceCountry(activity_id)
        For Each Item As ListItem In ctlSelectRecipientCountry.Items
            For i As Integer = 0 To dtRecipientCountry.Rows.Count - 1
                If Item.Value = dtRecipientCountry.Rows(i)("node_id").ToString Then
                    Item.Selected = True
                    _recipientcountryname &= Item.Text & ","
                    Exit For
                End If
            Next
        Next

        txtRecipient.Text = ""
        If _recipientname.Length > 0 Then
            txtRecipient.Text = _recipientname.Substring(0, _recipientname.Length - 1)
        End If

        txtRecipientCountry.Text = ""
        If _recipientcountryname.Length > 0 Then
            txtRecipientCountry.Text = _recipientcountryname.Substring(0, _recipientcountryname.Length - 1)
        End If

        txtBenificiary.Text = _beneficiary
        BindList(activity_id)
        BindListAdvance(activity_id)
    End Sub

    Private Sub ClearActivity()
        '##tabActivity
        '#TabActActDetail


        Old_Recipient_Type = ""
        lblActivityID.Text = "0"
        lblParentActivityID.Text = "0"
        txtActName.Text = ""
        txtActDescription.Text = ""
        BL.Bind_DDL_Perposecat(ddlSector)
        ddlSector_SelectedIndexChanged(Nothing, Nothing)

        chkActive.Checked = False
        ddlSelectReport.SelectedValue = 0
        ddlInkind.SelectedValue = ""
        ddlComponent.SelectedValue = ""
        chkActive.Visible = False
        Label1.Text = ""


        BL.Bind_DDL_ExpenseTemplate(lbltemplate)

        BL.Bind_DDL_Component(ddlComponent)
        BL.Bind_DDL_Inkind(ddlInkind)
        BL.Bind_DDL_Incoming_Budget(ddlBudgetYear)
        'BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)

        'ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)
        txtAmount.Text = ""

        Dim dtCountryRe As New DataTable
        With dtCountryRe
            .Columns.Add("id")
            .Columns.Add("country_node_id")
            .Columns.Add("amout_person")
        End With
        UCProjectCountryRe.ComponentDT = dtCountryRe

        Dim dtCountry As New DataTable
        With dtCountry
            .Columns.Add("id")
            .Columns.Add("country_node_id")
        End With
        UCProjectCountry.CountryDT = dtCountry

        Dim dtRecipient As New DataTable
        With dtRecipient
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("recipient_id")
        End With
        UCprojectRecipience.RecipienceDT = dtRecipient
        UCprojectRecipience.Bindata_toRptCountRe()


        Dim AdministrativeCost As New DataTable
        With AdministrativeCost
            .Columns.Add("id")
            .Columns.Add("date_payment")
            .Columns.Add("Description")
            .Columns.Add("amount")
        End With
        UCProjectAdministrativeCost.ComponentDT = AdministrativeCost

        BindList(0)

        txtPlanActStart.Text = ""
        txtplanActEnd.Text = ""
        txtActualActStart.Text = ""
        txtActualActEnd.Text = ""

        '#TabActReceipent
        ctlSelectRecipient.Visible = True
        txtRecipient.Visible = False
        ctlSelectRecipientCountry.Visible = True
        txtRecipientCountry.Visible = False
        SetRecipient()
        SetRecipientCountry()
        txtBenificiary.Text = ""
    End Sub

    Private Sub SetRecipient()
        Dim dt As New DataTable
        dt = BL.GetPersonList(Constants.Person_Type.Recipient)

        ctlSelectRecipient.DataValueField = "node_id"
        ctlSelectRecipient.DataTextField = "name_th"

        ctlSelectRecipient.DataSource = dt
        ctlSelectRecipient.DataBind()
    End Sub

    Private Sub SetRecipientCountry()
        Dim dt As New DataTable
        dt = BL.GetOrganizeUnitByNodeID(0, Constants.OU.Country)

        ctlSelectRecipientCountry.DataValueField = "node_id"
        ctlSelectRecipientCountry.DataTextField = "name_th"

        ctlSelectRecipientCountry.DataSource = dt
        ctlSelectRecipientCountry.DataBind()

    End Sub

#Region "Tab Activity"
    Protected Enum Tab
        ActActDetail = 1
        ActExpense = 2
        ActReceipent = 3
        ActPeriod = 4
        ActDisversment = 5
    End Enum
    Protected Property CurrentTabAct As Tab
        Get
            Select Case True
                Case TabActActDetail.Visible
                    Return Tab.ActActDetail
                Case TabActExpense.Visible
                    Return Tab.ActExpense
                Case TabActRecipent.Visible
                    Return Tab.ActReceipent
                Case Else
                    'Return Tab.ActActDetail
            End Select
        End Get
        Set(value As Tab)
            TabActActDetail.Visible = False
            TabActPeriod.Visible = False
            TabActRecipent.Visible = False
            TabActExpense.Visible = False
            TabActDisversement.Visible = False

            liTabActActDetail.Attributes("class") = ""
            liTabActPeriod.Attributes("class") = ""
            liTabActReceipent.Attributes("class") = ""
            liTabActExpense.Attributes("class") = ""
            liTabActDisversment.Attributes("class") = ""

            Select Case value
                Case Tab.ActActDetail
                    TabActActDetail.Visible = True
                    liTabActActDetail.Attributes("class") = "active"
                Case Tab.ActReceipent
                    TabActRecipent.Visible = True
                    liTabActReceipent.Attributes("class") = "active"
                Case Tab.ActPeriod
                    TabActPeriod.Visible = True
                    liTabActPeriod.Attributes("class") = "active"
                Case Tab.ActExpense
                    TabActExpense.Visible = True
                    liTabActExpense.Attributes("class") = "active"
                Case Tab.ActDisversment
                    TabActDisversement.Visible = True
                    liTabActDisversment.Attributes("class") = "active"
                Case Else
            End Select
        End Set

    End Property

    Private Sub ChangeTabAct(sender As Object, e As System.EventArgs) Handles btnTabActActDetail.Click, btnTabActExpense.Click, btnTabActReceipent.Click, btnTabActPeriod.Click, btnTabActDisversment.Click
        Select Case True
            Case Equals(sender, btnTabActActDetail)
                CurrentTabAct = Tab.ActActDetail
            Case Equals(sender, btnTabActReceipent)
                CurrentTabAct = Tab.ActReceipent
            Case Equals(sender, btnTabActPeriod)
                CurrentTabAct = Tab.ActPeriod
            Case Equals(sender, btnTabActExpense)
                CurrentTabAct = Tab.ActExpense
            Case Equals(sender, btnTabActDisversment)
                CurrentTabAct = Tab.ActDisversment
            Case Else
        End Select
    End Sub

#End Region
    Private Sub Validate_Recipient()

        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            If Session("status") = "add" Then
                '##tabActivity
                '#TabActActDetail

            End If

            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActName.Text
                .DESCRIPTION = txtActDescription.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .NOTIFY = IIf(chkActive.Checked, "Y", "N").ToString
                .ACTIVE_STATUS = "1"
                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If
            End With

            '#TabActExpense
            Dim DTBudget As New DataTable
            With DTBudget
                .Columns.Add("budget_year")
                .Columns.Add("budget_group_id")
                .Columns.Add("budget_sub_id")
                .Columns.Add("amount")
            End With
            Dim dr As DataRow
            dr = DTBudget.NewRow
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlBudgetSub.SelectedValue
            dr("amount") = txtAmount.Text
            DTBudget.Rows.Add(dr)

            With lnqActivity
                '.ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
                '.ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")

                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text



                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
                If txtActualActEnd.Text <> "" Then
                    Try
                        .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
            End With

            '#TabActReceipent
            Dim DTRecipientPerson As DataTable = GetRecipientPerson()
            Dim DTRecipentCountry As DataTable = GetRecipientCountry()

            With lnqActivity
                .BENEFICIARY = txtBenificiary.Text
            End With

            Dim DT_Administrative_Cost As New DataTable
            DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT


            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                If ddlComponent.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, DTComponent, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                If ddlInkind.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, Nothing, DTInkind, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    '--
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = lblActivityID.Text
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try


    End Sub

#Region "Activity Event"
    Private Sub btnCancelAct_Click(sender As Object, e As EventArgs) Handles btnCancelAct.Click
        pnlAdd.Visible = False

        RaiseEvent CancelAct()
    End Sub


    Private Sub btnSaveAct_Click(sender As Object, e As EventArgs) Handles btnSaveAct.Click
        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            If Session("status") = "add" Then
                '##tabActivity
                '#TabActActDetail

            End If

            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActName.Text
                .DESCRIPTION = txtActDescription.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .NOTIFY = IIf(chkActive.Checked, "Y", "N").ToString
                .ACTIVE_STATUS = "1"
                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If
            End With

            '#TabActExpense
            Dim DTBudget As New DataTable
            With DTBudget
                .Columns.Add("budget_year")
                .Columns.Add("budget_group_id")
                .Columns.Add("budget_sub_id")
                .Columns.Add("amount")
            End With
            Dim dr As DataRow
            dr = DTBudget.NewRow
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlBudgetSub.SelectedValue
            dr("amount") = txtAmount.Text
            DTBudget.Rows.Add(dr)

            With lnqActivity
                '.ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
                '.ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")

                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text



                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
                If txtActualActEnd.Text <> "" Then
                    Try
                        .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
            End With

            '#TabActReceipent
            Dim DTRecipientPerson As DataTable = GetRecipientPerson()
            Dim DTRecipentCountry As DataTable = GetRecipientCountry()

            With lnqActivity
                .BENEFICIARY = txtBenificiary.Text
            End With

            Dim DT_Administrative_Cost As New DataTable
            DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT


            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            Dim ret As New ProcessReturnInfo
            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                If ddlComponent.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                ret = New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, DTComponent, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then

                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                If ddlInkind.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                ret = New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, Nothing, DTInkind, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then

                    lblActivityID.Text = "0"
                    lblParentActivityID.Text = "0"
                    alertmsg("บันทึกข้อมูลเรียบร้อยแล้ว")

                    pnlAdd.Visible = False
                    RaiseEvent SaveActivityComplete()
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = ret.ID
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try

    End Sub

    Private Function ValidateAct() As Boolean
        Dim ret As Boolean = True
        If txtActName.Text.Trim() = "" Then
            alertmsg("กรุณาระบุหัวข้อกิจกรรม")
            ret = False
        End If

        If ddlSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุ Sector(สาขา)")
            ret = False
        End If

        If ddlSubSector.SelectedValue = "" Then
            alertmsg("กรุณาระบุ Sub Sector(สาขาย่อย)")
            ret = False
        End If

        If txtActualActStart.Text = "" Then
            alertmsg("กรุณาระบุวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtActualActEnd.Text = "" Then
            alertmsg("กรุณาระบุวันสิ้นสุดโครงการ")
            ret = False
        End If

        Try
            Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("แผนของวันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlBudgetYear.SelectedValue = "" Then
            alertmsg("กรุณาระบุปีงบประมาณ")
            ret = False
        End If

        If ddlBudgetGroup.SelectedValue = "" Then
            alertmsg("กรุณาระบุประเภทงบประมาณ")
            ret = False
        End If

        If ddlBudgetSub.SelectedValue = "" Then
            alertmsg("กรุณาระบุงบประมาณ")
            ret = False
        End If


        If txtAmount.Text = "" Then
            alertmsg("กรุณาระบุจำนวนเงิน")
            ret = False
        End If

        'Country re ประเทศผู้รับทุน
        If RadioSelectRe.SelectedValue = 1 Then
            Dim DTProjectCountryRe As DataTable = UCProjectCountryRe.ComponentDT
            For i As Integer = 0 To DTProjectCountryRe.Rows.Count - 1
                Dim id As String = DTProjectCountryRe.Rows(i)("id").ToString
                Dim country_node_id As String = DTProjectCountryRe.Rows(i)("country_node_id").ToString
                Dim amout_person As String = DTProjectCountryRe.Rows(i)("amout_person").ToString

                If country_node_id = "" Then
                    alertmsg("กรุณาระบุประเทศ ")
                    ret = False
                End If

                If amout_person = 0 Then
                    alertmsg("กรุณาระบุจำนวนคน ")
                    ret = False
                End If

                Dim tempdr() As DataRow
                tempdr = DTProjectCountryRe.Select("country_node_id='" & country_node_id & "' and id <> '" & id & "'")
                If tempdr.Length > 0 Then
                    alertmsg("รายชื่อประเทศรับทุนซ้ำ ")
                    ret = False
                End If
            Next
        ElseIf RadioSelectRe.SelectedValue = 2 Then
            'ผู้รับทุน Recipient
            Dim DTProjectRecipient As DataTable = UCprojectRecipience.RecipienceDT
            For i As Integer = 0 To DTProjectRecipient.Rows.Count - 1
                Dim id As String = DTProjectRecipient.Rows(i)("id").ToString
                Dim recipient_id As String = DTProjectRecipient.Rows(i)("recipient_id").ToString

                If recipient_id = "" Then
                    alertmsg("กรุณาระบุผู้รับทุน ")
                    ret = False
                End If

                Dim tempdr() As DataRow
                tempdr = DTProjectRecipient.Select("recipient_id ='" & recipient_id & "' and id <> '" & id & "'")
                If tempdr.Length > 0 Then
                    alertmsg("รายชื่อผู้รับทุนซ้ำ ")
                    ret = False
                End If
            Next
        End If
        'Admincost
        Dim DTProjectAdministrativeCost As DataTable = UCProjectAdministrativeCost.ComponentDT
        For i As Integer = 0 To DTProjectAdministrativeCost.Rows.Count - 1
            Dim id As String = DTProjectAdministrativeCost.Rows(i)("id").ToString
            Dim date_payment As String = DTProjectAdministrativeCost.Rows(i)("date_payment").ToString
            Dim Description As String = DTProjectAdministrativeCost.Rows(i)("Description").ToString
            Dim amount As String = DTProjectAdministrativeCost.Rows(i)("amount").ToString

            If date_payment = "" Then
                alertmsg("กรุณาระบุวันที่")
            End If

            Dim _date_payment As String = ""

            Try
                If IsDBNull(DTProjectAdministrativeCost.Rows(i)("date_payment")) Then
                    alertmsg("กรุณาตรวจสอบวันที่")
                    ret = False

                Else
                    _date_payment = Convert.ToDateTime(DTProjectAdministrativeCost.Rows(i)("date_payment")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                'Converter.StringToDate(date_payment, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
            End Try

            If Description = "" Then
                alertmsg("กรุณาระบุรายละเอียด")
            End If

            If amount = 0 Then
                alertmsg("กรุณาระบุจำนวนเงิน")
                ret = False
            End If

            Dim tempdr() As DataRow
            tempdr = DTProjectAdministrativeCost.Select("date_payment='" & Converter.StringToDate(_date_payment.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) & "' and id <> '" & id & "'")
            If tempdr.Length > 0 Then
                alertmsg("วันที่จ่ายเงินซ้ำ ")
                ret = False
            End If
        Next

        Return ret
    End Function

#End Region

    Private Function GetRecipientPerson() As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("activity_id")
            .Columns.Add("recipient_id")
        End With

        Dim dr As DataRow
        For Each Item As ListItem In ctlSelectRecipient.Items
            If Item.Selected Then
                dr = dt.NewRow
                dr("recipient_id") = Item.Value
                dt.Rows.Add(dr)
            End If
        Next
        Return dt
    End Function

    Private Function GetRecipientCountry() As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("activity_id")
            .Columns.Add("country_id")
        End With

        Dim dr As DataRow
        For Each Item As ListItem In ctlSelectRecipientCountry.Items
            If Item.Selected Then
                dr = dt.NewRow
                dr("country_id") = Item.Value
                dt.Rows.Add(dr)
            End If
        Next
        Return dt
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub ddlBudgetYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBudgetYear.SelectedIndexChanged
        If ddlBudgetYear.SelectedIndex > 0 Then
            BL.Bind_DDL_Incoming_Group(ddlBudgetGroup, ddlBudgetYear.SelectedValue)

        Else
            BL.Bind_DDL_Incoming_Group(ddlBudgetGroup, 0)
            ddlBudgetGroup.SelectedIndex = 0
            ddlBudgetSub.SelectedIndex = 0

        End If
        'ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)

        BL.Bind_DDL_Incoming_Sub(ddlBudgetSub, 0, 0)


    End Sub

    Private Sub ddlBudgetGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBudgetGroup.SelectedIndexChanged
        If ddlBudgetGroup.SelectedIndex > 0 Then
            BL.Bind_DDL_Incoming_Sub(ddlBudgetSub, ddlBudgetYear.SelectedValue, ddlBudgetGroup.SelectedValue)
        Else
            ddlBudgetSub.SelectedIndex = 0
        End If
    End Sub

    Private Sub ddlSector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSector.SelectedIndexChanged
        BL.Bind_DDL_Perpose(ddlSubSector, ddlSector.SelectedValue)
    End Sub

    Protected Sub lblGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Dim value As String = CType(sender, DropDownList).SelectedValue
        If ddlSelectReport.SelectedValue = 1 Then
            ddlComponent.Visible = True
            ddlInkind.Visible = False
            chkActive.Visible = True
            Label1.Text = "<p class=""pull-right"">ประกาศรับสมัครผู้รับทุน :</p>"
            traid.Visible = True
        ElseIf ddlSelectReport.SelectedValue = 2 Then
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            chkActive.Checked = False
            Label1.Text = ""
            traid.Visible = True
        Else
            ddlComponent.Visible = False
            ddlInkind.Visible = True
            chkActive.Visible = False
            Label1.Text = ""
            traid.Visible = False
        End If
    End Sub

    Private Sub btnAddRow1_Click(sender As Object, e As EventArgs) Handles btnAddRow1.Click
        UCProjectAdministrativeCost.btnAdd_Click(Nothing, Nothing)
    End Sub

    Private Sub AddExpense_Click(sender As Object, e As EventArgs) Handles btnApply.Click


        Try
            '## Validate
            If ValidateAct() = False Then
                Exit Sub
            End If

            If Session("status") = "add" Then
                '##tabActivity
                '#TabActActDetail

            End If

            Dim lnqActivity As New TbActivityLinqDB
            With lnqActivity
                .ID = lblActivityID.Text
                .ACTIVITY_NAME = txtActName.Text
                .DESCRIPTION = txtActDescription.Text

                If ddlSector.SelectedValue <> "" Then
                    .SECTOR_ID = ddlSector.SelectedValue
                End If
                If ddlSubSector.SelectedValue <> "" Then
                    .SUB_SECTOR_ID = ddlSubSector.SelectedValue
                End If

                .PARENT_ID = lblParentActivityID.Text
                .PROJECT_ID = lblProjectID.Text
                .NOTIFY = IIf(chkActive.Checked, "Y", "N").ToString
                .ACTIVE_STATUS = "1"
                If RadioSelectRe.SelectedValue = 1 Then
                    .RECIPIENT_TYPE = "G"
                ElseIf RadioSelectRe.SelectedValue = 2 Then
                    .RECIPIENT_TYPE = "R"
                ElseIf RadioSelectRe.SelectedValue = 3 Then
                    .RECIPIENT_TYPE = "C"
                End If
            End With

            '#TabActExpense
            Dim DTBudget As New DataTable
            With DTBudget
                .Columns.Add("budget_year")
                .Columns.Add("budget_group_id")
                .Columns.Add("budget_sub_id")
                .Columns.Add("amount")
            End With
            Dim dr As DataRow
            dr = DTBudget.NewRow
            dr("budget_year") = ddlBudgetYear.SelectedValue
            dr("budget_group_id") = ddlBudgetGroup.SelectedValue
            dr("budget_sub_id") = ddlBudgetSub.SelectedValue
            dr("amount") = txtAmount.Text
            DTBudget.Rows.Add(dr)

            With lnqActivity
                '.ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy")
                '.ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy")

                .ACTUAL_START = Converter.StringToDate(txtActualActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text



                If txtPlanActStart.Text <> "" Then
                    Try
                        .PLAN_START = Converter.StringToDate(txtPlanActStart.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If

                If txtplanActEnd.Text <> "" Then
                    Try
                        .PLAN_END = Converter.StringToDate(txtplanActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
                If txtActualActEnd.Text <> "" Then
                    Try
                        .ACTUAL_END = Converter.StringToDate(txtActualActEnd.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    Catch ex As Exception
                        alertmsg("กรุณาตรวจสอบวันที่")
                        Exit Sub
                    End Try

                End If
            End With

            '#TabActReceipent
            Dim DTRecipientPerson As DataTable = GetRecipientPerson()
            Dim DTRecipentCountry As DataTable = GetRecipientCountry()

            With lnqActivity
                .BENEFICIARY = txtBenificiary.Text
            End With

            Dim DT_Administrative_Cost As New DataTable
            DT_Administrative_Cost = UCProjectAdministrativeCost.ComponentDT
            Dim DT_ProjectCountryRe As New DataTable
            DT_ProjectCountryRe = UCProjectCountryRe.ComponentDT
            Dim DT_UCProjectCountry As New DataTable
            DT_UCProjectCountry = UCProjectCountry.CountryDT
            Dim DT_ProjectRecipient As New DataTable
            DT_ProjectRecipient = UCprojectRecipience.RecipienceDT


            If RadioSelectRe.SelectedValue = 1 Then
                If DT_ProjectCountryRe.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 2 Then
                If DT_ProjectRecipient.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุผู้รับทุน")
                    Exit Sub
                End If
            ElseIf RadioSelectRe.SelectedValue = 3 Then
                If DT_UCProjectCountry.Rows.Count = 0 Then
                    alertmsg("กรุณาระบุประเทศที่รับทุน")
                    Exit Sub
                End If

            End If

            If ddlSelectReport.SelectedValue = 0 Then
                alertmsg("กรุณาระบุประเภทความช่วยเหลือ")
                Exit Sub
            End If

            '#ถ้าเป็นComponent
            If ddlSelectReport.SelectedValue = 1 Then
                '#SetComponent
                If ddlComponent.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTComponent As New DataTable
                With DTComponent
                    .Columns.Add("component_id")
                End With
                Dim drcom As DataRow
                drcom = DTComponent.NewRow
                drcom("component_id") = ddlComponent.SelectedValue
                DTComponent.Rows.Add(drcom)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, DTComponent, Nothing, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If

                '#ถ้าเป็นInkind
            ElseIf ddlSelectReport.SelectedValue = 2 Then
                '#SetInkind
                If ddlInkind.SelectedValue = "" Then
                    alertmsg("กรุณาระบุประเภทความช่วยเหลือย่อย")
                    Exit Sub
                End If
                Dim DTInkind As New DataTable
                With DTInkind
                    .Columns.Add("inkind_id")
                End With
                Dim drik As DataRow
                drik = DTInkind.NewRow
                drik("inkind_id") = ddlInkind.SelectedValue
                DTInkind.Rows.Add(drik)

                Dim ret As New ProcessReturnInfo
                ret = BL.SaveProjectActivity(lnqActivity, DTBudget, Nothing, DTInkind, DTRecipientPerson, DTRecipentCountry, Nothing, Nothing, UserName, DT_ProjectCountryRe, Nothing, DT_Administrative_Cost, DT_ProjectRecipient, DT_UCProjectCountry, Nothing)
                If ret.IsSuccess Then
                    '--
                    lblActivityID.Text = ret.ID
                Else
                    alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
                End If
            End If


            '---Save Template---
            Dim Sql As String = " select * from TB_Expense_Template_Activity where Activity_id =" & lblActivityID.Text
            Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            Dim DR_Temp As DataRow

            If (DT.Rows.Count = 0) Then
                DR_Temp = DT.NewRow
                DR_Temp("id") = BL.GetNew_ID("TB_Expense_Template_Activity", "id")
                DR_Temp("Activity_id") = lblActivityID.Text
                DT.Rows.Add(DR_Temp)
            Else
                DR_Temp = DT.Rows(0)
            End If

            DR_Temp("template_id") = lbltemplate.SelectedValue

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
            Catch ex As Exception
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

                Exit Sub
            End Try


        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try


        If lblActivityID.Text <> "0" Then
            '-----Clear Dialog
            txtPaymentStartDate.Text = ""
            txtPaymentEndDate.Text = ""
            pnlAddRound.Visible = True
        End If

    End Sub

    Sub Authorize()
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnEdit As LinkButton = DirectCast(rptList.Items(i).FindControl("btnEdit"), LinkButton)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)


        Next




    End Sub

    Private Sub BindList(activity_id As Long)

        Dim DT As DataTable = BL.GetList_Expense2(activity_id)


        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            Dim SumSum As Decimal = lblBudget_Sum.Text - lblDisbursement_Sum.Text
            lblpayTotalSum.Text = SumSum.ToString("#,##0.00")

            lbltemplate.Enabled = False
            'Dim A As Long = txtAmount.Text - lblBudget_Sum.Text
            'txtbutged.Text = A.ToString("#,##0.00")
            pnlFooter.Visible = True


            '---Compare---
            If Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")) > 0 Then
                If txtAmount.Text <> lblBudget_Sum.Text Then
                    lblError_compare.Text = "ตรวจสอบข้อมูลงบประมาณทั้งหมด (total budget) กับ งบประมาณ (Budget) แผนการเบิกจ่าย ให้ถูกต้อง"
                    tr_Error_compare.Visible = True
                Else
                    tr_Error_compare.Visible = False
                End If
            Else
                tr_Error_compare.Visible = False
            End If
        Else
            tr_Error_compare.Visible = False
            lblError_compare.Text = ""


            lbltemplate.Enabled = True
            txtbutged.Text = txtAmount.Text
            pnlFooter.Visible = False
        End If


        rptList.DataSource = DT
        rptList.DataBind()

        txtbutged.Enabled = False
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lbldatePlan As Label = DirectCast(e.Item.FindControl("lbldatePlan"), Label)
        Dim lbldateActual As Label = DirectCast(e.Item.FindControl("lbldateActual"), Label)
        Dim lblpayActual As Label = DirectCast(e.Item.FindControl("lblpayActual"), Label)
        Dim lblpayPlan As Label = DirectCast(e.Item.FindControl("lblpayPlan"), Label)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblpayTotal As Label = DirectCast(e.Item.FindControl("lblpayTotal"), Label)
        Dim tdPayActual As HtmlTableCell = e.Item.FindControl("tdPayActual")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")

        Dim planpay As Long = e.Item.DataItem("Pay_Amount_Plan").ToString
        Dim actualpay As Long = e.Item.DataItem("Pay_Amount_Actual").ToString

        lblID.Text = e.Item.DataItem("id").ToString
        lbldatePlan.Text = e.Item.DataItem("Payment_Date_Start").ToString
        lbldateActual.Text = e.Item.DataItem("Payment_Date_End").ToString
        lblpayActual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        lblpayPlan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")


        Dim Sum As Decimal = lblpayPlan.Text - lblpayActual.Text

        If Sum < 0 Then
            tdPayActual.Attributes.Add("style", "background-color:red; text-align:right;")

            td2.Attributes.Add("style", "background-color:red; text-align:right;")
        End If

        lblpayTotal.Text = Sum.ToString("#,##0.00")
    End Sub


    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand


        If e.CommandName = "cmdEdit" Then
            '--Save Header-- 
            Validate_Recipient()


            If lblActivityID.Text <> "0" Then

                Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "edit" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

            End If
        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.DeleteExpenseHeaderDetail(e.CommandArgument)
            If ret.IsSuccess Then
                alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
                BindList(lblActivityID.Text)
            Else
                alertmsg(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdPay" Then
            Response.Redirect("frmRename.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "payinac" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

        ElseIf e.CommandName = "cmdView" Then
            Validate_Recipient()
            If lblActivityID.Text <> "0" Then
                Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & " &mode=" & "view" & "&HeaderId=" & e.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)
            End If

        End If
    End Sub

    Protected Sub hdnRadioButtonSelector_Click(sender As Object, e As EventArgs) Handles hdnRadioButtonSelector.Click

        '----ต้องเคลีบร์ข้อมูลการเบิกจ่ายก่อน  เนื่องจากมีการเปลี่ยนกลุ่มของผู้รับทุน

        '--เคลียร์ Expense--
        '--เคลียร์ ผู้รับทุน
        BL.DROP_EXPENSE_ACTIVITY(lblActivityID.Text)
        Old_Recipient_Type = RadioSelectRe.SelectedValue

        BindList(lblActivityID.Text)
        SetGroupUserControl()
    End Sub

    Private Sub RadioSelectRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioSelectRe.SelectedIndexChanged


        '---เชคเงื่อนไข ถ้า ข้อมีข้อมูลการเบิกจ่ายแล้ว ให้แจ้งเตือนเมื่อเปลี่ยน
        Dim DT As New DataTable
        DT = BL.GetList_Expense2(lblActivityID.Text)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "confirm", "confirmSelection();", True)

        Else
            BL.DROP_EXPENSE_ACTIVITY(lblActivityID.Text)
            BindList(lblActivityID.Text)
            SetGroupUserControl()

        End If


    End Sub

    Private Sub SetGroupUserControl()
        UCProjectCountryRe.SetDataRpt(lblActivityID.Text)
        UCprojectRecipience.SetDataRpt(lblActivityID.Text)
        UCProjectCountry.SetDataRpt(lblActivityID.Text)

        If RadioSelectRe.SelectedValue = "1" Then
            UCProjectCountryRe.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "2" Then
            UCprojectRecipience.Visible = True
            UCProjectCountryRe.Visible = False
            UCProjectCountry.Visible = False
        ElseIf RadioSelectRe.SelectedValue = "3" Then
            UCProjectCountry.Visible = True
            UCprojectRecipience.Visible = False
            UCProjectCountryRe.Visible = False
        End If

    End Sub


    Private Sub lnkDialogCancel_Click(sender As Object, e As EventArgs) Handles lnkDialogCancel.Click
        pnlAddRound.Visible = False
    End Sub

    Private Sub lnkDialogSave_Click(sender As Object, e As EventArgs) Handles lnkDialogSave.Click

        '-----Save Header

        If txtPaymentStartDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่เริ่มต้น');", True)
            Exit Sub
        End If
        If txtPaymentEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่สิ้นสุด');", True)
            Exit Sub
        End If


        If lblActivityID.Text <> "0" Then

            Dim lnqAEH As New TbActivityExpenseHeaderLinqDB
            With lnqAEH
                .ID = 0
                .ACTIVITY_ID = lblActivityID.Text
                .PAYMENT_DATE_START = Converter.StringToDate(txtPaymentStartDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                .PAYMENT_DATE_END = Converter.StringToDate(txtPaymentEndDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End With

            Dim tem_id As Long = lbltemplate.SelectedValue
            Dim HID As Double = BL.SaveHeaderID(lnqAEH, UserName, tem_id)
            Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID.Text & "&mode=" & "Add" & "&HeaderId=" & HID & "&PID=" & lblProjectID.Text & "&type=" & ProjectType)

        End If
    End Sub


    Private Sub btn_ReturnFalse_Click(sender As Object, e As EventArgs) Handles btn_ReturnFalse.Click
        RadioSelectRe.SelectedValue = Old_Recipient_Type
        Old_Recipient_Type = RadioSelectRe.SelectedValue
    End Sub

    '--Dialog Advance--
    Private Sub btnAddBorrower_Click(sender As Object, e As EventArgs) Handles btnAddBorrower.Click
        If ddlDialog_TypeBorrower.SelectedValue = "" Then
            alertmsg("กรุณาเลือกประเภทผู้ยืม")
            Exit Sub
        End If
        dialog_Advance_OU.Visible = True

        '--ซ่อนคอลัมน์ตามประเภท 
        divCountry.Visible = True
        divOrg.Visible = True
        divPerson.Visible = True
        thCountry.Visible = True
        thOrg.Visible = True
        thPerson.Visible = True
        thOrg_Under.Visible = True
        Select Case ddlDialog_TypeBorrower.SelectedValue.ToString()
            Case "TB_OU_Country"
                divCountry.Visible = True
                divOrg.Visible = False
                divPerson.Visible = False
                thCountry.Visible = True
                thOrg.Visible = False
                thPerson.Visible = False
                thOrg_Under.Visible = False
            Case "TB_OU_Organize"
                divCountry.Visible = True
                divOrg.Visible = True
                divPerson.Visible = False
                thCountry.Visible = True
                thOrg.Visible = True
                thPerson.Visible = False
                thOrg_Under.Visible = True
            Case "TB_OU_Person"
                divCountry.Visible = True
                divOrg.Visible = True
                divPerson.Visible = True
                thCountry.Visible = True
                thOrg.Visible = True
                thPerson.Visible = True
                thOrg_Under.Visible = False
        End Select
        '-----ดึงรายชื่อตาม OU ที่ยืม
        BindListBorrower()


    End Sub
    Private Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptAdvance_OU
    End Sub

    Private Sub BindListBorrower()
        Dim Sql As String = " select DISTINCT * from vw_Overview_OU where TB_NAME ='" & ddlDialog_TypeBorrower.SelectedValue.ToString & "'" + Environment.NewLine
        Sql &= "  AND node_id_OUTB_OU_Country IS NOT NULL " + Environment.NewLine
        If (txtdialog_Advance_OU_SearchCountry.Text <> "") Then
            Sql &= " AND ( name_th_OUTB_OU_Country Like '%" & txtdialog_Advance_OU_SearchCountry.Text & "%'  OR name_en_OUTB_OU_Country Like '%" & txtdialog_Advance_OU_SearchCountry.Text & "%'  )  " + Environment.NewLine
        End If

        If (txtdialog_Advance_OU_SearchOrg.Text <> "") Then
            Sql &= " AND ( name_th_OUTB_OU_Organize Like '%" & txtdialog_Advance_OU_SearchOrg.Text & "%'  OR name_en_OUTB_OU_Organize Like '%" & txtdialog_Advance_OU_SearchOrg.Text & "%'     " + Environment.NewLine
            Sql &= " OR name_th Like '%" & txtdialog_Advance_OU_SearchOrg.Text & "%'  OR name_en Like '%" & txtdialog_Advance_OU_SearchOrg.Text & "%'  )  " + Environment.NewLine

        End If

        If (txtdialog_Advance_OU_SearchPerson.Text <> "") Then
            Sql &= " AND ( name_th_OUTB_OU_Person Like '%" & txtdialog_Advance_OU_SearchPerson.Text & "%'  OR name_en_OUTB_OU_Person Like '%" & txtdialog_Advance_OU_SearchPerson.Text & "%'  )  " + Environment.NewLine
        End If
        Sql &= " ORDER BY  name_th_OUTB_OU_Country,name_th_OUTB_OU_Organize,name_th_OUTB_OU_Person"
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("_DT_Borrower") = DT
        Pager.SesssionSourceName = "_DT_Borrower"
        Pager.RenderLayout()

    End Sub

    Private Sub lnkAialog_Advance_OU_Close_Click(sender As Object, e As EventArgs) Handles lnkAialog_Advance_OU_Close.Click
        dialog_Advance_OU.Visible = False
    End Sub

    Private Sub rptAdvance_OU_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAdvance_OU.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbldialog_Advance_OU_Country As Label = e.Item.FindControl("lbldialog_Advance_OU_Country")
        Dim lbldialog_Advance_OU_Org As Label = e.Item.FindControl("lbldialog_Advance_OU_Org")
        Dim lbldialog_Advance_OU_Person As Label = e.Item.FindControl("lbldialog_Advance_OU_Person")
        Dim lbldialog_Advance_OU_Org_Under As Label = e.Item.FindControl("lbldialog_Advance_OU_Org_Under")
        Dim btndialog_Advance_OU_Select As Button = e.Item.FindControl("btndialog_Advance_OU_Select")
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
        Dim td3 As HtmlTableCell = e.Item.FindControl("td3")
        Dim td4 As HtmlTableCell = e.Item.FindControl("td4")


        lbldialog_Advance_OU_Country.Text = e.Item.DataItem("name_th_OUTB_OU_Country").ToString()
        lbldialog_Advance_OU_Org.Text = e.Item.DataItem("name_th_OUTB_OU_Organize").ToString()
        lbldialog_Advance_OU_Person.Text = e.Item.DataItem("name_th_OUTB_OU_Person").ToString()
        lbldialog_Advance_OU_Org_Under.Text = e.Item.DataItem("name_th").ToString()

        'For i As Integer = 1 To 3
        '    Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
        '    td.Attributes("onClick") = "document.getElementById('" & btndialog_Advance_OU_Select.ClientID & "').click();"
        'Next
        td1.Attributes("onClick") = "document.getElementById('" & btndialog_Advance_OU_Select.ClientID & "').click();"
        td2.Attributes("onClick") = "document.getElementById('" & btndialog_Advance_OU_Select.ClientID & "').click();"
        td3.Attributes("onClick") = "document.getElementById('" & btndialog_Advance_OU_Select.ClientID & "').click();"
        td4.Attributes("onClick") = "document.getElementById('" & btndialog_Advance_OU_Select.ClientID & "').click();"

        btndialog_Advance_OU_Select.CommandArgument = e.Item.DataItem("node_id")

        td1.Visible = True
        td2.Visible = True
        td3.Visible = True
        td4.Visible = True

        Select Case ddlDialog_TypeBorrower.SelectedValue.ToString()
            Case "TB_OU_Country"
                td1.Visible = True
                td2.Visible = False
                td3.Visible = False
                td4.Visible = False
            Case "TB_OU_Organize"
                td1.Visible = True
                td2.Visible = True
                td3.Visible = False
                td4.Visible = True
            Case "TB_OU_Person"
                td1.Visible = True
                td2.Visible = True
                td3.Visible = True
                td4.Visible = False
        End Select

    End Sub
    Private Sub rptAdvance_OU_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptAdvance_OU.ItemCommand
        Dim lbldialog_Advance_OU_Country As Label = e.Item.FindControl("lbldialog_Advance_OU_Country")
        Dim lbldialog_Advance_OU_Org As Label = e.Item.FindControl("lbldialog_Advance_OU_Org")
        Dim lbldialog_Advance_OU_Person As Label = e.Item.FindControl("lbldialog_Advance_OU_Person")
        Dim lbldialog_Advance_OU_Org_Under As Label = e.Item.FindControl("lbldialog_Advance_OU_Org_Under")
        Select Case e.CommandName
            Case "select"
                lblDialogAdvance_Borrower_ID.Text = e.CommandArgument
                Select Case ddlDialog_TypeBorrower.SelectedValue.ToString()
                    Case "TB_OU_Country"
                        lblDialogAdvance_Borrower.Text = lbldialog_Advance_OU_Country.Text
                    Case "TB_OU_Organize"
                        If lbldialog_Advance_OU_Org_Under.Text <> "" Then
                            lblDialogAdvance_Borrower.Text = lbldialog_Advance_OU_Org_Under.Text
                        Else
                            lblDialogAdvance_Borrower.Text = lbldialog_Advance_OU_Org.Text
                        End If

                    Case "TB_OU_Person"
                        lblDialogAdvance_Borrower.Text = lbldialog_Advance_OU_Person.Text
                End Select
                pnlAddAdvance.Visible = True
                dialog_Advance_OU.Visible = False

        End Select
    End Sub

    Private Sub txtdialog_Advance_OU_Search_TextChanged(sender As Object, e As EventArgs) Handles txtdialog_Advance_OU_SearchCountry.TextChanged, txtdialog_Advance_OU_SearchOrg.TextChanged, txtdialog_Advance_OU_SearchPerson.TextChanged
        BindListBorrower()
    End Sub


    '--Dialog Advance--
    Private Sub btnAddAdvance_Click(sender As Object, e As EventArgs) Handles btnAddAdvance.Click
        trDue_Date.Visible = False
        '---clear Textbox--
        txtDialogCash_Back_Date.Text = ""
        lblDialogAdvance_Borrower.Text = ""
        lblDialogAdvance_Borrower_ID.Text = ""
        txtDialogAdvance_No.Text = "" '--เลขที่เอกสาร
        txtDialogAdvance_Detail.Text = "" '--รายละเอียด
        txtDialogAdvance_Amount.Text = ""
        txtDialogDue_Date.Text = ""
        txtdialog_Advance_OU_SearchCountry.Text = ""
        txtdialog_Advance_OU_SearchOrg.Text = ""
        txtdialog_Advance_OU_SearchPerson.Text = ""

        ImplementJavaMoneyText(txtDialogAdvance_Amount)

        pnlAddAdvance.Visible = True
    End Sub


    Private Sub lnkDialogAdvance_Save_Click(sender As Object, e As EventArgs) Handles lnkDialogAdvance_Save.Click
        '--เพิ่มรายการ TB_Activity_Advance---


        If txtDialogCash_Back_Date.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันที่ยืม');", True)
                Exit Sub
            End If

        If trDue_Date.Visible = True Then
            If txtDialogDue_Date.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุวันครบกำหนด');", True)
                Exit Sub
            End If
        End If

        If lblDialogAdvance_Borrower_ID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุผู้ยืม');", True)
            Exit Sub
        End If
        If txtDialogAdvance_Detail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุรายละเอียด');", True)
            Exit Sub
        End If
        If txtDialogAdvance_No.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุเลขที่เอกสาร');", True)
            Exit Sub
        End If
        If txtDialogAdvance_No.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('โปรดระบุจำนวนเงินยืมทดรอง');", True)
            Exit Sub
        End If

        '---Save Header---
        Dim Sql As String = " select * from TB_Activity_Advance where 0=1 "
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR_Temp As DataRow
        Dim New_ID As Integer = BL.GetNew_ID("TB_Activity_Advance", "Advance_ID")
        If (DT.Rows.Count = 0) Then
            DR_Temp = DT.NewRow
            DR_Temp("Advance_ID") = New_ID
            DR_Temp("Borrower_Node_ID") = lblDialogAdvance_Borrower_ID.Text

            DR_Temp("Project_ID") = lblProjectID.Text
            DR_Temp("Activity_id") = lblActivityID.Text

            DR_Temp("Advance_Date") = Converter.StringToDate(txtDialogCash_Back_Date.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            If trDue_Date.Visible Then
                DR_Temp("Due_Date") = Converter.StringToDate(txtDialogDue_Date.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If
            DR_Temp("Advance_Detail") = txtDialogAdvance_Detail.Text
            DR_Temp("Advance_No") = txtDialogAdvance_No.Text
            DR_Temp("Amount") = txtDialogAdvance_Amount.Text.Replace(",", "")

            DT.Rows.Add(DR_Temp)
        End If
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

            Exit Sub
        End Try

        '----เปิดหน้าใหม่สำหรับรายละเอียด

        pnlAddAdvance.Visible = False
        Response.Redirect("frmActivityAdvance.aspx?ActivityID=" & lblActivityID.Text & "&Advance_ID=" & New_ID & "&PID=" & lblProjectID.Text & "&type=" & ProjectType & "&mode=" & mode)



    End Sub

    Private Sub lnkDialogAdvance_Cancel_Click(sender As Object, e As EventArgs) Handles lnkDialogAdvance_Cancel.Click
        pnlAddAdvance.Visible = False
    End Sub

    '====================================
    Private Sub BindListAdvance(activity_id As Long)
        Dim Sql As String = " SELECT *  ,ISNULL(Amount-(SUM_Expense+Cash_Back),0) Balance    FROM ( "
        Sql &= "  Select  Advance_ID,Project_ID,Activity_id,Borrower_Node_ID,dbo.GetFullThaiDate(Advance_Date) Advance_Date,dbo.GetFullThaiDate(Due_Date) Due_Date,Advance_Detail,Advance_No " + Environment.NewLine
        Sql &= "  ,Amount  ,vw_ou.name_th,vw_ou.name_en " + Environment.NewLine

        Sql &= "  , ISNULL((SELECT SUM(Amount) FROM TB_Activity_Advance_Expense WHERE Advance_ID =TB_Activity_Advance.Advance_ID ),0) SUM_Expense	--ยอดใช้จ่าย " + Environment.NewLine
        Sql &= "  , ISNULL(Cash_Back,0) Cash_Back  --เงินคืน " + Environment.NewLine

        Sql &= "  From TB_Activity_Advance " + Environment.NewLine
        Sql &= "  Left Join vw_ou ON vw_ou.node_id =TB_Activity_Advance.Borrower_Node_ID " + Environment.NewLine
        Sql &= "  WHERE Activity_id=" & activity_id
        Sql &= "  ) AS TB   ORDER BY Advance_Date "
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If (DT.Rows.Count > 0) Then
            lblSum_Advance_Amount.Text = Convert.ToDecimal(DT.Compute("SUM(Amount)", "")).ToString("#,##0.00")
            lblSum_Advance_Expense.Text = Convert.ToDecimal(DT.Compute("SUM(SUM_Expense)", "")).ToString("#,##0.00")
            lblSum_Advance_Case_Back.Text = Convert.ToDecimal(DT.Compute("SUM(Cash_Back)", "")).ToString("#,##0.00")
            lblSum_Advance_Balance.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")
        End If

        rptAdvance.DataSource = DT
        rptAdvance.DataBind()





    End Sub

    Private Sub rptAdvance_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAdvance.ItemDataBound

        Dim lblBorrow_Date As Label = e.Item.FindControl("lblBorrow_Date")
        Dim lblBorrower_Node_Name As Label = e.Item.FindControl("lblBorrower_Node_Name")
        Dim lblAdvance_No As Label = e.Item.FindControl("lblAdvance_No")
        Dim lblAdvance_Amount As Label = e.Item.FindControl("lblAdvance_Amount")
        Dim lblAdvance_Expense As Label = e.Item.FindControl("lblAdvance_Expense")
        Dim lblAdvance_Case_Back As Label = e.Item.FindControl("lblAdvance_Case_Back")
        Dim lblAdvance_Balance As Label = e.Item.FindControl("lblAdvance_Balance")
        Dim btnEdit As LinkButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As LinkButton = e.Item.FindControl("btnDelete")
        Dim lblDue_Date As Label = e.Item.FindControl("lblDue_Date")

        lblBorrow_Date.Text = e.Item.DataItem("Advance_Date").ToString
        lblDue_Date.Text = e.Item.DataItem("Due_Date").ToString

        lblBorrower_Node_Name.Text = e.Item.DataItem("name_th").ToString
        lblAdvance_No.Text = e.Item.DataItem("Advance_No").ToString
        If Not IsDBNull(e.Item.DataItem("Amount")) Then
            lblAdvance_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Amount")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("SUM_Expense")) Then
            lblAdvance_Expense.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Expense")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Cash_Back")) Then
            lblAdvance_Case_Back.Text = Convert.ToDecimal(e.Item.DataItem("Cash_Back")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Balance")) Then
            lblAdvance_Balance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


        btnEdit.CommandArgument = e.Item.DataItem("Advance_ID")
        btnDelete.CommandArgument = e.Item.DataItem("Advance_ID")

    End Sub

    Private Sub rptAdvance_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptAdvance.ItemCommand
        Dim btnEdit As LinkButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As LinkButton = e.Item.FindControl("btnDelete")

        Select Case e.CommandName
            Case "cmdEdit"
                Response.Redirect("frmActivityAdvance.aspx?ActivityID=" & lblActivityID.Text & "&Advance_ID=" & btnEdit.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType & "&mode=" & mode)

            Case "cmdDelete"
                Try

                    '-----ลบรายการ Detail ค่าใช้จ่ายเงินยืม
                    Dim Sql As String = " "
                    Sql &= "   DELETE FROM TB_Activity_Advance_Expense "
                    Sql &= "   WHERE  Advance_ID =" & btnEdit.CommandArgument
                    Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
                    Dim DT As New DataTable
                    DA.Fill(DT)

                    '-----ลบรายการ Header 
                    Sql = "   DELETE FROM TB_Activity_Advance "
                    Sql &= "   WHERE  Advance_ID =" & btnEdit.CommandArgument
                    DA = New SqlDataAdapter(Sql, BL.ConnectionString)
                    DT = New DataTable
                    DA.Fill(DT)
                    alertmsg("ลบข้อมูลเรียบร้อยแล้ว")
                    BindListAdvance(lblActivityID.Text)
                Catch ex As Exception
                    alertmsg(ex.Message.ToString())
                End Try
            Case "cmdView"
                Response.Redirect("frmActivityAdvance.aspx?ActivityID=" & lblActivityID.Text & "&Advance_ID=" & btnEdit.CommandArgument & "&PID=" & lblProjectID.Text & "&type=" & ProjectType & "&mode=" & mode)

        End Select


    End Sub



    '------New Person----

    Public Sub ClearData_Dialog_New_Person()

        txtPersonNameEN.Text = ""
        txtPersonNameTH.Text = ""
        txtPersonIDCard.Text = ""
        txtPersonPassportID.Text = ""
        txtPersonBirthDay.Text = ""
        txtPersonTelephone.Text = ""
        txtPersonEmail.Text = ""
        BL.Bind_DDL_Country(ddlExecutingCountry)
        ddlExecutingCountry.SelectedIndex = 0
        ImplementJavaOnlyNumberText(txtPersonIDCard, "left")
        ImplementJavaOnlyNumberText(txtPersonPassportID, "left")
        BL.Bind_DDL_OrganizeFromCountry(ddlExecutingOrganize, ddlExecutingCountry.SelectedValue)
        ddlExecutingOrganize.SelectedIndex = 0


    End Sub

    Private Sub lnkAdd_NewPerson_Click(sender As Object, e As EventArgs) Handles lnkAdd_NewPerson.Click
        ClearData_Dialog_New_Person()
        dialog_New_Person.Visible = True
    End Sub

    Private Sub btnNewPersonSave_Click(sender As Object, e As EventArgs) Handles btnNewPersonSave.Click
        '----Validate----
        If txtPersonNameTH.Text = "" Then
            txtPersonNameTH.Focus()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณากรอกชื่อภาษาไทย');", True)
            Exit Sub
        End If

        If txtPersonNameEN.Text = "" Then
            txtPersonNameEN.Focus()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณากรอกชื่อภาษาอังกฤษ');", True)
            Exit Sub
        End If

        'If txtPersonIDCard.Text = "" Then
        '    txtPersonIDCard.Focus()
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณากรอก Id Card');", True)
        '    Exit Sub
        'End If
        If txtPersonIDCard.Text <> "" Then
            If txtPersonIDCard.Text.Length < 13 Then
                txtPersonIDCard.Focus()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณากรอก Id Card 13 หลัก');", True)
                Exit Sub
            End If

            '----เชคบัตรประชาชนซ้ำ---
            Dim Sql_Dup As String = " select * from TB_OU_Person where id_card='" & txtPersonIDCard.Text & "' "
            Dim DA_Dup As New SqlDataAdapter(Sql_Dup, BL.ConnectionString)
            Dim DT_Dup As New DataTable
            DA_Dup.Fill(DT_Dup)
            If DT_Dup.Rows.Count > 0 Then
                txtPersonIDCard.Focus()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('มีเลขบัตรประชาชนซ้ำ กรุณาตรวจสอบ');", True)
                Exit Sub
            End If

        End If



        '------------------------------------------------
        If txtPersonPassportID.Text <> "" Then
            If txtPersonPassportID.Text.Length < 20 Then
                txtPersonPassportID.Focus()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณากรอกเลข Passport 20 หลัก');", True)
                Exit Sub
            End If

            '----เชค Passport ซ้ำ---
            Dim Sql_Dup As String = " select * from TB_OU_Person where passport_no='" & txtPersonPassportID.Text & "' "
            Dim DA_Dup As New SqlDataAdapter(Sql_Dup, BL.ConnectionString)
            Dim DT_Dup As New DataTable
            DA_Dup.Fill(DT_Dup)
            If DT_Dup.Rows.Count > 0 Then
                txtPersonIDCard.Focus()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('มีเลข Passport ซ้ำ กรุณาตรวจสอบ');", True)
                Exit Sub
            End If
        End If

        If ddlExecutingCountry.SelectedIndex = 0 Then
            ddlExecutingCountry.Focus()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณาเลือกประเทศ');", True)
            Exit Sub
        End If
        If ddlExecutingOrganize.SelectedIndex = 0 Then
            ddlExecutingOrganize.Focus()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('กรุณาเลือกหน่วยงาน');", True)
            Exit Sub
        End If

        '---Save ลงตาราง Preson  เป็น จนท. Active
        Dim Sql As String = " select * from TB_OU_Person where 0=1 "
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR_Temp As DataRow
        If (DT.Rows.Count = 0) Then
            DR_Temp = DT.NewRow
            DR_Temp("node_id") = BL.GetOUNodeID()
            'DR_Temp("prefix_th_id") =
            DR_Temp("name_th") = txtPersonNameTH.Text.Trim
            'DR_Temp("prefix_en_id") =
            DR_Temp("name_en") = txtPersonNameEN.Text.Trim
            DR_Temp("parent_id") = ddlExecutingOrganize.SelectedValue
            'DR_Temp("url") =
            DR_Temp("id_card") = txtPersonIDCard.Text
            If txtPersonPassportID.text <> "" Then
                DR_Temp("passport_no") = txtPersonPassportID.Text
            Else
                DR_Temp("passport_no") = DBNull.Value
            End If
            DR_Temp("expired_date_passport") = DBNull.Value
            If txtPersonBirthDay.Text <> "" Then
                DR_Temp("birthdate") = Converter.StringToDate(txtPersonBirthDay.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

            End If
            DR_Temp("created_by") = UserName
            DR_Temp("created_date") = Now
            DR_Temp("person_type") = 0
            DR_Temp("active_status") = "Y"
            DT.Rows.Add(DR_Temp)
        End If
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            '---contact---
            Sql = " select * from TB_OU_Person where id_card='" & txtPersonIDCard.Text & "' "
            DA = New SqlDataAdapter(Sql, BL.ConnectionString)
            Dim DT_Person = New DataTable
            DA.Fill(DT_Person)
            If DT_Person.Rows.Count = 1 Then
                Sql = " select * from TB_OU_Contact where 0=1"
                DA = New SqlDataAdapter(Sql, BL.ConnectionString)
                DT = New DataTable
                DA.Fill(DT)
                DR_Temp = DT.NewRow
                DR_Temp("parent_type") = 0
                DR_Temp("parent_id") = DT_Person.Rows(0).Item("node_id").ToString()
                DR_Temp("telephone") = txtPersonTelephone.Text.Trim
                DR_Temp("email") = txtPersonEmail.Text.Trim
                DT.Rows.Add(DR_Temp)
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)
                lblDialogAdvance_Borrower_ID.Text = DT_Person.Rows(0).Item("node_id").ToString()
            End If

            '---
            lblDialogAdvance_Borrower.Text = txtPersonNameTH.Text

            pnlAddAdvance.Visible = True
            dialog_Advance_OU.Visible = False

            dialog_New_Person.Visible = False
        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")

            Exit Sub
        End Try






    End Sub

    Private Sub btnNewPersonCancel_Click(sender As Object, e As EventArgs) Handles btnNewPersonCancel.Click
        dialog_New_Person.Visible = False
    End Sub

    Private Sub ddlExecutingCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlExecutingCountry.SelectedIndexChanged
        BL.Bind_DDL_OrganizeFromCountry(ddlExecutingOrganize, ddlExecutingCountry.SelectedValue)
    End Sub

    Private Sub ddlDialog_TypeBorrower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDialog_TypeBorrower.SelectedIndexChanged


        Select Case ddlDialog_TypeBorrower.SelectedValue
            Case "TB_OU_Organize"
                trDue_Date.Visible = False
            Case "TB_OU_Person"
                trDue_Date.Visible = True
            Case Else
                trDue_Date.Visible = False
        End Select
    End Sub


End Class



