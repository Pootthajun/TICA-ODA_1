﻿Imports System.Data

Partial Class usercontrol_UCDisbursementContribution
    Inherits System.Web.UI.UserControl

    Dim BL As New ODAENG

    Public Event EditDisbursement()

    Dim _disbursementDT As DataTable
    Public Property DisbursementDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _disbursementDT = value


            If (_disbursementDT.Rows.Count > 0) Then
                pnlFooter.Visible = True
            Else
                pnlFooter.Visible = False
            End If

            rptList.DataSource = _disbursementDT
            rptList.DataBind()
            txtAmount_TextChanged(Nothing, Nothing)
        End Set
    End Property

    Public ReadOnly Property TotalEditDisbursement As Decimal
        Get
            Return CDec(lblTotal.Text)
        End Get
    End Property

    Private Sub usercontrol_UCDisbursementLoan_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            btnAdd_Click(sender, e)
        End If
    End Sub

    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtTitle As TextBox = DirectCast(rptList.Items(i).FindControl("txtTitle"), TextBox)
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
            'Dim btnDelete As LinkButton = DirectCast(rptList.Items(i).FindControl("btnDelete"), LinkButton)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)

            txtTitle.Enabled = Not IsEnable
            ddlMultilateral.Enabled = Not IsEnable
            txtPaymentDate.Enabled = Not IsEnable
            txtAmount.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub

    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs)
        Dim _total As Decimal = 0.0
        For i As Integer = 0 To rptList.Items.Count - 1
            Try
                Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)
                _total += CDec(txtAmount.Text)
            Catch ex As Exception
            End Try
        Next

        If (rptList.Items.Count > 0) Then
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If


        lblTotal.Text = _total.ToString("#,##0.00")
        RaiseEvent EditDisbursement()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDataFromRpt(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = dt.Rows.Count + 1
        dr("Title") = ""
        dr("Multilateral_ID") = ""
        dr("Payment_Date") = ""
        dr("Amount") = 0.0
        dt.Rows.Add(dr)
        rptList.DataSource = dt
        rptList.DataBind()
    End Sub

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Title")
        dt.Columns.Add("Multilateral_ID")
        dt.Columns.Add("Payment_Date")
        dt.Columns.Add("Amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim lblid As Label = DirectCast(rptList.Items(i).FindControl("lblid"), Label)
            Dim txtTitle As TextBox = DirectCast(rptList.Items(i).FindControl("txtTitle"), TextBox)
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)

            dr = dt.NewRow
            dr("id") = dt.Rows.Count + 1
            dr("Title") = txtTitle.Text
            dr("Multilateral_ID") = ddlMultilateral.SelectedValue

            If txtPaymentDate.Text <> "" Then
                dr("Payment_Date") = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
            Else
                dr("Payment_Date") = ""
            End If

            If txtAmount.Text = "" Then
                dr("Amount") = 0.0
            Else
                dr("Amount") = txtAmount.Text
            End If

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim txtTitle As TextBox = DirectCast(e.Item.FindControl("txtTitle"), TextBox)
        Dim ddlMultilateral As DropDownList = DirectCast(e.Item.FindControl("ddlMultilateral"), DropDownList)
        Dim txtPaymentDate As TextBox = DirectCast(e.Item.FindControl("txtPaymentDate"), TextBox)
        Dim txtAmount As TextBox = DirectCast(e.Item.FindControl("txtAmount"), TextBox)
        'Dim btnDelete As LinkButton = DirectCast(e.Item.FindControl("btnDelete"), LinkButton)
        Dim btnDelete As Button = DirectCast(e.Item.FindControl("btnDelete"), Button)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)

        BL.Bind_DDL_Multilateral(ddlMultilateral)
        BL.SetTextDblKeypress(txtAmount)

        lblID.Text = e.Item.DataItem("id").ToString
        txtTitle.Text = e.Item.DataItem("Title").ToString
        ddlMultilateral.SelectedValue = e.Item.DataItem("Multilateral_ID").ToString

        txtPaymentDate.Text = ""
        If e.Item.DataItem("payment_date").ToString <> "" Then
            txtPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("payment_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        End If

        txtAmount.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        If e.CommandName = "Delete" Then
            Dim _id As String = e.CommandArgument
            Dim dt As DataTable = DeleteDataInRpt(rptList, _id)
            rptList.DataSource = dt
            rptList.DataBind()

            txtAmount_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Function DeleteDataInRpt(rpt As Repeater, id As String) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Title")
        dt.Columns.Add("Multilateral_ID")
        dt.Columns.Add("Payment_Date")
        dt.Columns.Add("Amount")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim lblid As Label = DirectCast(rptList.Items(i).FindControl("lblid"), Label)
            Dim txtTitle As TextBox = DirectCast(rptList.Items(i).FindControl("txtTitle"), TextBox)
            Dim ddlMultilateral As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlMultilateral"), DropDownList)
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtAmount As TextBox = DirectCast(rptList.Items(i).FindControl("txtAmount"), TextBox)

            If lblid.Text <> id Then
                dr = dt.NewRow
                dr("id") = dt.Rows.Count + 1
                dr("Title") = txtTitle.Text
                dr("Multilateral_ID") = ddlMultilateral.SelectedValue

                If txtPaymentDate.Text <> "" Then
                    dr("Payment_Date") = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy")
                Else
                    dr("Payment_Date") = ""
                End If

                If txtAmount.Text = "" Then
                    dr("Amount") = 0.0
                Else
                    dr("Amount") = txtAmount.Text
                End If

                dt.Rows.Add(dr)
            End If
        Next

        Return dt
    End Function


End Class

