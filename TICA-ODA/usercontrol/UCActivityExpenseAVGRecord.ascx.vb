﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class UCActivityExpenseAVGRecord
    Inherits System.Web.UI.UserControl
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Dim status As String
    Dim dtDtial As DataTable
    Dim dtTemplate As DataTable


    Public Event EditBudget()

    Dim _ComponentDT As DataTable
    Public Property ComponentDT As DataTable
        Get
            Return GetDataFromRpt(rptList)
        End Get
        Set(value As DataTable)
            _ComponentDT = value
            If (_ComponentDT.Rows.Count > 0) Then
                'pnlFooter.Visible = True
            Else
                'pnlFooter.Visible = False
            End If

            'rptList.DataSource = _ComponentDT
            'rptList.DataBind()

        End Set
    End Property


    Dim _ExpenstListDT As DataTable
    Public Property ExpenstListDT As DataTable
        Get
            Return GetDataFromRptcol(rptList)
        End Get
        Set(value As DataTable)
            _ExpenstListDT = value
            If (_ExpenstListDT.Rows.Count > 0) Then
                'pnlFooter.Visible = True
            Else
                'pnlFooter.Visible = False
            End If

        End Set
    End Property



    Dim _ActivityID As String
    Public WriteOnly Property ActivityID As String
        Set(value As String)
            _ActivityID = value
        End Set
    End Property



    Dim _HeaderID As String
    Public WriteOnly Property HeaderID As String
        Set(value As String)
            _HeaderID = value
        End Set
    End Property

    Dim _TemplateID As String
    Public WriteOnly Property TemplateID As String
        Set(value As String)
            _TemplateID = value
        End Set
    End Property

    Dim _PID As String
    Public WriteOnly Property PID As String
        Set(value As String)
            _PID = value
        End Set
    End Property



    Public Sub SetToViewMode(IsEnable As Boolean)
        btnAdd.Enabled = Not IsEnable
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim ddlCountry As DropDownList = DirectCast(rptList.Items(i).FindControl("ddlCountry"), DropDownList)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim btnDelete As Button = DirectCast(rptList.Items(i).FindControl("btnDelete"), Button)
            ddlCountry.Enabled = Not IsEnable
            txtExpand.Enabled = Not IsEnable
            btnDelete.Enabled = Not IsEnable
        Next
    End Sub




    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        status = "1"
        Dim dt As DataTable = GetDataCountryRe_FromRpt(rptList)
        dtDtial = GetDataFromRptcol(rptList)
        Dim dr As DataRow
        dr = dt.NewRow
        Dim idface As Integer
        For i As Integer = 0 To dt.Rows.Count - 1
            idface = dt.Rows(i)("id")
        Next
        dr("id") = idface + 1
        dr("SumCol") = 0
        dr("Payment_Date_Plan") = DBNull.Value
        dt.Rows.Add(dr)

        dt.Columns.Add("no", GetType(Long))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("no") = i + 1
        Next


        dtTemplate = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(lblTemplateId.Text))

        Dim id As Integer = idface + 1
        Dim drt As DataRow

        For j As Integer = 0 To dtTemplate.Rows.Count - 1
            drt = dtDtial.NewRow
            drt("Activity_Expense_Plan_id") = id.ToString
            drt("Expense_Sub_Activity_id") = dtTemplate.Rows(j).Item("id").ToString
            drt("Pay_Amount_Plan") = 0
            dtDtial.Rows.Add(drt)
        Next

        rptList.DataSource = dt
        rptList.DataBind()
        SetDataRptSum()
    End Sub

    Function SetHead(Activity_id As Double, Header_ID As Double, TB_Mode As String) As DataTable
        Dim dt_col As New DataTable
        Dim template_id As Double = BL.GetList_TemplateByActivity(GL.ConvertCINT(Activity_id))
        lblActivityID1.Text = GL.ConvertCINT(Activity_id)
        lblHeaderID.Text = Header_ID

        lbMode_Expense.Text = TB_Mode

        lblTemplateId.Text = template_id.ToString
        dt_col = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(template_id))
        RptHead.DataSource = dt_col
        RptHead.DataBind()
    End Function


    Function SetDataForGetRpt(Aepi As String) As DataTable

        Dim dt As New DataTable

        dt.Columns.Add("amount")
        dt.Columns.Add("expense_sub_id")
        dt.Columns.Add("aepi")

        Dim dr As DataRow
        For j As Integer = 0 To dtDtial.Rows.Count - 1

            dr = dt.NewRow
            Dim ID As String = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
            If ID = Aepi Then
                dr("amount") = dtDtial.Rows(j).Item("Pay_Amount_Plan").ToString
                dr("expense_sub_id") = dtDtial.Rows(j).Item("Expense_Sub_Activity_id").ToString
                dr("aepi") = dtDtial.Rows(j).Item("Activity_Expense_Plan_id").ToString
                dt.Rows.Add(dr)
            End If

        Next

        Return dt
    End Function

    Function GetDataFromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Plan", GetType(DateTime))
        dt.Columns.Add("Pay_Plan_Detail")
        dt.Columns.Add("Payment_Plan_Detail")
        dt.Columns.Add("SumCol")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim txtSumCol As TextBox = DirectCast(rptList.Items(i).FindControl("txtSumCol"), TextBox)
            dr = dt.NewRow

            dr("id") = lblID.Text
            'dr("Payment_Date_Plan") = txtPaymentDate.Text

            If txtPaymentDate.Text <> "" Then
                dr("Payment_Date_Plan") = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            dr("Pay_Plan_Detail") = txtDetail.Text
            dr("Payment_Plan_Detail") = txtDetailNo.Text
            dr("SumCol") = txtSumCol.Text

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Function GetDataFromRptcol(rpt As Repeater) As DataTable
        Dim dte As New DataTable
        dte.Columns.Add("Header_id")
        dte.Columns.Add("Recipience_id")
        dte.Columns.Add("Expense_Sub_Activity_id")
        dte.Columns.Add("Pay_Amount_Plan")
        dte.Columns.Add("Activity_Expense_Plan_id")

        Dim dre As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim rptColItem As Repeater = DirectCast(rptList.Items(i).FindControl("rptColItem"), Repeater)

            For j As Integer = 0 To rptColItem.Items.Count - 1
                Dim lblitem_subexpenseid As Label = DirectCast(rptColItem.Items(j).FindControl("lblitem_subexpenseid"), Label)
                Dim txtCol2 As TextBox = DirectCast(rptColItem.Items(j).FindControl("txtCol2"), TextBox)
                dre = dte.NewRow
                dre("Header_id") = lblitem_subexpenseid.Text
                dre("Recipience_id") = lblitem_subexpenseid.Text
                dre("Expense_Sub_Activity_id") = lblitem_subexpenseid.Text
                If txtCol2.Text <> "" Then
                    dre("Pay_Amount_Plan") = txtCol2.Text
                Else
                    dre("Pay_Amount_Plan") = 0
                End If
                dre("Activity_Expense_Plan_id") = lblID.Text


                dte.Rows.Add(dre)
            Next
        Next

        Return dte
    End Function

    Function GetDataCountryRe_FromRpt(rpt As Repeater) As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("Payment_Date_Plan", GetType(DateTime))
        dt.Columns.Add("Pay_Plan_Detail")
        dt.Columns.Add("Payment_Plan_Detail")
        dt.Columns.Add("SumCol")

        Dim dr As DataRow
        For i As Integer = 0 To rptList.Items.Count - 1
            Dim txtPaymentDate As TextBox = DirectCast(rptList.Items(i).FindControl("txtPaymentDate"), TextBox)
            Dim txtExpand As TextBox = DirectCast(rptList.Items(i).FindControl("txtExpand"), TextBox)
            Dim txtDetail As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetail"), TextBox)
            Dim txtDetailNo As TextBox = DirectCast(rptList.Items(i).FindControl("txtDetailNo"), TextBox)
            Dim lblID As Label = DirectCast(rptList.Items(i).FindControl("lblID"), Label)
            Dim txtSumCol As TextBox = DirectCast(rptList.Items(i).FindControl("txtSumCol"), TextBox)
            dr = dt.NewRow

            dr("id") = lblID.Text
            'dr("Payment_Date_Plan") = txtPaymentDate.Text
            If txtPaymentDate.Text <> "" Then
                dr("Payment_Date_Plan") = Converter.StringToDate(txtPaymentDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            End If

            dr("Pay_Plan_Detail") = txtDetail.Text
            dr("Payment_Plan_Detail") = txtDetailNo.Text
            dr("SumCol") = txtSumCol.Text
            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim ddlCountry As DropDownList = DirectCast(e.Item.FindControl("ddlCountry"), DropDownList)
        Dim txtDetail As TextBox = DirectCast(e.Item.FindControl("txtDetail"), TextBox)
        Dim txtDetailNo As TextBox = DirectCast(e.Item.FindControl("txtDetailNo"), TextBox)
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim rptColItem As Repeater = DirectCast(e.Item.FindControl("rptColItem"), Repeater)
        Dim txtPaymentDate As TextBox = DirectCast(e.Item.FindControl("txtPaymentDate"), TextBox)
        Dim txtSumCol As TextBox = DirectCast(e.Item.FindControl("txtSumCol"), TextBox)

        lblID.Text = e.Item.DataItem("id").ToString
        'txtPaymentDate.Text = e.Item.DataItem("Payment_Date_Plan").ToString
        'If txtPaymentDate.Text <> "" Then
        '    txtPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        'End If

        If Not IsDBNull(e.Item.DataItem("Payment_Date_Plan")) Then
            txtPaymentDate.Text = Convert.ToDateTime(e.Item.DataItem("Payment_Date_Plan")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        End If

        txtDetail.Text = e.Item.DataItem("Pay_Plan_Detail").ToString
        txtDetailNo.Text = e.Item.DataItem("Payment_Plan_Detail").ToString
        If Not IsDBNull(e.Item.DataItem("SumCol")) Then
            txtSumCol.Text = Convert.ToDecimal(e.Item.DataItem("SumCol")).ToString("#,##0.00")
        End If
        'e.Item.DataItem("SumCol").ToString()

        Session("b") = txtSumCol.ClientID
        AddHandler rptColItem.ItemDataBound, AddressOf rptColItem_ItemDataBound
        Dim dt As DataTable
        If status = "1" Then
            dt = SetDataForGetRpt(lblID.Text)
        Else
            If lblID.Text = "0" Then
                dt = BL.GetList_SubExpenseDetailPlanListG(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblID.Text)
            Else
                dt = BL.GetList_SubExpenseDetailPlanList(GL.ConvertCINT(lblActivityID1.Text), GL.ConvertCINT(lblHeaderID.Text), GL.ConvertCINT(lblTemplate.Text), lblRecipience.Text, lblID.Text)
            End If
        End If

        SetDataRptSum()
        rptColItem.DataSource = dt
        rptColItem.DataBind()

        SetDataRptSum()
    End Sub

    Protected Sub rptColItem_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim txtCol2 As TextBox = DirectCast(e.Item.FindControl("txtCol2"), TextBox)
        Dim lblitem_subexpenseid As Label = DirectCast(e.Item.FindControl("lblitem_subexpenseid"), Label)
        Dim Pay_Amount_Plan As Label = DirectCast(e.Item.FindControl("Pay_Amount_Plan"), Label)
        Dim txtTemp As TextBox = DirectCast(e.Item.FindControl("txtTemp"), TextBox)

        If Not IsDBNull(e.Item.DataItem("amount")) Then
            txtCol2.Text = Convert.ToDecimal(e.Item.DataItem("amount")).ToString("#,##0.00")
            txtTemp.Text = e.Item.DataItem("amount").ToString
        End If

        ImplementJavaMoneyText(txtCol2)

        Dim txtSum As TextBox = rptSum.Items(e.Item.ItemIndex).FindControl("txtSum")
        Dim b As String = Session("b")
        txtCol2.Attributes.Add("onblur", "myFunction(this,'" + txtTemp.ClientID + "','" + txtSum.ClientID + "','" + b + "')")

        lblitem_subexpenseid.Text = e.Item.DataItem("expense_sub_id").ToString
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.CommandName = "Delete" Then

            Dim dt As DataTable = GetDataFromRpt(rptList)
            dtDtial = GetDataFromRptcol(rptList)
            dt.Rows.RemoveAt(e.Item.ItemIndex)

            Dim dte As New DataTable
            dte.Columns.Add("Header_id")
            dte.Columns.Add("Recipience_id")
            dte.Columns.Add("Expense_Sub_Activity_id")
            dte.Columns.Add("Pay_Amount_Plan")
            dte.Columns.Add("Activity_Expense_Plan_id")

            Dim dre As DataRow

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim id = dt.Rows(i)("id").ToString()
                For j As Integer = 0 To dtDtial.Rows.Count - 1
                    If id = dtDtial.Rows(j)("Activity_Expense_Plan_id").ToString() Then
                        dre = dte.NewRow
                        dre("Header_id") = dtDtial.Rows(j)("Header_id").ToString()
                        dre("Recipience_id") = dtDtial.Rows(j)("Recipience_id").ToString()
                        dre("Expense_Sub_Activity_id") = dtDtial.Rows(j)("Expense_Sub_Activity_id").ToString()
                        dre("Pay_Amount_Plan") = dtDtial.Rows(j)("Pay_Amount_Plan").ToString()
                        dre("Activity_Expense_Plan_id") = dtDtial.Rows(j)("Activity_Expense_Plan_id").ToString()


                        dte.Rows.Add(dre)
                    End If
                Next
            Next

            dtDtial = dte
            status = "1"
            For j As Integer = 0 To dt.Rows.Count - 1
                If Not IsDBNull(dt.Rows(j)("Payment_Date_Plan")) Then
                    dt.Rows(j)("Payment_Date_Plan") = dt.Rows(j)("Payment_Date_Plan")
                End If

                'Dim swdate = dt.Rows(j)("Payment_Date_Plan").ToString()
                'If swdate <> "" Then
                '    'dt.Rows(j)("Payment_Date_Plan") = Converter.StringToDate(swdate.Trim, "dd/MM/yyyy")
                'End If
            Next

            rptList.DataSource = dt
            rptList.DataBind()
            SetDataRptSum()
        End If
    End Sub

    Private Sub RptHead_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptHead.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lbHeader As Label = DirectCast(e.Item.FindControl("lbHeader"), Label)

        lbHeader.Text = e.Item.DataItem("sub_name").ToString
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim DTExpenseData As New DataTable
            With DTExpenseData
                .Columns.Add("id")
                .Columns.Add("Payment_Date_Plan", GetType(DateTime))
                .Columns.Add("Pay_Plan_Detail")
                .Columns.Add("Payment_Plan_Detail")
                .Columns.Add("Recipience_id")
            End With

            Dim drH As DataRow
            Dim dtDetail As DataTable = ComponentDT
            For j As Integer = 0 To dtDetail.Rows.Count - 1
                drH = DTExpenseData.NewRow
                drH("id") = dtDetail.Rows(j)("id").ToString
                If Not IsDBNull(dtDetail.Rows(j)("Payment_Date_Plan")) Then
                    drH("Payment_Date_Plan") = dtDetail.Rows(j)("Payment_Date_Plan")
                End If

                'drH("Payment_Date_Plan") = dtDetail.Rows(j)("Payment_Date_Plan").ToString
                drH("Pay_Plan_Detail") = dtDetail.Rows(j)("Pay_Plan_Detail").ToString
                drH("Payment_Plan_Detail") = dtDetail.Rows(j)("Payment_Plan_Detail").ToString
                drH("Recipience_id") = dtDetail.Rows(j)("Payment_Plan_Detail").ToString
                DTExpenseData.Rows.Add(drH)
            Next

            Dim dt_ItemData As New DataTable
            With dt_ItemData
                .Columns.Add("Header_id")
                .Columns.Add("Recipience_id")
                .Columns.Add("Expense_Sub_Activity_id")
                .Columns.Add("Pay_Amount_Plan")
                .Columns.Add("Activity_Expense_Plan_id")
            End With

            Dim dr As DataRow
            Dim dtDetail1 As DataTable = ExpenstListDT
            For j As Integer = 0 To dtDetail1.Rows.Count - 1
                dr = dt_ItemData.NewRow
                dr("Header_id") = dtDetail1.Rows(j)("Header_id").ToString
                dr("Recipience_id") = dtDetail1.Rows(j)("Recipience_id").ToString
                dr("Expense_Sub_Activity_id") = dtDetail1.Rows(j)("Expense_Sub_Activity_id").ToString
                dr("Pay_Amount_Plan") = dtDetail1.Rows(j)("Pay_Amount_Plan").ToString
                dr("Activity_Expense_Plan_id") = dtDetail1.Rows(j)("Activity_Expense_Plan_id").ToString
                dt_ItemData.Rows.Add(dr)
            Next
            Dim ret As New ProcessReturnInfo


            Dim Recip As DataTable = BL.GetRecipinceType(lblActivityID1.Text)
            Dim rt As String = Recip.Rows(0)("Recipient_Type").ToString()
            Dim dt_Recip As New DataTable
            If rt = "R" Then
                dt_Recip = BL.GetActivityExpenseDataIntoTypeR(lblActivityID1.Text, lblHeaderID.Text)
            ElseIf rt = "C" Then
                dt_Recip = BL.GetActivityExpenseDataIntoTypeC(lblActivityID1.Text, lblHeaderID.Text)
            ElseIf rt = "O" Then
                dt_Recip = BL.GetActivityExpenseDataIntoTypeO(lblActivityID1.Text, lblHeaderID.Text)
            ElseIf rt = "G" Then
                dt_Recip = BL.GetActivityExpenseDataIntoTypeG(lblActivityID1.Text, lblHeaderID.Text)
            ElseIf rt = "M" Then
                dt_Recip = BL.GetActivityExpenseDataIntoTypeM(lblActivityID1.Text, lblHeaderID.Text)
            End If

            '----Loop ตามจำนวนผู้รับทุน---
            If lbMode_Expense.Text = "Plan" Then
                Save_Plan(dt_Recip, DTExpenseData, dt_ItemData, rt)
            Else
                '---Actual
                Save_Actual(dt_Recip, DTExpenseData, dt_ItemData, rt)
            End If



            'For k As Integer = 0 To dt_Recip.Rows.Count - 1

            '    '------SAVE-------

            '    Dim SQL As String = "Select * FROM TB_Activity_Expense_Plan WHERE 0=1 "
            '    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            '    Dim DT_Expense As New DataTable
            '    DA.Fill(DT_Expense)
            '    If DTExpenseData.Rows.Count > 0 Then

            '        For i As Integer = 0 To DTExpenseData.Rows.Count - 1
            '            Dim id As String = DTExpenseData.Rows(i)("id").ToString()
            '            Dim Payment_Date_Plan As String = DTExpenseData.Rows(i)("Payment_Date_Plan").ToString()
            '            Dim Pay_Plan_Detail As String = DTExpenseData.Rows(i)("Pay_Plan_Detail").ToString()
            '            Dim Payment_Plan_Detail As String = DTExpenseData.Rows(i)("Payment_Plan_Detail").ToString()
            '            Dim Recipience_id As String = DTExpenseData.Rows(i)("Recipience_id").ToString()

            '            Dim DR_Expense As DataRow
            '            DR_Expense = DT_Expense.NewRow
            '            DT_Expense.Rows.Add(DR_Expense)

            '            If Not IsDBNull(DTExpenseData.Rows(i)("Payment_Date_Plan")) Then
            '                DR_Expense("PAYMENT_DATE_PLAN") = DTExpenseData.Rows(i)("Payment_Date_Plan")
            '            End If

            '            DR_Expense("PAY_PLAN_DETAIL") = Pay_Plan_Detail
            '            DR_Expense("PAYMENT_PLAN_DETAIL") = Payment_Plan_Detail
            '            DR_Expense("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
            '            DR_Expense("HEADER_ID") = lblHeaderID.Text


            '            Dim cmd As New SqlCommandBuilder(DA)
            '            Try
            '                DA.Update(DT_Expense)
            '            Catch ex As Exception
            '                'Msg
            '                Exit Sub
            '            End Try

            '            SQL = "Select * FROM TB_Activity_Expense_Plan_Detail WHERE 0=1 "
            '            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            '            Dim DT_Item As New DataTable
            '            For j As Integer = 0 To dt_ItemData.Rows.Count - 1

            '                Dim Expense_Plan_Id As String = dt_ItemData.Rows(j)("Activity_Expense_Plan_id").ToString()
            '                Dim Expense_Sub_Activity_id As String = dt_ItemData.Rows(j)("Expense_Sub_Activity_id").ToString()
            '                Dim Pay_Amount_Plan As String = dt_ItemData.Rows(j)("Pay_Amount_Plan").ToString()

            '                Dim DR_Item As DataRow
            '                DR_Item = DT_Item.NewRow
            '                DT_Item.Rows.Add(DR_Item)

            '                DR_Item("HEADER_ID") = lblHeaderID.Text
            '                DR_Item("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
            '                DR_Item("EXPENSE_SUB_ACTIVITY_ID") = Expense_Sub_Activity_id
            '                DR_Item("PAY_AMOUNT_PLAN") = Pay_Amount_Plan
            '                DR_Item("ACTIVITY_EXPENSE_PLAN_ID") = DT_Expense.Rows(0).Item("id")

            '                cmd = New SqlCommandBuilder(DA)
            '                Try
            '                    DA.Update(DT_Item)
            '                Catch ex As Exception
            '                    'Msg
            '                    Exit Sub
            '                End Try


            '            Next






            '        Next
            '    End If







            'Next

            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');", True)
            Response.Redirect("frmActivityExpense.aspx?ActivityID=" & lblActivityID1.Text & "&mode=" & "Edit" & "&HeaderId=" & lblHeaderID.Text & "&PID=" & CInt(Request.QueryString("PID")) & "&type=" & CInt(Request.QueryString("type")))



        Catch ex As Exception

            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ไม่สามารถบันทึกข้อมูลได้');", True)
        End Try
    End Sub


    Public Sub Save_Plan(dt_Recip As DataTable, DTExpenseData As DataTable, dt_ItemData As DataTable, Recipient_Type As String)

        For k As Integer = 0 To dt_Recip.Rows.Count - 1

            '------SAVE-------

            If DTExpenseData.Rows.Count > 0 Then


                For i As Integer = 0 To DTExpenseData.Rows.Count - 1
                    Dim SQL As String = "Select * FROM TB_Activity_Expense_Plan WHERE 0=1 "
                    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                    Dim DT_Expense As New DataTable
                    DA.Fill(DT_Expense)


                    Dim id As String = DTExpenseData.Rows(i)("id").ToString()
                    Dim Payment_Date_Plan As String = DTExpenseData.Rows(i)("Payment_Date_Plan").ToString()
                    Dim Pay_Plan_Detail As String = DTExpenseData.Rows(i)("Pay_Plan_Detail").ToString()
                    Dim Payment_Plan_Detail As String = DTExpenseData.Rows(i)("Payment_Plan_Detail").ToString()
                    Dim Recipience_id As String = DTExpenseData.Rows(i)("Recipience_id").ToString()

                    Dim DR_Expense As DataRow
                    DR_Expense = DT_Expense.NewRow
                    DT_Expense.Rows.Add(DR_Expense)

                    If Not IsDBNull(DTExpenseData.Rows(i)("Payment_Date_Plan")) Then
                        DR_Expense("PAYMENT_DATE_PLAN") = DTExpenseData.Rows(i)("Payment_Date_Plan")
                    End If

                    DR_Expense("PAY_PLAN_DETAIL") = Pay_Plan_Detail
                    DR_Expense("PAYMENT_PLAN_DETAIL") = Payment_Plan_Detail
                    DR_Expense("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
                    DR_Expense("HEADER_ID") = lblHeaderID.Text

                    'Dim New_Expense_Plan_ID As Integer = GetNew_ID("TB_Activity_Expense_Plan", "id")
                    Dim cmd As New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT_Expense)
                    Catch ex As Exception
                        'Msg
                        Exit Sub
                    End Try


                    Dim SQL_NewID As String = "Select Top 1 id FROM TB_Activity_Expense_Plan WHERE Header_id=" & lblHeaderID.Text & "  order by id desc "
                    Dim DA_NewID As New SqlDataAdapter(SQL_NewID, BL.ConnectionString)
                    Dim DT_NewID As New DataTable
                    DA_NewID.Fill(DT_NewID)
                    Dim New_Expense_Plan_ID As Integer = DT_NewID.Rows(0).Item("id")

                    dt_ItemData.DefaultView.RowFilter = "Activity_Expense_Plan_id=" & id
                    For j As Integer = 0 To dt_ItemData.DefaultView.Count - 1
                        SQL = "Select * FROM TB_Activity_Expense_Plan_Detail WHERE 0=1 "
                        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                        Dim DT_Item As New DataTable
                        DA.Fill(DT_Item)
                        Dim Expense_Plan_Id As String = dt_ItemData.DefaultView(j)("Activity_Expense_Plan_id").ToString()
                        Dim Expense_Sub_Activity_id As String = dt_ItemData.DefaultView(j)("Expense_Sub_Activity_id").ToString()
                        Dim Pay_Amount_Plan As String = dt_ItemData.DefaultView(j)("Pay_Amount_Plan").ToString()



                        Dim DR_Item As DataRow
                        DR_Item = DT_Item.NewRow
                        DT_Item.Rows.Add(DR_Item)

                        DR_Item("HEADER_ID") = lblHeaderID.Text
                        DR_Item("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
                        DR_Item("EXPENSE_SUB_ACTIVITY_ID") = Expense_Sub_Activity_id
                        If Recipient_Type = "G" Then
                            Dim Pay_Amount As Decimal = Average((Convert.ToDecimal(dt_ItemData.DefaultView(j)("Pay_Amount_Plan"))), dt_Recip.Rows(k).Item("recipient_id").ToString())
                            DR_Item("PAY_AMOUNT_PLAN") = Pay_Amount
                        Else
                            DR_Item("PAY_AMOUNT_PLAN") = Pay_Amount_Plan
                        End If

                        DR_Item("ACTIVITY_EXPENSE_PLAN_ID") = New_Expense_Plan_ID

                        cmd = New SqlCommandBuilder(DA)
                        Try
                            DA.Update(DT_Item)
                            DT_Item.AcceptChanges()
                        Catch ex As Exception
                            'Msg
                            Exit Sub
                        End Try


                    Next

                Next
            End If

        Next

    End Sub


    Public Sub Save_Actual(dt_Recip As DataTable, DTExpenseData As DataTable, dt_ItemData As DataTable, Recipient_Type As String)

        For k As Integer = 0 To dt_Recip.Rows.Count - 1

            '------SAVE-------

            If DTExpenseData.Rows.Count > 0 Then

                For i As Integer = 0 To DTExpenseData.Rows.Count - 1
                    Dim SQL As String = "Select * FROM TB_Activity_Expense_Actual WHERE 0=1 "
                    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                    Dim DT_Expense As New DataTable
                    DA.Fill(DT_Expense)

                    Dim id As String = DTExpenseData.Rows(i)("id").ToString()
                    Dim Payment_Date_Actual As String = DTExpenseData.Rows(i)("Payment_Date_Plan").ToString()
                    Dim Pay_Actual_Detail As String = DTExpenseData.Rows(i)("Pay_Plan_Detail").ToString()
                    Dim Payment_Actual_Detail As String = DTExpenseData.Rows(i)("Payment_Plan_Detail").ToString()
                    Dim Recipience_id As String = DTExpenseData.Rows(i)("Recipience_id").ToString()

                    Dim DR_Expense As DataRow
                    DR_Expense = DT_Expense.NewRow
                    DT_Expense.Rows.Add(DR_Expense)

                    If Not IsDBNull(DTExpenseData.Rows(i)("Payment_Date_Plan")) Then
                        DR_Expense("PAYMENT_DATE_Actual") = DTExpenseData.Rows(i)("Payment_Date_Plan")
                    End If

                    DR_Expense("PAY_Actual_DETAIL") = Pay_Actual_Detail
                    DR_Expense("PAYMENT_Actual_DETAIL") = Payment_Actual_Detail
                    DR_Expense("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
                    DR_Expense("HEADER_ID") = lblHeaderID.Text
                    Dim New_Expense_Actual_ID As Integer = GetNew_ID("TB_Activity_Expense_Actual", "id")

                    DR_Expense("id") = New_Expense_Actual_ID


                    Dim cmd As New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT_Expense)
                    Catch ex As Exception
                        'Msg
                        Exit Sub
                    End Try



                    dt_ItemData.DefaultView.RowFilter = "Activity_Expense_Plan_id=" & id
                    For j As Integer = 0 To dt_ItemData.DefaultView.Count - 1
                        SQL = "Select * FROM TB_Activity_Expense_Actual_Detail WHERE 0=1 "
                        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                        Dim DT_Item As New DataTable
                        DA.Fill(DT_Item)
                        Dim Expense_Actual_Id As String = dt_ItemData.DefaultView(j)("Activity_Expense_Plan_id").ToString()
                        Dim Expense_Sub_Activity_id As String = dt_ItemData.DefaultView(j)("Expense_Sub_Activity_id").ToString()
                        Dim Pay_Amount_Actual As String = dt_ItemData.DefaultView(j)("Pay_Amount_Plan").ToString()


                        Dim DR_Item As DataRow
                        DR_Item = DT_Item.NewRow
                        DT_Item.Rows.Add(DR_Item)

                        DR_Item("HEADER_ID") = lblHeaderID.Text
                        DR_Item("RECIPIENCE_ID") = dt_Recip.Rows(k).Item("recipient_id").ToString()
                        DR_Item("EXPENSE_SUB_ACTIVITY_ID") = Expense_Sub_Activity_id


                        If Recipient_Type = "G" Then
                            Dim Pay_Amount As Decimal = Average((Convert.ToDecimal(dt_ItemData.DefaultView(j)("Pay_Amount_Plan"))), dt_Recip.Rows(k).Item("recipient_id").ToString())
                            DR_Item("PAY_AMOUNT_Actual") = Pay_Amount
                        Else
                            DR_Item("PAY_AMOUNT_Actual") = Pay_Amount_Actual
                        End If


                        DR_Item("ACTIVITY_EXPENSE_Actual_ID") = New_Expense_Actual_ID

                        Dim New_Expense_Actual_Detail_ID As Integer = GetNew_ID("TB_Activity_Expense_Actual_Detail", "id")

                        DR_Item("id") = New_Expense_Actual_Detail_ID

                        cmd = New SqlCommandBuilder(DA)
                        Try
                            DA.Update(DT_Item)
                        Catch ex As Exception
                            'Msg
                            Exit Sub
                        End Try

                    Next

                Next
            End If

        Next

    End Sub



    Private Sub rptSum_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptSum.ItemDataBound
        Dim txtSum As TextBox = DirectCast(e.Item.FindControl("txtSum"), TextBox)


        txtSum.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
    End Sub

    Function SetDataRptSum() As DataTable
        Dim dtSubExid As DataTable
        dtSubExid = BL.GetList_SubExpenseByTemplate(GL.ConvertCINT(lblTemplateId.Text))


        Dim dt As New DataTable
        With dt
            .Columns.Add("Expense_Sub_Activity_id")
            .Columns.Add("Pay_Amount_Plan")
        End With
        Dim dr As DataRow
        For i As Integer = 0 To dtSubExid.Rows.Count - 1
            dr = dt.NewRow
            dr("Expense_Sub_Activity_id") = dtSubExid.Rows(i)("id").ToString
            dr("Pay_Amount_Plan") = 0
            dt.Rows.Add(dr)
        Next


        Dim dtDetail1 As DataTable = ExpenstListDT

        For i As Integer = 0 To dt.Rows.Count - 1
            For j As Integer = 0 To dtDetail1.Rows.Count - 1
                If dt.Rows(i)("Expense_Sub_Activity_id").ToString = dtDetail1.Rows(j)("Expense_Sub_Activity_id").ToString Then
                    Dim a As Double = Convert.ToDouble(dtDetail1.Rows(j)("Pay_Amount_Plan"))
                    Dim b As Double = Convert.ToDouble(dt.Rows(i)("Pay_Amount_Plan"))
                    a += b
                    dt.Rows(i)("Pay_Amount_Plan") = a
                End If
            Next
        Next
        rptSum.DataSource = dt
        rptSum.DataBind()

    End Function


    Public Function GetNew_ID(ByRef Table_Name As String, ByRef Collumn_ID As String) As Integer
        Dim SQL As String = "SELECT IsNull(MAX(" & Collumn_ID & "),0)+1 FROM " & Table_Name & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Function Average(Pay_Amount As Decimal, country_node_id As Integer) As Decimal

        Dim Amout As Decimal = 0.00
        Dim DT_Amout As DataTable = BL.GetDataCountryRecipent_FromQurey(lblActivityID1.Text)
        DT_Amout.DefaultView.RowFilter = "country_node_id=" & country_node_id
        If DT_Amout.DefaultView.Count > 0 Then
            Amout = Convert.ToDecimal(DT_Amout.DefaultView(0).Item("amout_person"))
        End If
        If Amout = 0.00 Then Amout = 1.0


        Dim SQL As String = "SELECT SUM(amout_person) SumPerson FROM TB_Country_Recipient  WHERE activity_id=" & lblActivityID1.Text & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim AVG As Decimal = 0.00
        If DT.Rows.Count > 0 Then
            AVG = (Convert.ToDecimal(Pay_Amount) / Convert.ToDecimal(DT.Rows(0).Item("SumPerson"))) * Amout
        End If
        Return AVG
    End Function



End Class
