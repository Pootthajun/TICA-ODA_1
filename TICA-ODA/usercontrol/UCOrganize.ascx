﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCOrganize.ascx.vb" Inherits="usercontrol_UCOrganize" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr class="bg-gray">
                        <th>Country (ประเทศ)</th>
                        <th>Agency (หน่วยงาน)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-title="Country (ประเทศ)" style ="padding:0px;">
                                    <asp:UpdatePanel ID="udpList" runat="server">
                                        <ContentTemplate>
                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 100%; border:none;" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    </ContentTemplate>
                                        <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                                     </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                    <%--<asp:Button ID="btnCountry" CommandName="CountryChange" runat="server" Style="display: none;" />--%>
                                </td>
                                <td data-title="Agency (หน่วยงาน)" id="td" runat="server"  style ="padding:0px;">
                                    <asp:DropDownList ID="ddlAgency" runat="server" CssClass="form-control select2" Style="width: 100%; border:none;">
                                    </asp:DropDownList>
                                </td>
                                <td data-title="Delete" id="ColDelete" runat="server"  style ="padding:0px;">
                                    <center>
                                <asp:Button  ID="btnDelete" runat="server" Text="ลบ" w CssClass="btn btn-bricky" width="100%"  CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');" />
                            </center>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>

            </table>


            <div class="row">
                <center>
            <div class="col-sm-12"  style="margin-top:10px;">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary" />
            </div>
        </center>
            </div>
        </div>
        <script>
            function bdCountryChange(btn) {
                document.getElementById(btn).click();
                return true;
            }

        </script>
    </ContentTemplate>
</asp:UpdatePanel>
