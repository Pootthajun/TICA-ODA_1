﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCProjectAdministrativeCost.ascx.vb" Inherits="usercontrol_UCProjectAdministrativeCost" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr class="bg-gray">
                 <th>ครั้งที่</th>
                <th>วันที่จ่าย</th>
                <th>รายละเอียด</th>
                <th>จำนวน (บาท)</th>
                <th style="width: 50px">ลบ</th>
            </tr>
        </thead>
        <tbody>
             <asp:Label ID="lblActivityID" runat="server" Text ="0" Visible="false"></asp:Label>
            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <tr>
                        <td data-title="ครั้งที่" style="width:80px;padding:0px; vertical-align:middle; text-align:center;" class="center">
                             <asp:Label ID="lblNo" runat="server" ></asp:Label>
                              <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td data-title="วันที่จ่าย"  style ="padding:0px;">
                              <asp:TextBox CssClass="form-control m-b" ID="txtDate_payment" runat="server" placeholder=""></asp:TextBox>
                               <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                 Format="dd/MM/yyyy" TargetControlID="txtDate_payment" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                        </td>
                        <td data-title="รายละเอียด"  style ="padding:0px;">
                              <asp:TextBox CssClass="form-control m-b" ID="txtDescription" runat="server" placeholder=""></asp:TextBox>
                         </td>
                        <td data-title="จำนวน (บาท)" id="td" runat="server"  style ="padding:0px;">
                          <asp:TextBox ID="txtExpand" runat="server" CssClass="form-control" placeholder="" Style="text-align: right" OnTextChanged="txtExpand_TextChanged" AutoPostBack="true"  MaxLength="12"></asp:TextBox>

                        </td>
                        <td data-title="Delete" id="ColDelete" runat="server"  style ="padding:0px;" >
                            <center>
                                <asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn btn-bricky" width="100%"   CommandArgument='<%# Eval("id") %>' CommandName="Delete" OnClientClick="return confirm('ท่านต้องการลบรายการนี้หรือไม่?');"/>
                            </center>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
         <asp:Panel ID="pnlFooter" runat="server">
        <tfoot>
            <tr>
                <td style="background-color:darkkhaki;text-align: center ;" colspan="3">
                    <b>รวม</b>
                </td>
                <td style="background-color:darkkhaki;text-align: right;">
                     <b><asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
                </td>
            </tr>

        </tfoot>
        </asp:Panel>
    </table>
<%--    <div class="row">
        <div class="col-sm-5 cost">
            <b>รวม</b>
                          
        </div>
        <div class="col-sm-6 cost cost-uc">
            <b>
                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label></b>
        </div>
    </div>--%>

    <div class="row"  style="margin-top:5px;">
        <center>
            <div class="col-sm-12">
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="เพิ่ม"
                    CssClass="btn btn-primary" />
            </div>
        </center>
    </div>
</div>