﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptBudget_Group_L2.aspx.vb" Inherits="rptBudget_Group_L2" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Search | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานการจัดสรรงบประมาณ <asp:Label ID="lblReportName" runat ="server" ></asp:Label> </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายในองค์กร</a></li>
                <li class="active">รายงานการจัดสรรงบประมาณ</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white"  DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top:30px;*/"></div>
                                                                                
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray"> 
                                                    <th>โครงการ</th>
                                                    <th>กิจกรรม</th>
                                                    <th>งบประมาณ</th>
                                                    <th>จัดสรรแล้ว</th>
                                                    <th>เบิกจ่าย</th>
                                                    <th>คงเหลือ</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trgroup_name" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Group name" colspan="5" style="text-align:  left ">
                                                             <b><asp:Label ID="lblgroup_name" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>                                                         
                                                            <td data-title="โครงการ"><asp:Label ID="lblProjectName" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="กิจกรรม"><asp:Label ID="lblActivityName" runat="server" ForeColor="black"></asp:Label></td>
                                                            
                                                            <td data-title="งบประมาณ" style="text-align :right ;"><asp:Label ID="lblAmount_Budget" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="จัดสรรแล้ว" style="text-align :right ;"><asp:Label ID="lblAllocate_Budget" runat="server"></asp:Label></td>
                                                            <td data-title="เบิกจ่าย" style="text-align :right  ; "><asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                            <td data-title="คงเหลือ" style="text-align :right;"><asp:Label ID="lblBalance" runat="server" ForeColor="black"></asp:Label></td>
                                                                                                                    
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                            <tr id="trFooter_Qty" runat="server" >
                                                            <td colspan ="2" data-title="Total" style="text-align :center ;" ><b> Total </b></td>
                                                            <td data-title="Count_Project" style="text-align :right;text-decoration: underline;"><b><asp:Label ID="lblAmount_Budget_SUM" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Bachelor" style="text-align :right ;text-decoration: underline;"><b><asp:Label ID="lblAllocate_Budget_SUM" runat="server"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Training" style="text-align :right  ;text-decoration: underline; "><b><asp:Label ID="lblPay_Amount_Actual_SUM" runat="server"></asp:Label></b></td>
                                                            <td data-title="Count_Component_Other" style="text-align :right;text-decoration: underline;"><b><asp:Label ID="lblBalance_Budget_SUM" runat="server" ForeColor="black"></asp:Label></b></td>
                                                                                                                                                                                     
										    </tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>

                                        <div class="row"></div>
                                        
                                    </div>
                                    <!-- /.box-body -->
                                  


                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox> 
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

   
</asp:Content>


