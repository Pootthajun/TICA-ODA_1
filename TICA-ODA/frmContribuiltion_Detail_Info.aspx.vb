﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmContribuiltion_Detail_Info
    Inherits System.Web.UI.Page


    Dim BL As New ODAENG

#Region "Property"
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property


    Public Property PlanID As String
        Get
            Try
                Return ViewState("PlanID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("PlanID") = value
        End Set
    End Property

    Public Property MultilateralOrganizations As String
        Get
            Try
                Return ViewState("MultilateralOrganizations")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("MultilateralOrganizations") = value
        End Set
    End Property

    Public Property ImplementingAgency As String
        Get
            Try
                Return ViewState("ImplementingAgency")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("ImplementingAgency") = value
        End Set
    End Property

    Public Property DisplayActivity_Type As String
        'month/year
        Get
            Try
                Return ViewState("DisplayActivity_Type")
            Catch ex As Exception
                Return "month"
            End Try
        End Get
        Set(value As String)
            ViewState("DisplayActivity_Type") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property
#End Region

    Private Sub frmDetailContribuiltion_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack = False Then
            Try
                Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuproject")
                li.Attributes.Add("class", "active")
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aContribuition")
                a.Attributes.Add("style", "color:#FF8000")


                ClearForm()
                EditProjectID = CInt(Request.QueryString("ID"))
                mode = Request.QueryString("mode").ToString()
            Catch ex As Exception
                EditProjectID = 0
            End Try

            ProjectType = CInt(Request.QueryString("type"))
            If ProjectType = Constants.Project_Type.Contribuition Then
                lblProjectType.Text = "Contribution"
                lblNavProjectType.Text = "Contribution"
                lblHeadProjectType.Text = "Contribution"
            End If

            BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
            BL.Bind_DDL_Coperationtype(ddlCooperationType)
            BL.Bind_DDL_OECD(ddlOECDAidType)
            BL.Bind_DDL_Organize(ddlFundingAgency)
            ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
            BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)
            BL.Bind_DDL_Organize(ddlImplementingAgency)
            BL.Bind_DLL_Plan(DropDownListPlan)
            SetAssistant()
            SetProjectFile()

            CurrentTab = Tab.ProjectDetail

            If EditProjectID <> 0 Then
                SetProjectInfoByID(EditProjectID)
                If mode = "view" Then
                    SetControlToViewMode(True)
                Else
                    SetControlToViewMode(False)
                End If
            End If
        Else
            RestoreJQueryUI()
        End If

        If mode = "palnex" Then
            mode = "edit"
        End If
        btnTabActivity.Enabled = False
        If txtProjectID.Text.Trim <> "" Then
            btnTabActivity.Enabled = True
            lblTitle_Project.Visible = False
        Else
            lblTitle_Project.Visible = True
        End If

    End Sub

#Region "Event"

    Private Sub nav_click(sender As Object, e As EventArgs) Handles nav.ServerClick
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '## Validate
            If Validate() = False Then
                Exit Sub
            End If

            '##Save
            '##tabProjectDetail##
            '-- lnqProject
            Dim lnqProject As New TbProjectLinqDB
            With lnqProject
                .ID = EditProjectID
                .PROJECT_ID = txtProjectID.Text
                .PROJECT_TYPE = ProjectType 'Project_Type.Loan
                .PROJECT_NAME = txtProjectName.Text
                .DESCRIPTION = txtDescription.Text
                .START_DATE = Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .END_DATE = Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text

                If ddlFundingAgency.SelectedValue <> "" Then
                    .FUNDING_AGENCY_ID = ddlFundingAgency.SelectedValue
                End If

                If ddlExecutingAgency.SelectedValue <> "" Then
                    .EXECUTING_AGENCY_ID = ddlExecutingAgency.SelectedValue
                End If
                If ddlImplementingAgency.SelectedValue <> "" Then
                    .IMPLEMENTING_AGENCY_CONTRIBUTION = ddlImplementingAgency.SelectedValue
                End If

                If ddlCooperationFramework.SelectedValue <> "" Then
                    .COOPERATION_FRAMEWORK_ID = ddlCooperationFramework.SelectedValue
                End If

                If ddlCooperationType.SelectedValue <> "" Then
                    .COOPERATION_TYPE_ID = ddlCooperationType.SelectedValue
                End If

                If ddlOECDAidType.SelectedValue <> "" Then
                    .OECD_AID_TYPE_ID = ddlOECDAidType.SelectedValue
                End If

                .ASSISTANT = GetAssistant()
                .REMARK = txtNote.Text
                If ddlTransferto.SelectedValue <> "" Then
                    .TRANSFER_PROJECT_TO = ddlTransferto.SelectedValue
                End If

            End With


            '--lnqContact
            Dim DTContact As New DataTable
            DTContact = UCContact.ContactDT
            Dim lnqContact As New TbOuContactLinqDB
            If DTContact.Rows.Count > 0 Then
                Dim _dr As DataRow = DTContact.Rows(0)
                With lnqContact
                    .PARENT_TYPE = Contact_Parent_Type.Project
                    .PARENT_ID = lnqProject.ID
                    .CONTACT_NAME = _dr("contact_name").ToString()
                    .TELEPHONE = _dr("telephone").ToString()
                    .FAX = _dr("fax").ToString()
                    .EMAIL = _dr("email").ToString()
                    .POSITION = _dr("position").ToString()
                End With
            End If


            '##DTProjectFile
            Dim DTProjectFile As DataTable = UCFileUploadList1.GetFileList()

            '##DTProjectSection

            Dim lnqPjPlan As New TbProjectplanLinqDB
            With lnqPjPlan
                If DropDownListPlan.SelectedValue <> "" Then
                    .PLAN_ID = DropDownListPlan.SelectedValue
                End If
            End With

            Dim redirect As String
            redirect = "frmContribuiltion_Detail_Activity.aspx?"
            'Dim DTMiltilateral As New DataTable
            'DTMiltilateral = UCBudgetContribution.BudgetContDT

            Dim ret As New ProcessReturnInfo
            'ret = BL.SaveProject(lnqProject, DTMiltilateral, Nothing, Nothing, lnqContact, DTProjectFile, Nothing, lnqPjPlan, UserName)
            ret = BL.SaveProject(lnqProject, Nothing, Nothing, Nothing, lnqContact, DTProjectFile, Nothing, lnqPjPlan, UserName)
            If ret.IsSuccess Then
                txtProjectID.Text = ret.PROJECT_ID
                EditProjectID = ret.ID

                btnTabActivity.Enabled = True

                ProjectType = CInt(Request.QueryString("type"))
                Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว'); window.location ='" + redirect + Param + "';", True)
            Else
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End If

        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้")
        End Try

    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub
#End Region

#Region "Sub&Function"

    Private Sub SetProjectInfoByID(id As String)

        Try
            Dim dtProject As New DataTable
            dtProject = BL.GetProjectInfoByID(id)

            '##tabProjectDetail##
            If dtProject.Rows.Count > 0 Then
                Dim _project_id As String = ""
                Dim _project_name As String = ""
                Dim _description As String = ""
                Dim _start_date As String = ""
                Dim _end_date As String = ""
                Dim _funding_agency_id As String = ""
                Dim _executing_agency_id As String = ""
                Dim _multilateral_id As String = ""
                Dim _implementing_agency_contribution As String = ""
                Dim _assistant As String = ""
                Dim _remark As String = ""
                Dim _transfer_project_to As String = ""
                Dim _cooperation_framework_id As String = ""
                Dim _cooperation_type_id As String = ""
                Dim _oecd_aid_type_id As String = ""

                With dtProject
                    _project_id = .Rows(0)("project_id").ToString()
                    _project_name = .Rows(0)("project_name").ToString()
                    _description = .Rows(0)("description").ToString()
                    _start_date = Convert.ToDateTime(.Rows(0)("start_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _end_date = Convert.ToDateTime(.Rows(0)("end_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _funding_agency_id = .Rows(0)("funding_agency_id").ToString()
                    _executing_agency_id = .Rows(0)("executing_agency_id").ToString
                    _cooperation_framework_id = .Rows(0)("cooperation_framework_id").ToString()
                    _cooperation_type_id = .Rows(0)("cooperation_type_id").ToString()
                    _oecd_aid_type_id = .Rows(0)("oecd_aid_type_id").ToString()
                    _multilateral_id = .Rows(0)("multilateral_id").ToString
                    _implementing_agency_contribution = .Rows(0)("implementing_agency_contribution").ToString
                    _remark = .Rows(0)("remark").ToString
                    _transfer_project_to = .Rows(0)("transfer_project_to").ToString
                    _assistant = .Rows(0)("assistant").ToString
                End With

                txtProjectID.Text = _project_id
                txtProjectName.Text = _project_name
                txtDescription.Text = _description
                txtStartDate.Text = _start_date
                txtEndDate.Text = _end_date
                Try
                    If _funding_agency_id <> "" Then
                        ddlFundingAgency.SelectedValue = _funding_agency_id
                        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
                        If _executing_agency_id <> "" Then ddlExecutingAgency.SelectedValue = _executing_agency_id
                    End If
                Catch ex As Exception

                End Try


                If _implementing_agency_contribution <> "" Then ddlImplementingAgency.SelectedValue = _implementing_agency_contribution
                If _cooperation_type_id <> "" Then ddlCooperationType.SelectedValue = _cooperation_type_id
                If _cooperation_framework_id <> "" Then ddlCooperationFramework.SelectedValue = _cooperation_framework_id
                If _oecd_aid_type_id <> "" Then ddlOECDAidType.SelectedValue = _oecd_aid_type_id

                If ddlImplementingAgency.SelectedValue = "" Then
                    ImplementingAgency = ""
                Else
                    ImplementingAgency = ddlImplementingAgency.SelectedItem.Text
                End If

                UCContact.ContactDT = dtProject
                txtNote.Text = _remark
                ddlTransferto.SelectedValue = _transfer_project_to
                Dim _assistantname As String = ""
                If _assistant <> "" Then
                    Dim _strAssistant() As String = _assistant.Split(", ")
                    For Each Item As ListItem In ctlSelectAssistant.Items
                        For i As Integer = 0 To _strAssistant.Length - 1
                            If Item.Value = _strAssistant(i) Then
                                Item.Selected = True
                                _assistantname &= Item.Text & ","
                            End If
                        Next
                    Next
                End If

                txtAssistant.Text = ""
                If _assistantname.Length > 0 Then
                    txtAssistant.Text = _assistantname.Substring(0, _assistantname.Length - 1)
                End If
            End If

            'Dim dtBudgetCon As New DataTable
            'dtBudgetCon = BL.GetProjectBudgetCon(id)
            'UCBudgetContribution.BudgetContDT = dtBudgetCon

            'ตรงนี้GetProjectPlan
            Dim ProjectPlan As New DataTable
            ProjectPlan = BL.GetProjectPlan(id)
            If ProjectPlan.Rows.Count > 0 Then
                DropDownListPlan.SelectedValue = ProjectPlan.Rows(0)("plan_id").ToString()


            End If

            If DropDownListPlan.SelectedValue = "" Then
                PlanID = ""
            Else
                PlanID = DropDownListPlan.SelectedItem.Text
            End If

        Catch ex As Exception
            alertmsg(ex.Message.ToString)
        End Try
    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)
        DropDownListPlan.Enabled = Not IsView
        txtProjectID.Enabled = Not IsView
        txtProjectName.Enabled = Not IsView
        txtDescription.Enabled = Not IsView
        txtStartDate.Enabled = Not IsView
        txtEndDate.Enabled = Not IsView
        ddlImplementingAgency.Enabled = Not IsView
        ddlFundingAgency.Enabled = Not IsView
        ddlExecutingAgency.Enabled = Not IsView
        ctlSelectAssistant.Visible = Not IsView
        txtAssistant.Visible = IsView
        txtAssistant.Enabled = Not IsView
        UCContact.SetToViewMode(IsView)
        txtNote.Enabled = Not IsView
        ddlTransferto.Enabled = Not IsView
        ddlCooperationFramework.Enabled = Not IsView
        ddlCooperationType.Enabled = Not IsView
        ddlOECDAidType.Enabled = Not IsView
        btnSave.Visible = Not IsView

        UCFileUploadList1.SetToViewMode(IsView)
    End Sub

    Private Sub ClearForm()
        ''##tabProjectDetail##
        EditProjectID = 0
        txtProjectID.Text = ""
        lblTitle_Project.Text = "<span style='color: red;'>*</span>" + " Contribution Name<br>(ชื่อโครงการ)"
        txtProjectName.Text = ""
        txtDescription.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        BL.Bind_DDL_Organize(ddlFundingAgency)
        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
        BL.Bind_DDL_Organize(ddlImplementingAgency)

        BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
        BL.Bind_DDL_Coperationtype(ddlCooperationType)
        BL.Bind_DDL_OECD(ddlOECDAidType)
        SetAssistant()
        ctlSelectAssistant.Visible = True
        txtAssistant.Visible = False
        UCContact.ClearForm()
        txtNote.Text = ""
        BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)

        'Dim dtBudgetCon As New DataTable
        'dtBudgetCon = BL.GetProjectBudgetCon(0)
        'UCBudgetContribution.BudgetContDT = dtBudgetCon

    End Sub

    Private Sub SetProjectFile()
        UCFileUploadList1.ClearTempFolder(UserName)

        Dim dt As DataTable = BL.GetProjectFile(EditProjectID)
        If dt.Rows.Count > 0 Then
            UCFileUploadList1.SetFileList(dt)
        End If
    End Sub

    Private Sub RestoreJQueryUI()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RestoreJQueryUI", "restoreJQueryUI();", True)
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub


    Private Sub SetAssistant()
        Dim dt As New DataTable
        dt = BL.GetPersonList(Person_Type.Officer)

        ctlSelectAssistant.DataValueField = "id"
        ctlSelectAssistant.DataTextField = "name_th"

        ctlSelectAssistant.DataSource = dt
        ctlSelectAssistant.DataBind()
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtProjectName.Text.Trim() = "" Then
            alertmsg("กรุณาระบุชื่อโครงการ")
            ret = False
        End If
        If txtStartDate.Text = "" Then
            alertmsg("กรุณากรอกวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtEndDate.Text = "" Then
            alertmsg("กรุณากรอกวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlCooperationFramework.SelectedValue = "" Then
            alertmsg("กรุณาระบุกรอบความร่วมมือ")
            ret = False
        End If

        Try
            Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("วันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        '-----ตัดไปไว้ใน Activity-----
        'Dim DTMiltilateral As DataTable = UCBudgetContribution.BudgetContDT
        'For i As Integer = 0 To DTMiltilateral.Rows.Count - 1
        '    Dim id As String = DTMiltilateral.Rows(i)("id").ToString
        '    Dim multilateral_id As String = DTMiltilateral.Rows(i)("Multilateral_ID").ToString
        '    Dim amount As String = DTMiltilateral.Rows(i)("Amount").ToString

        '    If multilateral_id = "" Then
        '        alertmsg("กรุณาระบุองค์กรพหุภาคี")
        '        ret = False
        '    End If

        '    If amount = "" Then
        '        alertmsg("กรุณาระบุจำนวนเงิน ")
        '        ret = False
        '    End If

        '    Dim tempdr() As DataRow
        '    tempdr = DTMiltilateral.Select("Multilateral_ID ='" & multilateral_id & "' and id <> '" & id & "'")
        '    If tempdr.Length > 0 Then
        '        alertmsg("รายชื่อองค์กรพหุภาคีซ้ำ ")
        '        ret = False
        '    End If
        'Next
        Return ret
    End Function

    Private Function GetAssistant() As String
        Dim strAssistant As String = ""

        For Each Item As ListItem In ctlSelectAssistant.Items
            If Item.Selected Then
                strAssistant += Item.Value & ","
            End If
        Next
        If strAssistant.Length > 0 Then
            Return strAssistant.Substring(0, strAssistant.Length - 1)
        Else
            Return ""
        End If

    End Function

    Private Sub ddlFundingAgency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFundingAgency.SelectedIndexChanged
        BL.Bind_DDL_ChildsOrganize(ddlExecutingAgency, ddlFundingAgency.SelectedValue)
    End Sub

#End Region

#Region "Tab"
    Protected Enum Tab
        Unknown = 0
        ProjectDetail = 1
        Activity = 2
        Recipient = 3
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case tabProjectDetail.Visible
                    Return Tab.ProjectDetail
                Case Else
                    Return Tab.Unknown
            End Select
        End Get
        Set(value As Tab)
            tabProjectDetail.Visible = False

            liTabProjectDetail.Attributes("class") = ""
            liTabActivity.Attributes("class") = ""

            Select Case value
                Case Tab.ProjectDetail
                    tabProjectDetail.Visible = True
                    liTabProjectDetail.Attributes("class") = "active"
                Case Else
            End Select
        End Set
    End Property

    'Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabProjectDetail.Click, btnTabActivity.Click
    '    Select Case True
    '        Case Equals(sender, btnTabProjectDetail)
    '            CurrentTab = Tab.ProjectDetail
    '        Case Equals(sender, btnTabActivity)
    '            CurrentTab = Tab.Activity
    '        Case Else
    '    End Select
    'End Sub

    'Private Sub btnAddActivity_Click(sender As Object, e As EventArgs) Handles btnAddActivity.Click
    '    UCActivityTabContribuiltion1.AddActivity(0)
    'End Sub

    'Private Sub UCActivityTreeList1_AddChildNode(ParentID As Long) Handles UCActivityTreeList1.AddChildNode
    '    UCActivityTabContribuiltion1.AddActivity(ParentID)
    'End Sub

    'Private Sub UCActivityTreeList1_EditChildNode(ActivityID As Long) Handles UCActivityTreeList1.EditChildNode
    '    UCActivityTabContribuiltion1.EditActivity(ActivityID)
    'End Sub

    'Private Sub UCActivityTreeList1_ViewChildNode(ActivityID As Long) Handles UCActivityTreeList1.ViewChildNode
    '    UCActivityTabContribuiltion1.ViewActivity(ActivityID)
    'End Sub

    'Private Sub UCActivityTabLoan1_SaveActivityComplete() Handles UCActivityTabContribuiltion1.SaveActivityComplete
    '    UCActivityTreeList1.GenerateActivityListLone(EditProjectID, mode)
    'End Sub

#End Region

#Region " Print Button"
    Function GetParameter(Reportformat As String) As String
        Dim para As String = "&ReportName=rptLoanDetail"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectID=" & EditProjectID
        para += "&PlanID=" & PlanID
        para += "&MultilateralOrganizations=" & MultilateralOrganizations
        para += "&ImplementingAgency=" & ImplementingAgency

        'para += "&GrancePeriod=" & GrancePeriod
        'para += "&GrancePeriodMonth=" & GrancePeriodMonth
        'para += "&MaturityPeriod=" & MaturityPeriod
        'para += "&MaturityPeriodMonth=" & MaturityPeriodMonth

        para += "&ProjectTypeName=Contribution"
        Return para
    End Function



#End Region

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptContributionDetail.aspx?Mode=PDF" + para + "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptContributionDetail.aspx?Mode=EXCEL" + para + "');", True)
    End Sub

#End Region


#Region "Navigator"


    Private Sub btnTabProjectDetail_Click(sender As Object, e As EventArgs) Handles btnTabProjectDetail.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmContribuiltion_Detail_Info.aspx?" & Param & "';", True)
    End Sub

    Private Sub btnTabActivity_Click(sender As Object, e As EventArgs) Handles btnTabActivity.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmContribuiltion_Detail_Activity.aspx?" & Param & "';", True)
    End Sub

#End Region


End Class
