﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient

Partial Class rptCLMV
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptPage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptCLMU")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            pnlSearchActivity.Visible = False
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BindList()
            'Authorize()
        End If

    End Sub

    Public Function GetList() As DataTable
        Dim sql As String = ""
        Dim dt As New DataTable
        Try
            Dim Title As String = ""
            Dim Filter As String = ""
            sql += "  --หาจ่ายจริง ของประเทศใน CLMV ตามปีงบประมาณ																					 " + Environment.NewLine
            sql += "  SELECT  budget_year 																									 " + Environment.NewLine
            If ckShowActivity.Checked Then
                sql += "  		  --==GROUP PROJECT==																							 " + Environment.NewLine
                sql += "  		  ,Project_id																									 " + Environment.NewLine
                sql += "  	      ,project_name																									 " + Environment.NewLine
                sql += "  	      ,project_type																									 " + Environment.NewLine
            End If
            sql += "  		,sum(Cambodia) Cambodia,sum(Laos) Laos,sum(Myanmar) Myanmar,sum(Vietnam) Vietnam								 " + Environment.NewLine
            sql += "  		,sum(Cambodia)+sum(Laos)+sum(Myanmar)+sum(Vietnam) Summary_YEAR													 " + Environment.NewLine
            sql += "  	FROM (																												 " + Environment.NewLine
            sql += "  	SELECT budget_year																									 " + Environment.NewLine
            If ckShowActivity.Checked Then
                sql += "  	--==GROUP PROJECT==																									 " + Environment.NewLine
                sql += "  		  ,Project_id																									 " + Environment.NewLine
                sql += "  	      ,project_name																									 " + Environment.NewLine
                sql += "  	      ,project_type																									 " + Environment.NewLine
                sql += "  		  ------,commitment_budget																						 " + Environment.NewLine
            End If
            sql += "  		  ,country_node_id																								 " + Environment.NewLine
            sql += "  		  ,CASE WHEN country_node_id ='32' THEN ISNULL(SUM(Pay_Amount_Actual),0)    -- กัมพูชา	Cambodia					 " + Environment.NewLine
            sql += "    		  ELSE 0 END Cambodia																						 " + Environment.NewLine
            sql += "    		  ,CASE WHEN country_node_id ='107' THEN ISNULL(SUM(Pay_Amount_Actual),0)    -- สปป. ลาว	Laos					 " + Environment.NewLine
            sql += "    		  ELSE 0 END Laos																							 " + Environment.NewLine
            sql += "    		  ,CASE WHEN country_node_id ='136' THEN ISNULL(SUM(Pay_Amount_Actual),0)    -- เมียนมา	Myanmar					 " + Environment.NewLine
            sql += "    		  ELSE 0 END Myanmar																						 " + Environment.NewLine
            sql += "    		  ,CASE WHEN country_node_id ='215' THEN ISNULL(SUM(Pay_Amount_Actual),0)    -- เวียดนาม	Vietnam				 " + Environment.NewLine
            sql += "    		  ELSE 0 END Vietnam																						 " + Environment.NewLine
            sql += "  		  ------,ISNULL(SUM(Pay_Amount_Actual),0) Pay_Amount_Actual														 " + Environment.NewLine
            sql += "  	 FROM																												 " + Environment.NewLine
            sql += "  	(																													 " + Environment.NewLine
            sql += "  		SELECT TB_Activity_Budget.budget_year,_vw_Activity_Pay_Amount_Actual.* 											 " + Environment.NewLine
            sql += "  		FROM _vw_Activity_Pay_Amount_Actual																				 " + Environment.NewLine
            sql += "  		INNER JOIN vw_CountryCLMV ON vw_CountryCLMV.node_id=_vw_Activity_Pay_Amount_Actual.country_node_id				 " + Environment.NewLine
            sql += "  		INNER JOIN TB_Activity_Budget ON TB_Activity_Budget.activity_id = _vw_Activity_Pay_Amount_Actual.Activity_id 	 " + Environment.NewLine
            sql += "  	)AS TB	WHERE 1=1																											 " + Environment.NewLine

            If (ddlBudgetYear.SelectedIndex > 0) Then
                sql += " AND  budget_year = '" & ddlBudgetYear.SelectedValue & "' " + Environment.NewLine
                Title += " ใช้งบประมาณปี " & ddlBudgetYear.SelectedValue
            End If

            If (ddlProject_Type.SelectedValue > -1) Then
                sql += " AND  project_type = '" & ddlProject_Type.SelectedValue & "'   " + Environment.NewLine
                Title += " โครงการประเภท " & ddlProject_Type.SelectedItem.ToString()
            Else
                Title += "  "

            End If

            If (txtSearch_Project.Text <> "") Then
                sql += " AND project_name Like '%" & txtSearch_Project.Text & "%'   " + Environment.NewLine
                Title += " ชื่อ: " & ddlBudgetYear.SelectedItem.ToString
            End If


            sql += "  	GROUP BY budget_year																								 " + Environment.NewLine
                If ckShowActivity.Checked Then
                sql += "  		--==GROUP PROJECT==																								 " + Environment.NewLine
                sql += "  		  ,Project_id																									 " + Environment.NewLine
                sql += "  	      ,project_name																									 " + Environment.NewLine
                sql += "  	      ,project_type																									 " + Environment.NewLine
                sql += "  	   ------   ,commitment_budget																						 " + Environment.NewLine
            End If
            sql += "  		  ,country_node_id																								 " + Environment.NewLine
            sql += "  ) AS TB_SUM																											 " + Environment.NewLine



            sql += "  GROUP BY  budget_year																									 " + Environment.NewLine
            If ckShowActivity.Checked Then
                sql += "  		--==GROUP PROJECT==																								 " + Environment.NewLine
                sql += "  		  ,Project_id																									 " + Environment.NewLine
                sql += "  	      ,project_name																									 " + Environment.NewLine
                sql += "  	      ,project_type																									 " + Environment.NewLine
            End If


            sql += " ORDER BY     budget_year														 " + Environment.NewLine

            dt = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If dt.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(dt.Rows.Count, 0) & " รายการ"
            End If
            Session("Search_CLMV_List_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try

        Return dt
    End Function


    Private Sub BindList()

        Dim DT As DataTable

        If ckShowActivity.Checked Then
            tdHeader.Visible = True

            tdfooter1.Visible = False
            tdfooter2.Visible = True
        Else
            tdHeader.Visible = False

            tdfooter1.Visible = True
            tdfooter2.Visible = False
        End If

        DT = GetList()
        If (DT.Rows.Count > 0) Then
            '===Start Footer============================================================================================
            lblCambodia_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Cambodia)", "")).ToString("#,##0.00")
            lblLaos_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Laos)", "")).ToString("#,##0.00")
            lblMyanmar_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Myanmar)", "")).ToString("#,##0.00")
            lblVietnam_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Vietnam)", "")).ToString("#,##0.00")
            lblTotal_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_YEAR)", "")).ToString("#,##0.00")
            '===End Footer===========================================================================================
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()
        Session("Search_CLMV_List") = DT

        AllData = DT
        Pager.SesssionSourceName = "rptPage"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)

        Dim lblBudget_Year As Label = DirectCast(e.Item.FindControl("lblBudget_Year"), Label)
        Dim lblCambodia_Disbursement As Label = DirectCast(e.Item.FindControl("lblCambodia_Disbursement"), Label)
        Dim lblLaos_Disbursement As Label = DirectCast(e.Item.FindControl("lblLaos_Disbursement"), Label)
        Dim lblMyanmar_Disbursement As Label = DirectCast(e.Item.FindControl("lblMyanmar_Disbursement"), Label)
        Dim lblVietnam_Disbursement As Label = DirectCast(e.Item.FindControl("lblVietnam_Disbursement"), Label)
        Dim lblTotal_Amount As Label = DirectCast(e.Item.FindControl("lblTotal_Amount"), Label)

        'Dim tdHeader As HtmlTableCell = DirectCast(e.Item.FindControl("tdHeader"), HtmlTableCell)
        Dim tdLinkProject2 As HtmlTableCell = DirectCast(e.Item.FindControl("tdLinkProject2"), HtmlTableCell)
        'Dim tdfooter As HtmlTableCell = DirectCast(e.Item.FindControl("tdfooter"), HtmlTableCell)

        lblBudget_Year.Text = e.Item.DataItem("Budget_Year").ToString

        '================CLMV===========================

        lblCambodia_Disbursement.Text = Convert.ToDecimal(e.Item.DataItem("Cambodia")).ToString("#,##0.00")
        lblLaos_Disbursement.Text = Convert.ToDecimal(e.Item.DataItem("Laos")).ToString("#,##0.00")
        lblMyanmar_Disbursement.Text = Convert.ToDecimal(e.Item.DataItem("Myanmar")).ToString("#,##0.00")
        lblVietnam_Disbursement.Text = Convert.ToDecimal(e.Item.DataItem("Vietnam")).ToString("#,##0.00")
        lblTotal_Amount.Text = Convert.ToDecimal(e.Item.DataItem("Summary_YEAR")).ToString("#,##0.00")


        If tdHeader.Visible Then
            Dim lblProject_Name As Label = DirectCast(e.Item.FindControl("lblProject_Name"), Label)
            tdLinkProject2.Visible = True
            lblProject_Name.Text = e.Item.DataItem("project_name").ToString

            '============Click To Project===============
            Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

            For i As Integer = 1 To 6
                Dim tdLinkProject As HtmlTableCell = e.Item.FindControl("tdLinkProject" & i)
                tdLinkProject.Style("cursor") = "pointer"
                tdLinkProject.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
            Next
            '================Type And Click To Project===========================
            If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
                lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
            ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
                lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
            ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
                lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
            Else
                lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
            End If


        Else
            tdLinkProject2.Visible = False
        End If



    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        If ckShowActivity.Checked Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCLMVActivity.aspx?Mode=PDF');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCLMV.aspx?Mode=PDF');", True)
        End If


    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click

        If ckShowActivity.Checked Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCLMVActivity.aspx?Mode=EXCEL');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptCLMV.aspx?Mode=EXCEL');", True)
        End If

    End Sub

    Private Sub ckShowActivity_CheckedChanged(sender As Object, e As EventArgs) Handles ckShowActivity.CheckedChanged
        If ckShowActivity.Checked Then
            pnlSearchActivity.Visible = True
        Else
            ddlProject_Type.SelectedIndex = 0
            txtSearch_Project.Text = ""
            pnlSearchActivity.Visible = False
        End If
        BindList()
    End Sub

#End Region

End Class
