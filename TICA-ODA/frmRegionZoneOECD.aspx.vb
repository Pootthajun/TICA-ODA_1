﻿
Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports Constants

Partial Class frmRegionZoneOECD
    Inherits System.Web.UI.Page

    Public BL As New ODAENG

    Public Property AllData As DataTable
        Get
            Try
                Return Session("RegionZoneOEDCData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("RegionZoneOEDCData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aRegionZoneOECD")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            BindData()
            Authorize()
            pnlAdSearch.Visible = False
            BL.Bind_DDL_Region_forSearch(ddlRegion)
        End If
    End Sub


    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.OECD_RegionZone & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnStatusReady As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusReady"), LinkButton)
            Dim btnStatusDisabled As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusDisabled"), LinkButton)
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnStatusReady.Enabled = False
                btnStatusDisabled.Enabled = False
                HeadEdit.Visible = False
                ColEdit.Visible = False
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "select Rz.id,Rz.regionoda_id ,r.region_name,Rz.regionoda_name,Rz.regionoda_name_Th,Rz.ACTIVE_STATUS"
        sql &= " From TB_Region R LEFT JOIN TB_Regionzoneoda Rz on R.id = Rz.regionoda_id WHERE 1=1"

        If ddlRegion.SelectedValue <> "" Then
            sql &= " and Rz.regionoda_id = '" & ddlRegion.SelectedValue & "'"
        End If

        If txtSearch.Text.Trim <> "" Then
            sql &= " and (Rz.regionoda_name LIKE '%" + txtSearch.Text.Trim() + "%' Or Rz.regionoda_name_Th LIKE '%" + txtSearch.Text.Trim() + "%')"
        End If

        If ddlStatus.SelectedValue <> "" Then
            sql &= " And Rz.ACTIVE_STATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
        End If
        sql &= " order by r.region_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "RegionZoneOEDCData"
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        BindData()
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditRegionZoneOECD.aspx")
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblNameZoneTh As Label = DirectCast(e.Item.FindControl("lblNameZoneTh"), Label)
        Dim lblNameZoneEn As Label = DirectCast(e.Item.FindControl("lblNameZoneEn"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("region_name").ToString()
        lblNameZoneTh.Text = drv("regionoda_name_Th").ToString()
        lblNameZoneEn.Text = drv("regionoda_name").ToString()

        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False

        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If
    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditRegionZoneOECD.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditRegionZoneOECD.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then
            Dim tran As New TransactionDB
            Dim lnq As New TbRegionzoneodaLinqDB

            Dim ret As ExecuteDataInfo = lnq.DeleteByPK(e.CommandArgument, tran.Trans)
            If ret.IsSuccess = True Then
                tran.CommitTransaction()
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
            Else
                tran.RollbackTransaction()
                Dim _err As String = tran.ErrorMessage()
                Alert("ไม่สามารถลบข้อมูลเรียบร้อยแล้ว")
            End If
            lnq = Nothing
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo

            ret = BL.EditStatusAllY(e.CommandArgument, "TB_Regionzoneoda")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllN(e.CommandArgument, "TB_Regionzoneoda")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        End If
        BindData()
    End Sub
    Protected Sub Alert(ByVal message As String) 'Function Alert
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
    Protected Sub btnSearch_Click1(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        clearFormSearch()
        BindData()
        pnlAdSearch.Visible = False
    End Sub

    Function clearFormSearch()
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
        ddlRegion.SelectedValue = ""
    End Function
End Class


