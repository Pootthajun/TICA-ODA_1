﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Public Class frmEditCountryGroup
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditCountryGroupID As Long
        Get
            Try
                Return ViewState("CountryGroupID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("CountryGroupID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aCountryGroup")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditCountryGroupID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditCountryGroupID = 0
                End Try
            End If

            DisplayEditData()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtName.Enabled = False
            txtDescription.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub

    Protected Sub DisplayEditData()
        ClearData()

        If EditCountryGroupID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_CountryGroup(EditCountryGroupID, "")
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0)("countrygroup_name").ToString()
                txtDescription.Text = dt.Rows(0)("Detail").ToString()

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)

        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbCountrygroupLinqDB
                Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByCOUNTRYGROUP_NAME(txtName.Text.Trim, EditCountryGroupID, Nothing)
                If chkDupName = True Then
                    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                    Exit Sub
                End If

                lnqLoc.ChkDataByPK(EditCountryGroupID, trans.Trans)


                With lnqLoc
                    .COUNTRYGROUP_NAME = txtName.Text.Replace("'", "''")
                    .DETAIL = txtDescription.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmCountryGroup.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    Alert(ret.ErrorMessage.Replace("'", """"))
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                Alert(ex.Message.Replace("'", """"))
            End Try
        End If

    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True

        If txtName.Text = "" Then
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
            ret = False
        End If

        Return ret
    End Function

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

        lblName.Text = drv("countrygroup_name").ToString()
        lblDetail.Text = drv("detail").ToString()
    End Sub

    Public Sub ClearData()
        txtName.Text = ""
        txtDescription.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub
    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmCountryGroup.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class