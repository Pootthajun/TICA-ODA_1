﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports ODAENG
Imports Constants
Imports ProcessReturnInfo

Public Class frmEditCountry
    Inherits System.Web.UI.Page

    Protected ENG As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditCountryID As String
        Get
            Try
                Return ViewState("CountryID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("CountryID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            txtCountryNameTH.Focus()

            If Request.QueryString("NODE_ID") <> "" Then
                lblTitle1.Text = "Edit"
                lblTitle2.Text = "Edit"

                BindDataToAddCountry()
                DisplayEditData()

            Else
                BindDataToAddCountry()
                EditCountryID = ""
                lblTitle1.Text = "Add"
                lblTitle2.Text = "Add"
            End If

        End If
    End Sub
    Protected Sub BindDataToAddCountry()

        ENG.Bind_DDL_Region(ddlRegion)
        ENG.Bind_DDL_CountryGroup(ddlCountryGruop)
        ENG.Bind_DDL_RegionZone(ddlCountryZone)


    End Sub

    Protected Sub DisplayEditData()

        Dim Sql As String = "SELECT id,node_id,name_th,name_en,description,active_status" + Environment.NewLine
        Sql += "FROM TB_OU_Country WHERE node_id=@_ID"
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ID", Request.QueryString("NODE_ID"))

        Dim trans As New TransactionDB
        Dim lnq As New TbOuCountryLinqDB

        Dim dt As DataTable = lnq.GetListBySql(Sql, trans.Trans, p)
        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            If dt.Rows.Count > 0 Then

                'For i = 0 To dt.Rows.Count - 1
                '    EditCountryID = dt(i)("node_id").ToString()
                '    txtNameTH.Text = dt(i)("name_th").ToString()
                '    txtNameEN.Text = dt(i)("name_en").ToString()
                '    txtDescription.Text = dt(i)("description").ToString()
                '    If dt(i)("active_status").ToString() = "Y" Then
                '        chkACTIVE_STATUS.Checked = True
                '    Else
                '        chkACTIVE_STATUS.Checked = False
                '    End If
                'Next
            End If
        Else
            trans.RollbackTransaction()
            Dim _err As String = lnq.ErrorMessage
        End If
        lnq = Nothing



    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)

        If txtCountryNameTH.Text = "" And txtCountryNameEN.Text = "" Then
            Alert("กรุณากรอกชื่อประเทศภาษาไทยและภาษาอังกฤษ")
            txtCountryNameTH.Focus()

            Exit Sub
        End If

        If ddlRegion.SelectedIndex = 0 Then
            Alert("กรุณาเลือกภูมิภาค")
            Exit Sub
        ElseIf ddlCountryGruop.SelectedIndex = 0 Then
            Alert("กรุณาเลือกกลุ่มประเทศ")
            Exit Sub
        ElseIf ddlCountryZone.SelectedIndex = 0 Then
            Alert("กรุณาเลือกโซนประเทศ")
            Exit Sub
        End If

        Dim linqCountry As New TbOuCountryLinqDB
        linqCountry.NODE_ID = EditCountryID
        linqCountry.NAME_TH = txtCountryNameTH.Text.Trim.Replace("'", "''")
        linqCountry.NAME_EN = txtCountryNameEN.Text.Trim.Replace("'", "''")
        linqCountry.DESCRIPTION = txtDescription.Text.Trim.Replace("'", "''")


        If chkACTIVE_STATUS.Checked Then
            linqCountry.ACTIVE_STATUS = "Y"
        Else
            linqCountry.ACTIVE_STATUS = "N"
        End If

        Dim dtR As New DataTable 'Country Region
        dtR.Columns.Add("country_region_id")
        dtR.Columns.Add("country_id")
        Dim drR As DataRow
        drR = dtR.NewRow
        drR("country_region_id") = ddlRegion.SelectedValue.ToString()
        drR("country_id") = ""
        dtR.Rows.Add(drR)

        Dim dtG As New DataTable 'Country Group
        dtG.Columns.Add("country_group_id")
        dtG.Columns.Add("country_id")
        Dim drG As DataRow
        drG = dtG.NewRow
        drG("country_group_id") = ddlCountryGruop.SelectedValue.ToString()
        drG("country_id") = ""
        dtG.Rows.Add(drG)


        Dim dtZ As New DataTable 'Country Zone
        dtZ.Columns.Add("country_zone_id")
        dtZ.Columns.Add("country_id")
        Dim drZ As DataRow
        drZ = dtZ.NewRow
        drZ("country_zone_id") = ddlCountryZone.SelectedValue.ToString()
        drZ("country_id") = ""
        dtZ.Rows.Add(drZ)

        Dim ret As New ProcessReturnInfo
        ret = ENG.SaveOUCountry(linqCountry, dtR, dtG, dtZ, UserName)
        'ret = ENG.SaveOUCountry(linqCountry,,, "Administrator")
        If ret.IsSuccess = True Then

            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
        Else
            Alert(ret.ErrorMessage)
        End If



    End Sub
    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Session.Remove("SESSION_ID")
        Response.Redirect("frmStructure.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class