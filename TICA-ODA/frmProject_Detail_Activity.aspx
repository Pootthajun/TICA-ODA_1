﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmProject_Detail_Activity.aspx.vb" Inherits="frmProject_Detail_Activity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/usercontrol/UCProjectComponent.ascx" TagPrefix="uc1" TagName="UCProjectComponent" %>
<%@ Register Src="~/usercontrol/UCImplementingAgency.ascx" TagPrefix="uc1" TagName="UCImplementingAgency" %>
<%@ Register Src="~/usercontrol/UCCoFunding.ascx" TagPrefix="uc1" TagName="UCCoFunding" %>
<%@ Register Src="~/usercontrol/UCInkind.ascx" TagPrefix="uc1" TagName="UCInkind" %>
<%@ Register Src="~/usercontrol/UCContact.ascx" TagPrefix="uc1" TagName="UCContact" %>
<%@ Register Src="usercontrol/UCFileUploadList.ascx" TagName="UCFileUploadList" TagPrefix="uc2" %>
<%@ Register Src="~/usercontrol/UCActivityBudget.ascx" TagPrefix="uc4" TagName="UCActivityBudget" %>
<%@ Register Src="usercontrol/UCActivityTreeList.ascx" TagName="UCActivityTreeList" TagPrefix="uc3" %>
<%@ Register Src="usercontrol/UCActivityTabProject.ascx" TagName="UCActivityTabProject" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .tdborder {
            border-style: outset;
            border-width: 1px;
        }

        .tdbordertb {
            border-top-style: outset;
            border-bottom-style: outset;
            border-top-width: 1px;
            border-bottom-width: 1px;
        }

        .tdborderr {
            border-right-style: outset;
            border-right-width: 1px;
        }

        .tdborderl {
            border-left-style: outset;
            border-left-width: 1px;
        }

        .tdhearderinfo {
            text-align: center;
            background-color: #CED8F6;
        }

        .tdhearder {
            text-align: center;
            background-color: #F4F2EA;
        }

        .tdinprogress {
            /*background-color:#E1FFD4;*/
            background-color: #E1FFD4;
            
            border-top-style: solid;
            /*border-right-style:solid;*/
            border-bottom-style: solid;
            border-top-width: 1px;
            /*border-right-width:1px;*/
            border-bottom-width: 1px;
            border-top-color: #0AFB43;
            /*border-right-color:#00ff21;*/
            border-bottom-color: #0AFB43;
        }

        .tdnoneprogress {
            /*background-color:#E1FFD4;*/
            background-color: #ffffff;
            
            border-top-style: solid;
            /*border-right-style:solid;*/
            border-bottom-style: solid;
            border-top-width: 1px;
            /*border-right-width:1px;*/
            border-bottom-width: 1px;
            border-top-color: #0AFB43;
            /*border-right-color:#00ff21;*/
            border-bottom-color: #0AFB43;
        }
        .tdallprogress {
            /*background-color:#A7D991;*/
            background-color: #FAFAFA;
            border-top-style: solid;
            /*border-right-style:solid;*/
            border-bottom-style: solid;
            border-top-width: 1px;
            /*border-right-width:1px;*/
            border-bottom-width: 1px;
            border-top-color: #0AFB43;
            /*border-right-color:#00ff21;*/
            border-bottom-color: #0AFB43;
        }

        .tdblank {
            background-color: #ffffff;
        }

        .btn-print {
            margin: 10px 15px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
             <div class="row" style="margin-top:-5px;">
                    <div class="col-md-12">
                        <h3>
                            <asp:Label ID="lblHeadProjectType" runat="server" Text=""></asp:Label>  
                        </h3>
                    </div>
                    <div class="col-md-12">
                        <table  class="table" style="padding: 0px;margin: 0px;" >
                            <tr>
                                <td style="width:150px; text-align:center ">
                                    <asp:TextBox ID="txtProjectID"  Enabled="false"  ReadOnly="true"  style=" text-align:center ; border-style:None;font-size:14pt;width:100%;background-color: #ecf0f5;" runat="server" Text=""  ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProjectName" runat="server" onkeyup="auto_grow(this)"  TextMode="MultiLine" style=" min-height:40px; border-style:None;font-size:14pt;width:100%;background-color: #f4f4f4;" MaxLength="500" ></asp:TextBox>
                                    
                                </td>
                            </tr>

                        </table>
                    </div>
            </div>


            <ol class="breadcrumb">
                <li><a href="frmProjectStatus.aspx"><i class="fa fa-area-chart"></i>Overall</a></li>
                <li><a href="#" runat="server" id="nav">
                    <asp:Label ID="lblNavProjectType" runat="server" Text=""></asp:Label></a></li>
                <li class="active">
                    <asp:Label ID="lblProjectType" runat="server" Text=""></asp:Label>
                    Detail
                </li>
            </ol>
        </section>
        <!-- End Content Header (Page header) -->



        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="" id="liTabProjectDetail" runat="server">
                                            <asp:LinkButton ID="btnTabProjectDetail" runat="server"  style="padding :4px 10px 5px 20px;" >
                                        <div class="iti-flag gb" style="float:left; margin-top:3px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-list-alt distance"></i>Information(ข้อมูลโครงการ)</span></h5>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="" id="liTabActivity" runat="server">
                                            <asp:LinkButton ID="btnTabActivity" runat="server"  style="padding :4px 4px 5px 5px;">
                                <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer; "></div><h5><span style="cursor:pointer"><i class="fa fa-th-list distance"></i>Activity(กิจกรรม)</span></h5>
                                            </asp:LinkButton>
                                        </li>

                                        <%--Print Button--%>
                                        <li class="pull-right">
                                            <div class="btn-group" data-toggle="tooltip" title="วางกิจกรรมที่คัดลอก">
                                                <asp:LinkButton ID="btnPaste" runat="server" CssClass=" btn btn-warning  btn-flat dropdown-toggle btn-print"  style="margin-top:2px;"><i class="fa fa-paste"></i></asp:LinkButton>
                                            </div>

                                            <div class="btn-group" data-toggle="tooltip" title="Back to Activity Detials">
                                                <asp:LinkButton ID="btnBack" runat="server" CssClass=" btn btn-google btn-flat dropdown-toggle btn-print"  style="margin-top:2px;"><i class="fa fa-reply"></i></asp:LinkButton>
                                            </div>
                                           
                                            <%--<div class="btn-group" data-toggle="tooltip" title="Add Activity">
                                                <asp:LinkButton ID="btnAddActivity" runat="server" Visible ="false"  CssClass="btn btn-primary btn-flat dropdown-toggle btn-print"  style="margin-top:2px;"><i class="fa fa-plus"></i></asp:LinkButton>
                                            </div>--%>
                                            <div class="btn-group" data-toggle="tooltip" title="Add Activity">
                                                <asp:LinkButton ID="btnAdd" runat="server" Visible="false" CssClass="btn btn-primary btn-flat dropdown-toggle btn-print" Style="margin-top: 2px;"  data-toggle="dropdown" ><i class="fa fa-plus"></i></asp:LinkButton>

                                                <ul class="dropdown-menu pull-right"  >

                                                    <li id="liedit" runat="server">
                                                        <asp:LinkButton ID="btnAddFolder" runat="server">
                                                      <i class="fa fa-folder   "></i> Folder</asp:LinkButton>
                                                    </li>
                                                    <li id="lidelete" runat="server">
                                                        <asp:LinkButton ID="btnAddAct" runat="server"><i class="fa fa-font "></i> Activity</asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown"  style="margin-top:2px;">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <div style="">

                                        <%--Hide ifram for preview--%>
                                        <iframe id="myframe" src="" frameborder="0" height="0px" width="0px" runat="server"></iframe>



                                        <%--tabActivity--%>
                                        <asp:Panel ID="tabActivity" runat="server">

                                            <div class="tab-pane active" id="tab_2">
                                                <div class="box-body" style="margin-top: -20px;">
                                                    <%--<asp:Panel ID="pnlActivity" runat="server" Style="overflow-x: scroll; overflow-y: auto" Width="1000px" Height="400px">
                                                        <uc3:UCActivityTreeList ID="UCActivityTreeList1" runat="server" />
                                                        <uc5:UCActivityTabProject ID="UCActivityTabProject1" runat="server" />
                                                    </asp:Panel>--%>
                                                    <asp:Panel ID="pnlActivityList" runat="server" >
                                                        
                                                        <uc3:UCActivityTreeList ID="UCActivityTreeList1" runat="server" />


                                                                    <asp:Literal ID="ltActivity" runat="server" Visible ="false" ></asp:Literal>

                                                        
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlActivityDetail" runat="server" Style=" margin-top: -10px;" >
                                                        <uc5:UCActivityTabProject ID="UCActivityTabProject1" runat="server" />
                                                    </asp:Panel>


                                                </div>
                                                <!-- /.box-body -->
                                            </div>




                                        </asp:Panel>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Custom Tabs -->

                        <%--<div class="box-footer">
                           
                        </div>--%>
                    </div>
                </div>

                <script type="text/javascript">
                    // Specify the normal table row background color
                    //   and the background color for when the mouse 
                    //   hovers over the table row.

                    var TableBackgroundNormalColor = "#ffffff";
                    var TableBackgroundMouseoverColor = "#F2F2F2";

                    // These two functions need no customization.
                    function ChangeBackgroundColor(row, text) {
                        row.style.backgroundColor = TableBackgroundMouseoverColor;
                        text.style.backgroundColor = TableBackgroundMouseoverColor;
                    }

                    function RestoreBackgroundColor(row, text) {
                        row.style.backgroundColor = TableBackgroundNormalColor;
                        text.style.backgroundColor = TableBackgroundNormalColor;
                    }


                </script>

                <asp:Panel CssClass="modal" ID="pnlFolder" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                    <div class="modal-dialog" style="width: 700px; margin-top: 150px;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><i class="fa fa-folder   " aria-hidden="true"></i> เพิ่มชื่อ Folder</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="margin-left: 20px;"> 
                                            <asp:TextBox CssClass="form-control m-b" ID="txtFolder_Name" runat="server" placeholder="Name"></asp:TextBox>

                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="lnkDialogSave" runat="server" CssClass="btn btn-success">
                              Contunues >>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDialogCancel" runat="server" CssClass="btn btn-google">
                            Cancel
                                </asp:LinkButton>


                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />



</asp:Content>



