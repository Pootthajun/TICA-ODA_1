﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports System.Data.SqlClient
Public Class frmEditSubSector
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditSubSectorID As Long
        Get
            Try
                Return ViewState("SubSectorID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("SubSectorID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSubSector")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            txtName.Focus()
            BindPerposcat()
            BL.SetTextIntKeypress(txtCRScode)
            BL.SetTextIntKeypress(txtDAC5code)

            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditSubSectorID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditSubSectorID = 0
                End Try
            End If

            DisplayEditData()
            Authorize()
        End If
    End Sub
    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtName.Enabled = False
            txtDescription.Enabled = False
            txtCRScode.Enabled = False
            txtDAC5code.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub
    Protected Sub DisplayEditData()
        ClearEditForm()

        If EditSubSectorID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_SubSector(EditSubSectorID, "")
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0)("Name").ToString()
                txtDescription.Text = dt.Rows(0)("Description").ToString()
                txtDAC5code.Text = dt.Rows(0)("DAC5code").ToString()
                txtCRScode.Text = dt.Rows(0)("CRScode").ToString()

                ddlPurposeCategory.SelectedValue = dt.Rows(0)("CategoryID").ToString()

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If

    End Sub

    Protected Sub BindPerposcat()

        Dim sqlRegion As String = "SELECT id,perposecat_name FROM tb_purposecat WHERE 1=1 AND isnull(active_status,'Y')='Y'"
        Dim trans As New TransactionDB
        Dim lnq As New TbPerposecatLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sqlRegion, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            Dim BindDataToddl As New ODAENG
            BindDataToddl.BindData_dll(ddlPurposeCategory, "", dt, "Select Purpose Category", "perposecat_name")
        Else
            trans.RollbackTransaction()
            Dim _err As String = trans.ErrorMessage()
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbPerposeLinqDB
                Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByNAME(txtName.Text.Trim, EditSubSectorID, Nothing)
                If chkDupName = True Then
                    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                    Exit Sub
                End If

                lnqLoc.ChkDataByPK(EditSubSectorID, trans.Trans)

                With lnqLoc
                    .NAME = (txtName.Text.Trim)
                    .CATEGORYID = (ddlPurposeCategory.SelectedValue)
                    .CATEGORY = (ddlPurposeCategory.SelectedItem.Text)

                    If txtDAC5code.Text <> "" Then
                        .DAC5CODE = (txtDAC5code.Text.Trim())
                    End If

                    If txtCRScode.Text <> "" Then
                        .CRSCODE = (txtCRScode.Text.Trim())
                    End If

                    .DESCRIPTION = (txtDescription.Text.Trim())

                    If chkACTIVE_STATUS.Checked = True Then
                        .ACTIVE_STATUS = "Y"
                    Else
                        .ACTIVE_STATUS = "N"
                    End If
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmSubSector.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    Alert(ret.ErrorMessage.Replace("'", """"))
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                Alert(ex.Message.Replace("'", """"))
            End Try
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim check As Boolean = True

        If txtName.Text = "" Then
            check = False
            Alert("กรุณากรอกข้อมูลภาค/วัตถุประสงค์")
        End If

        If ddlPurposeCategory.SelectedValue = "" Or ddlPurposeCategory.SelectedItem.Text = "" Then
            check = False
            Alert("กรุณาเลือกหมวดหมู่วัตถุประสงค์")
        End If

        Return check
    End Function
    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmSubSector.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
    Sub ClearEditForm()
        txtName.Text = ""
        txtDescription.Text = ""
        txtCRScode.Text = ""
        txtDAC5code.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub
End Class