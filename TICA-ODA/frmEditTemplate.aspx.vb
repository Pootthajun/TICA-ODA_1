﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmEditTemplate
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditTemplateID As Long
        Get
            Try
                Return ViewState("TemplateID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("TemplateID") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    'Dim template_name As Label = DirectCast(FindControl("txtNames"), Label)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuFinance")
        li.Attributes.Add("class", "active")

        Dim li2 As HtmlGenericControl = Me.Page.Master.FindControl("mnuMasterExpense")
        li2.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aTemplate")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then

            If Not Request.QueryString("id") Is Nothing Then
                Try
                    EditTemplateID = CInt(Request.QueryString("id"))
                Catch ex As Exception
                    EditTemplateID = 0
                End Try

            End If

            BindList()
            Authorize()
            BindDataGroupCheck()

        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Template & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnSave.Visible = False
            txtNames.Enabled = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim checkAll As ImageButton = DirectCast(rptList.Items(i).FindControl("checkAll"), ImageButton)
            Dim check As ImageButton = DirectCast(rptList.Items(i).FindControl("check"), ImageButton)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                checkAll.Enabled = False
                check.Enabled = False
            End If
        Next
    End Sub

    Private Sub BindList()

        Dim DT_GetList_SubExpense As DataTable = BL.GetList_SubExpense_ForTemplate(0, "", "", "")
        rptList.DataSource = DT_GetList_SubExpense
        rptList.DataBind()

        txtNames.Text = ""
        lblSub.Text = ""

        If EditTemplateID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_Template_Name(EditTemplateID, "")
            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("template_name").ToString()
            End If

            dt = BL.GetList_Template(EditTemplateID, "")

            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("template_name").ToString()
                lblSub.Text = dt.Rows(0)("template_id").ToString()

                Dim get_template As New DataTable
                get_template = BL.GetList_Template(0, "")

                For Each row As DataRow In get_template.Rows
                    Dim tmp_name As String
                    lblSub.Text = row.Item("sub_expense_id").ToString()
                    tmp_name = row.Item("template_name").ToString()

                    If tmp_name = txtNames.Text Then

                        For Each item As RepeaterItem In rptList.Items
                            Dim lblID As Label = DirectCast(item.FindControl("lblID"), Label)
                            Dim chkCheck As CheckBox = DirectCast(item.FindControl("chkCheck"), CheckBox)

                            If lblSub.Text = lblID.Text Then
                                chkCheck.Checked = True
                                Dim check As ImageButton = item.FindControl("check")
                                check.ImageUrl = "images/check.png"
                            End If
                        Next

                    End If
                Next

            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
            BindDataGroupCheck()
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
            BindDataGroupCheck()
        End If

    End Sub
    Dim GroupName As String = ""
    Dim GroupID As String = ""
    Dim GroupSubId As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblNames"), Label)
        Dim lblGroupName As Label = DirectCast(e.Item.FindControl("lblGroupName"), Label)
        Dim lblGroupID As Label = DirectCast(e.Item.FindControl("lblGroupID"), Label)
        Dim chkCheck As CheckBox = DirectCast(e.Item.FindControl("chkCheck"), CheckBox)
        Dim ChkAll As CheckBox = DirectCast(e.Item.FindControl("ChkAll"), CheckBox)

        'test
        Dim checkAll As ImageButton = e.Item.FindControl("checkAll")

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = e.Item.DataItem("sub_name").ToString

        If GroupName <> e.Item.DataItem("group_name").ToString Then
            GroupName = e.Item.DataItem("group_name").ToString
            lblGroupName.Text = GroupName
            checkAll.Visible = True
        Else
            checkAll.Visible = False
        End If
        lblGroupID.Text = e.Item.DataItem("expense_group_id").ToString
        'If GroupID <> e.Item.DataItem("expense_group_id").ToString Then
        '    GroupID = e.Item.DataItem("expense_group_id").ToString
        '    lblGroupID.Text = GroupID
        'End If
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim checkAll As ImageButton = e.Item.FindControl("checkAll")
        Dim check As ImageButton = e.Item.FindControl("check")

        Dim GroupName As String = ""
        Dim GroupID As String = ""
        Dim GroupSubName As String = ""
        Dim GroupSubID As String = ""
        Dim SubID As String = ""
        Dim count As Integer = 0

        If e.CommandName = "checkAll" Then 'select all
            If checkAll.ImageUrl = "images/none.png" Then
                checkAll.ImageUrl = "images/check.png"
                Dim dt As New DataTable
                dt = BL.GetList_GroupExpense(e.CommandArgument, "","")

                If dt.Rows.Count > 0 Then
                    GroupName = dt.Rows(0)("group_name").ToString()
                    GroupID = dt.Rows(0)("id").ToString()

                    Dim get_sub_ex As New DataTable
                    get_sub_ex = BL.GetList_SubExpense(0, "", "", "")

                    For Each row As DataRow In get_sub_ex.Rows
                        SubID = row.Item("id").ToString()
                        GroupSubID = row.Item("expense_group_id").ToString()
                        GroupSubName = row.Item("group_name").ToString()

                        If GroupSubName = GroupName Then
                            For Each item As RepeaterItem In rptList.Items
                                Dim lblID As Label = DirectCast(item.FindControl("lblID"), Label)
                                Dim chkCheck As CheckBox = DirectCast(item.FindControl("chkCheck"), CheckBox)
                                If GroupSubID = GroupID Then
                                    If SubID = lblID.Text Then
                                        Dim check1 As ImageButton = item.FindControl("check")
                                        check1.ImageUrl = "images/check.png"
                                        chkCheck.Checked = True
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
                '----------------------------------------------------------------------------------------------------
            Else  'not select all
                checkAll.ImageUrl = "images/none.png"
                Dim dt As New DataTable
                dt = BL.GetList_GroupExpense(e.CommandArgument, "", "")

                If dt.Rows.Count > 0 Then
                    GroupName = dt.Rows(0)("group_name").ToString()
                    GroupID = dt.Rows(0)("id").ToString()

                    Dim get_sub_ex As New DataTable
                    get_sub_ex = BL.GetList_SubExpense(0, "", "", "")

                    For Each row As DataRow In get_sub_ex.Rows
                        SubID = row.Item("id").ToString()
                        GroupSubID = row.Item("expense_group_id").ToString()
                        GroupSubName = row.Item("group_name").ToString()
                        If GroupSubName = GroupName Then

                            For Each item As RepeaterItem In rptList.Items
                                Dim lblID As Label = DirectCast(item.FindControl("lblID"), Label)
                                Dim chkCheck As CheckBox = DirectCast(item.FindControl("chkCheck"), CheckBox)
                                If GroupSubID = GroupID Then
                                    If SubID = lblID.Text Then
                                        Dim check1 As ImageButton = item.FindControl("check")
                                        check1.ImageUrl = "images/none.png"
                                        chkCheck.Checked = False
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
            End If
        End If
        '--------------------------------------------------------------------------------------------------------
        If e.CommandName = "check" Then 'เปลี่ยนจาก check เป็น none และ check all เป็น none ด้วย
            If check.ImageUrl = "images/check.png" Then
                check.ImageUrl = "images/none.png"
                BindDataGroupCheck()
                Dim dt As New DataTable
                dt = BL.GetList_SubExpense(e.CommandArgument, "", "", "")

                If dt.Rows.Count > 0 Then
                    SubID = dt.Rows(0)("id").ToString()
                    GroupSubID = dt.Rows(0)("expense_group_id").ToString()
                    GroupSubName = dt.Rows(0)("group_name").ToString()

                    Dim get_sub_ex As New DataTable
                    get_sub_ex = BL.GetList_GroupExpense(0, "", "")

                    For Each row As DataRow In get_sub_ex.Rows
                        GroupName = row.Item("group_name").ToString()
                        GroupID = row.Item("id").ToString()

                        If GroupName = GroupSubName Then
                            For Each item As RepeaterItem In rptList.Items
                                Dim lblGroupID As Label = DirectCast(item.FindControl("lblGroupID"), Label)

                                If GroupID = lblGroupID.Text Then
                                    Dim checkAll1 As ImageButton = item.FindControl("checkAll")
                                    checkAll1.ImageUrl = "images/none.png"
                                End If
                            Next
                        End If
                    Next
                End If
                '-----------------------------------------------------------------------------------------------------------------
            Else  'เปลี่ยนจาก none เป็น check
                check.ImageUrl = "images/check.png"
                BindDataGroupCheck()
            End If
        End If
    End Sub

    Function BindDataGroupCheck() As DataTable
        Dim otherDT As New DataTable
        otherDT.Columns.Add("GroupCheck")
        Dim drow As DataRow

        For Each grv As RepeaterItem In rptList.Items
            Dim checkrow As ImageButton = grv.FindControl("check")

            If checkrow.ImageUrl = "images/check.png" Then
                Dim lblID As Label = grv.FindControl("lblID")
                Dim lblGroupID As Label = grv.FindControl("lblGroupID")

                drow = otherDT.NewRow
                drow("GroupCheck") = lblGroupID.Text
                otherDT.Rows.Add(drow)
            End If
        Next

        Dim dt1 As New DataTable
        dt1.Columns.Add("GroupCheck")
        dt1.Columns.Add("Count")
        Dim query = (From dr In (From d In otherDT.AsEnumerable Select New With {.date = d("GroupCheck")}) Select dr.date Distinct)
        For Each colName As String In query
            Dim cName = colName
            Dim cCount = (From row In otherDT.Rows Select row Where row("GroupCheck").ToString = cName).Count
            dt1.Rows.Add(colName, cCount)
        Next
        Repeater1.DataSource = dt1
        Repeater1.DataBind()

        CheckCountRow()
    End Function

    Function CheckCountRow()
        Dim count As Integer = 0
        Dim dt As New DataTable
        dt = BL.GetList_SubExpenseNumRow() ' ดึงค่า count ทั้งหมดออกมาของ Item Template 
        For Each row As DataRow In dt.Rows
            GroupID = row.Item("expense_group_id").ToString()
            Dim num As String = row.Item("num").ToString()

            For i As Integer = 0 To Repeater1.Items.Count - 1 'ดึงข้อมูลใน Repeater ที่เก็บค่า check
                Dim lblGroupcheck As Label = DirectCast(Repeater1.Items(i).FindControl("lblGroupcheck"), Label)
                Dim lblCount As Label = DirectCast(Repeater1.Items(i).FindControl("lblCount"), Label)
                GroupSubId = lblGroupcheck.Text
                count = lblCount.Text

                If GroupID = GroupSubId Then

                    For Each item As RepeaterItem In rptList.Items
                        Dim lblGroupID As Label = DirectCast(item.FindControl("lblGroupID"), Label)
                        If GroupSubId = lblGroupID.Text Then
                            If num = count Then
                                Dim checkAll1 As ImageButton = item.FindControl("checkAll")
                                checkAll1.ImageUrl = "images/check.png"
                            End If
                        End If
                    Next
                End If
            Next
        Next
    End Function

    Protected Sub Repeater1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles Repeater1.ItemDataBound 'Count Check
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblGroupcheck As Label = DirectCast(e.Item.FindControl("lblGroupcheck"), Label)
        Dim lblCount As Label = DirectCast(e.Item.FindControl("lblCount"), Label)

        lblGroupcheck.Text = e.Item.DataItem("GroupCheck").ToString
        lblCount.Text = e.Item.DataItem("Count").ToString
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim ret As New ExecuteDataInfo

                Dim lnqLoc As New TbTemplateLinqDB
                lnqLoc.ChkDataByPK(EditTemplateID, trans.Trans)
                lnqLoc.TEMPLATE_NAME = txtNames.Text

                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If

                If ret.IsSuccess = True Then
                    ret = BL.DeleteTemplateExpense(lnqLoc.ID, trans)
                    If ret.IsSuccess = True Then

                        For Each grv As RepeaterItem In rptList.Items

                            Dim lnqLoc2 As New TbTemplateItemLinqDB
                            'Dim chkCheck As CheckBox = grv.FindControl("chkCheck")
                            Dim check As ImageButton = grv.FindControl("check")


                            If check.ImageUrl = "images/check.png" Then
                                Dim lblID As Label = grv.FindControl("lblID")
                                '-------------------------------------------------

                                lnqLoc2.ChkDataByPK(EditTemplateID, trans.Trans)
                                With lnqLoc2
                                    .TEMPLATE_ID = lnqLoc.ID
                                    .SUB_EXPENSE_ID = lblID.Text
                                End With

                                ret = lnqLoc2.InsertData(UserName, trans.Trans)
                                If ret.IsSuccess = False Then
                                    Exit For
                                End If

                            End If
                        Next
                    End If
                End If

                '-------------------------------------------------
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    BindList()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmTemplate.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.Message.Replace("'", """") & "');", True)
            End Try
        End If


    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True

        If txtNames.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุชื่อรูปแบบ');", True)
            ret = False
        End If
        Return ret
    End Function

    Private Sub btnCancle_Click(sender As Object, e As System.EventArgs) Handles btnCancle.Click
        Response.Redirect("frmTemplate.aspx")
    End Sub
End Class
