﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptSummary_ByProject_In_Period.aspx.vb" Inherits="rptSummary_ByProject_In_Period" %>


<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานการให้ความช่วยเหลือแก่ประเทศ/ภูมิภาค </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายในองค์กร</a></li>
                <li class="active">รายงานการให้ความช่วยเหลือแก่ประเทศ/ภูมิภาค</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top: 30px; */"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"><span style="color: red;">*</span>    Report Type :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                        <asp:ListItem Value="-1" Text="...." Selected></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Project"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Non Project"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Loan"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Contribution"></asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-sm-6" id="divSearch_Year" runat="server" >
                                                <label for="inputname" class="col-sm-3 control-label line-height">Budget Year :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width:80%">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Country :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 80%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Region :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control select2" Style="width: 80%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>


                                        </div>


                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body"  style ="overflow-y :auto;">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <%--<tr class="bg-gray">

                                                    <th>ภูมิภาค/ประเทศ</th>
                                                    <th>โครงการที่อยู่ระหว่างดำเนินการ<br />
                                                        (จำนวน)</th>
                                                    <th>ทุนการศึกษา *</th>
                                                    <th>ทุนฝึกอบรม *</th>
                                                    <th>ทุนอื่นๆ *</th>
                                                    <th>มูลค่า</th>
                                                </tr>--%>
                                                <tr class="bg-gray">
                                                    <th rowspan="2" style="vertical-align: middle;">ประเทศ / ภูมิภาค</th>
                                                    <th rowspan="2" style="vertical-align: middle;">โครงการที่อยู่ระหว่าง<br />ดำเนินการ (จำนวน)</th>
                                                    <th colspan="11">Component<br />
                                                        (ประเภทความร่วมมือ)</th>
                                                    <th rowspan="2" style="vertical-align: middle;">Amount<br />
                                                        (มูลค่า)</th>
                                                </tr>
                                                <tr class="bg-gray">
                                                   <th title ="ทุนศึกษา">Bachelor</th>
                                                    <th title ="ทุนฝึกอบรม">Training</th>
                                                    <th title ="ผู้เชี่ยวชาญ">Expert</th>
                                                    <th title ="อาสาสมัคร">Volunteer</th>
                                                    <th title ="วัสดุอุปกรณ์">Equipment</th>

                                                    <th title ="เยี่ยมชมการศึกษา">Study Visit</th>
                                                    <th title ="จัดการประชุม">Meeting</th>
                                                    <th title ="ระดับปริญญาตรี">Undergraduate</th>
                                                    <th title ="">Mission</th>
                                                    <th title ="">Master</th>
                                                    <th title ="ประกาศนียบัตร">Diploma</th>  
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style="background-color: ivory;">
                                                            <td data-title="Budget year" colspan="14" style="text-align: center">
                                                                <b>ปี 
                                                                    <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  id="td1" runat="server" data-title="Count_Project">
                                                                <asp:Label ID="lblCountry" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td2" runat="server" data-title="Count_Project" style="text-align: center ; ">
                                                                <asp:Label ID="lblCount_Project" runat="server" ForeColor="black"></asp:Label></td>

                                                            <td  id="td3" runat="server" data-title="Bachelor" style="text-align: center ;">
                                                                <asp:Label ID="lblBachelor" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td4" runat="server" data-title="Training" style="text-align: center;">
                                                                <asp:Label ID="lblTraining" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td5" runat="server" data-title="Expert" style="text-align: center;">
                                                                <asp:Label ID="lblExpert" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td6" runat="server" data-title="Volunteer" style="text-align: center;">
                                                                <asp:Label ID="lblVolunteer" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td7" runat="server" data-title="Equipment" style="text-align: center;">
                                                                <asp:Label ID="lblEquipment" runat="server" ForeColor="black"></asp:Label></td>
                                                           <%-- <td  id="td8" runat="server" data-title="Other" style="text-align: center; min-width: 100px;">
                                                                <asp:Label ID="lblOther" runat="server" ForeColor="black" Text=""></asp:Label></td>--%>

                                                             <td  id="td8" runat="server" data-title="Study Visit" style="text-align: right;">
                                                                <asp:Label ID="lblStudyVisit" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td9" runat="server" data-title="Meeting" style="text-align: right;">
                                                                <asp:Label ID="lblMeeting" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td10" runat="server" data-title="Undergraduate" style="text-align: right;">
                                                                <asp:Label ID="lblUndergraduate" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td11" runat="server" data-title="Mission" style="text-align: right; ">
                                                                <asp:Label ID="lblMission" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td12" runat="server" data-title="Master" style="text-align: right;">
                                                                <asp:Label ID="lblMaster" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td13" runat="server" data-title="Diploma" style="text-align: right;">
                                                                <asp:Label ID="lblDiploma" runat="server" ForeColor="black"></asp:Label>
                                                                <asp:Button ID="btnDrillDown" runat="server" ToolTip="ดูรายชื่อโครงการ" Style="display: none;" CommandName="select"></asp:Button>
                                                            </td>

                                                            <td  id="td14" runat="server" data-title="Amount" style="text-align: right; min-width: 100px;"><b>
                                                                <asp:Label ID="lblAmout" runat="server" ForeColor="black"></asp:Label></b></td>


                                                        </tr>
                                                        <asp:Panel ID="pnlProject" runat="server">
                                                            <asp:Repeater ID="rptProject" runat="server">
                                                                <ItemTemplate>
                                                                    <tr style="color: blue;">
                                                                        <td data-title="รายการ" id="tdLinkProject1" runat="server" style="padding-left: 100px;" colspan="14">
                                                                            <asp:Label ID="lblProject_Name" runat="server"></asp:Label>
                                                                            <a id="lnkSelect" runat="server" tooltip="ดูรายละเอียดของโครงการ" style="display: none;" target="_blank"></a>
                                                                        </td>


                                                                        <%--<td data-title="จัดสรรแล้ว" style="text-align: right;" id="tdLinkProject2" runat="server">
                                                                            <asp:Label ID="lblAllocate_Budget" runat="server"></asp:Label></td>
                                                                        <td data-title="เบิกจ่าย" style="text-align: right;" id="tdLinkProject3" runat="server">
                                                                            <asp:Label ID="lblPay_Amount_Actual" runat="server"></asp:Label></td>
                                                                        <td data-title="คงเหลือ" style="text-align: right;" id="tdLinkProject4" runat="server">
                                                                            <asp:Label ID="lblBalance" runat="server" ToolTip="จัดสรรแล้ว-เบิกจ่าย"></asp:Label>
                                                                            
                                                                        </td>--%>

                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="Total" style="text-align: center;"><b>Total </b></td>
                                                        <td data-title="Count_Project" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblCount_Project" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Bachelor" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBachelor_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Training" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblTraining_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Expert" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblExpert_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Volunteer" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblVolunteer_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Equipment" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblEquipment_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                     <%--   <td data-title="Other" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblOther_Sum" runat="server" ForeColor="black" Text="0.00"></asp:Label></b></td>--%>

                                                            <td data-title="Study Visit" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblStudyVisit_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Meeting" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblMeeting_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Undergraduate" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblUndergraduate_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Mission" style="text-align: center; text-decoration: underline; ">
                                                                <b><asp:Label ID="lblMission_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Master" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblMaster_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            <td data-title="Diploma" style="text-align: center; text-decoration: underline;">
                                                                <b><asp:Label ID="lblDiploma_Sum" runat="server" ForeColor="black"></asp:Label></b></td>

                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAmout_Sum" runat="server" ForeColor="black"></asp:Label></b></td>

                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>

                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 style="margin-left: 18px;"><span>* จำนวนผู้รับทุน </span></h4>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->



                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>




