﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmEditBudgetGroup.aspx.vb" Inherits="frmEditBudgetGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Budget Group | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Budget Group (กลุ่มงบประมาณ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="frmBudget.aspx"><i class="fa fa-dollar"></i>Budget</a></li>
            <li><a href="frmBudgetGroup.aspx">Budget Group</a></li>
            <li class="active">Budget Group Detail</li>
          </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlEdit" runat="server" CssClass="card bg-white">

                    <section class="content">
                        <div class="row">
            <div class="col-md-12 form-horizontal">
              <!-- Horizontal Form -->
              <div class="box">
                  <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title">
                      <asp:Label ID="lblEditMode" runat="server"></asp:Label>
                      Budget Group
                      </h4>
                  </div>
               
                  <div class="box-body">
                   <div class="row">
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Budget Group (กลุ่มงบประมาณ) :<span style="color:red">*</span></label>
                      <div class="col-sm-4">
                          
                       <asp:TextBox ID="txtNames" runat="server" CssClass="form-control" placeholder="" MaxLength="250"></asp:TextBox>
                      </div>
                    </div>
                    </div>
                  </div>
                   <div class="box-body">
                   <div class="row">
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label"> Active Status :</label>
                      <div class="col-sm-4">
                       <asp:CheckBox ID="chkActive" runat="server" />
                      </div>
                    </div>
                    </div>
                  </div>

                   
                  <div class="box-footer">
                      <div class="col-sm-9"></div>
                      <div class="col-lg-8"></div>
                      <div class="col-lg-2">
                          <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" ><i class="fa fa-save"></i>Save</asp:LinkButton>
                      </div>
                      <div class="col-lg-2">
                          <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" >
                       <i class="fa fa-reply"></i> Cancel

                          </asp:LinkButton>
                      </div>
                  </div>

               
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div>
                    </section>

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
</asp:Content>

