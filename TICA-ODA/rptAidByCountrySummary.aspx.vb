﻿Imports Constants
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptAidByCountrySummary
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    ReadOnly Property ProjectType As Long
        Get
            Try
                Return ddlReportType.SelectedValue
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("rptSummary_AidByCountryPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("rptSummary_AidByCountryPage") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuComponent")
        li_mnuReports_foreign.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("a2rptAidByCountrySummary")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            Bind_DDL_Country(ddlCountry)
            BindList()
            BL.Bind_DDL_Year(ddlBudgetYear, True)


        End If

    End Sub


    Private Sub BindList()

        Dim Title As String = ""
        Dim sql As String = ""

        sql += " Select * ,Summary_Actual_Bachelor +Summary_Actual_Training +Summary_Actual_Expert +Summary_Actual_Volunteer +Summary_Actual_Equipment +Summary_Actual_Other  Summary_Actual  " + Environment.NewLine
        sql += " FROM _vw_Summary_Aid_Group_By_Country  " + Environment.NewLine

        If (ProjectType > -1) Then
            sql += " WHERE project_type=" & ProjectType & Environment.NewLine
            Title += " ประเภทโครงการ : " & ddlReportType.SelectedItem.ToString
        Else
            sql += " WHERE 0=1  " + Environment.NewLine
        End If

        If ProjectType < 2 Then
            If (ddlBudgetYear.SelectedIndex > 0) Then
                sql += " AND  budget_year = '" & ddlBudgetYear.SelectedValue & "' " + Environment.NewLine
                Title += " ปีงบประมาณ " & ddlBudgetYear.SelectedValue
            End If
        End If


        If (ddlCountry.SelectedIndex > 0) Then
            sql += " AND  convert(bigint,country_node_id) = " & ddlCountry.SelectedValue & "   " + Environment.NewLine
            Title += " การให้ความช่วยเหลือแก่ประเทศ " & ddlCountry.SelectedItem.ToString()
        Else
        End If

        If (txtSearch_Project.Text <> "") Then
            sql &= " AND project_name Like '%" & txtSearch_Project.Text & "%'   " + Environment.NewLine
            Title += " ของ " & txtSearch_Project.Text
        End If

        If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
            Dim date1 As DateTime = Convert.ToDateTime(txtStartDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            Dim date2 As DateTime = Convert.ToDateTime(txtEndDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
            Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

            sql &= " AND ( start_date  >= '" & StartDate & "' " + Environment.NewLine
            sql &= " AND end_date  <= '" & EndDate & "'  ) " + Environment.NewLine

            Title += " ตั้งแต่วันที่ " & txtStartDate.Text & " ถึงวันที่ " & txtEndDate.Text

        End If

        sql += "  ORDER BY   budget_year DESC,project_name "

        Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.SelectCommand.CommandTimeout = 2000
        DA.Fill(DT)


        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_Summary_AidByCountry_Title") = lblTotalRecord.Text

        If (DT.Rows.Count > 0) Then
            lblBachelor_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Bachelor)", "")).ToString("#,##0.00")
            lblTraining_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Training)", "")).ToString("#,##0.00")
            lblExpert_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Expert)", "")).ToString("#,##0.00")
            lblVolunteer_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Volunteer)", "")).ToString("#,##0.00")
            lblEquipment_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Equipment)", "")).ToString("#,##0.00")
            lblOther_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual_Other)", "")).ToString("#,##0.00")

            lblAmout_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Summary_Actual)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        Session("Search_Summary_AidByCountry") = DT

        AllData = DT
        Pager.SesssionSourceName = "Search_Summary_AidByCountry"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblBachelor As Label = DirectCast(e.Item.FindControl("lblBachelor"), Label)
        Dim lblTraining As Label = DirectCast(e.Item.FindControl("lblTraining"), Label)
        Dim lblExpert As Label = DirectCast(e.Item.FindControl("lblExpert"), Label)
        Dim lblVolunteer As Label = DirectCast(e.Item.FindControl("lblVolunteer"), Label)
        Dim lblEquipment As Label = DirectCast(e.Item.FindControl("lblEquipment"), Label)
        Dim lblOther As Label = DirectCast(e.Item.FindControl("lblOther"), Label)

        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblPlanDate As Label = DirectCast(e.Item.FindControl("lblPlanDate"), Label)
        Dim lblAmout As Label = DirectCast(e.Item.FindControl("lblAmout"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"

        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("budget_year").ToString Then
            Lastbudget_year = e.Item.DataItem("budget_year").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True
        Else
            trbudget_year.Visible = False
        End If

        lblProject.Text = e.Item.DataItem("project_name").ToString
        lblPlanDate.Text = "Start/End Date:" & e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString
        lblCountry.Text = e.Item.DataItem("name_th").ToString

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Bachelor")) = False Then
            lblBachelor.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Bachelor")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Training")) = False Then
            lblTraining.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Training")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Expert")) = False Then
            lblExpert.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Expert")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Volunteer")) = False Then
            lblVolunteer.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Volunteer")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Equipment")) = False Then
            lblEquipment.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Equipment")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual_Other")) = False Then
            lblOther.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual_Other")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Summary_Actual")) = False Then
            lblAmout.Text = Convert.ToDecimal(e.Item.DataItem("Summary_Actual")).ToString("#,##0.00")
        End If


        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        For i As Integer = 1 To 8
            Dim tdLinkProject As HtmlTableCell = e.Item.FindControl("tdLinkProject" & i)
            tdLinkProject.Style("cursor") = "pointer"
            tdLinkProject.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "view&type=" & e.Item.DataItem("project_type")
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidByCountrySummary.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptAidByCountrySummary.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    '--Distinct เฉพ่าะประเทศที่อยู่ใน List
    Public Sub Bind_DDL_Country(ByRef ddl As DropDownList)

        Dim Sql As String = " Select DISTINCT country_node_id,name_th FROM _vw_Summary_Aid_Group_By_Country  WHERE country_node_id IS NOT NULL ORDER BY name_th    " + Environment.NewLine
        Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Select Country", ""))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("name_th"), DT.Rows(i).Item("country_node_id"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
    End Sub

    Private Sub ddlReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReportType.SelectedIndexChanged
        If ProjectType > 1 Then
            divSearch_Year.Visible = False
        Else
            divSearch_Year.Visible = True
        End If

    End Sub
End Class
