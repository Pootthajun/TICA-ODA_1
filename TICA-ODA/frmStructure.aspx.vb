﻿Imports ODAENG
Imports Constants
Imports System.Data
Imports LinqDB.TABLE
Imports System.Windows.Forms.TreeNode


Public Class frmStructure
    Inherits System.Web.UI.Page
    Public Count As Integer = 1
    Public _txtTooltripPeople As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่eeeองมือ'><a class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'><i class='fa fa-ellipsis-h'></i></a><ul class='dropdown-menu'><li><a href='#'><i class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li><li><a href='#'><i class='fa fa-gears text-blue'></i>  กำหนดสิทธิ์</a></li><li class='divider'></li><li><a href='#'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li><li><a href='#'><i class='fa fa-trash text-red'></i>  ลบ</a></li></ul></div>"
    Dim ENG As New ODAENG
    Dim strNodeID As String

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuStructure")
        li.Attributes.Add("class", "active")


        If Not IsPostBack Then
            BindDataToTreeView()

            'Panel Agency
            'ENG.SetTextIntKeypress(txtAgencyTelephone)
            'ENG.SetTextIntKeypress(txtAgencyFax)
        End If

    End Sub
    Sub ClearData()
        'Clear Dialog Country
        lblCountryName.Text = ""
        txtCountryNameEN.Text = ""
        txtCountryNameTH.Text = ""
        txtDescriptionCountry.Text = ""
        chkACTIVE_STATUS_Country.Checked = False

        'Clear Dialog Agency
        lblAgencyCountryName.Text = ""
        lblAgencyOrganizeName.Text = ""
        txtAgencyNameEN.Text = ""
        txtAgencyNameTH.Text = ""
        txtAgencyABBRNameTH.Text = ""
        txtAgencyABBRNameEN.Text = ""
        txtAgencyEmail.Text = ""
        txtAgencyLocation.Text = ""
        txtAgencyTelephone.Text = ""
        txtAgencyFax.Text = ""
        txtAgencyWebsite.Text = ""
        txtAgencyNote.Text = ""

        txtstrEditAgenct.Text = ""
        txtparentAgency.Text = ""

        chkAgency_ExecutingAgency.Checked = False
        chkAgency_FundingAgency.Checked = False
        chkAgency_ImplementingAgency.Checked = False
        chkACTIVE_STATUS_Agency.Checked = False


        'Clear Dialog Person
        UCFormOUPerson.ClearData()
        txtstrEditPerson.Text = ""
        txtNodeID.Text = ""
        txtparentAgencyID.Text = ""

    End Sub

    Public Property EditCountryID As String
        Get
            Try
                Return ViewState("CountryID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            ViewState("CountryID") = value
        End Set
    End Property

    'หัวหลักประเทศ
    Protected Sub BindDataToTreeView()

        Dim dt As New DataTable
        dt = ENG.GetOrganizeUnitByNodeID("", OU.Country)

        For i = 0 To dt.Rows.Count() - 1
            If dt.Rows(i)("Node_ID").ToString() <> "" Then
                Dim NODE_ID As String = dt.Rows(i)("Node_ID").ToString()

                Dim node As New TreeNode
                NODE_ID = dt.Rows(i)("Node_ID").ToString()


                Dim _txtTooltripAddCountry As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่องมือ'>"
                _txtTooltripAddCountry += "<a Class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'><i class='fa fa-ellipsis-h'></i></a>"
                _txtTooltripAddCountry += "<ul class='dropdown-menu'>"
                _txtTooltripAddCountry += "<li> <a href='javascript:;' onclick='fnBindCountry1(" & NODE_ID & ")'><i Class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li>"
                _txtTooltripAddCountry += "<li><a href='javascript:;'  onclick='fnBindAgency(" & NODE_ID & ")'><i class='fa fa-building text-blue'></i>  เพิ่มหน่วยงาน</a></li>"
                _txtTooltripAddCountry += "<li> <a href='javascript:;'  onclick='fnBindOrganize(" & NODE_ID & ")'><i Class='fa fa-user text-blue'></i>  เพิ่มบุคคล</a></li>"
                _txtTooltripAddCountry += "<li><a href='frmEditUserLevel.aspx?NODE_ID=" & NODE_ID & "'><i class='fa fa-gears text-blue'></i>  กำหนดสิทธิ์</a></li>"
                _txtTooltripAddCountry += "<li Class='divider'></li>"
                _txtTooltripAddCountry += "<li><a href='javascript:;' onclick='fnBindCountry(" & NODE_ID & ")'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li>"
                _txtTooltripAddCountry += "<li><a href='#' onclick='fndelete(" & NODE_ID & ")'><i class='fa fa-trash text-red'></i>  ลบ</a></li>"
                _txtTooltripAddCountry += "</ul>"
                _txtTooltripAddCountry += "</div>"

                node.Text = "<img src='Flag/" & NODE_ID & ".png' width='23' height='15' onError=""this.src='Flag/Default.png'"" /> " & dt(i)("Name_EN").ToString() & "  " & "(" & dt(i)("Name_TH").ToString() & ")" & _txtTooltripAddCountry
                node.Value = dt(i)("Node_ID").ToString()


                TRV_OU.Nodes.Add(node)
                node.CollapseAll()

                If dt.Rows(i)("childs") > 0 Then
                    Dim _node As New TreeNode
                    _node.Text = ""
                    _node.Value = ""
                    _node.Collapse()
                    node.ChildNodes.Add(_node)
                End If
            End If


        Next

    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditCountry.aspx")
    End Sub

    Sub ViewDataCountry(country_id As String)

        ENG.Bind_DDL_Region(DDL_CountryRegion)
        ENG.Bind_DDL_CountryGroup(DDL_CountryGroup)
        ENG.Bind_DDL_RegionZone(DDL_CountryZone)

        PNLmyModalCountry.Visible = True

        If country_id <> "" Then
            strNodeID = country_id
            Dim dtC As New DataTable
            dtC = ENG.GetCountryInfoByID(strNodeID)

            For i = 0 To dtC.Rows.Count - 1
                txtCountryNameTH.Text = dtC(i)("name_th").ToString()
                txtCountryNameEN.Text = dtC(i)("name_en").ToString()
                txtDescriptionCountry.Text = dtC(i)("description").ToString()
                DDL_CountryRegion.SelectedValue = dtC(i)("country_region_id").ToString()
                DDL_CountryGroup.SelectedValue = dtC(i)("country_group_id").ToString()
                DDL_CountryZone.SelectedValue = dtC(i)("country_zone_id").ToString()

                If dtC(i)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS_Country.Checked = True
                Else
                    chkACTIVE_STATUS_Country.Checked = False
                End If
            Next

        End If


    End Sub
    Sub ViewDataCountry1(country_id As String)

        ENG.Bind_DDL_Region(DropDownList1)
        ENG.Bind_DDL_CountryGroup(DropDownList2)
        ENG.Bind_DDL_RegionZone(DropDownList3)

        PNLmyModalViewCountry.Visible = True

        If country_id <> "" Then
            strNodeID = country_id
            Dim dtC As New DataTable
            dtC = ENG.GetCountryInfoByID(strNodeID)

            For i = 0 To dtC.Rows.Count - 1
                TextBox1.Text = dtC(i)("name_th").ToString()
                TextBox2.Text = dtC(i)("name_en").ToString()
                TextBox3.Text = dtC(i)("description").ToString()
                DropDownList1.SelectedValue = dtC(i)("country_region_id").ToString()
                DropDownList2.SelectedValue = dtC(i)("country_group_id").ToString()
                DropDownList3.SelectedValue = dtC(i)("country_zone_id").ToString()
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox3.Enabled = False
                DropDownList1.Enabled = False
                DropDownList2.Enabled = False
                DropDownList3.Enabled = False
                If dtC(i)("active_status").ToString() = "Y" Then
                    CheckBox1.Checked = True
                Else
                    CheckBox1.Checked = False
                End If
                CheckBox1.Enabled = False
            Next

        End If


    End Sub

    Sub FillInDataAgency(org_node_id As String)
        PNLmyModalAgency.Visible = True
        If org_node_id <> "" Then
            strNodeID = org_node_id
            Dim dtA As New DataTable
            dtA = ENG.GetOrganizeInfoByID(strNodeID)

            For i = 0 To dtA.Rows.Count - 1
                txtNodeID.Text = dtA(i)("node_id").ToString
                txtAgencyNameTH.Text = dtA(i)("name_th").ToString()
                txtAgencyABBRNameTH.Text = dtA(i)("abbr_name_th").ToString()
                txtAgencyNameEN.Text = dtA(i)("name_en").ToString()
                txtAgencyABBRNameEN.Text = dtA(i)("abbr_name_en").ToString()

                txtAgencyLocation.Text = dtA(i)("address").ToString()
                txtAgencyTelephone.Text = dtA(i)("telephone").ToString()
                txtAgencyFax.Text = dtA(i)("fax").ToString()
                txtAgencyWebsite.Text = dtA(i)("website").ToString()
                txtAgencyEmail.Text = dtA(i)("email").ToString()
                txtAgencyNote.Text = dtA(i)("description").ToString()

                If dtA(i)("funding_agency_type").ToString() = "Y" Then
                    chkAgency_FundingAgency.Checked = True
                End If
                If dtA(i)("executing_agency_type").ToString() = "Y" Then
                    chkAgency_ExecutingAgency.Checked = True
                End If
                If dtA(i)("implementing_agency_type").ToString() = "Y" Then
                    chkAgency_ImplementingAgency.Checked = True
                End If

                If dtA(i)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS_Agency.Checked = True
                Else
                    chkACTIVE_STATUS_Agency.Checked = False
                End If
            Next
        End If
    End Sub

    Sub ViewDataAgency(org_node_id As String)
        PNLmyModalAgencyView.Visible = True
        If org_node_id <> "" Then
            strNodeID = org_node_id
            Dim dtA As New DataTable
            dtA = ENG.GetOrganizeInfoByID(strNodeID)

            For i = 0 To dtA.Rows.Count - 1
                txtViewAgencyNameTh.Text = dtA(i)("name_th").ToString()
                txtViewAgencyAbbrNameTh.Text = dtA(i)("abbr_name_th").ToString()
                txtViewAgencyNameEn.Text = dtA(i)("name_en").ToString()
                txtViewAgencyAbbrNameEn.Text = dtA(i)("abbr_name_en").ToString()

                txtViewAgencyAddress.Text = dtA(i)("address").ToString()
                txtViewAgencyTelNo.Text = dtA(i)("telephone").ToString()
                txtViewAgencyFaxNo.Text = dtA(i)("fax").ToString()
                txtViewAgencyWebSite.Text = dtA(i)("website").ToString()
                txtViewAgencyEmail.Text = dtA(i)("email").ToString()
                txtViewAgencyNote.Text = dtA(i)("description").ToString()

                txtViewAgencyNameTh.Enabled = False
                txtViewAgencyAbbrNameTh.Enabled = False
                txtViewAgencyNameEn.Enabled = False
                txtViewAgencyAbbrNameEn.Enabled = False

                txtViewAgencyAddress.Enabled = False
                txtViewAgencyTelNo.Enabled = False
                txtViewAgencyFaxNo.Enabled = False
                txtViewAgencyWebSite.Enabled = False
                txtViewAgencyEmail.Enabled = False
                txtViewAgencyNote.Enabled = False

                If dtA(i)("funding_agency_type").ToString() = "Y" Then
                    chkViewAgencyTypeFunding.Checked = True
                End If
                If dtA(i)("executing_agency_type").ToString() = "Y" Then
                    chkViewAgencyTypeExecuting.Checked = True
                End If
                If dtA(i)("implementing_agency_type").ToString() = "Y" Then
                    chkViewAgencyTypeImplementing.Checked = True
                End If

                If dtA(i)("active_status").ToString() = "Y" Then
                    chkViewAgencyActiveStatus.Checked = True
                Else
                    chkViewAgencyActiveStatus.Checked = False
                End If

                chkViewAgencyTypeFunding.Enabled = False
                chkViewAgencyTypeExecuting.Enabled = False
                chkViewAgencyTypeImplementing.Enabled = False
                chkViewAgencyActiveStatus.Enabled = False
            Next

        End If


    End Sub

    Sub FillInDataPerson(PersonNodeID As String)
        PNLmyModalPerson.Visible = True
        UCFormOUPerson.FillInDataPerson(PersonNodeID)

        'If PersonNodeID <> "" Then
        '    strNodeID = PersonNodeID
        '    Dim dtP As New DataTable
        '    dtP = ENG.GetPersonInfoByID(strNodeID)

        '    For i = 0 To dtP.Rows.Count - 1
        '        txtNodeID.Text = dtP(i)("node_id")
        '        txtPersonNameTH.Text = dtP(i)("name_th").ToString()
        '        txtPersonNameEN.Text = dtP(i)("name_en").ToString()
        '        txtPersonIDCard.Text = dtP(i)("id_card").ToString()
        '        txtPersonPassportID.Text = dtP(i)("passport_no").ToString()
        '        If dtP(i)("birthdate").ToString <> "" Then
        '            txtPersonBirthDay.Text = Convert.ToDateTime(dtP(i)("birthdate")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        '        End If
        '        txtPersonTelephone.Text = dtP(i)("telephone").ToString()
        '        txtPersonEmail.Text = dtP(i)("email").ToString()
        '        txtPersonNote.Text = dtP(i)("description").ToString()

        '        If dtP(i)("active_status").ToString() = "Y" Then
        '            chkACTIVE_STATUS_Person.Checked = True
        '        Else
        '            chkACTIVE_STATUS_Person.Checked = False
        '        End If

        '        If dtP(i)("person_type").ToString() = "1" Then
        '            RaRecipient.Checked = True
        '        Else
        '            RaAssistant.Checked = True
        '        End If
        '    Next

        'End If

    End Sub

    Sub ViewDataPerson(Grganize_id As String, parent_id As String)


        PNLmyModalPersonView.Visible = True

        If Grganize_id <> "" Then
            strNodeID = Grganize_id
            Dim dtP As New DataTable
            dtP = ENG.GetPersonInfoByID(strNodeID)

            For i = 0 To dtP.Rows.Count - 1

                TextBox14.Text = dtP(i)("name_th").ToString()
                TextBox15.Text = dtP(i)("name_en").ToString()
                TextBox16.Text = dtP(i)("id_card").ToString()
                TextBox17.Text = dtP(i)("passport_no").ToString()
                If dtP(i)("birthdate").ToString <> "" Then
                    TextBox18.Text = Convert.ToDateTime(dtP(i)("birthdate")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                End If
                TextBox19.Text = dtP(i)("telephone").ToString()
                TextBox20.Text = dtP(i)("email").ToString()
                TextBox21.Text = dtP(i)("description").ToString()

                If dtP(i)("active_status").ToString() = "Y" Then
                    CheckBox6.Checked = True
                Else
                    CheckBox6.Checked = False
                End If
            Next

            TextBox14.Enabled = False
            TextBox15.Enabled = False
            TextBox16.Enabled = False
            TextBox17.Enabled = False
            TextBox18.Enabled = False
            TextBox19.Enabled = False
            TextBox20.Enabled = False
            TextBox21.Enabled = False
            CheckBox6.Enabled = False

        End If

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)

        Dim NODE_ID As String = txtNodeID.Text.ToString()

        If NODE_ID <> "" Then
            Dim ret As ProcessReturnInfo = ENG.DeleteOUCountry(NODE_ID)
            If ret.IsSuccess = True Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ลบข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('" + ret.ErrorMessage() + "');window.location ='frmStructure.aspx';", True)
            End If
        End If
        BindDataToTreeView()
        'Response.Redirect("frmStructure.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub TRV_OU_TreeNodeExpanded(sender As Object, e As TreeNodeEventArgs) Handles TRV_OU.TreeNodeExpanded

        If e.Node.ChildNodes.Count = 1 AndAlso e.Node.ChildNodes(0).Text = "" Then ' มีลูกและยังไม่เคย Load ลูกมาใส่
            e.Node.ChildNodes.RemoveAt(0) 'ลบ Node ว่างออก

            Dim ENG As New ODAENG
            Dim dt As New DataTable
            dt = ENG.GetOrganizeUnitByNodeID(e.Node.Value, OU.Organize) 'ดึงลูกมาใส่

            For j = 0 To dt.Rows.Count() - 1
                Dim NODE_ID As String = dt(j)("Node_ID").ToString()

                Dim ParentID As String = "0"

                If e.Node IsNot Nothing Then
                    ParentID = e.Node.Value
                End If
                'หัวรองหน่วยงาน
                Dim _txtTooltripAddCountry As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่องมือ'>"
                _txtTooltripAddCountry += "<a Class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'>"
                _txtTooltripAddCountry += "<i Class='fa fa-ellipsis-h'></i></a><ul class='dropdown-menu'><li><a href='javascript:;' onclick='fnBindAgencyToView(" & NODE_ID & "," & ParentID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li><li><a href='javascript:;' onclick='fnBindAgency(" & NODE_ID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-building text-blue'></i>  เพิ่มหน่วยงาน</a></li><li><a href='javascript:;'  onclick='fnBindOrganize(" & NODE_ID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-user text-blue'></i>  เพิ่มบุคคล</a></li><li><a href='frmEditUserLevel.aspx?NODE_ID=" & NODE_ID & "'>"
                _txtTooltripAddCountry += "<i Class='fa fa-gears  text-blue'></i>  กำหนดสิทธิ์</a></li><li class='divider'></li>"
                _txtTooltripAddCountry += "<li><a href ='javascript:;' onclick='fnBindAgencyToEdit(" & NODE_ID & "," & ParentID & ")'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li>"
                _txtTooltripAddCountry += "<li><a href='#' onclick='fndeleteorganize(" & NODE_ID & ")'><i class='fa fa-remove text-red'></i>  ลบ</a></li></ul></div>"

                Dim childNode As New TreeNode
                childNode.Text = "<div class='btn btn-social-icon btn-defult text-blue'><i class='fa fa-building-o'></i></div>" & "  " & dt(j)("Name_EN").ToString() & "  " & "(" & dt(j)("Name_TH").ToString() & ")" & _txtTooltripAddCountry
                childNode.Value = dt(j)("Node_ID").ToString()
                e.Node.ChildNodes.Add(childNode)

                If dt.Rows(j)("childs") > 0 Then ' ถ้า Node นั้นมีลูกอีก ให้ใส่ Node ว่าง

                    Dim _node As New TreeNode
                    _node.Collapse()
                    childNode.ChildNodes.Add(_node)

                End If


            Next

            dt = ENG.GetOrganizeUnitByNodeID(e.Node.Value, OU.Person) 'ดึงลูกมาใส่

            For j = 0 To dt.Rows.Count() - 1
                Dim NODE_ID As String = dt(j)("Node_ID").ToString()
                Dim ParentID As String = "0"
                If e.Node IsNot Nothing Then
                    ParentID = e.Node.Value
                End If
                'หัวรองบุคคล
                Dim _txtTooltripAddCountry As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่องมือ'>"
                _txtTooltripAddCountry += "<a Class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'>"
                _txtTooltripAddCountry += "<i Class='fa fa-ellipsis-h'></i></a><ul class='dropdown-menu'><li><a href ='javascript:;' onclick='fnBindPersonToView(" & NODE_ID & "," & ParentID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li><li><a href='frmEditUserLevel.aspx?NODE_ID=" & NODE_ID & "'>"
                _txtTooltripAddCountry += "<i Class='fa fa-gears  text-blue'></i>  กำหนดสิทธิ์</a></li><li class='divider'></li>"
                _txtTooltripAddCountry += "<li><a href ='javascript:;' onclick='fnBindPersonToEdit(" & NODE_ID & "," & ParentID & ")'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li>"
                _txtTooltripAddCountry += "<li><a href='#' onclick='fndeleteperson(" & NODE_ID & ")'><i class='fa fa-remove text-red'></i>  ลบ</a></li></ul></div>"

                Dim childNode As New TreeNode
                childNode.Text = "<div class='btn btn-social-icon btn-defult text-blue'><i class='fa fa-user'></i></div>" & "  " & dt(j)("Name_EN").ToString() & "  " & "(" & dt(j)("Name_TH").ToString() & ")" & _txtTooltripAddCountry
                childNode.Value = dt(j)("Node_ID").ToString()
                e.Node.ChildNodes.Add(childNode)

                If dt.Rows(j)("childs") > 0 Then ' ถ้า Node นั้นมีลูกอีก ให้ใส่ Node ว่าง

                    Dim _node As New TreeNode
                    _node.Collapse()
                    childNode.ChildNodes.Add(_node)

                End If


            Next
        Else

        End If

    End Sub

    Private Sub TRV_OU_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TRV_OU.SelectedNodeChanged

        Dim NODE_ID As String = TRV_OU.SelectedNode.Value
        If TRV_OU.Nodes.Count = 1 AndAlso TRV_OU.Nodes(0).Text = "" Then ' มีลูกและยังไม่เคย Load ลูกมาใส่
            TRV_OU.Nodes.RemoveAt(0) 'ลบ Node ว่างออก

            Dim ENG As New ODAENG
            Dim dt As New DataTable
            dt = ENG.GetOrganizeUnitByNodeID(NODE_ID, OU.Organize) 'ดึงลูกมาใส่

            For j = 0 To dt.Rows.Count() - 1
                NODE_ID = dt(j)("Node_ID").ToString()

                Dim ParentID As String = "0"

                If NODE_ID IsNot Nothing Then
                    ParentID = NODE_ID
                End If

                Dim _txtTooltripAddCountry As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่องมือ'>"
                _txtTooltripAddCountry += "<a Class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'>"
                _txtTooltripAddCountry += "<i Class='fa fa-ellipsis-h'></i></a><ul class='dropdown-menu'><li><a href='javascript:;' onclick='fnBindAgencyToView(" & NODE_ID & "," & ParentID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li><li><a href='javascript:;' onclick='fnBindAgency(" & NODE_ID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-building text-blue'></i>  เพิ่มหน่วยงาน</a></li><li><a href='javascript:;'  onclick='fnBindOrganize(" & NODE_ID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-user text-blue'></i>  เพิ่มบุคคล</a></li><li><a href='frmEditUser.aspx" & "?NODE_ID=" & NODE_ID & "'>"
                _txtTooltripAddCountry += "<i Class='fa fa-gears  text-blue'></i>  กำหนดสิทธิ์</a></li><li class='divider'></li>"
                _txtTooltripAddCountry += "<li><a href ='javascript:;' onclick='fnBindAgencyToEdit(" & NODE_ID & ")'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li>"
                _txtTooltripAddCountry += "<li><a href='#' onclick='fndeleteorganize(" & NODE_ID & ")'><i class='fa fa-remove text-red'></i>  ลบ</a></li></ul></div>"

                Dim childNode As New TreeNode
                childNode.Text = "<div class='btn btn-social-icon btn-defult text-blue'><i class='fa fa-building-o'></i></div>" & "  " & dt(j)("Name_EN").ToString() & "  " & "(" & dt(j)("Name_TH").ToString() & ")" & _txtTooltripAddCountry
                childNode.Value = dt(j)("Node_ID").ToString()
                TRV_OU.Nodes.Add(childNode)

                If dt.Rows(j)("childs") > 0 Then ' ถ้า Node นั้นมีลูกอีก ให้ใส่ Node ว่าง

                    Dim _node As New TreeNode
                    _node.Collapse()
                    childNode.ChildNodes.Add(_node)

                End If


            Next

            dt = ENG.GetOrganizeUnitByNodeID(NODE_ID, OU.Person) 'ดึงลูกมาใส่

            For j = 0 To dt.Rows.Count() - 1
                NODE_ID = dt(j)("Node_ID").ToString()
                Dim ParentID As String = "0"
                If NODE_ID IsNot Nothing Then
                    ParentID = NODE_ID
                End If

                Dim _txtTooltripAddCountry As String = "<div class='btn-group' data-toggle='tooltip' title='เครื่องมือ'>"
                _txtTooltripAddCountry += "<a Class='btn btn-social-icon btn-defult text-green' data-toggle='dropdown'>"
                _txtTooltripAddCountry += "<i Class='fa fa-ellipsis-h'></i></a><ul class='dropdown-menu'><li><a href ='javascript:;' onclick='fnBindPersonToView(" & NODE_ID & "," & ParentID & ")'>"
                _txtTooltripAddCountry += "<i Class='fa fa-search text-green'></i>  ดูรายละเอียด</a></li><li><a href='frmEditStructure.aspx" & "?NODE_ID=" & NODE_ID & "'>"
                _txtTooltripAddCountry += "<i Class='fa fa-gears  text-blue'></i>  กำหนดสิทธิ์</a></li><li class='divider'></li>"
                _txtTooltripAddCountry += "<li><a href ='javascript:;' onclick='fnBindPersonToEdit(" & NODE_ID & "," & ParentID & ")'><i class='fa fa-edit text-blue'></i>  แก้ไข</a></li>"
                _txtTooltripAddCountry += "<li><a href='#' onclick='fndeleteperson(" & NODE_ID & ")'><i class='fa fa-remove text-red'></i>  ลบ</a></li></ul></div>"

                Dim childNode As New TreeNode
                childNode.Text = "<div class='btn btn-social-icon btn-defult text-blue'><i class='fa fa-user'></i></div>" & "  " & dt(j)("Name_EN").ToString() & "  " & "(" & dt(j)("Name_TH").ToString() & ")" & _txtTooltripAddCountry
                childNode.Value = dt(j)("Node_ID").ToString()
                TRV_OU.Nodes.Add(childNode)

                If dt.Rows(j)("childs") > 0 Then ' ถ้า Node นั้นมีลูกอีก ให้ใส่ Node ว่าง

                    Dim _node As New TreeNode
                    _node.Collapse()
                    childNode.ChildNodes.Add(_node)
                End If

            Next
        Else

        End If
        TRV_OU.SelectedNode.Expand()

    End Sub
    Protected Sub btnSetDataCountry_Click(sender As Object, e As EventArgs) Handles btnSetDataCountry.Click
        ViewDataCountry(txtNodeID.Text)
    End Sub
    Protected Sub btnSetDataCountry1_Click(sender As Object, e As EventArgs) Handles btnSetDataCountry1.Click
        ViewDataCountry1(txtNodeID.Text)
    End Sub
    Protected Sub btndeleteOrganize_Click(sender As Object, e As EventArgs) Handles btndeleteOrganize.Click
        Dim NODE_ID As String = txtNodeID.Text.ToString()

        If NODE_ID <> "" Then
            Dim ret As ProcessReturnInfo = ENG.DeleteOUOrganize(NODE_ID)
            If ret.IsSuccess = True Then
                'Alert("ลบข้อมูลเรียบร้อยแล้ว")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ลบข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            Else
                Alert(ret.ErrorMessage())
            End If
        End If
        BindDataToTreeView()
    End Sub
    Protected Sub btndeletePerson_Click(sender As Object, e As EventArgs) Handles btndeletePerson.Click
        Dim NODE_ID As String = txtNodeID.Text.ToString()

        If NODE_ID <> "" Then
            Dim ret As ProcessReturnInfo = ENG.DeleteOUPerson(NODE_ID)
            If ret.IsSuccess = True Then
                'Alert("ลบข้อมูลเรียบร้อยแล้ว")
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('ลบข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            Else
                Alert(ret.ErrorMessage())
            End If
        End If
        BindDataToTreeView()
    End Sub
    Protected Sub btnSave_Country_Click(sender As Object, e As EventArgs) Handles btnSave_Country.Click
        If txtCountryNameTH.Text = "" Then
            Alert("กรุณากรอกชื่อประเทศภาษาไทย")
            txtCountryNameTH.Focus()
            Exit Sub
        End If

        If txtCountryNameEN.Text = "" Then
            Alert("กรุณากรอกชื่อประเทศภาษาอังกฤษ")
            txtCountryNameEN.Focus()
            Exit Sub
        End If

        If DDL_CountryRegion.SelectedValue = "" Then
            Alert("กรุณาระบุภูมิภาค")
            DDL_CountryRegion.Focus()
            Exit Sub
        End If

        If DDL_CountryGroup.SelectedValue = "" Then
            Alert("กรุณาระบุกลุ่มประเทศ")
            DDL_CountryGroup.Focus()
            Exit Sub
        End If

        If DDL_CountryZone.SelectedValue = "" Then
            Alert("กรุณาระบุโซนประเทศ")
            DDL_CountryZone.Focus()
            Exit Sub
        End If

        Dim linqCountry As New TbOuCountryLinqDB
        linqCountry.NODE_ID = txtNodeID.Text.Trim()
        linqCountry.NAME_TH = txtCountryNameTH.Text.Trim.Replace("'", "''")
        linqCountry.NAME_EN = txtCountryNameEN.Text.Trim.Replace("'", "''")
        linqCountry.DESCRIPTION = txtDescriptionCountry.Text.Trim.Replace("'", "''")

        If chkACTIVE_STATUS_Country.Checked Then
            linqCountry.ACTIVE_STATUS = "Y"
        Else
            linqCountry.ACTIVE_STATUS = "N"
        End If

        Dim ret As New ProcessReturnInfo

        Dim dtR As New DataTable 'Country Region
        dtR.Columns.Add("country_region_id")
        dtR.Columns.Add("country_id")
        Dim drR As DataRow
        drR = dtR.NewRow
        drR("country_region_id") = DDL_CountryRegion.SelectedValue.ToString()
        drR("country_id") = ""
        dtR.Rows.Add(drR)

        Dim dtG As New DataTable 'Country Group
        dtG.Columns.Add("country_group_id")
        dtG.Columns.Add("country_id")
        Dim drG As DataRow
        drG = dtG.NewRow
        drG("country_group_id") = DDL_CountryGroup.SelectedValue.ToString()
        drG("country_id") = ""
        dtG.Rows.Add(drG)


        Dim dtZ As New DataTable 'Country Zone
        dtZ.Columns.Add("country_zone_id")
        dtZ.Columns.Add("country_id")
        Dim drZ As DataRow
        drZ = dtZ.NewRow
        drZ("country_zone_id") = DDL_CountryZone.SelectedValue.ToString()
        drZ("country_id") = ""
        dtZ.Rows.Add(drZ)

        ret = ENG.SaveOUCountry(linqCountry, dtR, dtG, dtZ, UserName)
        If ret.IsSuccess = True Then
            ' Alert("บันทึกข้อมูลเรียบร้อยแล้ว")                                                                     ';window.location ='frmStructure.aspx';
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            ClearData()
        Else
            Alert(ret.ErrorMessage)
        End If
    End Sub
    Protected Sub btnCancel_Country_Click(sender As Object, e As EventArgs) Handles btnCancel_Country.Click
        PNLmyModalCountry.Visible = False
        ClearData()
    End Sub
    Protected Sub BtnCancelCountry_Click(sender As Object, e As EventArgs) Handles BtnCancelCountry.Click
        PNLmyModalViewCountry.Visible = False
        ClearData()
    End Sub

    Public strCountryID As String

    Protected Sub btnAgencySave_Click(sender As Object, e As EventArgs) Handles btnAgencySave.Click
        If txtNodeID.Text = Nothing Then
            Alert("ไม่พบ Node ID")
            txtAgencyNameTH.Focus()
            Exit Sub
        End If

        If txtAgencyNameTH.Text = "" Then
            Alert("กรุณากรอกชื่อหน่วยงานภาษาไทย")
            txtAgencyNameTH.Focus()
            Exit Sub
        End If

        If txtAgencyNameEN.Text = "" Then
            Alert("กรุณากรอกชื่อหน่วยงานภาษาอังกฤษ")
            txtAgencyNameEN.Focus()
            Exit Sub
        End If

        Dim linqOrganize As New TbOuOrganizeLinqDB
        If txtstrEditAgenct.Text.ToUpper = "EDIT" Then
            linqOrganize.NODE_ID = txtNodeID.Text.Trim.Replace("'", "''")
        End If

        If txtstrEditAgenct.Text.ToUpper = "EDIT" Then
            linqOrganize.PARENT_ID = txtparentAgencyID.Text.Trim
        Else
            linqOrganize.PARENT_ID = txtNodeID.Text.Trim
        End If

        linqOrganize.NAME_TH = txtAgencyNameTH.Text.Trim.Replace("'", "''")
        linqOrganize.NAME_EN = txtAgencyNameEN.Text.Trim.Replace("'", "''")
        linqOrganize.ABBR_NAME_TH = txtAgencyABBRNameTH.Text.Trim.Replace("'", "''")
        linqOrganize.ABBR_NAME_EN = txtAgencyABBRNameEN.Text.Trim.Replace("'", "''")
        linqOrganize.FUNDING_AGENCY_TYPE = IIf(chkAgency_FundingAgency.Checked, "Y", "N")
        linqOrganize.EXECUTING_AGENCY_TYPE = IIf(chkAgency_ExecutingAgency.Checked, "Y", "N")
        linqOrganize.IMPLEMENTING_AGENCY_TYPE = IIf(chkAgency_ImplementingAgency.Checked, "Y", "N")
        linqOrganize.ACTIVE_STATUS = Convert.ToChar(IIf(chkACTIVE_STATUS_Agency.Checked, "Y", "N"))

        Dim ret As New ProcessReturnInfo

        Dim dtA As New DataTable
        dtA.Columns.Add("DTOrganize", GetType(TbOuContactLinqDB))
        Dim conLnq As New TbOuContactLinqDB
        conLnq.PARENT_TYPE = (Contact_Parent_Type.Oraganize)
        conLnq.ADDRESS = txtAgencyLocation.Text.Trim
        conLnq.TELEPHONE = txtAgencyTelephone.Text.Trim
        conLnq.FAX = txtAgencyFax.Text.Trim
        conLnq.WEBSITE = txtAgencyWebsite.Text.Trim
        conLnq.DESCRIPTION = txtAgencyNote.Text.Trim
        conLnq.EMAIL = txtAgencyEmail.Text.Trim

        dtA.Rows.Add(conLnq)

        ret = ENG.SaveOUOrganize(linqOrganize, dtA, UserName)
        If ret.IsSuccess = True Then
            ' Alert("บันทึกข้อมูลเรียบร้อยแล้ว")                                                                     ';window.location ='frmStructure.aspx';
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            ClearData()
        Else
            Alert(ret.ErrorMessage)
        End If

    End Sub
    Protected Sub btnAgencyCancel_Click(sender As Object, e As EventArgs) Handles btnAgencyCancel.Click
        PNLmyModalAgency.Visible = False
        ClearData()
    End Sub
    Protected Sub btnAgencyCancel1_Click(sender As Object, e As EventArgs) Handles btnAgencyCancel1.Click
        PNLmyModalAgencyView.Visible = False
        ClearData()
    End Sub
    Protected Sub btnGetCountryID_Click(sender As Object, e As EventArgs) Handles btnGetCountryID.Click
        strCountryID = txtNodeID.Text
        PNLmyModalAgency.Visible = True

    End Sub

    Public strOrganizeID As String
    Protected Sub btnPersonSave_Click(sender As Object, e As EventArgs) Handles btnPersonSave.Click
        Dim ret As ProcessReturnInfo = UCFormOUPerson.SavePerson(UserName, True)
        If ret.IsSuccess = True Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
            ClearData()
        Else
            Alert(ret.ErrorMessage)
        End If

        'If txtPersonNameTH.Text = "" Then
        '    Alert("กรุณากรอกชื่อภาษาไทย")
        '    txtPersonNameTH.Focus()

        '    Exit Sub
        'End If

        'If txtPersonNameEN.Text = "" Then
        '    Alert("กรุณากรอกชื่อภาษาอังกฤษ")
        '    txtPersonNameEN.Focus()

        '    Exit Sub
        'End If

        ''เช็ค ID Card กับ Passport ID ซ้ำกัน
        'Dim chkDupIdcardPassport As Boolean = ENG.CheckDupplicateIDCardPassport(txtPersonIDCard.Text.Trim, txtPersonPassportID.Text.Trim, txtNodeID.Text.Trim)
        'If chkDupIdcardPassport = True Then
        '    Alert("กรุณากรอกข้อมูลใหม่ IDCard หรือ Passport ID ซ้ำ")
        '    Exit Sub
        'End If
        'If RaRecipient.Checked = False And RaAssistant.Checked = False Then
        '    Alert("กรุณาระบุประเภทบุคคล")
        '    Exit Sub
        'End If

        'Dim linqPerson As New TbOuPersonLinqDB
        'If txtstrEditPerson.Text.ToUpper = "EDIT" Then
        '    linqPerson.NODE_ID = txtNodeID.Text.Trim
        'Else
        '    linqPerson.NODE_ID = EditCountryID
        'End If

        'linqPerson.NAME_TH = txtPersonNameTH.Text.Trim.Replace("'", "''")
        'linqPerson.NAME_EN = txtPersonNameEN.Text.Trim.Replace("'", "''")
        'If txtPersonBirthDay.Text <> "" Then
        '    linqPerson.BIRTHDATE = Converter.StringToDate(txtPersonBirthDay.Text, "dd/MM/yyyy")
        'End If
        'linqPerson.ID_CARD = txtPersonIDCard.Text.Trim.Replace("'", "''")
        'linqPerson.PASSPORT_NO = txtPersonPassportID.Text.Trim.Replace("'", "''")


        'If txtstrEditPerson.Text.ToUpper = "EDIT" Then
        '    linqPerson.PARENT_ID = txtparentAgency.Text.Trim
        'Else
        '    linqPerson.PARENT_ID = txtNodeID.Text.Trim()
        'End If



        'If chkACTIVE_STATUS_Person.Checked Then
        '    linqPerson.ACTIVE_STATUS = "Y"
        'Else
        '    linqPerson.ACTIVE_STATUS = "N"
        'End If

        'Dim ret As New ProcessReturnInfo

        'Dim dtP As New DataTable
        'dtP.Columns.Add("DTPerson", GetType(TbOuContactLinqDB))
        'Dim conLnq As New TbOuContactLinqDB
        'conLnq.PARENT_TYPE = (Contact_Parent_Type.Person)
        'conLnq.TELEPHONE = txtPersonTelephone.Text.Trim
        'conLnq.EMAIL = txtPersonEmail.Text.Trim
        'conLnq.DESCRIPTION = txtPersonNote.Text.Trim

        'dtP.Rows.Add(conLnq)

        'Dim persontype As Integer
        'If RaAssistant.Checked = True Then
        '    persontype = 0
        'ElseIf RaRecipient.Checked = True Then
        '    persontype = 1

        'End If

        'ret = ENG.SaveOUPerson(linqPerson, dtP, persontype, UserName)
        'If ret.IsSuccess = True Then

        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmStructure.aspx';", True)
        '    ClearData()
        'Else
        '    Alert(ret.ErrorMessage)
        'End If

    End Sub
    Protected Sub btnPersonCancel_Click(sender As Object, e As EventArgs) Handles btnPersonCancel.Click
        PNLmyModalPerson.Visible = False
        ClearData()
    End Sub
    Protected Sub btnPersonViewCancel_Click(sender As Object, e As EventArgs) Handles btnPersonViewCancel.Click
        PNLmyModalPersonView.Visible = False
        ClearData()
    End Sub
    Protected Sub btnGetOrganizeID_Click(sender As Object, e As EventArgs) Handles btnGetOrganizeID.Click
        'strOrganizeID = txtNodeID.Text
        UCFormOUPerson.ParentNodeID = txtNodeID.Text
        PNLmyModalPerson.Visible = True
    End Sub
    Protected Sub btnGetAgencyToEdit_Click(sender As Object, e As EventArgs) Handles btnGetAgencyToEdit.Click
        FillInDataAgency(txtNodeID.Text.Trim)
    End Sub
    Protected Sub btnGetAgencyToView_Click(sender As Object, e As EventArgs) Handles btnGetAgencyToView.Click
        ViewDataAgency(txtNodeID.Text.Trim)
    End Sub
    Protected Sub btnGetPersonToEdit_Click(sender As Object, e As EventArgs) Handles btnGetPersonToEdit.Click
        FillInDataPerson(txtNodeID.Text)
    End Sub
    Protected Sub btnGetPersonToView_Click(sender As Object, e As EventArgs) Handles btnGetPersonToView.Click
        ViewDataPerson(txtNodeID.Text, "")
    End Sub


    '---------------------------------------

    Private Sub txtFilterToolIDs_TextChanged(sender As Object, e As EventArgs) Handles txtFilterToolIDs.TextChanged
        'TRV_OU.SelectedValue = txtFilterToolIDs.Text

        'Dim node As TreeNode() = TRV_OU.Nodes.Find("<selected word>", True)
        'If node IsNot Nothing Then
        '    'Or, also tried : If node(0) IsNot Nothing Then
        '    TRV_OU.SelectedNode = node(0)
        '    TRV_OU.Focus()
        'Else
        '    TRV_OU.CollapseAll()
        'End If

        FindByText()
    End Sub

    Private Sub FindByText()
        Dim nodes As TreeNodeCollection = TRV_OU.Nodes
        'Dim n As TreeNode
        For Each n As TreeNode In nodes
            If n.Text.Contains(txtFilterToolIDs.Text) Then

                'NodeIndent.ForeColor = System.Drawing.Color.MidnightBlue
                'TRV_OU.BackColor = System.Drawing.Color.PowderBlue
                'TRV_OU.Font.Bold = True


                'Try
                '    n.Parent.Expanded = True
                'Catch ex As Exception

                'End Try
                'TRV_OU.BackColor = Drawing.Color.CornflowerBlue

                Exit For
                'Alert(n.Text)
            Else
                Try
                    n.Expand()
                Catch ex As Exception

                End Try

                'Alert("Not Found")
            End If
        Next



    End Sub


    'Protected void FindByText()
    '{
    '    TreeNodeCollection nodes = tvOptions.Nodes;
    '    foreach (TreeNode n in nodes)
    '    {
    '        // if the text properties match, color the item
    '        If (n.Text.Contains(this.txtSearch.Text) == True)
    '        {
    '            n.Parent.Expanded = true;
    '            lblResult.Text = n.Text;
    '        }
    '        Else
    '        {
    '            n.Expand();
    '            lblResult.Text = "Not Found";
    '        }
    '    }






    '}

End Class