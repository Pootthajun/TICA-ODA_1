﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmContribuiltion_Detail_Info.aspx.vb" Inherits="frmContribuiltion_Detail_Info" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<%@ Register Src="~/usercontrol/UCContact.ascx" TagPrefix="uc1" TagName="UCContact" %>
<%@ Register Src="usercontrol/UCFileUploadList.ascx" TagName="UCFileUploadList" TagPrefix="uc2" %>

<%--<%@ Register Src="usercontrol/UCActivityTabContribuiltion.ascx" TagName="UCActivityTabContribuiltion" TagPrefix="uc2" %>--%>
<%@ Register Src="usercontrol/UCActivityTreeList.ascx" TagName="UCActivityTreeList" TagPrefix="uc3" %>

<%@ Register Src="~/usercontrol/UCBudgetContribution.ascx" TagPrefix="uc4" TagName="UCBudgetContribution" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .tdborder {
            border-style: outset;
            border-width: 1px;
        }

        .tdbordertb {
            border-top-style: outset;
            border-bottom-style: outset;
            border-top-width: 1px;
            border-bottom-width: 1px;
        }

        .tdborderr {
            border-right-style: outset;
            border-right-width: 1px;
        }

        .tdborderl {
            border-left-style: outset;
            border-left-width: 1px;
        }

        .tdhearderinfo {
            text-align: center;
            background-color: #CED8F6;
        }

        .tdhearder {
            text-align: center;
            background-color: #F4F2EA;
        }

        .tdinprogress {
            /*background-color:#E1FFD4;*/
            background-color: #E1FFD4;
            
            border-top-style: solid;
            /*border-right-style:solid;*/
            border-bottom-style: solid;
            border-top-width: 1px;
            /*border-right-width:1px;*/
            border-bottom-width: 1px;
            border-top-color: #0AFB43;
            /*border-right-color:#00ff21;*/
            border-bottom-color: #0AFB43;
        }

        .tdallprogress {
            /*background-color:#A7D991;*/
            background-color: #FAFAFA;
            border-top-style: solid;
            /*border-right-style:solid;*/
            border-bottom-style: solid;
            border-top-width: 1px;
            /*border-right-width:1px;*/
            border-bottom-width: 1px;
            border-top-color: #0AFB43;
            /*border-right-color:#00ff21;*/
            border-bottom-color: #0AFB43;
        }

        .tdblank {
            background-color: #ffffff;
        }

        .btn-print {
            margin: 10px 15px;
        }

        textarea {
        resize: none;
        overflow: hidden;
        min-height: 50px;
        }


    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
             <div class="row" style="margin-top:-5px;">
                    <div class="col-md-12">
                        <h3>
                            <asp:Label ID="lblHeadProjectType" runat="server" Text=""></asp:Label>  
                        </h3>
                    </div>
                    <div class="col-md-12">
                        <table  class="table" style="padding: 0px;margin: 0px;" >
                            <tr>
                                <td style="width:150px; text-align:center ">
                                    <asp:Label ID="lblTitle_Project" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtProjectID"  Enabled="false"  ReadOnly="true"  style=" text-align:center ; border-style:None;font-size:14pt;width:100%;background-color: #ecf0f5;" runat="server" Text=""  ></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProjectName" runat="server" onkeyup="auto_grow(this)" PlaceHolder="Contribution Name"  TextMode="MultiLine" style=" min-height:40px; border-style:None;font-size:14pt;width:100%;background-color: #f4f4f4;" MaxLength="500" ></asp:TextBox>
                                    
                                </td>
                            </tr>

                        </table>
                    </div>
            </div>


            <ol class="breadcrumb">
                <li><a href="frmProjectStatus.aspx"><i class="fa fa-area-chart"></i>Overall</a></li>
                <li><a href="#" runat="server" id="nav">
                    <asp:Label ID="lblNavProjectType" runat="server" Text=""></asp:Label></a></li>
                <li class="active">
                    <asp:Label ID="lblProjectType" runat="server" Text=""></asp:Label>
                    Detail
                </li>
            </ol>
        </section>
        <!-- End Content Header (Page header) -->



        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>

                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="" id="liTabProjectDetail" runat="server">
                                            <asp:LinkButton ID="btnTabProjectDetail" runat="server"  style="padding :4px 10px 5px 20px;" >
                                        <div class="iti-flag gb" style="float:left; margin-top:3px; margin-right:3px; cursor:pointer;"></div><h5><span style="cursor:pointer"><i class="fa fa-list-alt distance"></i>Information(ข้อมูลโครงการ)</span></h5>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="" id="liTabActivity" runat="server">
                                            <asp:LinkButton ID="btnTabActivity" runat="server"  style="padding :4px 4px 5px 5px;">
                                <div class="iti-flag ru" style="float:left; margin-top:0px; margin-right:3px; cursor:pointer; "></div><h5><span style="cursor:pointer"><i class="fa fa-th-list distance"></i>Activity(กิจกรรม)</span></h5>
                                            </asp:LinkButton>
                                        </li>
                                       

                                        <%--Print Button--%>
                                        <li class="pull-right">
                                           

                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown" style="margin-top:2px;">
                                                    <i class="fa fa-print"></i>
                                                </button>

                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <div style="">

                                       <%--Hide ifram for preview--%>
                                        <iframe id="myframe" src="" frameborder="0" height="0px" width="0px" runat="server"></iframe>

                                        <%--tabProjectDetail--%>
                                        <asp:Panel ID="tabProjectDetail" runat="server">

                                            <div class="tab-pane active" id="tab_1">
                                                <div class="box-body" style="margin-top: -20px;">
                                                    <table class="table table-bordered">
                                                        <tr class="bg-info">
                                                            <th colspan="2">
                                                                <h5 style="margin: 2px;"><b class="text-blue">Informations (ข้อมูลทั่วไป) </b></h5>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p class="">Plan<br /></p>
                                                                    <p class="pull-right">(แผน) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="DropDownListPlan" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 260px; text-align: right;">
                                                                <p>Description</p>
                                                                <p class="pull-right">(รายละเอียด) :</p>
                                                            </td>
                                                            <td class="">
                                                                <div class="col-sm-12">
                                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="4000"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 260px; text-align: right;">
                                                                <p><span style="color: red;">*</span>Start/End Date </p>
                                                                <p class="pull-right">(วันเริ่มต้น/สิ้นสุดโครงการ) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-3">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder=""></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                                    <!-- /.input group -->

                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <h5>
                                                                        <p>TO (ถึง)</p>

                                                                        <h5></h5>

                                                                        <h5></h5>

                                                                        <h5></h5>

                                                                    </h5>
                                                                </div>
                                                                <div class="col-sm-3 left-8">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder=""></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                                                                                <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p class="pull-right"><span style="color: red;">*</span>
                                                                    Cooperation Framework  </b>
                                                <br />
                                                                    <p class="pull-right">(กรอบความร่วมมือ) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlCooperationFramework"   runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p class="pull-right">
                                                                    Cooperation Type </b>
                                                <br />
                                                                    <p class="pull-right">(ประเภทความร่วมมือ) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlCooperationType" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p>OECD Aid Type</p>
                                                                <p class="pull-right">(ประเภท OECD):</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlOECDAidType" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>

                                                        <tr class="bg-info">
                                                            <th  colspan="2">
                                                                <h5  style="margin: 2px;"><b class="text-blue">Agency (หน่วยงาน) </b></h5>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p class="pull-right">
                                                                    Funding Agency </b>
                                                <br />
                                                                    <p class="pull-right">(หน่วยงานให้ความช่วยเหลือ) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlFundingAgency" runat="server" CssClass="form-control select2" Style="width: 100%" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p class="pull-right">
                                                                    Executing Agency </b>
                                                <br />
                                                                    <p class="pull-right">(หน่วยงานดำเนินการ) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlExecutingAgency" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>

                                                      <%--  ตัดย้ายไปไว้ใน Activity สำหรับ Contribute เท่านั้น--%>
<%--                                                        <tr>
                                                            <td style="width: 260px; text-align: right;">
                                                                <p>Multilateral Organizations</p>
                                                                <p class="pull-right">(องค์กรพหุภาคี) :</p>
                                                            </td>
                                                            <td class="">
                                                                <div class="col-sm-12">
                                                                    <uc4:UCBudgetContribution runat="server" ID="UCBudgetContribution" />
                                                                </div>
                                                            </td>
                                                        </tr>--%>

                                                        <tr>
                                                            <td style="width: 260px; text-align: right;">
                                                                <p>Implementing Agency</p>
                                                                <p class="pull-right">(หน่วยงานความร่วมมือ) :</p>
                                                            </td>
                                                            <td class="">
                                                                <div class="col-sm-12">
                                                                    <asp:DropDownList ID="ddlImplementingAgency" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </td>
                                                        </tr>




                                                        <tr class="bg-info">
                                                            <th  colspan="2">
                                                                <h5  style="margin: 2px;"><b class="text-blue">Other Detail (รายละเอียดอื่นๆ) </b></h5>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p>Assistant </p>
                                                                <p class="pull-right">(ผู้ช่วยโครงการ) :</p>
                                                            </td>
                                                            <td>
                                                                <asp:ListBox ID="ctlSelectAssistant" runat="server" CssClass="form-control select2" SelectionMode="Multiple"
                                                                    data-placeholder="-" Style="width: 100%;"></asp:ListBox>
                                                                <asp:TextBox ID="txtAssistant" runat="server" CssClass="form-control" placeholder="" Visible="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p>Contact Person </p>
                                                                <p class="pull-right">(ข้อมูลผู้ติดต่อ) :</p>
                                                            </td>
                                                            <td>
                                                                <uc1:UCContact runat="server" ID="UCContact" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; text-align: right">
                                                                <p>Note</p>
                                                                <p class="pull-right">(หมายเหตุ) :</p>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Height="60px" Width="100%"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; text-align: right">
                                                                <p>File input </p>
                                                                <p class="pull-right">(เพิ่มไฟล์) :</p>
                                                            </td>
                                                            <td>
                                                                <uc2:UCFileUploadList ID="UCFileUploadList1" runat="server" FileFilter="DOCX,XLSX,VSDX,PDF,PPTX" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 250px; text-align: right">
                                                                <p>Transfer Project To </p>
                                                                <p class="pull-right">(มอบหมายโครงการให้) :</p>
                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-users"></i>
                                                                    </div>

                                                                    <asp:DropDownList ID="ddlTransferto" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <!-- /.form-group -->
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                        </asp:Panel>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Custom Tabs -->

                        <div class="box-footer">
                            <div class="col-sm-9"></div>
                            <div class="col-lg-8"></div>
                            <div class="col-lg-2">
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save</asp:LinkButton>
                            </div>
                            <div class="col-lg-2">
                                <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google">
                                                        <i class="fa fa-reply"></i> Cancel

                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    // Specify the normal table row background color
                    //   and the background color for when the mouse 
                    //   hovers over the table row.

                    var TableBackgroundNormalColor = "#ffffff";
                    var TableBackgroundMouseoverColor = "#F2F2F2";

                    // These two functions need no customization.
                    function ChangeBackgroundColor(row, text) {
                        row.style.backgroundColor = TableBackgroundMouseoverColor;
                        text.style.backgroundColor = TableBackgroundMouseoverColor;
                    }

                    function RestoreBackgroundColor(row, text) {
                        row.style.backgroundColor = TableBackgroundNormalColor;
                        text.style.backgroundColor = TableBackgroundNormalColor;
                    }

                    function auto_grow(element) {
                        element.style.height = "5px";
                        element.style.height = (element.scrollHeight) + "px";
                    }

                </script>

            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />



</asp:Content>




