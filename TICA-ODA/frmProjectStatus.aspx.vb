﻿Imports System.Data
Imports System.Data.SqlClient
Imports ODAENG
Imports Constants

Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Globalization

Partial Class frmProjectStatus
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property



    Public Property DT_DataList As DataTable
        Get
            Try
                Return Session("DT_DataList")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("DT_DataList") = value
        End Set
    End Property

    Private Sub frmProjectStatus_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuProjectStatus")
        li.Attributes.Add("class", "active")
        If IsPostBack = False Then
            '==Start Authorize
            If AuthorizeDT Is Nothing Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
            End If


            Bind_Cooperation_Master_Col()
            BindData_Summary_Cooperation()

            Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Project & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length > 0 Then
                div_Project.Attributes.Add("class", "small-box bg-aqua poiter")
                div_Project.Attributes.Add("onclick", "link(" & Constants.Project_Type.Project & ")")
            Else
                div_Project.Attributes.Add("class", "small-box bg-gray")
            End If

            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Non_Project & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length > 0 Then
                div_NonProject.Attributes.Add("class", "small-box bg-green poiter")
                div_NonProject.Attributes.Add("onclick", "link(" & Constants.Project_Type.NonProject & ")")
            Else
                div_NonProject.Attributes.Add("class", "small-box bg-gray")
            End If

            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Loan & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length > 0 Then
                div_Loan.Attributes.Add("class", "small-box bg-yellow poiter")
                div_Loan.Attributes.Add("onclick", "link(" & Constants.Project_Type.Loan & ")")
            Else
                div_Loan.Attributes.Add("class", "small-box bg-gray")
            End If

            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Contribution & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length > 0 Then
                div_Contribution.Attributes.Add("class", "small-box bg-red poiter")
                div_Contribution.Attributes.Add("onclick", "link(" & Constants.Project_Type.Contribuition & ")")
            Else
                div_Contribution.Attributes.Add("class", "small-box bg-gray")
            End If
            '==End Authorize

            lblTotalProject.Text = " 0 โครงการ"
            lblTotalNonProject.Text = " 0 โครงการ"
            lblTotalLoan.Text = " 0 โครงการ"
            lblTotalContribution.Text = " 0 โครงการ"
            lbltextcomponent.Text = "Top 5 Component (5 อันดับ ประเภทความช่วยเหลือ)" & " ปี " & Today.Year

            Dim dt As New DataTable
            dt = BL.GetCountProjectByProjectType()
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim cntpj As Integer = CInt(dt.Rows(i)("cnt"))
                    Dim pjtype As Integer = CInt(dt.Rows(i)("project_type"))

                    If pjtype = Project_Type.Project Then
                        lblTotalProject.Text = cntpj & " โครงการ"
                    End If

                    If pjtype = Project_Type.NonProject Then
                        lblTotalNonProject.Text = cntpj & " โครงการ"
                    End If

                    If pjtype = Project_Type.Loan Then
                        lblTotalLoan.Text = cntpj & " โครงการ"
                    End If

                    If pjtype = Project_Type.Contribuition Then
                        lblTotalContribution.Text = cntpj & " โครงการ"
                    End If

                Next
            End If



        End If
        BindTop5Project()
        BindTop5Component()
    End Sub

    Private Sub BindTop5Project()
        Dim dt As New DataTable
        dt = BL.GetCountProjectLastestFiveYear
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Script As String = ""
        Script &= " <script> $(function () {"
        Script &= """use strict"";"
        Script &= " var bar = new Morris.Bar({"
        Script &= "     element: 'bar-chart',"
        Script &= "     resize: true,"
        Script &= "     data: ["

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim y As String = dt.Rows(i)("stryear").ToString
            Dim a As String = dt.Rows(i)("a").ToString
            Dim b As String = dt.Rows(i)("b").ToString
            Dim c As String = dt.Rows(i)("c").ToString
            Dim d As String = dt.Rows(i)("d").ToString

            Script &= "         {y: '" & y & "', a: " & a & ", b: " & b & ",c: " & c & ",d:" & d & "}"
            If i < (dt.Rows.Count - 1) Then
                Script &= ","
            End If
        Next

        Script &= "     ],"
        Script &= "     barColors: ['#00c0ef', '#00a65a', '#f39c12', '#dd4b39'],"
        Script &= "     xkey: 'y',"
        Script &= "     ykeys: ['a', 'b','c','d'],"
        Script &= "     labels: ['Project', 'Non-Project','Loan','Contribuition'],"
        Script &= "     hideHover: 'auto'"
        Script &= "     });"
        Script &= " }); </script>"
        lblChart.Text = Script
    End Sub

    Private Sub BindTop5Component()
        Dim dt As New DataTable
        dt = BL.GetCountComponentProject()
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim TextHtml As String = "" ' "<div class='col-sm-12'>"

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim component_name As String = dt.Rows(i)("component_name").ToString.Replace(vbNullChar, "")
            Dim count_pj As String = dt.Rows(i)("count_pj").ToString
            Dim percent_pj As String = dt.Rows(i)("percent_pj").ToString

            TextHtml &= "  <div class='clearfix'>"
            TextHtml &= "     <span Class='pull-left'>" & i + 1 & "." & component_name & " </span>"
            TextHtml &= "     <p class='pull-right'>" & count_pj & " โครงการ</p>"
            TextHtml &= "  </div>"
            TextHtml &= "  <div class=""progress sm"">"
            TextHtml &= "        <div Class='progress-bar progress-bar-light-blue' style='width: " & percent_pj & "%;'></div>"
            TextHtml &= "  </div>"
        Next


        'TextHtml &= " </div>"

        lblComponent.Text = TextHtml
    End Sub




    '====================Chart===================================


    Function Get_Cooperation_Master() As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "SELECT id,cptype_id,cptype_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationtype WHERE ACTIVE_STATUS='Y' Order by sort  "
        DT = SqlDB.ExecuteTable(SQL)

        Return DT
    End Function

    Private Sub Bind_Cooperation_Master_Col()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        rptCol.DataSource = DT_Cooperation
        rptCol.DataBind()


    End Sub

    Private Sub BindData_Summary_Cooperation()
        Dim DT As New DataTable
        Dim SQL As String = ""

        '------ข้อมูล Cooperation จาก Master เพื่อสร้างเป็น Collumn------
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()


        DT = SqlDB.ExecuteTable(SQL)
        Dim str_Field_SUM As String = ""

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  Select *, (" & str_Field_SUM & ")  SUM_Total FROM (  " & vbNewLine

        '----------------------------------------------------------
        SQL &= "  Select  ID, regionoda_name  " & vbNewLine
        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ") SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & "          " & vbNewLine

            Next
        End If
        SQL &= "    FROM (   " & vbNewLine
        '---------------------------------------------------------

        SQL &= "  Select																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  --//---Loop เพื่อหา cooperation_type_id สร้าง คอลัมน์																				   " & vbNewLine

        If (DT_Cooperation.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Cooperation.Rows.Count - 1

                SQL &= "  ,Case When cooperation_type_id =" & DT_Cooperation.Rows(i).Item("id") & " 																							 " & vbNewLine
                SQL &= "        Then SUM(SUM_Payment_Actual)																							 " & vbNewLine
                SQL &= "  	  Else 0																													 " & vbNewLine
                SQL &= "  	  End SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " 																									 " & vbNewLine

                str_Field_SUM += "SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & " + "
            Next
        End If

        If str_Field_SUM <> "" Then
            str_Field_SUM = str_Field_SUM.Substring(0, str_Field_SUM.Length - 2)
        End If

        SQL &= "  																																 " & vbNewLine
        SQL &= "  FROM TB_Regionzoneoda																											 " & vbNewLine
        SQL &= "  LEFT JOIN 																													 " & vbNewLine
        SQL &= "  			 (																													 " & vbNewLine
        SQL &= "  			  Select vw_Activity.project_id																						 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.start_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.end_date																							 " & vbNewLine
        SQL &= "  			  ,vw_AllProject.cooperation_type_id																				 " & vbNewLine
        SQL &= "  			  ,Data_Project_Recipience_RegionZone.*  FROM (																		 " & vbNewLine
        SQL &= "  			  --====== กิจกรรมและจำนวนเงิน ของ Region Zone ============															   " & vbNewLine
        SQL &= "  			  Select TB_Activity_Expense_Header.Activity_id, Data_Activity_Expense_Actual.* 									 " & vbNewLine
        SQL &= "  				  ,(Select dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')) node_id_OU 								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode(vw_ou.node_id ,'TB_OU_Country')) name_th_OU  								 " & vbNewLine
        SQL &= "  				  ,(select dbo.GetParentByChildNode_eng(vw_ou.node_id ,'TB_OU_Country')) name_en_OU  							 " & vbNewLine
        SQL &= "  				  ,TB_OU_CountryZone.country_zone_id , TB_Regionzoneoda.regionoda_name											 " & vbNewLine
        SQL &= "  			  FROM (																											 " & vbNewLine
        SQL &= "  			  select Header_id,SUM(Pay_Amount_Actual) SUM_Payment_Actual   ,Recipience_id										 " & vbNewLine
        SQL &= "  			  from TB_Activity_Expense_Actual_Detail																			 " & vbNewLine
        SQL &= "  			  GROUP BY Header_id,Recipience_id																					 " & vbNewLine
        SQL &= "  			  ) AS Data_Activity_Expense_Actual																					 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Activity_Expense_Header ON TB_Activity_Expense_Header.id=Data_Activity_Expense_Actual.Header_id		 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  -- หา ผู้รับทุนอยู่ region zone อะไร																					  " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_ou ON Data_Activity_Expense_Actual.Recipience_id = vw_ou.node_id										 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_OU_CountryZone ON TB_OU_CountryZone.country_id =dbo.GetParentByChildNode_id(vw_ou.node_id ,'TB_OU_Country')			 " & vbNewLine
        SQL &= "  			  LEFT JOIN TB_Regionzoneoda ON TB_Regionzoneoda.id = TB_OU_CountryZone.country_zone_id								 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  			  WHERE TB_Activity_Expense_Header.Activity_id IS NOT NULL 															 " & vbNewLine
        SQL &= "  			  ) AS Data_Project_Recipience_RegionZone 																			 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_Activity ON vw_Activity.activity_id = Data_Project_Recipience_RegionZone.Activity_id					 " & vbNewLine
        SQL &= "  			  LEFT JOIN vw_AllProject ON vw_AllProject.id =vw_Activity.project_id 												 " & vbNewLine
        SQL &= "  			  																													 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  ) AS Data_Project_Recipience_RegionZone  ON Data_Project_Recipience_RegionZone.country_zone_id = TB_Regionzoneoda.id			 " & vbNewLine
        SQL &= "  																																 " & vbNewLine
        SQL &= "  GROUP BY 																														 " & vbNewLine
        SQL &= "  TB_Regionzoneoda.id,TB_Regionzoneoda.regionoda_name,cooperation_type_id														 " & vbNewLine

        SQL &= "  )AS TB_SUM  Group by id,regionoda_name  " & vbNewLine
        SQL &= "  )AS TB   " & vbNewLine
        SQL &= "  Order by  regionoda_name  " & vbNewLine


        If (DT_Cooperation.Rows.Count > 0) Then
            SQL &= "  "
        End If


        DT = SqlDB.ExecuteTable(SQL)
        'Session("DT_Data_Summary_Cooperation") = DT

        DT_DataList = DT

        BindTotal()
        BindPerTotal()

        rptList.DataSource = DT
        rptList.DataBind()

        'If (DT.Rows.Count > 0) Then
        '    i_Programme.Attributes("class") = "fa fa-plus"
        '    div_Programme.Style("display") = "none"

        '    i_region.Attributes("class") = "fa fa-plus"
        '    div_region.Style("display") = "none"
        'End If



    End Sub

    Private Sub BindTotal()

        Dim DT As DataTable = DT_DataList()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()

        Dim DT_Data As New DataTable
        Dim DR As DataRow

        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")

        If (DT.Rows.Count > 0) Then


            If (DT_Cooperation.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    DR = DT_Data.NewRow
                    DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", "")).ToString("#,##0.00")
                    DT_Data.Rows.Add(DR)

                Next

            End If




            '---% row ของ sub-region        
            Dim myAL As New ArrayList()
            If (DT.Rows.Count > 0) Then
                For i As Integer = 0 To DT.Rows.Count - 1
                    Try
                        myAL.Add(Convert.ToDecimal(Convert.ToDecimal((DT.Rows(i).Item("SUM_Total") / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")))

                    Catch ex As Exception
                        myAL.Add(Convert.ToDecimal(Convert.ToDecimal(0.00).ToString("#,##0.00")))

                    End Try

                Next
            End If

            lbl_Footer_Tatal.Text = Convert.ToDecimal(DT.Compute("SUM(SUM_Total)", "")).ToString("#,##0.00")

            If (Convert.ToDecimal(DT.Compute("SUM(SUM_Total)", "")) = 0) Then

                If (DT.Rows.Count > 0) Then
                    i_Programme.Attributes("class") = "fa fa-plus"
                    div_Programme.Style("display") = "none"

                    i_region.Attributes("class") = "fa fa-plus"
                    div_region.Style("display") = "none"
                End If

            End If

            rptTotal.DataSource = DT_Data
            rptTotal.DataBind()

            '----bind Chart Sub-region----
            If (DT.Rows.Count > 0) Then
                Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())
                Chart_Sub_region.Series("Series_Sub_region").Points.DataBindY(YValue)
                For i As Integer = 0 To DT.Rows.Count - 1
                    Dim Value_SUM_Pay_Amount_Actual As Double = DT.Rows(i).Item("SUM_Total")
                    Chart_Sub_region.Series("Series_Sub_region").Points(i).LegendText = DT.Rows(i).Item("regionoda_name").ToString()
                    Chart_Sub_region.Series("Series_Sub_region").Points(i).ToolTip = DT.Rows(i).Item("regionoda_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " ฿"
                    If (Value_SUM_Pay_Amount_Actual > 0) Then
                        'Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = YValue(i) & " % "
                        Chart_Sub_region.Series("Series_Sub_region").Points(i).ToolTip &= " เฉลี่ย : " & YValue(i) & " % "
                    Else
                        'Chart_Sub_region.Series("Series_Sub_region").Points(i).Label = ""
                    End If

                    Chart_Sub_region.Series("Series_Sub_region").YValueType = DataVisualization.Charting.ChartValueType.Auto
                    'Chart_Sub_region.ChartAreas(0).Area3DStyle.Enable3D = True
                    'Chart_Sub_region.ChartAreas(0).Area3DStyle.Inclination = 40

                    'Chart_Sub_region.Series("Series_Sub_region").Points(i)("PieLineColor") = "Black"
                    'Chart_Sub_region.Series("Series_Sub_region")("PieLabelStyle") = "Outside"
                Next

            End If
        End If


    End Sub

    Private Sub BindPerTotal()

        Dim DT As DataTable = DT_DataList()
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        Dim DT_Data As New DataTable
        Dim DR As DataRow
        DT_Data.Columns.Add("SUM_Pay_Amount_Actual")
        DT_Data.Columns.Add("Amount_Actual")

        If (DT.Rows.Count > 0) Then


            Dim myAL As New ArrayList()
            If (DT_Cooperation.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    DR = DT_Data.NewRow
                    'DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", "")) / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")

                    Try
                        DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", "")) / (DT.Compute("SUM(SUM_Total)", ""))) * 100).ToString("#,##0.00")

                    Catch ex As Exception
                        DR("SUM_Pay_Amount_Actual") = Convert.ToDecimal(0).ToString("#,##0.00")

                    End Try


                    DR("Amount_Actual") = Convert.ToDecimal((DT.Compute("SUM(SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", "") & ")", ""))).ToString("#,##0.00")

                    DT_Data.Rows.Add(DR)
                    myAL.Add(Convert.ToDecimal(DR("SUM_Pay_Amount_Actual")))

                Next
            End If

            lbl_Per_Footer_Tatal.Text = Convert.ToDecimal(100).ToString("#,##0.00")
            rptPerTotal.DataSource = DT_Data
            rptPerTotal.DataBind()


            Dim Url As String = ""


            Dim ArrPastel() As String = {"#87CEEB", "#32CD32", "#BA55D3", "#F08080", "#4682B4", "#9ACD32", "#40E0D0", "#FF69B4", "#F0E68C", "#D2B48C", "#8FBC8B", "#6495ED", "#DDA0DD", "#5F9EA0", "#FFDAB9", "#FFA07A"}
            Dim Get_Color_Pie As String = ""
            '----bind Chart Programme----
            If (DT_Data.Rows.Count > 0) Then
                Dim YValue As Decimal() = CType(myAL.ToArray(GetType(Decimal)), Decimal())
                'Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}
                ChartByProgramme.Series("Series1").Points.DataBindY(YValue)
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    Dim Value_SUM_Pay_Amount_Actual As Double = DT_Data.Rows(i).Item("SUM_Pay_Amount_Actual")
                    ChartByProgramme.Series("Series1").Points(i).LegendText = DT_Cooperation.Rows(i).Item("cptype_name").ToString()
                    ChartByProgramme.Series("Series1").Points(i).ToolTip = DT_Cooperation.Rows(i).Item("detail").ToString() & " : " & Convert.ToDecimal(DT_Data.Rows(i).Item("Amount_Actual")).ToString("#,##0.00") & " ฿  "
                    If (Value_SUM_Pay_Amount_Actual > 0) Then
                        'ChartByProgramme.Series("Series1").Points(i).Label = DT_Cooperation.Rows(i).Item("cptype_name").ToString() & " : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " % "
                        ChartByProgramme.Series("Series1").Points(i).ToolTip &= " เฉลี่ย : " & Convert.ToDecimal(Value_SUM_Pay_Amount_Actual).ToString("#,##0.00") & " % "
                    Else
                        'ChartByProgramme.Series("Series1").Points(i).Label = ""
                    End If

                    ChartByProgramme.Series("Series1").YValueType = DataVisualization.Charting.ChartValueType.Auto

                    '----------Url---------------
                    Url = "frmDashbord_Cooperation.aspx?Cooperation_ID=" & DT_Cooperation.Rows(i).Item("id")
                    ChartByProgramme.Series("Series1").Points(i).Url = Url
                    'ChartByProgramme.ChartAreas(0).Area3DStyle.Enable3D = True
                    'ChartByProgramme.ChartAreas(0).Area3DStyle.Inclination = 40


                    'ChartByProgramme.Series("Series1").Points(i)("PieLineColor") = "Black"
                    'ChartByProgramme.Series("Series1")("PieLabelStyle") = "Outside"

                Next

            End If

        End If
    End Sub




    '=======================================================


    Private Sub rptCol_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCol.ItemDataBound
        Dim lbl_cooperation_type_id As Label = e.Item.FindControl("lbl_cooperation_type_id")
        Dim lbl_cooperation_type_Name As Label = e.Item.FindControl("lbl_cooperation_type_Name")
        Dim th_radius_2 As HtmlTableCell = e.Item.FindControl("th_radius_2")

        Dim ArrBrightPastel() As String = {"#418CF0", "#FCB441", "#E0400A", "#056492", "#BFBFBF", "#1A3B69", "#FFE382", "#129CDD", "#CA6B4B", "#005CDB", "#F3D288", "#506381", "#F1B9A8", "#E0830A", "#7893BE"}

        th_radius_2.Style("background-color") = ArrBrightPastel(e.Item.ItemIndex).ToString()
        th_radius_2.Style("border-radius") = "30px 30px 0px 0px"
        th_radius_2.Style("height") = "30px"
        th_radius_2.Style("padding") = "20px"
        lbl_cooperation_type_id.Text = e.Item.DataItem("id")
        lbl_cooperation_type_Name.Text = e.Item.DataItem("cptype_name").ToString()

    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim lbl_regionoda_name As Label = e.Item.FindControl("lbl_regionoda_name")
        Dim lblTotal_Row As Label = e.Item.FindControl("lblTotal_Row")

        lbl_regionoda_name.Text = e.Item.DataItem("regionoda_name").ToString()

        If (Convert.ToDecimal(e.Item.DataItem("SUM_Total")) > 0) Then

            If Convert.IsDBNull(e.Item.DataItem("SUM_Total")) = False Then
                lblTotal_Row.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Total")).ToString("#,##0.00")
            End If
        Else
            lblTotal_Row.Text = ""
        End If


        Dim rptPayment As Repeater = e.Item.FindControl("rptPayment")
        AddHandler rptPayment.ItemDataBound, AddressOf rptPayment_ItemDataBound
        '-----แปลงข้อมูลแนวนอนเป็นแนวตั้ง -----
        Dim DT_Data_Summary As DataTable = DT_DataList()
        If (DT_Data_Summary.Rows.Count > 0) Then


            DT_Data_Summary.DefaultView.RowFilter = "id=" & e.Item.DataItem("id")
            DT_Data_Summary = DT_Data_Summary.DefaultView.ToTable()

            Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
            Dim DT_Data As New DataTable
            Dim DR As DataRow
            DT_Data.Columns.Add("Pay_Amount_Actual")

            If (DT_Cooperation.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Cooperation.Rows.Count - 1
                    DR = DT_Data.NewRow
                    DR("Pay_Amount_Actual") = DT_Data_Summary.Rows(0).Item("SUM_Payment_Actual_" & DT_Cooperation.Rows(i).Item("cptype_name").ToString().Replace(" ", "").Replace("/", ""))
                    DT_Data.Rows.Add(DR)
                Next

            End If

            rptPayment.DataSource = DT_Data
            rptPayment.DataBind()
        End If

    End Sub

    Protected Sub rptPayment_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim lbl_Pay_Amount_Actual As Label = e.Item.FindControl("lbl_Pay_Amount_Actual")

        '----สร้าง columl ตามจำนวนใน Master----
        Dim DT_Cooperation As DataTable = Get_Cooperation_Master()
        If (DT_Cooperation.Rows.Count > 0) Then

        End If

        If (Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")) > 0) Then

            If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
                lbl_Pay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
            End If
        Else
            lbl_Pay_Amount_Actual.Text = ""
        End If





    End Sub



    '// Total
    Private Sub rptTotal_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTotal.ItemDataBound
        Dim lbl_Footer_SUM As Label = e.Item.FindControl("lbl_Footer_SUM")

        lbl_Footer_SUM.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Pay_Amount_Actual")).ToString("#,##0.00")

    End Sub


    '// %Total
    Private Sub rptPerTotal_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPerTotal.ItemDataBound
        Dim lbl_Per_Footer_SUM As Label = e.Item.FindControl("lbl_Per_Footer_SUM")

        lbl_Per_Footer_SUM.Text = Convert.ToDecimal(e.Item.DataItem("SUM_Pay_Amount_Actual")).ToString("#,##0.00")

    End Sub



End Class
