﻿Imports System.Data
Imports Constants
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmProject_Detail_Info
    Inherits System.Web.UI.Page


    Dim BL As New ODAENG

#Region "Property"
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditProjectID As Long
        Get
            Try
                Return ViewState("ProjectID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectID") = value
        End Set
    End Property

    Public Property DisplayActivity_Type As String
        'month/year
        Get
            Try
                Return ViewState("DisplayActivity_Type")
            Catch ex As Exception
                Return "month"
            End Try
        End Get
        Set(value As String)
            ViewState("DisplayActivity_Type") = value
        End Set
    End Property

    Public Property mode As String
        'view/edit
        Get
            Try
                Return ViewState("mode")
            Catch ex As Exception
                Return "add"
            End Try
        End Get
        Set(value As String)
            ViewState("mode") = value
        End Set
    End Property

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property
#End Region


    Private Sub frmProjectDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuProjectStatus")
        li.Attributes.Add("class", "active")

        ''แยก Tab
        ''btnTabRecipient.Visible = False
        If Me.IsPostBack = False Then
            Try

                ClearForm()
                EditProjectID = CInt(Request.QueryString("ID"))
                mode = Request.QueryString("mode").ToString()
            Catch ex As Exception
                EditProjectID = 0
            End Try

            ' Project Type
            ProjectType = CInt(Request.QueryString("type"))
            If ProjectType = Constants.Project_Type.Project Then
                lblProjectType.Text = "Project"
                lblNavProjectType.Text = "Project"
                lblHeadProjectType.Text = "Project"
            End If

            If ProjectType = Constants.Project_Type.NonProject Then
                lblProjectType.Text = "Non Project"
                lblNavProjectType.Text = "Non Project"
                lblHeadProjectType.Text = "Non Project"
            End If
            'pnlDialog.Visible = False
            'BL.Bind_DDL_Year(ddlBudgetYear)
            'BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)
            'ddlBudgetGroup_SelectedIndexChanged(sender, e)
            BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
            BL.Bind_DDL_Coperationtype(ddlCooperationType)
            BL.Bind_DDL_OECD(ddlOECDAidType)
            BL.Bind_DLL_Plan(DropDownListPlan)
            BL.Bind_DDL_Organize(ddlFundingAgency)
            ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
            BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)
            'BL.Bind_DDL_Perposecat(ddlSector)
            'BL.Bind_DDL_Perpose(ddlSubSector)

            SetAssistant()
            'SetRecipient()
            'SetRecipientCountry()
            SetProjectFile()

            CurrentTab = Tab.ProjectDetail
            'CurrentTabAct = Tab.ActActDetail

            If EditProjectID <> 0 Then
                SetProjectInfoByID(EditProjectID)
                If mode = "view" Then
                    SetControlToViewMode(True)
                Else
                    SetControlToViewMode(False)
                End If
            End If
        Else
            RestoreJQueryUI()
        End If

        If mode = "palnex" Then
            mode = "edit"
        End If

        btnTabActivity.Enabled = False
        ''แยก Tab
        ''btnTabRecipient.Enabled = False
        If txtProjectID.Text.Trim <> "" Then
            btnTabActivity.Enabled = True
            lblTitle_Project.Visible = False
        Else
            lblTitle_Project.Visible = True
        End If


    End Sub


#Region "Event"

    ''แยก tab
    ''Protected Sub rptRecipientList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptRecipientList.ItemDataBound
    ''    If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
    ''        Return
    ''    End If
    ''    Dim lblRecipientNo As Label = DirectCast(e.Item.FindControl("lblRecipientNo"), Label)
    ''    'Dim lblRecipientStudentID As Label = DirectCast(e.Item.FindControl("lblRecipientStudentID"), Label)
    ''    Dim lblRecipientName As Label = DirectCast(e.Item.FindControl("lblRecipientName"), Label)
    ''    Dim lblRecipientCountry As Label = DirectCast(e.Item.FindControl("lblRecipientCountry"), Label)
    ''    Dim lblRecipientCourse As Label = DirectCast(e.Item.FindControl("lblRecipientCourse"), Label)
    ''    Dim lblRecipientStartEndDate As Label = DirectCast(e.Item.FindControl("lblRecipientStartEndDate"), Label)
    ''    'Dim lblRecipientStatus As Label = DirectCast(e.Item.FindControl("lblRecipientStatus"), Label)

    ''    lblRecipientNo.Text = e.Item.DataItem("seq").ToString
    ''    'lblRecipientStudentID.Text = e.Item.DataItem("student_id").ToString
    ''    lblRecipientName.Text = e.Item.DataItem("name_th").ToString
    ''    lblRecipientCountry.Text = e.Item.DataItem("country_name").ToString
    ''    lblRecipientCourse.Text = e.Item.DataItem("course").ToString
    ''    lblRecipientStartEndDate.Text = e.Item.DataItem("period_plan_date").ToString
    ''    'lblRecipientStatus.Text = e.Item.DataItem("status").ToString

    ''End Sub

    Private Sub nav_click(sender As Object, e As EventArgs) Handles nav.ServerClick
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Try
            '## Validate
            If Validate() = False Then
                Exit Sub
            End If

            '##Save
            '##tabProjectDetail##
            '-- lnqProject
            Dim lnqProject As New TbProjectLinqDB
            With lnqProject
                .ID = EditProjectID
                .PROJECT_ID = txtProjectID.Text
                .PROJECT_TYPE = ProjectType 'Project_Type.Project
                .PROJECT_NAME = txtProjectName.Text
                .OBJECTIVE = txtObjective.Text
                .DESCRIPTION = txtDescription.Text
                .START_DATE = Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtStartDate.Text
                .END_DATE = Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH")) 'txtEndDate.Text
                If ddlCooperationFramework.SelectedValue <> "" Then
                    .COOPERATION_FRAMEWORK_ID = ddlCooperationFramework.SelectedValue
                End If

                If ddlCooperationType.SelectedValue <> "" Then
                    .COOPERATION_TYPE_ID = ddlCooperationType.SelectedValue
                End If

                If ddlOECDAidType.SelectedValue <> "" Then
                    .OECD_AID_TYPE_ID = ddlOECDAidType.SelectedValue
                End If

                .LOCATION = txtLocation.Text

                If ddlFundingAgency.SelectedValue <> "" Then
                    .FUNDING_AGENCY_ID = ddlFundingAgency.SelectedValue
                End If

                If ddlExecutingAgency.SelectedValue <> "" Then
                    .EXECUTING_AGENCY_ID = ddlExecutingAgency.SelectedValue
                End If

                .ASSISTANT = GetAssistant()
                .REMARK = txtNote.Text
                If ddlTransferto.SelectedValue <> "" Then
                    .TRANSFER_PROJECT_TO = ddlTransferto.SelectedValue
                End If

            End With

            ''--DTProjectBudget
            'Dim DTProjectBudget As New DataTable
            'DTProjectBudget = UCActivityBudget.BudgetDT

            'Dim sumBudget As Double = 0.0
            'If DTProjectBudget.Rows.Count > 0 Then
            '    sumBudget = DTProjectBudget.Compute("sum(amount)", "")
            'End If
            'lnqProject.ALLOCATE_BUDGET = sumBudget

            '--DTImplementingAgency
            Dim DTImplementingAgency As New DataTable
            DTImplementingAgency = UCImplementingAgency.ImplementingAgencyDT

            '--DTCoFunding
            Dim DTCoFunding As New DataTable
            DTCoFunding = UCCoFunding.CoFundingDT


            '--lnqContact
            Dim DTContact As New DataTable
            DTContact = UCContact.ContactDT
            Dim lnqContact As New TbOuContactLinqDB
            If DTContact.Rows.Count > 0 Then
                Dim _dr As DataRow = DTContact.Rows(0)
                With lnqContact
                    .PARENT_TYPE = Contact_Parent_Type.Project
                    .PARENT_ID = lnqProject.ID
                    .CONTACT_NAME = _dr("contact_name").ToString()
                    .TELEPHONE = _dr("telephone").ToString()
                    .FAX = _dr("fax").ToString()
                    .EMAIL = _dr("email").ToString()
                    .POSITION = _dr("position").ToString()
                End With
            End If


            '##DTProjectFile
            Dim DTProjectFile As DataTable = UCFileUploadList1.GetFileList()

            '##DTProjectSection
            Dim lnqPjPlan As New TbProjectplanLinqDB
            With lnqPjPlan
                .PLAN_ID = DropDownListPlan.SelectedValue
            End With


            Dim ret As New ProcessReturnInfo
            ret = BL.SaveProject(lnqProject, Nothing, DTImplementingAgency, DTCoFunding, lnqContact, DTProjectFile, Nothing, lnqPjPlan, UserName)
            If ret.IsSuccess Then
                txtProjectID.Text = ret.PROJECT_ID
                EditProjectID = ret.ID

                btnTabActivity.Enabled = True
                ''แยก Tab
                ''btnTabRecipient.Enabled = True

                'SetActivity(EditProjectID, DisplayActivity_Type)
                SetProjectFile()

                ' ================ Redirect ================
                Dim redirect As String

                ProjectType = CInt(Request.QueryString("type"))
                If ProjectType = Constants.Project_Type.Project Then
                    redirect = "frmProject_Detail_Activity.aspx?"
                End If
                If ProjectType = Constants.Project_Type.NonProject Then
                    redirect = "frmProject_Detail_Activity.aspx?"
                End If
                Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว'); window.location ='" + redirect + Param + "';", True)
            Else
                alertmsg("ไม่สามารถบันทึกข้อมูลได้ " & ret.ErrorMessage)
            End If

        Catch ex As Exception
            alertmsg("ไม่สามารถบันทึกข้อมูลได้ ")
        End Try

    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        Response.Redirect("frmProject.aspx?type=" & ProjectType)
    End Sub


#End Region

#Region "Sub&Function"

    Private Sub SetProjectInfoByID(id As String)

        Try
            Dim dtProject As New DataTable
            dtProject = BL.GetProjectInfoByID(id)

            '##tabProjectDetail##
            If dtProject.Rows.Count > 0 Then
                Dim _project_id As String = ""
                Dim _project_name As String = ""
                Dim _objective As String = ""
                Dim _description As String = ""
                Dim _start_date As String = ""
                Dim _end_date As String = ""
                Dim _allocate_budget As String = ""
                'Dim _budgetyear As String = ""
                'Dim _budget_group_id As String = ""
                'Dim _budget_sub_id As String = ""
                Dim _cooperation_framework_id As String = ""
                Dim _cooperation_type_id As String = ""
                Dim _oecd_aid_type_id As String = ""
                Dim _location As String = ""
                Dim _funding_agency_id As String = ""
                Dim _executing_agency_id As String = ""
                Dim _remark As String = ""
                Dim _transfer_project_to As String = ""
                Dim _assistant As String = ""

                With dtProject
                    _project_id = .Rows(0)("project_id").ToString()
                    _project_name = .Rows(0)("project_name").ToString()
                    _objective = .Rows(0)("objective").ToString()
                    _description = .Rows(0)("description").ToString()
                    _start_date = Convert.ToDateTime(.Rows(0)("start_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _end_date = Convert.ToDateTime(.Rows(0)("end_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                    _allocate_budget = .Rows(0)("allocate_budget").ToString()
                    '_budgetyear = .Rows(0)("budgetyear").ToString()
                    '_budget_group_id = .Rows(0)("budget_group_id").ToString()
                    '_budget_sub_id = .Rows(0)("budget_sub_id").ToString()
                    _cooperation_framework_id = .Rows(0)("cooperation_framework_id").ToString()
                    _cooperation_type_id = .Rows(0)("cooperation_type_id").ToString()
                    _oecd_aid_type_id = .Rows(0)("oecd_aid_type_id").ToString()
                    _location = .Rows(0)("location").ToString()
                    _funding_agency_id = .Rows(0)("funding_agency_id").ToString()
                    _executing_agency_id = .Rows(0)("executing_agency_id").ToString
                    _remark = .Rows(0)("remark").ToString
                    _transfer_project_to = .Rows(0)("transfer_project_to").ToString
                    _assistant = .Rows(0)("assistant").ToString
                End With

                txtProjectID.Text = _project_id
                txtProjectName.Text = _project_name
                txtObjective.Text = _objective
                txtDescription.Text = _description
                txtStartDate.Text = _start_date
                txtEndDate.Text = _end_date

                If _cooperation_framework_id <> "" Then ddlCooperationFramework.SelectedValue = _cooperation_framework_id
                If _cooperation_type_id <> "" Then ddlCooperationType.SelectedValue = _cooperation_type_id
                If _oecd_aid_type_id <> "" Then ddlOECDAidType.SelectedValue = _oecd_aid_type_id
                txtLocation.Text = _location
                Try
                    If _funding_agency_id <> "" Then
                        ddlFundingAgency.SelectedValue = _funding_agency_id
                        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)
                        If _executing_agency_id <> "" Then ddlExecutingAgency.SelectedValue = _executing_agency_id
                    End If
                Catch ex As Exception
                End Try

                UCContact.ContactDT = dtProject
                txtNote.Text = _remark
                ddlTransferto.SelectedValue = _transfer_project_to
                Dim _assistantname As String = ""
                If _assistant <> "" Then
                    Dim _strAssistant() As String = _assistant.Split(", ")
                    For Each Item As ListItem In ctlSelectAssistant.Items
                        For i As Integer = 0 To _strAssistant.Length - 1
                            If Item.Value = _strAssistant(i) Then
                                Item.Selected = True
                                _assistantname &= Item.Text & ","
                            End If
                        Next
                    Next
                End If

                txtAssistant.Text = ""
                If _assistantname.Length > 0 Then
                    txtAssistant.Text = _assistantname.Substring(0, _assistantname.Length - 1)
                End If
            End If

            Dim dtImplementingAgency As New DataTable
            dtImplementingAgency = BL.GetProjectImplementingAgency(id)
            If dtImplementingAgency.Rows.Count > 0 Then
                UCImplementingAgency.ImplementingAgencyDT = dtImplementingAgency
            End If

            Dim dtCoFunding As New DataTable
            dtCoFunding = BL.GetProjectCoFunding(id)
            If dtCoFunding.Rows.Count > 0 Then
                UCCoFunding.CoFundingDT = dtCoFunding
            End If

            '##tabActivity
            'SetActivity(id, DisplayActivity_Type)


            'ตรงนี้GetProjectPlan
            Dim ProjectPlan As New DataTable
            ProjectPlan = BL.GetProjectPlan(id)
            If ProjectPlan.Rows.Count > 0 Then
                DropDownListPlan.SelectedValue = ProjectPlan.Rows(0)("plan_id").ToString()
            End If


            ''แยก Tab
            ''UCActivityTreeList1.GenerateActivityList(id, mode)
            ''UCActivityTabProject1.ProjectID = id
            ''UCActivityTabProject1.ActivityMode = mode



        Catch ex As Exception
            alertmsg(ex.Message.ToString)
        End Try
    End Sub

    Private Sub SetControlToViewMode(IsView As Boolean)
        txtProjectID.Enabled = Not IsView
        txtProjectName.Enabled = Not IsView
        txtObjective.Enabled = Not IsView
        txtDescription.Enabled = Not IsView
        txtStartDate.Enabled = Not IsView
        txtEndDate.Enabled = Not IsView
        'txtAllocatedBudget.Enabled = Not IsView
        'ddlBudgetYear.Enabled = Not IsView
        'ddlBudgetGroup.Enabled = Not IsView
        'ddlSubBudget.Enabled = Not IsView
        'UCProjectBudget.SetToViewMode(IsView)

        DropDownListPlan.Enabled = Not IsView
        ddlCooperationFramework.Enabled = Not IsView
        ddlCooperationType.Enabled = Not IsView
        ddlOECDAidType.Enabled = Not IsView
        txtLocation.Enabled = Not IsView

        ddlFundingAgency.Enabled = Not IsView
        ddlExecutingAgency.Enabled = Not IsView
        UCImplementingAgency.SetToViewMode(IsView)
        UCCoFunding.SetToViewMode(IsView)

        ctlSelectAssistant.Visible = Not IsView
        txtAssistant.Visible = IsView
        txtAssistant.Enabled = Not IsView

        UCContact.SetToViewMode(IsView)

        txtNote.Enabled = Not IsView
        ddlTransferto.Enabled = Not IsView

        ''แยก Tab
        ''UCActivityTreeList1.SetViewMode = IsView
        ''UCActivityTabProject1.SetControlToViewMode(IsView)

        UCFileUploadList1.SetToViewMode(IsView)

        btnSave.Visible = Not IsView
        'btnAddActivity.Visible = Not IsView
    End Sub

    Private Sub ClearForm()
        '##tabProjectDetail##
        EditProjectID = 0
        txtProjectID.Text = ""
        If CInt(Request.QueryString("type")) = 0 Then
            lblTitle_Project.Text = "<span style='color: red;'>*</span>" + " Project Name<br>(ชื่อโครงการ)"
        ElseIf CInt(Request.QueryString("type")) = 1 Then
            lblTitle_Project.Text = "<span style='color: red;'>*</span>" + " Non Project Name<br>(ชื่อโครงการ)"
        End If
        txtProjectName.Text = ""
        txtObjective.Text = ""
        txtDescription.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        'txtAllocatedBudget.Text = ""
        'BL.Bind_DDL_Year(ddlBudgetYear)
        'BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)
        'ddlBudgetGroup_SelectedIndexChanged(Nothing, Nothing)
        BL.Bind_DDL_CoperationFrameWork(ddlCooperationFramework)
        BL.Bind_DDL_Coperationtype(ddlCooperationType)
        BL.Bind_DDL_OECD(ddlOECDAidType)
        txtLocation.Text = ""

        BL.Bind_DDL_Organize(ddlFundingAgency)
        ddlFundingAgency_SelectedIndexChanged(Nothing, Nothing)

        Dim dtImplementingAgency As New DataTable
        With dtImplementingAgency
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("organize_id")
        End With
        UCImplementingAgency.ImplementingAgencyDT = dtImplementingAgency

        Dim dtCoFunding As New DataTable
        With dtCoFunding
            .Columns.Add("id")
            .Columns.Add("country_id")
            .Columns.Add("amount")
        End With

        UCCoFunding.CoFundingDT = dtCoFunding

        SetAssistant()
        ctlSelectAssistant.Visible = True
        txtAssistant.Visible = False

        UCContact.ClearForm()
        txtNote.Text = ""
        BL.Bind_DDL_Person(ddlTransferto, Person_Type.Officer)

        '#TabRecipient
        SetProjectRecipient()

    End Sub

    Private Sub RestoreJQueryUI()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RestoreJQueryUI", "restoreJQueryUI();", True)
    End Sub

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub SetAssistant()
        Dim dt As New DataTable
        dt = BL.GetPersonList(Person_Type.Officer)

        ctlSelectAssistant.DataValueField = "id"
        ctlSelectAssistant.DataTextField = "name_th"

        ctlSelectAssistant.DataSource = dt
        ctlSelectAssistant.DataBind()

    End Sub

    Private Sub SetProjectFile()
        UCFileUploadList1.ClearTempFolder(UserName)

        Dim dt As DataTable = BL.GetProjectFile(EditProjectID)
        If dt.Rows.Count > 0 Then
            UCFileUploadList1.SetFileList(dt)
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If DropDownListPlan.SelectedValue = "" Then
            alertmsg("กรุณาระบุแผน")
            ret = False
        End If
        If txtProjectName.Text.Trim() = "" Then
            alertmsg("กรุณาระบุชื่อโครงการ")
            ret = False
        End If
        If txtStartDate.Text = "" Then
            alertmsg("กรุณากรอกวันเริ่มต้นโครงการ")
            ret = False
        End If

        If txtEndDate.Text = "" Then
            alertmsg("กรุณากรอกวันสิ้นสุดโครงการ")
            ret = False
        End If

        If ddlCooperationFramework.SelectedValue = "" Then
            alertmsg("กรุณาระบุกรอบความร่วมมือ")
            ret = False
        End If

        Try
            Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
            Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
        Catch ex As Exception
            alertmsg("กรุณาตรวจสอบวันที่")
            ret = False
        End Try

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("แผนของวันเริ่มต้นต้องน้อยกว่าวันสิ้นสุดโครงการ")
            ret = False
        End If

        Dim DTImplementingAgency As New DataTable
        DTImplementingAgency = UCImplementingAgency.ImplementingAgencyDT
        For i As Integer = 0 To DTImplementingAgency.Rows.Count - 1
            Dim id As String = DTImplementingAgency.Rows(i)("id").ToString()
            Dim country_id As String = DTImplementingAgency.Rows(i)("country_id").ToString()
            Dim organize_id As String = DTImplementingAgency.Rows(i)("organize_id").ToString()

            Dim temdr As DataRow() = DTImplementingAgency.Select("country_id='" & country_id & "' and organize_id='" & organize_id & "' and id <> '" & id & "'")
            If temdr.Length > 0 Then
                alertmsg("หน่วยงานดำเนินการซ้ำซ้อนกัน")
                ret = False
                Exit For
            End If
        Next


        Dim DTCoFunding As New DataTable
        DTCoFunding = UCCoFunding.CoFundingDT
        For i As Integer = 0 To DTCoFunding.Rows.Count - 1
            Dim id As String = DTCoFunding.Rows(i)("id").ToString()
            Dim country_id As String = DTCoFunding.Rows(i)("country_id").ToString()
            Dim amount As String = DTCoFunding.Rows(i)("amount").ToString()
            If amount = "" Then
                alertmsg("จำนวนผู้ร่วมให้เงินทุนต้องเป็นตัวเลข")
                ret = False
                Exit For
            End If

            Dim temdr As DataRow() = DTCoFunding.Select("country_id='" & country_id & "'  and id <> '" & id & "'")
            If temdr.Length > 0 Then
                alertmsg("ผู้ร่วมให้เงินทุนซ้ำซ้อนกัน")
                ret = False
                Exit For
            End If
        Next


        Return ret
    End Function

    Private Sub SetProjectRecipient()
        Dim dt As New DataTable
        dt = BL.GetProjectRecipincePerson(EditProjectID)

        ''แยก Tab
        ''rptRecipientList.DataSource = dt
        ''rptRecipientList.DataBind()

    End Sub

    Private Function GetAssistant() As String
        Dim strAssistant As String = ""

        For Each Item As ListItem In ctlSelectAssistant.Items
            If Item.Selected Then
                strAssistant += Item.Value & ","
            End If
        Next
        If strAssistant.Length > 0 Then
            Return strAssistant.Substring(0, strAssistant.Length - 1)
        Else
            Return ""
        End If
    End Function


#End Region

#Region "Tab"
    Protected Enum Tab
        Unknown = 0
        ProjectDetail = 1
        Activity = 2
        Recipient = 3
        ActActDetail = 4
        ActExpense = 5
        ActReceipent = 6
        ActPeriod = 7
    End Enum

    Protected Property CurrentTab As Tab
        Get
            Select Case True
                Case tabProjectDetail.Visible
                    Return Tab.ProjectDetail
                    'Case tabActivity.Visible
                    '    Return Tab.Activity
                    'Case TabRecipient.Visible
                    '    Return Tab.Recipient
                Case Else
                    Return Tab.Unknown
            End Select
        End Get
        Set(value As Tab)
            tabProjectDetail.Visible = False

            ''แยก Tab
            ''tabActivity.Visible = False
            ''TabRecipient.Visible = False
            'btnAddActivity.Visible = False


            liTabProjectDetail.Attributes("class") = ""
            liTabActivity.Attributes("class") = ""

            Select Case value
                Case Tab.ProjectDetail
                    tabProjectDetail.Visible = True
                    liTabProjectDetail.Attributes("class") = "active"
                    'btnAddActivity
                    ''แยก Tab
                    ''Case Tab.Activity
                    ''    tabActivity.Visible = True
                    ''    liTabActivity.Attributes("class") = "active"
                    ''    btnAddActivity.Visible = True
                    ''Case Tab.Recipient
                    ''    TabRecipient.Visible = True
                    ''    liTabRecipient.Attributes("class") = "active"
                Case Else
            End Select
        End Set
    End Property

    'Private Sub ChangeTab(sender As Object, e As System.EventArgs) Handles btnTabProjectDetail.Click, btnTabActivity.Click
    '    Select Case True
    '        Case Equals(sender, btnTabProjectDetail)
    '            CurrentTab = Tab.ProjectDetail
    '        Case Equals(sender, btnTabActivity)
    '            CurrentTab = Tab.Activity
    '            ''Case Equals(sender, btnTabRecipient)
    '            ''    CurrentTab = Tab.Recipient

    '            ' Update Recipient 
    '            SetProjectRecipient()
    '        Case Else
    '    End Select
    'End Sub

    '=====================


    ''แยก Tab
    ''Private Sub btnAddActivity_Click(sender As Object, e As EventArgs) Handles btnAddActivity.Click
    ''    UCActivityTabProject1.AddActivity(0)
    ''End Sub

    ''Private Sub UCActivityTreeList1_AddChildNode(ParentID As Long) Handles UCActivityTreeList1.AddChildNode
    ''    UCActivityTabProject1.AddActivity(ParentID)
    ''End Sub

    ''Private Sub UCActivityTreeList1_EditChildNode(ActivityID As Long) Handles UCActivityTreeList1.EditChildNode
    ''    UCActivityTabProject1.EditActivity(ActivityID)
    ''End Sub

    ''Private Sub UCActivityTreeList1_ViewChildNode(ActivityID As Long) Handles UCActivityTreeList1.ViewChildNode
    ''    UCActivityTabProject1.ViewActivity(ActivityID)
    ''End Sub

    ''Private Sub UCActivityTabProject1_SaveActivityComplete() Handles UCActivityTabProject1.SaveActivityComplete
    ''    UCActivityTreeList1.GenerateActivityList(EditProjectID, mode)
    ''End Sub




#End Region

#Region " Print Button"

    Function GetParameter(Reportformat As String) As String
        Dim para As String = "&ReportName=rptProjectDetail"
        para += "&ReportFormat=" & Reportformat
        para += "&ProjectID=" & EditProjectID

        If ProjectType = Constants.Project_Type.Project Then
            para += "&ProjectTypeName=Project"
        End If
        If ProjectType = Constants.Project_Type.NonProject Then
            para += "&ProjectTypeName=Non Project"
        End If

        Return para
    End Function

    'Private Sub aPDF_ServerClick(sender As Object, e As EventArgs) Handles aPDF.ServerClick
    '    myframe.Attributes.Add("src", "about: blank")

    '    Dim para As String = GetParameter("PDF")
    '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
    'End Sub

    'Private Sub aEXCEL_ServerClick(sender As Object, e As EventArgs) Handles aEXCEL.ServerClick
    '    Dim para As String = GetParameter("EXCEL")
    '    'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), "window.open('" + "reports/frmPreviewReport.aspx" + para + "', '_blank', 'height=650,left=600,location=no,menubar=no,toolbar=no,status=yes,resizable=yes,scrollbars=yes', true);", True)
    '    myframe.Attributes.Add("src", "reports/frmPreviewReport.aspx" + para)
    'End Sub

    Private Sub ddlFundingAgency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFundingAgency.SelectedIndexChanged
        BL.Bind_DDL_ChildsOrganize(ddlExecutingAgency, ddlFundingAgency.SelectedValue)
    End Sub

#End Region



#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProjectDetail.aspx?Mode=PDF" + para + "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim para As String = GetParameter("PDF")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptProjectDetail.aspx?Mode=EXCEL" + para + "');", True)
    End Sub

#End Region


#Region "Navigator"


    Private Sub btnTabProjectDetail_Click(sender As Object, e As EventArgs) Handles btnTabProjectDetail.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Info.aspx?" & Param & "';", True)

    End Sub

    Private Sub btnTabActivity_Click(sender As Object, e As EventArgs) Handles btnTabActivity.Click
        Dim Param As String = "id=" + EditProjectID.ToString() + "&mode=" + mode + "&type=" + ProjectType.ToString()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='frmProject_Detail_Activity.aspx?" & Param & "';", True)
    End Sub

#End Region


End Class
