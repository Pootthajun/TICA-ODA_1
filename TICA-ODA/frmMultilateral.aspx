﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmMultilateral.aspx.vb" Inherits="frmMultilateral" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Multilateral Type | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Multilateral (พหุภาคี)
            </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li class="active">Multilateral</li>
            </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto">


                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>


                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                             <i class="fa fa-plus"></i>Add Multilateral
                                            </asp:LinkButton>
                                        <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                    </p>

                                     <asp:Panel ID="pnlAdSearch" runat="server">
                                            <br/>
                                            <div class="col-sm-1">
                                            </div>

                                            <table>
                                                <tr>
                                                    <td>Multilateral (พหุภาคี)</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหา Multilateral"  CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr> 
                                                <tr><td width="200px" height="50">Status (สถานะ) :</td>
                                                    <td><asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                        <asp:ListItem Value="Y">Activate</asp:ListItem>
                                                        <asp:ListItem Value="N">Disabled</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td><td width="50px"></td>
                                                    <td> <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"> ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                 </td> 
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ</h4>
                                        </div>
                                    </div>
                                    <%--         <div class="card-header" style="padding-left:30px">
                                    <h4 class="text-primary">
                                        พบทั้งหมด :
                                        <asp:Label ID="lblTotalList" runat="server"></asp:Label>
                                        รายการ
                                        </h4>
                                    </div>--%>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="center">Multilateral</th>
                                                    <th class="center">Description (รายละเอียด)</th>
                                                    <th style="width: 11%;">Status (สถานะ)</th>
                                                    <th class="tools" style="width: 120px;" id="HeadEdit" runat="server" >Tool (เครื่องมือ)</th>
                                                     <th id="HeadColDelete" runat="server" class="tools" visible="false">Delete (ลบ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="Multilateral">
                                                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lblNames" runat="server"></asp:Label></td>
                                                            <td data-title="Description" id="td" runat="server">
                                                                <asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social">
                             <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                             <i class="fa fa-times"></i>Disabled
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td data-title="Edit" id="ColEdit" runat="server">
                                                               <center>
                                                            <div class="btn-group">
                                                                
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fa fa-navicon text-green"></i>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <%--<li id="liview" runat="server"><a href="javascript:;" onclick='btnViewClick(<%#Eval("ID") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>--%>
                                                                    <li id="liedit" runat="server">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="Edit">
                                    <i class="fa fa-pencil text-blue"></i> แก้ไข
                                                                        </asp:LinkButton></li>
                                                                    <li class="divider"></li>
                                                                    <li id="lidelete" runat="server">
                                    <asp:LinkButton ID="LinkButton2" runat="server"  OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")' 
                                    CommandArgument='<%# Eval("id") %>' CommandName="Delete">
                                    <i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton></li>
                                                                </ul>
                                                            </div></center>

                                                            
                                                            </td>
                                                             <td id="ColDelete" runat="server" Visible="false" >
                                                                
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                                </div>


                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>

    <script src="plugins/fastclick/fastclick.min.js"></script>
</asp:Content>

