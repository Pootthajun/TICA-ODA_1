﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmEditCooperationFramework
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditCooFrameworklID As Long
        Get
            Try
                Return ViewState("CooFrameworklID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("CooFrameworklID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aCooperationFramework")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            txtName.Focus()

            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditCooFrameworklID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditCooFrameworklID = 0
                End Try

            End If
            DisplayEditData()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtName.Enabled = False
            txtDetail.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub

    Protected Sub DisplayEditData()
        ClearEditForm()

        If EditCooFrameworklID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_CooperationFramework(EditCooFrameworklID, "")
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0)("fwork_name").ToString()
                txtDetail.Text = dt.Rows(0)("Detail").ToString()
                TxtDetail_th.Text = dt.Rows(0)("Detail_th").ToString()
                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If

    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbCoperationframeworkLinqDB
                Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByFWORK_NAME(txtName.Text.Trim, EditCooFrameworklID, Nothing)
                If chkDupName = True Then
                    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                    Exit Sub
                End If

                lnqLoc.ChkDataByPK(EditCooFrameworklID, trans.Trans)

                With lnqLoc
                    .FWORK_NAME = txtName.Text.Replace("'", "''")
                    .DETAIL = txtDetail.Text.Replace("'", "''")
                    .DETAIL_TH = TxtDetail_th.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmCooperationFramework.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    Alert(ret.ErrorMessage.Replace("'", """"))
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                Alert(ex.Message.Replace("'", """"))
            End Try
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtName.Text = "" Then
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
            ret = False
        End If

        Return ret
    End Function

    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmCooperationFramework.aspx")
    End Sub

    Public Sub ClearEditForm()
        txtName.Text = ""
        TxtDetail_th.Text = ""
        txtDetail.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class