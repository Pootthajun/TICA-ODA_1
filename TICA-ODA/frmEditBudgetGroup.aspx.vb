﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Partial Class frmEditBudgetGroup
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditBudgetGroupID As Long
        Get
            Try
                Return ViewState("BudgetGroupID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("BudgetGroupID") = value
        End Set
    End Property

    Private Sub frmEditGroupBudget_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuAllocatedBudget")
        li.Attributes.Add("class", "active")

        Dim li2 As HtmlGenericControl = Me.Page.Master.FindControl("mnuMasterBudget")
        li2.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aGroupBudget")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditBudgetGroupID = CInt(Request.QueryString("id"))
                Catch ex As Exception
                    EditBudgetGroupID = 0
                End Try

            End If

            GetGroupBudgetInfoForEdit()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtNames.Enabled = False
            chkActive.Enabled = False
            btnSave.Visible = False
        End If
    End Sub

    Sub GetGroupBudgetInfoForEdit()
        ClearEditForm()

        If EditBudgetGroupID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_GroupBudget(EditBudgetGroupID, "", "")
            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("group_name").ToString()
                Dim _active_status As String = dt.Rows(0)("active_status").ToString()
                If _active_status = "Y" Then
                    chkActive.Checked = True
                Else
                    chkActive.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
        End If
    End Sub

    Private Sub ClearEditForm()
        txtNames.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbBudgetGroupLinqDB
                lnqLoc.ChkDataByPK(EditBudgetGroupID, trans.Trans)
                With lnqLoc
                    .GROUP_NAME = txtNames.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkActive.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmBudgetGroup.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.Message.Replace("'", """") & "');", True)
            End Try
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtNames.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุกลุ่มงบประมาณ');", True)
            ret = False
        End If


        Return ret
    End Function

    Private Sub btnCancle_Click(sender As Object, e As System.EventArgs) Handles btnCancle.Click
        Response.Redirect("frmBudgetGroup.aspx")
    End Sub
End Class
