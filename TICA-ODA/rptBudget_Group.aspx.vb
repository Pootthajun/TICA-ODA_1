﻿
Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptBudget_Group
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib


    Private Sub rptSummary_ByProject_In_Period_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_03_1")
            a.Attributes.Add("style", "color:#FF8000")
            BL.Bind_DDL_Year(ddlBudgetYear, True)
            BL.Bind_DDL_BudgetGroup(ddlBudgetGroup)

            divSearchDate_23.Visible = False
            divSearch_01.Visible = True
            divSearchProject_23.Visible = False
            BindList()
        End If

    End Sub

#Region "Old"



    'sql += "   -- รายงานการจัดสรรงงประมาณ  Project/Non Project  																		" + Environment.NewLine
    'sql += "   Select                                                                                                                   " + Environment.NewLine
    'sql += "     budget_year                                                                                                                " + Environment.NewLine
    'sql += "   --Group Budget -----																											" + Environment.NewLine
    'sql += "   	,budget_group_id																											" + Environment.NewLine
    'sql += "   	,group_name																													" + Environment.NewLine
    'sql += "   	,budget_sub_id																												" + Environment.NewLine
    'sql += "   	,sub_name																													" + Environment.NewLine
    'sql += "   --, Amount_Budget																											" + Environment.NewLine
    'sql += "   ,ISNULL(SUM(Amount_Budget),0.00) Amount_Budget																				" + Environment.NewLine
    'sql += "   ,  ISNULL(SUM(Pay_Amount_Plan),0.00) Pay_Amount_Plan , ISNULL(SUM(Pay_Amount_Actual),00) Pay_Amount_Actual					" + Environment.NewLine
    'sql += "   ,(ISNULL(SUM(Pay_Amount_Plan),0.00)-ISNULL(SUM(Pay_Amount_Actual),0.00))  Balance											" + Environment.NewLine
    'sql += "   FROM(                                                                                                                        " + Environment.NewLine
    'sql += "          Select  DISTINCT TB_Activity_Budget.budget_year                                                                       " + Environment.NewLine
    'sql += " 		,TB_Activity.id Activity_id				                                                                                " + Environment.NewLine
    'sql += "    		,TB_Activity.activity_name 						                                                                    " + Environment.NewLine
    'sql += "   		,(SELECT SUM(pay_Amount_Plan) pay_Amount_Plan FROM TB_Activity_Expense_Plan_Detail WHERE TB_Activity_Expense_Plan_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Plan	" + Environment.NewLine
    'sql += "   		,(Select SUM(pay_Amount_Actual) pay_Amount_Actual FROM TB_Activity_Expense_Actual_Detail WHERE TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Actual	" + Environment.NewLine
    'sql += "   		,TB_Activity_Budget.amount Amount_Budget																				" + Environment.NewLine
    'sql += "   --Group Budget -----																											" + Environment.NewLine
    'sql += "    		,TB_Budget_Sub.budget_group_id																						" + Environment.NewLine
    'sql += "    		,TB_Budget_Group.group_name																							" + Environment.NewLine
    'sql += "    		,TB_Activity_Budget.budget_sub_id																					" + Environment.NewLine
    'sql += "    		,TB_Budget_Sub.sub_name																								" + Environment.NewLine
    'sql += "    		From TB_Activity                                                                                                    " + Environment.NewLine
    'sql += "            Left Join TB_Activity_Budget On TB_Activity_Budget.activity_id=TB_Activity.id										" + Environment.NewLine
    'sql += "            Left Join TB_Activity_Expense_Header On TB_Activity_Expense_Header.activity_id=TB_Activity.id			            " + Environment.NewLine
    'sql += "            Left Join TB_Activity_Expense_Actual_Detail On TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id	" + Environment.NewLine
    'sql += "            Left Join TB_Project On TB_Project.id= TB_Activity.project_id														" + Environment.NewLine
    'sql += "            INNER Join TB_Budget_Sub On TB_Budget_Sub.id=TB_Activity_Budget.budget_sub_id										" + Environment.NewLine
    'sql += "            INNER Join TB_Budget_Group On TB_Budget_Group.id=TB_Budget_Sub.budget_group_id		   								" + Environment.NewLine
    'sql += "            WHERE  Project_Type In (0, 1) And ISNULL(TB_Activity.Is_Folder,0)=0	                                                " + Environment.NewLine

    'If ddlBudgetYear.SelectedIndex > 0 Then
    '    sql += " And budget_year = '" + ddlBudgetYear.SelectedValue.ToString + "'" + Environment.NewLine
    'End If
    'If ddlBudgetGroup.SelectedIndex > 0 Then
    '    sql += " And TB_Budget_Sub.budget_group_id = " + ddlBudgetGroup.SelectedValue + Environment.NewLine
    'End If

    'sql += "                                                                                                                                " + Environment.NewLine
    'sql += "   ) AS TB                                                                                                              " + Environment.NewLine
    'sql += "     Group BY budget_year																										" + Environment.NewLine
    'sql += "   		---Group Budget -----																									" + Environment.NewLine
    'sql += "   		,budget_group_id																										" + Environment.NewLine
    'sql += "   		,group_name																												" + Environment.NewLine
    'sql += "   		,budget_sub_id																											" + Environment.NewLine
    'sql += "   		,sub_name				 																								" + Environment.NewLine
    'sql += "   ORDER BY budget_year DESC, budget_group_id, budget_sub_id                                                                    " + Environment.NewLine

#End Region



    Private Sub BindList()
        pnlType_01.Visible = True
        pnlType_23.Visible = False
        Dim filter As String = ""
        Dim Title As String = " รายงานการจัดสรรงบประมาณ "
        Dim sql As String = ""

        sql += "  -- รายงานการจัดสรรงงประมาณ  Project/Non Project    " + Environment.NewLine
        sql += "  Select     " + Environment.NewLine
        sql += "       TB.budget_year   " + Environment.NewLine
        sql += "       , 'ปี ' + convert (varchar,TB.budget_year) + ' : ' + TB.group_name rpt_group   " + Environment.NewLine
        sql += "       --Group Budget -----   " + Environment.NewLine
        sql += "      ,TB.budget_group_id	   " + Environment.NewLine
        sql += "      ,TB.group_name			   " + Environment.NewLine
        sql += "      ,TB.budget_sub_id		   " + Environment.NewLine
        sql += "      ,TB.sub_name			   " + Environment.NewLine
        sql += "      ,ISNULL(_vw_Allocate_Budget.amount,0.00) 	Amount_Budget		   " + Environment.NewLine
        sql += "      ,ISNULL(SUM(TB.Pay_Amount_Plan),0.00) Pay_Amount_Plan , ISNULL(SUM(TB.Pay_Amount_Actual),00) Pay_Amount_Actual		   " + Environment.NewLine
        sql += "      ,(ISNULL(_vw_Allocate_Budget.amount,0.00)-ISNULL(SUM(TB.Pay_Amount_Plan),0.00))  Balance							   " + Environment.NewLine
        sql += "      FROM(   " + Environment.NewLine
        sql += "             Select  DISTINCT TB_Activity_Budget.budget_year     " + Environment.NewLine
        sql += "    		,TB_Activity.id Activity_id				           " + Environment.NewLine
        sql += "       		,TB_Activity.activity_name 						   " + Environment.NewLine
        sql += "      		,(Select SUM(pay_Amount_Plan) pay_Amount_Plan FROM TB_Activity_Expense_Plan_Detail WHERE TB_Activity_Expense_Plan_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Plan	   " + Environment.NewLine
        sql += "      		,(Select SUM(pay_Amount_Actual) pay_Amount_Actual FROM TB_Activity_Expense_Actual_Detail WHERE TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id ) pay_Amount_Actual	   " + Environment.NewLine
        sql += "   " + Environment.NewLine
        sql += "      --Group Budget -----						   " + Environment.NewLine
        sql += "       		,TB_Budget_Sub.budget_group_id				   " + Environment.NewLine
        sql += "       		,TB_Budget_Group.group_name					   " + Environment.NewLine
        sql += "       		,TB_Activity_Budget.budget_sub_id			   " + Environment.NewLine
        sql += "       		,TB_Budget_Sub.sub_name							   " + Environment.NewLine
        sql += "       		From TB_Activity                                " + Environment.NewLine
        sql += "               Left Join TB_Activity_Budget On TB_Activity_Budget.activity_id=TB_Activity.id	   " + Environment.NewLine
        sql += "               Left Join TB_Activity_Expense_Header On TB_Activity_Expense_Header.activity_id=TB_Activity.id			               " + Environment.NewLine
        sql += "               Left Join TB_Activity_Expense_Actual_Detail On TB_Activity_Expense_Actual_Detail.header_id = TB_Activity_Expense_Header.id	   " + Environment.NewLine
        sql += "               Left Join TB_Project On TB_Project.id= TB_Activity.project_id														   " + Environment.NewLine
        sql += "               INNER Join TB_Budget_Sub On TB_Budget_Sub.id=TB_Activity_Budget.budget_sub_id										   " + Environment.NewLine
        sql += "               INNER Join TB_Budget_Group On TB_Budget_Group.id=TB_Budget_Sub.budget_group_id		   								   " + Environment.NewLine
        sql += "               WHERE  Project_Type In (0,1) And ISNULL(TB_Activity.Is_Folder,0)=0	                                                   " + Environment.NewLine

        If ddlBudgetYear.SelectedIndex > 0 Then
            sql += " And budget_year = '" + ddlBudgetYear.SelectedValue.ToString + "'" + Environment.NewLine
        End If
        If ddlBudgetGroup.SelectedIndex > 0 Then
            sql += " And TB_Budget_Sub.budget_group_id = " + ddlBudgetGroup.SelectedValue + Environment.NewLine
        End If


        sql += "   " + Environment.NewLine
        sql += "      ) AS TB                           " + Environment.NewLine
        sql += "      Left Join _vw_Allocate_Budget On _vw_Allocate_Budget.budget_year=TB.budget_year And _vw_Allocate_Budget.budget_group_id=TB.budget_group_id And _vw_Allocate_Budget.budget_sub_id=TB.budget_sub_id        " + Environment.NewLine
        sql += "        Group BY    " + Environment.NewLine
        sql += "            TB.budget_year   " + Environment.NewLine
        sql += "      		---Group Budget -----					   " + Environment.NewLine
        sql += "      		,TB.budget_group_id					   " + Environment.NewLine
        sql += "      		,TB.group_name					   " + Environment.NewLine
        sql += "      		,TB.budget_sub_id				   " + Environment.NewLine
        sql += "      		,TB.sub_name	   " + Environment.NewLine
        sql += "   		,_vw_Allocate_Budget.amount			   " + Environment.NewLine
        sql += "      ORDER BY TB.budget_year DESC, TB.budget_group_id, TB.budget_sub_id     " + Environment.NewLine




        'ต้องแยก Loan / Contribuite   _vw_Project_Budget_Plan_Actual



        Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)


        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        Session("Search_Budget_Group_Title") = lblTotalRecord.Text


        If (DT.Rows.Count > 0) Then
            Dim DT_Fill As New DataTable
            DT.DefaultView.RowFilter = "Amount_Budget IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblAmount_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Amount_Budget)", "")).ToString("#,##0.00")
            End If

            DT.DefaultView.RowFilter = "Pay_Amount_Plan IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblAllocate_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            End If
            DT.DefaultView.RowFilter = "Pay_Amount_Actual IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblPay_Amount_Actual_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            End If
            DT.DefaultView.RowFilter = "Balance IS NOT NULL"
            DT_Fill = DT.DefaultView.ToTable
            If DT_Fill.Rows.Count > 0 Then
                lblBalance_Budget_SUM.Text = Convert.ToDecimal(DT_Fill.Compute("SUM(Balance)", "")).ToString("#,##0.00")
            End If

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()


        Session("Search_Budget_Group") = DT

        Pager.SesssionSourceName = "Search_Budget_Group"
        Pager.RenderLayout()
    End Sub

    Private Sub BindList_Type23()

        pnlType_01.Visible = False
        pnlType_23.Visible = True

        Dim filter As String = ""
        Dim Title As String = " รายงานการจัดสรรงบประมาณ "

        Dim SQL As String = ""
            SQL += "  SELECT Budget.project_id							 " & vbLf
            SQL += "        ,Budget.project_Code						 " & vbLf
            SQL += "        ,Budget.project_name						 " & vbLf
            SQL += "        ,Budget.project_type						 " & vbLf
            SQL += "        --กรอบคึวามร่วมมือ--								 " & vbLf
            SQL += "        ,TB_Project.cooperation_framework_id 		 " & vbLf
            SQL += "        ,TB_CoperationFramework.fwork_name 			 " & vbLf
            SQL += "        ,Commitment_budget							 " & vbLf
            SQL += "        ,Pay_Amount_Plan							 " & vbLf
            SQL += "        ,Pay_Amount_Actual							 " & vbLf
            SQL += "        ,Balance									 " & vbLf
            SQL += "    FROM _vw_Project_Budget_Plan_Actual Budget		 " & vbLf
            SQL += "    Left Join TB_Project On TB_Project.id= Budget.project_id  " & vbLf
            SQL += "    --กรอบคึวามร่วมมือ--											  " & vbLf
            SQL += "    Left Join TB_CoperationFramework On TB_CoperationFramework.id =TB_Project .cooperation_framework_id  " & vbLf
        SQL += "    where TB_Project.project_type  in (2,3) " & vbLf
        If txtSearch_Project.Text <> "" Then
            filter += "   AND ( Budget.project_Code LIKE '%" & txtSearch_Project.Text & "%'   " + Environment.NewLine
            filter += "   OR Budget.project_name LIKE '%" & txtSearch_Project.Text & "%' )   " + Environment.NewLine
        End If

        SQL += "    ORDER BY Budget.project_name  " & vbLf
            Dim DA As SqlDataAdapter = New SqlDataAdapter(sql, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= ddlReportType.SelectedItem.ToString & " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= ddlReportType.SelectedItem.ToString & " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

        Session("Search_rptBudget_Group23_Title") = lblTotalRecord.Text


        If (DT.Rows.Count > 0) Then
            lblSUM_commitment_budget.Text = Convert.ToDecimal(DT.Compute("SUM(commitment_budget)", "")).ToString("#,##0.00")
            lblSUM_Pay_Amount_Plan_Type_23.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")
            lblSUM_Pay_Amount_Actual_Type_23.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            lblSUM_Balance_Type_23.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        RptList_Type_23.DataSource = DT
        RptList_Type_23.DataBind()

        Session("Search_rptBudget_Group23") = DT

        Pager_Type23.SesssionSourceName = "Search_rptBudget_Group23"
        Pager_Type23.RenderLayout()
    End Sub



    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub
    Protected Sub Pager_Type23_PageChanging(Sender As PageNavigation) Handles Pager_Type23.PageChanging
        Pager_Type23.TheRepeater = RptList_Type_23
    End Sub

    Dim Lastsub_name As String = ""

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblsub_name As Label = DirectCast(e.Item.FindControl("lblsub_name"), Label)

        Dim lblAmount_Budget As Label = DirectCast(e.Item.FindControl("lblAmount_Budget"), Label)
        Dim lblAllocate_Budget As Label = DirectCast(e.Item.FindControl("lblAllocate_Budget"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim trgroup_name As HtmlTableRow = e.Item.FindControl("trgroup_name")
        Dim lblgroup_name As Label = e.Item.FindControl("lblgroup_name")

        '--------Lastbudget_year------------
        If Lastsub_name <> "ปี " + e.Item.DataItem("budget_year").ToString + " : " + e.Item.DataItem("group_name").ToString Then
            Lastsub_name = "ปี " + e.Item.DataItem("budget_year").ToString + " : " + e.Item.DataItem("group_name").ToString
            lblgroup_name.Text = Lastsub_name
            trgroup_name.Visible = True

        Else
            trgroup_name.Visible = False
        End If

        lblsub_name.Text = "" & e.Item.DataItem("sub_name").ToString

        If Convert.IsDBNull(e.Item.DataItem("Amount_Budget")) = False Then
            lblAmount_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Amount_Budget")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
            lblAllocate_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


        Dim btnDrillDown As Button = e.Item.FindControl("btnDrillDown")
        For i As Integer = 1 To 5
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & btnDrillDown.ClientID & "').click();"
        Next

        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)
        pnlProject.Visible = False
        lblsub_name.Attributes("budget_year") = e.Item.DataItem("budget_year")
        lblsub_name.Attributes("budget_sub_id") = e.Item.DataItem("budget_sub_id")

    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim pnlProject As Panel = DirectCast(e.Item.FindControl("pnlProject"), Panel)

        Dim lblsub_name As Label = DirectCast(e.Item.FindControl("lblsub_name"), Label)
        Dim rptProject As Repeater = DirectCast(e.Item.FindControl("rptProject"), Repeater)
        AddHandler rptProject.ItemDataBound, AddressOf rptProject_ItemDataBound

        Select Case e.CommandName
            Case "select"
                If pnlProject.Visible Then
                    pnlProject.Visible = False
                    Exit Sub
                End If

                Dim SQL As String = ""
                SQL += " SELECT * FROM _vw_Allocate_Budget_Project WHERE budget_year=" & lblsub_name.Attributes("budget_year")
                SQL += " AND budget_sub_id=" & lblsub_name.Attributes("budget_sub_id")
                SQL += " ORDER BY project_name "
                Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                rptProject.DataSource = DT
                rptProject.DataBind()
                pnlProject.Visible = True
        End Select


    End Sub

    Protected Sub rptProject_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblProject_Name As Label = e.Item.FindControl("lblProject_Name")

        Dim lblAllocate_Budget As Label = DirectCast(e.Item.FindControl("lblAllocate_Budget"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        lblProject_Name.Text = e.Item.ItemIndex + 1 & ". " & e.Item.DataItem("project_name").ToString()
        If Not IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) Then
            lblAllocate_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Balance")) Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If
        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        For i As Integer = 1 To 4
            Dim tdLinkProject As HtmlTableCell = e.Item.FindControl("tdLinkProject" & i)
            tdLinkProject.Style("cursor") = "pointer"
            tdLinkProject.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        End If


    End Sub


    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        If pnlType_01.Visible Then
            BindList()
        Else
            BindList_Type23()
        End If

    End Sub

    Private Sub ddlReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReportType.SelectedIndexChanged
        If ddlReportType.SelectedIndex = 0 Then
            divSearchDate_23.Visible = False
            divSearch_01.Visible = True
            divSearchProject_23.Visible = False
            BindList()
        Else
            divSearchDate_23.Visible = False
            divSearch_01.Visible = False
            divSearchProject_23.Visible = True
            BindList_Type23()

        End If


    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        If ddlReportType.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget_Group.aspx?Mode=PDF');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget_Group23.aspx?Mode=PDF');", True)
        End If
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        If ddlReportType.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget_Group.aspx?Mode=EXCEL');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptBudget_Group23.aspx?Mode=EXCEL');", True)
        End If
    End Sub

    Private Sub RptList_Type_23_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RptList_Type_23.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblProject_Name As Label = e.Item.FindControl("lblProject_Name")
        Dim lblcommitment_budget As Label = DirectCast(e.Item.FindControl("lblcommitment_budget"), Label)
        Dim lblAllocate_Budget As Label = DirectCast(e.Item.FindControl("lblAllocate_Budget"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)

        lblProject_Name.Text = e.Item.ItemIndex + 1 & ". " & e.Item.DataItem("project_name").ToString()
        If Not IsDBNull(e.Item.DataItem("commitment_budget")) Then
            lblcommitment_budget.Text = Convert.ToDecimal(e.Item.DataItem("commitment_budget")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) Then
            lblAllocate_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If
        If Not IsDBNull(e.Item.DataItem("Balance")) Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If
        '============Click To Project===============
        Dim lnkSelect As HtmlAnchor = e.Item.FindControl("lnkSelect")

        For i As Integer = 1 To 5
            Dim tdLinkProject As HtmlTableCell = e.Item.FindControl("tdLinkProject" & i)
            tdLinkProject.Style("cursor") = "pointer"
            tdLinkProject.Attributes("onClick") = "document.getElementById('" & lnkSelect.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelect.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelect.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelect.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        End If


    End Sub

#End Region



End Class


