﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmEditMultilateral.aspx.vb" Inherits="frmEditMultilateral"  EnableEventValidation="false" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Multilateral Type | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> Multilateral (พหุภาคี) </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
                <li><a href="frmMultilateral.aspx">Multilateral</a></li>
                <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> Multilateral</li>                    
            </ol>

        </section>
        <!-- Main content -->
        <section class="content">
            <br />
            <!-- START CUSTOM TABS -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border bg-blue-gradient">
                            <h4 class="box-title">
                                <asp:Label ID="lblEditMode2" runat="server" Text="Label"></asp:Label>
                                Multilateral
                            </h4>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="Sector" class="col-sm-2 control-label">Multilateral <br/>
                                        (พหุภาคี): <span style="color: red">*</span></label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtNames" runat="server" CssClass="form-control" placeholder="" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>

                                <br />
                                <br />
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label for="inputname" class="col-sm-2 control-label">
                                        Description<br />
                                        (รายละเอียด) :</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" placeholder="" TextMode="MultiLine" Height="100px" MaxLength="255"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <!-- /.box -->
                                <div class="clearfix"></div>
                                <br />

                                <div class="form-group">
                                    <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                                    <div class="col-sm-9">
                                        <label>
                                            <asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" Checked="true" /></label>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="col-sm-9"></div>
                                <div class="col-lg-8"></div>
                                <div class="col-lg-2">

                                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success">
                                                <i class="fa fa-save"></i>Save</asp:LinkButton>
                                </div>
                                <div class="col-lg-2">
                                    <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google">
                                                <i class="fa fa-reply"></i> Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->
            <!-- END CUSTOM TABS -->


        </section>
        <!-- /.content -->
    </div>
</asp:Content>

