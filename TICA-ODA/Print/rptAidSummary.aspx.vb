﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Print_rptCLMV
    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim DT_rpt As DataTable = Session("Search_Summary_Aid")
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../reports/rptAidSummary.rpt"))



        '     //---------------- Format DT ---------------
        Dim DT As DataTable = DT_rpt.Copy()
        DT.Columns.Add("New_Country", Type.GetType("System.Int32"))
        DT.Columns.Add("New_Sector", Type.GetType("System.Int32"))

        Dim LastCountry As String = ""
        Dim LastSector As String = ""

        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i)("country_name_th").ToString() <> LastCountry Then
                DT.Rows(i)("New_Country") = 1
                LastCountry = DT.Rows(i)("country_name_th").ToString()
                LastSector = ""
            End If

            If DT.Rows(i)("perposecat_name").ToString() <> LastCountry Then
                DT.Rows(i)("New_Sector") = 1
                LastSector = DT.Rows(i)("perposecat_name").ToString()
            End If


        Next

        cc.SetDataSource(DT)

        cc.SetParameterValue("Title", Session("Search_Summary_Aid_Title"))
        cc.SetParameterValue("CurDate", "วันที่ " & GL.ReportThaiDateTime(Now))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=รายงานการให้ความช่วยเหลือกับต่างประเทศ_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=รายงานการให้ความช่วยเหลือกับต่างประเทศ_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub

End Class
