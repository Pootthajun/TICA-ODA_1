﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Partial Class Print_rptBudget_Group_CooFramework23
    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim DT As DataTable = Session("Search_Budget_Group_CooFramework")
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../reports/rptBudget_Group_CooFramework23.rpt"))
        cc.SetDataSource(DT)
        cc.SetParameterValue("Title", Session("Search_Budget_Group_CooFramework_Title"))
        cc.SetParameterValue("CurDate", "วันที่ " & GL.ReportThaiDateTime(Now))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=รายงานการจัดสรรงบประมาณแบ่งตามกรอบความร่วมมือ_LoanContribuition_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=รายงานการจัดสรรงบประมาณแบ่งตามกรอบความร่วมมือ_LoanContribuition_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub

End Class
