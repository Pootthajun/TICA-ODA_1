﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class rptAdvance_Expense_Detail
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New ODAENG
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim Sql As String = " "
        Sql &= "    Select * FROM vw_Advance_Expense_Detail WHERE  Advance_ID=" & CInt(Request.QueryString("Advance_ID"))

        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../reports/rptAdvance_Expense_Detail.rpt"))
        cc.SetDataSource(DT)
        If (DT.Rows.Count > 0) Then
            Dim Advance_ As Object = 0
            Dim Expense_ As Object = 0
            Dim Cash_Back As Object = 0

            Advance_ = Convert.ToDecimal(DT.Compute("SUM(Amount)", "Group_Name='เงินยืมทดรอง'")).ToString("#,##0.00")
            DT.DefaultView.RowFilter = "Group_Name='ค่าใช้จ่าย'"
            If DT.DefaultView.Count > 0 Then
                Expense_ = Convert.ToDecimal(DT.Compute("SUM(Amount)", "Group_Name='ค่าใช้จ่าย'")).ToString("#,##0.00")
            End If

            Cash_Back = Convert.ToDecimal(DT.Compute("SUM(Amount)", "Group_Name='เงินคืน'")).ToString("#,##0.00")

            cc.SetParameterValue("Balance", (Convert.ToDecimal(Advance_)) - (Expense_ + Convert.ToDecimal(Cash_Back)))
        End If



        Sql = " "
        Sql &= "   Select  vw_Activity.project_name ,vw_Activity.activity_name  , TB_Activity_Advance.* ,TB_NAME,name_th_OUTB_OU_Country,name_th_OUTB_OU_Organize,name_th_OUTB_OU_Person " & vbLf
        Sql &= "   ,TB_Budget_Group.group_name " & vbLf
        Sql &= "   from TB_Activity_Advance  " & vbLf
        Sql &= "   Left Join vw_Activity ON vw_Activity.activity_id=TB_Activity_Advance.activity_id " & vbLf
        Sql &= "   LEFT JOIN vw_Overview_OU ON vw_Overview_OU.node_id=TB_Activity_Advance.Borrower_Node_ID " & vbLf
        Sql &= "   LEFT jOIN TB_Activity_Budget ON TB_Activity_Budget.activity_id=vw_Activity.activity_id " & vbLf
        Sql &= "   LEFT JOIN TB_Budget_Group ON TB_Budget_Group.id=TB_Activity_Budget.budget_group_id " & vbLf

        Sql &= "   WHERE  Advance_ID =" & CInt(Request.QueryString("Advance_ID"))


        DA = New SqlDataAdapter(Sql, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            cc.SetParameterValue("Project_Name", DT.Rows(0).Item("project_name").ToString())
            cc.SetParameterValue("Activity_Name", DT.Rows(0).Item("activity_name").ToString())
            cc.SetParameterValue("Budget_Group", DT.Rows(0).Item("group_name").ToString())


            Dim Borrower_Name As String = ""
            Select Case DT.Rows(0).Item("TB_NAME").ToString()
                Case "TB_OU_Country"
                    Borrower_Name = " ยืมโดยประเทศ : " & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()
                    cc.SetParameterValue("Title", "รายงานใบสำคัญลูกหนี้ทดรองจ่าย")
                Case "TB_OU_Organize"
                    Borrower_Name = " ยืมโดยหน่วยงาน : " & DT.Rows(0).Item("name_th_OUTB_OU_Organize").ToString()
                    Borrower_Name &= " ประเทศ" & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()
                    cc.SetParameterValue("Title", "รายงานใบสำคัญลูกหนี้เงินจ่ายล่วงหน้า")
                Case "TB_OU_Person"
                    Borrower_Name = " ยืมโดย : " & DT.Rows(0).Item("name_th_OUTB_OU_Person").ToString()
                    Borrower_Name &= " สังกัดหน่วยงาน : " & DT.Rows(0).Item("name_th_OUTB_OU_Organize").ToString() & "   ประเทศ" & DT.Rows(0).Item("name_th_OUTB_OU_Country").ToString()
                    cc.SetParameterValue("Title", "รายงานใบสำคัญลูกหนี้ทดรองจ่าย")
            End Select

            cc.SetParameterValue("Borrower_Name", Borrower_Name)
        End If

        cc.SetParameterValue("CurDate", "วันที่ " & GL.ReportThaiDateTime(Now))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=รายงานลูกหนี้ทดรองจ่าย_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=รายงานลูกหนี้ทดรองจ่าย_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte_rpt(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub


End Class
