﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmEditRegionOECD
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditRegionOECDID As Long
        Get
            Try
                Return ViewState("RegionOECDID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("RegionOECDID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aRegionOECD")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditRegionOECDID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditRegionOECDID = 0
                End Try

            End If
            DisplayEditData()
            Authorize()
        End If
    End Sub
    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtName.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

        lblName.Text = drv("region_name").ToString()
    End Sub

    Protected Sub DisplayEditData()

        ClearForm()

        If EditRegionOECDID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_RegionOECD(EditRegionOECDID, "")
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0)("region_name").ToString()
                TxtNameTh.Text = dt.Rows(0)("region_name_th").ToString()

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If

    End Sub

    Sub ClearForm()
        txtName.Text = ""
        TxtNameTh.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub


    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbRegionLinqDB
                Dim chkDup As Boolean = lnqLoc.ChkDuplicateByREGION_NAME(txtName.Text.Trim, EditRegionOECDID, Nothing)
                If chkDup = True Then
                    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                    Exit Sub
                End If

                lnqLoc.ChkDataByPK(EditRegionOECDID, trans.Trans)

                With lnqLoc
                    .REGION_NAME = txtName.Text.Replace("'", "''")
                    .REGION_NAME_TH = TxtNameTh.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmRegionOECD.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    Alert(ret.ErrorMessage.Replace("'", """"))
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                Alert(ex.Message.Replace("'", """"))
            End Try
        End If

    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtName.Text = "" Then
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
            ret = False
        End If

        Return ret
    End Function

    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmRegionOECD.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class