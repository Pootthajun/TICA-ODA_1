﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmEditUserInformationAgency
    Inherits System.Web.UI.Page

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then
            txtGOVTH.Focus()
            If Session("SESSION_ID_GOVCONTACT") <> "" Then
                BindData()
            End If

        End If
    End Sub
    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode"
        sql += ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS"
        sql += " FROM tb_contactgoverment WHERE id=" & Session("SESSION_ID_GOVCONTACT")
        Dim trans As New TransactionDB
        Dim lnq As New TbContactgovermentLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sql, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("id").ToString() <> "" Then
                    txtGOVTH.Text = dt(i)("govnamethai").ToString()
                    txtGOVEN.Text = dt(i)("govnameeng").ToString()
                    txtDivision.Text = dt(i)("kong").ToString()
                    txtMinistry.Text = dt(i)("ministry").ToString()

                    txtBuilding.Text = dt(i)("govnumber").ToString()
                    txtRoad.Text = dt(i)("govroad").ToString()

                    txtTambon.Text = dt(i)("tambol").ToString()
                    txtDistrict.Text = dt(i)("ampher").ToString()

                    txtProvince.Text = dt(i)("province").ToString()
                    txtZipCode.Text = dt(i)("postcode").ToString()

                    txtTelephone.Text = dt(i)("telephone").ToString()
                    txtFax.Text = dt(i)("fax").ToString()

                    txtWebSite.Text = dt(i)("website").ToString()
                    txtEmail.Text = dt(i)("email").ToString()
                    txtNote.Text = dt(i)("detail").ToString()

                    If dt.Rows(i)("ACTIVE_STATUS").ToString() = "Y" Then
                        chkActiveStatus.Checked = True
                    Else
                        chkActiveStatus.Checked = False
                    End If
                End If
            Next

        Else
            trans.RollbackTransaction()
            Dim _err = trans.ErrorMessage()
        End If
        lnq = Nothing
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If txtGOVTH.Text <> "" Then

            Dim tran As New TransactionDB
            Dim lnq As New TbContactgovermentLinqDB
            Dim ret As ExecuteDataInfo
            Dim strID As String = 1 'Session("SESSION_ID")
            Dim chkPK As Boolean = lnq.ChkDataByPK(strID, tran.Trans)
            If chkPK = True Then
                lnq.GOVNAMETHAI = (txtGOVTH.Text.Trim)
                lnq.GOVNAMEENG = (txtGOVEN.Text.Trim)
                lnq.KONG = (txtDivision.Text.Trim)
                lnq.MINISTRY = (txtMinistry.Text.Trim)

                lnq.GOVNUMBER = (txtBuilding.Text.Trim)
                lnq.GOVROAD = (txtRoad.Text.Trim)

                lnq.TAMBOL = (txtTambon.Text.Trim)
                lnq.AMPHER = (txtDistrict.Text.Trim)

                lnq.PROVINCE = (txtProvince.Text.Trim)
                lnq.POSTCODE = (txtZipCode.Text.Trim)

                lnq.TELEPHONE = (txtTelephone.Text.Trim)
                lnq.FAX = (txtFax.Text.Trim)

                lnq.WEBSITE = (txtWebSite.Text.Trim)
                lnq.EMAIL = (txtEmail.Text.Trim)
                lnq.DETAIL = (txtNote.Text.Trim)

                If chkActiveStatus.Checked = True Then
                    lnq.ACTIVE_STATUS = "Y"
                Else
                    lnq.ACTIVE_STATUS = "N"
                End If


                ret = lnq.UpdateData(UserName, tran.Trans)
            Else
                lnq.GOVNAMETHAI = (txtGOVTH.Text.Trim)
                lnq.GOVNAMEENG = (txtGOVEN.Text.Trim)
                lnq.KONG = (txtDivision.Text.Trim)
                lnq.MINISTRY = (txtMinistry.Text.Trim)

                lnq.GOVNUMBER = (txtBuilding.Text.Trim)
                lnq.GOVROAD = (txtRoad.Text.Trim)

                lnq.TAMBOL = (txtTambon.Text.Trim)
                lnq.AMPHER = (txtDistrict.Text.Trim)

                lnq.PROVINCE = (txtProvince.Text.Trim)
                lnq.POSTCODE = (txtZipCode.Text.Trim)

                lnq.TELEPHONE = (txtTelephone.Text.Trim)
                lnq.FAX = (txtFax.Text.Trim)

                lnq.WEBSITE = (txtWebSite.Text.Trim)
                lnq.EMAIL = (txtEmail.Text.Trim)
                lnq.DETAIL = (txtNote.Text.Trim)

                If chkActiveStatus.Checked = True Then
                    lnq.ACTIVE_STATUS = "Y"
                Else
                    lnq.ACTIVE_STATUS = "N"
                End If
                ret = lnq.InsertData(UserName, tran.Trans)

            End If

            If ret.IsSuccess = True Then
                tran.CommitTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmAgencyInformation.aspx';", True)

            Else
                tran.RollbackTransaction()
                Alert("ไม่สามารถบันทึกข้อมูลได้")
            End If
            lnq = Nothing
            Session.Remove("SESSION_ID_GOVCONTACT")
        Else
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
        End If
    End Sub
    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Session.Remove("SESSION_ID_GOVCONTACT")
        Response.Redirect("frmAgencyInformation.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class