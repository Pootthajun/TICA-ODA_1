﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmOverduePlan.aspx.vb" Inherits="frmOverduePlan" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Plan Overdue | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Plan Overdue <span style="font-size: 20px">(แผนค้างชำระ)</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="frmOverduePlan.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
                <li class="active">Expense</li>
            </ol>
        </section>
        <br />
        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <p>
                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <div class="col-sm-1">
                                            </div>
                                            <table>
                                                 <tr>
                                                    <td width="200px" height="50">Project Time : </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>

                                                    </td>
                                                    <td width="50px" style="text-align: center">- </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td width="200px" height="50">Project name (โครงการ):</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtSearchProname" runat="server" placeholder="ชื่อโครงการ" CssClass="form-control"></asp:TextBox></td>
                                                </tr>
                                              
                                               
                                                <tr>
                                                    <td width="200px" height="50">Activity (กิจกรรม) : </td>
                                                    <td  colspan="4">
                                                        <asp:TextBox ID="TxtserachActivity" runat="server" placeholder="ชื่อกิจกรรม" CssClass="form-control"></asp:TextBox></td>
                                                    <td></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td  width="200px" height="50">Balance Status : </td>
                                                    <td><asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All Balance</asp:ListItem>
                                                        <asp:ListItem Value="0">Finish</asp:ListItem>
                                                        <asp:ListItem Value="1">UnFinish</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search"> ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    <div class="col-md-8">
                                        <h4 class="text-primary">พบทั้งหมด :
                                        <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                            รายการ</h4>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-gray">
                                                <th>โครงการ</th>
                                                <th>กิจกรรม</th>
                                                <th style="width: 15%;">วันที่เริ่มต้น</th>
                                                <th style="width: 15%;">วันที่สิ้นสุด</th>
                                                <th style="width: 7%;" class="tools">เครื่องมือ</th>
                                                <th id="HeadColDelete" runat="server" class="tools" visible="false">Delete (ลบ)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNameProject" runat="server">
                                                                </asp:Label>
                                                                <asp:Label ID="lblID" runat="server" Visible ="false" ></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblNameAct" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblDateStart" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblDateEnd" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>

                                                                    <ul class="dropdown-menu pull-right">
                                                               <li id="liview" runat="server"  Visible ="false" ><a href="javascript:;" onclick='btnViewClick(<%#Eval("id") %>,<%#Eval("Activity_id") %>,<%#Eval("project_id") %>,<%#Eval("project_type") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>
                                                                        <li id="liedit" runat="server"><a href="javascript:;" onclick='btnEditClick(<%#Eval("id") %>,<%#Eval("Activity_id") %>)'><i class="fa fa-credit-card text-blue"></i>ชำระค่าใช้จ่าย</a></li>
                                                                    </ul>
                                                                </div>
                                                              </center>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                        </tbody>
                                         <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="No" class="center" id="tr1" runat="server" visible="false"></td>
                                                        <td style="text-align: center;" colspan="3"><b>Total</b></td>
                                                        <td style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td></td>

                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                    </table>

                                </div>
                                <!-- /.box-body -->
                                <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                            </div>
                            <!-- /.box -->


                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
                     <asp:TextBox ID="txtClickPID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:TextBox ID="txtClickType" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:TextBox ID="txtClickActivityID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:TextBox ID="txtClickID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <script type="text/javascript">
        <%--function btnSelectClick(project_id) {
            document.getElementById('<%= txtClickID.ClientID %>').value = 0;
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = project_id;
            document.getElementById('<%= btnSelectProject.ClientID %>').click();
        }--%>
        function btnEditClick(id, Activity_id) {
            document.getElementById('<%= txtClickActivityID.ClientID %>').value = Activity_id;
            document.getElementById('<%= txtClickID.ClientID %>').value = id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(id, Activity_id, project_id, project_type) {
            document.getElementById('<%= txtClickActivityID.ClientID %>').value = Activity_id;
            document.getElementById('<%= txtClickID.ClientID %>').value = id;
            document.getElementById('<%= txtClickPID.ClientID %>').value = project_id;
            document.getElementById('<%= txtClickType.ClientID %>').value = project_type;
            document.getElementById('<%= btnView.ClientID %>').click();
        }

    </script>
</asp:Content>

