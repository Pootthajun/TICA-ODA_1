﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmEditSector
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditSectorID As Long
        Get
            Try
                Return ViewState("SectorID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("SectorID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSector")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            txtName.Focus() 'Focus txtName

            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditSectorID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditSectorID = 0
                End Try

            End If
            DisplayEditData()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtName.Enabled = False
            txtCode.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub
    Protected Sub DisplayEditData()
        ClearEditForm()

        If EditSectorID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_Sector(EditSectorID, "")
            If dt.Rows.Count > 0 Then
                txtName.Text = dt.Rows(0)("perposecat_name").ToString()
                txtCode.Text = dt.Rows(0)("code").ToString()

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)

        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbPerposecatLinqDB
                Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByPERPOSECAT_NAME(txtName.Text.Trim, EditSectorID, Nothing)
                If chkDupName = True Then
                    Alert("ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!")
                    Exit Sub
                End If

                lnqLoc.ChkDataByPK(EditSectorID, trans.Trans)

                With lnqLoc
                    .PERPOSECAT_NAME = txtName.Text.Replace("'", "''")
                    .CODE = txtCode.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmSector.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    Alert(ret.ErrorMessage.Replace("'", """"))
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                Alert(ex.Message.Replace("'", """"))
            End Try
        End If

    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtName.Text = "" Then
            Alert("กรุณากรอกข้อมูลให้ครบถ้วน")
            ret = False
        End If

        Return ret
    End Function

    Protected Sub btnCancle_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmSector.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
    Sub ClearEditForm()
        txtName.Text = ""
        txtCode.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub
End Class