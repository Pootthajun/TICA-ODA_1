﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmPlan.aspx.vb" Inherits="frmPlan" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Plan and policies | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Plan and policies (นโยบายและแผนงาน) </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Master</a></li>
                <li class="active">Plan and policies</li>
            </ol>
        </section>
        <br />

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto">

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                                <i class="fa fa-plus"></i>New Plan and policies
                                            </asp:LinkButton>
                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <br/>
                                            <div class="col-sm-1">
                                            </div>

                                            <table>
                                                <tr>
                                                    <td>Plan and policies<br />
                                                        (นโยบายและแผนงาน)</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากชื่อนโยบายและแผนงาน" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Plan Time : </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>

                                                    </td>
                                                    <td width="50px" style="text-align: center">- </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="" Width="150px"></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr><td width="200px" height="50">Status :</td>
                                                    <td><asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                        <asp:ListItem Value="Y">Activate</asp:ListItem>
                                                        <asp:ListItem Value="N">Disabled</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td><td></td>
                                                    <td><asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                 </td> 
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ
                                            </h4>
                                        </div>
                                    </div>

                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th style="width: 24%;">Plan and policies </th>
                                                    <th style="width: 24%;">นโยบายและแผนงาน</th>
                                                    <th style="width: 160px;">Start Date (วันเริ่มต้น)</th>
                                                    <th style="width: 160px;">End Date (วันสิ้นสุด)</th>
                                                    <th style="width: 11%;">Status (สถานะ)</th>
                                                    <th class="tools" style="width: 11%;" id="HeadEdit" runat="server" >Tool (เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblPlanName" runat="server"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblPlanNameTh" runat="server"></asp:Label></td>
                                                            <td class="date">
                                                                <asp:Label ID="lblStartDate" runat="server"></asp:Label></td>
                                                            <td class="date">
                                                                <asp:Label ID="lblEndDate" runat="server"></asp:Label></td>
                                                            <td>
                                                       <asp:LinkButton ID="btnStatusReady" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusready" CssClass="btn bg-green margin-r-4 btn-social" >
                             <i class="fa fa-check"></i>Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:LinkButton>
                                                       <asp:LinkButton ID="btnStatusDisabled" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="cmdstatusDisabled" CssClass="btn bg-red margin-r-4 btn-social">
                             <i class="fa fa-times"></i>Disabled
                                        </asp:LinkButton>
                                                            </td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="tools">
                                                                <center>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>

                                                                    <ul class="dropdown-menu pull-right">
                                                                         <li  id="liedit" runat="server">
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>'>
                                                                            <i class="fa fa-pencil text-blue"></i> แก้ไข</asp:LinkButton>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li id="lidelete" runat="server">
                                                                            <asp:LinkButton ID="btnDelete" runat="server" OnClientClick='javascript:return confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?")'
                                                                            CommandArgument='<%# Eval("id") %>' CommandName="Delete"><i class="fa fa-trash text-danger"></i> ลบ</asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                              </center>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <%-- Page Navigation --%>
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

