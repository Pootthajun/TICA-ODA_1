﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptAdvance_Expense_Organize.aspx.vb" Inherits="rptAdvance_Expense_Organize" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานลูกหนี้เงินจ่ายล่วงหน้า  </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">รายงานลูกหนี้เงินจ่ายล่วงหน้า</li>
            </ol>
        </section>
        <br />


<asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    <%--<li><a href="#" id="aPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" id="aEXCEL" runat="server"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>--%>

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 30px;"></div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">ชื่อโครงการ :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Project_Name" runat="server" placeholder="ค้นหาจากชื่อโครงการ" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">ชื่อหน่วยงาน :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Borrower_Name" runat="server" placeholder="ค้นหาจากชื่อหน่วยงานที่ยืม" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">วัตถุประสงค์ :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Advance_Detail" runat="server" placeholder="ค้นหาจากวัตถุประสงค์" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">ประเภทงบ :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlSearch_group" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">วันยืม :</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="เริ่มต้น" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label for="inputname" class="col-sm-2 control-label line-height">To</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุด" Width="100px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">เลขที่เงินยืม :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Advance_No" runat="server" placeholder="ค้นหาจากเลขที่เอกสารเงินยืม" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>




                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no" id="th2" runat="server" visible="false"></th>
                                                    <th>หน่วยงาน</th>
                                                    <th>วัตถุประสงค์</th>
                                                    <th style =" width :150px;">ประเภทงบประมาณ</th>
                                                    <th style =" width :100px;">วันยืม</th> 
                                                    <th>จำนวนเงินที่ยืม<br />(A)</th>
                                                    <th title ="เลขที่ใบสำคัญที่ผู้ยืมนำมาเคลียร์ค่าใช้จ่าย เช่นเอาใบเสร็จมาเคลียร์หรือเอาเงินสดมาเคลียร์">ค่าใช้จ่าย<br />(B)</th>
                                                    <th>เงินคืน<br />(C)</th>
                                                    <%--<th>เลขที่ใบยืม</th>--%>
                                                    <th>คงเหลือ<br />A-(B+C)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trproject_name" runat="server" style="background-color: ivory;">
                                                            <td data-title="Budget year" colspan="9" style="text-align: center"  id="tdLinkProject1" runat="server" >
                                                                <b>
                                                                    <asp:Label ID="lblproject_name" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                                <a  ID="lnkSelectProject" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="td1" runat="server" style="">
                                                                <asp:Label ID="lblname_th" runat="server"></asp:Label></td>
                                                            <td id="td2" runat="server">
                                                                <asp:Label ID="lblAdvance_Detail" runat="server" ForeColor="black"></asp:Label><br />
                                                                &nbsp; &nbsp;  <small>
                                                                    <asp:Label ID="lblactivity_name" runat="server" ForeColor="blue"></asp:Label></small>
                                                            </td>
                                                            <td id="td3" runat="server">
                                                                <asp:Label ID="lblbudget_group_Name" runat="server"></asp:Label></td>
                                                            <td id="td4" runat="server"  style="text-align: center;">
                                                                <asp:Label ID="lblAdvance_Date" runat="server"></asp:Label></td>

                                                            <td id="td5" runat="server" style="text-align: right;">
                                                                <asp:Label ID="lblAmout" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td id="td6" runat="server" style="text-align: right;">
                                                                <asp:Label ID="lblAmout_Expense" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td id="td7" runat="server" style="text-align: right;">
                                                                <asp:Label ID="lblCash_Back" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td id="td8" runat="server" style="text-align: right;">
                                                                <asp:Label ID="lblBalance" runat="server" ForeColor="black"></asp:Label>
                                                                <asp:Label ID="lblAdvance_No" runat="server" Visible="false"></asp:Label>
                                                                <a  ID="lnkSelectActivity" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="tr2" runat="server">

                                                        <td data-title="Total" colspan="4" style="text-align: center;"><b>Total</b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAmout_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblAmout_Expense_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblCash_Back_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBalance_Sum" runat="server" ForeColor="black"></asp:Label></b></td>


                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

 </asp:Content>




