﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports Constants
Imports System.Data.SqlClient

Public Class frmSubSector
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Public Property AllData As DataTable
        Get
            Try
                Return Session("SubSectorData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("SubSectorData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aSubSector")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            BindData()
            Authorize()
            BindPerposcat()
            BL.SetTextIntKeypress(TxtCRS)
            BL.SetTextIntKeypress(TxtDAC)
        End If
    End Sub

    Protected Sub BindPerposcat()

        Dim sqlRegion As String = "SELECT id,perposecat_name FROM tb_purposecat WHERE 1=1"
        Dim trans As New TransactionDB
        Dim lnq As New TbPerposecatLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sqlRegion, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            Dim BindDataToddl As New ODAENG
            BindDataToddl.BindData_dll(ddlPurposeCategory, "", dt, "Select Purpose Category", "perposecat_name")
        Else
            trans.RollbackTransaction()
            Dim _err As String = trans.ErrorMessage()
        End If
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Sub_Sector & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnStatusReady As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusReady"), LinkButton)
            Dim btnStatusDisabled As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusDisabled"), LinkButton)
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnStatusReady.Enabled = False
                btnStatusDisabled.Enabled = False
                HeadEdit.Visible = False
                ColEdit.Visible = False
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT  tb_purpose.id,Name,CategoryID,tb_purposecat.perposecat_name,DAC5code,CRScode,tb_purpose.ACTIVE_STATUS"
        sql += " From tb_purpose"
        sql += " Left JOIN tb_purposecat ON tb_purposecat.id=tb_purpose.CategoryID WHERE 1=1 "

        If txtSearch.Text.Trim <> "" Then
            sql &= " and Name LIKE '%" + txtSearch.Text.Trim().Replace("'", "''") + "%'"
        End If

        If ddlPurposeCategory.SelectedValue <> "" Then
            sql &= " and tb_purpose.CategoryID =  '" + ddlPurposeCategory.SelectedValue + "' "
        End If

        If TxtDAC.Text.Trim <> "" Then
            sql &= " and DAC5code LIKE '%" + TxtDAC.Text.Trim().Replace("'", "''") + "%'"
        End If

        If TxtCRS.Text.Trim <> "" Then
            sql &= " and CRScode LIKE '%" + TxtCRS.Text.Trim().Replace("'", "''") + "%'"
        End If

        sql &= " order by Name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "SubSectorData"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditSubSector.aspx")
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblCategory As Label = DirectCast(e.Item.FindControl("lblCategory"), Label)
        Dim lblDAC5code As Label = DirectCast(e.Item.FindControl("lblDAC5code"), Label)
        Dim lblCRScode As Label = DirectCast(e.Item.FindControl("lblCRScode"), Label)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)


        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("Name").ToString()
        lblCategory.Text = drv("perposecat_name").ToString()
        lblDAC5code.Text = drv("DAC5code").ToString()
        lblCRScode.Text = drv("CRScode").ToString()

        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditSubSector.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditSubSector.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.DeleteSubSector(e.CommandArgument)
            If ret.IsSuccess Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllY(e.CommandArgument, "TB_Purpose")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllN(e.CommandArgument, "TB_Purpose")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        End If

    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        ClearEditForm()
        BindData()
    End Sub

    Sub ClearEditForm()
        txtSearch.Text = ""
        ddlPurposeCategory.SelectedValue = ""
        TxtCRS.Text = ""
        TxtDAC.Text = ""
    End Sub
End Class