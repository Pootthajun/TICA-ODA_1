﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports System.Data.SqlClient

Partial Class TREE
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            BindTreeData()
        End If
    End Sub

    Protected Sub BindTreeData()

        Dim Sql As String = "SELECT * FROM TB_OU_Country" 'OU
        Dim p(1) As SqlParameter
        p(0) = SqlDB.SetBigInt("@_ID", 1)

        Dim trans As New TransactionDB
        Dim lnq As New TbOuCountryLinqDB

        Dim dt As DataTable = lnq.GetListBySql(Sql, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            '  trans.CommitTransaction()


            For i As Integer = 0 To dt.Rows.Count()
                If dt.Rows(i)("Node_ID").ToString() <> "" Then


                    Dim node As New TreeNode
                    node.Text = dt(i)("Name_EN").ToString()
                    node.Value = dt(i)("Node_ID").ToString()

                    TreeView1.Nodes.Add(node)

                    If dt(i)("Node_ID") = 194 Then
                        Dim Sqlchild As String = "SELECT * FROM TB_OU_Organize where Parent_ID=" & dt(i)("Node_ID")
                        Dim pchild(1) As SqlParameter
                        pchild(0) = SqlDB.SetBigInt("@_ID", 1)

                        Dim transchild As New TransactionDB
                        Dim lnqchild As New TbOuOrganizeLinqDB
                        Dim dtchild As DataTable = lnqchild.GetListBySql(Sqlchild, transchild.Trans, p)


                        For j As Integer = 0 To dtchild.Rows.Count - 1
                            Dim childNode As New TreeNode
                            childNode.Text = dtchild(j)("Name_EN").ToString()
                            childNode.Value = dtchild(j)("Node_ID").ToString()
                            node.ChildNodes.Add(childNode)

                        Next
                        node.CollapseAll()
                    Else
                        'Dim _node As New TreeNode
                        '_node.Text = ""
                        '_node.Value = ""

                        'node.ChildNodes.Add(_node)
                        'node.CollapseAll()
                    End If
                    Dim _node As New TreeNode
                    _node.Text = ""
                    _node.Value = ""

                    node.ChildNodes.Add(_node)
                    node.CollapseAll()


                End If
            Next


        Else
            trans.RollbackTransaction()
            Dim _err As String = lnq.ErrorMessage
        End If
        lnq = Nothing





    End Sub

    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Private Sub TreeView1_TreeNodeExpanded(sender As Object, e As TreeNodeEventArgs) Handles TreeView1.TreeNodeExpanded
        'For i As Integer = 1 To 10
        '    Dim childNode As New TreeNode
        '    childNode.Text = i
        '    childNode.Value = i
        '    e.Node.ChildNodes.Add(childNode)

        'Next
        e.Node.ChildNodes.RemoveAt(0)
        If e.Node.Value <> "" Then
            Dim Sqlchild As String = "SELECT * FROM TB_OU_Organize where Parent_ID=" & e.Node.Value
            Dim pchild(1) As SqlParameter
            pchild(0) = SqlDB.SetBigInt("@_ID", 1)

            Dim transchild As New TransactionDB
            Dim lnqchild As New TbOuOrganizeLinqDB
            Dim dtchild As DataTable = lnqchild.GetListBySql(Sqlchild, transchild.Trans, pchild)


            For j As Integer = 0 To dtchild.Rows.Count - 1
                Dim childNode As New TreeNode
                childNode.Text = dtchild(j)("Name_EN").ToString()
                childNode.Value = dtchild(j)("Node_ID").ToString()
                e.Node.ChildNodes.Add(childNode)

            Next
            'e.Node.CollapseAll()
        Else
            'Dim _node As New TreeNode
            '_node.Text = ""
            '_node.Value = ""

            'e.Node.ChildNodes.Add(_node)
            'e.Node.CollapseAll()
        End If
    End Sub
End Class
