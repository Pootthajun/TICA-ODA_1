﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class frmRecipient_Passport
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG

    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property Register_ID As Integer

        Get
            Try
                Return ViewState("Register_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Register_ID") = value
        End Set
    End Property

    Public Property node_id As String

        Get
            Try
                Return ViewState("node_id")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            ViewState("node_id") = value
        End Set
    End Property

    Public Property Activity_ID As Integer

        Get
            Try
                Return ViewState("Activity_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Activity_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
        li.Attributes.Add("class", "active")
        Dim a As HtmlAnchor = Me.Page.Master.FindControl("arptProject")
        a.Attributes.Add("style", "color:#FF8000")

        If Not IsPostBack Then
            BindList()
            pnlAdSearch.Visible = False

            pnlList.Visible = True
            pnlEdit.Visible = True
        End If

    End Sub

    Private Sub BindList()


        Dim DT As New DataTable
        Try
            Dim Sql As String = ""

            Sql &= " SELECT * FROM vw_Activity_Recipient_Info "
            Sql &= " WHERE person_type=1   " + Environment.NewLine          '--ผ่านการคัดเลือกเท่านั้น

            If (ddlProject_Type.SelectedValue > -1) Then
                Sql &= "   And project_type =  " & ddlProject_Type.SelectedValue & " " + Environment.NewLine
                Title += " ประเภทโครงการ : " & ddlProject_Type.SelectedItem.ToString()
            Else
                Title += " โครงการทั้งหมด "
            End If

            If (txtSearch_Project.Text <> "") Then
                Sql &= "  AND project_name Like '%" & txtSearch_Project.Text & "%'     " + Environment.NewLine
                Title += " ของ " & txtSearch_Project.Text
            End If

            If (txtSearch_Activity.Text <> "") Then
                Sql &= "  AND project_name Like '%" & txtSearch_Activity.Text & "%'     " + Environment.NewLine
                Title += " กิจกรรม : " & txtSearch_Activity.Text
            End If

            If (txtSearch_Country.Text <> "") Then
                Sql &= "  AND ( OU_name_th Like '%" & txtSearch_Country.Text & "%' OR " + Environment.NewLine
                Sql &= "  OU_name_en Like '%" & txtSearch_Country.Text & "%')    " + Environment.NewLine
                Title += " ประเทศ " & txtSearch_Country.Text
            End If


            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = Convert.ToDateTime(txtStartDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = Convert.ToDateTime(txtEndDate.Text).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                'Sql &= " AND start_date  Between '" & StartDate & "' And '" & EndDate & "' " + Environment.NewLine

                Sql &= " AND ( start_date  >= '" & StartDate & "' " + Environment.NewLine
                Sql &= " AND end_date  <= '" & EndDate & "'  ) " + Environment.NewLine

                Title += "ตั้งแต่วันที่" & txtStartDate.Text & "ถึงวันที่" & txtEndDate.Text

            End If

            Sql &= "  ORDER BY project_type,project_Name ,Activity_Name  " + Environment.NewLine

            Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnectionString)

            DA.Fill(DT)


            lblCount.Text = Title
            If DT.Rows.Count = 0 Then
                lblCount.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblCount.Text &= "  " & FormatNumber(DT.Rows.Count, 0) & " คน"
            End If

            Session("Search_Candidate_Title") = lblCount.Text

        Catch ex As Exception
            Dim Msg As String = ex.Message
        End Try



        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Candidate_List") = DT


        Pager.SesssionSourceName = "rptProjectPage"
        Pager.RenderLayout()
    End Sub



    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblCourse As Label = DirectCast(e.Item.FindControl("lblCourse"), Label)
        Dim lblPeriod As Label = DirectCast(e.Item.FindControl("lblPeriod"), Label)
        Dim lblCandidate_Name As Label = DirectCast(e.Item.FindControl("lblCandidate_Name"), Label)
        Dim lblCandidate_ID As Label = DirectCast(e.Item.FindControl("lblCandidate_ID"), Label)
        Dim lblCountry As Label = DirectCast(e.Item.FindControl("lblCountry"), Label)
        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)

        Dim lblPassport_ID As Label = DirectCast(e.Item.FindControl("lblPassport_ID"), Label)
        Dim lblDate_Insurance As Label = DirectCast(e.Item.FindControl("lblDate_Insurance"), Label)
        Dim lblDate_VISA As Label = DirectCast(e.Item.FindControl("lblDate_VISA"), Label)
        Dim lblDate_Passport As Label = DirectCast(e.Item.FindControl("lblDate_Passport"), Label)
        Dim lblDate_Of_Birth As Label = DirectCast(e.Item.FindControl("lblDate_Of_Birth"), Label)

        Dim lblEMail As Label = DirectCast(e.Item.FindControl("lblEMail"), Label)
        Dim lblTelephone As Label = DirectCast(e.Item.FindControl("lblTelephone"), Label)
        Dim lblStatus As Label = DirectCast(e.Item.FindControl("lblStatus"), Label)

        Dim btnEdit As LinkButton = DirectCast(e.Item.FindControl("btnEdit"), LinkButton)

        lblCourse.Text = e.Item.DataItem("activity_name").ToString
        lblCandidate_Name.Text = e.Item.DataItem("name_th").ToString
        lblCandidate_ID.Text = e.Item.DataItem("node_id").ToString
        lblCountry.Text = e.Item.DataItem("OU_name_th").ToString
        lblProject.Text = e.Item.DataItem("project_name").ToString
        lblPeriod.Text = "Period :" & e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString

        lblPassport_ID.Text = e.Item.DataItem("passport_ID").ToString
        lblDate_Insurance.Text = e.Item.DataItem("date_TH_Insurance").ToString
        lblDate_VISA.Text = e.Item.DataItem("date_TH_Visa").ToString
        lblDate_Passport.Text = e.Item.DataItem("date_TH_passport").ToString
        lblDate_Of_Birth.Text = e.Item.DataItem("date_TH_Birth").ToString

        lblEMail.Text = e.Item.DataItem("email").ToString
        lblTelephone.Text = e.Item.DataItem("telephone").ToString
        lblStatus.Text = e.Item.DataItem("Lean_Status_Name").ToString


        If Not IsDBNull(e.Item.DataItem("TB_Recipience_Register_ID")) Then
            Register_ID = e.Item.DataItem("TB_Recipience_Register_ID")
        Else
            Register_ID = 0
        End If
        btnEdit.CommandArgument = e.Item.DataItem("node_id")
        node_id = e.Item.DataItem("node_id")
    End Sub

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Dim btnEdit As LinkButton = DirectCast(e.Item.FindControl("btnEdit"), LinkButton)

        Select Case e.CommandName
            Case "Edit"
                ClearData()
                ' 
                '----Select Data---
                Dim Sql As String = " select * from vw_Activity_Recipient_Info where node_id=" & btnEdit.CommandArgument
                Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then

                    txtPersonNameTH.Text = DT.Rows(0).Item("name_th").ToString()
                    txtNodeID.Text = DT.Rows(0).Item("node_id").ToString()
                    txtparentNodeID.Text = DT.Rows(0).Item("parent_id").ToString()
                    txtPersonNameEN.Text = DT.Rows(0).Item("name_en").ToString()


                    'ddlCountry.SelectedIndex = Nothing
                    'ddlExecutingOrganize.SelectedIndex = Nothing
                    txtInstitute.Text = DT.Rows(0).Item("Institute").ToString()
                    'txtAgency.Text = DT.Rows(0).Item("").ToString()
                    'txtAgency_Address.Text = DT.Rows(0).Item("").ToString()
                    txtPersonTelephone.Text = DT.Rows(0).Item("telephone").ToString()


                    rdLearning.Checked = False
                    rdDegree.Checked = False
                    rdRetire.Checked = False

                    Select Case DT.Rows(0).Item("Lean_Status")
                        Case 0
                            rdLearning.Checked = True
                        Case 1
                            rdDegree.Checked = True
                        Case 2
                            rdRetire.Checked = True
                        Case Else
                            '--ไม่เลือก
                    End Select


                    'txtDescription.Text = DT.Rows(0).Item("").ToString()
                    txtPosition.Text = DT.Rows(0).Item("Position").ToString()
                    txtHome_Address.Text = DT.Rows(0).Item("Home_Address").ToString()
                    'txtPersonIDCard.Text = DT.Rows(0).Item("").ToString()
                    txtPersonPassportID.Text = DT.Rows(0).Item("passport_ID").ToString()


                    If Not IsDBNull(DT.Rows(0).Item("expired_date_Insurance")) Then
                        txtDate_Insurance.Text = Convert.ToDateTime(DT.Rows(0).Item("expired_date_Insurance").ToString()).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                    End If
                    If Not IsDBNull(DT.Rows(0).Item("expired_date_Visa")) Then
                        txtDate_VISA.Text = Convert.ToDateTime(DT.Rows(0).Item("expired_date_Visa").ToString()).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                    End If
                    If Not IsDBNull(DT.Rows(0).Item("expired_date_passport")) Then
                        txtDate_Passport.Text = Convert.ToDateTime(DT.Rows(0).Item("expired_date_passport").ToString()).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                    End If
                    'If Not IsDBNull(DT.Rows(0).Item("Birth_date")) Then
                    '    txtPersonBirthDay.Text = Convert.ToDateTime(DT.Rows(0).Item("Birth_date").ToString()).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

                    'End If


                    txtPersonEmail.Text = DT.Rows(0).Item("email").ToString()

                Else
                    '----รายการใหม่ ไม่ได้มาจากการสมัครแต่เป็นการเพิ่มข้อมูล คน ใน OU เอง
                    '--ไม่มีรายการดังกล่าว
                End If



        End Select










    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        '----Validate

        '----Save
        '----Save--- 
        Dim Sql As String = " select * from TB_Recipience_Register where node_id=" & node_id & " AND Activity_id=" & Activity_ID
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If (DT.Rows.Count = 0) Then
            DR = DT.NewRow
            DR("id") = BL.GetNew_ID("TB_Recipience_Register", "id")
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
        End If
        DR("node_id") = node_id
        If Activity_ID > 0 Then
            DR("Activity_ID") = Activity_ID
        End If
        DR("name_th") = txtPersonNameTH.Text
        DR("name_en") = txtPersonNameEN.Text
        DR("parent_id") = txtparentNodeID.Text
        DR("Institute") = txtInstitute.Text
        DR("telephone") = txtPersonTelephone.Text
        DR("email") = txtPersonEmail.Text

        If rdLearning.Checked Then
            DR("Lean_Status") = 0
        ElseIf rdDegree.Checked Then
            DR("Lean_Status") = 1
        ElseIf rdRetire.Checked Then
            DR("Lean_Status") = 2
        Else
            DR("Lean_Status") = DBNull.Value
        End If

        DR("Position") = txtPosition.Text
        DR("Home_Address") = txtHome_Address.Text
        DR("passport_ID") = txtPersonPassportID.Text
        DR("expired_date_Insurance") = Converter.StringToDate(txtDate_Insurance.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        DR("expired_date_Visa") = Converter.StringToDate(txtDate_VISA.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        DR("expired_date_passport") = Converter.StringToDate(txtDate_Passport.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))
        'DR("Birth_date") = Converter.StringToDate(txtPersonBirthDay.Text.Trim, "dd/MM/yyyy", New System.Globalization.CultureInfo("th-TH"))

        DR("updated_by") = UserName
        DR("updated_date") = Now


        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('" & ex.Message.ToString() & "');", True)
            Exit Sub
        End Try

        BindList()
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click

    End Sub


    Private Sub ClearData()
        Register_ID = 0
        node_id = 0
        Activity_ID = 0
        txtPersonNameTH.Text = ""
        txtNodeID.Text = ""
        txtparentNodeID.Text = ""
        txtPersonNameEN.Text = ""
        'ddlCountry.SelectedIndex = Nothing
        'ddlExecutingOrganize.SelectedIndex = Nothing
        txtInstitute.Text = ""
        'txtAgency.Text = ""
        'txtAgency_Address.Text = ""
        txtPersonTelephone.Text = ""

        rdLearning.Checked = False
        rdDegree.Checked = False
        rdRetire.Checked = False

        'txtDescription.Text = ""
        txtPosition.Text = ""
        txtHome_Address.Text = ""
        'txtPersonIDCard.Text = ""
        txtPersonPassportID.Text = ""


        txtDate_Insurance.Text = ""
        txtDate_VISA.Text = ""
        txtDate_Passport.Text = ""
        'txtPersonBirthDay.Text = ""

        txtPersonEmail.Text = ""


    End Sub


End Class
