﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmBudgetDetail.aspx.vb" Inherits="frmBudgetDetail1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Incoming Budget | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>Incoming Budget Detail (รายละเอียดงบประมาณ)
            </h4>
            <ol class="breadcrumb">
                <li><a href="frmBudget.aspx"><i class="fa fa-dollar"></i>Budget</a></li>
                <li class="active">Incoming Budget Detail</li>
            </ol>
        </section>

        <!-- Main content -->
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <section class="content">

                    <%-- Informations --%>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                    <table class="table table-bordered">
                                        <tr class="bg-info">
                                            <th style="width: 150px" colspan="2">
                                                <h5><b class="text-blue">Informations Budget (ข้อมูลงบประมาณ) </b></h5>
                                            </th>
                                        </tr>

                                        <%--Record & Received Date --%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Budget Group (กลุ่มงบประมาณ) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlGroupBudget" runat="server" CssClass="form-control select2" Width="450px" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--Record & Received Date --%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Sub Budget (งบประมาณย่อย) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="dllSubBudget" runat="server" CssClass="form-control select2" Width="760px"></asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--Record & Received Date --%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Budget Years (ปีงบประมาณ) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Width="180px" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div class="col-sm-4">
                                                    <p class="pull-right">Received Date (วันที่ได้รับงบประมาณ) :<span style="color: red">*</span></p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtReceivedDate" Width="150px" runat="server" placeholder=""></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtReceivedDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--Record & Received Date --%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Provide By (ผู้ให้) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtProvideBy" CssClass="form-control m-b" runat="server" Width="250px" MaxLength="250"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-4">
                                                    <p class="pull-right">amount (จำนวนเงิน) :<span style="color: red">*</span></p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control cost"  placeholder="" MaxLength="20"></asp:TextBox>
                                                </div>

                                            </td>
                                        </tr>

                                        <%--Description--%>
                                        <tr>
                                            <td style="width: 250px">
                                                <p class="pull-right">Description (รายละเอียด) :</p>
                                            </td>
                                            <td>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <%-- Footer --%>
                                    <div class="modal-footer">
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
                                            <i class="fa fa-save"></i> Save
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google" OnClick="btnCancel_Click">
                                            <i class="fa fa-reply"></i> Cancel
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /.content -->


    </div>
</asp:Content>

