﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptCountryReceiveAidEveryYear.aspx.vb" Inherits="rptAidSummary" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานประเทศที่ได้รับความช่วยเหลือในแต่ละปี</h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายนอกองค์กร</a></li>
                <li class="active">รายงานประเทศที่ได้รับความช่วยเหลือในแต่ละปี</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white"  DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top:30px;*/"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Year :</label>
                                                 <div class="col-sm-9">
                                                     <asp:DropDownList ID="ddlProject_Year" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                       
                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Country :</label>
                                                 <div class="col-sm-9">
                                                 <asp:DropDownList ID="ddlProject_Country" runat="server" CssClass="form-control select2" Style="width: 50%">
                                                       
                                                     </asp:DropDownList>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                     <div class="row" style="margin-top:30px;"></div>
                                      

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                   <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>



                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;">Unit : THB</h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no"  id="th1" runat="server">No.<br/>(ลำดับ)</th>
                                                  <%--  <th visible="false">Project Code<br/>(รหัส)</th>--%>
                                                    <th>Country<br/>(ประเทศ)</th>
                                                    <th>Project Amount<br/>(จำนวนโปรเจค)</th>
                                                    <th>Disbursed Amount<br/>(จ่ายจริง)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Budget year" colspan="4" style="text-align: center">
                                                             <b>  ปี  <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-title="No" class="center"  id="td1" runat="server"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                            <td data-title="Country_name_th"  id="td2" runat="server"><asp:Label ID="lblCountry_name_th" runat="server"></asp:Label></td>
                                                            <td data-title="Project_Amount" style="text-align :center;width: 120px;" id="td3" runat="server"><asp:Label ID="lblProject_Amount" runat="server"></asp:Label></td>
                                                            <td data-title="Disbursed_Amount" style="text-align :right;width: 180px;" id="td4" runat="server"><b><asp:Label ID="lblDisbursed_Amount" runat="server" ForeColor="black"></asp:Label></b>
                                                                <asp:Button  ID="btnDrillDown" runat="server" ToolTip ="ดูรายชื่อโครงการ"   Style="display: none;" CommandName="select"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="pnlProject" runat ="server" >
                                                            <asp:Repeater ID="rptProject" runat="server">
                                                                <ItemTemplate>
                                                                    <tr style ="color: blue;">
                                                                        <td data-title="perposecat_name"  id="tdLinkProject1" runat="server" colspan ="3" style ="padding-left :100px;"><asp:Label ID="lblProject_Name" runat="server"></asp:Label></td>
                                                                        <td data-title="Disbursed_Amount" style="text-align :right;width: 180px;" id="tdLinkProject2" runat="server"><asp:Label ID="lblAmount_Actual" runat="server" ForeColor="black"></asp:Label>
                                                                            <a  ID="lnkSelect" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible ="True">
                                            <tfoot style="background-color:LemonChiffon;" >
                                        <tr id="trFooter_Qty" runat="server" >
                                             
                                            <td  data-title="Country_name_th" colspan ="2" style="text-align :center ;" ><b>Budget Summary</b></td>
          
                                            <td data-title="Project_Amount"  style="text-align :center ;" ><b><asp:Label ID="lblProject_Sum" runat="server" ForeColor="black" Text =""></asp:Label></b></td>
                                            <td data-title="Total Amount" style="text-align :right ;text-decoration: underline;" ><b><asp:Label ID="lblDisbursed_Amount" runat="server" ForeColor="black"></asp:Label></b></td>
                                                            

										</tr>
                                    

                                        </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>

