﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptSummary_ByCountry.aspx.vb" Inherits="rptSummary_ByCountry" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>การให้ความช่วยเหลือแก่ต่างประเทศ (สรุปรายประเทศ)   </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>รายงานภายในองค์กร</a></li>
                <li class="active">การให้ความช่วยเหลือแก่ต่างประเทศ (สรุปรายประเทศ)</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top: 30px; */"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"><span style="color: red;">*</span>    Report Type :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control select2" Style="width: 80%" AutoPostBack="true">
                                                        <asp:ListItem Value="-1" Text="...." Selected></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Project"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Non Project"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Loan"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Contribution"></asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-sm-6" id="divSearch_Year" runat="server" >
                                                <label for="inputname" class="col-sm-3 control-label line-height">Budget Year :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlBudgetYear" runat="server" CssClass="form-control select2" Style="width:80%">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Country :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control select2" Style="width: 80%">
                                                    </asp:DropDownList>
                                                    <%--<asp:TextBox ID="txtSearch_Country" runat="server" placeholder="ค้นหาจากชื่อประเทศ" CssClass="form-control" Style="width: 80%"></asp:TextBox>--%>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>


                                        </div>
                                        <div class="row" style="margin-top: 30px;"></div>





                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">

                                                    <th>ประเทศ</th>
                                                    <th>โครงการ (จำนวน)</th>
                                                    <th>ทุน * (จำนวน)</th>
                                                    <th>มูลค่า</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style="background-color: ivory;">
                                                            <td data-title="Budget year" colspan="5" style="text-align: center">
                                                                <b>ปี 
                                                                    <asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  id="td1" runat="server" data-title="Country">
                                                                <asp:Label ID="lblCountry" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td  id="td2" runat="server" data-title="QTY Project" style="text-align: center;">
                                                                <asp:Label ID="lblQTY_Project" runat="server"></asp:Label></td>
                                                            <td  id="td3" runat="server" data-title="QTY Recipient" style="text-align: center;">
                                                                <asp:Label ID="lblQTY_Recipient" runat="server"></asp:Label></td>
                                                            <td  id="td4" runat="server" data-title="Disbursed Amount" style="text-align: right;">
                                                                <b><asp:Label ID="lblSUM_Amount" runat="server" ForeColor="black"></asp:Label></b>
                                                                <asp:Button ID="btnDrillDown" runat="server" ToolTip="ดูรายชื่อโครงการ" Style="display: none;" CommandName="select"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="pnlProject" runat="server">
                                                            <asp:Repeater ID="rptProject" runat="server">
                                                                <ItemTemplate>
                                                                    <tr style="color: blue;">
                                                                        <td data-title="รายการ" id="tdLinkProject1" runat="server" style="padding-left: 50px;" colspan="4">
                                                                            <asp:Label ID="lblProject_Name" runat="server"></asp:Label>
                                                                            <a id="lnkSelect" runat="server" tooltip="ดูรายละเอียดของโครงการ" style="display: none;" target="_blank"></a>
                                                                        </td>
                                                                         

                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="Country" style="text-align: center;"><b>Total </b></td>
                                                        <td data-title="QTY Project" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblQTY_Project" runat="server"></asp:Label></b></td>
                                                        <td data-title="QTY Recipient" style="text-align: center; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblQTY_Recipient" runat="server"></asp:Label></b></td>
                                                        <td data-title="Disbursed Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblSUM_Amount" runat="server" ForeColor="black"></asp:Label></b></td>

                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>

                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 style="margin-left: 18px;"><span>* ทุนภายใต้ประเภทของแต่ละโครงการ  </span></h4>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->



                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>



