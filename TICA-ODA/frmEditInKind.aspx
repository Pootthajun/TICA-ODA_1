﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditInKind.aspx.vb" Inherits="frmEditInKind" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>In Kind | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> In Kind (การสนับสนุนอย่างอื่น) </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
                <li><a href="frmInKind.aspx"> In Kind</a></li>
                <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> In Kind</li>
            </ol>
        </section>

        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <!-- Main content -->
                <section class="content">
                    <br />
                    <!-- START CUSTOM TABS -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal Form -->
                            <div class="box">
                                <div class="box-header with-border bg-blue-gradient">
                                    <h4 class="box-title">
                                        <asp:Label ID="lblEditMode2" runat="server" Text="Label"></asp:Label>
                                        In Kind
                                    </h4>
                                </div>
                                <!-- /.box-header -->

                                <!-- form start -->
                                <form class="form-horizontal">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Sector" class="col-sm-2 control-label">In Kind (Eng)
                                                <br/>(ความช่วยเหลืออื่น): <span style="color:red">*</span></label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>

                                         <br /><br />
                                        <div class="clearfix"></div>

                                        <div class="form-group">
                                            <label for="inputname" class="col-sm-2 control-label">In Kind (Thai)
                                                <br/>(ความช่วยเหลืออื่น) :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control" ></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <!-- /.box -->
                                        <div class="clearfix"></div>
                                        <br />

                                        <div class="form-group">
                                            <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" Checked="true" /></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="col-sm-9"></div>
                                        <div class="col-lg-8"></div>
                                        <div class="col-lg-2">

                                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                            <i class="fa fa-save"></i> Save
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google"  OnClick="btnCancle_Click">
                                <i class="fa fa-reply"></i> Cancel 
                                            </asp:LinkButton>

                                        </div>
                                    </div>
                                    <!-- /.box-footer -->
                                    <%--           <div class="box-body">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="bg-gray">
                        <th style="width: 100px">ลำดับ</th>
                        <th>In Kind 5 Last Order (In Kind 5 ลำดับล่าสุด )</th>
                        <th>Description (รายละเอียด)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList"  runat="server" OnItemDataBound="rptList_ItemDataBound">
				        <ItemTemplate> 
                        <tr>
                            <td><asp:Label ID="lblNumber" runat="server"></asp:Label></td>
                        <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                            <td><asp:Label ID="lblDetail" runat="server"></asp:Label></td>
                        </tr>
                        </ItemTemplate>
                            </asp:Repeater>
                      
                    
                    </table>
                </div>--%>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.col -->

                    </div>
                    <!-- /.row -->
                    <!-- END CUSTOM TABS -->


                </section>
                <!-- /.content -->
            </ContentTemplate>
        </asp:UpdatePanel>
  </div>
 

<!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" /> 

</asp:Content>
