﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptSummary_Advance_Plan_Expense
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Private Sub rptExpense_Activity_GroupBy_Country_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_04")
            a.Attributes.Add("style", "color:#FF8000")

            BindList()
            BL.Bind_DDL_BudgetGroup(ddlSearch_group)

        End If

    End Sub


    Public Function GetList() As DataTable


        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""
            sql &= "    Select * FROM  _vw_Activity_Budget_Plan_Actual_Advance WHERE Project_type in (0,1)  " + Environment.NewLine

            If (txtSearch_Project_Name.Text <> "") Then
                filter &= "  project_name Like '%" & txtSearch_Project_Name.Text & "%'  AND " + Environment.NewLine
                Title += " โครงการ: " & txtSearch_Project_Name.Text
            End If
            If (txtSearch_Activity_Name.Text <> "") Then
                filter &= "  activity_name Like '%" & txtSearch_Activity_Name.Text & "%'  AND " + Environment.NewLine
                Title += " กิจกรรม: " & txtSearch_Activity_Name.Text
            End If


            If (ddlSearch_group.SelectedIndex > 0) Then
                filter &= "  budget_group_id =" & ddlSearch_group.SelectedValue & "  AND " + Environment.NewLine
                Title += " ประเภทงบประมาณ: " & ddlSearch_group.SelectedItem.ToString()
            End If

            If (txtStartDate.Text <> "" And txtEndDate.Text <> "") Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                sql &= " And  ( CONVERT(VARCHAR,start_date,112) >=dbo.GetDateFormatSearch('" & StartDate & "') AND CONVERT(VARCHAR,start_date,112) <=dbo.GetDateFormatSearch('" & EndDate & "')" + Environment.NewLine
                sql &= " Or     Convert(VARCHAR,end_date, 112) >= dbo.GetDateFormatSearch('" & StartDate & "') AND CONVERT(VARCHAR,end_date,112) <=dbo.GetDateFormatSearch('" & EndDate & "')  )" + Environment.NewLine

                Title += " ระหว่างวันที่ " & txtStartDate.Text & " ถึงวันที่ " & txtEndDate.Text
            End If

            If (txtStartDate_Activity.Text <> "" And txtEndDate_Activity.Text <> "") Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate_Activity.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate_Activity.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                sql &= " And  ( CONVERT(VARCHAR,Activity_actual_start,112) >=dbo.GetDateFormatSearch('" & StartDate & "') AND CONVERT(VARCHAR,Activity_actual_start,112) <=dbo.GetDateFormatSearch('" & EndDate & "')" + Environment.NewLine
                sql &= " Or     Convert(VARCHAR,Activity_actual_end, 112) >= dbo.GetDateFormatSearch('" & StartDate & "') AND CONVERT(VARCHAR,Activity_actual_end,112) <=dbo.GetDateFormatSearch('" & EndDate & "')  )" + Environment.NewLine

                Title += " ระยะเวลากิจกรรม " & txtStartDate_Activity.Text & " ถึงวันที่ " & txtEndDate_Activity.Text
            End If


            If filter <> "" Then
                sql += " AND " & filter.Substring(0, filter.Length - 6) + Environment.NewLine
            End If

            sql &= " ORDER BY project_type,project_Code, project_name ,activity_name  ,end_date " + Environment.NewLine

            DT = SqlDB.ExecuteTable(sql)


            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If


            Session("Search_Advance_Plan_Expense_Title") = lblTotalRecord.Text


        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()

        If (DT.Rows.Count > 0) Then
            lblPay_Amount_Plan_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Plan)", "")).ToString("#,##0.00")

            DT.DefaultView.RowFilter = "Advance_Balance IS NOT NULL"
            If DT.DefaultView.Count > 0 Then
                lblAdvance_Balance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Advance_Balance)", "")).ToString("#,##0.00")
            Else
                lblAdvance_Balance_Sum.Text = ""
            End If
            lblPay_Amount_Actual_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Pay_Amount_Actual)", "")).ToString("#,##0.00")
            lblBalance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")

            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Advance_Plan_Expense") = DT

        Pager.SesssionSourceName = "Search_Advance_Plan_Expense"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub


    Dim LastProject As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblproject_name As Label = DirectCast(e.Item.FindControl("lblproject_name"), Label)

        Dim lblActivity As Label = DirectCast(e.Item.FindControl("lblActivity"), Label)
        Dim lblbudget_group_Name As Label = DirectCast(e.Item.FindControl("lblbudget_group_Name"), Label)
        Dim lblPeriod As Label = DirectCast(e.Item.FindControl("lblPeriod"), Label)
        Dim lblPeriod_Activity As Label = DirectCast(e.Item.FindControl("lblPeriod_Activity"), Label)
        Dim lblPay_Amount_Plan As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Plan"), Label)
        Dim lblAdvance_Balance As Label = DirectCast(e.Item.FindControl("lblAdvance_Balance"), Label)
        Dim lblPay_Amount_Actual As Label = DirectCast(e.Item.FindControl("lblPay_Amount_Actual"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)
        Dim trproject_name As HtmlTableRow = e.Item.FindControl("trproject_name")


        '--------Lastbudget_year------------
        If LastProject <> e.Item.DataItem("project_name").ToString Then
            LastProject = e.Item.DataItem("project_name").ToString
            lblproject_name.Text = LastProject
            trproject_name.Visible = True
        Else
            trproject_name.Visible = False
        End If
        lblActivity.Text = e.Item.DataItem("activity_name").ToString
        lblbudget_group_Name.Text = e.Item.DataItem("budget_group_Name").ToString

        If Convert.IsDBNull(e.Item.DataItem("start_date_th")) = False Then
            lblPeriod.Text = e.Item.DataItem("start_date_ShortTH").ToString & "-" & e.Item.DataItem("end_date_ShortTH").ToString
        End If
        If Convert.IsDBNull(e.Item.DataItem("Activity_actual_start")) = False Then
            lblPeriod_Activity.Text = e.Item.DataItem("Activity_start_date_ShortTH").ToString & "-" & e.Item.DataItem("Activity_end_date_ShortTH").ToString
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Plan")) = False Then
            lblPay_Amount_Plan.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Plan")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Advance_Balance")) = False Then
            lblAdvance_Balance.Text = Convert.ToDecimal(e.Item.DataItem("Advance_Balance")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Pay_Amount_Actual")) = False Then
            lblPay_Amount_Actual.Text = Convert.ToDecimal(e.Item.DataItem("Pay_Amount_Actual")).ToString("#,##0.00")
        End If

        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If

        Dim lnkSelectProject As HtmlAnchor = e.Item.FindControl("lnkSelectProject")
        Dim tdLinkProject1 As HtmlTableCell = e.Item.FindControl("tdLinkProject1")
        tdLinkProject1.Style("cursor") = "pointer"
        tdLinkProject1.Attributes("onClick") = "document.getElementById('" & lnkSelectProject.ClientID & "').click();"
        Dim lnkSelectActivity As HtmlAnchor = e.Item.FindControl("lnkSelectActivity")
        For i As Integer = 1 To 7
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & lnkSelectActivity.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Or (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelectProject.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmProject_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelectProject.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmLoan_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        Else
            lnkSelectProject.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
            lnkSelectActivity.HRef = "frmContribuiltion_Detail_Activity.aspx?id=" & e.Item.DataItem("project_id") & "&mode=edit&type=" & e.Item.DataItem("project_type")

        End If
    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If (txtStartDate.Text <> "" Or txtEndDate.Text <> "") Then
            If txtStartDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('เลือกวันเริ่มต้น');", True)
                Exit Sub
            End If

            If txtEndDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('เลือกวันสิ้นสุด');", True)
                Exit Sub
            End If

        End If
        BindList()

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_Advance_Plan_Expense.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptSummary_Advance_Plan_Expense.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class



