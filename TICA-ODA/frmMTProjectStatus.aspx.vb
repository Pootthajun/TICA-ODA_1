﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmMTProjectStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BindData()
        End If
    End Sub
    Protected Sub BindData()

        Dim sql As String = "SELECT id,pstatus_id,pstatus_name FROM web_pstatus WHERE 1=1"
        Dim trans As New TransactionDB
        Dim lnq As New TbPstatusLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sql, trans.Trans, p)

        Dim Filter As String = ""
        If txtSearch.Text <> "" Then
            Filter += "pstatus_name LIKE '%" + txtSearch.Text.Trim() + "%'"
        End If
        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            dt.DefaultView.RowFilter = Filter
            rptList.DataSource = dt.DefaultView
            rptList.DataBind()

            lblCount.Text = dt.DefaultView.Count
        Else
            trans.RollbackTransaction()
            Dim err = lnq.ErrorMessage()
        End If
        lnq = Nothing
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        BindData()
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

        lblName.Text = drv("pstatus_name").ToString()

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Session("SESSION_ID") = e.CommandArgument
            Response.Redirect("frmEditProjectStatus.aspx")
        End If

        If e.CommandName = "cmdDelete" Then
            Dim tran As New TransactionDB
            Dim lnq As New tbPstatusLinqDB

            Dim ret As ExecuteDataInfo = lnq.DeleteByPK(e.CommandArgument, tran.Trans)
            If ret.IsSuccess = True Then
                tran.CommitTransaction()
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
            Else
                tran.RollbackTransaction()
                Alert("ไม่สามารถลบข้อมูลได้")
                Dim _err As String = tran.ErrorMessage()
            End If
            lnq = Nothing
        End If
        BindData()
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)

        Response.Redirect("frmEditProjectStatus.aspx")
    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub
End Class