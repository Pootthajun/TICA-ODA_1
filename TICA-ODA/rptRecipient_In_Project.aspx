﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptRecipient_In_Project.aspx.vb" Inherits="rptRecipient_In_Project" %>


<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>รายงานรายชื่อผู้รับทุนแบ่งตามโครงการ </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">รายงานรายชื่อผู้รับทุนแบ่งตามโครงการ</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                   

                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right;margin-bottom:10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    
                                                    <li><asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
											        <li><asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>	

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="/*margin-top:30px;*/"></div>
                                        <div class="row">
                                           
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Project :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากชื่อโครงการ" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Recipient :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Recipient" runat="server" placeholder="ค้นหาจากชื่อผู้รับทุน" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                        </div>
                                        <div class="row">
                                           
                                             <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height">Course :</label>
                                                 <div class="col-sm-9">
                                                     <asp:TextBox ID="txtSearch_Course" runat="server" placeholder="ค้นหาจาก Course" Style="width: 80%" CssClass="form-control"></asp:TextBox>
                                                 </div>
                                             </div>
                                            <div class="col-sm-6">
                                                
                                             </div>
                                        </div>
                                        

                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">

                                                 </div>
                                                </div>
                                            <div class="col-sm-6">
                                                 <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                 <div class="col-sm-9 ">
                                                     <p class="pull-right" style="margin-right:10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social"   >
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"  ></asp:Label> 
                                                        </asp:LinkButton>
                                                    </p>

                                                 </div>
                                                </div>

                                        </div>



                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                            <h4 class="text-primary" style="margin-left :20px;"><asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                        </div>
                                        </div>
                                         <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                            <h5  style="margin-left :20px;"></h5>
                                        
                                        </div> 
                                    </div> 
                                        </div>   
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no"  id="th1" runat="server" visible="false">No.<br/>(ลำดับ)</th>  
                                                    <th style="width:200px;">Name Surname</th>                                                 
                                                    <th>Course</th>
                                                    <th style="width:100px;">Start - End Date</th>
                                                    <th>Institute</th>
                                                    <th style="width:100px;">Expire Date of<br /> Insurance </th>
                                                    <th style="width:100px;">Expire Date of<br /> VISA</th>
                                                    <th style="width:100px;">Expire Date of<br /> Passport</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trbudget_year" runat="server" style ="background-color :ivory;">
                                                            <td data-title="Budget year" colspan="7" style="text-align: center">
                                                             <b><asp:Label ID="lblbudget_year" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="td1"  runat="server"  data-title="No" class="center" visible="false" >
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                            <td id="td2"  runat="server"  data-title="Name Surname" ><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                                            <td id="td3"  runat="server"  data-title="Course" ><asp:Label ID="lblCourse" runat="server"></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblProject" runat="server" ForeColor="Blue"></asp:Label> : <asp:Label ID="lblActivity" runat="server" ForeColor="Blue"></asp:Label>
                                                            </td>
                                                            <td id="td4"  runat="server"  runat ="server"  data-title="Start - End Date"><asp:Label ID="lblPeriod" runat="server"></asp:Label></td>
                                                    
                                                            <td id="td5"  runat="server"  data-title="Institute" style="text-align :center ;"><asp:Label ID="lblInstitute" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td id="td6"  runat="server"  data-title="Insurance" style="text-align :center ;" ><asp:Label ID="lblexpired_date_Insurance" runat="server" ForeColor="black"></asp:Label></td>

                                                            <td id="td7"  runat="server"  data-title="VISA" style="text-align :center ;" ><asp:Label ID="lblexpired_date_Visa" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td id="td8"  runat="server"  data-title="Passport" style="text-align :center ;"><asp:Label ID="lblexpired_date_passport" runat="server" ForeColor="black"></asp:Label>
                                                                <a  ID="lnkSelectProject" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>
                                                            </td>

                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                           
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="20" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
     
</asp:Content>





