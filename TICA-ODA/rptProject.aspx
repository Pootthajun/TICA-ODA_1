﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="rptProject.aspx.vb" Inherits="rptProject" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Search | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Search Project </h1>
            <ol class="breadcrumb">
                <li><a href="#" style="cursor: text"><i class="fa fa-university"></i>Report</a></li>
                <li class="active">Search Project</li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">


                                    <div class="box-header">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    <%--<li><a href="#" id="aPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" id="aEXCEL" runat="server"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>--%>

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 30px;"></div>




                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Project Type :</label>
                                                <div class="col-sm-9">
                                                    <asp:DropDownList ID="ddlProject_Type" runat="server" CssClass="form-control select2" Style="width: 100%">
                                                        <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                                                        <asp:ListItem Value="0">Project</asp:ListItem>
                                                        <asp:ListItem Value="1">Non Project</asp:ListItem>
                                                        <asp:ListItem Value="2">Loan</asp:ListItem>
                                                        <asp:ListItem Value="3">Contribuition</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Project :</label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtSearch_Project" runat="server" placeholder="ค้นหาจากรหัสและชื่อโครงการ" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height" width="100px;">Start/End Date:</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder="วันเริ่มต้น" Width="120px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label for="inputname" class="col-sm-2 control-label line-height">To</label></div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder="สิ้นสุดโครงการ" Width="110px"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                            Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height">Balance :</label>
                                                <div class="col-sm-9 ">
                                                    <div class="col-sm-4" style="margin-left: -20px;">
                                                        <label class="input-group">
                                                            <asp:CheckBox ID="ck_Balance_Y" runat="server" CssClass="minimal" Checked="true" />
                                                            <span style="margin-left: 10px;">มี</span></label>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="input-group">
                                                            <asp:CheckBox ID="ck_Balance_N" runat="server" CssClass="minimal" Checked="true" />
                                                            <span style="margin-left: 10px;">ไม่มี</span></label>
                                                    </div>
                                                </div>
                                            </div>




                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="inputname" class="col-sm-3 control-label line-height"></label>
                                                <div class="col-sm-9 ">
                                                    <%--<p class="pull-right" style="margin-right:60px;">--%>
                                                    <p class="pull-right" style="margin-right: 10px;">
                                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn bg-blue margin-r-5 btn-social">
                                                            <i class="fa fa-search"></i>
                                                            <asp:Label ID="Search" runat="server" Text="Search"></asp:Label>
                                                        </asp:LinkButton>
                                                    </p>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="row"></div>
                                        <div class="row pull-left">

                                            <div class="col-md-12">
                                                <h4 class="text-primary" style="margin-left: 20px;">
                                                    <asp:Label ID="lblTotalRecord" runat="server" Text=""></asp:Label></h4>


                                            </div>
                                        </div>
                                        <div class="row pull-right">
                                            <div class="col-md-12" style="margin-right: 30px;">

                                                <h5 style="margin-left: 20px;">Unit : THB</h5>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no" id="th1" runat="server" visible="false">No.<br />
                                                        (ลำดับ)</th>
                                                    <th>Project Type<br />
                                                        (ประเภท)</th>
                                                    <th class="project">Project ID<br />
                                                        (รหัส)</th>
                                                    <th>Description<br />
                                                        (รายละเอียด)</th>

                                                    <th title="งบประมาณใน Activtiy">Budget<br />
                                                        (งบประมาณ)</th>
                                                    <th title="จ่ายจริงใน Expense">Disbursement<br />
                                                        (จ่ายจริง)</th>
                                                    <th title="งบประมาณ - จ่ายจริง">Balance<br />
                                                        (คงเหลือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr >
                                                            <td data-title="No" class="center" id="tr1" runat="server" visible="false">
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                            <td data-title="Project Type" width="120;" id="tdLinkProject1" runat="server" style ="cursor: pointer; " tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblProject_Type" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Project ID" class="center" width="120;" id="tdLinkProject2" runat="server" style ="cursor: pointer; " tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblProjectID" runat="server"></asp:Label></td>
                                                            <td data-title="Description" id="tdLinkProject3" runat="server" style ="cursor: pointer; " tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label><br />
                                                                <asp:Label ID="lblPlanDate" runat="server" ForeColor="Blue"></asp:Label>

                                                            </td>
                                                            <td data-title="Budget" style="text-align: right;cursor: pointer; " width="120;" id="tdLinkProject4" runat="server"  tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblSUM_Budget" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Disbursement" style="text-align: right;cursor: pointer; " width="120;" id="tdLinkProject5" runat="server"  tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblDisbursement" runat="server" ForeColor="black"></asp:Label></td>
                                                            <td data-title="Balance" style="text-align: right;cursor: pointer; " width="120;" id="tdLinkProject6" runat="server"  tooltip="ดูรายละเอียดของโครงการ">
                                                                <asp:Label ID="lblBalance" runat="server" ForeColor="black"></asp:Label>
                                                            
                                                                  <a  ID="lnkSelect" runat="server" ToolTip ="ดูรายละเอียดของโครงการ"  Style="display: none;" target ="_blank"    ></a>

                                                            </td>

                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>
                                            <asp:Panel ID="pnlFooter" runat="server" Visible="True">
                                                <tfoot style="background-color: LemonChiffon;">
                                                    <tr id="trFooter_Qty" runat="server">

                                                        <td data-title="No" class="center" id="tr1" runat="server" visible="false">
                                                            <asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                        <td data-title="Budget Year" style="text-align: center;"><b>Total</b></td>

                                                        <td data-title="Cambodia" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblCambodia_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Laos" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblLaos_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Myanmar" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBudget_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Vietnam" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblDisbursement_Sum" runat="server" ForeColor="black"></asp:Label></b></td>
                                                        <td data-title="Total Amount" style="text-align: right; text-decoration: underline;"><b>
                                                            <asp:Label ID="lblBalance_Sum" runat="server" ForeColor="black"></asp:Label></b></td>


                                                    </tr>


                                                </tfoot>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <%--<asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--<script type="text/javascript">

        function btnEditClick(activity_id) {
            //alert(activity_id);
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickProjectID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>--%>
</asp:Content>


