﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports Constants

Public Class frmMaster
    Inherits System.Web.UI.MasterPage
    Dim BL As New ODAENG
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()

            If Session("UserName") Is Nothing Then
                Response.Redirect("frmLogin.aspx")
                Exit Sub
            End If

            lblName.Text = Session("FullNameTH")
            lblDetail.Text = Session("FullNameTH")

            BindMenu()
        End If

    End Sub
    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode"
        sql += ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS"
        sql += " FROM tb_contactgoverment WHERE id=1"
        Dim trans As New TransactionDB
        'Dim lnq As New WebContactgovermentLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = SqlDB.ExecuteTable(sql, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("id").ToString() <> "" Then
                    lblGOVEN.Text = dt(i)("govnameeng").ToString()
                    lblBuliding.Text = dt(i)("govnumber").ToString()
                    lblRoad.Text = dt(i)("govroad").ToString()
                    lblProvice.Text = dt(i)("province").ToString()

                    lblZipCode.Text = dt(i)("postcode").ToString()
                    lblTelephone.Text = dt(i)("telephone").ToString()

                    lblFax.Text = dt(i)("fax").ToString()
                    lblWebSite.Text = dt(i)("website").ToString()

                    lblEmail.Text = dt(i)("email").ToString()
                    lblMinistry.Text = dt(i)("ministry").ToString()

                End If
            Next

        Else
            trans.RollbackTransaction()
            Dim _err = trans.ErrorMessage()
        End If
    End Sub

    Sub SetVisibleMenuToDefault()
        ''overall
        'mnuProjectStatus.Visible = False

        ''---Tab Allocated Budget---
        'mnuAllocatedBudget.Visible = False 'Budget
        'aBudget.Visible = False 'Incoming Budget
        'aBudgetReport.Visible = False 'Report

        'mnuMasterBudget.Visible = False 'Budget Setting
        'aGroupBudget.Visible = False 'Budget Group
        'aSubBudget.Visible = False 'Sub Budget

        ''---Tab Finance---
        ''Finance | mnuFinance 
        'aProjectExpense.Visible = False 'Expense
        'aExpenseReport.Visible = False 'Report

        ''Setting | mnuMasterExpense
        'aExpense.Visible = False 'Expense Group
        'aSubExpense.Visible = False 'Sub Expense

        ''---Tab Master---
        ''Master | mnuMaster
        ''aPlan.Visible = False 'Plan and policies
        'aCountryGroup.Visible = False 'Country Group
        'aOECD.Visible = False 'OECD
        'aRegionOECD.Visible = False 'OECD Region
        'aCooperationFramework.Visible = False 'Cooperation Framework
        'aCooperationType.Visible = False 'Cooperation Type
        'aSector.Visible = False 'Sector
        'aSubSector.Visible = False 'Sub Sector
        'aComponent.Visible = False 'Component
        'aMultilateral.Visible = False  'Multilateral
        'aInKind.Visible = False 'In Kind

        ''-- Org Structure --
        'mnuStructure.Visible = False

        ''-- Report --
        'mnuReports.Visible = False
    End Sub
    Protected Sub BindMenu()
        SetVisibleMenuToDefault()
        Dim dt As New DataTable
        If Session("Authorize") Is Nothing Then Exit Sub
        dt = CType(Session("Authorize"), DataTable)

        Dim dtmenu As DataTable = BL.GetList_AllMenu()
        For i As Integer = 0 To dtmenu.Rows.Count - 1
            Dim _menu_id As String = dtmenu.Rows(i)("id").ToString
            Dim tmpdr() As DataRow = dt.Select("menu_id = '" & _menu_id & "' and (isnull(is_save,'N') ='Y' or isnull(is_view,'N') ='Y')")
            If tmpdr.Length = 0 Then
                Select Case _menu_id
                    Case Menu.Overall
                        mnuProjectStatus.Visible = False
                    Case Menu.Project
                        aProject.Visible = False
                    Case Menu.Non_Project
                        aNProject.Visible = False
                    Case Menu.Loan
                        aLoan.Visible = False
                    Case Menu.Contribution
                        aContribuition.Visible = False
                    Case Menu.Organize_Structure
                        mnuStructure.Visible = False

                    '================Budget======================
                    Case Menu.Incoming_Budget
                        aBudget.Visible = False
                    Case Menu.Budget_Group
                        aGroupBudget.Visible = False
                    Case Menu.Sub_Budget
                        aSubBudget.Visible = False

                        '================Finance======================
                    Case Menu.Expense
                        aProjectExpense.Visible = False
                    Case Menu.Expense_Group
                        aExpense.Visible = False
                    Case Menu.Sub_Expense
                        aSubExpense.Visible = False
                    Case Menu.Template
                        aTemplate.Visible = False

                     '32  Manage Recipient	Candidate
                    '33  Manage Recipient	Recipient

                     '================Master======================
                    Case Menu.Country_Group
                        aCountryGroup.Visible = False
                    Case Menu.OECD
                        aOECD.Visible = False
                    Case Menu.OECD_Region
                        aRegionOECD.Visible = False
                    Case Menu.Cooperation_Framework
                        aCooperationFramework.Visible = False
                    Case Menu.Cooperation_Type
                        aCooperationType.Visible = False
                    Case Menu.Sector
                        aSector.Visible = False
                    Case Menu.Sub_Sector
                        aSubSector.Visible = False
                    Case Menu.Component
                        aComponent.Visible = False
                    Case Menu.Multilateral
                        aMultilateral.Visible = False
                    Case Menu.In_Kind
                        aInKind.Visible = False
                    Case Menu.OECD_RegionZone
                        aRegionZoneOECD.Visible = False
                    Case Menu.Person
                        aPerson.Visible = False
                    Case Menu.Plan
                        aPlan.Visible = False

                        'Case Menu.Candidate

                        'Case Menu.Recipient

                    Case Menu.Report_Project
                        arptProject.Visible = False
                    Case Menu.CLMU
                        arptCLMU.Visible = False
                    Case Menu.AidSummary
                        arptAidSummary.Visible = False
                    Case Menu.AidByProjectSummary
                        a1rptAidByProjectSummary.Visible = False
                    Case Menu.AidByCountrySummary
                        a2rptAidByCountrySummary.Visible = False
                    Case Menu.Reports_foreign
                        mnuReports_foreign.Visible = False
                    Case Menu.Reports_Admin
                        mnuReports_Admin.Visible = False


                End Select
            End If
        Next

        If aProject.Visible = False And aNProject.Visible = False And aLoan.Visible = False And aContribuition.Visible = False Then
            mnuproject.Visible = False
        End If
        If aBudget.Visible = False And aGroupBudget.Visible = False And aSubBudget.Visible = False Then
            mnuAllocatedBudget.Visible = False
        End If
        If aGroupBudget.Visible = False And aSubBudget.Visible = False Then
            mnuMasterBudget.Visible = False
        End If

        If aProjectExpense.Visible = False And aExpense.Visible = False And aSubExpense.Visible = False And aTemplate.Visible = False Then
            mnuFinance.Visible = False
        End If
        If aExpense.Visible = False And aSubExpense.Visible = False And aTemplate.Visible = False Then
            mnuMasterExpense.Visible = False
        End If

        If aPlan.Visible = False And aCountryGroup.Visible = False And aOECD.Visible = False And aRegionOECD.Visible = False And aRegionZoneOECD.Visible = False And aCooperationFramework.Visible = False _
            And aCooperationType.Visible = False And aSector.Visible = False And aSubSector.Visible = False And aComponent.Visible = False _
            And aMultilateral.Visible = False And aInKind.Visible = False And aPerson.Visible = False Then
            mnuMaster.Visible = False
        End If

        If a1rptAidByProjectSummary.Visible = False And a2rptAidByCountrySummary.Visible = False Then
            mnuComponent.Visible = False
        End If

        If arptProject.Visible = False And arptCLMU.Visible = False And arptAidSummary.Visible = False And mnuComponent.Visible = False And mnuReports_foreign.Visible = False And mnuReports_Admin.Visible = False Then
            mnuReports.Visible = False
        End If

    End Sub
End Class