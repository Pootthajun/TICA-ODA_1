﻿Imports LinqDB.ConnectDB
Imports System.Data
Imports Constants

Public Class frmComponent
    Inherits System.Web.UI.Page

    Public Property AllData As DataTable
        Get
            Try
                Return Session("ComponentData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ComponentData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aComponent")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            BindData()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Component & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnStatusReady As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusReady"), LinkButton)
            Dim btnStatusDisabled As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusDisabled"), LinkButton)
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnStatusReady.Enabled = False
                btnStatusDisabled.Enabled = False
                HeadEdit.Visible = False
                ColEdit.Visible = False
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT id,component_name,detail,allowcatetype,disbursetype,ACTIVE_STATUS FROM tb_component WHERE 1=1 "
        If txtSearch.Text.Trim <> "" Then
            sql &= " and component_name LIKE '%" + txtSearch.Text.Trim() + "%'"
        End If

        If ddlStatus.SelectedValue <> "" Then
            sql &= " and ACTIVE_STATUS LIKE '%" + ddlStatus.SelectedValue + "%'"
        End If
        sql &= " order by component_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "ComponentData"
        Pager.RenderLayout()
        pnlAdSearch.Visible = False
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditComponent.aspx")
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged
        BindData()
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)

    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub

    Protected Sub rptList_ItemDataBound1(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblDisburse As Label = DirectCast(e.Item.FindControl("lblDisburse"), Label)
        Dim lblPayment As Label = DirectCast(e.Item.FindControl("lblPayment"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("component_name").ToString()

        If drv("allowcatetype") = "1" Then
            lblDisburse.Text = "รายคน"
        Else
            lblDisburse.Text = "รายกลุ่ม"
        End If

        If drv("disbursetype") = "1" Then
            lblPayment.Text = "รายคน"
        Else
            lblPayment.Text = "รายกลุ่ม"
        End If
        Dim CheckAT As String = e.Item.DataItem("ACTIVE_STATUS").ToString()

        Dim A As String = e.Item.DataItem("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If




    End Sub
    Protected Sub rptList_ItemCommand1(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditComponent.aspx?id=" & e.CommandArgument & "&mode=edit")
        ElseIf e.CommandName = "View" Then
            Response.Redirect("frmEditComponent.aspx?id=" & e.CommandArgument & "&mode=view")
        ElseIf e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.DeleteComponent(e.CommandArgument)
            If ret.IsSuccess Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllY(e.CommandArgument, "TB_Component")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllN(e.CommandArgument, "TB_Component")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        End If
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        clearFormSearch()
        BindData()
        pnlAdSearch.Visible = False
    End Sub
    Function clearFormSearch()
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
    End Function
End Class