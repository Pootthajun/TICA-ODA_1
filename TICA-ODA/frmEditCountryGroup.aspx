﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditCountryGroup.aspx.vb" Inherits="frmEditCountryGroup" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Country Group | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Country Group (กลุ่มประเทศ)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
            <li><a href="frmCountryGroup.aspx">Country Group</a></li>
            <li class="active"><asp:Label ID="lblEditMode" runat="server" Text="Label"></asp:Label> Country Group</li>
              
          </ol>
        </section>

        <!-- Main content -->
         <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>

        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title"><asp:Label ID="lblEditMode2" runat="server" Text="Label"></asp:Label> 
                      Country Group</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Country Group<br /> (กลุ่มประเทศ) : 
                          <span style="color:red">*</span> </label>
                      <div class="col-sm-4">
                        
                          <asp:TextBox ID="txtName" runat="server" Width="600px" CssClass="form-control" MaxLength="255"></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Description<br /> (รายละเอียด) :</label>
                      <div class="col-sm-9">
                        
                          <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px" Width="600px" MaxLength="255"></asp:TextBox>
                      </div>
                    </div>
              <!-- /.box -->
                                        <div class="clearfix"></div>
                       <br />
                      
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                      <div class="col-sm-9">
                     <label><asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" checked="true"/></label>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                   <div class="col-sm-9"></div>
                    <div class="col-lg-8"></div><div class="col-lg-2">
                      
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-block btn-social btn-success" OnClick="btnSave_Click">
                            <i class="fa fa-save"></i> Save
                        </asp:LinkButton>
                                                </div>
                    <div class="col-lg-2">
                        <asp:LinkButton ID="btnCancle" runat="server" CssClass="btn btn-block btn-social btn-google" OnClick="btnCancle_Click">
                            <i class="fa fa-reply"></i> Cancel
                        </asp:LinkButton>
                    </div>
                  </div><!-- /.box-footer -->

             
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
                 </ContentTemplate>
        </asp:UpdatePanel>
    </div>

<!----------------Page Advance---------------------->
  <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>
