﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmExpense.aspx.vb" Inherits="frmExpense" %>
<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <title>Expense | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Expense (ประเภทค่าใช้จ่าย)
            </h1>
            <ol class="breadcrumb"> 
                <li><a href="frmProjectExpense.aspx"><i class="fa fa-calculator"></i>Finance</a></li>
                <li class="active">Expense</li>
            </ol>
        </section>
        <br />
         
        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClientClick="btnAddClick(); return false;">
                                                <i class="fa fa-plus"></i>
                                                New Expense 
                                            </asp:LinkButton>
                                        </p>
                                        
                                    </div>

                                    <div class="box-header">
                                         <div class="form-group">
                                             <label for="inputname" class="col-sm-2 control-label line-height">ประเภทโครงการ :</label>
                                             <div class="col-sm-2">
                                                 <asp:DropDownList ID="ddlProjectType" runat="server" AutoPostBack="true" CssClass="form-control select2" Style="width: 170px">
                                                 </asp:DropDownList>
                                             </div>
                                             <%--<label for="inputname" class="col-sm-1 control-label">ครั้งที่ :</label>
                                             <div class="col-sm-2">
                                                  <asp:DropDownList ID="ddlPlanYear" runat="server" CssClass="form-control select2" Style="width: 150px">
                                                 </asp:DropDownList>
                                             </div>--%>
                                               <div class="col-sm-3">
                                               </div>
                                             <div class="col-lg-4">
                                                 <div class="input-group">
                                                     <div class="col-sm-10 ">
                                                         <asp:TextBox ID="txtSearch" runat="server" placeholder="ค้นหาจากรหัสและชือโครงการ" CssClass="form-control" Width="220px"></asp:TextBox>
                                                     </div>

                                                     <div class="col-sm-1">
                                                         <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">                             
                                                         </asp:LinkButton>
                                                     </div>
                                                 </div>
                                                 <!-- /input-group -->
                                             </div>
                                             <div class="col-lg-1">
                                                 <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                     <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                                         <i class="fa fa-print"></i>
                                                     </button>
                                                     <ul class="dropdown-menu pull-right">
                                                         <li><a href="#" id="aPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                                         <li class="divider"></li>
                                                         <li><a href="#" id="aEXCEL" runat="server"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                                     </ul>
                                                 </div>
                                             </div>
                                         </div>
                                    </div>

                                    <div class="box-header">
                                    <div class="col-md-8">
                                            <h4 class="text-primary">พบทั้งหมด :
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                                รายการ</h4>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                   <%-- <th>ลำดับ</th>--%>
                                                    <th class="project">Project ID </br>(รหัสโครงการ)</th>
                                                    <th style="width:450px">Project Name </br>(ชื่อโครงการ)</th>
                                                    <th class="no">Records </br>(ครั้งที่)</th>
                                                    <th>Payment Date </br>(วันที่จ่าย)</th>
                                                    <%--<th style="width:150px">ผู้รับ</th>--%>
                                                    <th>Amount </br>(จำนวนเงิน)</th>
                                                    <th class="tools">tools </br>(เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <%--<td data-title="No"  style="text-align: center">
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label></td>--%>
                                                            <td data-title="Project ID"  style="text-align: center">
                                                                <asp:Label ID="lblProjectID" runat="server"></asp:Label></td>
                                                            <td data-title="Project Name"  style="text-align: left;">
                                                                <asp:Label ID="lblProjectName" runat="server"></asp:Label></td>
                                                            <td data-title="Record"  style="text-align: center">
                                                                <asp:Label ID="lblrecord" runat="server"></asp:Label></td>
                                                            <td data-title="Payment Date" id="td" runat="server" class="date">
                                                                <asp:Label ID="lblPaymentDate" runat="server"></asp:Label>
                                                            </td>
                                                            <%-- <td data-title="Recived By"  style="text-align: right">
                                                                <asp:Label ID="lblRecievedBy" runat="server"></asp:Label></td>--%>
                                                            <td data-title="Expense"  style="text-align: right">
                                                                <asp:Label ID="lblExpense" runat="server"></asp:Label></td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="center">
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <%--<li><a href="javascript:;" onclick='btnAddClick(<%#Eval("ID") %>,<%#Eval("project_id") %>)'><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>--%>
                                                                        <li id="liview" runat="server"  Visible ="false" ><a href="javascript:;" onclick='btnViewClick(<%#Eval("ID") %>,<%#Eval("project_id") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>
                                                                        <li id="liedit" runat="server"><a href="javascript:;" onclick='btnEditClick(<%#Eval("ID") %>,<%#Eval("project_id") %>)'><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                                                                        <li class="divider"></li>
                                                                        <li id="lidelete" runat="server"><a href="javascript:;" onclick='btnDeleteClick(<%#Eval("ID") %>)'><i class="fa fa-remove text-red"></i>ลบ</a></li>
                                                                    </ul>
                                                                </div>
                                                             </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    
                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                     <asp:TextBox ID="txtClickProjectID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:TextBox ID="txtClickID" runat="server" Text="" Style="display: none"></asp:TextBox>
                     <asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                     <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <ContentTemplate>
                 <asp:Button ID="btnServerModal" runat="server" Text="OpenJSButton" Style="display: none"></asp:Button>
                 <asp:Panel CssClass="modal" ID="pnlDialog" runat="server" Visible="false" Style="display: block; padding-right: 17px;">
                     <div class="modal-dialog" style="padding-left: 4%;">
                         <!-- Modal content-->
                         <div class="modal-content" style="width: 850px;">
                             <div class="modal-header">
                                 <!-- Custom Tabs -->
                                 <div class="nav-tabs-custom">
                                     <div style="padding: 0.9375rem; min-height: 450px; overflow-y: auto;">
                                         
                                         <table class="table table-bordered">
                                             <tr class="bg-info">
                                                 <th style="width: 150px">
                                                     <h5><b class="text-blue">Select Project (เลือกโครงการ) </b></h5>
                                                 </th>
                                             </tr>
                                             <tr>
                                                 <td>
                                                     <div class="col-sm-12">
                                                         <div class="col-sm-7">
                                                             <div class="form-group">
                                                                 <label for="inputname" class="col-sm-5 control-label">ประเภทโครงการ :</label>
                                                                 <div class="col-sm-2">
                                                                     <asp:DropDownList ID="ddlProjectTypeDialog" runat="server" AutoPostBack="true" CssClass="form-control select2" Style="width: 170px">
                                                                     </asp:DropDownList>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-sm-5">
                                                             <div class="input-group">
                                                                 <div class="col-sm-10 ">
                                                                     <asp:TextBox ID="txtSearchProject" runat="server" placeholder="ค้นหาจากรหัสและชื่อโครงการ" CssClass="form-control" Width="220px"></asp:TextBox>
                                                                 </div>

                                                                 <div class="col-sm-1">
                                                                     <asp:LinkButton ID="btnSearchProject" runat="server" Text="" CssClass="fa fa-search">                             
                                                                     </asp:LinkButton>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </td>
                                             </tr>
                                         </table>

                                         <asp:Panel ID="pnlProjectList" runat="server">
                                            <div class="box-body" style="padding: 0px; ">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th class="no">No.(ลำดับ)</th>
                                                    <th class="project">Project ID <br>(รหัสโครงการ)</th>
                                                    <th>Description <br>(รายละเอียด)</th>
                                                    <th class="tools">Tool <br>(เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptProject" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-title="No">
                                                                <center>
                                                                <asp:Label ID="lblNo" runat="server" Text='<%# Eval("seq") %>'></asp:Label></center></td>
                                                            <td data-title="Project ID">
                                                                <asp:Label ID="lblProjectID" runat="server" Text='<%# Eval("project_id") %>'></asp:Label></td>
                                                            <td data-title="Description" id="td" runat="server">
                                                                <asp:Label ID="lblProjectTitle" runat="server" Text='<%# Eval("project_name") %>'></asp:Label>
                                                            </td>
                                                            <td data-title="Select" id="ColSelect" runat="server">

                                                                <center>
                                                                  <asp:LinkButton ID="btnSelect" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="Select"><i class="fa fa-check text-primary"></i> Select</asp:LinkButton>
                                                                </center>

                                                                <%--<div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li><a href="javascript:;" onclick='btnSelectClick(<%#Eval("ID") %>)'><i class="fa fa-search text-blue"></i>เลือก</a></li>
                                                                    </ul>
                                                                </div>--%>
                                                             </td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>                                    
                                  </asp:Panel>                                 
                             </div>
                                 </div>

                                 <div class="modal-footer">
                                    <asp:LinkButton ID="btnClose" runat="server" CssClass="btn  btn-social btn-google" OnClick="btnCancelAct_Click">
                                    <i class="fa fa-reply"></i> Cancel
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                 </asp:Panel>
                  <iframe id="myframe" src="" frameborder="0" height="0px" width="0px" runat="server"></iframe>
             </ContentTemplate>
         </asp:UpdatePanel>


        <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>

            


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <script type="text/javascript">
        <%--function btnSelectClick(project_id) {
            document.getElementById('<%= txtClickID.ClientID %>').value = 0;
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = project_id;
            document.getElementById('<%= btnSelectProject.ClientID %>').click();
        }--%>
        function btnAddClick() {
            <%-- document.getElementById('<%= txtClickProjectID.ClientID %>').value = project_id;
            document.getElementById('<%= txtClickID.ClientID %>').value = expense_id;--%>
            document.getElementById('<%= btnServerModal.ClientID %>').click();
        }
        function btnEditClick(expense_id,project_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = project_id;
            document.getElementById('<%= txtClickID.ClientID %>').value = expense_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(expense_id, project_id) {
            document.getElementById('<%= txtClickProjectID.ClientID %>').value = project_id;
            document.getElementById('<%= txtClickID.ClientID %>').value = expense_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(expense_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickID.ClientID %>').value = expense_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>
</asp:Content>


