﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmTestLinq.aspx.vb" Inherits="frmTestLinq" %>



<%@ Register src="usercontrol/UCActivityTreeList.ascx" tagname="UCActivityTreeList" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <br /><br />

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Test Linq DB
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> </a></li>
                <li class="active"></li>
              </ol>
            </section>
            <!-- Main content -->
            <section class="content"><br />
                Test Table Treeview 
                
                <br />

                <uc1:UCActivityTreeList ID="UCActivityTreeList1" runat="server" />
                <table >
                    <tr>
                        <td>Node ID</td>
                        <td><asp:TextBox ID="txtNodeID" runat="server" ></asp:TextBox> </td>
                    </tr>
                    <tr>
                        <td>Node Name</td>
                        <td><asp:TextBox ID="txtNodeName" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Parent ID</td>
                        <td><asp:TextBox ID="txtParentID" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><asp:CheckBox ID="chkExpand" runat="server" ></asp:CheckBox></td>
                    </tr>
                </table>


                <asp:Button ID="btnAddNode" runat="server" Text="Add Node" ></asp:Button>
                
                <asp:Button ID="btnGetCheckNode" runat="server" Text="Get Check Node" ></asp:Button>
                <asp:Button ID="btnSetCheckNode" runat="server" Text="Set Check Node" ></asp:Button> 
                <br />
                <asp:Label ID="lblNavigateNodeText" runat="server"></asp:Label>
                
                
                <br />
                <br />
                <br />
                <asp:Button ID="btnGetSelectedNode" runat="server" Text="Get Selected Node" ></asp:Button>
                <asp:Button ID="btnSetSelectedNode" runat="server" Text="Set Selected Node" ></asp:Button>


                <br />
                <br />
                <br />
                <asp:Button ID="btnInsertData" runat="server" Text="Insert Data" ></asp:Button>
                <asp:Button ID="btnGetDataList" runat="server" Text="Get Data List"  ></asp:Button>
                <asp:Button ID="btnGetListBySql" runat="server" Text="Get List By Sql"  ></asp:Button>
                <asp:Button ID="btnUpdateData" runat="server" Text="Update Data"  ></asp:Button>
                <asp:Button ID="btnDeleteData" runat="server" Text="Delete Data"  ></asp:Button>
                <br />





                
            </section>



            


            <asp:UpdatePanel ID="udpList" runat="server" ClientIDMode="Static">
                <ContentTemplate>
                    <div class="box-body">
                        
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>




</asp:Content>


