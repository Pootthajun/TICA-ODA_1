﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Partial Class frmEditMultilateral
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Protected ReadOnly Property UserName As String
        Get
            Try
                Return Session("UserName")
            Catch ex As Exception
                Return "Administrator"
            End Try
        End Get
    End Property

    Public Property EditMultilateralID As Long
        Get
            Try
                Return ViewState("MultilateralID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("MultilateralID") = value
        End Set
    End Property

    Private Sub frmEditMultilateral_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aMultilateral")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            txtNames.Focus()

            If Not Request.QueryString("ID") Is Nothing Then
                Try
                    EditMultilateralID = CInt(Request.QueryString("ID"))
                Catch ex As Exception
                    EditMultilateralID = 0
                End Try

            End If

            GetMultilateralInfoForEdit()
            Authorize()
        End If
    End Sub

    Sub Authorize()
        Dim mode As String = CType(Request.QueryString("mode"), String)
        If mode = "view" Then
            txtNames.Enabled = False
            chkACTIVE_STATUS.Enabled = False
            btnSave.Visible = False
        End If
    End Sub

    Sub GetMultilateralInfoForEdit()
        ClearEditForm()

        If EditMultilateralID > 0 Then
            Dim dt As New DataTable
            dt = BL.GetList_Multilateral(EditMultilateralID, "", "")
            If dt.Rows.Count > 0 Then
                txtNames.Text = dt.Rows(0)("Names").ToString()
                txtDescription.Text = dt.Rows(0)("Detail").ToString()

                If dt.Rows(0)("active_status").ToString() = "Y" Then
                    chkACTIVE_STATUS.Checked = True
                Else
                    chkACTIVE_STATUS.Checked = False
                End If
            End If

            lblEditMode.Text = "Edit "
            lblEditMode2.Text = "Edit "
        Else
            lblEditMode.Text = "Add "
            lblEditMode2.Text = "Add "
        End If
    End Sub

    Private Sub ClearEditForm()
        txtNames.Text = ""
        txtDescription.Text = ""
        chkACTIVE_STATUS.Checked = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Validate() Then

            Dim trans As New TransactionDB
            Try
                Dim lnqLoc As New TbMultilateralLinqDB
                Dim chkDupName As Boolean = lnqLoc.ChkDuplicateByMultilateral_NAME(txtNames.Text.Trim, EditMultilateralID, Nothing)
                If chkDupName = True Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถบันทึกข้อมูลได้ข้อมูลซ้ำ!');", True)
                    Exit Sub
                End If
                lnqLoc.ChkDataByPK(EditMultilateralID, trans.Trans)
                With lnqLoc
                    .NAMES = txtNames.Text.Replace("'", "''")
                    .DETAIL = txtDescription.Text.Replace("'", "''")
                    .ACTIVE_STATUS = IIf(chkACTIVE_STATUS.Checked, "Y", "N")
                End With

                Dim ret As New ExecuteDataInfo
                If lnqLoc.ID = 0 Then
                    ret = lnqLoc.InsertData(UserName, trans.Trans)
                Else
                    ret = lnqLoc.UpdateData(UserName, trans.Trans)
                End If
                If ret.IsSuccess = True Then
                    trans.CommitTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "alert", "alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location ='frmMultilateral.aspx';", True)

                Else
                    trans.RollbackTransaction()
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage.Replace("'", """") & "');", True)
                End If
            Catch ex As Exception
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ex.Message.Replace("'", """") & "');", True)
            End Try
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtNames.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุ พหุภาคี');", True)
            ret = False
        End If

        Return ret
    End Function

    Private Sub btnCancle_Click(sender As Object, e As System.EventArgs) Handles btnCancle.Click
        Response.Redirect("frmMultilateral.aspx")
    End Sub

End Class

