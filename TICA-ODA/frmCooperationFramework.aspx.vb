﻿Imports System.Data
Imports LinqDB.ConnectDB
Imports Constants

Public Class frmCooperationFramework
    Inherits System.Web.UI.Page

    Public Property AllData As DataTable
        Get
            Try
                Return Session("CopperationFrameworkData")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("CopperationFrameworkData") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuMaster")
        li.Attributes.Add("class", "active")

        Dim a As HtmlAnchor = Me.Page.Master.FindControl("aCooperationFramework")
        a.Attributes.Add("style", "color:#FF8000")

        If IsPostBack = False Then
            BindData()
            Authorize()
            pnlAdSearch.Visible = False
        End If
    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        Dim tmpdr() As DataRow = AuthorizeDT.Select("menu_id = '" & Menu.Cooperation_Framework & "' and isnull(is_save,'N') ='Y'")

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
            HeadColDelete.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim btnStatusReady As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusReady"), LinkButton)
            Dim btnStatusDisabled As LinkButton = DirectCast(rptList.Items(i).FindControl("btnStatusDisabled"), LinkButton)
            Dim ColEdit As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColEdit"), HtmlTableCell)
            Dim ColDelete As HtmlTableCell = DirectCast(rptList.Items(i).FindControl("ColDelete"), HtmlTableCell)

            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                btnStatusReady.Enabled = False
                btnStatusDisabled.Enabled = False
                HeadEdit.Visible = False
                ColEdit.Visible = False
                ColDelete.Visible = False
            End If
        Next
    End Sub

    Protected Sub BindData()
        Dim sql As String = "SELECT id,fwork_name,detail,detail_th,ACTIVE_STATUS FROM tb_coperationframework WHERE 1=1"
        If txtSearch.Text.Trim <> "" Then
            sql &= " and fwork_name LIKE '%" + txtSearch.Text.Trim() + "%' OR detail LIKE '%" + txtSearch.Text.Trim() + "%' OR detail_th LIKE '%" + txtSearch.Text.Trim() + "%'"
        End If

        If ddlStatus.SelectedValue <> "" Then
            sql &= " And ACTIVE_STATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
        End If
        sql &= " order by fwork_name"
        Dim dt As DataTable = SqlDB.ExecuteTable(sql)
        rptList.DataSource = dt
        rptList.DataBind()
        lblCount.Text = dt.DefaultView.Count

        AllData = dt
        Pager.SesssionSourceName = "CopperationFrameworkData"
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Response.Redirect("frmEditCooperationFramework.aspx")
    End Sub
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblID As Label = DirectCast(e.Item.FindControl("lblID"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblDetail As Label = DirectCast(e.Item.FindControl("lblDetail"), Label)
        Dim lblDetialTh As Label = DirectCast(e.Item.FindControl("lblDetialTh"), Label)
        Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
        Dim btnStatusReady As LinkButton = DirectCast(e.Item.FindControl("btnStatusReady"), LinkButton)
        Dim btnStatusDisabled As LinkButton = DirectCast(e.Item.FindControl("btnStatusDisabled"), LinkButton)

        lblID.Text = e.Item.DataItem("id").ToString
        lblName.Text = drv("fwork_name").ToString()
        lblDetail.Text = drv("detail").ToString()
        lblDetialTh.Text = drv("detail_th").ToString()

        Dim A As String = drv("ACTIVE_STATUS").ToString
        If A = "Y" Then
            btnStatusReady.Visible = True
            btnStatusDisabled.Visible = False
        ElseIf A = "N" Then
            btnStatusDisabled.Visible = True
            btnStatusReady.Visible = False
        End If

    End Sub
    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        If e.CommandName = "cmdEdit" Then
            Response.Redirect("frmEditCooperationFramework.aspx?ID=" & e.CommandArgument)
        End If

        If e.CommandName = "cmdDelete" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.DeleteCooperationFramework(e.CommandArgument)
            If ret.IsSuccess Then
                Alert("ลบข้อมูลเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusready" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllY(e.CommandArgument, "TB_CoperationFramework")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        ElseIf e.CommandName = "cmdstatusDisabled" Then
            Dim BL As New ODAENG
            Dim ret As ProcessReturnInfo
            ret = BL.EditStatusAllN(e.CommandArgument, "TB_CoperationFramework")
            If ret.IsSuccess Then
                Alert("แก้ไขสถานะเรียบร้อยแล้ว")
                BindData()
            Else
                Alert(ret.ErrorMessage)
            End If
        End If

    End Sub
    Protected Sub Alert(ByVal message As String)
        ScriptManager.RegisterStartupScript(Me.Page, Page.[GetType](), "err_msg", (Convert.ToString("alert('") & message) + "');", True)
    End Sub


    Protected Sub btnSearch_Click1(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        clearFormSearch()
        BindData()
        pnlAdSearch.Visible = False
    End Sub
    Function clearFormSearch()
        txtSearch.Text = ""
        ddlStatus.SelectedValue = ""
    End Function
End Class