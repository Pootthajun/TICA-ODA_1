﻿Imports System.Data
Imports System.Globalization
Imports Constants


Partial Class frmProject
    Inherits System.Web.UI.Page
    Dim BL As New ODAENG

    Public Property ProjectType As Long
        Get
            Try
                Return ViewState("ProjectType")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Long)
            ViewState("ProjectType") = value
        End Set
    End Property

    Public Property AllData As DataTable
        Get
            Try
                Return Session("ProjectPage")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("ProjectPage") = value
        End Set
    End Property

    Protected ReadOnly Property AuthorizeDT As DataTable
        Get
            Try
                Return CType(Session("Authorize"), DataTable)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuproject")
        li.Attributes.Add("class", "active")

        If Not IsPostBack Then
            ProjectType = CInt(Request.QueryString("type"))

            lblProjectType.Text = ""
            lblAddProjectType.Text = ""
            lblHeadProjectType.Text = ""
            Dim strProject As String = ""

            If ProjectType = Constants.Project_Type.Project Then
                strProject = "Project"
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aProject")
                a.Attributes.Add("style", "color:#FF8000")
            End If
            If ProjectType = Constants.Project_Type.NonProject Then
                strProject = "Non Project"
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aNProject")
                a.Attributes.Add("style", "color:#FF8000")
            End If
            If ProjectType = Constants.Project_Type.Loan Then
                strProject = "Loan"
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aLoan")
                a.Attributes.Add("style", "color:#FF8000")
            End If
            If ProjectType = Constants.Project_Type.Contribuition Then
                strProject = "Contribuition"
                Dim a As HtmlAnchor = Me.Page.Master.FindControl("aContribuition")
                a.Attributes.Add("style", "color:#FF8000")
            End If

            lblProjectType.Text = strProject
            lblHeadProjectType.Text = strProject
            lblAddProjectType.Text = "Add " & strProject

            BindList()
            Authorize()
            TxtSearchBudgetStart_TextChanged(Nothing, Nothing)
            TxtSearchBudgetEnd_TextChanged(Nothing, Nothing)
            pnlAdSearch.Visible = False

        End If

    End Sub

    Sub Authorize()
        If AuthorizeDT Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        ProjectType = CInt(Request.QueryString("type"))
        Dim tmpdr() As DataRow
        If ProjectType = Constants.Project_Type.Project Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Project & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.NonProject Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Non_Project & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.Loan Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Loan & "' and isnull(is_save,'N') ='Y'")
        End If
        If ProjectType = Constants.Project_Type.Contribuition Then
            tmpdr = AuthorizeDT.Select("menu_id = '" & Menu.Contribution & "' and isnull(is_save,'N') ='Y'")
        End If

        If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
            btnAdd.Visible = False
        End If

        For i As Integer = 0 To rptList.Items.Count - 1
            Dim liview As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liview"), HtmlGenericControl)
            Dim liedit As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("liedit"), HtmlGenericControl)
            Dim lidelete As HtmlGenericControl = DirectCast(rptList.Items(i).FindControl("lidelete"), HtmlGenericControl)

            liview.Visible = False
            If tmpdr IsNot Nothing AndAlso tmpdr.Length = 0 Then
                liview.Visible = True
                liedit.Visible = False
                lidelete.Visible = False
            End If
        Next

    End Sub

    Private Sub BindList()

        Dim DT As DataTable = BL.GetProjectList(ProjectType, txtSearch.Text.Trim().Replace("'", ""))

        rptList.DataSource = DT
        rptList.DataBind()
        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Budget)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Disbursement)", "")).ToString("#,##0.00")
            lblBalance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If
        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "ProjectPage"
        Pager.RenderLayout()


    End Sub
    Public Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click, txtSearch.TextChanged 'test
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Validate() Then
                Dim date1 As DateTime = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim StartDate As String = date1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                Dim date2 As DateTime = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                Dim EndDate As String = date2.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

                BindListForAdSearch(TxtSearchID.Text, txtSearch.Text.Trim().Replace("'", ""), "", "", StartDate, EndDate, TxtserachCountry.Text,ddlStatus.SelectedValue )
                pnlAdSearch.Visible = False
                If TxtSearchBudgetStart.Text <> "" And TxtSearchBudgetEnd.Text <> "" Then
                    If ValiBudget() Then
                        Dim test1 As Integer = TxtSearchBudgetStart.Text
                        Dim test2 As Integer = TxtSearchBudgetEnd.Text

                        BindListForAdSearch(TxtSearchID.Text, txtSearch.Text.Trim().Replace("'", ""), test1.ToString(), test2.ToString(), StartDate, EndDate, TxtserachCountry.Text, ddlStatus.SelectedValue)
                        pnlAdSearch.Visible = False
                    End If
                End If
            End If

        ElseIf TxtSearchBudgetStart.Text <> "" And TxtSearchBudgetEnd.Text <> "" Then
            If ValiBudget() Then
                Dim test1 As Integer = TxtSearchBudgetStart.Text
                Dim test2 As Integer = TxtSearchBudgetEnd.Text
                BindListForAdSearch(TxtSearchID.Text, txtSearch.Text.Trim().Replace("'", ""), test1.ToString(), test2.ToString(), "", "", TxtserachCountry.Text, ddlStatus.SelectedValue)
                pnlAdSearch.Visible = False
            End If
        Else
            BindListForAdSearch(TxtSearchID.Text, txtSearch.Text.Trim().Replace("'", ""), "", "", "", "", TxtserachCountry.Text, ddlStatus.SelectedValue)
            pnlAdSearch.Visible = False
        End If
    End Sub

    Private Sub BindListForAdSearch(code As String, Pl As String, Bs As String, Be As String, Ds As String, De As String, Bc As String, status As String) ' test

        Dim DT As DataTable = BL.GetProjectListforAdSearch(ProjectType, code, Pl, Bs, Be, Ds, De, Bc, status)

        rptList.DataSource = DT
        rptList.DataBind()
        If (DT.Rows.Count > 0) Then
            lblBudget_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Budget)", "")).ToString("#,##0.00")
            lblDisbursement_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Disbursement)", "")).ToString("#,##0.00")
            lblBalance_Sum.Text = Convert.ToDecimal(DT.Compute("SUM(Balance)", "")).ToString("#,##0.00")
            pnlFooter.Visible = True
        Else
            pnlFooter.Visible = False
        End If
        lblTotalList.Text = DT.Rows.Count

        AllData = DT
        Pager.SesssionSourceName = "ProjectPage"
        Pager.RenderLayout()


    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblProjectID As Label = DirectCast(e.Item.FindControl("lblProjectID"), Label)
        Dim lblDescription As Label = DirectCast(e.Item.FindControl("lblDescription"), Label)
        Dim lblPlanDate As Label = DirectCast(e.Item.FindControl("lblPlanDate"), Label)
        Dim lblSUM_Budget As Label = DirectCast(e.Item.FindControl("lblSUM_Budget"), Label)
        Dim lblDisbursement As Label = DirectCast(e.Item.FindControl("lblDisbursement"), Label)
        Dim lblBalance As Label = DirectCast(e.Item.FindControl("lblBalance"), Label)


        lblProjectID.Text = e.Item.DataItem("project_id").ToString
        lblDescription.Text = e.Item.DataItem("project_name").ToString
        lblPlanDate.Text = "Start/End Date:" & e.Item.DataItem("start_date_th").ToString & "-" & e.Item.DataItem("end_date_th").ToString

        If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then
            If Convert.IsDBNull(e.Item.DataItem("Budget")) = False Then
                lblSUM_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Budget")).ToString("#,##0.00")
            End If
        End If

        If ProjectType = Constants.Project_Type.Loan Then
            If Convert.IsDBNull(e.Item.DataItem("Budget")) = False Then
                lblSUM_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Budget")).ToString("#,##0.00")
            End If
        End If

        If ProjectType = Constants.Project_Type.Contribuition Then
            lblSUM_Budget.Text = ""

        End If


        If Convert.IsDBNull(e.Item.DataItem("Budget")) = False Then
            lblSUM_Budget.Text = Convert.ToDecimal(e.Item.DataItem("Budget")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Disbursement")) = False Then
            lblDisbursement.Text = Convert.ToDecimal(e.Item.DataItem("Disbursement")).ToString("#,##0.00")
        End If
        If Convert.IsDBNull(e.Item.DataItem("Balance")) = False Then
            lblBalance.Text = Convert.ToDecimal(e.Item.DataItem("Balance")).ToString("#,##0.00")
        End If


    End Sub



    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then
            Response.Redirect("frmProjectDetail.aspx?type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Loan Then
            Response.Redirect("frmLoanDetail.aspx?type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Contribuition Then
            Response.Redirect("frmContribuiltionDetail.aspx?type=" & ProjectType.ToString())
        End If
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

    End Sub
    Protected Sub btnView_Click(sender As Object, e As EventArgs)
        Dim _project_id As String = txtClickProjectID.Text
        If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then
            Response.Redirect("frmProjectDetail.aspx?id=" & _project_id & " &mode=" & "view&type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Loan Then
            Response.Redirect("frmLoanDetail.aspx?id=" & _project_id & " &mode=" & "view&type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Contribuition Then
            Response.Redirect("frmContribuiltionDetail.aspx?id=" & _project_id & " &mode=" & "view&type=" & ProjectType.ToString())
        End If
    End Sub
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim _project_id As String = txtClickProjectID.Text
        If ProjectType = Constants.Project_Type.Project Or ProjectType = Constants.Project_Type.NonProject Then
            Response.Redirect("frmProjectDetail.aspx?id=" & _project_id & " &mode=" & "edit&type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Loan Then
            Response.Redirect("frmLoanDetail.aspx?id=" & _project_id & " &mode=" & "edit&type=" & ProjectType.ToString())
        End If
        If ProjectType = Constants.Project_Type.Contribuition Then
            Response.Redirect("frmContribuiltionDetail.aspx?id=" & _project_id & " &mode=" & "edit&type=" & ProjectType.ToString())
        End If
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim _project_id As String = txtClickProjectID.Text
        Dim ret As New ProcessReturnInfo
        ret = BL.DeleteProject(_project_id)
        If ret.IsSuccess = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('ไม่สามารถลบข้อมูลได้');", True)
        Else
            BindList()
        End If
    End Sub

    Private Function Validate() As Boolean
        Dim ret As Boolean = True
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            Try
                Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy")
                Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy")
            Catch ex As Exception
                alertmsg("กรุณาตรวจสอบวันที่")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function
            End Try
        End If

        If Converter.StringToDate(txtStartDate.Text.Trim, "dd/MM/yyyy") > Converter.StringToDate(txtEndDate.Text.Trim, "dd/MM/yyyy") Then
            alertmsg("กรุณาระบุวันที่เริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
            ret = False
            pnlAdSearch.Visible = True
            Exit Function
        End If

        Return ret
    End Function

    Private Function ValiBudget() As Boolean
        Dim ret As Boolean = True

        If TxtSearchBudgetStart.Text <> "" And TxtSearchBudgetEnd.Text <> "" Then
            Dim test1 As Integer = TxtSearchBudgetStart.Text
            Dim test2 As Integer = TxtSearchBudgetEnd.Text

            If test1 > test2 Then
                alertmsg("กรุณาระบุงบประมาณเริ่มต้นและสิ้นสุดให้สอดคล้องกัน")
                ret = False
                pnlAdSearch.Visible = True
                Exit Function

            End If
        End If
        Return ret
    End Function

    Private Sub alertmsg(msg As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & msg & "');", True)
    End Sub

    Private Sub TxtSearchBudgetStart_TextChanged(sender As Object, e As EventArgs) Handles TxtSearchBudgetStart.TextChanged
        BL.SetTextDblKeypress(TxtSearchBudgetStart)
    End Sub

    Private Sub TxtSearchBudgetEnd_TextChanged(sender As Object, e As EventArgs) Handles TxtSearchBudgetEnd.TextChanged
        BL.SetTextDblKeypress(TxtSearchBudgetEnd)
    End Sub

    Private Sub btnOpenSearch_Click(sender As Object, e As EventArgs) Handles btnOpenSearch.Click
        pnlAdSearch.Visible = True
    End Sub

    Private Sub btnCancle_Click(sender As Object, e As EventArgs) Handles btnCancle.Click
        pnlAdSearch.Visible = False
        clearFormSearch()
        btnSearch_Click(Nothing, Nothing)
    End Sub

    Function clearFormSearch()
        TxtSearchID.Text = ""
        txtSearch.Text = ""
        TxtSearchBudgetStart.Text = ""
        TxtSearchBudgetEnd.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        TxtserachCountry.Text = ""
        ddlStatus.SelectedValue = ""
    End Function
End Class
