﻿<%@ Page Title="" Language="VB" MasterPageFile="~/frmMaster.Master" AutoEventWireup="false" CodeFile="frmBudget.aspx.vb" Inherits="frmBudget1" %>

<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>Incoming Budget | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Incoming Budget (งบประมาณที่ได้รับ)  
            </h1>
            <ol class="breadcrumb">
                <li><a href="frmBudget.aspx"><i class="fa fa-dollar"></i>Budget</a></li>
                <li class="active">Incoming Budget </li>
            </ol>
        </section>
        <br />


        <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Height="1000px" ScrollBars="Auto" DefaultButton="btnSearch">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <p>
                                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn bg-orange margin-r-5 btn-social" OnClick="btnAdd_Click">
                                                <i class="fa fa-plus"></i>
                                                Add Incoming Budget 
                                            </asp:LinkButton>
                                            <asp:Button ID="btnOpenSearch" runat="server" class="btn btn-info" Text="Advance Search" />
                                        </p>

                                        <asp:Panel ID="pnlAdSearch" runat="server">
                                            <br />
                                            <div class="col-sm-1">
                                            </div>

                                            <table>
                                                <tr>
                                                    <td>Budget Years :</td>
                                                    <td colspan="4">
                                                        <asp:DropDownList ID="ddlBudgetYear" runat="server" AutoPostBack="true" CssClass="form-control select2" Style="width: 100px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Budget Group :</td>
                                                    <td colspan="4" height="50">
                                                        <asp:DropDownList ID="ddlGroupBudget" runat="server" CssClass="form-control select2" Width="100%" AutoPostBack="true"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" height="50">Received Date: </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtStartDate" runat="server" placeholder=""></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtStartDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>

                                                    </td>
                                                    <td width="50px" style="text-align: center">- </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <asp:TextBox CssClass="form-control m-b" ID="txtEndDate" runat="server" placeholder=""></asp:TextBox>
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                                Format="dd/MM/yyyy" TargetControlID="txtEndDate" PopupPosition="BottomLeft"></ajaxToolkit:CalendarExtender>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="3">
                                                        <asp:LinkButton ID="btnSearch" runat="server" Text="" CssClass="fa fa-search">ค้นหา</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="btnCancle" runat="server" Text="" CssClass="fa fa-times" ForeColor="red"> ยกเลิก</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>

                                    </div>
                                    <div class="box-header">
                                        <div class="col-md-11" >
                                            <h4 class="text-primary">
                                                <asp:Label ID="lblTotalList" runat="server" Text=""></asp:Label>
                                            </h4>
                                        </div>
                                        <div class="col-lg-1">
                                            <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                                <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-print" data-toggle="dropdown">
                                                    <i class="fa fa-print"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">

                                                    <li>
                                                        <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i>รูปแบบ PDF</asp:LinkButton></li>
                                                    <li>
                                                        <asp:LinkButton ID="btnExcel" runat="server"><i class="fa fa-file-excel-o text-green"></i>รูปแบบ Excel</asp:LinkButton></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-gray">
                                                    <th>Budget Years<br />
                                                        (ปีงบประมาณ)</th>
                                                    <th>Budget Group<br />
                                                        (กลุ่มงบประมาณ)</th>
                                                    <th>Budget Type<br />
                                                        (ประเภทงบประมาณ)</th>
                                                    <th>Budget Received date<br />
                                                        (วันที่ได้รับงบประมาณ)</th>
                                                    <th>Budget amount<br />
                                                        (จำนวนงบประมาณ)</th>
                                                    <th id="thEdit" runat="server" >Tools<br />
                                                        (เครื่องมือ)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="footer" runat="server"></asp:Label>--%>
                                                        <tr>
                                                            <td data-title="Budget Year" style="text-align: center" width="1%">
                                                                <asp:Label ID="lblBudgetYear" runat="server"></asp:Label></td>
                                                            <td data-title="Budget Group" style="text-align: left" width="22%">
                                                                <asp:Label ID="lblBG" runat="server"></asp:Label></td>
                                                            <td data-title="Budget Sub" style="text-align: left" width="37%">
                                                                <asp:Label ID="lblBS" runat="server"></asp:Label></td>
                                                            <td data-title="Created Date" id="td" runat="server" style="text-align: center" width="17%">
                                                                <asp:Label ID="lblCreatedDate" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Budget" style="text-align: right" width="14%">
                                                                <asp:Label ID="lblBudget" runat="server"></asp:Label></td>
                                                            <td data-title="Edit" id="ColEdit" runat="server" class="center" width="9%">
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="fa fa-navicon text-green"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <%--<li id="liview" runat="server"><a href="javascript:;" onclick='btnViewClick(<%#Eval("ID") %>)'><i class="fa fa-search text-blue"></i>ดูรายละเอียด</a></li>--%>
                                                                        <li id="liedit" runat="server"><a href="javascript:;" onclick='btnEditClick(<%#Eval("ID") %>)'><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                                                                        <li class="divider"></li>
                                                                        <li id="lidelete" runat="server"><a href="javascript:;" onclick='btnDeleteClick(<%#Eval("ID") %>)'><i class="fa fa-remove text-red"></i>ลบ</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.box-body -->

                                    <!-- Page Navigation -->
                                    <uc1:PageNavigation runat="server" ID="Pager" PageSize="10" />

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->

                    <asp:TextBox ID="txtClickID" runat="server" Text="" Style="display: none"></asp:TextBox>
                    <asp:Button ID="btnView" OnClick="btnView_Click" runat="server" Style="display: none" />
                    <asp:Button ID="btnEdit" OnClick="btnEdit_Click" runat="server" Style="display: none" />
                    <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Style="display: none" />
                </asp:Panel>


                <iframe id="myframe" src="" frameborder="0" height="0px" width="0px" runat="server"></iframe>


            </ContentTemplate>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <%--    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
   <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>--%>

    <script type="text/javascript">

        function btnEditClick(activity_id) {
            document.getElementById('<%= txtClickID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnEdit.ClientID %>').click();
        }
        function btnViewClick(activity_id) {
            document.getElementById('<%= txtClickID.ClientID %>').value = activity_id;
            document.getElementById('<%= btnView.ClientID %>').click();
        }
        function btnDeleteClick(activity_id) {

            var c = confirm("ท่านต้องการลบข้อมูลใช่หรือไม่?");
            if (c == true) {
                document.getElementById('<%= txtClickID.ClientID %>').value = activity_id;
                document.getElementById('<%= btnDelete.ClientID %>').click();
            }

        }
    </script>
</asp:Content>
