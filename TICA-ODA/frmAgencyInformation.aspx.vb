﻿Imports System.Data
Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE

Public Class frmUserInformation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then

            BindData()

        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)

        Response.Redirect("frmEditAgencyInformation.aspx")

    End Sub
    Protected Sub BindData()
        Dim sql As String = "SELECT id,govnamethai,govnameeng,kong,govnumber,govroad,tambol,ampher,province,postcode"
        sql += ",telephone,fax,website,detail,ministry,email,ACTIVE_STATUS"
        sql += " FROM web_contactgoverment WHERE id=1"
        Dim trans As New TransactionDB
        Dim lnq As New TbContactgovermentLinqDB
        Dim p(1) As SqlParameter

        p(0) = SqlDB.SetBigInt("@_ID", 1)
        Dim dt As DataTable = lnq.GetListBySql(sql, trans.Trans, p)

        If dt.Rows.Count > 0 Then
            trans.CommitTransaction()
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("id").ToString() <> "" Then
                    lblGOVNameTH.Text = dt(i)("govnamethai").ToString()
                    lblGOVNameEN.Text = dt(i)("govnameeng").ToString()
                    lblDivision.Text = dt(i)("kong").ToString()
                    lblMinistry.Text = dt(i)("ministry").ToString()

                    lblLocation.Text = dt(i)("govnumber").ToString() & " ถนน " & dt(i)("govroad").ToString()
                    lblLocation.Text += " แขวง " & dt(i)("tambol").ToString() & " เขต " & dt(i)("ampher").ToString()
                    lblLocation.Text += " จังหวัด " & dt(i)("province").ToString() & " " & dt(i)("postcode").ToString()

                    lblTelephone.Text = dt(i)("telephone").ToString()
                    lblFax.Text = dt(i)("fax").ToString()

                    lblWebSite.Text = dt(i)("website").ToString()
                    lblEmail.Text = dt(i)("email").ToString()
                    lblDivision.Text = dt(i)("kong").ToString()

                    lblMinistry.Text = dt(i)("ministry").ToString()
                    lblNote.Text = dt(i)("detail").ToString()

                End If
                Session("SESSION_ID_GOVCONTACT") = dt.Rows(i)("id").ToString()
            Next

        Else
            trans.RollbackTransaction()
            Dim _err = trans.ErrorMessage()
        End If
        lnq = Nothing
    End Sub
End Class