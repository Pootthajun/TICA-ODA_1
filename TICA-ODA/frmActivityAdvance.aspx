﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmActivityAdvance.aspx.vb" Inherits="frmActivityAdvance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--<%@ Register Src="~/PageNavigation.ascx" TagPrefix="uc1" TagName="PageNavigation" %>--%>
<%@ Register Src="~/usercontrol/UCActivityAdvanceExpense.ascx" TagName="UCActivityAdvanceExpense" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" href="assets/img/logo.ico" />
    <title></title>
    <style type="text/css">
        table {
            border-collapse: separate;
        }

        .tr-top {
            height: 45px;
            background-color: lightblue;
            border-color: black;
        }

        .tb-fix {
            width: 100%;
            overflow-x: scroll;
            margin-left: 1px;
            overflow-y: visible;
            position: relative;
        }

        .tb-fix-col-1 {
            position: fixed;
            width: 75px;
            left: 0;
            top: auto;
        }

        .tb-fix-col-2 {
            position: fixed;
            width: 170px;
            left: 75px;
            top: auto;
        }

        .tbExpense tr th {
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="Scripts/txtClientControl.js"></script>
</head>
<body>
    <br />
    <form id="form1" runat="server">
        <div class="content-wrapper" style="margin-left: 0px;">

            <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" EnableScriptLocalization="true" EnableScriptGlobalization="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="udpList" runat="server">
                <ContentTemplate>
                    <!-- Main content -->
                    <%--  <section class="content">--%>
                    <div class="row">
                        <div class="col-xs-12">
                            <%-- <asp:Panel ID="Panel1" runat="server" CssClass="card bg-white" Style="position: fixed;width: 100%;">--%>
                            <asp:Panel ID="pnlList" runat="server" CssClass="card bg-white" Style="">
                                <asp:Label ID="lblexpenseid" runat="server" Text="0" Visible="false"></asp:Label>
                                <table id="example2" class="table" style="margin-bottom: unset; background-color: white; border-bottom: 1px solid #cccccc; z-index: 100;">
                                    <tbody>
                                        <tr class="bg-light-blue-gradient">
                                            <th>
                                                <table>

                                                    <tr>
                                                        <td rowspan="2" style="margin-right: 50px; padding-right: 30px;">
                                                            <b>
                                                                <h3 style="color: blanchedalmond;">Advance (เงินยืมทดรอง)   </h3>
                                                            </b>
                                                        </td>
                                                        <td>
                                                            <h4 style="color: white"><b>
                                                                <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                            </b></h4>

                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="margin-left: 20px;">กิจกรรม :
                                                           <asp:Label ID="lblActivitytName" runat="server"></asp:Label>
                                                        </td>

                                                    </tr>
                                                </table>





                                            </th>
                                            <th></th>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-9" text-align="center">
                                        <h4 style=""><b>
                                            <asp:Label ID="lblBorrowwer" runat="server"></asp:Label></b></h4>

                                        <h5 style="color: #229414;"><b>
                                            <asp:Label ID="lblBorrowwer_Detail" runat="server"></asp:Label></b> </h5>


                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-9" text-align="center">
                                        <div class="col-sm-12" style="text-align: right; margin-bottom: 10px;">
                                            <asp:LinkButton ID="btnPDF" runat="server"><i class="fa fa-file-pdf-o text-red"></i> รูปแบบ PDF</asp:LinkButton>
                                            <asp:LinkButton ID="btnExcel" runat="server" Style=" margin-left :10px;"><i class="fa fa-file-excel-o text-green"></i> รูปแบบ Excel</asp:LinkButton>

                                           
                                        </div>



                                        <asp:Panel ID="pnlAdvance" runat ="server" >
                                            <uc1:UCActivityAdvanceExpense runat="server" ID="UCActivityAdvanceExpense" />
                                        </asp:Panel>
                                    </div>
                                </div>


                            </asp:Panel>
                            <%--<div id="topmargin"></div>--%>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- <div style="height: 70px;"></div>--%>
        </div>

        <%-- <div id="pnlFooter_btnSave" runat="server" class="modal-footer" style="bottom: 0px; text-align: right; background-color: white; width: 100%;">
            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn  btn-social btn-success">
               <i class="fa fa-save"></i> Save
            </asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn  btn-social btn-google">
                <i class="fa fa-reply"></i> Cancel
            </asp:LinkButton>
        </div>--%>
    </form>



    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

</body>
</html>
