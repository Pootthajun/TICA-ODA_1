﻿Imports Constants
Imports System.Data
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class rptRecipient_In_Project
    Inherits System.Web.UI.Page

    Dim BL As New ODAENG
    Dim GL As New GenericLib
    Private Sub rptExpense_Activity_GroupBy_Country_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim li As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports")
            li.Attributes.Add("class", "active")
            Dim li_mnuReports_foreign As HtmlGenericControl = Me.Page.Master.FindControl("mnuReports_Admin")
            li_mnuReports_foreign.Attributes.Add("class", "active")
            Dim a As HtmlAnchor = Me.Page.Master.FindControl("Admin_05")
            a.Attributes.Add("style", "color:#FF8000")

            BindList()
        End If

    End Sub


    Public Function GetList() As DataTable

        Dim DT As New DataTable

        Dim filter As String = ""
        Dim Title As String = ""
        Try
            Dim sql As String = ""
            sql &= "     Select * FROM vw_Activity_Recipient_rpt " + Environment.NewLine

            'sql &= "     Select DISTINCT project_id " + Environment.NewLine
            'sql &= "           ,project_name " + Environment.NewLine
            'sql &= "           ,project_type     " + Environment.NewLine
            'sql &= "           ,node_id " + Environment.NewLine
            'sql &= "           ,vw_Recipient_In_Project.name_th Full_Name " + Environment.NewLine
            'sql &= "           ,node_id_OU " + Environment.NewLine
            'sql &= "           ,name_th_OU " + Environment.NewLine
            'sql &= "           ,TB_Recipience_Register.* ,perposecat_name Cource " + Environment.NewLine
            'sql &= "           ,dbo.GetShortThaiDate(TB_Recipience_Register.start_date) start_date_th " + Environment.NewLine
            'sql &= "           ,dbo.GetShortThaiDate(TB_Recipience_Register.end_date) end_date_th " + Environment.NewLine
            'sql &= "           ,dbo.GetShortThaiDate(TB_Recipience_Register.expired_date_passport) expired_date_passport_th " + Environment.NewLine
            'sql &= "           ,dbo.GetShortThaiDate(TB_Recipience_Register.expired_date_Visa) expired_date_Visa_th " + Environment.NewLine
            'sql &= "           ,dbo.GetShortThaiDate(TB_Recipience_Register.expired_date_Insurance) expired_date_Insurance_th " + Environment.NewLine
            'sql &= "       From vw_Recipient_In_Project " + Environment.NewLine
            'sql &= "       Left Join TB_Recipience_Register On TB_Recipience_Register.activity_id = vw_Recipient_In_Project.activity_id And TB_Recipience_Register.person_id = vw_Recipient_In_Project.node_id   " + Environment.NewLine
            'sql &= "       LEFT JOIN TB_Purposecat ON TB_Purposecat.id=TB_Recipience_Register.sector_id   " + Environment.NewLine
            'sql &= "       where rectype ='P'   " + Environment.NewLine


            'If (txtSearch_Project.Text <> "") Then

            '    filter &= "  project_name Like '%" & txtSearch_Project.Text & "%'  AND " + Environment.NewLine
            '    Title += " รายชื่อผู้รับทุน ของ " & txtSearch_Project.Text
            'End If

            'If (txtSearch_Recipient.Text <> "") Then

            '    filter &= "  REPLACE(vw_Recipient_In_Project.name_th,'  ',' ') Like REPLACE('%" & txtSearch_Recipient.Text & "%','  ',' ')  AND " + Environment.NewLine
            '    Title += " ชื่อ " & txtSearch_Recipient.Text
            'End If

            'If (txtSearch_Course.Text <> "") Then

            '    filter &= "  REPLACE(perposecat_name,'  ',' ') Like REPLACE('%" & txtSearch_Course.Text & "%','  ',' ')  AND " + Environment.NewLine
            '    Title += " Course " & txtSearch_Course.Text
            'End If

            'If filter <> "" Then
            '    sql += " WHERE " & filter.Substring(0, filter.Length - 6) + Environment.NewLine
            'End If

            'sql &= "order by project_id   " + Environment.NewLine



            If (txtSearch_Project.Text <> "") Then

                filter &= "  project_name Like '%" & txtSearch_Project.Text & "%'  AND " + Environment.NewLine
                Title += " รายชื่อผู้รับทุน ของ " & txtSearch_Project.Text
            End If

            If (txtSearch_Recipient.Text <> "") Then

                filter &= "  REPLACE(Full_Name,'  ',' ') Like REPLACE('%" & txtSearch_Recipient.Text & "%','  ',' ')  AND " + Environment.NewLine
                Title += " ชื่อ " & txtSearch_Recipient.Text
            End If

            If (txtSearch_Course.Text <> "") Then

                filter &= "  REPLACE(Cource,'  ',' ') Like REPLACE('%" & txtSearch_Course.Text & "%','  ',' ')  AND " + Environment.NewLine
                Title += " Course " & txtSearch_Course.Text
            End If

            If filter <> "" Then
                sql += " WHERE " & filter.Substring(0, filter.Length - 6) + Environment.NewLine
            End If

            sql &= "order by project_id   " + Environment.NewLine

            DT = SqlDB.ExecuteTable(sql)

            lblTotalRecord.Text = Title
            If DT.Rows.Count = 0 Then
                lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
            Else
                lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
            End If

            Session("Search_Recipient_In_Project_Title") = lblTotalRecord.Text
        Catch ex As Exception
        End Try
        Return DT

    End Function



    Private Sub BindList()


        Dim DT As DataTable = GetList()

        rptList.DataSource = DT
        rptList.DataBind()

        Session("Search_Recipient_In_Project") = DT

        Pager.SesssionSourceName = "Search_Recipient_In_Project"
        Pager.RenderLayout()
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Dim Lastbudget_year As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If
        Dim lblNo As Label = DirectCast(e.Item.FindControl("lblNo"), Label)
        Dim lblName As Label = DirectCast(e.Item.FindControl("lblName"), Label)
        Dim lblCourse As Label = DirectCast(e.Item.FindControl("lblCourse"), Label)
        Dim lblPeriod As Label = DirectCast(e.Item.FindControl("lblPeriod"), Label)
        Dim lblInstitute As Label = DirectCast(e.Item.FindControl("lblInstitute"), Label)

        Dim lblProject As Label = DirectCast(e.Item.FindControl("lblProject"), Label)
        Dim lblActivity As Label = DirectCast(e.Item.FindControl("lblActivity"), Label)

        Dim lblexpired_date_Insurance As Label = DirectCast(e.Item.FindControl("lblexpired_date_Insurance"), Label)
        Dim lblexpired_date_Visa As Label = DirectCast(e.Item.FindControl("lblexpired_date_Visa"), Label)
        Dim lblexpired_date_passport As Label = DirectCast(e.Item.FindControl("lblexpired_date_passport"), Label)

        Dim DuplicatedStyleTop As String = "border-top:none;"


        Dim trbudget_year As HtmlTableRow = e.Item.FindControl("trbudget_year")
        Dim lblbudget_year As Label = e.Item.FindControl("lblbudget_year")

        '--------Lastbudget_year------------
        If Lastbudget_year <> e.Item.DataItem("project_name").ToString Then
            Lastbudget_year = e.Item.DataItem("project_name").ToString
            lblbudget_year.Text = Lastbudget_year
            trbudget_year.Visible = True
        Else
            trbudget_year.Visible = False
        End If


        lblProject.Text = e.Item.DataItem("project_name").ToString
        lblActivity.Text = e.Item.DataItem("activity_name").ToString


        lblName.Text = e.Item.DataItem("Full_Name").ToString
        lblCourse.Text = e.Item.DataItem("Cource").ToString
        lblInstitute.Text = e.Item.DataItem("Institute").ToString

        If Convert.IsDBNull(e.Item.DataItem("start_date")) = False Then
            lblPeriod.Text = GL.ReportThaiDate(e.Item.DataItem("start_date")) & "-" & GL.ReportThaiDate(e.Item.DataItem("end_date"))
        End If


        If Convert.IsDBNull(e.Item.DataItem("expired_date_Insurance")) = False Then
            lblexpired_date_Insurance.Text = GL.ReportThaiDate(e.Item.DataItem("expired_date_Insurance"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("expired_date_Visa")) = False Then
            lblexpired_date_Visa.Text = GL.ReportThaiDate(e.Item.DataItem("expired_date_Visa"))
        End If

        If Convert.IsDBNull(e.Item.DataItem("expired_date_passport")) = False Then
            lblexpired_date_passport.Text = GL.ReportThaiDate(e.Item.DataItem("expired_date_passport"))
        End If


        '============Click To Project===============
        Dim lnkSelectProject As HtmlAnchor = e.Item.FindControl("lnkSelectProject")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & lnkSelectProject.ClientID & "').click();"
        Next
        '================Type And Click To Project===========================
        If (e.Item.DataItem("project_type") = Constants.Project_Type.Project) Or (e.Item.DataItem("project_type") = Constants.Project_Type.NonProject) Then
            lnkSelectProject.HRef = "frmProject_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        ElseIf (e.Item.DataItem("project_type") = Constants.Project_Type.Loan) Then
            lnkSelectProject.HRef = "frmLoan_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        Else
            lnkSelectProject.HRef = "frmContribuiltion_Detail_Info.aspx?id=" & e.Item.DataItem("project_id") & " &mode=" & "edit&type=" & e.Item.DataItem("project_type")
        End If

    End Sub



    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        BindList()

    End Sub




    '  Select Case project_id
    '    ,project_name
    '    ,project_type    
    '    ,node_id
    '    ,vw_Recipient_In_Project.name_th Full_Name
    '    ,node_id_OU
    '    ,name_th_OU
    ' ,TB_Recipience_Register.*
    'FROM vw_Recipient_In_Project
    'LEFT JOIN TB_Recipience_Register On TB_Recipience_Register.activity_id = vw_Recipient_In_Project.activity_id And TB_Recipience_Register.person_id = vw_Recipient_In_Project.node_id 
    'where rectype ='P'

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptRecipient_In_Project.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/rptRecipient_In_Project.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class
