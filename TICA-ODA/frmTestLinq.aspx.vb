﻿Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Imports System.Data
Imports System.Data.SqlClient

Partial Class frmTestLinq
    Inherits System.Web.UI.Page

#Region "Test Linq"
    Private Sub btnInsertData_Click(sender As Object, e As EventArgs) Handles btnInsertData.Click
        '#########Test Insert Data #################
        'Dim trans As New TransactionDB
        'Dim lnq As New MsIndicatorLinqDB
        'lnq.INDICATOR_CODE = "001"
        'lnq.INDICATOR_NAME = "ด้านเศรษฐกิจ"
        'lnq.ACTIVE_STATUS = "Y"
        'Dim ret As ExecuteDataInfo = lnq.InsertData("TestFunction", trans.Trans)

        'If ret.IsSuccess = True Then



        'trans.CommitTransaction()
        '    Dim _id As Long = lnq.ID
        'Else
        '    trans.RollbackTransaction()
        '    Dim _err As String = ret.ErrorMessage
        'End If
        'lnq = Nothing
        '#########################################
    End Sub

    Private Sub btnGetDataList_Click(sender As Object, e As EventArgs) Handles btnGetDataList.Click
        '//######### Test Select Data #################
        '//####### Test 1 GetDataList

        'Dim trans As New TransactionDB
        'Dim lnq As New MsIndicatorLinqDB
        'Dim p(1) As SqlParameter
        'p(0) = SqlDB.SetBigInt("@_ID", 1)

        'Dim dt As DataTable = lnq.GetDataList("id=@_ID", "id", trans.Trans, p)
        'If dt.Rows.Count > 0 Then
        '    trans.CommitTransaction()
        'Else
        '    trans.RollbackTransaction()
        '    Dim _err = lnq.ErrorMessage
        'End If
        'lnq = Nothing

    End Sub

    Private Sub btnGetListBySql_Click(sender As Object, e As EventArgs) Handles btnGetListBySql.Click
        ''//####### Test 2 GetListBySql
        'Dim Sql As String = "select id, indicator_code, indicator_name from ms_indicator where id=@_ID"
        'Dim p(1) As SqlParameter
        'p(0) = SqlDB.SetBigInt("@_ID", 1)

        'Dim trans As New TransactionDB
        'Dim lnq As New MsIndicatorLinqDB

        'Dim dt As DataTable = lnq.GetListBySql(Sql, trans.Trans, p)
        'If dt.Rows.Count > 0 Then
        '    trans.CommitTransaction()
        'Else
        '    trans.RollbackTransaction()
        '    Dim _err As String = lnq.ErrorMessage
        'End If
        'lnq = Nothing

        ''//#########################################
    End Sub

    Private Sub btnUpdateData_Click(sender As Object, e As EventArgs) Handles btnUpdateData.Click
        ''//######### Test Update Data #################
        'Dim trans As New TransactionDB
        'Dim lnq As New MsIndicatorLinqDB
        'lnq.GetDataByPK(1, trans.Trans)
        'lnq.INDICATOR_CODE = "0001"
        'lnq.INDICATOR_NAME = "ด้านเศรษฐกิจ0000"
        'lnq.ACTIVE_STATUS = "N"

        'Dim ret As ExecuteDataInfo
        'If lnq.ID > 0 Then
        '    ret = lnq.UpdateData("TestUpdateFunction", trans.Trans)
        'Else
        '    ret = lnq.InsertData("TestInsertFunction", trans.Trans)
        'End If

        'If ret.IsSuccess = True Then
        '    trans.CommitTransaction()
        'Else
        '    trans.RollbackTransaction()
        '    Dim _err As String = ret.ErrorMessage
        'End If
        'lnq = Nothing

        ''//#########################################
    End Sub

    Private Sub btnDeleteData_Click(sender As Object, e As EventArgs) Handles btnDeleteData.Click
        ''//######### Test Delete Data #################
        'Dim trans As New TransactionDB
        'Dim lnq As New MsIndicatorLinqDB

        'Dim ret As ExecuteDataInfo = lnq.DeleteByPK(1, trans.Trans)
        'If ret.IsSuccess = True Then
        '    trans.CommitTransaction()
        'Else
        '    trans.RollbackTransaction()
        '    Dim _err As String = ret.ErrorMessage
        'End If
        'lnq = Nothing
        ''//############################################
    End Sub
#End Region

#Region "Test Dropdown Treeview"
    Private Sub frmTestLinq_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            'UCOUDropdownlist1.SetOrganizeByCountryID(194)
            'UCOUDropdownlist1.SetOUDropdownlist()

            'UCOUDropdownlist1.SelectedValue = 256
        End If
    End Sub

    Private Sub btnGetCheckNode_Click(sender As Object, e As EventArgs) Handles btnGetCheckNode.Click
        'UCOUDropdownlist1.GetCheckedNode()
    End Sub

    Private Sub btnSetCheckNode_Click(sender As Object, e As EventArgs) Handles btnSetCheckNode.Click
        'Dim dt As New DataTable
        'dt.Columns.Add("node_id")

        'dt.Rows.Add("10")
        'dt.Rows.Add("101")
        'dt.Rows.Add("257")
        'dt.Rows.Add("147")

        'UCOUDropdownlist1.SetCheckNode(dt)
    End Sub

    Private Sub btnGetSelectedNode_Click(sender As Object, e As EventArgs) Handles btnGetSelectedNode.Click
        'Dim s As String = UCOUDropdownlist1.SelectedValue

        'Dim bl As New ODAENG
        'lblNavigateNodeText.Text = bl.GetNodePathTextByNodeID(s, "TH")



        'Dim a As String = s
    End Sub

    Private Sub btnSetSelectedNode_Click(sender As Object, e As EventArgs) Handles btnSetSelectedNode.Click
        'UCOUDropdownlist1.SelectedValue = 194


    End Sub

    Private Sub btnAddNode_Click(sender As Object, e As EventArgs) Handles btnAddNode.Click


        'UCActivityTreeList1.AddNode(txtNodeID.Text, txtNodeName.Text, "1", txtParentID.Text, Now, DateAdd(DateInterval.Day, 2, Now), "", chkExpand.Checked)
        UCActivityTreeList1.GenerateActivityList(1, "view")
    End Sub
#End Region

#Region "Test Tree View Table"

#End Region
End Class
