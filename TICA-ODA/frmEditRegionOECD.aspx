﻿<%@ Page Title="" Language="vb" enableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeFile="frmEditRegionOECD.aspx.vb" Inherits="frmEditRegionOECD" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Region (OECD) | ODA</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            OECD Region (ภูมิภาค OECD)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" style="cursor:text"><i class="fa fa-university"></i> Master</a></li>
            <li><a href="frmRegionOECD.aspx"> OECD Region</a></li>
            <li class="active"><asp:Label ID="lblEditMode" runat="server" Text=""></asp:Label> OECD Region</li>
              
          </ol>
        </section>
         <asp:UpdatePanel ID="udpList" runat="server">
            <ContentTemplate>
        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title"><asp:Label ID="lblEditMode2" runat="server" Text=""></asp:Label> OECD Region</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputRegion" class="col-sm-3 control-label">OECD Region 
                          <br/> ภูมิภาค OECD : <span style="color:red">*</span></label>
                      <div class="col-sm-8">
                        
                          <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                      <div class="form-group">
                      <label for="inputRegion" class="col-sm-3 control-label">OECD Region Th
                          <br/> ภูมิภาค OECD(ภาษาไทย) : <span style="color:red"></span></label>
                      <div class="col-sm-8">
                        
                          <asp:TextBox ID="TxtNameTh" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>
                      </div>
                    </div>
                    <div class="clearfix"></div>
              <!-- /.box -->
                                        <div class="clearfix"></div>
                       <br />
                      
                    <div class="form-group">
                      <label for="inputname" class="col-sm-3 control-label">Active Status :</label>
                      <div class="col-sm-8">
                     <label><asp:CheckBox ID="chkACTIVE_STATUS" runat="server" CssClass="minimal" checked="true"/></label>
                      </div>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                   <div class="col-sm-9"></div>
                    <div class="col-lg-8"></div>
                      <div class="col-lg-2">
                         
                          <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="btn btn-block btn-social btn-success">
                              <i class="fa fa-save"></i> Save
                          </asp:LinkButton>
                      </div>
                    <div class="col-lg-2">
                         <asp:LinkButton ID="btnCancle" runat="server" OnClick="btnCancle_Click" CssClass="btn btn-block btn-social btn-google">
                              <i class="fa fa-reply"></i> Cancel
                          </asp:LinkButton>
                    </div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->

         </ContentTemplate>
        </asp:UpdatePanel>
        </div>
 

<!----------------Page Advance---------------------->
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />     
  
</asp:Content>
