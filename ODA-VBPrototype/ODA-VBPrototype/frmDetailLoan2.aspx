﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailLoan2.aspx.vb" Inherits="ODA_VBPrototype.frmDetailLoan2" %>
<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Detail Loan | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file-o"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file-o"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-align-justify"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-align-justify"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-align-justify"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <a class="text-yellow">CODE ID 201400671:</a> <a class="text-blue"> การศึกษาความเหมาะสมและออกแบบการก่อสร้างสายส่งไฟฟ้า 115 kV<br /> และสถานีไฟฟ้าช่วงน้ำทง- ห้วยทราย</a>
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmProjectStatus.aspx">Project Status</a></li>
            <li><a href="frmLoan.aspx">Loan</a></li>
            <li class="active">Detail Loan</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-alt"></i> Infomation(ข้อมูลโครงการ)</a></li>
                  <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-th-list"></i> Activity(กิจกรรม)</a></li>
                  <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-users"></i> Recipience(ผู้รับทุน)</a></li>
                    <li class="dropdown pull-right" data-toggle="tooltip" title="เครื่องมือ">
                    <a class="dropdown-toggle text-muted" data-toggle="dropdown" href="#">
                      <i class="fa fa-gear"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmDetailActivityLoan2.aspx"><i class="fa fa-edit text-blue"></i> Edit</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-print text-blue"></i> Print</a></li>
                      <li role="presentation" class="divider"></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash text-red"></i> Delete</a></li>
                    </ul>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    
                    <div class="box-body">
                      <table class="table table-bordered">
                         <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Informations (ข้อมูลทั่วไป) </p></h4></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Loan Project Name (ชื่อโครงการเงินกู้) :</p></td>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="การศึกษาความเหมาะสมและออกแบบการก่อสร้างสายส่งไฟฟ้า 115 kV และสถานีไฟฟ้าช่วงน้ำทง- ห้วยทราย" disabled></textarea></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Objectives (วัตถุประสงค์) :</p></td>
                          <td class="text-primary">
                            <textarea class="form-control" rows="4" placeholder="	เงินให้เปล่า (มูลค่า 15 ล้านบาท)" disabled></textarea></td>
                        </tr>
                        
                       <tr>
                          <td style="width: 250px"><p class="pull-right">Start Date/End Date</p><p class="pull-right">(วันเริ่มต้น/สิ้นสุดโครงการ) :</p></td>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation" placeholder="10/06/2556-04/02/2557" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <td style="width: 280px"><p class="pull-right">Commitment (พันธกรณี) :</p></td>
                          <td class="text-success">
                             <div class="col-sm-6">
                               <div class="input-group">
                                 <div class="input-group-addon">
                                 <i class="fa fa-dollar"></i>
                                 </div>
                                <input type="text" class="form-control pull-right" id="Budget" placeholder="15,000,000.00 บาท" disabled >
                               </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Cooperation Framework</p><p class="pull-right">(กรอบความร่วมมือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">- (N/A)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                  
                        <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Agency (หน่วยงาน) </p></h4></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Funding Agency</p><p class="pull-right">(หน่วยงานให้ความช่วยเหลือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Neigbouring Countries Economic Development Cooperation Agency NEDA (Public Organization)</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                         </tr>
                         <tr>
                          <td style="width: 250px"><p class="pull-right">Executing Agency(หน่วยงานดำเนินการ) :</p></td>
                          <td class="text-primary">
                          <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Neigbouring Countries Economic Development Cooperation Agency NEDA</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group -->
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Implementing Agency</p><p class="pull-right">(หน่วยงานความร่วมมือ) :</p></td>
                          <td class="text-primary">
                           <div class="col-sm-12">
                            <textarea class="form-control" rows="2" placeholder="สพพ.ว่าจ้างที่ปรึกษาดำเนินการศึกษา" disabled></textarea>
                           </div></td>
                        </tr>
                       <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Detail Loan (รายละเอียดเงินกู้) </p></h4></td>
                        </tr>
                        <tr>
                            <td style="width: 150px"><p class="pull-right">ระยะผ่อนปรน (Grance Period) :</p></td>
                            <td><div class="col-sm-6">
                                <input type="text" class="form-control pull-right" placeholder="" disabled>
                                </div><p>Year (ปี)</p></td>
                         </tr>
                        <tr>
                            <td style="width: 150px"><p class="pull-right">ระยะเวลากู้ (Maturity Period) :</p></td>
                            <td><div class="col-sm-6">
                                <input type="text" class="form-control pull-right" placeholder="" disabled>
                                </div><p>Year (ปี)</p></td>
                         </tr>
                         <tr>
                            <td style="width: 150px"><p class="pull-right">อัตราดอกเบี้ย (Interest Rate) :</p></td>
                            <td><div class="col-sm-6">
                                <input type="text" class="form-control pull-right" placeholder="วงเงินผูกพันสำหรับการดำเนินโครงการตั้งแต่ปี 2556-2557" disabled>
                                </div><p>%</p></td>
                         </tr>
                        <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Other Detail (รายละเอียดอื่นๆ) </p></h4></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Assistant (ผู้ช่วยโครงการ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" multiple="multiple" data-placeholder="-" style="width: 100%;" disabled>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                             </div></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Contact Person (ข้อมูลผู้ติดต่อ) :</p></td>
                              <td class="text-primary">
                                  <table class="table table-bordered">
                                    <tr>
                                      <td style="width: 150px" class="bg-gray"><p class="pull-right">Name (ชื่อ) :</p></td>
                                      <td>
                                        <div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-user"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Position (ตำแหน่ง):</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-building"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Phone (โทรศัพท์) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-phone"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Fax (โทรสาร) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-fax"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Email :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-list-alt"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                   </table></td>
                        </tr>
                        
                        <tr>
                          <td style="width: 150px"><p class="pull-right">Note (หมายเหตุ) :</p></td>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="" disabled></textarea></td>
                        </tr>
                         <tr>
                          <td style="width: 150px"><p class="pull-right">File input (เพิ่มไฟล์) :</p> </td>
                          <td class="text-primary"><input type="file" id="exampleInputFile"> <i class="text-green">(ขนาดไฟล์ไม่เกิน 5 MB)</i><br />
                              <table class="table">
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-pdf-o text-red"></i> ข่าวประชาสัมพันธ์.pdf</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-excel-o text-green"></i> รายชื่อผู้รับทุน.xlsx</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-picture-o"></i> ความช่วยเหลือ.png</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                   </table>
                            
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Transfer Project To (มอบหมายโครงการให้) :</p></td>
                          <td class="text-primary"> 
                             <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                              </div>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                      </table>
                  <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                        
                    </div>
                    </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">

                    <div class="box-header with-border">        
                      <div class="col-sm-8">
                                <p><a data-toggle="modal" data-target="#AddCourseLoan">
                                <button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Activity</button></a></p> </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Year" class="btn btn-block btn-social btn-info"><i class="fa fa-calendar"></i>Year </a>
                        </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Month" class="btn btn-block btn-social btn-primary"><i class="fa fa-calendar"></i>Month </a>
                        </div>

                      </div>
                <div id="Year" class="panel-collapse collapse active">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr>
                        <th colspan="8" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="7" class="text-center bg-gray">Year (ปี)</th>
                    </tr>
                    <tr>
                       
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th style="width: 800px" class="bg-info">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Butget</th>
                      <th class="bg-info">Tool</th>

                      <th class="bg-gray">2552</th>
                      <th class="bg-gray">2553</th>
                      <th class="bg-gray">2554</th>
                      <th class="bg-gray">2555</th>
                      <th class="bg-gray">2556</th>
                      <th class="bg-gray">2557</th>
                      <th class="bg-gray">2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txt1" value="การศึกษาความเหมาะสมและออกแบบการก่อสร้างสายส่งไฟฟ้า 115 kV และสถานีไฟฟ้าช่วงน้ำทง- ห้วยทราย" size="50" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>15,000,000.00</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="4">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="3"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">เงินให้เปล่า (มูลค่า 15 ล้านบาท)</td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>15,000,000.00</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="4"></td>
                    </tr>
                    
                 
                  </table>
                </div>
               </div>

                <div id="Month" class="panel-collapse collapse">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr class="bg-gray text-center">
                        <th colspan="8" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="84" class="text-center bg-gray">Month (เดือน)</th>
                    </tr>
                    <tr class="bg-gray">
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th class="bg-info" style="width: 70%;">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Budget</th>
                      <th class="bg-info">Tool</th>
                        
                      <th class="bg-gray text-center">January 2552</th>
                      <th class="bg-gray text-center">February 2552</th>
                      <th class="bg-gray text-center">March 2552</th>
                      <th class="bg-gray text-center">April 2552</th>
                      <th class="bg-gray text-center">May 2552</th>
                      <th class="bg-gray text-center">June 2552</th>
                      <th class="bg-gray text-center">July 2552</th>
                      <th class="bg-gray text-center">August 2552</th>
                      <th class="bg-gray text-center">September 2552</th>
                      <th class="bg-gray text-center">October 2552</th>
                      <th class="bg-gray text-center">November 2552</th>
                      <th class="bg-gray text-center">December 2552</th>
                      <th class="bg-gray text-center">January 2553</th>
                      <th class="bg-gray text-center">February 2553</th>
                      <th class="bg-gray text-center">March 2553</th>
                      <th class="bg-gray text-center">April 2553</th>
                      <th class="bg-gray text-center">May 2553</th>
                      <th class="bg-gray text-center">June 2553</th>
                      <th class="bg-gray text-center">July 2553</th>
                      <th class="bg-gray text-center">August 2553</th>
                      <th class="bg-gray text-center">September 2553</th>
                      <th class="bg-gray text-center">October 2553</th>
                      <th class="bg-gray text-center">November 2553</th>
                      <th class="bg-gray text-center">December 2553</th>
                      <th class="bg-gray text-center">January 2554</th>
                      <th class="bg-gray text-center">February 2554</th>
                      <th class="bg-gray text-center">March 2554</th>
                      <th class="bg-gray text-center">April 2554</th>
                      <th class="bg-gray text-center">May 2554</th>
                      <th class="bg-gray text-center">June 2554</th>
                      <th class="bg-gray text-center">July 2554</th>
                      <th class="bg-gray text-center">August 2554</th>
                      <th class="bg-gray text-center">September 2554</th>
                      <th class="bg-gray text-center">October 2554</th>
                      <th class="bg-gray text-center">November 2554</th>
                      <th class="bg-gray text-center">December 2554</th>
                      <th class="bg-gray text-center">January 2555</th>
                      <th class="bg-gray text-center">February 2555</th>
                      <th class="bg-gray text-center">March 2555</th>
                      <th class="bg-gray text-center">April 2555</th>
                      <th class="bg-gray text-center">May 2555</th>
                      <th class="bg-gray text-center">June 2555</th>
                      <th class="bg-gray text-center">July 2555</th>
                      <th class="bg-gray text-center">August 2555</th>
                      <th class="bg-gray text-center">September 2555</th>
                      <th class="bg-gray text-center">October 2555</th>
                      <th class="bg-gray text-center">November 2555</th>
                      <th class="bg-gray text-center">December 2555</th>
                      <th class="bg-gray text-center">January 2556</th>
                      <th class="bg-gray text-center">February 2556</th>
                      <th class="bg-gray text-center">March 2556</th>
                      <th class="bg-gray text-center">April 2556</th>
                      <th class="bg-gray text-center">May 2556</th>
                      <th class="bg-gray text-center">June 2556</th>
                      <th class="bg-gray text-center">July 2556</th>
                      <th class="bg-gray text-center">August 2556</th>
                      <th class="bg-gray text-center">September 2556</th>
                      <th class="bg-gray text-center">October 2556</th>
                      <th class="bg-gray text-center">November 2556</th>
                      <th class="bg-gray text-center">December 2556</th>
                      <th class="bg-gray text-center">January 2557</th>
                      <th class="bg-gray text-center">February 2557</th>
                      <th class="bg-gray text-center">March 2557</th>
                      <th class="bg-gray text-center">April 2557</th>
                      <th class="bg-gray text-center">May 2557</th>
                      <th class="bg-gray text-center">June 2557</th>
                      <th class="bg-gray text-center">July 2557</th>
                      <th class="bg-gray text-center">August 2557</th>
                      <th class="bg-gray text-center">September 2557</th>
                      <th class="bg-gray text-center">October 2557</th>
                      <th class="bg-gray text-center">November 2557</th>
                      <th class="bg-gray text-center">December 2557</th>
                      <th class="bg-gray text-center">January 2558</th>
                      <th class="bg-gray text-center">February 2558</th>
                      <th class="bg-gray text-center">March 2558</th>
                      <th class="bg-gray text-center">April 2558</th>
                      <th class="bg-gray text-center">May 2558</th>
                      <th class="bg-gray text-center">June 2558</th>
                      <th class="bg-gray text-center">July 2558</th>
                      <th class="bg-gray text-center">August 2558</th>
                      <th class="bg-gray text-center">September 2558</th>
                      <th class="bg-gray text-center">October 2558</th>
                      <th class="bg-gray text-center">November 2558</th>
                      <th class="bg-gray text-center">December 2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="50" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>15,000,000.00</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm11" value="เงินให้เปล่า (มูลค่า 15 ล้านบาท)" size="50" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>15,000,000.00</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseLoan"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="48"></td>
                    </tr>
                    
                  
                  </table>
                </div>
               </div> 
                  <%--<div id="gantt_here" style='width:1060px; height:480px;'></div>  
                  
                   <script src="dist/Gantter/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
                   <link rel="stylesheet" href="dist/Gantter/codebase/skins/dhtmlxgantt_meadow.css" type="text/css" media="screen" title="no title">

                    <script type="text/javascript" src="dist/Gantter/common/testdata.js"></script>

                        <script type="text/javascript">
	                    gantt.init("gantt_here");

	                    gantt.templates.rightside_text = function(start, end, task){
		                    return "ID: #" + task.id;
	                    };

	                    gantt.templates.leftside_text = function(start, end, task){
		                    return task.duration + " days";
	                    };
	                    gantt.parse(demo_tasks);
                    </script>

                    <script type="text/javascript">
                            var demo_tasks = {
                                "data": [
                                    { "id": 10, "text": "แผนงานที่ 1", "start_date": "", "duration": "", "progress": 0.6, "open": true },
                
                                    { "id": 11, "text": "แผนงานพัฒนาบุคลากรของวิทยาลัย", "start_date": "03-10-2016", "duration": "40", "parent": "10", "progress": 1, "open": true },
                                    { "id": 12, "text": "1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท.", "start_date": "06-10-2016", "duration": "30", "parent": "11", "progress": 0.8, "open": true },
                                    { "id": 13, "text": "1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน", "start_date": "10-10-2016", "duration": "14", "parent": "11", "progress": 0.2, "open": true },
                                    { "id": 14, "text": "1.3 ทุนฝึกอบรมหลักสูตรเฉพาะทางหลักสูตรสำหรับครูด้านศิลปดนตรี (ตัวโน้ต (ซอลแฟร์ กลอง แคน ระนาด ขิม ซอ ฆ้องวง ร้องเพลง ฟ้อน เปียโน) กีต้าร์ และเปียโน) จำนวน 15 คน / 2 เดือน", "start_date": "10-10-2016", "duration": "40", "parent": "11", "progress": 0, "open": true },
                                    { "id": 15, "text": "โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556", "start_date": "03-10-2016", "duration": "24", "parent": "13", "progress": 0.5, "open": true },


                                    { "id": 20, "text": "แผนงานที่ 2", "start_date": "", "duration": "", "progress": 0.6, "open": true },
                                    { "id": 21, "text": "แผนงานถ่ายทอดความรู้", "start_date": "29-09-2016", "duration": "4", "parent": "20", "progress": 0.1, "open": true },
                                    { "id": 22, "text": "2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี", "start_date": "03-10-2016", "duration": "5", "parent": "21", "progress": 0, "open": true },

                                    { "id": 30, "text": "แผนงานที่ 3", "start_date": "", "duration": "", "progress": 0.6, "open": true },
                                    { "id": 31, "text": "แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด", "start_date": "03-10-2016", "duration": "5", "parent": "30", "progress": 1, "open": true },

                                    { "id": 40, "text": "แผนงานที่ 4", "start_date": "", "duration": "", "progress": 0.6, "open": true },
                                    { "id": 41, "text": "แผนงานติดตามและประเมินผล", "start_date": "", "duration": "", "parent": "40", "progress": 0.5, "open": true },
                                    { "id": 42, "text": "4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง", "start_date": "02-10-2016", "duration": "60", "parent": "41", "progress": 0.8, "open": true },
                                    { "id": 43, "text": "4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา", "start_date": "", "duration": "", "parent": "41", "progress": 0.2, "open": true },

                                ]
                            };

                            gantt.init("gantt_here");

                            gantt.parse(demo_tasks);


                        </script>--%>
                
                     
                  </div><!-- /.tab-pane -->
                   <div class="tab-pane" id="tab_3">
                  <div class="box-header with-border">
                                
                             <div class="col-lg-5"><br />
                                <h5 class="text-primary"><b>พบทั้งหมด 4 รายการ</b></h5>
                             </div>

                            <div class="col-lg-5">
                                <div class="input-group margin">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search" data-toggle="tooltip" title="ค้นหา"></i></button>
                                </span>
                              </div><!-- /input-group -->
                           </div>
                           <div class="col-lg-2">
                                <div class="input-group margin">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                    <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                    <i class="fa fa-magic"></i>
                                    </button></a>
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       <div id="collap1" class="panel-collapse collapse">
                           
                        <div class="box-header with-border">
                            <div class="box-body">
                                <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Student ID :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>

                                       <label for="Sector" class="col-sm-2 control-label">Country :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                   </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Course :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                  
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                      <label for="inputname" class="col-sm-2 control-label">Status :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>


                                  </div><!-- /.box-body -->
                                  </div>
                                </form>
                               
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                           
                        </div>
                        </div>
                      </div>

                 <table class="table">
                      <tr class="bg-gray text-center">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                      <th>Start-End Date</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                  </table>
                
                   </div><!-- /.tab-content -->
              
              </div><!-- nav-tabs-custom -->

            </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->

          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>


