﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmFinanceProject.aspx.vb" Inherits="ODA_VBPrototype.frmFinanceProject" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Project | Finance</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="active treeview">
              <a href="#">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li class="active" ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li class="active"><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Project (โครงการ)
          </h4>
          <ol class="breadcrumb">
            <li class="active"><i class="fa fa-calculator"></i>Finance</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header with-border">
                           <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group"><div class="col-lg-2">
                                        <h5 class="text-primary"><b>พบทั้งหมด 8 รายการ</b></h5>
                                     </div>

                                       <div class="col-sm-2">     
                                        <div class="input-group margin" data-toggle="tooltip" title="เลือกประเภทโครงการ">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>Project</option>
                                          <option>Non-Project</option>
                                          <option>Loan</option>
                                        </select>
                                      </div><!-- /input-group -->
                                      </div>
                                      <div class="col-sm-2">     
                                        <div class="input-group margin" data-toggle="tooltip" title="เลือกปี">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>2560</option>
                                          <option>2559</option>
                                          <option>2558</option>
                                          <option>2557</option>
                                          <option>2556</option>
                                          <option>2555</option>
                                          <option>2554</option>
                                          <option>2553</option>
                                          <option>2552</option>
                                          <option>2551</option>
                                          <option>2550</option>
                                          <option>2549</option>
                                        </select>
                                      </div><!-- /input-group -->
                                      </div>

                                      <div class="col-lg-4">
                                        <div class="input-group margin">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                          <button class="btn btn-info btn-flat" type="button" data-toggle="tooltip" title="ค้นหา"><i class="fa fa-search"></i></button>
                                        </span>
                                      </div><!-- /input-group -->
                                   </div>
                                    <div class="col-lg-2 pull-right">
                                    <div class="input-group margin">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                        <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                        <i class="fa fa-magic"></i>
                                        </button></a>
                                        <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                            <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-print"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                    </div>
                                   
                                  </div><!-- /.box-body -->
                                 </div>
                                </form>
                        </div>
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                        
                       <div id="collap1" class="panel-collapse collapse">
                           
                        <div class="box-header with-border">
                           <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">ID Project :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Projectid" placeholder="">
                                      </div>

                                      
                                      <label for="Sector" class="col-sm-2 control-label">Project Title :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>
                                    </div>
                                   
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Payment Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                  
                                  </div><!-- /.box-body -->
                                 
                                </form>
                        </div>
                        </div>
                     
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray text-center">
                        <th class="text-center">No.</th>
                        <th class="text-center">ID Project</th>
                        <th style="width: 420px" class="text-center">Project</th>
                        <th class="text-center">Start/End Date </th>
                        <th class="text-center">Allocated Budget</th>
                        <th class="text-center">Disbursement</th>
                        <th class="text-center">Payment Date</th>
                        <th style="width: 70px" class="text-center">Tool</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td>201400265</td>
                        <td>โครงการพัฒนาวิทยาลัยศิลปศึกษา สปป.ลาว ประจำปี 2556</td>
                        <td class="text-center">12/01/2555 - 31/05/2557</td>
                        <td class="text-center">884,802.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td>             
                      </tr>
                      <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">201400267</td>
                        <td>โครงการพัฒนาห้องปฎิบัติการวิเคราะห์คุณภาพอาหารสัตว์ ประจำปี 2556</td>
                        <td class="text-center">24/03/2556 - 31/05/2559</td>
                        <td class="text-center">10,000,000.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">201400273</td>
                        <td>โครงการพัฒนาหลักสูตรนานาชาติระดับปริญญาโทสาขาการศึกษาพัฒนาของมหาวิทยาลัยแห่งชาติลาว ประจำปี 2557</td>
                        <td class="text-center">14/07/2557 - 31/07/2560</td>
                        <td class="text-center">10,000,000.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td class="text-center">201400272</td>
                        <td>โครงการพัฒนาหลักสูตรนานาชาติระดับปริญญาโทสาขาการศึกษาพัฒนาของมหาวิทยาลัยแห่งชาติลาว ประจำปี 2552</td>
                        <td class="text-center">25/05/2552 - 31/05/2554</td>
                        <td class="text-center">2,437,602.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">201500020</td>
                        <td>Sufficiency Economy in Agriculture 2013</td>
                        <td class="text-center">02/05/2556 - 01/06/2556</td>
                        <td class="text-center">2,437,602.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">201500013</td>
                        <td>Tropical Emerging and Re-emerging Diseases in Animals; Surveillance and Diagnosis 2012</td>
                        <td class="text-center">12/11/2555 - 03/12/2555</td>
                        <td class="text-center">2,520,794.00</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td class="text-center">201500055</td>
                        <td>Master of Rural Development Management 2011</td>
                        <td class="text-center">1/06/2554 - 31/08/2555</td>
                        <td class="text-center">738,711.50</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      <tr>
                        <td class="text-center">8</td>
                        <td class="text-center">201500148</td>
                        <td>Master of Public Health (Global Health) 2014</td>
                        <td class="text-center">21/08/2557 - 31/08/2558</td>
                        <td class="text-center">840,651.25</td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-navicon text-green"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-search text-blue"></i>  ดูรายละเอียด</a></li>
                              <li><a href="frmDetailFinanceProject.aspx"><i class="fa fa-pencil text-blue"></i>  แก้ไข</a></li>
                              <li class="divider"></li>
                              <li><a href="#"><i class="fa fa-remove text-red"></i>  ลบ</a></li>
                            </ul>
                          </div></td> 
                      </tr>
                      
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
   <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
