﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmUserLevel.aspx.vb" Inherits="ODA_VBPrototype.frmUserLevel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>User Level | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            User Level (ระดับผู้ใช้งาน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">User Level</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header">
                     <p><a href="frmEditUserLevel.aspx"><button class="btn bg-orange btn-flat margin btn-social"><i class="fa fa-plus"></i>Add Status</button></a></p>
                   <div class="col-lg-7">
                     <h4 class="text-primary">พบทั้งหมด 5 รายการ</h4>
                  </div>
                        
                        <div class="input-group col-sm-5 pull-right">
                       <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                     </div>
                    </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class="bg-gray">
                        <th>Group (กลุ่ม)</th>
                        <th style="width: 150px">Amount (จำนวน)</th>
                        <th style="width: 70px">Edit(แก้ไข)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>เจ้าหน้าที่อำนวยสิทธิ์</td>
                        <td>4</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ส่วนการเงิน สพร.</td>
                        <td>20</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>หน่วยงานภายนอก</td>
                        <td>2</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>หน่วยงานภายใน สพร.</td>
                        <td>96</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>Administrator</td>
                        <td>2</td>
                        <td><i class="fa fa-edit text-success"></i> Edit</td>
                        <td><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                   <h4 class="box-title">Access to the program (การเข้าถึงโปรแกรม)</h4>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class="bg-gray">
                        <th style="width: 100px">ลำดับ</th>
                        <th style="width: 150px">Group (กลุ่ม)</th>
                          <th>Detail (รายละเอียด)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>เจ้าหน้าที่อำนวยสิทธิ์</td>
                        <td>ผู้ดูแลระบบทั้งหมด</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>ส่วนการเงิน สพร.</td>
                        <td>เจ้าหน้าที่ภายใน สพร</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>หน่วยงานภายนอก</td>
                        <td>ผู้ลงข้อมูล หน่วยงานภายนอก ที่ไม่ใช่ สพร.</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>หน่วยงานภายใน สพร.</td>
                        <td>ส่วนการเงินและงบประมาณของ สพร.</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Administrator</td>
                        <td>ทำหน้าที่จัดการ project ได้ทั้งหมด เหมือนแอดมินแต่ไม่เห็นข้อมูลเท่าแอดมิน</td>
                      </tr>
                      
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

     <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/demo.js"></script>
   
</asp:Content>

