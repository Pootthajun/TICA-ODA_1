﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmDetailContribuiltion1.aspx.vb" Inherits="ODA_VBPrototype.frmDetailContribuiltion1" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Contribuition | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
  
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
          <a class="text-yellow">CODE ID 201500346 :</a> <a class="text-blue">Contribution to UN Capital Development Fund</a>
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmProjectStatus.aspx"> Project Status</a></li>
            <li><a href="frmContribution.aspx"> Contribuition</a></li>
            <li class="active">Detail Contribution</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-list-alt"></i> Infomation(ข้อมูล)</a></li>
                  <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-th-list"></i> Activity(กิจกรรม)</a></li>
                    <li class="dropdown pull-right" data-toggle="tooltip" title="เครื่องมือ">
                    <a class="dropdown-toggle text-muted" data-toggle="dropdown" href="#">
                      <i class="fa fa-gear"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="frmDetailActivityCon.aspx"><i class="fa fa-edit text-blue"></i> Edit</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-print text-blue"></i> Print</a></li>
                      <li role="presentation" class="divider"></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash text-red"></i> Delete</a></li>
                    </ul>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    
                    <div class="box-body">
                      <table class="table table-bordered">
                         <tr class="bg-info">
                          <th style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Informations (ข้อมูลทั่วไป) </p></h4></th>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Contribution Title (ชื่อโครงการเงินกู้) :</p></td>
                          <td class="text-primary">
                            <textarea class="form-control" rows="3" placeholder="Contribution to UN Capital Development Fund" disabled></textarea></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Description (รายละเอียด) :</p></td>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="-" disabled></textarea></td>
                        </tr>
                        
                       <tr>
                          <td style="width: 250px"><p class="pull-right">Start Date/End Date<br /></p><p class="pull-right"> (วันเริ่มต้น/สิ้นสุดโครงการ) :</p></td>
                          <td class="text-primary">
                            <div class="col-sm-6">
                                <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" class="form-control pull-right" id="reservation" placeholder="1/01/2557-31/07/2557" disabled>
                                 </div><!-- /.input group -->
                              </div></td>
                        </tr>
                        
                        <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Agency (หน่วยงาน) </p></h4></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Funding Agency </p><p class="pull-right">(หน่วยงานให้ความช่วยเหลือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Ministry of foreign affairs</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                         </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Executing Agency</p><p class="pull-right"> (หน่วยงานดำเนินการ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Department of International Organization</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                         <tr>
                          <td style="width: 250px"><p class="pull-right">Multilateral Oraganizations</p><p class="pull-right"> (องค์กรพหุภาคี) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">UNCDF</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Implementing Agency</p><p class="pull-right">(หน่วยงานความร่วมมือ) :</p></td>
                          <td class="text-primary">
                              <div class="col-sm-12">
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">Department of International Organization</option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                  <option></option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                       
                        <tr class="bg-info">
                          <td style="width: 150px" colspan="2">
                          <h4><p class="text-blue">Other Detail (รายละเอียดอื่นๆ) </p></h4></td>
                        </tr><tr>
                          <td style="width: 250px"><p class="pull-right">Assistant (ผู้ช่วยโครงการ) :</p></td>
                          <td class="text-primary">
                                <select class="form-control select2" multiple="multiple" data-placeholder="-" style="width: 100%;" disabled>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                           </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Contact Person (ข้อมูลผู้ติดต่อ) :</p></td>
                              <td class="text-primary">
                                  <table class="table table-bordered">
                                    <tr>
                                      <td style="width: 150px" class="bg-gray"><p class="pull-right">Name (ชื่อ) :</p></td>
                                      <td>
                                        <div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-user"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="sirikul" disabled>
                                       </div></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Position (ตำแหน่ง):</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-building"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Phone (โทรศัพท์) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-phone"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="022035000" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Fax (โทรสาร) :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-fax"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                    <tr>
                                      <td style="width: 50px" class="bg-gray"><p class="pull-right">Email :</p></td>
                                      <td><div class="input-group">
                                         <div class="input-group-addon">
                                           <i class="fa fa-list-alt"></i>
                                         </div>
                                        <input type="text" class="form-control pull-right" placeholder="" disabled>
                                       </div></td>
                                    </tr>
                                   </table></td>
                        </tr>
                        <tr>
                          <td style="width: 150px"><p class="pull-right">Note (หมายเหตุ) :</p></td>
                          <td class="text-primary"><textarea class="form-control" rows="3" placeholder="" disabled></textarea></td>
                        </tr>
                         <tr>
                          <td style="width: 150px"><p class="pull-right">File input (เพิ่มไฟล์) :</p> </td>
                          <td class="text-primary"><input type="file" id="exampleInputFile"> <i class="text-green">(ขนาดไฟล์ไม่เกิน 5 MB)</i><br />
                              <table class="table">
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-pdf-o text-red"></i> ข่าวประชาสัมพันธ์.pdf</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                     <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-file-excel-o text-green"></i> รายชื่อผู้รับทุน.xlsx</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                    <tr>
                                      <td style="width: 250px"><input type="text" class="form-control pull-right" placeholder="" data-toggle="tooltip" title="แก้ไขชื่อไฟล์"></td>
                                      <td style="width: 400px"><i class="fa fa-picture-o"></i> ความช่วยเหลือ.png</td>
                                      <td><i class="fa fa-close  text-red" data-toggle="tooltip" title="ลบไฟล์"></i></td>
                                     </tr>
                                   </table>
                            
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px"><p class="pull-right">Transfer Project To</p><p class="pull-right">(มอบหมายโครงการให้) :</p></td>
                          <td class="text-primary"> 
                             <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-users"></i>
                              </div>
                                <select class="form-control select2" style="width: 100%;" disabled>
                                  <option selected="selected">-</option>
                                  <option>เจนจิรา  ทองมี</option>
                                  <option>ภัทรา  ลีลาทนาพร</option>
                                  <option>อุษา  ชิตวิลัย</option>
                                  <option>อาณัติชัย  สงมา</option>
                                  <option>ธวัชชัย  มูลมาตร</option>
                                  <option>พัชรินทร์  อาบครบุรี</option>
                                  <option>ตะวัน  เวียงธงษารักษ์</option>
                                </select>
                                </div><!-- /.form-group --></td>
                        </tr>
                      </table>
                  <div class="box-footer">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="#" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                        </div>
                        
                    </div>
                    </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="tab_2">

                    <div class="box-header with-border">        
                      <div class="col-sm-8">
                                <p><a data-toggle="modal" data-target="#AddCourseContribuiltion">
                                <button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Activity</button></a></p> </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Year" class="btn btn-block btn-social btn-info"><i class="fa fa-calendar"></i>Year </a>
                        </div>
                        <div class="col-sm-2">
                          <a data-toggle="collapse" data-parent="#accordion" href="#Month" class="btn btn-block btn-social btn-primary"><i class="fa fa-calendar"></i>Month </a>
                        </div>

                      </div>
                <div id="Year" class="panel-collapse collapse active">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr>
                        <th colspan="8" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="7" class="text-center bg-gray">Year (ปี)</th>
                    </tr>
                    <tr>
                       
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th style="width: 800px" class="bg-info">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Butget</th>
                      <th class="bg-info">Tool</th>

                      <th class="bg-gray">2552</th>
                      <th class="bg-gray">2553</th>
                      <th class="bg-gray">2554</th>
                      <th class="bg-gray">2555</th>
                      <th class="bg-gray">2556</th>
                      <th class="bg-gray">2557</th>
                      <th class="bg-gray">2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txt1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="40" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>50,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="4">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="3"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท.</td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>25,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน</td>
                      <td>01/01/2553</td>
                      <td>01/01/2555</td>
                      <td>1064 วัน</td>
                      <td>25,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="1"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="3"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 2 แผนงานถ่ายทอดความรู้</td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>100,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="2"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี</td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>100,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="2"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 3 แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด</td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>2,050,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="3"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="1"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td style="width: 800px">กิจกรรมที่ 4 แผนงานติดตามและประเมินผล</td>
                      <td>01/01/2555</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>1,000,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="3"></td>
                      <td colspan="4">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง</td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>500,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="3"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="1"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td style="width: 800px">4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา</td>
                      <td>01/01/2556</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>500,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="4"></td>
                      <td colspan="3">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                  </table>
                </div>
               </div>

                <div id="Month" class="panel-collapse collapse">
                <div style="overflow-x:auto;"> 
                 <table class="table table-bordered" style="width: 100%">
                    <tr class="bg-gray text-center">
                        <th colspan="8" class="text-center bg-info">Detail (รายละเอียด)</th>
                        <th colspan="84" class="text-center bg-gray">Month (เดือน)</th>
                    </tr>
                    <tr class="bg-gray">
                      <th class="bg-info"></th>
                      <th class="bg-info"></th>
                      <th class="bg-info" style="width: 70%;">Course (หลักสูตร)</th>
                      
                      <th class="bg-info">Start Date</th>
                      <th class="bg-info">End Date</th>
                      <th class="bg-info">Duration</th>
                      <th class="bg-info">Budget</th>
                      <th class="bg-info">Tool</th>
                        
                      <th class="bg-gray text-center">January 2552</th>
                      <th class="bg-gray text-center">February 2552</th>
                      <th class="bg-gray text-center">March 2552</th>
                      <th class="bg-gray text-center">April 2552</th>
                      <th class="bg-gray text-center">May 2552</th>
                      <th class="bg-gray text-center">June 2552</th>
                      <th class="bg-gray text-center">July 2552</th>
                      <th class="bg-gray text-center">August 2552</th>
                      <th class="bg-gray text-center">September 2552</th>
                      <th class="bg-gray text-center">October 2552</th>
                      <th class="bg-gray text-center">November 2552</th>
                      <th class="bg-gray text-center">December 2552</th>
                      <th class="bg-gray text-center">January 2553</th>
                      <th class="bg-gray text-center">February 2553</th>
                      <th class="bg-gray text-center">March 2553</th>
                      <th class="bg-gray text-center">April 2553</th>
                      <th class="bg-gray text-center">May 2553</th>
                      <th class="bg-gray text-center">June 2553</th>
                      <th class="bg-gray text-center">July 2553</th>
                      <th class="bg-gray text-center">August 2553</th>
                      <th class="bg-gray text-center">September 2553</th>
                      <th class="bg-gray text-center">October 2553</th>
                      <th class="bg-gray text-center">November 2553</th>
                      <th class="bg-gray text-center">December 2553</th>
                      <th class="bg-gray text-center">January 2554</th>
                      <th class="bg-gray text-center">February 2554</th>
                      <th class="bg-gray text-center">March 2554</th>
                      <th class="bg-gray text-center">April 2554</th>
                      <th class="bg-gray text-center">May 2554</th>
                      <th class="bg-gray text-center">June 2554</th>
                      <th class="bg-gray text-center">July 2554</th>
                      <th class="bg-gray text-center">August 2554</th>
                      <th class="bg-gray text-center">September 2554</th>
                      <th class="bg-gray text-center">October 2554</th>
                      <th class="bg-gray text-center">November 2554</th>
                      <th class="bg-gray text-center">December 2554</th>
                      <th class="bg-gray text-center">January 2555</th>
                      <th class="bg-gray text-center">February 2555</th>
                      <th class="bg-gray text-center">March 2555</th>
                      <th class="bg-gray text-center">April 2555</th>
                      <th class="bg-gray text-center">May 2555</th>
                      <th class="bg-gray text-center">June 2555</th>
                      <th class="bg-gray text-center">July 2555</th>
                      <th class="bg-gray text-center">August 2555</th>
                      <th class="bg-gray text-center">September 2555</th>
                      <th class="bg-gray text-center">October 2555</th>
                      <th class="bg-gray text-center">November 2555</th>
                      <th class="bg-gray text-center">December 2555</th>
                      <th class="bg-gray text-center">January 2556</th>
                      <th class="bg-gray text-center">February 2556</th>
                      <th class="bg-gray text-center">March 2556</th>
                      <th class="bg-gray text-center">April 2556</th>
                      <th class="bg-gray text-center">May 2556</th>
                      <th class="bg-gray text-center">June 2556</th>
                      <th class="bg-gray text-center">July 2556</th>
                      <th class="bg-gray text-center">August 2556</th>
                      <th class="bg-gray text-center">September 2556</th>
                      <th class="bg-gray text-center">October 2556</th>
                      <th class="bg-gray text-center">November 2556</th>
                      <th class="bg-gray text-center">December 2556</th>
                      <th class="bg-gray text-center">January 2557</th>
                      <th class="bg-gray text-center">February 2557</th>
                      <th class="bg-gray text-center">March 2557</th>
                      <th class="bg-gray text-center">April 2557</th>
                      <th class="bg-gray text-center">May 2557</th>
                      <th class="bg-gray text-center">June 2557</th>
                      <th class="bg-gray text-center">July 2557</th>
                      <th class="bg-gray text-center">August 2557</th>
                      <th class="bg-gray text-center">September 2557</th>
                      <th class="bg-gray text-center">October 2557</th>
                      <th class="bg-gray text-center">November 2557</th>
                      <th class="bg-gray text-center">December 2557</th>
                      <th class="bg-gray text-center">January 2558</th>
                      <th class="bg-gray text-center">February 2558</th>
                      <th class="bg-gray text-center">March 2558</th>
                      <th class="bg-gray text-center">April 2558</th>
                      <th class="bg-gray text-center">May 2558</th>
                      <th class="bg-gray text-center">June 2558</th>
                      <th class="bg-gray text-center">July 2558</th>
                      <th class="bg-gray text-center">August 2558</th>
                      <th class="bg-gray text-center">September 2558</th>
                      <th class="bg-gray text-center">October 2558</th>
                      <th class="bg-gray text-center">November 2558</th>
                      <th class="bg-gray text-center">December 2558</th>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm1" value="กิจกรรมที่ 1 แผนงานพัฒนาบุคลากรของวิทยาลัย" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2555</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm11" value="1.1 ทุนดูงานสำหรับผู้บริหาร 7 คน ณ ปทท." size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2552</td>
                      <td>01/01/2554</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="48"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm12" value="1.2 ทุนศึกษา ป.ตรี 2 ทุน ป.โท 2 ทุน/ปี รวม 4 ทุน" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>03/01/2553</td>
                      <td>03/10/2555</td>
                      <td>1064 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="12"></td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm2" value="กิจกรรมที่ 2 แผนงานถ่ายทอดความรู้" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="24"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm21" value="2.1 ส่ง ผชช. ไทย สาขาละ 2 คน ไปนิเทศน์การถ่ายทอดความรู้ ณ สปป.ลาว 2 ครั้ง/ปี" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2554</td>
                      <td>01/01/2556</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="24"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="36"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm3" value="กิจกรรมที่ 3 แผนงานสนับสนุนอุปกรณ์และสื่อการเรียนการสอนและคอมพิวเตอร์ 41 ชุด" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="36"></td>
                      <td colspan="24">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="24"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><a class="icon fa fa-plus"></a></td>
                      <td><input type="text" id="txtm4" value="กิจกรรมที่ 4 แผนงานติดตามและประเมินผล" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="36"></td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm41" value="4.1 การประชุม PMC ทุกปี ๆ ละ 1 ครั้ง" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2555</td>
                      <td>01/01/2557</td>
                      <td>1065 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="36"></td>
                      <td colspan="36">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="12"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a class="icon fa fa-minus"></a></td>
                      <td><input type="text" id="txtm42" value="4.2 การติดตามประเมินผลโครงการ หลังจากสิ้นสุดระยะเวลา" size="60" style="border:none; cursor:default;" readonly /></td>
                      <td>01/01/2556</td>
                      <td>01/01/2558</td>
                      <td>1430 วัน</td>
                      <td>10,000</td>
                      <td class="text-center">
                        <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <i class="fa fa-navicon text-green"></i>
                          </button>
                             <ul class="dropdown-menu pull-right">
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-plus text-blue"></i>เพิ่ม</a></li>
                             <li><a data-toggle="modal" data-target="#AddCourseContribuiltion"><i class="fa fa-pencil text-blue"></i>แก้ไข</a></li>
                             <li class="divider"></li>
                             <li><a href="#"><i class="fa fa-remove text-red"></i>ลบ</a></li>
                            </ul>
                       </div>
                     </td>
                     <td colspan="36"></td>
                      <td colspan="48">
                          <div class="progress">
                          <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div></td>
                     <td colspan="10"></td>
                    </tr>
                  </table>
                </div>
               </div> 
             </div>
                  <%--<div class="tab-pane" id="tab_2">
                  <div id="gantt_here" style='width:1060px; height:480px;'></div>  
                  
                  <br />
                   <script src="dist/Gantter/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
                   <link rel="stylesheet" href="dist/Gantter/codebase/skins/dhtmlxgantt_meadow.css" type="text/css" media="screen" title="no title" charset="utf-8">

                    <script type="text/javascript" src="dist/Gantter/common/testdata.js"></script>

                        <script type="text/javascript">
	                    gantt.init("gantt_here");

	                    gantt.templates.rightside_text = function(start, end, task){
		                    return "ID: #" + task.id;
	                    };

	                    gantt.templates.leftside_text = function(start, end, task){
		                    return task.duration + " days";
	                    };
	                    gantt.parse(demo_tasks);
                    </script>

                    <script type="text/javascript">
                            var demo_tasks = {
                                "data": [
                                    { "id": 10, "text": "ค่าบำรุงสมาชิกศูนย์พยากรณ์อากาศโลก Exeter ระบบ SADIS ประจำปี 2558", "start_date": "06-10-2016", "duration": "30", "progress": 0.6, "open": true },
                
                                    { "id": 11, "text": "ค่าบำรุงสมาชิกคณะกรรมการไต้ฝุ่น ZESCAP/WMO Typhoon Committree Trust fund) ประจำปี 2558", "start_date": "03-10-2016", "duration": "40", "progress": 1, "open": true },
                                    { "id": 12, "text": "ค่าบำรุงสมชิกคณะกรรมการพายุไซโคลนเขตร้อน", "start_date": "16-11-2016", "duration": "30", "progress": 0.8, "open": true },
                                    { "id": 13, "text": "เงินอุดหนุนสมาชิก", "start_date": "1-12-2016", "duration": "31", "progress": 0.2, "open": true },
                                    { "id": 14, "text": "ค่าบำรุงรัฐสมาชิก", "start_date": "15-12-2016", "duration": "20", "progress": 0, "open": true },
                                    { "id": 15, "text": "ค่าบำรุงสหภาพสากลไปรษณีย์", "start_date": "13-01-2017", "duration": "24", "progress": 0.5, "open": true },


                                    { "id": 16, "text": "ค่าบำรุงกลุ่มภาษาอังกฤษ", "start_date": "07-01-2017", "duration": "", "progress": 0.6, "open": true },
                                    { "id": 17, "text": "ค่าบำรุงสมทบกองทุนพิเศษสหภาพสากลไปรษณีย์", "start_date": "29-02-2017", "duration": "4", "progress": 0.1, "open": true },
                                    { "id": 18, "text": "ค่าบำรุงสหภาพไปรษณีย์แห่งเอเชียและแปซิฟิก", "start_date": "13-03-2017", "duration": "5", "progress": 0, "open": true },

                                    { "id": 19, "text": "ค่าบำรุงวิทยาลัยการไปรษณีย์แห่งเอเชียและแปซิฟิก", "start_date": "06-03-2017", "duration": "", "progress": 0.6, "open": true },

                                ]
                            };

                            gantt.init("gantt_here");

                            gantt.parse(demo_tasks);


                        </script>
                
                     
                  </div><!-- /.tab-pane -->--%>
                  <%--<div class="tab-pane" id="tab_3">
                  <div class="box-header">
                     <p><a href="#"><button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Recipient</button></a></p>
                       <div class="col-lg-7">
                           <h4 class="text-primary">พบทั้งหมด 4 รายการ</h4>
                         </div>
                       
                     <div class="input-group col-sm-5 pull-right">
                       <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                     </div>

                  <table class="table">
                    <tr class="bg-gray">
                      <th style="width: 100px">No.(ลำดับ)</th>
                      <th>Country (ประเทศ)</th>
                      <th style="width: 250px">Recipient Country</b><br /><b class="pull-right"> (ประเทศที่รับทุน)</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>ลาว (Laos)</td>
                      <td>ศรีหวงศ์ คนสวรรค์</td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>ลาว (Laos)</td>
                      <td>สุขพล วงเชียงคู</td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>ไทย (Thailand)</td>
                      <td>สมชาย  รักชาติ</td>
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>ไทย (Thailand)</td>
                      <td>วีรยุทธ ไชยบุญชู</td>
                    </tr>
                  </table>
                  </div><!-- /.tab-pane -->
                </div>--%><!-- /.tab-content -->
              
              </div><!-- nav-tabs-custom -->

            </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->

          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />

</asp:Content>
