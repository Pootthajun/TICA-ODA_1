﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmProjectStatus.aspx.vb" Inherits="ODA_VBPrototype.frmProjectStatus" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Project | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li>
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            Project Status (สถานะโครงการปี 2559)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li><a href="frmProjectStatus.aspx">Project Status</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
           
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <font size="5">Project</font>
                  <h4>180 โครงการ</h4>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="frmProject.aspx" class="small-box-footer">More info... <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <font size="5">Non Project</font>
                  <h4>738 โครงการ</h4>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="frmNonProject.aspx" class="small-box-footer">More info... <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <font size="5">Loan</font>
                  <h4>16 โครงการ</h4>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="frmLoan.aspx" class="small-box-footer">More info... <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <font size="5">Contribution</font>
                  <h4>115 โครงการ</h4>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="frmContribuiltion.aspx" class="small-box-footer">More info...<i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
          <div class="row">
              
            <div class="col-md-6">
               <!-- BAR CHART -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">จำนวนโครงการทั้งหมด</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-defult btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-defult btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><br />
                  
                     <div class="col-md-8">
                    <h5 class="pull-right">ค้นหาตามปี</h5>
                 
                    </div>
                   <div class="col-md-4">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ทั้งหมด</option>
                      <option>2560</option>
                      <option>2559</option>
                      <option>2558</option>
                      <option>2557</option>
                      <option>2556</option>
                      <option>2555</option>
                      <option>2554</option>
                      <option>2553</option>
                      <option>2552</option>
                      <option>2551</option>
                      <option>2550</option>
                      <option>2549</option>
                    </select>
                    </div>
                    
                  <div class="clearfix"></div><br /><br />
                <div class="box-body chart-responsive">
                  <div class="chart" id="bar-chart" style="height: 230px;"></div>
                </div><!-- /.box-body -->
                
                <div class="box-footer clearfix"><a href="frmTotalProject.aspx">
                   <button class="pull-right btn btn-default" id="seemore">ดูทั้งหมด <i class="fa fa-arrow-circle-right"></i></button></a>
               </div>
              </div><!-- /.box -->

            </div><!-- /.col -->

              <div class="col-md-6">
              <!-- LINE CHART -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h5 class="box-title">Project Component (ประเภทความช่วยเหลือ)</h5>
                  <div class="box-tools pull-right">
                    <button class="btn btn-defult btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-defult btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                 <div class="box-body">
                  <div class="row">
                     <div class="col-md-8">
                    <h5>Top 5 Component (5 อันดับ ประเภทความช่วยเหลือ)</h5>
                 
                    </div>

                      <div class="col-md-4">
                    <select class="form-control select2" style="width: 100%;">
                      <option>Select Year</option>
                      <option>2560</option>
                      <option selected="selected">2559</option>
                      <option>2558</option>
                      <option>2557</option>
                      <option>2556</option>
                      <option>2555</option>
                      <option>2554</option>
                      <option>2553</option>
                      <option>2552</option>
                      <option>2551</option>
                      <option>2550</option>
                      <option>2549</option>
                    </select>
                    </div>
                  <div class="clearfix"></div><br />
                    <div class="col-md-12">
                      <div class="chart-responsive">
	                    <div id="chartContainer1" style="height: 2px; width: 0%;"></div>
                      </div><!-- ./chart-responsive -->
                    </div><!-- /.col -->
                   
                    <div class="col-sm-12">
                      <!-- Progress bars -->
                      <div class="clearfix">
                        <span class="pull-left">1. Master</span>
                        <p class="pull-right">630 โครงการ</p>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 87%;"></div>
                      </div>

                     <div class="clearfix">
                        <span class="pull-left">2. Bachelor</span>
                        <p class="pull-right">520 โครงการ</p>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 75%;"></div>
                      </div>

                      <div class="clearfix">
                        <span class="pull-left">3. Meeting</span>
                        <p class="pull-right">400 โครงการ</p>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 60%;"></div>
                      </div>

                    <div class="clearfix">
                        <span class="pull-left">4. Certificate</span>
                        <p class="pull-right">384 โครงการ</p>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 45%;"></div>
                      </div>

                    <div class="clearfix">
                        <span class="pull-left">5. PhD.</span>
                        <p class="pull-right">256 โครงการ</p>
                      </div>
                      <div class="progress sm">
                        <div class="progress-bar progress-bar-light-blue" style="width: 30%;"></div>
                      </div>
                    </div><!-- /.col -->
                   </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                      <button class="pull-right btn btn-default" id="more">ดูทั้งหมด <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                  </div><!-- /.row -->
                </div><!-- /.box-body -->

                </div>
            <%--<div class="col-md-6">
              <!-- LINE CHART -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Line Chart</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                 <div class="box-body">
                  <div class="row">
                      <div class="col-md-6">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Alabama</option>
                      <option>Alaska</option>
                      <option>California</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select>
                    </div>
                    <div class="col-md-6">
                    <!-- Date dd/mm/yyyy -->
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div><!-- /.input group -->
                      </div><!-- /.form group -->
                    </div>
                  <div class="clearfix"></div><br />
                    <div class="col-md-8">
                      <div class="chart-responsive">
	                    <div id="chartContainer1" style="height: 250px; width: 100%;"></div>
                      </div><!-- ./chart-responsive -->
                    </div><!-- /.col -->
                    <div class="col-md-4"><br /><br /><br /><br />
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle text-aqua"></i>  Project</li>
                        <li><i class="fa fa-circle text-green"></i>   Non-Project</li>
                        <li><i class="fa fa-circle text-yellow"></i>  Loan</li>
                        <li><i class="fa fa-circle text-red"></i>  Contribuition</li>
                      </ul><br /><br /><br />
                    </div><!-- /.col -->
                    <%--<div class="col-md-12"><br />
                      
                    <div class="box-header with-border">
                      <h3 class="box-title">Line Chart</h3>
                    </div><br />
                   <div class="progress-group">
                        <span class="progress-text">Poject</span>
                        <span class="progress-number"><b>160</b>/200</span>
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-info" style="width: 80%"></div>
                        </div>
                      </div><!-- /.progress-group -->
                      <div class="progress-group">
                        <span class="progress-text">Non Project</span>
                        <span class="progress-number"><b>310</b>/400</span>
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                        </div>
                      </div><!-- /.progress-group -->
                      <div class="progress-group">
                        <span class="progress-text">Loan</span>
                        <span class="progress-number"><b>480</b>/800</span>
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                        </div>
                      </div><!-- /.progress-group -->
                      <div class="progress-group">
                        <span class="progress-text">Contribution</span>
                        <span class="progress-number"><b>250</b>/500</span>
                        <div class="progress sm">
                          <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                        </div>
                      </div><!-- /.progress-group -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
                 
                  </div><!-- /.row -->
                </div><!-- /.box-body -->

                </div>--%>
           
            
          </div><!-- /.row -->
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->

  
<!-- Morris charts -->
    <link rel="stylesheet" href="plugins/morris/morris.css">

  <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>--%>
    <script src="plugins/morris/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- FastClick -->
  <%--      <script src="plugins/fastclick/fastclick.min.js"></script>--%>





    <script>
      $(function () {
        "use strict";

        //BAR CHART
        var bar = new Morris.Bar({
          element: 'bar-chart',
          resize: true,
          data: [
            //{ y: '2555', a: 1175, b: 1365,c: 680,d:130},
            { y: '2556', a: 1100, b: 790,c:1080,d:145},
            { y: '2557', a: 1950, b: 1140,c: 1580,d:179},
            { y: '2558', a: 1175, b: 1365, c: 680, d: 130 },
            { y: '2559', a: 1950, b: 1140, c: 1580, d: 179 },
            { y: '2560', a: 1175, b: 1365, c: 680, d: 130 },
          ],
          barColors: ['#00c0ef', '#00a65a', '#f39c12', '#dd4b39'],
          xkey: 'y',
          ykeys: ['a', 'b','c','d'],
          labels: ['Project', 'Non-Project','Loan','Contribuition'],
          hideHover: 'auto'
        });

     //DONUT CHART
        //var donut = new Morris.Donut({
        //  element: 'sales-chart',
        //  resize: true,
        //  colors: ['#00c0ef', '#00a65a', '#dd4b39', '#f39c12'],
        //  data: [
        //    { label: "Project", value: 120000000 },
        //    { label: "Non-Project", value: 120000000 },
        //    {label: "Contribution", value: 6520000},
        //    {label: "Loan", value: 30000000}
        //  ],
        //  hideHover: 'auto'
        //});
      });
    </script>
    <script type="text/javascript">
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer1",
	        {

	            data: [
		        {
		            type: "pie",
		            dataPoints: [
				        { y: 4181563 },
				        { y: 2175498 },
				        { y: 3125844 },
				        { y: 1717786 }
		            ]
		        }
	            ]
	        });
            chart.render();

      }
	</script>


	<script src="dist/canvasjs/canvasjs.min.js"></script>
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />


</asp:Content>

