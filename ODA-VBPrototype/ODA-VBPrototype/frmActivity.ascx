﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmActivity.ascx.vb" Inherits="ODA_VBPrototype.frmActivity" %>

<link rel="stylesheet" href="dist/modal/modal.css">

<div class="container">
 
      <!--------------------------------------------- ModalAddCourse------------------------------------------>
  <div class="modal fade" id="AddCourse" role="dialog">
      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#InfoPro" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#AllocatedPro" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                  <li class="bg-aqua"><a href="#RecipientPro" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="InfoPro">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Course (หลักสูตร) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="การประชุมคณะกรรมการดำเนินการโครงการฯฝ่ายไทยและกัมพูชา"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Description (รายละเอียด) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="มีผู้รับทุนเป็นคณะผู้บริหาร ครู นักเรียนและประชาชนในชุมชนบริเวณใกล้เคียงวิทยาลัย กำปงเฌอเตียล ราชอาณาจักรกัมพูชา"></textarea></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sector (สาขา) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;" >
                                                                  <option selected="selected">-</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sub Sector (สาขาย่อย) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">-</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">Plan(Start/End Date)</small><small class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanPro" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Actual (Start/End Date)</small><small class="pull-right">(วันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                              <div class="input-group">
                                                               <div class="input-group-addon">
                                                                  <i class="fa fa-calendar"></i>
                                                                </div>
                                                               <input type="text" class="form-control pull-right" id="ActualPro" placeholder="21/01/2015-21/01/2015">
                                                             </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="width: 180px"><p class="pull-right">ประกาศรับสมัครผู้รับทุน :</p></td>
                                                            <td><div class="form-group">
                                                                  <div class="col-sm-9">
                                                                 <label><input type="checkbox" class="minimal"></label>
                                                                  </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                      </table>
                                                  </div>

                                                  <div class="tab-pane" id="AllocatedPro">
                                                      <table class="table table-bordered">
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">In kind (ความช่วยเหลืออื่นๆ):</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">In kind</td>
                                                                  <td style="width: 340px" colspan="2">Estimated</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Estimated1" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">Project Component</small><small class="pull-right">(ประเภทความช่วยเหลือ) :</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">Component</td>
                                                                  <td style="width: 340px" colspan="2">Budget</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Budget1" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                       
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Commitment Budget </small><small class="pull-right">(จัดสรรงบประมาณ) :</small></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="BudgetPro" placeholder="2,000,000,000 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><p class="pull-right">Disbursement (จ่ายจริง):</p></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="DisbursementPro" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Administrative </small><small class="pull-right">(ค่าบริหารจัดการ):</small></td>
                                                          <td class="text-success">
                                                            <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="AdministrativePro" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        
                                                      </table>
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="RecipientPro">
                    
                                                   <table class="table table-bordered">
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Person</p><p class="pull-right"> (บุคคลที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Person" style="width: 100%;">
                                                              <option>เจนจิรา  ทองมี</option>
                                                              <option>ภัทรา  ลีลาทนาพร</option>
                                                              <option>อุษา  ชิตวิลัย</option>
                                                              <option>อาณัติชัย  สงมา</option>
                                                              <option>ธวัชชัย  มูลมาตร</option>
                                                              <option>พัชรินทร์  อาบครบุรี</option>
                                                              <option>ตะวัน  เวียงธงษารักษ์</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Country</p><p class="pull-right"> (ประเทศที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Cambodia" style="width: 100%;">
                                                              <option>Thailand</option>
                                                              <option>Lao</option>
                                                              <option>Malaysia</option>
                                                              <option></option>
                                                              <option>Vietnam</option>
                                                              <option>Singapore</option>
                                                              <option>Chinese</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Benificiary</p><p class="pull-right"> (ผู้รับผลประโยชน์) :</p></td>
                                                      <td class="">
                                                        <textarea class="form-control" rows="3" placeholder=""></textarea>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </div><!-- /.tab-pane -->
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

    </div>

    
      <!--------------------------------------------- ModalAddCourse------------------------------------------>
  <div class="modal fade" id="AddCoursePro2" role="dialog">
      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#InfoPro1" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#AllocatedPro1" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                  <li class="bg-aqua"><a href="#RecipientPro1" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="InfoPro1">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Course (หลักสูตร) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="1.2 จัดฝึกอบรมหลักสูตรเฉพาะทางระยะสั้น 2 "></textarea></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Description (รายละเอียด) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="(1) การใช้อุปกรณ์ในห้องวิจัยสำหรับการวิเคราะห์คุณภาพอาหารสัตว์และการตรวจหาสารพิษในอาหารสัตว์ เช่น ไซยาไนต์ "></textarea></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sector (สาขา) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;" >
                                                                  <option selected="selected">EDUCATION 110</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sub Sector (สาขาย่อย) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">-</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Plan(Start/End Date)</small><small class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanPro1" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Actual (Start/End Date)</small><small class="pull-right">(วันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                              <div class="input-group">
                                                               <div class="input-group-addon">
                                                                  <i class="fa fa-calendar"></i>
                                                                </div>
                                                               <input type="text" class="form-control pull-right" id="ActualPro1" placeholder="21/01/2015-21/01/2015">
                                                             </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="width: 180px"><p class="pull-right">ประกาศรับสมัครผู้รับทุน :</p></td>
                                                            <td><div class="form-group">
                                                                  <div class="col-sm-9">
                                                                 <label><input type="checkbox" class="minimal"></label>
                                                                  </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                      </table>
                                                  </div>

                                                  <div class="tab-pane" id="AllocatedPro1">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">In kind (ความช่วยเหลืออื่นๆ):</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">In kind</td>
                                                                  <td style="width: 340px" colspan="2">Estimated</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Estimated2" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">Project Component</small><small class="pull-right">(ประเภทความช่วยเหลือ) :</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">Component</td>
                                                                  <td style="width: 340px" colspan="2">Budget</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Budget2" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Commitment Budget </small><small class="pull-right">(จัดสรรงบประมาณ) :</small></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="BudgetPro1" placeholder="2,000,000,000 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><p class="pull-right">Disbursement (จ่ายจริง):</p></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="DisbursementPro1" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Administrative </small><small class="pull-right">(ค่าบริหารจัดการ):</small></td>
                                                          <td class="text-success">
                                                            <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="AdministrativePro1" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                         
                                                      </table>
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="RecipientPro1">
                    
                                                   <table class="table table-bordered">
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Person</p><p class="pull-right"> (บุคคลที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Person" style="width: 100%;">
                                                              <option>เจนจิรา  ทองมี</option>
                                                              <option>ภัทรา  ลีลาทนาพร</option>
                                                              <option>อุษา  ชิตวิลัย</option>
                                                              <option>อาณัติชัย  สงมา</option>
                                                              <option>ธวัชชัย  มูลมาตร</option>
                                                              <option>พัชรินทร์  อาบครบุรี</option>
                                                              <option>ตะวัน  เวียงธงษารักษ์</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Country</p><p class="pull-right"> (ประเทศที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Cambodia" style="width: 100%;">
                                                              <option>Thailand</option>
                                                              <option>Lao</option>
                                                              <option>Malaysia</option>
                                                              <option></option>
                                                              <option>Vietnam</option>
                                                              <option>Singapore</option>
                                                              <option>Chinese</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Benificiary</p><p class="pull-right"> (ผู้รับผลประโยชน์) :</p></td>
                                                      <td class="">
                                                        <textarea class="form-control" rows="3" placeholder=""></textarea>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </div><!-- /.tab-pane -->
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

                            </div>

    <!--------------------------------------------- AddCourseNon1------------------------------------------>
  <div class="modal fade" id="AddCourseNon1" role="dialog">
      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#InfoNon1" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#AllocatedNon1" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                  <li class="bg-aqua"><a href="#RecipientNon1" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="InfoNon1">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Course (หลักสูตร) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="แผนที่ 1 จัดฝึกอบรมหลักสูตรเฉพาะทางระยะสั้น จำนวน 17 คน เกี่ยวกับ  Sufficiency Economy in Agriculture"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Description </p><p class="pull-right">(รายละเอียด) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder=""></textarea></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sector (สาขา) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;" >
                                                                  <option selected="selected">AGRICULTURE (311)</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sub Sector (สาขาย่อย) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">Agricultural policy and administrative management 31110</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Plan(Start/End Date)</small><small class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanNon1" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Actual (Start/End Date)</small><small class="pull-right">(วันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                              <div class="input-group">
                                                               <div class="input-group-addon">
                                                                  <i class="fa fa-calendar"></i>
                                                                </div>
                                                               <input type="text" class="form-control pull-right" id="ActualNon1" placeholder="21/01/2015-21/01/2015">
                                                             </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="width: 180px"><p class="pull-right">ประกาศรับสมัครผู้รับทุน:</p></td>
                                                            <td><div class="form-group">
                                                                  <div class="col-sm-9">
                                                                 <label><input type="checkbox" class="minimal"></label>
                                                                  </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                      </table>
                                                  </div>

                                                  <div class="tab-pane" id="AllocatedNon1">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">In kind (ความช่วยเหลืออื่นๆ):</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">In kind</td>
                                                                  <td style="width: 340px" colspan="2">Estimated</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Estimated3" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">Activity Component </small><small class="pull-right">(กิจกรรมความช่วยเหลือ) :</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">Component</td>
                                                                  <td style="width: 340px" colspan="2">Budget</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Budget3" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Commitment Budget </small><small class="pull-right">(จัดสรรงบประมาณ) :</small></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="BudgetNon1" placeholder="2,000,000,000 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><p class="pull-right">Disbursement (จ่ายจริง):</p></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="DisbursementNon1" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Administrative </small><small class="pull-right">(ค่าบริหารจัดการ):</small></td>
                                                          <td class="text-success">
                                                            <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="AdministrativeNon1" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                         
                                                      </table>
                                                      
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="RecipientNon1">
                    
                                                   <table class="table table-bordered">
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Person</p><p class="pull-right"> (บุคคลที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Person" style="width: 100%;">
                                                              <option>เจนจิรา  ทองมี</option>
                                                              <option>ภัทรา  ลีลาทนาพร</option>
                                                              <option>อุษา  ชิตวิลัย</option>
                                                              <option>อาณัติชัย  สงมา</option>
                                                              <option>ธวัชชัย  มูลมาตร</option>
                                                              <option>พัชรินทร์  อาบครบุรี</option>
                                                              <option>ตะวัน  เวียงธงษารักษ์</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Country</p><p class="pull-right"> (ประเทศที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Cambodia" style="width: 100%;">
                                                              <option>Cambodia </option>
                                                              <option>China </option>
                                                              <option>Madagascar </option>
                                                              <option>Malawi </option>
                                                              <option>Maldives</option>
                                                              <option>Morocco </option>
                                                              <option>Mozambique </option>
                                                              <option>Myanmar  </option>
                                                              <option>Nepal </option>
                                                              <option>Philippines  </option>
                                                              <option>Samoa  </option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Benificiary</p><p class="pull-right"> (ผู้รับผลประโยชน์) :</p></td>
                                                      <td class="">
                                                        <textarea class="form-control" rows="3" placeholder=""></textarea>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </div><!-- /.tab-pane -->
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

                            </div>

    <!--------------------------------------------- AddCourseNon2------------------------------------------>
  <div class="modal fade" id="AddCourseNon2" role="dialog">
      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#InfoNon2" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#AllocatedNon2" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                  <li class="bg-aqua"><a href="#RecipientNon2" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="InfoNon2">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Course (หลักสูตร) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder="แผนที่1 จัดฝึกอบรมหลักสูตรเฉพาะทางระยะสั้น จำนวน 17 คน เกี่ยวกับ   Tropical Emerging and Re-emerging Diseases in Animals; Surveillance and Diagnosis"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Description </p><p class="pull-right">(รายละเอียด) :</p></td>
                                                          <td class=""><textarea class="form-control" rows="3" placeholder=""></textarea></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sector (สาขา) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;" >
                                                                  <option selected="selected">AGRICULTURE (311)</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sub Sector (สาขาย่อย) :</p></td>
                                                          <td class="">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">Agricultural policy and administrative management 31110</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Plan(Start/End Date)</small><small class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanNon2" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Actual (Start/End Date)</small><small class="pull-right">(วันเริ่มต้น/สิ้นสุด):</small></td>
                                                          <td class="">
                                                              <div class="input-group">
                                                               <div class="input-group-addon">
                                                                  <i class="fa fa-calendar"></i>
                                                                </div>
                                                               <input type="text" class="form-control pull-right" id="ActualNon2" placeholder="21/01/2015-21/01/2015">
                                                             </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="width: 180px"><p class="pull-right">ประกาศรับสมัครผู้รับทุน:</p></td>
                                                            <td><div class="form-group">
                                                                  <div class="col-sm-9">
                                                                 <label><input type="checkbox" class="minimal"></label>
                                                                  </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                      </table>
                                                  </div>

                                                  <div class="tab-pane" id="AllocatedNon2">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">In kind (ความช่วยเหลืออื่นๆ):</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">In kind</td>
                                                                  <td style="width: 340px" colspan="2">Estimated</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Estimated4" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 160px"><small class="pull-right">Activity Component</small><small class="pull-right">(กิจกรรมความช่วยเหลือ) :</small></td>
                                                          <td class="">
                                                            <table class="table table-bordered">
                                                                <tr class="bg-gray">
                                                                  <td style="width: 150px">Component</td>
                                                                  <td style="width: 340px" colspan="2">Budget</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected"></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select></td>
                                                                <td><div class="input-group">
                                                                 <div class="input-group-addon">
                                                                   <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Budget4" placeholder="0.00 บาท">
                                                               </div></td>
                                                              <td style="width: 50px"><a class="btn btn-social-icon btn-google btn-sm" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pull-right"><b>รวมทั้งสิ้น</b></td>
                                                                <td>0.00 บาท</td>
                                                                <td  style="width: 50px"><a class="btn btn-social-icon btn-linkedin btn-sm" data-toggle="tooltip" title="ความช่วยเหลือด้านอื่นๆ"><i class="fa fa-plus"></i></a></td>
                               
                                                                </tr>
                                                         </table></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Commitment Budget </small><small class="pull-right">(จัดสรรงบประมาณ) :</small></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="BudgetNon2" placeholder="2,000,000,000 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><p class="pull-right">Disbursement (จ่ายจริง):</p></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="DisbursementNon2" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 160px"><small class="pull-right">Administrative </small><small class="pull-right">(ค่าบริหารจัดการ):</small></td>
                                                          <td class="text-success">
                                                            <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="AdministrativeNon2" placeholder="0 บาท" >
                                                               </div><!-- /.input group -->
                                                            </td>
                                                        </tr>
                                                         
                                                      </table>
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="RecipientNon2">
                    
                                                   <table class="table table-bordered">
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Person</p><p class="pull-right"> (บุคคลที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Person" style="width: 100%;">
                                                              <option>เจนจิรา  ทองมี</option>
                                                              <option>ภัทรา  ลีลาทนาพร</option>
                                                              <option>อุษา  ชิตวิลัย</option>
                                                              <option>อาณัติชัย  สงมา</option>
                                                              <option>ธวัชชัย  มูลมาตร</option>
                                                              <option>พัชรินทร์  อาบครบุรี</option>
                                                              <option>ตะวัน  เวียงธงษารักษ์</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Country</p><p class="pull-right"> (ประเทศที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Cambodia" style="width: 100%;">
                                                              <option>Cambodia </option>
                                                              <option>China </option>
                                                              <option>Madagascar </option>
                                                              <option>Malawi </option>
                                                              <option>Maldives</option>
                                                              <option>Morocco </option>
                                                              <option>Mozambique </option>
                                                              <option>Myanmar  </option>
                                                              <option>Nepal </option>
                                                              <option>Philippines  </option>
                                                              <option>Samoa  </option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Benificiary</p><p class="pull-right"> (ผู้รับผลประโยชน์) :</p></td>
                                                      <td class="">
                                                        <textarea class="form-control" rows="3" placeholder=""></textarea>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </div><!-- /.tab-pane -->
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

                            </div>

          <!--------------------------------------------- ModalAddCourseContribuiltion------------------------------------------>
              <div class="modal fade" id="AddCourseContribuiltion" role="dialog">
                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#Infocon" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#Allocatedcon" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                 <%-- <li class="bg-aqua"><a href="#Recipientcon" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>--%>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="Infocon">
                                                    <table class="table table-bordered">
                      
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Description (รายละเอียด) :</p></td>
                                                          <td class="">
                                                              <div class="col-sm-12"><textarea class="form-control" rows="2" placeholder="-"></textarea></div></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sector (สาขา) :</p></td>
                                                          <td class="">
                                                              <div class="col-sm-12">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">MULTISECTOR/CROSS-CUTTING 400</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select>
                                                                </div><!-- /.form-group --></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Sub Sector (สาขาย่อย) :</p></td>
                                                          <td class="">
                                                              <div class="col-sm-12">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">-</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select>
                                                                </div><!-- /.form-group --></td>
                                                        </tr>
                                                      
                                                      </table>
                                                  </div>

                                                  <div class="tab-pane" id="Allocatedcon">
                                                      <table class="table table-bordered">
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Plan(Start/End Date) </p><p class="pull-right">(แผนวันเริ่มต้น/สิ้นสุด) :</p></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanCon" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right">Actual (Start/End Date) </p><p class="pull-right">(วันเริ่มต้น/สิ้นสุด) :</p></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="ActualCon" placeholder="21/01/2015-21/01/2015">
                                                                 </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 180px"><p class="pull-right">Commitment (งบประมาณ) :</p></td>
                                                          <td class="">
                                                             <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="Commitment" placeholder="664,000.00 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                           </td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 180px"><p class="pull-right">Disbursement(เบิกจ่ายจริง) :</p></td>
                                                          <td class="">
                                                             <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="DisbursementCon" placeholder="664,000.00 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                           </td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 180px"><p class="pull-right">Payment Date<p class="pull-right">(วันที่จ่ายเงิน):</p></td>
                                                          <td class="">
                                                           <!-- Date dd/mm/yyyy -->
                                                              <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                            </td>
                                                        </tr>
                        
                                                         <tr>
                                                          <td style="width: 180px"><p class="pull-right">Pay As (จ่ายเงินตาม) :</p></td>
                                                          <td class="">
                                                           <!-- radio -->
                                                              <div class="form-group">
                                                                <label>
                                                                  <input type="radio" name="r1" class="minimal" checked>
                                                                    Mandary (ตามพันธกรณี)
                                                                </label>
                                                                <label>
                                                                  <input type="radio" name="r1" class="minimal">
                                                                   Voluntary (ตามความสมัครใจ)
                                                                </label>
                                                           </div></td>
                                                        </tr>
                    
                                                       
                                                      </table>
                                                  </div><!-- /.tab-pane -->

                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

    <!--------------------------------------------- ModalAddCourseLoan------------------------------------------>
              <div class="modal fade" id="AddCourseLoan" role="dialog">
                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-aqua"><a href="#InfoLoan" data-toggle="tab">ข้อมูลกิจกรรม</a></li>
                                                  <li class="bg-aqua"><a href="#AllocatedLoan" data-toggle="tab">ข้อมูลค่าใช้จ่าย</a></li>
                                                 <li class="bg-aqua"><a href="#RecipientLoan" data-toggle="tab">ข้อมูลผู้รับทุน</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="InfoLoan">
                                                    <table class="table table-bordered">
                      
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right"><small>Description (รายละเอียด) :</small></p></td>
                                                          <td>
                                                              <div class="col-sm-12"><textarea class="form-control" rows="2" placeholder="-"></textarea></div></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td style="width: 180px"><p class="pull-right"><small>Sector (สาขา) :</small></p></td>
                                                          <td class="">
                                                              <div class="col-sm-12">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">TRANSPORT AND STORAGE 210</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select>
                                                                </div><!-- /.form-group --></td>
                                                        </tr>
                        
                                                        <tr>
                                                          <td><p class="pull-right"><small>Sub Sector (สาขาย่อย) :</small></p></td>
                                                          <td class="">
                                                              <div class="col-sm-12">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                  <option selected="selected">Road transport 21020</option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                  <option></option>
                                                                </select>
                                                                </div><!-- /.form-group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td><p class="pull-right"><small>Plan(Start/End Date)</small> </p><p class="pull-right"><small>(แผนวันเริ่มต้น/สิ้นสุด) :</small></p></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="PlanLoan" placeholder="10/01/2015-15/01/2015">
                                                                 </div><!-- /.input group --></td>
                                                        </tr>
                                                        <tr>
                                                          <td><p class="pull-right"><small>Actual (Start/End Date)</small> </p><p class="pull-right"><small>(วันเริ่มต้น/สิ้นสุด) :</small></p></td>
                                                          <td class="">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                <input type="text" class="form-control pull-right" id="ActualLoan" placeholder="21/01/2015-21/01/2015">
                                                                 </div><!-- /.input group -->
                                                          </td>
                                                        </tr>
                                                      </table>
                                                  </div>
                                                  
                                                  <div class="tab-pane" id="AllocatedLoan">
                                                      <table class="table table-bordered">
                                                      <tr class="bg-info">
                                                          <td colspan="2">Gant (เงินให้เปล่า)</td>
                                                      </tr>
                                                          <tr>
                                                            <td style="width: 180px"><p class="pull-right"><small>Commitment (พันธกรณี) :</small></p></td>
                                                             <td>
                                                             <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="CommitmentLoan1" placeholder="237,218,076.28 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                          </tr>
                                                          <tr>
                                                            <td style="width: 180px"><p class="pull-right"><small>Disbursement (จ่ายจริง) :</small></p></td>
                                                             <td>
                                                              <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="DisbursementLoan1" placeholder="237,218,076.28 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                           </td>
                                                        </tr>
                                                        <tr>
                                                          <td><p class="pull-right"><small>Payment Date (วันที่จ่ายเงิน) :</small></p></td>
                                                          <td class="">
                                                           <!-- Date dd/mm/yyyy -->
                                                              <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                            </td>
                                                        </tr>
                                                        <tr class="bg-info">
                                                          <td colspan="2">Loan (เงินกู้)</td>
                                                          </tr>
                                                          <tr>
                                                            <td style="width: 180px"><p class="pull-right"><small>Commitment (พันธกรณี) :</small></p></td>
                                                             <td>
                                                             <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="CommitmentLoan2" placeholder="237,218,076.28 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                          </tr>
                                                          <tr>
                                                            <td style="width: 180px"><p class="pull-right"><small>Disbursement (จ่ายจริง) :</small></p></td>
                                                             <td>
                                                              <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-dollar "></i>
                                                                  </div>
                                                                  <input type="text" class="form-control pull-right" id="DisbursementLoan2" placeholder="237,218,076.28 บาท">
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                           </td>
                                                        
                                                        </tr>
                                                         <tr>
                                                          <td><p class="pull-right"><small>Payment Date (วันที่จ่ายเงิน) :</small></p></td>
                                                          <td class="">
                                                           <!-- Date dd/mm/yyyy -->
                                                              <div class="form-group">
                                                                <div class="input-group">
                                                                  <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                  </div>
                                                                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                                </div><!-- /.input group -->
                                                              </div><!-- /.form group -->
                                                            </td>
                                                        </tr>
                        
                                                          <tr>
                                                          <td><p class="pull-right"><small>Administrative (ค่าบริหารจัดการ):</small></p></td>
                                                          <td class="text-success">
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                 <i class="fa fa-dollar"></i>
                                                                 </div>
                                                                <input type="text" class="form-control pull-right" id="Administ" placeholder="0.00 บาท" disabled >
                                                               </div><!-- /.input group -->
                                                              </td>
                                                        </tr>
                                                       
                                                      </table>
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="RecipientLoan">
                    
                                                   <table class="table table-bordered">
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Person</p><p class="pull-right"> (บุคคลที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Person" style="width: 100%;">
                                                              <option>เจนจิรา  ทองมี</option>
                                                              <option>ภัทรา  ลีลาทนาพร</option>
                                                              <option>อุษา  ชิตวิลัย</option>
                                                              <option>อาณัติชัย  สงมา</option>
                                                              <option>ธวัชชัย  มูลมาตร</option>
                                                              <option>พัชรินทร์  อาบครบุรี</option>
                                                              <option>ตะวัน  เวียงธงษารักษ์</option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width: 180px"><p class="pull-right">Recipient Country</p><p class="pull-right"> (ประเทศที่รับทุน) :</p></td>
                                                      <td class="">
                                                          <div class="col-sm-12">
                                                             <select class="form-control select2" multiple="multiple" data-placeholder="Laos" style="width: 100%;">
                                                              <option>Cambodia </option>
                                                              <option>China </option>
                                                              <option>Madagascar </option>
                                                              <option>Malawi </option>
                                                              <option>Maldives</option>
                                                              <option>Morocco </option>
                                                              <option>Mozambique </option>
                                                              <option>Myanmar  </option>
                                                              <option>Nepal </option>
                                                              <option>Philippines  </option>
                                                              <option>Samoa  </option>
                                                            </select>
                                                         </div></td>
                                                    </tr>
                                                    <tr>
                                                      <td><p class="pull-right">Benificiary</p><p class="pull-right"> (ผู้รับผลประโยชน์) :</p></td>
                                                      <td class="">
                                                        <textarea class="form-control" rows="3" placeholder="Laos"></textarea>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </div>
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                           
                                            <div class="modal-footer">
                                                <Button Class="btn  btn-social btn-success">
                                                    <i class="fa fa-save"></i> Save
                                                </Button>
                                                <Button  Class="btn  btn-social btn-google">
                                                    <i class="fa fa-reply"></i> Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

    <!--------------------------------------------- ModalNotify------------------------------------------>
              <div class="modal fade" id="Notify" role="dialog">
                        <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           
                                                <!-- Custom Tabs -->
                                              <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                  <li class="active bg-success"><a href="#Passport" data-toggle="tab">Passport expired</a></li>
                                                  <li class="bg-success"><a href="#Insurance" data-toggle="tab">Insurance expired</a></li>
                                                 <li class="bg-success"><a href="#Visa" data-toggle="tab">Visa expired</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="Passport">
                                                    <table class="table table-striped">
                                                        <tr class="bg-gray">
                                                          <th style="width: 10px">No.</th>
                                                          <th style="width: 200px">Name</th>
                                                          <th>Country</th>
                                                          <th>Course</th>
                                                          <th>Passport expired</th>
                                                        </tr>
                                                 
                                                          <tr>
                                                          <td>1</td>
                                                          <td>Viengthone Thoummachan</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Engineering In Civil Engineering</td>
                                                          <td><span class="badge bg-red">30/04/2554</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>2</td>
                                                          <td>Ms.Aminath Ali</td>
                                                          <td>Bangladesh</td>
                                                          <td>Master of Engineering In Civil Engineering</td>
                                                          <td><span class="badge bg-red">30/04/2554</span></td>
                                                        </tr>
                                                          <tr>
                                                          <td>3</td>
                                                          <td>Mr.Owino Omondi Samuel</td>
                                                          <td>Kenya</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-red">31/05/2555</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>4</td>
                                                          <td>Mrs.Ibrahim Zarana</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-yellow">31/05/2555</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>5</td>
                                                          <td>Mr.Shrestha Narayan</td>
                                                          <td>Nepa</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-yellow">31/05/2555</span></td>
                                                        </tr>
                                                      </table>
                                                    <div class="box-footer clearfix">
                                                          <button class="pull-right btn btn-primary" id="total">ดูทั้งหมด <i class="fa fa-arrow-circle-right"></i></button>
                                                        </div>
                                                  </div>

                                                  <div class="tab-pane" id="Insurance">
                                                      <table class="table table-striped">
                                                        <tr class="bg-gray">
                                                          <th style="width: 10px">No.</th>
                                                          <th style="width: 200px">Name</th>
                                                          <th>Country</th>
                                                          <th>Course</th>
                                                          <th>Insurance expired</th>
                                                        </tr>
                                                        <tr>
                                                          <td>1</td>
                                                          <td>Mr.Kencho Tshering</td>
                                                          <td>Bhutan</td>
                                                          <td>Master of Science Program in Service Innovation</td>
                                                          <td><span class="badge bg-yellow">03/12/2553</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>2</td>
                                                          <td>Mr.Mohammad Rafiqul Alam</td>
                                                          <td>Bangladesh</td>
                                                          <td>Master of Science Program in Service Innovation</td>
                                                          <td><span class="badge bg-yellow">13/12/2553</span></td>
                                                        </tr>
                                                          <tr>
                                                          <td>3</td>
                                                          <td>Viengthone Thoummachan</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Engineering In Civil Engineering</td>
                                                          <td><span class="badge bg-yellow">30/04/2554</span></td>
                                                        </tr>
                                                  
                                                        <tr>
                                                          <td>4</td>
                                                          <td>Mrs.Ibrahim Zarana</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-yellow">31/05/2555</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>5</td>
                                                          <td>Mr.Shrestha Narayan</td>
                                                          <td>Nepa</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-yellow">31/05/2555</span></td>
                                                        </tr>
                                                      </table>
                                                      <div class="box-footer clearfix">
                                                          <button class="pull-right btn btn-primary" id="more">ดูทั้งหมด <i class="fa fa-arrow-circle-right"></i></button>
                                                        </div>
                                                  </div><!-- /.tab-pane -->

                                                  <div class="tab-pane" id="Visa">
                    
                                                      <table class="table table-striped">
                                                        <tr class="bg-gray">
                                                          <th style="width: 10px">No.</th>
                                                          <th style="width: 200px">Name</th>
                                                          <th>Country</th>
                                                          <th>Course</th>
                                                          <th>Visa expired</th>
                                                        </tr>
                                                        <tr>
                                                          <td>1</td>
                                                          <td>Mr.Kencho Tshering</td>
                                                          <td>Bhutan</td>
                                                          <td>Master of Science Program in Service Innovation</td>
                                                          <td><span class="badge bg-yellow">03/12/2553</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>2</td>
                                                          <td>Mr.Mohammad Rafiqul Alam</td>
                                                          <td>Bangladesh</td>
                                                          <td>Master of Science Program in Service Innovation</td>
                                                          <td><span class="badge bg-yellow">13/12/2553</span></td>
                                                        </tr>
                                                          <tr>
                                                          <td>3</td>
                                                          <td>Viengthone Thoummachan</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Engineering In Civil Engineering</td>
                                                          <td><span class="badge bg-yellow">30/04/2554</span></td>
                                                        </tr>
                                                        <tr>
                                                          <td>4</td>
                                                          <td>Ms.Aminath Ali</td>
                                                          <td>Bangladesh</td>
                                                          <td>Master of Engineering In Civil Engineering</td>
                                                          <td><span class="badge bg-yellow">30/04/2554</span></td>
                                                        </tr>
                                                          <tr>
                                                          <td>5</td>
                                                          <td>Mr.Owino Omondi Samuel</td>
                                                          <td>Kenya</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td><span class="badge bg-defult">31/05/2555</span></td>
                                                        </tr>
                                                    <%--<tr>
                                                          <td>6</td>
                                                          <td>Mrs.Ibrahim Zarana</td>
                                                          <td>Maldives</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td>31/05/2555</td>
                                                        </tr>
                                                        <tr>
                                                          <td>7</td>
                                                          <td>Mr.Shrestha Narayan</td>
                                                          <td>Nepa</td>
                                                          <td>Master of Primary Health Care Management</td>
                                                          <td>31/05/2555</td>
                                                        </tr>--%>
                                                      </table>
                                                    <div class="box-footer clearfix">
                                                          <button class="pull-right btn btn-primary" id="morevisa">ดูทั้งหมด <i class="fa fa-arrow-circle-right"></i></button>
                                                        </div>
                                                  </div>
                                                </div><!-- /.tab-content -->
                                              </div><!-- nav-tabs-custom -->
                                         
                                        </div>
                                    </div>
                                  </div>
                                </div>

</div>

