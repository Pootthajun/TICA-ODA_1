﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmSubExpense.aspx.vb" Inherits="ODA_VBPrototype.frmSubExpense" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Sub Expenses | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="active treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
              <li class="active treeview">
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li class="active"><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Sub Expenses (รายจ่ายย่อย)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Sub Expenses</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                     <p><a href="frmEditSubExpenses.aspx"><button class="btn bg-orange margin-r-5 btn-social"><i class="fa fa-plus"></i>Add Sub Expenses</button></a></p>
                   <div class="col-lg-7">
                     <h4 class="text-primary">พบทั้งหมด 9 รายการ</h4>
                  </div>
                        
                        <div class="input-group col-sm-5 pull-right">
                       <input type="text" class="form-control">
                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                     </div>
                     <div class="clearfix"></div>
                    </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray">
                        <th>Expenses (รายจ่าย)</th>
                        <th>Sub Expenses (รายจ่ายย่อย)</th>
                        <th>Form (แบบฟอร์ม)</th>
                        <th style="width: 70px">Edit(แก้ไข)</th>
                        <th style="width: 50px">Delete(ลบ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่า ธ.ตรวจลงตรา/ขยายVISA</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่ากินอยู่</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าขนย้าย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าขนส่งสัมภาระ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าก่อสร้าง & วัสดุอุปกรณ์</td>
                        <td>ค่าของขวัญ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าจัดรายการเหมาจ่าย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าเช่า LCD</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าเช่ารถยนต์</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าเช่ารถและน้ำมันเชื้อเพลิง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าเช่าห้องประชุม</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าใช้จ่ายในการจัดทำเอกสารรายงาน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าใช้จ่ายอื่นๆ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าโดยสารเครื่องบิน (ในประเทศ)</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าโดยสารเครื่องบิน (ระหว่างประเทศ)</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าโดยสารเครื่องบิน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าตกแต่งสถานที่</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าตอบแทน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าเตรียมการก่อนเดินทาง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าทำหนังสือเดินทาง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (ที่พัก)</td>
                        <td>ค่าที่พัก</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าธรรมเนียมการศึกษา</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าธรรมเนียมการโอนเงิน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าน้ำมัน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าบรรยาย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าบริหารจัดการ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>ค่าเบี้ยประกันชีวิตและสุขภาพ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (ที่พัก)</td>
                        <td>ค่าเบี้ยเลี้ยง ที่พัก พาหนะ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าเบี้ยเลี้ยง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าปฏิบัติงานนอกเวลา</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าพาหนะคนไทย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าพาหนะต่างชาติ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าพาหนะใน ตปท.</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าพาหนะในประเทศ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าใช้จ่ายเดินทาง</td>
                        <td>ค่าพาหนะรับ-ส่ง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าที่พัก</td>
                        <td>ค่าพาหนะเหมาจ่าย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าภาษีสนามบิน</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>ค่ารักษาพยาบาล</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าจัดรายการ</td>
                        <td>ค่าเลี้ยงรับรอง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าก่อสร้าง & วัสดุอุปกรณ์</td>
                        <td>ค่าวัดดุอุปกรณ์</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าก่อสร้าง & วัสดุอุปกรณ์</td>
                        <td>ค่าวิทยานิพนธ์</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>ค่าสมนาคุณพิเศษ</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าก่อสร้าง & วัสดุอุปกรณ์</td>
                        <td>ค่าหนังสือ/อุปกรณ์</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่าก่อสร้าง & วัสดุอุปกรณ์</td>
                        <td>ค่าหนังสือเหมาจ่าย</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>ค่าอาหารว่างและเครื่องดื่ม</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>ค่าอาหาร</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      <tr>
                        <td>ค่ากินอยู่ (เบี้ยเลี้ยง)</td>
                        <td>เงินได้เมื่อแรกถึง</td>
                        <td>ฟอร์ม 1,2</td>
                        <td class="text-center"><i class="fa fa-edit text-success"></i> Edit</td>
                        <td class="text-center"><i class="fa fa-trash text-danger"></i> Delete</td>
                      </tr>
                      
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
  <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
    
</asp:Content>
     
