﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master"CodeBehind="frmBudgetReport.aspx.vb" Inherits="ODA_VBPrototype.frmBudgetReport" %>


<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Budget | Budget</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="#">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <li><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="active treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li class="active"><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li class="active"><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Report (รายงาน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Project</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header with-border">
                     <div class="box-body">
                      <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">ปีงบประมาณ :</label>
                                      <div class="col-sm-2">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>2560</option>
                                          <option>2559</option>
                                          <option>2558</option>
                                          <option>2557</option>
                                          <option>2556</option>
                                          <option>2555</option>
                                          <option>2554</option>
                                          <option>2553</option>
                                          <option>2552</option>
                                          <option>2551</option>
                                          <option>2550</option>
                                          <option>2549</option>
                                        </select>
                                      </div>
                                       <label for="inputname" class="col-sm-2 control-label">ประเภทงบประมาณ :</label>
                                      <div class="col-sm-2">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">ทั้งหมด</option>
                                          <option>งบเงินอุดหนุน</option>
                                          <option>งบรายจ่ายอื่น</option>
                                        </select>
                                      </div>
                                      <div class="col-lg-3">
                                        <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                          <button class="btn btn-info btn-flat" type="button" data-toggle="tooltip" title="ค้นหา"><i class="fa fa-search"></i></button>
                                        </span>
                                      </div><!-- /input-group -->
                                   </div>
                                   <div class="col-lg-1">
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                         </ul>
                                        </div>
                                      </div>
                                    </div>

                                  </div><!-- /.box-body -->
                                 
                                </form>                            
                              </div><!-- /.box-body -->
                            </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="bg-gray text-center">
                        <th class="text-center">ลำดับ</th>
                        <th class="text-center">ปีงบประมาณ</th>
                        <th class="text-center">ประเภทงบประมาณ</th>
                        <th style="width: 350px" class="text-center">รายละเอียด</th>
                        <th class="text-center">งบประมาณที่ได้รับ</th>
                        <th class="text-center">ค่าใช้จ่ายเบิกจ่ายแล้ว</th>
                        <th class="text-center">ค่าใช้จ่ายในครั้งนี้</th>
                        <th class="text-center">งบประมาณคงเหลือ</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนองค์กรการระหว่างประเทศที่ประเทศไทยเข้าเป็นสมาชิก</a></td>
                        <td>44,568,000.00</td>
                        <td>25,800,000.00</td>
                        <td>0.00</td>
                        <td>18,768,000.00</td>         
                      </tr>
                      <tr>
                        <td class="text-center">2</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนกรให้ความช่วยเหลือและความร่วมมือทางด้านวิชาการฯ (ภูมิภาคอื่นๆ)</a></td>
                        <td>120,000,000.00</td>
                        <td>1,286,800.00</td>
                        <td>0.00</td>
                        <td>118,713,200.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนค่าใช้จ่ายในการเดินทางสำหรับผู้รับทุนรัฐบาลต่างประเทศ</a></td>
                        <td>1,000,000.00</td>
                        <td>46,620.00</td>
                        <td>0.00</td>
                        <td>953,380.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนการให้ความช่วยเหลือและความร่วมมือทางด้านวิชาการและเศรษฐกิจแก่ต่างประเทศ (ประเทศเพื่อนบ้าน)</a></td>
                        <td>360,000,000.00</td>
                        <td>1,927,000.00</td>
                        <td>966,015.00</td>
                        <td>957,106,985.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนแผนงานพัฒนาชุมชนในเขตเศรษฐกิจพิเศษทวาย</a></td>
                        <td>15,000,000.00</td>
                        <td>0.00</td>
                        <td></td>
                        <td>15,000,000.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนในการสร้างความตระหนักและเตรียมความพร้อมสำหรับโรคติดต่อและโรคอุบัติใหม่ตามแนวชายแดนไทย-ประเทศเพื่อนบ้าน</a></td>
                        <td>15,000,000.00</td>
                        <td>0.00</td>
                        <td></td>
                        <td>15,000,000.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td>2558</td>
                        <td><a href="#">งบเงินอุดหนุน</a></td>
                        <td><a href="#">เงินอุดหนุนการพัฒนาฝีมือแรงงานตามแนวชายแดนไทย-เพื่อนบ้าน</a></td>
                        <td>11,000,000.00</td>
                        <td>0.00</td>
                        <td></td>
                        <td>11,000,000.00</td>
                      </tr>
                      <tr>
                        <td class="text-right" colspan="4"><b>Total</b></td>
                        <td><b>566,568,00.00</b></td>
                        <td><b>29,060,420.00</b></td>
                        <td><b>966.015.00</b></td>
                        <td><b>536,541,565.00</b></td>
                      </tr>
                   
                      <tr>
                        <td class="text-center">1</td>
                        <td>2558</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการเจรจาและการประชุมนานาชาติ</a></td>
                        <td>7,360,00.00</td>
                        <td>1,407,525.00</td>
                        <td>0.00</td>
                        <td>5,952,475.00</td>         
                      </tr>
                      <tr>
                        <td class="text-center">2</td>
                        <td>2558</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการเดินทางไปราชการต่างประเทศชั่วคราว</a></td>
                        <td>8,365,000.00</td>
                        <td>1,758,800.00</td>
                        <td>0.00</td>
                        <td>6,606,200.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการจัดการประชุมระหว่างประเทศ</a></td>
                        <td>470,000.00</td>
                        <td>39,360.00</td>
                        <td>0.00</td>
                        <td>430,640.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">4.1</td>
                        <td>2558</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                        - ภาษีชดเชย ผชช. และมูลนิธิ
                            </td>
                        <td>19,000,000.00</td>
                        <td>0.00</td>
                        <td>0.00</td>
                        <td>19,000,000.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">4.2</td>
                        <td>2558</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                        - จัดทำแผนให้ฯ
                            </td>
                        <td>1,000,000.00</td>
                        <td>0.00</td>
                        <td>0.00</td>
                        <td>1,000,000.00</td>
                      </tr>
                      <tr>
                        <td class="text-center">4.3</td>
                        <td>2558</td>
                        <td><a href="#">งบรายจ่ายอื่น</a></td>
                        <td><a href="#">ค่าใช้จ่ายในการสนับสนุนความร่วมมือทางวิชาการกับต่างประเทศ</a><br />
                                        - สมทบ ผชช./อาสาสมัคร
                            </td>
                        <td>16,000,000.00</td>
                        <td>1,555,2120.91</td>
                        <td>0.00</td>
                        <td>14,444,787.09</td>
                      </tr>
                      <tr>
                        <td class="text-right" colspan="4"><b>Total</b></td>
                        <td><b>52,195,00.00</b></td>
                        <td><b>4,760,897.91</b></td>
                        <td><b>0.00</b></td>
                        <td><b>47,434,102.09</b></td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
   <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
