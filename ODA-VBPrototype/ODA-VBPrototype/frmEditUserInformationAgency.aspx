﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmEditUserInformationAgency.aspx.vb" Inherits="ODA_VBPrototype.frmEditUserInformationAgency" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Agency Information | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-users"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Agency Information (รายละเอียดข้อมูลหน่วยงาน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li><a href="#"> Agency Information</a></li>
            <li class="active">Edit Agency Information</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"><br />
          <!-- START CUSTOM TABS -->
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box">
                <div class="box-header with-border bg-blue-gradient">
                  <h4 class="box-title">Edit Agency Information</h4>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputnameth" class="col-sm-3 control-label">ชื่อหน่วยงาน :</label>
                      <div class="col-sm-8">
                        <input class="form-control" id="inputnameth" placeholder="สำนักงานความร่วมมือเพื่อการพัฒนาระหว่างประเทศ">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="form-group">
                      <label for="inputnameen" class="col-sm-3 control-label">Agency Name :</label>
                      <div class="col-sm-8">
                        <input class="form-control" id="inputnameen" placeholder="Thailand International Development Cooperation Agency">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputDirector" class="col-sm-3 control-label">Division / Part (ส่วน/กอง) :</label>
                      <div class="col-sm-8">
                        <input class="form-control" id="inputDirector" placeholder="สำนักผู้อำนวยการ">
                      </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="box-header with-border">
                         <h4 class="box-title text-primary">Location (ที่ตั้ง)</h4>
                    </div><!-- /.box-header --> <br />
                    <div class="form-group">
                      <label for="building" class="col-sm-2 control-label">No./ building<br />(เลขที่/อาคาร) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="building" placeholder="อาคาร B">
                      </div>
                      <label for="road" class="col-sm-2 control-label">Road  (ถนน) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="road" placeholder="แจ้งวัฒนะ">
                      </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="district" class="col-sm-2 control-label">Parish (ตำบล/แขวง) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="district" placeholder="ทุ่งสองห้อง" />
                    </div>
                    <label for="prefecture" class="col-sm-2 control-label">District (อำเภอ) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="prefecture" placeholder="หลักสี่" />
                    </div>
                  </div><!-- /.box-body -->

                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="county" class="col-sm-2 control-label">Province (จังหวัด) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="county" placeholder="กรุงเทพฯ" />
                    </div>
                    <label for="Zipcode" class="col-sm-2 control-label">Postal Code (ไปรษณีย์) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="Zipcode" placeholder="11140" />
                    </div>
                  </div><!-- /.box-body -->
                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="phone" class="col-sm-2 control-label">Telephone (โทรศัพท์) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="phone" placeholder="0-203-5000" />
                    </div>
                    <label for="fax" class="col-sm-2 control-label">Fax (โทรสาร) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="fax" placeholder="0-2143-9327" />
                    </div>
                  </div><!-- /.box-body -->

                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="website" class="col-sm-2 control-label">Website (เว็บไซต์) :</label>
                      <div class="col-sm-4">
                        <input class="form-control" id="website" placeholder="http://www.tica.thaigov.net/" />
                    </div>
                    <label for="fax" class="col-sm-2 control-label">Note (หมายเหตุ) :</label>
                      <div class="col-sm-4">
                        <textarea class="form-control" rows="3" placeholder=""></textarea>
                    </div>
                  </div><!-- /.box-body -->
                  
                  <div class="clearfix"></div>
                    <div class="form-group">
                      <label for="inputname" class="col-sm-2 control-label">Active Status :</label>
                      <div class="col-sm-9">
                     <label><input type="checkbox" class="minimal" checked></label>
                      </div>
                    </div>
                 </div>
                  <div class="box-footer">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-success"><i class="fa fa-save"></i>Save </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="frmProjectExam.aspx" class="btn btn-block btn-social btn-google"><i class="fa fa-reply"></i>Cancel </a>
                    </div>  
                    </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div> <!-- /.row -->
          <!-- END CUSTOM TABS -->
          

        </section><!-- /.content -->
    </div>
 


<!----------------Page Advance---------------------->
        
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />  

</asp:Content>
