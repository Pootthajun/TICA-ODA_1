﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmStructure.aspx.vb" Inherits="ODA_VBPrototype.frmStructure" %>

<%@ Register Src="~/frmModal.ascx" TagPrefix="uc1" TagName="frmModal" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<title>Organization Structure | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="active treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file-o"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file-o"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-align-justify"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-align-justify"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-align-justify"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           Organization Structure (โครงสร้างองค์กร)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashborad</a></li>
            <li class="active">Organization Structure</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body">
                  <table class="table table-bordered">
                    <tr class="bg-gray">
                      <th>โครงสร้างองค์กร</th>
                    </tr>
                   
                    <tr>
                      <td>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          ไทย (Thailand)
                        </a> 
                         
                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                   <ul class="dropdown-menu">
                                      <li><a data-toggle="modal" data-target="#myModalCountryDetail"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                      <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                      <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                      <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                      <li class="divider"></li>
                                      <li><a data-toggle="modal" data-target="#myModalCountry"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                      <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                    </ul>
                                  </div>
                         <div id="collap1" class="panel-collapse collapse">
                            <table class="table">
                            <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap12">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>สำนักนายกรัฐมนตรี</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 

                                  <div id="collap12" class="panel-collapse collapse">

                                   <table class="table">
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสมร
                                     
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                      <ul class="dropdown-menu">
                                        <li><a data-toggle="modal" data-target="#myModalPerson" ><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                        <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                        <li class="divider"></li>
                                        <li><a  data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                        <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                      </ul>
                                    </div></td>
                                    </tr>
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสาวกันยา
                                     
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a  data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                    </div></td>
                                    </tr>
                                    <tr>
                                       <td class="text-center" style="width: 50px"></td>
                                       <td>
                                           <a data-toggle="collapse" data-parent="#accordion" href="#collap13">
                                           <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานที่ 1</a>
                                    
                                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>
                                     
                                        <div id="collap13" class="panel-collapse collapse">
                                           <table class="table">
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td>
                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collap14">
                                                  <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานย่อย</a>
                                            
                                                 <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                                <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                  <ul class="dropdown-menu">
                                                    <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                    <li class="divider"></li>
                                                    <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                                  </ul>
                                                </div>

                                                  <div id="collap14" class="panel-collapse collapse">
                                                   <table class="table">
                                                    <tr>
                                                      <td class="text-center" style="width: 50px"></td>
                                                      <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                     
                                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                                    <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                      <ul class="dropdown-menu">
                                                        <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                        <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                                        <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                        <li class="divider"></li>
                                                        <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                        <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                                      </ul>
                                                    </div></td>
                                                    </tr>
                                                    </table>
                                                 </div>
                                                  </td>
                                                </tr>
                                           </table>
                                         </div>
                                      </tr>
                                      <tr>
                                    </table></div>
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงเกษตรและสหกรณ์</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงศึกษาธิการ</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                             <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>สำนักงานคณะกรรมการอุดมศึกษา (สกอ.)</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>สำนักงานความร่วมมือพัฒนาเศรษฐกิจกับประเทศเพื่อนบ้านฯ</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงอุตสาหกรรม</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                             <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงวิทยาศาตร์</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงเทคโนโลยีสารสนเทศฯ</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงพานิชย์</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงวัฒนธรรม</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div> 
                                   </td>
                                </tr>
                                
                                <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap11">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>กระทรวงการท่องเที่ยวและกีฬา</a>
                               
                                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                        <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a data-toggle="modal" data-target="#myModalPerson"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a data-toggle="modal" data-target="#myModalAgency"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>
                                   </td>
                                </tr>
                               
                             </table>
                         </div>
                      </td>
                      
                    </tr>
                   <%-- <tr>
                      <td>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap2">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          ลาว (Laos)
                        </a> 
                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                      <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                        <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                        <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                        <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                        <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                      </ul>
                    </div>

                         <div id="collap2" class="panel-collapse collapse">

                           <table class="table">
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสมร
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสาวกันยา
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap21">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานที่ 1</a>
                                    
                                 <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                  <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                    <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                  </ul>
                                </div>
                                     
                                <div id="collap21" class="panel-collapse collapse">
                                   <table class="table">
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td>
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collap22">
                                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานย่อย</a>
                                            
                                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>

                                          <div id="collap22" class="panel-collapse collapse">
                                           <table class="table">
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                     
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            </table>
                                         </div>
                                          </td>
                                        </tr>
                                   </table>
                                 </div>
                              </tr>
                              <tr>
                            </table>
                         </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap3">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          มาเลเซีย (Malaysia)
                        </a> 
                         <div class="btn-group">
                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                          <ul class="dropdown-menu" data-toggle="tooltip" title="เครื่องมือ">
                            <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                            <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                            <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                          </ul>
                        </div>

                         <div id="collap3" class="panel-collapse collapse">

                           <table class="table">
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายกรัฐมนตรี
                                   
                                 <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                  <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                  </ul>
                                </div></td>
                            </tr>
                            </table>
                         </div>
                      </td>
                    </tr>
                    
                    <tr>
                      <td colspan="3">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap4">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          เวียดนาม (Vietnam)
                        </a>
                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                          <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                    <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                          </ul>
                        </div>

                         <div id="collap4" class="panel-collapse collapse">

                           <table class="table">
                            <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap41">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานที่ 1</a> 
                                 <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                  <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                    <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                  </ul>
                                </div>
                                     
                                <div id="collap41" class="panel-collapse collapse">
                                   <table class="table">
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td>
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collap42">
                                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานย่อย</a>
                                               
                                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>

                                          <div id="collap42" class="panel-collapse collapse">
                                           <table class="table">
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                 
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                    
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                    
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                                 
                                            </tr>
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                    
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            </table>
                                         </div>
                                          </td>
                                        </tr>
                                   </table>
                                 </div>
                              </tr>
                             </table>
                          </div>
                         </td>
                            
                        </tr>
                       <tr>
                      <td>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap5">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          กัมพูชา (Cambodia)
                        </a> 
                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                      <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                        <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                        <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                        <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                        <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                      </ul>
                    </div>

                         <div id="collap5" class="panel-collapse collapse">

                           <table class="table">
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสมร
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสาวกันยา
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap51">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานที่ 1</a>
                                    
                                 <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                  <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                    <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                  </ul>
                                </div>
                                     
                                <div id="collap51" class="panel-collapse collapse">
                                   <table class="table">
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td>
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collap52">
                                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานย่อย</a>
                                            
                                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>

                                          <div id="collap52" class="panel-collapse collapse">
                                           <table class="table">
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                     
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            </table>
                                         </div>
                                          </td>
                                        </tr>
                                   </table>
                                 </div>
                              </tr>
                              <tr>
                            </table>
                         </div>
                      </td>
                    </tr>
                      <tr>
                          <td>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collap6">
                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-globe"></i></div>
                          กัมพูชา (Cambodia)
                        </a> 
                     <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                      <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                        <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                        <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                        <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                        <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                      </ul>
                    </div>

                         <div id="collap6" class="panel-collapse collapse">

                           <table class="table">
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสมร
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                              <td class="text-center" style="width: 50px"></td>
                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นางสาวกันยา
                                     
                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                              </ul>
                            </div></td>
                            </tr>
                            <tr>
                               <td class="text-center" style="width: 50px"></td>
                               <td>
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collap61">
                                   <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานที่ 1</a>
                                    
                                 <div class="btn-group" data-toggle="tooltip" title="Tool">
                                  <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                    <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                    <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                    <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                    <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                  </ul>
                                </div>
                                     
                                <div id="collap61" class="panel-collapse collapse">
                                   <table class="table">
                                    <tr>
                                      <td class="text-center" style="width: 50px"></td>
                                      <td>
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collap62">
                                          <div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-building-o"></i></div>หน่วยงานย่อย</a>
                                            
                                         <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                          <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                            <li><a href="frmEditStructure.aspx"><i class="fa fa-building text-blue"></i>  เพิ่มหน่วยงาน</a></li>
                                            <li><a href="frmEditUser.aspx"><i class="fa fa-user text-blue"></i>  เพิ่มบุคคล</a></li>
                                            <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                            <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                          </ul>
                                        </div>

                                          <div id="collap62" class="panel-collapse collapse">
                                           <table class="table">
                                            <tr>
                                              <td class="text-center" style="width: 50px"></td>
                                              <td><div class="btn btn-social-icon btn-defult text-blue"><i class="fa fa-user"></i></div>นายสมศักดิ์  บุญล้อม
                                                     
                                             <div class="btn-group" data-toggle="tooltip" title="เครื่องมือ">
                                              <a class="btn btn-social-icon btn-defult text-green" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                              <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-search text-green"></i>  ดูรายละเอียด</a></li>
                                                <li><a href="#"><i class="fa fa-gears text-blue"></i>  กำหนดสิทธิ์</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-edit text-blue"></i>  แก้ไข</a></li>
                                                <li><a href="#"><i class="fa fa-trash text-red"></i>  ลบ</a></li>
                                              </ul>
                                            </div></td>
                                            </tr>
                                            </table>
                                         </div>
                                          </td>
                                        </tr>
                                   </table>
                                 </div>
                              </tr>
                              <tr>
                            </table>
                         </div>
                      </td>
                      </tr>--%>
                     </table>
                    </div><!-- /.box-body -->
                 </div><!-- /.col -->
              </div>
	
              </div><!-- /.row -->
        </section></div>
    <uc1:frmModal runat="server" ID="frmModal" />
</asp:Content>

