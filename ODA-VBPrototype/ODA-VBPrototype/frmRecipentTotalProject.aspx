﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmMaster.Master" CodeBehind="frmRecipentTotalProject.aspx.vb" Inherits="ODA_VBPrototype.frmRecipentTotalProject" %>

<%@ Register Src="~/frmScriptAdvance.ascx" TagPrefix="uc1" TagName="frmScriptAdvance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Recipent Total | ODA</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

<ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <%--<li>
              <a href="frmDashborad.aspx">
                <i class="fa fa-area-chart"></i> <span>Dashboard</span>
              </a>
            </li>--%>
            <li>
              <a href="frmProjectStatus.aspx">
                <i class="fa fa-area-chart"></i>
                <span>Project Status</span>
                <small class="label pull-right bg-yellow">4</small>
              </a>
            </li>
            
           <li class="treeview">
              <a href="frnFinance.aspx">
                <i class="fa fa-calculator"></i> <span>Finance</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <li ><a href="frmFinanceProject.aspx"><i class="fa fa-file-text-o"></i> Project</a></li>
                <li><a href="frmExpenseReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmExpenses.aspx"><i class="fa fa-list"></i> Expense Group</a></li>
                    <li><a href="frmSubExpense.aspx"><i class="fa fa-list"></i> Sub Expense</a></li>
                  </ul>
                </li>
              </ul>
            </li> 
            <li class="treeview">
              <a href="frmAllocatedBudget.aspx">
                <i class="fa fa-dollar"></i> <span>Allocated Budget</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="frmBudget.aspx"><i class="fa fa-file-text-o"></i> Budget</a></li>
                <li><a href="frmBudgetReport.aspx"><i class="fa fa-file-text-o"></i> Report</a></li>
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Master<i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="frmGroupBudget.aspx"><i class="fa fa-list"></i>Group Budget </a></li>
                    <li><a href="frmSubBudget.aspx"><i class="fa fa-list"></i> Sub Budget</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmReport.aspx">
                <i class="fa fa-file-text-o"></i> <span>Report</span></a>
            </li> 

            <li class="treeview">
              <a href="frmStructure.aspx">
                <i class="fa fa-sitemap"></i> <span>Organize Structure</span>
              </a>
             
            </li>
             <li>
              <a href="#">
                <i class="fa fa-gear"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="frmCountryGroup.aspx"><i class="fa fa-globe"></i> Country Group</a></li>
                <li><a href="frmOECD.aspx"><i class="fa fa-globe"></i> OECD</a></li>
                <li><a href="frmRegionOECD.aspx"><i class="fa fa-globe"></i> RegionOECD</a></li>
                <li><a href="frmCooperationFramework.aspx"><i class="fa fa-globe"></i> Cooperation Framework</a></li>
                <li><a href="frmCooperationType.aspx"><i class="fa fa-globe"></i> Cooperation Type</a></li>
                <li><a href="frmSector.aspx"><i class="fa fa-file"></i> Sector</a></li>
                <li><a href="frmSubSector.aspx"><i class="fa fa-file"></i> Sub Sector</a></li>
                <li><a href="frmComponent.aspx"><i class="fa fa-list"></i> Component</a></li>
                <li><a href="frmMultilateral.aspx"><i class="fa fa-list"></i> Multilateral</a></li>
                <li><a href="frmInKind.aspx"><i class="fa fa-list"></i> In Kind</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="frmContect.aspx">
                <i class="fa fa-list-alt"></i>
                <span>Contect</span></a>
            </li>
          </ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
 
     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           List Recipient (รายชื่อผู้รับทุน)
          </h4>
          <ol class="breadcrumb">
            <li><a href="frmDashborad.aspx"><i class="fa fa-area-chart"></i> Dashboard</a></li>
            <li class="active">Sector</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                 <div class="box-header with-border">
                                
                             <div class="col-lg-5"><br />
                                <h5 class="text-primary"><b>พบทั้งหมด 2 รายการ</b></h5>
                             </div>

                            <div class="col-lg-5">
                                <div class="input-group margin">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-info btn-flat" type="button"><i class="fa fa-search" data-toggle="tooltip" title="ค้นหา"></i></button>
                                </span>
                              </div><!-- /input-group -->
                           </div>
                           <div class="col-lg-2">
                                <div class="input-group margin">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collap1">
                                    <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="ค้นหาเพิ่มเติม">
                                    <i class="fa fa-magic"></i>
                                    </button></a>
                                    <div class="btn-group" data-toggle="tooltip" title="พิมพ์รายงาน">
                                        <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-print"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="#"><i class="fa fa-file-pdf-o text-red"></i>PDF</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-file-excel-o text-green"></i>EXCEL</a></li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            
                        <!-- /.box-header -->
                        <div class="clearfix"></div>
                       <div id="collap1" class="panel-collapse collapse">
                        
                            <div class="box-body">
                                <!-- form start -->
                                <form class="form-horizontal">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Student ID :</label>
                                      <div class="col-sm-4">
                                        <input class="form-control" id="Project" placeholder="">
                                      </div>

                                       <label for="Sector" class="col-sm-2 control-label">Country :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                   </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="Sector" class="col-sm-2 control-label">Course (หลักสูตร) :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>
                                      
                                      <label for="Sector" class="col-sm-2 control-label">Start/End Date :</label>
                                      <div class="col-sm-4">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                           </div>
                                         <input type="text" class="form-control pull-right" id="reservation">
                                        </div><!-- /.input group -->
                                      </div>
                                    </div>
                                  
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                      <label for="inputname" class="col-sm-2 control-label">Sector :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>

                                      <label for="inputname" class="col-sm-2 control-label">Status :</label>
                                      <div class="col-sm-4">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select</option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                          <option></option>
                                        </select>
                                      </div>


                                  </div><!-- /.box-body -->
                                  </div>
                                </form>
                               
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                           
                        </div>
                      </div>
                <div class="box-body">
                  <table class="table">
                      <tr class="bg-gray text-center">
                      <th style="width: 10px">No.</th>
                      <th>Student ID</th>
                      <th style="width: 200px">Name</th>
                      <th>Country</th>
                      <th>Course</th>
                      <th>Start-End Date</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">56199130400</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Viengthone Thoummachan</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">56199130401</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Khamphet Sengouly</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Laos</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">Master of Arts</a></td>
                      <td><a href="frmDetailRecipient2.aspx" target="_blank">24/03/2013-31/05/2015</a></td>
                      <td><a href="frmDetailRecipient1.aspx" target="_blank"><span class="badge bg-green">กำลังศึกษา</span></a></td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
   </div>
    <uc1:frmScriptAdvance runat="server" ID="frmScriptAdvance" />
</asp:Content>
