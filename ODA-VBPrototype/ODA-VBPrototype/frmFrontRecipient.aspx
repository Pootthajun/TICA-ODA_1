﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/frmFrontMaster.Master" CodeBehind="frmFrontRecipient.aspx.vb" Inherits="ODA_VBPrototype.frmFrontRecipient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>Metronic Frotnend | Features - Forms</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <!-- BEGIN TOP NAVIGATION MENU -->
	                <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="frmFrontIndex.aspx">หน้าหลัก</a></li>
                            <li>
                                <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    เกี่ยวกับกรมฯ
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontTICAAbout.aspx">ประวัติกรมความร่วมมือระหว่างประเทศ</a></li>
                                    <li><a href="frmFrontPolicyPlan.aspx">วิสัยทัศน์ พันธกิจ สัญลักษณ์</a></li>
                                    <li><a href="frmFrontOrganization">โครงสร้างองค์กร</a></li>
                                    <li><a href="frmFrontPerformance.aspx">ผลการดำเนินงานของกรม</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="frmAbout.aspx">
                                    ศูนย์ข่าว
                                    <i class="icon-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="frmFrontNews.aspx">ข่าวทั่วไป</a></li>
                                    <li><a href="frmFrontNewsODA.aspx">ข่าวกรมฯ</a></li>
                            </ul></li>
                            <li class="active"><a href="frmFrontRecipient.aspx">ฟอร์มกรอกเอกสาร</a></li>  
                            <li><a href="frmFrontContact.aspx">ติดต่อกรมฯ</a></li>                        
                          
                          
                         </ul>    
                    </div>
	                <!-- BEGIN TOP NAVIGATION MENU -->
                </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
                     <!-- BEGIN BREADCRUMBS -->   
                    <div class="row-fluid breadcrumbs margin-bottom-40">
                        <div class="container">
                            <div class="span4">
                                <h1>Forms</h1>
                            </div>
                            <div class="span8">
                                <ul class="pull-right breadcrumb">
                                    <li><a href="index.html">Home</a> <span class="divider">/</span></li>
                                    <li><a href="#">Features</a> <span class="divider">/</span></li>
                                    <li class="active">Forms</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END BREADCRUMBS -->

        <!-- BEGIN CONTAINER -->   
        <div class="container min-hight">

		    <!-- ROW 1 -->
		    <div class="row-fluid margin-bottom-40">
			    <!-- COLUMN 1 -->
               <div class="span3">
                <div class="row-fluid recent-work margin-bottom-40">
                        <ul class="bxslider">
                        <li>
                          <em>
                            <img src="dist/img/user1-128x128.jpg" alt="" />
                            <a href="#"><i class="icon-edit icon-hover icon-hover-1" data-toggle="tooltip" title="แก้ไข"></i></a>
                            <a href="dist/img/user1-128x128.jpg" class="fancybox-button" title="Soulivanh KHANMPHANOUVONG" data-rel="fancybox-button"><i class="icon-search icon-hover icon-hover-2"></i></a>
                          </em>
                        </li>
                        </ul>
                    </div>
                   <!-- BLOCK BUTTONS -->
				    <div class="row-fluid">
					    <p>
						    <a  href="#Per" data-toggle="tab" class="btn blue btn-block">A. PERSONAL HISTORY</a>
						    <a  href="#Edu" data-toggle="tab" class="btn blue btn-block">B. EDUCATION RECORD</a>
						    <a  href="#Emp" data-toggle="tab" class="btn blue btn-block">C. EMPLOYMENT RECORD</a>
						    <a  href="#Exp" data-toggle="tab" class="btn blue btn-block">D. EXPECTATIONS</a>
						    <a  href="#Gov" data-toggle="tab" class="btn blue btn-block">E. GOVERNMENT AUTHORISATION</a>
					    </p>
				    </div>
				   <!-- END BLOCK BUTTONS -->
                   </div>
			           <div class="span9"> 
                        <div class="tab-content">
                         <div class="tab-pane row-fluid fade in active" id="Per"> 
				            <%--<h4 class="text-info">Informations (ข้อมูลทั่วไป)</h4>--%>
						    <!-- BEGIN FORM-->

                          <div class="form-group">
                          <label for="district" class="col-sm-3 control-label"><b>Course  (หลักสูตร)</b>   : 
                              <select class="large m-wrap" tabindex="1">
									    <option value="Category 1">Course (หลักสูตร)1</option>
									    <option value="Category 2">Course (หลักสูตร) 2</option>
									    <option value="Category 3">Course (หลักสูตร) 5</option>
									    <option value="Category 4">Course (หลักสูตร) 4</option>
						            </select></label>
                        
                         </div>
                          <h5 class="text-info"><b>A. PERSONAL HISTORY</b></h5>
                          <table class="table">
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Prefix<%-- (คำนำหน้า)--%> :</b></th>
                              <td><select class="small m-wrap" tabindex="1">
								    <option value="Category 1">Ms.</option>
									    <option value="Category 2">Mrs.</option>
									    <option value="Category 3">Mr.</option>
							    </select></td>
                            </tr>
                             <tr>
                              <th style="width: 250px"><b class="pull-right">Family name (as shown in passport) :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 250px"><b class="pull-right">Given names :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Sex  <%--(เพศ)--%> :</b></th>
                              <td><select class="small m-wrap" tabindex="1">
								        <option value="Category 1">Male</option>
									    <option value="Category 2">Female</option>
							    </select></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Country and City Of Birth :</b></th>
                               <td><select class="medium m-wrap" tabindex="1">
									    <option value="Category 1">Select Country</option>
									    <option value="Category 2">Category 2</option>
									    <option value="Category 3">Category 5</option>
									    <option value="Category 4">Category 4</option>
						            </select>
                                   <select class="medium m-wrap" tabindex="1">
									    <option value="Category 1">Select City</option>
									    <option value="Category 2">Category 2</option>
									    <option value="Category 3">Category 5</option>
									    <option value="Category 4">Category 4</option>
						            </select>
                                   
                               </td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Nationality :</b></th>
                              <td><input type="text" placeholder="Nationality" class="m-wrap medium" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Date of Birth :</b></th>
                              <td><input type="text" placeholder="DD/MM/YYS" class="m-wrap medium" />
                                  <select class="small m-wrap" tabindex="1">
                                        <option value="Category 1">Select Age</option>
									    <option value="Category 2">1</option>
									    <option value="Category 3">2</option>
									    <option value="Category 4">3</option>
									    <option value="Category 5">4</option>
								    </select></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Marital Status :</b></th>
                              <td><select class="small m-wrap" tabindex="1">
                                        <option value="Category 1">Select Status</option>
                                        <option value="Category 1">single</option>
									    <option value="Category 2">married</option>
									    <option value="Category 3">separated</option>
									    <option value="Category 4">divorced</option>
									    <option value="Category 5">widowed</option>
								    </select></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Religion :</b></th>
                              <td><input type="text" placeholder="Religion" class="m-wrap medium" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Work Address :</b></th>
                              <td><textarea class="large m-wrap" placeholder="Address" rows="2"></textarea></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
                              <td><input type="text" placeholder="Telephone1" class="m-wrap medium" />
                                  <input type="text" placeholder="Telephone2" class="m-wrap medium" />
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Fax :</b></th>
                              <td><input type="text" placeholder="Fax" class="m-wrap medium" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Email :</b></th>
                              <td><input type="text" placeholder="Email" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Home Address :</b></th>
                              <td><textarea class="large m-wrap" placeholder="Address" rows="2"></textarea></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
                              <td><input type="text" placeholder="Telephone1" class="m-wrap medium" />
                                  <input type="text" placeholder="Telephone2" class="m-wrap medium" />
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">International Airport :</b><br /><b class="pull-right">City for departture :</b></th>
                              <td><textarea class="large m-wrap" rows="2"></textarea></td>
                            </tr>
                            <tr>
                                  <th style="width: 250px"><b class="pull-right">Name and address of person to<br /> be notified in case of emergency :</b></th>
                                  <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Telephone :</b></th>
                              <td><input type="text" placeholder="Telephone" class="m-wrap medium" />
                                  <input type="text" placeholder="Relationship with applicant" class="m-wrap large" />
                              </td>
                            </tr>



                            
                          <%--  <tr>
                              <th style="width: 250px"><b class="pull-right">Address (ที่อยู่) :</b></th>
                              <td><textarea class="large m-wrap" rows="2"></textarea></td>
                            </tr>
                            <tr>
                                  <th style="width: 250px"><b class="pull-right">Date Expire (วันหมดอายุ) :</b></th>
                                  <td><input type="text" placeholder="DD/MM/YYS" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Region (ภูมิภาค) :</b></th>
                              <td><select class="medium m-wrap" tabindex="1">
									    <option value="Category 1">ทวีปเอเชีย</option>
									    <option value="Category 2">ทวีปแอฟริกา</option>
									    <option value="Category 3">ทวีปอเมริกาเหนือ</option>
									    <option value="Category 4">ทวีปอเมริกาใต้ </option>
									    <option value="Category 5">ทวีปแอนตาร์กติกา </option>
									    <option value="Category 6">ทวีปออสเตรเลีย  </option>
									    <option value="Category7">ทวีปยุโรป  </option>
						            </select></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Country (ประเทศ) :</b></th>
                               <td><select class="medium m-wrap" tabindex="1">
									    <option value="Category 1">Thailand</option>
									    <option value="Category 2">Category 2</option>
									    <option value="Category 3">Category 5</option>
									    <option value="Category 4">Category 4</option>
						            </select></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Passport ID (เลขบัตรพาสปอร์ต) :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Passport Expire (วันหมดอายุ) :</b></th>
                              <td><input type="text" placeholder="DD/MM/YYS" class="m-wrap medium" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Expiry Date of Insurance</b><br /><b class="pull-right">(วันหมดอายุประกันภัย) :</b></th>
                              <td><input type="text" placeholder="DD/MM/YYS" class="m-wrap medium" /></td>
                            </tr>
                            <tr>
                              <th style="width: 250px"><b class="pull-right">Expiry Date of VISA (วันหมดอายุวีซ่า) :</b></th>
                              <td><input type="text" placeholder="DD/MM/YYS" class="m-wrap medium" /></td>
                            </tr>--%>
                            </table>

						    <hr>
							    <div class="control-group">
								    <label class="control-label"></label>
									    <div class="controls">
										    <a  href="#Edu" data-toggle="tab" type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										    <button type="button" class="btn red">Cancel</button>
									    </div>
								    </div>
							
			                    </div>  
                         <div class="tab-pane fade" id="Edu">
				            <h4 class="text-info"><b>B. EDUCATION RECORD</b></h4>
						    <!-- BEGIN FORM-->
                            
                              <table class="table">
                               <tr>
                                 <td class="text-primary" colspan="2">
                                      <div class="box-body no-padding"> 
                                           <table class="table table-bordered"">
                                                        <tr>
                                                          <th style="width: 150px" class="bg-gray"rowspan="2"><b>languages</b></th>
                                                          <th style="width: 50px" class="bg-gray" colspan="3"><b>Read</b></th>
                                                          <th style="width: 50px" class="bg-gray" colspan="3"><b>Write</b></th>
                                                          <th style="width: 50px" class="bg-gray" colspan="3"><b>Speak</b></th>
                                                         </tr>
                                                        <tr>
                                                          <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Excellent</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Good</b></th>
                                                          <th style="width: 50px" class="bg-gray"><b>Fair</b></th>
                                                         </tr>
                                                         <tr>
                                                          <td><b>Mother Tongue :</b><input type="text" placeholder="" class="m-wrap medium" /></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                         </tr>
                                                         <tr>
                                                          <td><b>English</b></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                        </tr>
                                                        <tr>
                                                          <td><b>Other :</b><input type="text" placeholder="" class="m-wrap medium" /></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r1" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r2" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                          <td><input type="radio" name="r3" class="minimal"></td>
                                                        </tr>
                                            </table>
           
                                       </div></td>             
                                  </tr>
                              
                                <tr>
                                  <th style="width: 230px"><b class="pull-right">English Proficiency Test :</b></th>
                                  <td class="text-primary">
                                          <table class="table table-bordered">
                                            <tr>
                                              <th style="width: 150px" class="bg-gray"><b>TOEFL Score </b></th>
                                              <td><input type="text" placeholder="TOEFL Score" class="m-wrap medium" /></td>
                                             </tr>
                                             <tr>
                                              <th style="width: 150px" class="bg-gray"><b>IELTs Score</b></th>
                                              <td><input type="text" placeholder="IELTs Score" class="m-wrap medium" /></td>
                                            </tr>
                                             <tr>
                                              <th style="width: 150px" class="bg-gray"><b>Other (specify)</b></th>
                                              <td><input type="text" placeholder="Other (specify)" class="m-wrap large" /></td>
                                            </tr>
                                    </table></td>
                                </tr>
                                <tr>
                                  <tr>
                                  <td class="text-primary" colspan="2"> 
                                    <div class="box-body no-padding"> 
                                          <table class="table table-bordered"">
                                 
                                            <tr>
                                              <th style="width: 150px" class="bg-gray" rowspan="2"><b>Education Institution</b></th>
                                              <th style="width: 50px" class="bg-gray" rowspan="2"><b>City / Country</b></th>
                                              <th style="width: 50px" class="bg-gray" colspan="2"><b>Years Attended</b></th>
                                              <th style="width: 50px" class="bg-gray" rowspan="2"><b>Degrees, Diplomas and Certificates :</b></th>
                                              <th style="width: 50px" class="bg-gray" rowspan="2"><b>Special fields of study</b></th>
                                             </tr>
                                            <tr>
                                              <th style="width: 50px" class="bg-gray"><b>From</b></th>
                                              <th style="width: 50px" class="bg-gray"><b>To</b></th>
                                             </tr>
                                             <tr>
                                              <td><input type="text" placeholder="" class="m-wrap medium" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                            </tr>
                                             <tr>
                                              <td><input type="text" placeholder="" class="m-wrap medium" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                            </tr>
                                             <tr>
                                              <td><input type="text" placeholder="" class="m-wrap medium" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                              <td><input type="text" placeholder="" class="m-wrap small" /></td>
                                            </tr>
                                    </table>
                                      </div></td>
                                </tr>
                         
                                </tr>
                                <tr>
                                  <th style="width: 230px"><b class="pull-right">Have you ever been trained in Thailand? If yes, please specify title of the course, where and for how long? </b></th>
                                  <td><textarea class="large m-wrap" rows="3"></textarea></td>
                                </tr>
                                <tr>
                                  <th style="width: 230px"><b class="pull-right">For a candidate for a degree program, please give a list of relevant publications/researches (do not attach details) </b></th>
                                  <td><textarea class="large m-wrap" rows="3"></textarea></td>
                                </tr>

                            </table>
						    <hr>
							    <div class="control-group">
								    <label class="control-label"></label>
									    <div class="controls">
										    <a  href="#Emp" data-toggle="tab" type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										    <button type="button" class="btn red">Cancel</button>
									    </div>
								    </div>
			             </div> 
                        <div class="tab-pane fade" id="Emp">
				            <h4 class="text-info"><b>C. EMPLOYMENT RECORD</b></h4>
						    <!-- BEGIN FORM-->
                          <table class="table">
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Present or most recent post :</b></th>
                              <td><input type="text" placeholder="2555" class="m-wrap small" /> <b>To</b>
                                  <input type="text" placeholder="2558" class="m-wrap small" />
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
                              <td><textarea class="large m-wrap" placeholder="Official Address" rows="2"></textarea>
                                  <textarea class="large m-wrap" placeholder="Desrciption"  rows="3"></textarea>
                              </td>
                            </tr>
                           </table>
                           <table class="table">
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Previous post : </b></th>
                              <td><input type="text" placeholder="" class="m-wrap small" /> <b>To</b>
                                  <input type="text" placeholder="" class="m-wrap small" />
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Title of your post :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Name of organisation :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                            <tr>
                              <th style="width: 200px"><b class="pull-right">Type of organisation :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 200px"><b class="pull-right">Official Address :</b></th>
                              <td><textarea class="large m-wrap" placeholder="Official Address" rows="2"></textarea>
                                  <textarea class="large m-wrap" placeholder="Desrciption"  rows="3"></textarea>
                              </td>
                            </tr>
                           </table>
                            <hr>
							<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div>
							   
			                 </div>
                        <div class="tab-pane fade" id="Exp">
				            <h4 class="text-info"><b>D. EXPECTATIONS</b></h4>
						    <!-- BEGIN FORM-->
                          <table class="table">
                            <tr>
                              <td colspan="2"><b>Please describe the practical use you will make of this training/study on your return home in relation to the responsibilities you expect to assume and the conditions existing in your country in the field of your training.(give the attached paper, if necessary) </b><br />
                                  <textarea class="large m-wrap" placeholder="Desrciption"  rows="3"></textarea>
                              </td>
                            </tr>
                             <tr>
                              <td colspan="2"><b>REFERENCES</b>(only a candidate for a degree program please attach the recommendation letters from two persons acquainted with your academic and professional experiences.)<br />
                              
                                  I certify that my statements in answer to the foregoing questions are true, complete and correct to the best of my knowledge and belief. <br />
                                  If accepted for a training award, I undertake to :-<br />
                                  (a)	carry out such instructions and abide by such conditions as may be stipulated by both the nominating government and the host government in respect of this course of training;<br />
                                  (b)	follow the course of training, and abide by the rules of the University or other institutions or establishment in which I undertake to train;<br />
                                  (c)	refrain from engaging in political activities, or any form of employment for profit or gain;<br />
                                  (d)	submit any progress reports which may be prescribed;<br />
                                  (e)	return to my home country promptly upon the completion of my course of training
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2">I also fully understand that if I am granted a fellowship award, it may be subsequently withdrawn if I fail to make adequate progress or for other sufficient cause determined by the host Government.</td>
                            </tr>
                            <tr>
                              <th style="width: 550px"><b class="pull-right">Signature of applicant :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 550px"><b class="pull-right">Printed name :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 550px"><b class="pull-right">Date :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap medium" /></td>
                            </tr>
                           </table>

                            <hr>
							<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div>
			                 </div> 
                        <div class="tab-pane fade" id="Gov">
				            <h4 class="text-info"><b>E.GOVERNMENT AUTHORISATION</b></h4>
						    <!-- BEGIN FORM-->
                              <table class="table">
                            <tr>
                              <td colspan="2">I certify that, to the best of my knowledge, <br />
                                   (a) all information supplied by the nominee is complete and correct ; <br />
                                   (b) the nominee has adequate knowledge and experience in related fields and has adequate English proficiency for the purpose of the fellowship in Thailand.<br />
                                    On return from the fellowship, the nominee will be employed in the following position :
                              </td>
                            </tr>
                            <tr>
                              <th style="width: 180px"><b class="pull-right">Title of post :</b></th>
                              <td><textarea class="large m-wrap" placeholder="Desrciption"  rows="2"></textarea></td>
                            </tr>
                            <tr>
                              <th style="width: 180px"><b class="pull-right">Duties and responsibilities :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap large" /></td>
                            </tr>
                             <tr>
                              <th style="width: 180px"><b class="pull-right"> Official stamp:</b></th>
                              <td><input type="text" placeholder="" class="m-wrap medium" />   <b>Title :</b>
                                  <input type="text" placeholder="" class="m-wrap medium" />
                              </td>
                            </tr>
                             <tr>
                              <th style="width: 180px"><b class="pull-right"> Organisation :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap medium" /><b> Official Address  :</b>
                                  <textarea class="medium m-wrap" placeholder="Desrciption"  rows="2"></textarea>
                              </td>
                            </tr>

                             <tr>
                              <th style="width: 180px"><b class="pull-right">Date :</b></th>
                              <td><input type="text" placeholder="" class="m-wrap medium" /></td>
                            </tr>
                           </table>
                            <hr>
							<div class="control-group">
								<label class="control-label"></label>
									<div class="controls">
										<a type="submit" class="btn green"><i class="icon-ok"></i> Save </a>
										<button type="button" class="btn red">Cancel</button>
									</div>
								</div>    
			             </div>
                         </div> 
                     </div>
		         </div>
		       <!-- END ROW 1 -->
				
             </div>
          <!-- END CONTAINER -->
         </asp:Content>

